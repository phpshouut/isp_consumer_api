-- phpMyAdmin SQL Dump
-- version 4.0.10.19
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 11, 2017 at 05:55 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `radius`
--

-- --------------------------------------------------------

--
-- Table structure for table `nas`
--

CREATE TABLE IF NOT EXISTS `nas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nasname` varchar(128) NOT NULL,
  `shortname` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT 'other',
  `ports` int(5) DEFAULT NULL,
  `secret` varchar(60) NOT NULL DEFAULT 'secret',
  `server` varchar(64) DEFAULT NULL,
  `community` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT 'RADIUS Client',
  `nastype` int(1) NOT NULL DEFAULT '0',
  `nasfqdn` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nasname` (`nasname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `nas`
--

INSERT INTO `nas` (`id`, `nasname`, `shortname`, `type`, `ports`, `secret`, `server`, `community`, `description`, `nastype`, `nasfqdn`) VALUES
(30, '172.16.255.255', 'SDA Market', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(31, '172.16.26.248', 'SHOUUT Core', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(32, '172.16.255.254', 'ShouutOffice', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(33, '172.16.255.253', 'DLF_promenade', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(34, '172.16.255.252', 'sdashouut', '0', NULL, 'testing123', NULL, NULL, 'API password : sdashouut', 0, NULL),
(35, '172.16.255.251', 'testingrouterpkv', '0', NULL, 'testing123', NULL, NULL, 'pwd : testing123', 0, NULL),
(36, '172.16.255.250', 'SatyaNiketan', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(37, '172.16.255.249', 'Dharamshala1', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(38, '172.16.255.248', 'officewap', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(39, '172.16.255.247', 'airtel4goff', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(40, '172.16.255.246', 'ozonetest1', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(41, '172.16.255.245', 'telexcelltest', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(42, '172.16.255.244', 'ozone', '255', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(43, '172.16.255.243', 'KhagenHOTSPOT', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(44, '172.16.255.242', 'TELEXCELL1', '0', NULL, 'testing123', NULL, NULL, 'testing123radiusapi', 0, NULL),
(45, '172.16.255.241', 'Bhushantest', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(46, '172.16.255.240', 'OyoAir028DEL', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(47, '172.16.255.239', 'OyoAir028ADEL', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(50, '172.16.255.238', 'dnsinfo033jai', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(51, '172.16.255.237', 'Airgenie', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(52, '172.16.255.236', 'svgdns030jpr', '0', NULL, 'testing123', NULL, NULL, 'testing123radiusapi', 0, NULL),
(53, '172.16.255.235', 'svgdns031jpr', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(54, '172.16.255.230', 'Javedtry', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(55, '172.16.255.229', 'cenvay038dhm', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(56, '172.16.255.228', 'keralatrial', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(57, '172.16.255.227', 'babvay040dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(58, '172.16.255.226', 'babvay041dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(59, '172.16.255.225', 'bhrvay042dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(60, '172.16.255.224', 'irdvay043dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(61, '172.16.255.223', 'hqaotmationtest44', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(62, '172.16.255.222', 'mbrvay045dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(63, '172.16.255.221', 'bbjdns046jai', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(64, '172.16.255.220', 'nitbsn047frb', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(65, '172.16.255.219', 'sfasbb048del', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(66, '172.16.255.218', 'tisstc049mum', '0', NULL, 'testing123', NULL, NULL, '', 0, NULL),
(67, '172.16.255.217', 'dhdvay051dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(68, '172.16.255.216', '⁠⁠⁠jsjdns050jai', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(69, '172.16.255.215', 'skfbsnl052fbd', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(70, '172.16.255.214', 'ltdvay61dhm', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(71, '172.16.255.213', 'jgbktpl062guj', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(72, '171.61.145.44', 'testpra063shouut', '0', 0, 'testing123', NULL, '', '', 1, '5c1304b96bbb.sn.mynetname.net'),
(73, '172.16.255.211', 'tstpra66shouut', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(74, '172.16.255.210', 'sbsleo65mum', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(75, '172.16.255.209', 'kbshbb066', '0', 0, 'testing123', NULL, '', '', 0, NULL),
(76, '106.201.19.171', 'mankomaloffice', 'other', NULL, 'testing123', NULL, NULL, 'RADIUS Client', 1, '6769057dc707.sn.mynetname.net');

-- --------------------------------------------------------

--
-- Table structure for table `nas_original`
--

CREATE TABLE IF NOT EXISTS `nas_original` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nasname` varchar(128) NOT NULL,
  `shortname` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT 'other',
  `ports` int(5) DEFAULT NULL,
  `secret` varchar(60) NOT NULL DEFAULT 'secret',
  `server` varchar(64) DEFAULT NULL,
  `community` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT 'RADIUS Client',
  PRIMARY KEY (`id`),
  KEY `nasname` (`nasname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `radacct`
--

CREATE TABLE IF NOT EXISTS `radacct` (
  `radacctid` bigint(21) NOT NULL AUTO_INCREMENT,
  `acctsessionid` varchar(64) NOT NULL DEFAULT '',
  `acctuniqueid` varchar(32) NOT NULL DEFAULT '',
  `username` varchar(64) NOT NULL DEFAULT '',
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `realm` varchar(64) DEFAULT '',
  `nasipaddress` varchar(15) NOT NULL DEFAULT '',
  `nasportid` varchar(15) DEFAULT NULL,
  `nasporttype` varchar(32) DEFAULT NULL,
  `acctstarttime` datetime DEFAULT NULL,
  `acctstoptime` datetime DEFAULT NULL,
  `acctsessiontime` int(12) unsigned DEFAULT NULL,
  `acctauthentic` varchar(32) DEFAULT NULL,
  `connectinfo_start` varchar(50) DEFAULT NULL,
  `connectinfo_stop` varchar(50) DEFAULT NULL,
  `acctinputoctets` bigint(20) DEFAULT NULL,
  `acctoutputoctets` bigint(20) DEFAULT NULL,
  `calledstationid` varchar(50) NOT NULL DEFAULT '',
  `callingstationid` varchar(50) NOT NULL DEFAULT '',
  `acctterminatecause` varchar(32) NOT NULL DEFAULT '',
  `servicetype` varchar(32) DEFAULT NULL,
  `framedprotocol` varchar(32) DEFAULT NULL,
  `framedipaddress` varchar(15) NOT NULL DEFAULT '',
  `acctstartdelay` int(12) unsigned DEFAULT NULL,
  `acctstopdelay` int(12) unsigned DEFAULT NULL,
  `xascendsessionsvrkey` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`radacctid`),
  UNIQUE KEY `acctuniqueid` (`acctuniqueid`),
  KEY `username` (`username`),
  KEY `framedipaddress` (`framedipaddress`),
  KEY `acctsessionid` (`acctsessionid`),
  KEY `acctsessiontime` (`acctsessiontime`),
  KEY `acctstarttime` (`acctstarttime`),
  KEY `acctstoptime` (`acctstoptime`),
  KEY `nasipaddress` (`nasipaddress`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `radacct`
--

INSERT INTO `radacct` (`radacctid`, `acctsessionid`, `acctuniqueid`, `username`, `groupname`, `realm`, `nasipaddress`, `nasportid`, `nasporttype`, `acctstarttime`, `acctstoptime`, `acctsessiontime`, `acctauthentic`, `connectinfo_start`, `connectinfo_stop`, `acctinputoctets`, `acctoutputoctets`, `calledstationid`, `callingstationid`, `acctterminatecause`, `servicetype`, `framedprotocol`, `framedipaddress`, `acctstartdelay`, `acctstopdelay`, `xascendsessionsvrkey`) VALUES
(1, '80100000', 'aa96204bab0630ad', 'user', '', '', '192.168.10.210', '2148532224', 'Wireless-802.11', '2017-04-03 21:14:57', '2017-04-03 21:26:54', 718, '', '', '', 41585, 52668, 'hotspot1', '0C:51:01:35:BC:4A', 'Admin-Reset', '', '', '192.168.1.220', 0, 0, ''),
(2, '80100001', '75fe9b6748b24cf3', 'user', '', '', '192.168.10.210', '2148532225', 'Wireless-802.11', '2017-04-03 21:34:44', '2017-04-03 21:37:27', 163, '', '', '', 3145, 3327, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.220', 0, 0, ''),
(3, '80100002', '4217de2f09b9f63f', 'user', '', '', '192.168.10.210', '2148532226', 'Wireless-802.11', '2017-04-04 06:56:12', '2017-04-04 07:01:26', 315, '', '', '', 237959, 512381, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.218', 0, 0, ''),
(4, '80100004', '7d8b63c2f7d8043a', 'user', '', '', '192.168.10.210', '2148532228', 'Wireless-802.11', '2017-04-04 07:28:37', '2017-04-04 07:35:55', 438, '', '', '', 12568, 22016, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.218', 0, 0, ''),
(6, '80100005', '84badec8ee90a963', 'user', '', '', '192.168.10.210', '2148532229', 'Wireless-802.11', '2017-04-04 07:40:08', '2017-04-04 07:45:34', 326, '', '', '', 36922, 48271, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.218', 0, 0, ''),
(7, '80100006', 'b17dcd9e06f36d3e', 'user', '', '', '192.168.10.210', '2148532230', 'Wireless-802.11', '2017-04-04 07:58:21', '2017-04-04 08:11:52', 810, '', '', '', 63077, 369630, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.218', 0, 1, ''),
(8, '80100008', '203e63894d3742ce', 'user', '', '', '192.168.10.210', '2148532232', 'Wireless-802.11', '2017-04-06 06:41:33', '2017-04-06 07:38:49', 3436, '', '', '', 495076, 2873501, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.218', 0, 0, ''),
(9, '80100009', '35bd6ad57d585123', '7982081401', '', '', '192.168.10.210', '2148532233', 'Wireless-802.11', '2017-04-06 07:58:29', '2017-04-06 09:17:07', 4717, '', '', '', 930530, 8945205, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Service', '', '', '192.168.1.218', 0, 0, ''),
(10, '80300001', 'a3f77494700b7b0b', '7752900626', '', '', '192.168.1.210', '2150629377', 'Wireless-802.11', '2017-04-07 07:43:27', '2017-04-07 08:02:29', 1144, '', '', '', 228956, 1042071, 'hotspot1', '0C:51:01:35:BC:4A', 'Lost-Carrier', '', '', '192.168.23.254', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `radcheck`
--

CREATE TABLE IF NOT EXISTS `radcheck` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '==',
  `value` varchar(253) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `username` (`username`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `radcheck`
--

INSERT INTO `radcheck` (`id`, `username`, `attribute`, `op`, `value`) VALUES
(1, 'user', 'Cleartext-Password', ':=', '1111');

-- --------------------------------------------------------

--
-- Table structure for table `radgroupcheck`
--

CREATE TABLE IF NOT EXISTS `radgroupcheck` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '==',
  `value` varchar(253) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `groupname` (`groupname`(32))
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `radgroupreply`
--

CREATE TABLE IF NOT EXISTS `radgroupreply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '=',
  `value` varchar(253) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `groupname` (`groupname`(32))
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `radpostauth`
--

CREATE TABLE IF NOT EXISTS `radpostauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `pass` varchar(64) NOT NULL DEFAULT '',
  `reply` varchar(32) NOT NULL DEFAULT '',
  `authdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `radpostauth`
--

INSERT INTO `radpostauth` (`id`, `username`, `pass`, `reply`, `authdate`) VALUES
(1, 'user', '1111', 'Access-Accept', '2017-04-03 14:40:31'),
(2, 'user', '0x2fd914e9b7776e50cc2a936704f03915d8', 'Access-Accept', '2017-04-03 15:44:56'),
(3, 'user', '0x2a71e338948ea89226115b2de4a159a000', 'Access-Accept', '2017-04-03 16:04:44'),
(4, 'user', '0x599102697c296f75c31114045551d025ff', 'Access-Accept', '2017-04-04 01:26:11'),
(5, 'user', '0x6339259b21b5ccd55411bbfcb8742cd9fc', 'Access-Accept', '2017-04-04 01:29:32'),
(6, 'user', '0x33c1571654ead93b56f7a61c91bf0a2a39', 'Access-Accept', '2017-04-04 01:58:36'),
(7, 'user', '0xf5c4809a8abb6760fc4ece14826fb8de13', 'Access-Accept', '2017-04-04 02:10:08'),
(8, 'user', '0xdc2cf1acaac77ca73174042884cafc3110', 'Access-Accept', '2017-04-04 02:28:21'),
(9, 'user', '0xef613026d67211acce3016053318a33e6b', 'Access-Accept', '2017-04-06 01:11:33'),
(10, 'user', '0xef613026d67211acce3016053318a33e6b', 'Access-Accept', '2017-04-06 02:28:29'),
(11, 'user', '0x65b8a258e20c6a6de82509e5bb994ad29e', 'Access-Accept', '2017-04-07 02:13:26');

-- --------------------------------------------------------

--
-- Table structure for table `radreply`
--

CREATE TABLE IF NOT EXISTS `radreply` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL DEFAULT '',
  `attribute` varchar(64) NOT NULL DEFAULT '',
  `op` char(2) NOT NULL DEFAULT '=',
  `value` varchar(253) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `username` (`username`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `radreply`
--

INSERT INTO `radreply` (`id`, `username`, `attribute`, `op`, `value`) VALUES
(1, 'user', 'Mikrotik-Rate-Limit', '=', '10240k/10240k');

-- --------------------------------------------------------

--
-- Table structure for table `radusergroup`
--

CREATE TABLE IF NOT EXISTS `radusergroup` (
  `username` varchar(64) NOT NULL DEFAULT '',
  `groupname` varchar(64) NOT NULL DEFAULT '',
  `priority` int(11) NOT NULL DEFAULT '1',
  KEY `username` (`username`(32))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sht_allowednases`
--

CREATE TABLE IF NOT EXISTS `sht_allowednases` (
  `srvid` int(11) NOT NULL,
  `nasid` int(11) NOT NULL,
  KEY `srvid` (`srvid`),
  KEY `nasid` (`nasid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sht_allowednases`
--

INSERT INTO `sht_allowednases` (`srvid`, `nasid`) VALUES
(0, 30),
(0, 31),
(35, 31),
(0, 32),
(35, 32),
(0, 33),
(35, 33),
(0, 34),
(35, 34),
(0, 35),
(35, 35),
(0, 36),
(35, 36),
(0, 37),
(35, 37),
(0, 38),
(35, 38),
(0, 39),
(35, 39),
(0, 40),
(35, 40),
(0, 41),
(35, 41),
(0, 42),
(35, 42),
(0, 43),
(35, 43),
(0, 44),
(35, 44),
(0, 45),
(35, 45),
(0, 46),
(35, 46),
(0, 47),
(35, 47),
(0, 51),
(35, 51),
(0, 52),
(35, 52),
(0, 53),
(35, 53),
(0, 54),
(35, 54),
(0, 55),
(35, 55),
(0, 56),
(35, 56),
(39, 51),
(39, 39),
(39, 57),
(39, 58),
(39, 59),
(39, 45),
(39, 55),
(39, 37),
(39, 33),
(39, 50),
(39, 60),
(39, 54),
(39, 56),
(39, 43),
(39, 43),
(39, 38),
(39, 47),
(39, 46),
(39, 42),
(39, 40),
(39, 36),
(39, 30),
(39, 34),
(39, 31),
(39, 32),
(39, 52),
(39, 53),
(39, 44),
(39, 41),
(39, 35),
(38, 51),
(38, 39),
(38, 57),
(38, 58),
(38, 59),
(38, 45),
(38, 55),
(38, 37),
(38, 33),
(38, 50),
(38, 60),
(38, 54),
(38, 56),
(38, 43),
(38, 43),
(38, 38),
(38, 47),
(38, 46),
(38, 42),
(38, 40),
(38, 36),
(38, 30),
(38, 34),
(38, 31),
(38, 32),
(38, 52),
(38, 53),
(38, 44),
(38, 41),
(38, 35),
(33, 51),
(33, 39),
(33, 57),
(33, 58),
(33, 59),
(33, 45),
(33, 55),
(33, 37),
(33, 33),
(33, 50),
(33, 60),
(33, 54),
(33, 56),
(33, 43),
(33, 43),
(33, 38),
(33, 47),
(33, 46),
(33, 42),
(33, 40),
(33, 36),
(33, 30),
(33, 34),
(33, 31),
(33, 32),
(33, 52),
(33, 53),
(33, 44),
(33, 41),
(33, 35),
(34, 51),
(34, 39),
(34, 57),
(34, 58),
(34, 59),
(34, 45),
(34, 55),
(34, 37),
(34, 33),
(34, 50),
(34, 60),
(34, 54),
(34, 56),
(34, 43),
(34, 43),
(34, 38),
(34, 47),
(34, 46),
(34, 42),
(34, 40),
(34, 36),
(34, 30),
(34, 34),
(34, 31),
(34, 32),
(34, 52),
(34, 53),
(34, 44),
(34, 41),
(34, 35),
(33, 61),
(34, 61),
(38, 61),
(39, 61),
(33, 62),
(34, 62),
(38, 62),
(39, 62),
(33, 63),
(34, 63),
(38, 63),
(39, 63),
(33, 64),
(34, 64),
(38, 64),
(39, 64),
(0, 65),
(39, 65),
(35, 65),
(38, 65),
(33, 65),
(34, 65),
(0, 66),
(39, 66),
(35, 66),
(38, 66),
(33, 66),
(34, 66),
(37, 51),
(37, 39),
(37, 57),
(37, 58),
(37, 63),
(37, 59),
(37, 45),
(37, 55),
(37, 37),
(37, 33),
(37, 50),
(37, 61),
(37, 60),
(37, 54),
(37, 56),
(37, 43),
(37, 43),
(37, 62),
(37, 64),
(37, 38),
(37, 47),
(37, 46),
(37, 42),
(37, 40),
(37, 36),
(37, 30),
(37, 34),
(37, 65),
(37, 31),
(37, 32),
(37, 52),
(37, 53),
(37, 44),
(37, 41),
(37, 35),
(37, 66),
(36, 51),
(36, 39),
(36, 57),
(36, 58),
(36, 63),
(36, 59),
(36, 45),
(36, 55),
(36, 37),
(36, 33),
(36, 50),
(36, 61),
(36, 60),
(36, 54),
(36, 56),
(36, 43),
(36, 43),
(36, 62),
(36, 64),
(36, 38),
(36, 47),
(36, 46),
(36, 42),
(36, 40),
(36, 36),
(36, 30),
(36, 34),
(36, 65),
(36, 31),
(36, 32),
(36, 52),
(36, 53),
(36, 44),
(36, 41),
(36, 35),
(36, 66),
(33, 67),
(34, 67),
(36, 67),
(37, 67),
(38, 67),
(39, 67),
(33, 68),
(34, 68),
(36, 68),
(37, 68),
(38, 68),
(39, 68),
(33, 69),
(34, 69),
(36, 69),
(37, 69),
(38, 69),
(39, 69),
(40, 51),
(40, 39),
(40, 57),
(40, 58),
(40, 63),
(40, 59),
(40, 45),
(40, 55),
(40, 37),
(40, 67),
(40, 33),
(40, 50),
(40, 61),
(40, 60),
(40, 54),
(40, 56),
(40, 43),
(40, 62),
(40, 64),
(40, 38),
(40, 47),
(40, 46),
(40, 42),
(40, 40),
(40, 36),
(40, 30),
(40, 34),
(40, 65),
(40, 31),
(40, 32),
(40, 69),
(40, 52),
(40, 53),
(40, 44),
(40, 41),
(40, 35),
(40, 66),
(40, 68),
(41, 51),
(41, 39),
(41, 57),
(41, 58),
(41, 63),
(41, 59),
(41, 45),
(41, 55),
(41, 37),
(41, 67),
(41, 33),
(41, 50),
(41, 61),
(41, 60),
(41, 54),
(41, 56),
(41, 43),
(41, 62),
(41, 64),
(41, 38),
(41, 47),
(41, 46),
(41, 42),
(41, 40),
(41, 36),
(41, 30),
(41, 34),
(41, 65),
(41, 31),
(41, 32),
(41, 69),
(41, 52),
(41, 53),
(41, 44),
(41, 41),
(41, 35),
(41, 66),
(41, 68),
(33, 70),
(34, 70),
(36, 70),
(37, 70),
(38, 70),
(39, 70),
(33, 71),
(34, 71),
(36, 71),
(37, 71),
(38, 71),
(39, 71),
(33, 72),
(34, 72),
(36, 72),
(37, 72),
(38, 72),
(39, 72),
(33, 73),
(34, 73),
(36, 73),
(37, 73),
(38, 73),
(39, 73),
(33, 74),
(34, 74),
(36, 74),
(37, 74),
(38, 74),
(39, 74),
(33, 75),
(34, 75),
(36, 75),
(37, 75),
(38, 75),
(39, 75);

-- --------------------------------------------------------

--
-- Table structure for table `sht_nas`
--

CREATE TABLE IF NOT EXISTS `sht_nas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nasname` varchar(128) NOT NULL,
  `shortname` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT 'other',
  `ports` int(5) DEFAULT NULL,
  `secret` varchar(60) NOT NULL DEFAULT 'secret',
  `community` varchar(50) DEFAULT NULL,
  `description` varchar(200) DEFAULT 'RADIUS Client',
  `starospassword` varchar(32) NOT NULL,
  `ciscobwmode` tinyint(1) NOT NULL,
  `apiusername` varchar(32) NOT NULL,
  `apipassword` varchar(32) NOT NULL,
  `enableapi` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nasname` (`nasname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `sht_nas`
--

INSERT INTO `sht_nas` (`id`, `nasname`, `shortname`, `type`, `ports`, `secret`, `community`, `description`, `starospassword`, `ciscobwmode`, `apiusername`, `apipassword`, `enableapi`) VALUES
(30, '172.16.255.255', 'SDA Market', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123api', 1),
(31, '172.16.26.248', 'SHOUUT Core', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(32, '172.16.255.254', 'ShouutOffice', '0', NULL, 'testing123', NULL, '', '', 0, 'API', 'testing123', 1),
(33, '172.16.255.253', 'DLF_promenade', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123', 1),
(34, '172.16.255.252', 'sdashouut', '0', NULL, 'testing123', NULL, 'API password : sdashouut', '', 0, 'API', 'sdashouut', 1),
(35, '172.16.255.251', 'testingrouterpkv', '0', NULL, 'testing123', NULL, 'pwd : testing123', '', 0, 'API', 'testing123', 1),
(36, '172.16.255.250', 'SatyaNiketan', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123', 1),
(37, '172.16.255.249', 'Dharamshala1', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123api', 1),
(38, '172.16.255.248', 'officewap', '0', NULL, 'testing123', NULL, '', '', 0, 'API', 'testing123radiusapi', 1),
(39, '172.16.255.247', 'airtel4goff', '0', NULL, 'testing123', NULL, '', '', 0, 'API', 'testing123radiusapi', 1),
(40, '172.16.255.246', 'ozonetest1', '0', NULL, 'testing123', NULL, '', '', 0, 'API', 'testing123radiusapi', 1),
(41, '172.16.255.245', 'telexcelltest', '0', NULL, 'testing123', NULL, '', '', 0, 'API', 'testing123radiusapi', 1),
(42, '172.16.255.244', 'ozone', '255', NULL, 'testing123', NULL, '', '', 0, '', '', 0),
(43, '172.16.255.243', 'KhagenHOTSPOT', '0', NULL, 'testing123', NULL, '', '', 0, 'API', 'testing123radiusapi', 1),
(44, '172.16.255.242', 'TELEXCELL1', '0', NULL, 'testing123', NULL, 'testing123radiusapi', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(45, '172.16.255.241', 'Bhushantest', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'radiusapitesting123', 1),
(46, '172.16.255.240', 'OyoAir028DEL', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'radiusapitesting123', 1),
(47, '172.16.255.239', 'OyoAir028ADEL', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'radiusapitesting123', 1),
(50, '172.16.255.238', 'dnsinfo033jai', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(51, '172.16.255.237', 'Airgenie', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(52, '172.16.255.236', 'svgdns030jpr', '0', NULL, 'testing123', NULL, 'testing123radiusapi', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(53, '172.16.255.235', 'svgdns031jpr', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(54, '172.16.255.230', 'Javedtry', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(55, '172.16.255.229', 'cenvay038dhm', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(56, '172.16.255.228', 'keralatrial', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(57, '172.16.255.227', 'babvay040dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(58, '172.16.255.226', 'babvay041dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(59, '172.16.255.225', 'bhrvay042dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(60, '172.16.255.224', 'irdvay043dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(61, '172.16.255.223', 'hqaotmationtest44', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(62, '172.16.255.222', 'mbrvay045dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(63, '172.16.255.221', 'bbjdns046jai', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(64, '172.16.255.220', 'nitbsn047frb', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(65, '172.16.255.219', 'sfasbb048del', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(66, '172.16.255.218', 'tisstc049mum', '0', NULL, 'testing123', NULL, '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(67, '172.16.255.217', 'dhdvay051dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(68, '172.16.255.216', '⁠⁠⁠jsjdns050jai', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(69, '172.16.255.215', 'skfbsnl052fbd', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(70, '172.16.255.214', 'ltdvay61dhm', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(71, '172.16.255.213', 'jgbktpl062guj', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', 'testing123radiusapi', 1),
(72, '172.16.255.212', 'testpra063shouut', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(73, '172.16.255.211', 'tstpra66shouut', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(74, '172.16.255.210', 'sbsleo65mum', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1),
(75, '172.16.255.209', 'kbshbb066', '0', 0, 'testing123', '', '', '', 0, 'RadiusAPI', ' radiusapitesting123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sht_services`
--

CREATE TABLE IF NOT EXISTS `sht_services` (
  `srvid` int(11) NOT NULL,
  `srvname` varchar(50) NOT NULL,
  `servicetype` varchar(25) DEFAULT NULL,
  `descr` varchar(255) NOT NULL,
  `downrate` int(11) NOT NULL,
  `uprate` int(11) NOT NULL,
  `limitdl` tinyint(1) NOT NULL,
  `limitul` tinyint(1) NOT NULL,
  `limitcomb` tinyint(1) NOT NULL,
  `limitexpiration` tinyint(1) NOT NULL,
  `limituptime` tinyint(1) NOT NULL,
  `poolname` varchar(50) NOT NULL,
  `unitprice` decimal(25,6) NOT NULL,
  `unitpriceadd` decimal(25,6) NOT NULL,
  `timebaseexp` tinyint(1) NOT NULL,
  `timebaseonline` tinyint(1) NOT NULL,
  `timeunitexp` int(11) NOT NULL,
  `timeunitonline` int(11) NOT NULL,
  `trafficunitdl` int(11) NOT NULL,
  `trafficunitul` int(11) NOT NULL,
  `trafficunitcomb` int(11) NOT NULL,
  `inittimeexp` int(11) NOT NULL,
  `inittimeonline` int(11) NOT NULL,
  `initdl` int(11) NOT NULL,
  `initul` int(11) NOT NULL,
  `inittotal` int(11) NOT NULL,
  `srvtype` tinyint(1) NOT NULL,
  `timeaddmodeexp` tinyint(1) NOT NULL,
  `timeaddmodeonline` tinyint(1) NOT NULL,
  `trafficaddmode` tinyint(1) NOT NULL,
  `monthly` tinyint(1) NOT NULL,
  `enaddcredits` tinyint(1) NOT NULL,
  `minamount` int(20) NOT NULL,
  `minamountadd` int(20) NOT NULL,
  `resetcounters` tinyint(1) NOT NULL,
  `pricecalcdownload` tinyint(1) NOT NULL,
  `pricecalcupload` tinyint(1) NOT NULL,
  `pricecalcuptime` tinyint(1) NOT NULL,
  `unitpricetax` decimal(25,6) NOT NULL,
  `unitpriceaddtax` decimal(25,6) NOT NULL,
  `enableburst` tinyint(1) NOT NULL,
  `dlburstlimit` int(11) NOT NULL,
  `ulburstlimit` int(11) NOT NULL,
  `dlburstthreshold` int(11) NOT NULL,
  `ulburstthreshold` int(11) NOT NULL,
  `dlbursttime` int(11) NOT NULL,
  `ulbursttime` int(11) NOT NULL,
  `enableservice` int(11) NOT NULL,
  `dlquota` bigint(20) NOT NULL,
  `ulquota` bigint(20) NOT NULL,
  `combquota` bigint(20) NOT NULL,
  `timequota` bigint(20) NOT NULL,
  `priority` smallint(6) NOT NULL,
  `nextsrvid` int(11) NOT NULL,
  `dailynextsrvid` int(11) NOT NULL,
  `disnextsrvid` int(11) NOT NULL,
  `availucp` tinyint(1) NOT NULL,
  `renew` tinyint(1) NOT NULL,
  `carryover` tinyint(1) NOT NULL,
  `policymapdl` varchar(50) NOT NULL,
  `policymapul` varchar(50) NOT NULL,
  `custattr` varchar(255) NOT NULL,
  `gentftp` tinyint(1) NOT NULL,
  `cmcfg` varchar(10240) NOT NULL,
  `advcmcfg` tinyint(1) NOT NULL,
  `addamount` int(11) NOT NULL,
  `ignstatip` tinyint(1) NOT NULL,
  `ippool` varchar(50) NOT NULL,
  PRIMARY KEY (`srvid`),
  KEY `srvname` (`srvname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sht_services`
--

INSERT INTO `sht_services` (`srvid`, `srvname`, `servicetype`, `descr`, `downrate`, `uprate`, `limitdl`, `limitul`, `limitcomb`, `limitexpiration`, `limituptime`, `poolname`, `unitprice`, `unitpriceadd`, `timebaseexp`, `timebaseonline`, `timeunitexp`, `timeunitonline`, `trafficunitdl`, `trafficunitul`, `trafficunitcomb`, `inittimeexp`, `inittimeonline`, `initdl`, `initul`, `inittotal`, `srvtype`, `timeaddmodeexp`, `timeaddmodeonline`, `trafficaddmode`, `monthly`, `enaddcredits`, `minamount`, `minamountadd`, `resetcounters`, `pricecalcdownload`, `pricecalcupload`, `pricecalcuptime`, `unitpricetax`, `unitpriceaddtax`, `enableburst`, `dlburstlimit`, `ulburstlimit`, `dlburstthreshold`, `ulburstthreshold`, `dlbursttime`, `ulbursttime`, `enableservice`, `dlquota`, `ulquota`, `combquota`, `timequota`, `priority`, `nextsrvid`, `dailynextsrvid`, `disnextsrvid`, `availucp`, `renew`, `carryover`, `policymapdl`, `policymapul`, `custattr`, `gentftp`, `cmcfg`, `advcmcfg`, `addamount`, `ignstatip`, `ippool`) VALUES
(0, 'Default service', '', '', 0, 0, 0, 0, 0, 0, 0, '', '1.000000', '0.000000', 2, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, '0.190000', '0.000000', 0, 1048576, 1048576, 1048576, 1048576, 10, 10, 0, 0, 0, 0, 0, 4, -1, -1, -1, 0, 0, 0, '512', '128', '', 0, '', 0, 1, 1, '192.168.1.1-192.168.1.255'),
(33, 'SHOUUT30MINS', 'time', '30 mins free access to users can be reloaded by AP', 1048576, 524288, 0, 0, 0, 1, 1, 'public', '100.000000', '0.000000', 3, 0, 1, 30, 0, 0, 0, 1, 30, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, '19.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.2.1-192.168.2.255'),
(34, 'SHOUUT5MINS', 'time', '5 MINs plan for testing', 524288, 262144, 1, 1, 0, 1, 1, 'public', '0.000000', '0.000000', 3, 0, 1, 5, 0, 0, 0, 1, 5, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 10485760, 2097152, 12582912, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.3.1-192.168.3.255'),
(35, 'Shouut Routers', 'data_combo', '', 0, 0, 0, 0, 0, 0, 0, '', '0.000000', '0.000000', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.4.1-192.168.4.255'),
(36, 'SHOUUT15MINS', 'time', '15 MINs plan for testing now its 30mins live CP2', 10240000, 5120000, 0, 0, 0, 0, 1, 'public', '0.000000', '0.000000', 3, 0, 1, 30, 0, 0, 0, 1, 30, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 1073741824, 524288000, 419430400, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.5.1-192.168.5.255'),
(37, 'SHOUUT2HOUR', 'time', '2 Hr free access to users can be reloaded by AP', 10485760, 10485760, 0, 0, 0, 0, 1, 'public', '0.000000', '0.000000', 2, 1, 1, 2, 0, 0, 0, 1, 2, 0, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 10737418240, 10737418240, 0, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.6.1-192.168.26.255'),
(38, 'SHOUUT10MB', 'data', 'ADDING 10MB DATA FOR TESTING', 10240000, 5120000, 0, 0, 1, 0, 0, 'public', '0.000000', '0.000000', 2, 0, 1, 0, 10, 0, 0, 1, 0, 10, 0, 0, 0, 2, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 1073741824, 524288000, 524288000, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.7.1-192.168.7.255'),
(39, 'Shouut Data service 4M download', 'fup', 'Modified bandwidth limit 10240/10240kbps', 10485760, 10485760, 0, 0, 1, 0, 0, 'public', '50.000000', '0.000000', 3, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, '9.500000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 8, -1, -1, -1, 0, 1, 0, '', '', '', 0, '', 0, 1, 0, '192.168.8.1-192.168.8.255'),
(40, 'Surajkund-Dedicated', 'time', '', 10485760, 10485760, 0, 0, 0, 0, 0, 'public1', '0.000000', '0.000000', 2, 1, 0, 24, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 8, -1, -1, -1, 0, 0, 0, '', '', '', 0, '', 0, 1, 0, '192.168.9.1-192.168.9.255'),
(41, 'Surajkund-30Mins', 'time', '', 20480000, 20480000, 0, 0, 0, 0, 1, 'public', '0.000000', '0.000000', 2, 0, 0, 30, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, '0.000000', '0.000000', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 8, -1, -1, -1, 0, 0, 0, '', '', '', 0, '', 0, 1, 0, '192.168.10.1-192.168.10.255');

-- --------------------------------------------------------

--
-- Table structure for table `sht_users`
--

CREATE TABLE IF NOT EXISTS `sht_users` (
  `username` varchar(64) NOT NULL,
  `password` varchar(32) NOT NULL,
  `groupid` int(11) NOT NULL,
  `enableuser` tinyint(1) NOT NULL,
  `uplimit` bigint(20) NOT NULL,
  `downlimit` bigint(20) NOT NULL,
  `comblimit` bigint(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(8) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `gpslat` decimal(17,14) NOT NULL,
  `gpslong` decimal(17,14) NOT NULL,
  `mac` varchar(17) NOT NULL,
  `usemacauth` tinyint(1) NOT NULL,
  `expiration` datetime NOT NULL,
  `uptimelimit` bigint(20) NOT NULL,
  `srvid` int(11) NOT NULL,
  `staticipcm` varchar(15) NOT NULL,
  `staticipcpe` varchar(15) NOT NULL,
  `ipmodecm` tinyint(1) NOT NULL,
  `ipmodecpe` tinyint(1) NOT NULL,
  `poolidcm` int(11) NOT NULL,
  `poolidcpe` int(11) NOT NULL,
  `createdon` date NOT NULL,
  `acctype` tinyint(1) NOT NULL,
  `credits` decimal(20,2) NOT NULL,
  `cardfails` tinyint(4) NOT NULL,
  `createdby` varchar(64) NOT NULL,
  `owner` varchar(64) NOT NULL,
  `taxid` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `maccm` varchar(17) NOT NULL,
  `custattr` varchar(255) NOT NULL,
  `warningsent` tinyint(1) NOT NULL,
  `verifycode` varchar(10) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `selfreg` tinyint(1) NOT NULL,
  `verifyfails` tinyint(4) NOT NULL,
  `verifysentnum` tinyint(4) NOT NULL,
  `verifymobile` varchar(15) NOT NULL,
  `contractid` varchar(50) NOT NULL,
  `contractvalid` date NOT NULL,
  `actcode` varchar(60) NOT NULL,
  `pswactsmsnum` tinyint(4) NOT NULL,
  `alertemail` tinyint(1) NOT NULL,
  `alertsms` tinyint(1) NOT NULL,
  `lang` varchar(30) NOT NULL,
  `lastlogoff` datetime DEFAULT NULL,
  `maclockedstatus` int(2) NOT NULL,
  `plantype` varchar(25) DEFAULT NULL,
  `fupplan` varchar(25) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `srvid` (`srvid`),
  KEY `groupid` (`groupid`),
  KEY `enableuser` (`enableuser`),
  KEY `firstname` (`firstname`),
  KEY `lastname` (`lastname`),
  KEY `company` (`company`),
  KEY `phone` (`phone`),
  KEY `mobile` (`mobile`),
  KEY `address` (`address`),
  KEY `city` (`city`),
  KEY `zip` (`zip`),
  KEY `country` (`country`),
  KEY `state` (`state`),
  KEY `comment` (`comment`(255)),
  KEY `mac` (`mac`),
  KEY `acctype` (`acctype`),
  KEY `email` (`email`),
  KEY `maccm` (`maccm`),
  KEY `owner` (`owner`),
  KEY `staticipcpe` (`staticipcpe`),
  KEY `staticipcm` (`staticipcm`),
  KEY `expiration` (`expiration`),
  KEY `createdon` (`createdon`),
  KEY `contractid` (`contractid`),
  KEY `contractvalid` (`contractvalid`),
  KEY `lastlogoff` (`lastlogoff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sht_users`
--

INSERT INTO `sht_users` (`username`, `password`, `groupid`, `enableuser`, `uplimit`, `downlimit`, `comblimit`, `firstname`, `lastname`, `company`, `phone`, `mobile`, `address`, `city`, `zip`, `country`, `state`, `comment`, `gpslat`, `gpslong`, `mac`, `usemacauth`, `expiration`, `uptimelimit`, `srvid`, `staticipcm`, `staticipcpe`, `ipmodecm`, `ipmodecpe`, `poolidcm`, `poolidcpe`, `createdon`, `acctype`, `credits`, `cardfails`, `createdby`, `owner`, `taxid`, `email`, `maccm`, `custattr`, `warningsent`, `verifycode`, `verified`, `selfreg`, `verifyfails`, `verifysentnum`, `verifymobile`, `contractid`, `contractvalid`, `actcode`, `pswactsmsnum`, `alertemail`, `alertsms`, `lang`, `lastlogoff`, `maclockedstatus`, `plantype`, `fupplan`) VALUES
('7752900626', '832688ff5af6c6dfef974773740ef2b5', 1, 1, 0, 5, 0, 'prashant', '', '', '7752900626', '7752900626', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'ab:bc', 0, '2016-11-22 00:00:00', 900, 36, '', '', 0, 0, 0, 0, '2016-10-23', 0, '0.00', 0, 'admin', 'admin', '', 'aarav2004@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2016-10-23 20:29:52', 1, 'time', 'FUP'),
('7838226699', '2d3b02917ea2900fe129741a9c0f3857', 1, 1, 0, 0, 0, 'Prashant', '', '', '7838226699', '7838226699', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'cd:de', 0, '2017-01-21 00:00:00', 0, 0, '', '', 0, 0, 0, 0, '2016-12-22', 0, '0.00', 0, 'admin', 'admin', '', 'prashpri28@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', NULL, 1, 'time', 'FUP'),
('7982081401', 'a9b4ec2eb4ab7b1b9c3392bb5388119d', 1, 1, 0, 0, 1592786944, 'Prashant', '', '', '7982081401', '7982081401', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'ef:fg', 0, '2017-03-30 00:00:00', 0, 139, '', '', 0, 0, 0, 0, '2016-11-10', 0, '0.00', 0, 'admin', 'admin', '', 'mkyadav@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2017-02-21 14:51:31', 0, 'data', 'FUP'),
('7982096278', 'ef8b5fcc338e003145ac9c134754db71', 1, 1, 0, 0, 209715200, 'Prashant', '', '', '7982096278', '7982096278', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'gh:hi', 0, '2016-11-16 00:00:00', 0, 39, '', '', 0, 0, 0, 0, '2016-11-10', 0, '0.00', 0, 'admin', 'admin', '', 'prashant278@shouut.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2016-11-15 19:34:50', 1, 'data', 'FUP'),
('8106383991', 'fc5dda17bef960b7c200e7d8063142ec', 1, 1, 0, 0, 0, 'Prashanth', '', '', '8106383991', '8106383991', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'ij:jk', 0, '2016-12-27 00:00:00', 1800, 36, '', '', 0, 0, 0, 0, '2016-11-27', 0, '0.00', 0, 'admin', 'admin', '', 'navyseal.ncw@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2016-11-27 17:47:36', 0, 'time', 'FUP'),
('8287276776', '61f2585b0ebcf1f532c4d1ec9a7d51aa', 1, 1, 0, 0, 0, 'Prashant', '', '', '8287276776', '8287276776', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'kl:lm', 0, '2016-11-09 00:00:00', 0, 36, '', '', 0, 0, 0, 0, '2016-10-10', 0, '0.00', 0, 'admin', 'admin', '', 'prashantchandra440@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', NULL, 1, 'time', 'FUP'),
('8287964681', '67974233917cea0e42a49a2fb7eb4cf4', 1, 1, 0, 0, 0, 'Prashant', '', '', '8287964681', '8287964681', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'mn:01', 0, '2016-11-06 00:00:00', 10727, 36, '', '', 0, 0, 0, 0, '2016-10-07', 0, '0.00', 0, 'admin', 'admin', '', 'painterprashant1996@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2016-12-13 22:28:17', 0, 'data', 'FUP'),
('8588988244', '5e6bd7a6970cd4325e587f02667f7f73', 1, 1, 0, 0, 0, 'prashant', '', '', '8588988244', '8588988244', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'op:02', 0, '2016-11-15 00:00:00', 900, 36, '', '', 0, 0, 0, 0, '2016-10-16', 0, '0.00', 0, 'admin', 'admin', '', 'pk.mps4698@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2016-10-16 23:13:06', 1, 'data', 'FUP'),
('8743078478', '1763ea5a7e72dd7ee64073c2dda7a7a8', 1, 1, 0, 0, 0, 'Prashant', '', '', '8743078478', '8743078478', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'qr:12', 0, '2016-12-10 00:00:00', 900, 36, '', '', 0, 0, 0, 0, '2016-11-10', 0, '0.00', 0, 'admin', 'admin', '', 'cabba123@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2016-11-10 23:51:42', 1, 'data', 'FUP'),
('8750736124', '5b970a1d9be0fd100063fd6cd688b73e', 1, 1, 0, 0, 0, 'Prashant', '', '', '8750736124', '8750736124', '', '', '', '', '', '', '0.00000000000000', '0.00000000000000', 'st:13', 0, '2017-04-30 00:00:00', 1800, 36, '', '', 0, 0, 0, 0, '2017-03-31', 0, '0.00', 0, 'admin', 'admin', '', 'prashantsingh.jaunpur01@gmail.com', '', '', 0, '', 0, 0, 0, 0, '', '', '0000-00-00', '', 0, 1, 1, 'English', '2017-03-31 15:51:39', 1, 'data', 'FUP');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
