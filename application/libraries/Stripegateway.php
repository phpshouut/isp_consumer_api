<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("stripe-php/init.php");
class Stripegateway {
    
    public function __construct($stripe){
        /*$stripe = array(
            'secret_key'      => 'sk_live_FbWnEfCFgCQkFrHJSR5xLWaO',
            'publishable_key' => 'pk_live_fEHEWhOB8hlOt5r9uJSLvXd7'
        );*/
        if ($stripe['stripe_secret_key'] !== null && $stripe['stripe_publication_key'] !== null){
            \Stripe\Stripe::setApiKey($stripe['stripe_secret_key']);
        }
        
    }
    
    public function create_customer($data){
        $customer_id = '';
        $customer = array(
          'email' => $data['email'],
          'source' => $data['token']
        );
        $stripe_customer = \Stripe\Customer::create(array('email' => $data['email'], 'source' => $data['token']));
        $customer_id = $stripe_customer->id;
        
        return $customer_id;
    }
    
    public function create_card($data){
        $customer_id = $data['customer_id'];
        
        $stripe_customer = \Stripe\Customer::retrieve($customer_id);
        $card_response = $stripe_customer->sources->create(array("source" => $data['token']));
        
        return $card_response->id;
    }
    
    public function create_charges($data){
        $message = '';
        try{
            $charge = \Stripe\Charge::create(array('amount' => $data['amount'], 'currency' => $data['currency'], 'source' => $data['token'], 'description' => $data['description']));
            $message = $charge;
        }catch(Exception $e){
            $message = $e->getMessage();
        }
        return $message;
    }
    
    public function create_customer_charges($data){
        $message = '';
        try{
            if($data['source'] == ''){
                $charge = \Stripe\Charge::create(array('amount' => $data['amount'], 'currency' => $data['currency'], 'customer' => $data['customer'],  'description' => $data['description']));
            }else{
                $charge = \Stripe\Charge::create(array('amount' => $data['amount'], 'currency' => $data['currency'], 'customer' => $data['customer'], 'source' => $data['source'], 'description' => $data['description']));
            }
            $message = $charge;
        }catch(Exception $e){
            $message = $e->getMessage();
        }
        return $message;
    }
    
    public function list_all_cards($data){
        $customer_id = $data['customer_id'];
        
        $stripe_customer = \Stripe\Customer::retrieve($customer_id);
        $card_response = $stripe_customer->sources->all(array('object' => 'card'));
        
        return $card_response->data;
    }
    
    public function delete_card($data){
        $customer_id = $data['customer_id'];
        $card_id = $data['card_id'];
        
        $stripe_customer = \Stripe\Customer::retrieve($customer_id);
        $card_response = $stripe_customer->sources->retrieve($card_id)->delete();
        
        return $card_response->deleted;
    }
}