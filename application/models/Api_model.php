<?php
class Api_model extends CI_Model{
    public function log($function, $request, $responce) {
		$log_data = array(
			'function_name' => $function,
			'request' => $request,
			'responce' => $responce,
			'time' => date("d-m-Y H:i:s"),
		);
		//$this->mongo_db->insert('log_data_new', $log_data);
    }
    public function login($jsondata){
        $data = array();
        $username = $jsondata->username;
        $password = $jsondata->password;
        $isp_uid = $jsondata->isp_uid;
        $password_new = md5($password);
        //$query = $this->db->query("select isp_uid,id,uid,username from sht_users WHERE isp_uid = '$isp_uid' AND expired = '0' and enableuser = '1' and password = '$password_new' AND (uid = '$username' OR username = '$username' OR mobile = '$username' OR email = '$username')");
	$query = $this->db->query("select isp_uid,id,uid,username from sht_users WHERE expired = '0' and enableuser = '1' and password = '$password_new' AND (uid = '$username' OR username = '$username' OR mobile = '$username' OR email = '$username')");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['resultCode'] = '1';
            $data['id'] = $row['id'];
            $data['user_uid'] = $row['uid'];
	    $data['isp_uid'] = $row['isp_uid'];
            $data['username'] = $row['username'];
            $data['resultMsg'] = 'Success';
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid username or password';
        }
        return $data;
    }
    
    public function isp_detail($jsondata){
        $data = array();
        $isp_id = $jsondata->isp_uid;
        $query = $this->db->query("select isp_name,logo_image, help_number1,help_number2,help_number3,support_email from sht_isp_detail where  isp_uid = '$isp_id'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $data['isp_name'] = $row['isp_name'];
	    if($row['logo_image'] != ''){
                $data['logo_image'] = CLOUD_IMAGEPATH."isp_logo/logo/".$row['logo_image'];
            }else{
                $data['logo_image'] = '';
            }
            $data['help_number1'] = $row['help_number1'];
            $data['help_number2'] = $row['help_number2'];
            $data['help_number3'] = $row['help_number3'];
            $data['support_email'] = $row['support_email'];
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid ISP ID';
            $data['isp_name'] = '';
            $data['logo_image'] = '';
            $data['help_number1'] = '';
            $data['help_number2'] = '';
            $data['help_number3'] = '';
            $data['support_email'] = '';
        }
        return $data;
    }
    
    public function slider_data($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select schsp.*, ss.srvname,ss.plantype from sht_consumer_home_slider_promotion as schsp inner join sht_services as ss on (schsp.plan_id = ss.srvid) where schsp.isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $i = 0;
            $slider_data = array();
            foreach($query->result() as $row){
                $slider_data[$i]['service_id'] = $row->plan_id;
                $slider_data[$i]['service_name'] = $row->srvname;
                $slider_data[$i]['description'] = $row->description;
		if($row->image_path != ''){
		    $slider_data[$i]['image_path'] = CLOUD_IMAGEPATH."promotion_image/slider_promotion/".$row->image_path;
		}else{
		    $slider_data[$i]['image_path'] = '';
		}
                
                if($row->plantype == '0'){
                    // topup
                    $plan_id = $row->plan_id;
                    $price_query = $this->db->query("select net_total from sht_topup_pricing where srvid = '$plan_id'");
                    $row_price = $price_query->row_array();
                    $slider_data[$i]['service_price'] = "Rs ".$row_price['net_total'];
		    $slider_data[$i]['service_curency'] = "Rupee";
                }else{
                    // plan
                    $plan_id = $row->plan_id;
                    $price_query = $this->db->query("select net_total from sht_plan_pricing where srvid = '$plan_id'");
                    $row_price = $price_query->row_array();
                    $slider_data[$i]['service_price'] = "Rs ".$row_price['net_total']." / month";
		    $slider_data[$i]['service_curency'] = "Rupee";
                }
                $i++;
            }
            
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $data['slider_data'] = $slider_data;
           
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid ISP ID OR No Data found';
        }
        return $data;
    }

    public function live_usage($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        $live_usage =0;
        $total_data = 0;
        $topupdata = 0;
        $monthly_data = 0;
        $speed = '';
        $due_amount = 0;
        $due_amout_expiration = '';
        $plan_type = '';
        // get topup limit
        $get_topup = $this->db->query("select ss.plantype, ss.datalimit from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
        if($get_topup->num_rows() > 0){
            foreach($get_topup->result() as $tobj){
                $topupdata += $tobj->datalimit;
            }
        }
         // get user plan limit
        $get_plan = $this->db->query("select su.expiration, ss.plantype, ss.datalimit, ss.datacalc,ss.downrate, ss.fupdownrate from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid'");
        $data_allocated = 0;
        $plan_type = '';
        if($get_plan->num_rows() > 0){
            $get_plan_row = $get_plan->row_array();
            $plan_type = $get_plan_row['plantype'];
            $due_amout_expiration = date("d-m-Y", strtotime($get_plan_row['expiration']));
            //get speed
            $download_speed = $this->convertTodata($get_plan_row['downrate']."MB");
            if($download_speed < 1){
                $download_speed = $this->convertTodata($get_plan_row['downrate']."KB");
                $speed = round($download_speed,0)." kbps"; 
            
            }else{
               $speed = round($download_speed,0)." mbps"; 
            }
            if($get_plan_row['plantype'] == '1'){
                $plan_msg = "Unlimited plan";
                $total_data = $plan_msg;
                $monthly_data = "";
                $topupdata = "";
            }elseif($get_plan_row['plantype'] == '3'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                //$total_data = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB";
                $total_data = $data_allocated;
                $monthly_data = "Monthly Data: ".$monthly_data;
                $topupdata = "Topup Data: ".$topupdata;
                
                // for after fup speed
                 $unlimited_speed = $this->convertTodata($get_plan_row['fupdownrate']."MB");
                if($unlimited_speed < 1){
                    $unlimited_speed = $this->convertTodata($get_plan_row['fupdownrate']."KB");
                    $speed = $speed."/".round($unlimited_speed,0)." kbps (Unlimited)"; 
                
                }else{
                   $speed = $speed."/".round($unlimited_speed,0)." mbps (Unlimited)"; 
                }
                
            }elseif($get_plan_row['plantype'] == '4'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
               // $total_data = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB";
                $total_data = $data_allocated;
                $monthly_data = "Monthly Data: ".$monthly_data;
                $topupdata = "Topup Data: ".$topupdata;
            }
        }
        // get billing date
        $get_billing_date = $this->db->query("select billing_cycle from sht_billing_cycle where isp_uid = '$isp_uid'");
        $start_date = ''; $end_date = '';
        if($get_billing_date->num_rows() > 0){
            $row_billing_date = $get_billing_date->row_array();
            if(date("j") >= $row_billing_date['billing_cycle']){
                //current month to next month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                $start_date =  $date->format('Y-m-d');
                $end_date = date('Y-m-d', strtotime('+1 month -1 day', strtotime($start_date)));
            }else{
                //previous month to current month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                $end_date =  date('Y-m-d', strtotime('-1 day', strtotime($date->format('Y-m-d'))));
                $start_date = date('Y-m-d', strtotime('-1 month +1 day', strtotime($end_date)));
            }
        }
      // get user device macid
        $user_macid = array();
        $get_macid = $this->db->query("select hotspotMac from sht_user_hotspot_assoc where uid = '$useruid'");
        if($get_macid->num_rows() > 0){
            foreach($get_macid->result() as $get_macid_row){
                $user_macid[] = $get_macid_row->hotspotMac;        
            }
        }
        $user_macid = '"'.implode('", "', $user_macid).'"';
       
        // get monthly usage
        $query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where (username = '$useruid' OR username IN ($user_macid)) AND DATE(acctstarttime) between '$start_date' and '$end_date'");
        
        foreach($query->result() as $row){
            $datacalc = $get_plan_row['datacalc'];
           if($datacalc == 2){
            $live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
           }else{
            $live_usage = $live_usage + $row->acctoutputoctets;
           }
        }
        /*$live_usage_value = round($live_usage/(1024*1024*1024),2);
        if($live_usage_value < 1){
            //get in mb
            $live_usage_value = round($live_usage/(1024*1024),2);
            if($live_usage_value < 1){
                //get in kb
                $live_usage_value = round($live_usage/(1024),2)."KB";
            }else{
               $live_usage_value = $live_usage_value."MB" ;
            }
        }else{
            $live_usage_value = $live_usage_value."GB";
        }*/
        $live_usage_value = $live_usage;
        $data['live_usage'] = $live_usage_value;
        $data['total_data'] = $total_data;
        $data['monthly_data'] = $monthly_data;
        $data['topup_data'] = $topupdata;
        $data['current_speed'] = $speed;
    
        $get_due_amount =  $this->userbalance_amount($useruid);
        if($get_due_amount < 0){
            $due_amount = abs($get_due_amount);
        }else{
            $due_amout_expiration = '';
        }
        $data['due_amount'] = $due_amount;
        $data['due_amount_expiration'] = $due_amout_expiration;
        $data['plan_type'] = $plan_type;
        $data['data_allocated'] = $data_allocated;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
            $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
            $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;

        return $balanceamt;
    }
    public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number /(1024*1024);
            case "GB":
                return $number / (1024*1024*1024);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }

    public function promotions($jsondata){
        $data = array();
        $promotions = array();
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select * from sht_consumer_marketing_promotion where start_date <= now() AND end_date >= now() and is_deleted = '0' and status = '1' AND isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $i = 0;
            foreach($query->result() as $row){
                $promotions[$i]['image_path'] = IMAGEPATH.'promotion_image/marketing_promotion/'.$row->image_path;
                $promotions[$i]['title'] = $row->title;
                $promotions[$i]['description'] = $row->description;
                $promotions[$i]['promotion_offer'] = $row->promotion_offer;
                $promotions[$i]['redirect_link'] = $row->redirect_link;
               $i++; 
            }
            
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'No promotion live';
        }
        $data['promotions'] = $promotions;
        //echo "<pre>";print_r($data);die;
        return $data;
     }

    
    public function account_detail($jsondata){
        $data = array();
        $userid = $jsondata->user_uid;
        $query = $this->db->query("select su.*, sc.city_name, sz.zone_name from sht_users as su left join sht_cities as sc on (su.city = sc.city_id)
                                  left join sht_zones as sz on (su.zone = sz.id) where su.uid = '$userid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['name'] = $row['firstname']." ".$row['middlename'].' '.$row['lastname'];
            $data['uid'] = $row['uid'];
            $data['username'] = $row['username'];
            $data['email'] = $row['email'];
            $data['mobile'] = $row['mobile'];
            $data['address'] = $row['flat_number'].', '. $row['address'];
            $data['city'] = $row['city_name'];
            $data['zone'] = $row['zone_name'];
	    $data['geoaddress'] = $row['geoaddress'];
	    $data['placeid'] = $row['place_id'];
	    $data['lat'] = $row['lat'];
	    $data['long'] = $row['longitude'];
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    private  $baseUrl = "http://sendotp.msg91.com/api";
    public function update_mobile_send_otp($jsondata){
        $data_responce = array();
        $phone = $jsondata->old_number;
        $data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
        $data_string = json_encode($data);
        $ch = curl_init($this->baseUrl.'/generateOTP');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json',
     'Content-Length: ' . strlen($data_string),
     'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        $otp_gets = '';
        $response = json_decode($result,true);
        if($response["status"] == "error"){
           
        }else{
            $otp_gets =  $response["response"]["oneTimePassword"];
        }
        
        $useruid = $jsondata->user_uid;
        $this->db->query("insert into sht_otp_verification (uid, otp, is_used) values('$useruid', '$otp_gets', '0')");
        $data_responce['otp'] = $otp_gets;
        return $data_responce;
    }
    
    public function update_mobile_verify_otp($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $otp = $jsondata->otp;
        $mobile = $jsondata->new_number;
        $otp_veri = $this->db->query("select * from sht_otp_verification where uid = '$useruid' and otp = '$otp' and is_used = '0' order by id desc limit 1");
        if($otp_veri->num_rows() > 0){
            $row = $otp_veri->row_array();
            $id = $row['id'];
            $this->db->query("update sht_otp_verification set is_used = '1' where id = '$id'");
            // update mobile
            $this->db->query("update sht_users set mobile = '$mobile' where uid = '$useruid'");
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'SUccessfully Updated phone number';
        }else{
           $data['resultCode'] = '0';
           $data['resultMsg'] = 'Invalid OTP';
        }
        return $data;
    }

    public function usage_logs($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $month = explode(" ", $jsondata->month);
        $current_month = $month[0] ;
        $current_year = $month[1];
        $logs = array();
        $query = $this->db->query("select * from sht_user_daily_data_usage where uid = '$useruid' AND MONTH(start_time) = '$current_month' AND YEAR(start_time) = '$current_year' order by id desc");
    
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
            $i = 0;
            foreach($query->result() as $row){
                $logs[$i]['start_time'] = $row->start_time;
                $logs[$i]['end_time'] = $row->end_time;
                $logs[$i]['upload'] = round($row->upload/(1024*1024),2);
                $logs[$i]['download'] = round($row->download/(1024*1024),2);
                $i++;
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "No logs found";
        }
        $data['usage_logs'] = $logs;
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    public function my_plan($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = '';
        if(isset($jsondata->isp_uid)){
            $isp_uid = $jsondata->isp_uid;
        }
        
        $query = $this->db->query("select ss.plantype,su.uid,ss.srvname,ss.plantype,ss.downrate, ss.fupdownrate,ss.datalimit, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "success";
            
            $row = $query->row_array();
            $data['planname'] = $row['srvname'];
            $data['rental'] = $row['net_total'];
            $speed = '';
            $download_speed = $this->convertTodata($row['downrate']."MB");
            if($download_speed < 1){
                $download_speed = $this->convertTodata($row['downrate']."KB");
                $speed = round($download_speed,0)." kbps"; 
            
            }else{
               $speed = round($download_speed,0)." mbps"; 
            }
            if($row['plantype'] == 3){
               $unlimited_speed = $this->convertTodata($row['fupdownrate']."MB");
                if($unlimited_speed < 1){
                    $unlimited_speed = $this->convertTodata($row['fupdownrate']."KB");
                    $speed = $speed."(FUP) /".round($unlimited_speed,0)." kbps(unlimited)"; 
                
                }else{
                   $speed = $speed."(FUP) / ".round($unlimited_speed,0)." mbps(Unlimited)"; 
                }
                
                $fup_limit = $this->convertTodata($row['datalimit']."GB");
                $data['fup_limit'] = round($fup_limit,2)." GB". " / month";
            }else{
                $data['fup_limit'] = 'None';
            }
            $data['speed'] = $speed;
             //get num of topup assign to user
            $uid = $row['uid'];
            $get_otp_query = $this->db->query("select * from sht_usertopupassoc where uid = '$uid' and terminate = '0' and topuptype != '1' and topup_enddate >= CURDATE()");
            $data['topup'] = $get_otp_query->num_rows();
            //get user next scheduled plan
            $useruid = $uid;
            $next_scheduled_plan = '';
            $next_scheduled_plan_date = '';
            $get_scheduled_plan = $this->db->query("select snu.baseplanid, ss.srvname,ss.plantype from sht_nextcycle_userplanassoc as snu inner join  sht_services as ss on (snu.baseplanid = ss.srvid)  where snu.uid = '$useruid' and snu.status = '0'");
            if($get_scheduled_plan->num_rows() > 0){
                $row_scheduled = $get_scheduled_plan->row_array();
                $next_scheduled_plan = $row_scheduled['srvname'];
                $user_current_plan_type=  $row['plantype'];
                $get_billing_date = $this->db->query("select billing_cycle from sht_billing_cycle where isp_uid = '$isp_uid' ");
                $row_billing_date = $get_billing_date->row_array();
                if(date("j") > $row_billing_date['billing_cycle']){
                    // date will be next month
                    $date = date('Y-m-d', strtotime('+1 month'));
                    $date = new DateTime($date);
                    $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                    $next_scheduled_plan_date =  $date->format('d-m-Y'); 
                    
                }else{
                    //date will be same month
                    $date = date('Y-m-d');
                    $date = new DateTime($date);
                    $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                     $next_scheduled_plan_date =  $date->format('d-m-Y');
                   
                }
                
            }
            $data['next_scheduled_plan'] = $next_scheduled_plan;
            $data['next_scheduled_plan_date'] =  $next_scheduled_plan_date;
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

     public function recommended_plans($jsondata){
        $data = array();
        $plans = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select su.zone, su.city, su.state,su.baseplanid, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_active = $row['baseplanid'];
            $user_current_plan_price = $row['net_total'];
            //get all live plan
            $get_plan = $this->db->query("select ss.*, spp.region_type, spp.net_total from sht_services as ss  left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where ss.plantype != '0' and ss.plantype != '2' and ss.is_private = '0' and ss.srvid != '$user_current_plan_active' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' AND ss.isp_uid = '$isp_uid'");
            $i = 0;
            if($get_plan->num_rows() > 0){
                
                foreach($get_plan->result() as $get_plan1){
                    // first check plan price is equal or grater
                    if($get_plan1->net_total >= $user_current_plan_price){
                        // if region_type  = allindia in sht_plan_pricing then show this plan dont check in plan region table
                        if($get_plan1->region_type == 'allindia'){
                            $plans[$i]['service_id'] = $get_plan1->srvid;
                            $plans[$i]['plan_name'] = $get_plan1->srvname;
                            if($get_plan1->plantype == 1){
                                $plan_type = "Unlimited Plan";
                                $plans[$i]['total_data'] = "";
                            }elseif($get_plan1->plantype == 2){
                                $plan_type = 'Time Plan';
                                $plans[$i]['total_data'] = " ";
                            }
                            elseif($get_plan1->plantype == 3){
                                $plan_type = 'FUP Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $plans[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                            }
                            elseif($get_plan1->plantype == 4){
                                $plan_type = 'Data Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $plans[$i]['total_data'] = round($data_limit,2)." GB";
                            }
                            $plans[$i]['plan_type'] = $plan_type;
                            $plans[$i]['plan_cost'] = ceil($get_plan1->net_total);
                            $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                $plans[$i]['speed'] = ceil($download_speed)." kbps";
                            }else{
                                $plans[$i]['speed'] = ceil($download_speed)." mbps"; 
                            }
                            $i++;
                        }
                        else{
                            // get region is same as user from sht_plan_region table
                            $service_id = $get_plan1->srvid;
                            //$zone_ids = array();
                            $is_valid_offer = 0;
                            $get_plan_region = $this->db->query("select zone_id, city_id,state_id from sht_plan_region where plan_id = '$service_id'");
                            foreach($get_plan_region->result() as $get_plan_region1){
                                //$zone_ids[] = $get_plan_region1->zone_id;
                                //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_offer = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_offer = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_offer = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $is_fup_plan = '';
                             // now check region is same as user region or all
                             if($is_valid_offer == 1){
                                $plans[$i]['service_id'] = $get_plan1->srvid;
                                $plans[$i]['plan_name'] = $get_plan1->srvname;
                                $plan_type = '';
                                if($get_plan1->plantype == 1){
                                    $plan_type = "Unlimited Plan";
                                    $plans[$i]['total_data'] = "&nbsp;";
                                }elseif($get_plan1->plantype == 2){
                                    $plan_type = 'Time Plan';
                                    
                                    $plans[$i]['total_data'] = "&nbsp;";
                                }
                                elseif($get_plan1->plantype == 3){
                                    $plan_type = 'FUP Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                    $plans[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                                }
                                elseif($get_plan1->plantype == 4){
                                    $plan_type = 'Data Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                   $plans[$i]['total_data'] = round($data_limit,2)." GB";
                                }
                                $plans[$i]['plan_type'] = $plan_type;
                                $plans[$i]['plan_cost'] = ceil($get_plan1->net_total);
                                $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                    $plans[$i]['speed'] = ceil($download_speed)." <small>kbps</small>";
                                }else{
                                    $plans[$i]['speed'] = ceil($download_speed)." <small>mbps</small>"; 
                                }
                           
                                
                                    $i++;
                    
                            }
                        }
                    }
                }
                if(count($plans) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['recommended_plans'] = $plans;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Suggested Plan";
                }
                
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Suggested Plan";
            }
            
            
           
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    public function recommended_plans_app($jsondata){
        $data = array();
        $plans = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select su.zone, su.city, su.state,su.baseplanid, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_active = $row['baseplanid'];
            $user_current_plan_price = $row['net_total'];
            //get all live plan
            $get_plan = $this->db->query("select ss.*, spp.region_type, spp.net_total from sht_services as ss  left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where ss.plantype != '0' and ss.plantype != '2' and ss.is_private = '0' and ss.srvid != '$user_current_plan_active' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' AND ss.isp_uid = '$isp_uid'");
            $i = 0;
            if($get_plan->num_rows() > 0){
                
                foreach($get_plan->result() as $get_plan1){
                    // first check plan price is equal or grater
                    if($get_plan1->net_total >= $user_current_plan_price){
                        // if region_type  = allindia in sht_plan_pricing then show this plan dont check in plan region table
                        if($get_plan1->region_type == 'allindia'){
                            $plans[$i]['service_id'] = $get_plan1->srvid;
                            $plans[$i]['plan_name'] = $get_plan1->srvname;
                            if($get_plan1->plantype == 1){
                                $plan_type = "Unlimited Plan";
                                $plans[$i]['total_data'] = "&nbsp;";
                            }elseif($get_plan1->plantype == 2){
                                $plan_type = 'Time Plan';
                                $plans[$i]['total_data'] = " ";
                            }
                            elseif($get_plan1->plantype == 3){
                                $plan_type = 'FUP Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $plans[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                            }
                            elseif($get_plan1->plantype == 4){
                                $plan_type = 'Data Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $plans[$i]['total_data'] = round($data_limit,2)." GB";
                            }
                            $plans[$i]['plan_type'] = $plan_type;
                            $plans[$i]['plan_cost'] = ceil($get_plan1->net_total);
                            $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                $plans[$i]['speed'] = ceil($download_speed)." kbps";
                            }else{
                                $plans[$i]['speed'] = ceil($download_speed)." mbps"; 
                            }
                            $i++;
                        }
                        else{
                            // get region is same as user from sht_plan_region table
                            $service_id = $get_plan1->srvid;
                            //$zone_ids = array();
                            $is_valid_offer = 0;
                            $get_plan_region = $this->db->query("select zone_id, city_id,state_id from sht_plan_region where plan_id = '$service_id'");
                            foreach($get_plan_region->result() as $get_plan_region1){
                                //$zone_ids[] = $get_plan_region1->zone_id;
                                //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_offer = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_offer = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_offer = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $is_fup_plan = '';
                             // now check region is same as user region or all
                             if($is_valid_offer == 1){
                                $plans[$i]['service_id'] = $get_plan1->srvid;
                                $plans[$i]['plan_name'] = $get_plan1->srvname;
                                $plan_type = '';
                                if($get_plan1->plantype == 1){
                                    $plan_type = "Unlimited Plan";
                                    $plans[$i]['total_data'] = "&nbsp;";
                                }elseif($get_plan1->plantype == 2){
                                    $plan_type = 'Time Plan';
                                    
                                    $plans[$i]['total_data'] = "&nbsp;";
                                }
                                elseif($get_plan1->plantype == 3){
                                    $plan_type = 'FUP Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                    $plans[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                                }
                                elseif($get_plan1->plantype == 4){
                                    $plan_type = 'Data Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                   $plans[$i]['total_data'] = round($data_limit,2)." GB";
                                }
                                $plans[$i]['plan_type'] = $plan_type;
                                $plans[$i]['plan_cost'] = ceil($get_plan1->net_total);
                                $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                    $plans[$i]['speed'] = ceil($download_speed);
                                }else{
                                    $plans[$i]['speed'] = ceil($download_speed); 
                                }
                           
                                
                                    $i++;
                    
                            }
                        }
                    }
                }
                if(count($plans) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['recommended_plans'] = $plans;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Suggested Plan";
                }
                
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Suggested Plan";
            }
            
            
           
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }


    public function active_topups($jsondata){
        $data = array();
        $topup = array();
        $user_uid = $jsondata->user_uid;
        $get_otp_query = $this->db->query("select su.*,ss.srvname, ss.topuptype from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$user_uid' and su.terminate = '0' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' and su.topuptype != '1'  and su.topup_enddate >= CURDATE()");
        if($get_otp_query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $i = 0;
            foreach($get_otp_query->result() as $row){
                $topup[$i]['topupname'] = $row->srvname;
                if($row->topuptype == '2'){
                    $topuptype = "Dataunacnttopup";
                    $topup[$i]['topupstartdate'] = date("d-m-Y",strtotime($row->topup_startdate));
                    $topup[$i]['topupenddate'] = date("d-m-Y",strtotime($row->topup_enddate));
                }else{
                    $topuptype = "SpeedTopup";
                    $topup[$i]['topupstartdate'] = date("d-m-Y",strtotime($row->topup_startdate));
                    $topup[$i]['topupenddate'] = date("d-m-Y",strtotime($row->topup_enddate));
                }
            
                $topup[$i]['topuptype'] = $topuptype;
                $i++;
            }
            $data['active_topup'] = $topup;
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'No topup apply';
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    public function recommended_speed_topup($jsondata){
        $data = array();
        $speed_topup = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.is_private = '0' and ss.topuptype ='3' and stp.payment_type = 'Paid' AND ss.isp_uid = '$isp_uid'");
            if($get_topup->num_rows() > 0){
                $k = 0;
                foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                        $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                        $speed_topup[$k]['topup_id'] = $get_topup1->srvid;
                        $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                        if($download_speed < 1){
                            $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                            $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                        }else{
                            $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                        }
                        //pricing
                        $speed_topup_price = 0;
                        $start_time = '';
                        $end_time = '';
                        $days = array();
                        $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                        foreach($get_speed_pricing->result() as $get_speed_pricing1){
                            $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                            $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                            $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                            $days[] = $get_speed_pricing1->days;
                        }
                        $speed_topup[$k]['topup_price'] = $speed_topup_price;
                        if(count($days) == '7'){
                            $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                        }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                            $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                        }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                            $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                        }else{
                            $days_new = array();
                            foreach($days as $days1){
                                $days_new[] = substr(trim($days1), 0, 3);
                            }
                            $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                        }
                           $k++;
                    }
                    else{
                         $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        if($is_valid_topup == 1){
                            $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                                 $speed_topup[$k]['topup_id'] = $get_topup1->srvid;
                                $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                                    $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                                
                                }else{
                                   $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                                }
                                //pricing
                                $speed_topup_price = 0;
                            
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                    $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                                    $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                    $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                    $days[] = $get_speed_pricing1->days;
                                }
                                $speed_topup[$k]['topup_price'] = $speed_topup_price;
                                if(count($days) == '7'){
                                   $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                    $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                    $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                            
                                $k++;
                        }
                        
                    }
                }
                if(count($speed_topup) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['speed_topup'] = $speed_topup;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Recommended Speed Topup";
                }
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Recommended Speed Topup";
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>"; print_r($data);die;
        return $data;
    }

    
    public function recommended_unaccoundancy_topup($jsondata){
        $data = array();
        $data_unacc_topup = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.is_private = '0' and ss.topuptype ='2' and stp.payment_type = 'Paid' AND ss.isp_uid = '$isp_uid'");
            if($get_topup->num_rows() > 0){
                $j = 0;
                 foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                        $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                            $data_unacc_topup[$j]['topup_id'] = $get_topup1->srvid;
                            //pricing
                            $unaccount_topup_price = 0;
                            $unaccount_topup_percent = 0;
                            $start_time = '';
                            $end_time = '';
                            $days = array();
                            $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                            foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                $days[] = $get_unaccount_pricing->days;
                            }
                            $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                            $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                            if(count($days) == '7'){
                               $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                            $j++;
                    }
                    else{
                         $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        if($is_valid_topup == 1){
                            $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                               $data_unacc_topup[$j]['topup_id'] = $get_topup1->srvid;
                                //pricing
                                $unaccount_topup_price = 0;
                                $unaccount_topup_percent = 0;
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                                foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                    $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                    $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                    $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                    $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                    $days[] = $get_unaccount_pricing->days;
                                }
                                $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                                $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                                if(count($days) == '7'){
                                   $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                                $j++;
                        }
                        
                    }
                }
                if(count($data_unacc_topup) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['unaccoundancy_topup'] = $data_unacc_topup;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Recommended Unaccoundancy Topup";
                }
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Recommended Unaccoundancy Topup";
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>"; print_r($data);die;
        return $data;
    }

    public function recommended_data_topup($jsondata){
        $data = array();
        $data_topup = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.is_private = '0' and ss.topuptype ='1' and stp.payment_type = 'Paid' AND isp_uid = '$isp_uid'");
            if($get_topup->num_rows() > 0){
                $i = 0;
                 foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                         $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                            $data_topup[$i]['topup_id'] = $get_topup1->srvid;
                            $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                            $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                            $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                            $i++;
                    }
                    else{
                         $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        if($is_valid_topup == 1){
                            $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                                $data_topup[$i]['topup_id'] = $get_topup1->srvid;
                                $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                                $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                                $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                                $i++;
                        }
                        
                    }
                }
                if(count($data_topup) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['data_topup'] = $data_topup;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Recommended Data Topup";
                }
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Recommended Data Topup";
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>"; print_r($data);die;
        return $data;
    }
    public function pending_bills($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $uuid = $useruid;
            $wallet_amt = 0;
            $wallet_amt = 0;
	    $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
	    if($walletQ->num_rows() > 0){
		  $wallet_amt = $walletQ->row()->wallet_amt;
	    }
	    $passbook_amt = 0;
	    $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
	    if($passbookQ->num_rows() > 0){
		  $passbook_amt = $passbookQ->row()->plan_cost;
	    }
	    $balanceamt = ($wallet_amt - $passbook_amt);
            /*if($balanceamt < 0){
                  $data['pending_amount'] = abs($balanceamt);
            }*/
	    $payableamount = 0;
	    $totalbillamt = 0; $totalrcptamt = 0; $totalbalamt = 0;
	    $custbillQ = $this->db->query("SELECT COALESCE(SUM(total_amount),0) as total_amount FROM sht_subscriber_custom_billing WHERE uid='".$uuid."' AND is_deleted='0'");
	    if($custbillQ->num_rows() > 0){
		  $totalbillamt = $custbillQ->row()->total_amount;
	    }
	    $recptQ = $this->db->query("SELECT COALESCE(SUM(receipt_amount),0) as receipt_amount FROM sht_custom_receipt_list WHERE uid='".$uuid."' ORDER BY id DESC");
	    if($recptQ->num_rows() > 0){
		  $totalrcptamt = $recptQ->row()->receipt_amount;
	    }
	    $totalbalamt = ($totalrcptamt - $totalbillamt);
	    
	    if($totalbalamt < 0){
		  $payableamount += $totalbalamt;
	    }
	    if($balanceamt < 0){
		  $payableamount += $balanceamt;
	    }
            $data['pending_amount'] = abs($payableamount);
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    public function planname($srvid){
	    $planname = '-';
	    $query = $this->db->query("SELECT tb2.srvname FROM sht_services as tb2 WHERE tb2.srvid='".$srvid."'");
	    if($query->num_rows() > 0){
		  $planname = $query->row()->srvname;
	    }
	    return $planname;
      }

    public function previous_bills($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
	$year_filter = date('Y');
        $bills = array();
        $query = $this->db->query("select * from sht_subscriber_billing where subscriber_uuid = '$useruid' and (bill_type = 'midchange_plancost' OR bill_type = 'montly_pay' OR bill_type = 'topup') AND YEAR(bill_added_on) = '$year_filter'  order by id desc");
	$i = 0;
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
		$bills[$i]['bill_added_on'] = $row->bill_added_on;
                $bills[$i]['generated_on'] = date("d.m.Y",strtotime($row->bill_added_on));
                $bills[$i]['paid_on'] = ($row->bill_paid_on != '0000-00-00 00:00:00')?date("d.m.Y",strtotime($row->bill_paid_on)):"-";
		$bills[$i]['service_name'] = $this->planname($row->plan_id);
                $bills[$i]['payment_type'] = $row->bill_type;
                $bills[$i]['bill_number'] = ($row->bill_number != '')?$row->bill_number:"-";
                $bills[$i]['receipt_number'] = ($row->receipt_number != '')?$row->receipt_number:"-";
                $bills[$i]['transaction_id'] = ($row->transection_id!=0)?$row->transection_id:"-";
                $bills[$i]['payment_amount'] = $row->total_amount;
                $bills[$i]['payment_mode'] = $row->payment_mode;
                $bills[$i]['bill_status'] = ($row->receipt_received == '1')?'Paid':"Pending";
                $bills[$i]['bill_payed'] = ($row->receipt_received == '1')?'1':"0";
                $i++;
              
            }
            
        }
	$custbillQ = $this->db->query("SELECT * FROM sht_subscriber_custom_billing WHERE uid='".$useruid."' AND is_deleted='0'  AND YEAR(added_on) = '$year_filter'  ORDER BY id DESC");
	if($custbillQ->num_rows() > 0){
	      foreach($custbillQ->result() as $row){
		    $bills[$i]['bill_added_on'] = $row->added_on;
		    $bills[$i]['generated_on'] = date("d.m.Y",strtotime($row->added_on));
		    $bills[$i]['paid_on'] = ($row->bill_paid_on != '0000-00-00 00:00:00')?date("d.m.Y",strtotime($row->bill_paid_on)):"-";
		    $bills[$i]['service_name'] = $row->bill_details;
		    $bills[$i]['payment_type'] = 'custom_invoice';
		    $bills[$i]['bill_number'] = ($row->bill_number != '')?$row->bill_number:"-";
		    $bills[$i]['receipt_number'] = ($row->receipt_number != '')?$row->receipt_number:"-";
		    $bills[$i]['transaction_id'] = "-";
		    $bills[$i]['payment_amount'] = $row->total_amount;
		    $bills[$i]['payment_mode'] = $row->payment_mode;
		    $bills[$i]['bill_status'] = ($row->receipt_received == '1')?'Paid':"Pending";
		    $bills[$i]['bill_payed'] = ($row->receipt_received == '1')?'1':"0";
		    $i++;
	      }
	}
	if(count($bills) > 0){
	    usort($bills, function($a, $b) {
		  return strtotime($b['bill_added_on']) - strtotime($a['bill_added_on']);
	    });
	    $data['bills'] = $bills;
	    $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
	}else{
	    $data['resultCode'] = '0';
            $data['resultMsg'] = "No previous bill";
	}
	
        
            
        
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    


    public function current_service_request($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $current_request = array();
        $query = $this->db->query("select * from sht_subscriber_tickets where subscriber_uuid = '$useruid' and status = '0' order by id desc");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
             $i = 0;
            foreach($query->result() as $row){
                $current_request[$i]['reqiested_on'] = date("d.m.Y",strtotime($row->added_on));
                $current_request[$i]['ticket_type'] = ucfirst(str_replace('_', ' ', $row->ticket_type));
                $current_request[$i]['ticket_desc'] = $row->ticket_description;
                if($row->ticket_assign_to != '0'){
                    $current_request[$i]['status'] = "In Progress ";
                }else{
                  $current_request[$i]['status'] = "Open";  
                }
                $current_request[$i]['ticket_id'] = $row->ticket_id;
                
                $i++;
            }
            $data['current_request'] = $current_request;
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "No prending request";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    public function service_type($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $data = array();
        $i = 0;
	$abc = array();
        $get = $this->db->query("select * from sht_tickets_type where isp_uid = '$isp_uid'");
        foreach($get->result() as $row){
            $abc[$i]['ticket_type_value'] = $row->ticket_type_value;
            $abc[$i]['ticket_type'] = $row->ticket_type;
            $i++;
        }
	$data['abc'] = $abc;
	//echo "<pre>";print_r($data);die;
        return $data;
    }
    
    public function generate_service_request($jsondata){
        $data = array();
	$isp_uid = 0;
	if(isset($jsondata->isp_uid)){
	    $isp_uid = $jsondata->isp_uid;  
	}
	
        $subscriber_uuid = $jsondata->user_uid;
        $ticket_type = $jsondata->ticket_type;
        $ticket_priority = $jsondata->ticket_priority;
        $ticket_description = $jsondata->ticket_description;
        $ticketid = date("j").date('n').date("y").date('His');
        if($ticket_type != '' && $ticket_priority != '' && $ticket_description != ''){
            $data['resuldCode'] = '1';
            $data['resultMsg'] = "Success";
            //get sescriberid
            $subscriber_id = '';
            $get_id = $this->db->query("select id from sht_users where uid = '$subscriber_uuid'");
            if($get_id->num_rows() > 0){
                $row = $get_id->row_array();
                $subscriber_id = $row['id'];
            }
            $insert = $this->db->query("insert into sht_subscriber_tickets(isp_uid,ticket_id, subscriber_id, subscriber_uuid, ticket_type, ticket_priority,ticket_raise_by,ticket_description,status,added_on) values('$isp_uid','$ticketid', '$subscriber_id', '$subscriber_uuid', '$ticket_type', '$ticket_priority', '0', '$ticket_description','0', now())");
	     // send notification
            $this->ticket_activation_alert($isp_uid,$subscriber_uuid,$ticket_type,$ticketid);
        }else{
            $data['resuldCode'] = '0';
            $data['resultMsg'] = "Please Fill all data";
        }
        
        return $data;
    }
    
    public function ticket_activation_alert($isp_uid,$uuid, $tickettype, $tkt_number){
       
       $username = $this->getcustomer_fullname($uuid);
       $mobileno = $this->getcustomer_mobileno($uuid);
       $ispdetArr = $this->getispdetails($isp_uid);
       $company_name = $ispdetArr['company_name'];
       $isp_name = $ispdetArr['isp_name'];
       $help_number = $ispdetArr['help_number'];
       
       $message = urlencode('Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered.').'%0a'.urlencode('Please allow us sometime to address your request, our team will ensure fastest possible action.').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
       
       $sms_tosend = 'Dear '.$username.', your '.$tickettype.' #'.$tkt_number.' has been registered. <br/> Please allow us sometime to address your request, our team will ensure fastest possible action. <br/> For any queries please call us @ '.$help_number.' <br/>Team '.$isp_name;
       
       $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
       if($txtsmsQ->num_rows() > 0){
           foreach($txtsmsQ->result() as $txtsmsobj){
               $gupshup_user = $txtsmsobj->gupshup_user;
               $gupshup_pwd = $txtsmsobj->gupshup_pwd;
               $msg91authkey = $txtsmsobj->msg91authkey;
               $bulksmsuser = $txtsmsobj->bulksmsuser;
               $bulksms_pwd = $txtsmsobj->bulksms_pwd;
               $rightsms_user = $txtsmsobj->rightsms_user;
               $rightsms_pwd = $txtsmsobj->rightsms_pwd;
               $rightsms_sender = $txtsmsobj->rightsms_sender;
               $cloudplace_user = $txtsmsobj->cloudplace_user;
               $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
               $cloudplace_sender = $txtsmsobj->scope;
               
               $sender = $txtsmsobj->scope;
               
               if(($gupshup_user != '') && ($gupshup_pwd != '')){
                   $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif($msg91authkey != ''){
                   $this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                   $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                   $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                   $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
                   $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                   return 1;
               }else{
                   return 1;
               }
           }
       }
    }
    public function getcustomer_fullname($uuid){
        $mobileQ = $this->db->query("SELECT firstname,lastname FROM sht_users WHERE uid='".$uuid."'");
        $rdata = $mobileQ->row();
        return $rdata->firstname .' '. $rdata->lastname;
    }
    public function getcustomer_mobileno($uuid){
        $mobileQ = $this->db->query("SELECT mobile FROM sht_users WHERE uid='".$uuid."'");
        return $mobileQ->row()->mobile;
    }
    public function getispdetails($isp_uid){
        $data = array();
        $ispdetQ = $this->db->query("SELECT company_name,isp_name,help_number1 FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
        $rdata = $ispdetQ->row();
        $data['company_name'] = $rdata->company_name;
        $data['isp_name'] = $rdata->isp_name;
        $data['help_number'] = $rdata->help_number1;
        
        return $data;
    }
    public function gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message){
        $param = array();
        $request =""; //initialise the request variable
        $param['method']= "sendMessage";
        $param['send_to'] = "91"+$user_mobileno;
        $param['msg'] = $user_message;
        $param['userid'] = $gupshup_user;
        $param['password'] = $gupshup_pwd;
        $param['v'] = "1.1";
        $param['msg_type'] = "TEXT"; //Can be "FLASH�/"UNICODE_TEXT"/�BINARY�
        $param['auth_scheme'] = "PLAIN";
        
        //Have to URL encode the values
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        //echo $curl_scraped_page;
        
        return 1;
    }
    public function addmessage_todb($isp_uid, $uuid, $message){
        $this->db->insert("sht_users_notifications", array('isp_uid' => $isp_uid, 'uid' => $uuid, 'notify_type' => 'SMS', 'message' => $message, 'added_on' => date('Y-m-d H:i:s')));
        return 1;
    }
    public function msg91_txtsmsgateway($msg91authkey, $phone, $msg, $sender) {
        $postData = array(
           'authkey' => $msg91authkey, //'106103ADQeqKxOvbT856d19deb',
           'mobiles' => $phone,
           'message' => $msg,
           'sender' => $sender, //'SHOUUT',
           'route' => '4'
        );

        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
           CURLOPT_URL => $url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_POST => true,
           CURLOPT_POSTFIELDS => $postData
           //,CURLOPT_FOLLOWLOCATION => true
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }


    public function bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $phone, $msg){
        $bulksmsurl = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
        $postvalues = "user=".$bulksmsuser."&pwd=".$bulksms_pwd."&senderid=ABC&mobileno=".$phone."&msgtext=".$msg."&smstype=0/4/3";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
    public function rightchoice_txtsmsgateway($rcuser, $rc_pwd, $phone, $msg, $sender){
        $rcsmsurl = "http://www.sms.rightchoicesms.com/sendsms/groupsms.php?";
        $postvalues = "username=".$rcuser."&password=".$rc_pwd."&type=TEXT&mobile=".$phone."&sender=".$sender."&message=".$msg;
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($rcsmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }

    public function cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender){
        $bulksmsurl = "http://203.212.70.200/smpp/sendsms?";
        $postvalues = "username=".$cloudplace_user."&password=".$cloudplace_pwd."&to=".$user_mobileno."&from=".$cloudplace_sender."&udh=&text=".$user_message."&dlr-mask=19";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
     
    
    
    public function update_password($jsondata){
        $data = array();
        $user_uid = $jsondata->user_uid;
        $current_password = $jsondata->current_password;
        $current_password = md5($current_password);
        $query = $this->db->query("select uid from sht_users where uid = '$user_uid' and password = '$current_password'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Successfully updated';
            $new_password = $jsondata->new_password;
            $new_password_mdb = md5($new_password);
            $update = $this->db->query("update sht_users set password = '$new_password_mdb', orig_pwd = '$new_password' where uid = '$user_uid'");
        
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid Current Password';
        }
        
       
        return $data;
    }
    
    public function contact_us($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select * from sht_isp_detail where isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $row = $query->row_array();
            $data['company_name'] = $row['company_name'];
            $data['address'] = $row['address1'].','.$row['address2'];
            $data['city'] = $row['city'];
            $data['pin'] = $row['pincode'];
            $data['help_number1'] = $row['help_number1'];
            $data['help_number2'] = $row['help_number2'];
            $data['help_number3'] = $row['help_number3'];
            $data['support_email'] = $row['support_email'];
            $data['lat'] = $row['lat'];
            $data['long'] = $row['longitude'];
            $data['place_id'] = $row['place_id'];
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'No data found';
        }
        //echo "<pre>";print_r($data);
        return $data;
    }
    
    public function user_permissions($jsondata){
        $data = array();
        $isp_id = $jsondata->isp_uid;
        $customer_model_permission = 0;
        $website_module_permission = 0;
        $engage_module_permission = 0;
        $shop_module_permission = 0;
        $query = $this->db->query("select * from sht_isp_admin_modules where isp_uid = '$isp_id' AND status = '1'");
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                //for customer pannel
                if($row->module == '3'){
                    $customer_model_permission = 1;
                }
                elseif($row->module == '4'){
                    $website_module_permission = 1;
                }
                elseif($row->module == '5'){
                    $engage_module_permission = 1;
                }
                elseif($row->module == '6'){
                    $shop_module_permission = 1;
                }
            }
        }
        $data['customer_model_permission'] = $customer_model_permission;
        $data['website_module_permission'] = $website_module_permission;
        $data['engage_module_permission'] = $engage_module_permission;
        $data['shop_module_permission'] = $shop_module_permission;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    public function about_us($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $data = array();
        $content = '';
        $query = $this->db->query("select about_us from sht_isp_detail where isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $content = $row['about_us'];
        }
        $data['content'] = $content;
        return $data;
    }
    public function our_services($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $data = array();
        $content = '';
        $query = $this->db->query("select services from sht_isp_detail where isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $content = $row['services'];
        }
        $data['content'] = $content;
        return $data;
    }
    public function request_plan_topup($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $name = $jsondata->name;
        $email = $jsondata->email;
        $mobile = $jsondata->mobile;
        $srvid = $jsondata->srvid;
        $msg = $jsondata->msg;
        $this->db->query("insert into sht_comsumer_request(name,email,mobile,msg,requested_on,srvid, isp_uid) values('$name', '$email', '$mobile', '$msg', now(), '$srvid', '$isp_uid')");
        $data = array();
        $data['resultCode'] = '1';
        return $data;
    }
    public function plan_type_plan_all($jsondata){
        $data = array();$plan = array();
        $isp_uid = $jsondata->isp_uid;
        $plan_type = $jsondata->plan_type;
        $plantypewhere = '';
        if($plan_type == 'Unlimited'){
            $plantypewhere = 'AND ss.plantype = "1"';
        }
        elseif($plan_type == 'Fup'){
            $plantypewhere = 'AND ss.plantype = "3"';
        }
        elseif($plan_type == 'Data'){
            $plantypewhere = 'AND ss.plantype = "4"';
        }
        $plan_query = $this->db->query("select ss.srvid,ss.srvname,ss.plantype, ss.datalimit,ss.downrate, ss.fupdownrate,spp.net_total from sht_services as ss inner join sht_plan_pricing as spp on(ss.srvid = spp.srvid) where ss.plantype != '0' AND ss.plantype != '2' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$isp_uid' $plantypewhere");
        if($plan_query->num_rows()){
            $data['resultCode'] = '1';
            $i = 0;
            foreach($plan_query->result() as $row){
                $plan[$i]['srvid'] = $row->srvid;
                $plan[$i]['downrate'] = $row->downrate;
                $plan[$i]['plantype'] = $row->plantype;
                $plan[$i]['datalimit'] = $row->datalimit;
                $plan[$i]['fupdownrate'] = $row->fupdownrate;
                $plan[$i]['srvname'] = $row->srvname;
                $plan[$i]['net_total'] = $row->net_total;
                $i++;
            }
            
        }else{
            $data['resultCode'] = '0';
        }
        $data['plans'] = $plan;
        return $data;
    }
    public function isp_all_plan_data($jsondata){
        $data = array();$plan = array();$speed_topup = array();$data_topup = array();$data_un_topup = array();
        $isp_uid = $jsondata->isp_uid;
        $plan_query = $this->db->query("select ss.srvid,ss.srvname,ss.plantype, ss.datalimit,ss.downrate, ss.fupdownrate,spp.net_total from sht_services as ss inner join sht_plan_pricing as spp on(ss.srvid = spp.srvid) where ss.plantype != '0' AND ss.plantype != '2' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' ANd ss.isp_uid = '$isp_uid'");
        if($plan_query->num_rows() > 0){
            $i = 0;
            foreach($plan_query->result() as $plan_query_row){
                $plan[$i]['plantype'] = $plan_query_row->plantype;
                $plan[$i]['srvid'] = $plan_query_row->srvid;
                $plan[$i]['datalimit'] = $plan_query_row->datalimit;
                $plan[$i]['fupdownrate'] = $plan_query_row->fupdownrate;
                $plan[$i]['srvname'] = $plan_query_row->srvname;
                $plan[$i]['net_total'] = $plan_query_row->net_total;
                $plan[$i]['downrate'] = $plan_query_row->downrate;
                $i++;
            }
        }
        $speed_topup_query = $this->db->query("select ss.srvid,ss.srvname,ss.downrate,stp.net_total from sht_services as ss inner join sht_topup_pricing as stp on(ss.srvid = stp.srvid) where ss.plantype = '0' AND ss.topuptype = '3' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$isp_uid'");
        if($speed_topup_query->num_rows()){
            $j = 0;
            foreach($speed_topup_query->result() as $speed_topup_query_row){
                $topupid = $speed_topup_query_row->srvid;
                $speed_topup[$j]['srvid'] = $speed_topup_query_row->srvid;
                $speed_topup[$j]['downrate'] = $speed_topup_query_row->downrate;
                $speed_topup_price = 0;$days = array();
                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                    foreach($get_speed_pricing->result() as $get_speed_pricing1){
                        $speed_topup_price = $speed_topup_price + $speed_topup_query_row->net_total;
                        $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                        $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                        $days[] = $get_speed_pricing1->days;
                    }
                $speed_topup[$j]['speed_topup_price'] = $speed_topup_price;
                $speed_topup[$j]['start_time'] = $start_time;
                $speed_topup[$j]['end_time'] = $end_time;
                $speed_topup[$j]['days'] = $days;
                $speed_topup[$j]['srvname'] = $speed_topup_query_row->srvname;
                $j++;
            }
        }
        $data_topup_query = $this->db->query("select ss.srvid,ss.srvname,ss.datalimit,stp.net_total from sht_services as ss inner join sht_topup_pricing as stp on(ss.srvid = stp.srvid) where ss.plantype = '0' AND ss.topuptype = '1' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$isp_uid'");
        if($data_topup_query->num_rows() > 0){
            $k = 0;
            foreach($data_topup_query->result() as $data_topup_query_row){
                $data_topup[$k]['srvid'] = $data_topup_query_row->srvid;
                $data_topup[$k]['datalimit'] = $data_topup_query_row->datalimit;
                $data_topup[$k]['net_total'] = $data_topup_query_row->net_total;
                $data_topup[$k]['srvname'] = $data_topup_query_row->srvname;
                $k++;
            }
        }
        $dataunaccount_topup_query = $this->db->query("select ss.srvid,ss.srvname,ss.downrate,stp.net_total from sht_services as ss inner join sht_topup_pricing as stp on(ss.srvid = stp.srvid) where ss.plantype = '0' AND ss.topuptype = '2' AND ss.enableplan = '1' AND ss.is_deleted = '0' AND ss.active = '0' AND ss.isp_uid = '$isp_uid'");
        if($dataunaccount_topup_query->num_rows() > 0){
            $l = 0;
            foreach($dataunaccount_topup_query->result() as $dataunaccount_topup_query_row){
                $topupid = $dataunaccount_topup_query_row->srvid;
                $data_un_topup[$l]['srvid'] = $dataunaccount_topup_query_row->srvid;
                $unaccount_topup_price = 0;
                $unaccount_topup_percent = 0;$start_time = '';$end_time = '';$days = array();
                $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                        foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                    $unaccount_topup_price = $unaccount_topup_price + $dataunaccount_topup_query_row->net_total;
                                    $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                     $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                    $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                    $days[] = $get_unaccount_pricing->days;
                                }
                $data_un_topup[$l]['unaccount_topup_price'] = $unaccount_topup_price;
                $data_un_topup[$l]['unaccount_topup_percent'] = $unaccount_topup_percent;
                $data_un_topup[$l]['start_time'] = $start_time;
                $data_un_topup[$l]['end_time'] = $end_time;
                $data_un_topup[$l]['days'] = $days;
                $data_un_topup[$l]['srvname'] = $dataunaccount_topup_query_row->srvname;
                $l++;
            }
        }
        $data['plans'] = $plan;
        $data['speed_topup'] = $speed_topup;
        $data['data_topup'] = $data_topup;
        $data['data_un_topup'] = $data_un_topup;
        return $data;
    }
    public function update_email($jsondata){
        $user_uid = $jsondata->user_uid;
        $email_id = $jsondata->email_id;
        $this->db->query("update sht_users set email = '$email_id' where uid = '$user_uid'");
        $data = array();
        $data['resultCode'] = '1';
        return $data;
    }
    public function schedule_next_plan($jsondata){
        $useruid = $jsondata->user_uid;
        $service_id = $jsondata->service_id;
        $data = array();
       //check already schedule plan if schedule then check same plan schedule which user request
        $msg = '';
	// check user can change plan or not
        $is_plan_can_change = 1;
        $check = $this->db->query("select planautorenewal from sht_users where uid = '$useruid'");
        if($check->num_rows() > 0){
            $check_row = $check->row_array();
            $is_plan_can_change = $check_row['planautorenewal'];
        }
	if($is_plan_can_change == '1'){
	    $query = $this->db->query("select * from sht_nextcycle_userplanassoc where uid = '$useruid' and status = '0'");
	    if($query->num_rows() > 0){
		//already schedule , reschedule new plan
		$row = $query->row_array();
		$scheduled_service_id = $row['baseplanid'];
		if($scheduled_service_id == $service_id){
		    $msg = "This Plan already Scheduled";
		}else{
		    $id = $row['id'];
		    $update = $this->db->query("update sht_nextcycle_userplanassoc set baseplanid = '$service_id', updated_on = now() where id = '$id'");
		    $msg = "Successfully Scheduled requested  plan";
		}
	    }
	    else{
		//first time schedule plan
		$insert = $this->db->query("insert into sht_nextcycle_userplanassoc(uid, baseplanid, created_on) values('$useruid', '$service_id', now())");
		$msg = "Successfully Scheduled requested  plan";
	    }
	}else{
	    $msg = "Oops! You can't assign plan for your next cycle. Please contact to your ISP"; 
	}
        $data['msg'] = $msg;
        return $data;
    }
    
    public function check_already_speed_topup_apply($jsondata){
        $user_uid = $jsondata->user_uid;
        $topupid = $jsondata->topupid;
        $week = $jsondata->week;
        $today_date = date("Y-m-d");
        $data = array();
        
        $get_current_speed_topup = $this->db->query("select speedtopup_enddate from sht_users where uid = '$user_uid'");
        if($get_current_speed_topup->num_rows() > 0){
            $row = $get_current_speed_topup->row_array();
            $speed_topup_enddate = $row['speedtopup_enddate'];
            if(strtotime($speed_topup_enddate) >= strtotime($today_date)){
                //already speed topup running
                $data['resultCode'] = '0'; 
            }else{
                //get topup detail
                $days = array();
                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                    $days[] = $get_speed_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $data['price'] = $total_days*$get_price_row['net_total'];
                $data['resultCode'] = '1';
            }
            
        }else{
            $data['resultCode'] = '0';
        }
        return $data;
    }
    public function check_already_data_unacc_topup_apply($jsondata){
        $user_uid = $jsondata->user_uid;
        $topupid = $jsondata->topupid;
        $week = $jsondata->week;
        $data = array();
        $today_date = date("Y-m-d");
        $get_current_data_unacc_topup = $this->db->query("select dataunacnttopup_enddate from sht_users where uid = '$user_uid'");
        if($get_current_data_unacc_topup->num_rows() > 0){
            $row = $get_current_data_unacc_topup->row_array();
            $data_unacc_topup_enddate = $row['dataunacnttopup_enddate'];
            if(strtotime($data_unacc_topup_enddate) >= strtotime($today_date)){
                //already speed topup running
                $data['resultCode'] = '0'; 
            }else{
                //get topup detail
                $days = array();
                $get_data_unacc_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                foreach($get_data_unacc_pricing->result() as $get_data_unacc_pricing1){
                    $days[] = $get_data_unacc_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $data['price'] = $total_days*$get_price_row['net_total'];
                $data['resultCode'] = '1';
            }
            
        }else{
            $data['resultCode'] = '0';
        }
        
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    public function get_topup_price($jsondata){
        $topupid = $jsondata->topupid;
        $data = array();
        $amount = '';
        $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
			if($get_price->num_rows() > 0){
				$row = $get_price->row_array();
				$amount = $row['net_total'];
				
			}
        $data['amount'] = $amount;
        return $data;
    }
    public function calculate_speed_topup_price($jsondata){
        $topupid = $jsondata->topupid;
        $week = $jsondata->week;
        $data = array();
                $days = array();
                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                    $days[] = $get_speed_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $price = $total_days*$get_price_row['net_total'];
                
                $data['actual_price'] = $get_price_row['net_total'];
                $data['total_price'] = $price;
                $data['total_days'] = $total_days;
               return $data;
    }
    public function calculate_data_unacc_topup_price($jsondata){
        $topupid = $jsondata->topupid;
        $week = $jsondata->week;
        $data = array();
                $days = array();
                $get_speed_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                    $days[] = $get_speed_pricing1->days;
                }
                $total_days = count($days)*$week;
                //get price of one day
                $get_price = $this->db->query("select net_total from sht_topup_pricing where srvid = '$topupid'");
                $get_price_row = $get_price->row_array();
                $price = $total_days*$get_price_row['net_total'];
                 $data['actual_price'] = $get_price_row['net_total'];
                $data['total_price'] = $price;
                 $data['total_days'] = $total_days;
               return $data;
    }
    public function citrus_transection_detail($jsondat){
        $id = $jsondat->id;
        $data = array();
        $query = $this->db->query("select trns_resp from sht_topup_payment_responce where id = '$id'");
        $email = '';
        $authIdCode = 0;
        $txnDateTime = 0;
        $transactionId = 0;
        $issuerRefNo = 0;
        $TxRefNo = 0;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $detail = $row['trns_resp'];
            $def = json_decode($detail);
            $email = $def->email;
            $authIdCode = $def->authIdCode;
            $txnDateTime = $def->txnDateTime;
            $transactionId = $def->transactionId;
            $issuerRefNo = $def->issuerRefNo;
            $TxRefNo = $def->TxRefNo;
        }
        $data['email'] = $email;
        $data['authIdCode'] = $authIdCode;
        $data['txnDateTime'] = $txnDateTime;
        $data['transactionId'] = $transactionId;
        $data['issuerRefNo'] = $issuerRefNo;
        $data['TxRefNo'] = $TxRefNo;
        return $data;
    }
    public function citrus_credential($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $data = array();
        	$formPostUrl = CITRUSPOSTURL;
		$secret_key = CITRUSSECRETKEY;
		$vanityUrl = CITRUSVANITYURL;
		$currency = CITRUSCURRENCY;
        $query = $this->db->query("select citrus_secret_key, citrus_post_url, citrus_vanity_url from sht_merchant_account where isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            if($row['citrus_secret_key'] != '' || $row['citrus_post_url'] != '' || $row['citrus_vanity_url'] != ''){
                $formPostUrl = $row['citrus_post_url'];
                $secret_key = $row['citrus_secret_key'];
                $vanityUrl = $row['citrus_vanity_url'];
            }
        }
        $data['fromPostUrl'] = $formPostUrl;
        $data['secret_key'] = $secret_key;
        $data['vanityUrl'] = $vanityUrl;
        $data['currency'] = $currency;
        return $data;
    }
    
    public function add_topup_order_citrus($jsondata){
        $userid = $jsondata->userid;
        $user_uid = $jsondata->user_uid;
        $topup_id = $jsondata->topup_id;
        $topuptype = $jsondata->topuptype;
        $addorder = $jsondata->addorder;
        $postdata = json_decode(json_encode($jsondata->postdata),true);
        $total_days = $jsondata->total_days;
        $actual_amount = $jsondata->actual_amount;
        $total_amount = $jsondata->total_amount;
        if($addorder == '1'){
            if($topuptype == 'datatopup'){
              // apply topup(data topup)
            
                $get_datacalc = $this->db->query("select ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$user_uid'");
                if($get_datacalc->num_rows() > 0){
                    $row = $get_datacalc->row_array();
                    
                    $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                    $row_data = $get_data->row_array();
                    $data_to_update = $row_data['datalimit'];
                    $topuptype = $row_data['topuptype'];
                    if($row['datacalc'] == '1'){
                        $this->db->query("update sht_users set downlimit = downlimit+'$data_to_update' where uid = '$user_uid'");
                    }else{
                        $this->db->query("update sht_users set comblimit = comblimit+'$data_to_update' where uid = '$user_uid'");
                    }
                    $this->db->query("insert into sht_usertopupassoc (uid, topuptype, topup_id, status, terminate, added_on) values('$user_uid', '$topuptype', '$topup_id', '1', '0', now())");
                }  
            }
            elseif($topuptype == 'speedtopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set speedtopup_startdate = '$start_date', speedtopup_enddate = '$end_date', speedtopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $this->db->query("insert into sht_usertopupassoc (uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                
            }
            elseif($topuptype == 'dataunaccounttopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set dataunacnttopup_startdate = '$start_date', dataunacnttopup_enddate = '$end_date', dataunacttopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $this->db->query("insert into sht_usertopupassoc (uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
            }
            
            // apply topup(data topup) end
            $tabledata=array("user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>$postdata['txnDateTime']);
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            //insert into sht_subscriber_billing table
            
            $transection_id = $postdata['transactionId'];
            $bill_number = date('jnyHis');
            $this->db->query("insert into sht_subscriber_billing(subscriber_id, subscriber_uuid, plan_id, bill_number, bill_type, payment_mode, actual_amount,total_amount, alert_user, bill_generate_by, status, is_deleted, bill_added_on, receipt_received, bill_paid_on, transection_id)
                             values('$userid', '$user_uid', '$topup_id', '$bill_number', 'topup', 'Net Banking', '$actual_amount', '$total_amount', '1', '0', '1', '0', now(), '1', now(), '$transection_id')");
            
        }else{
            $tabledata=array("user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
        }
        $data = array();
        $data['insert_id'] = $id;
        return $data;
    }
        public function update_bill_success_citrus($jsondata){
        $postdata = json_decode(json_encode($jsondata->postdata),true);
        $user_uid = $jsondata->user_uid;
        $addorder = $jsondata->addorder;
        $amount_payed = $jsondata->amount_payed;
        if($addorder == '1'){
             // apply topup(data topup) end
            $tabledata=array("user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>$postdata['txnDateTime']);
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            
            
            
            $transection_id = $postdata['transactionId'];
            //insert into sht_subscriber_wallet
            $this->db->query("insert into sht_subscriber_wallet (subscriber_uuid, wallet_amount, added_on) values('$user_uid', '$amount_payed', now())");
            // update bill
           
            $this->db->query("update sht_subscriber_billing SET receipt_received = '1', transection_id = '$transection_id', payment_mode = 'Net Banking', bill_paid_on = now() where subscriber_uuid = '$user_uid' and bill_type != 'advprepay' AND bill_type != 'installation' AND bill_type != 'security' and receipt_received = '0'");
            
        }else{
            $tabledata=array("user_uid"=>$user_uid,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
        }
        $data = array();
        $data['insert_id'] = $id;
        return $data;
    }
    
    public function isp_payment_gateway($jsondata){
        $isp_uid = $jsondata->isp_uid;
       
        $query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
	$i = 0;
	$gatways = array();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
	    $data['resultMsg'] = 'Success';
	    
	   
	    $row = $query->row_array();
	    
	    // for citrus
            if($row['citrus_secret_key'] != '' || $row['citrus_post_url'] != '' || $row['citrus_vanity_url'] != ''){
		$gatways[$i]['gatway_name'] = 'citrus';
                $gatways[$i]['citrus_post_url'] = $row['citrus_post_url'];
                $gatways[$i]['citrus_secret_key'] = $row['citrus_secret_key'];
                $gatways[$i]['citrus_vanity_url'] = $row['citrus_vanity_url'];
		$i++;
            }
	    // for paytm
	    if($row['paytm_merchant_key'] != '' || $row['paytm_merchant_mid'] != '' || $row['paytm_merchant_web'] != '' || $row['paytm_environment'] != ''){
		$gatways[$i]['gatway_name'] = 'paytm';
                $gatways[$i]['paytm_merchant_key'] = $row['paytm_merchant_key'];
                $gatways[$i]['paytm_merchant_mid'] = $row['paytm_merchant_mid'];
                $gatways[$i]['paytm_merchant_web'] = $row['paytm_merchant_web'];
		$gatways[$i]['paytm_environment'] = $row['paytm_environment'];
		$i++;
            }
	    // for payu 
	    if($row['payu_merchantid'] != '' || $row['payu_merchantkey'] != '' || $row['payu_merchantsalt'] != '' || $row['payu_authheader'] != ''){
		$gatways[$i]['gatway_name'] = 'payu';
                $gatways[$i]['payu_merchantid'] = $row['payu_merchantid'];
                $gatways[$i]['payu_merchantkey'] = $row['payu_merchantkey'];
                $gatways[$i]['payu_merchantsalt'] = $row['payu_merchantsalt'];
		$gatways[$i]['payu_authheader'] = $row['payu_authheader'];
		$i++;
            }
	    
        }
	else{
	    $data['resultCode'] = '0';
	    $data['resultMsg'] = 'No record found';
	}
	// for stpl harcoaded
	    if($isp_uid == '129'){
		$data['resultCode'] = '1';
		$data['resultMsg'] = 'Success';
		$gatways[$i]['gatway_name'] = 'EBS';
                $gatways[$i]['Account_Id'] = '22206';
                $gatways[$i]['Secret_Key'] = 'ecefa930fbbbd6d38d9cbc752ed8e57a';
                $gatways[$i]['Account_Name'] = 'Speed4net Technologies Pvt Ltd';
		$i++;
	    }
	    $data['gatways'] = $gatways;
        //echo "<pre>"; print_r($data);die;
        return $data;
    }
    
    public function paytm_checksum($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$orderid = $jsondata->orderid;
	$industry_type_id = $jsondata->industry_type_id;
	$channel_id = $jsondata->channel_id;
	$txn_amount = $jsondata->txn_amount;
	$mobile_no = $jsondata->mobile_no;
	$email = $jsondata->email;
	$paytm_merchant_web = $jsondata->paytm_merchant_web;
	$call_back_url = $jsondata->call_back_url;
	$StatusCheckSum = '';
	$query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    
	    $paramList = array();
	    // Create an array having all required parameters for creating checksum.
	    $paramList["MID"] = $row['paytm_merchant_mid'];
	    $paramList["ORDER_ID"] = $orderid;
	    $paramList["CUST_ID"] = $isp_uid ;
	    $paramList["INDUSTRY_TYPE_ID"] = $industry_type_id;
	    $paramList["CHANNEL_ID"] = $channel_id;
	    $paramList["TXN_AMOUNT"] = $txn_amount;
	    //$paramList["WEBSITE"] = $row['paytm_merchant_web'];
	    $paramList["WEBSITE"] = $paytm_merchant_web;
	    $paramList["MOBILE_NO"] = $mobile_no;
	    //$paramList['MERC_UNQ_REF']= $isp_uid;
	    $paramList['EMAIL']= $email;
	    $paramList["CALLBACK_URL"] =  $call_back_url;
	    //Here checksum string will return by getChecksumFromArray() function.
	    $StatusCheckSum = getChecksumFromArray($paramList,$row['paytm_merchant_key']);
	}
        
	
	
	$data['checksum'] = $StatusCheckSum;
	return $data;
    }
    public function paytm_checksum_old($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$orderid = $jsondata->orderid;
	$StatusCheckSum = '';
	$query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
	if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $paytm_merchant_key = $row['paytm_merchant_key'];
            $paytm_merchant_mid = $row['paytm_merchant_mid'];
	    $paytm_environment = $row['paytm_environment'];
	    $PAYTM_DOMAIN = "pguat.paytm.com";
	    if ($paytm_environment == 'PROD') {
		$PAYTM_DOMAIN = 'secure.paytm.in';
	    }
	    $requestParamList = array("MID" => $paytm_merchant_mid , "ORDERID" => $orderid);  
	    $StatusCheckSum = getChecksumFromArray($requestParamList,$paytm_merchant_key);
	}
        
	
	
	$data['checksum'] = $StatusCheckSum;
	return $data;
    }
    
        public function topup_payment($jsondata){
	    $userid = '';
	    if(isset($jsondata->userid)){
		$userid = $jsondata->userid;
	    }
	    $isp_uid = 0;
	    if(isset($jsondata->isp_uid)){
		$isp_uid = $jsondata->isp_uid;
	    }
        
        $user_uid = $jsondata->user_uid;
        $topup_id = $jsondata->topup_id;
        $topuptype = $jsondata->topuptype;
        $addorder = $jsondata->addorder;
        $postdata = json_decode(json_encode($jsondata->postdata),true);
	$gateway_name = $jsondata->gateway_name;
	$payment_platform = 0;
	if(isset($jsondata->payment_platform)){
	    $payment_platform = $jsondata->payment_platform;
	}
	if($gateway_name == 'EBS'){
	    $firstName = '';
	    $email = '';
	    $mobile = '';
	    $tra_date = date('Y-m-d H:i:s');
	    $total_amount = $postdata['Amount'];
	    $transection_id = $postdata['TransactionId'];
	}if($gateway_name == 'paytm'){
	    $firstName = '';
	    $email = '';
	    $mobile = '';
	    $tra_date = date('Y-m-d H:i:s');
	    $total_amount = $postdata['TXNAMOUNT'];
	    $transection_id = $postdata['TXNID'];
	}else{
	     $firstName = $postdata['firstname'];
	     $email = $postdata['email'];
	     $mobile = $postdata['phone'];
	     $tra_date = date('Y-m-d H:i:s');
	     $total_amount = $postdata['amount'];
	     $transection_id = $postdata['txnid'];
	}
        $total_days = $jsondata->total_days;
        //$actual_amount = $jsondata->actual_amount;
        //$total_amount = $jsondata->total_amount;
        if($addorder == '1'){
            if($topuptype == 'datatopup'){
              // apply topup(data topup)
            
                $get_datacalc = $this->db->query("select ss.datacalc from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$user_uid'");
                if($get_datacalc->num_rows() > 0){
                    $row = $get_datacalc->row_array();
                    
                    $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                    $row_data = $get_data->row_array();
                    $data_to_update = $row_data['datalimit'];
                    $topuptype = $row_data['topuptype'];
                    if($row['datacalc'] == '1'){
                        $this->db->query("update sht_users set downlimit = downlimit+'$data_to_update' where uid = '$user_uid'");
                    }else{
                        $this->db->query("update sht_users set comblimit = comblimit+'$data_to_update' where uid = '$user_uid'");
                    }
                    $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now())");
                }  
            }
            elseif($topuptype == 'speedtopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set speedtopup_startdate = '$start_date', speedtopup_enddate = '$end_date', speedtopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
                
            }
            elseif($topuptype == 'dataunaccounttopup'){
                $week = $this->session->userdata['user_topup_recharge_session']['week'];
                $get_data = $this->db->query("select topuptype, datalimit from sht_services where srvid = '$topup_id'");
                $row_data = $get_data->row_array();
                $topuptype = $row_data['topuptype'];
                $start_date = date('Y-m-d',strtotime("+1 day"));
                $end_date = date('Y-m-d',strtotime("+".$week." week"));
                //update speed topup entery in sht_user table
                $this->db->query("update sht_users set dataunacnttopup_startdate = '$start_date', dataunacnttopup_enddate = '$end_date', dataunacttopupid = '$topup_id' where uid = '$user_uid'");
                // insert topup record
                $this->db->query("insert into sht_usertopupassoc (isp_uid,uid, topuptype, topup_id, status, terminate, added_on, topup_startdate, topup_enddate, applicable_days) values('$isp_uid','$user_uid', '$topuptype', '$topup_id', '1', '0', now(), '$start_date', '$end_date', '$total_days')");
            }
            
            // apply topup(data topup) end
	    $responseParamList = '';
	    if($gateway_name == 'paytm'){
		if (isset($postdata["ORDERID"]) && $postdata["ORDERID"] != "") {
		    $isp_uid = 0;
		    if(isset($jsondata->isp_uid)){
			$isp_uid = $jsondata->isp_uid;
		    }
		    
		    $paytm_merchant_mid = '';$paytm_environment = '';$paytm_merchant_key = '';
		    $query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
		    if($query->num_rows() > 0){
			$row_payment = $query->row_array();
			$paytm_merchant_mid = $row_payment['paytm_merchant_mid'];
			$paytm_environment = $row_payment['paytm_environment'];
			$paytm_merchant_key = $row_payment['paytm_merchant_key'];
		    }
		    // In Test Page, we are taking parameters from POST request. In actual implementation these can be collected from session or DB. 
		    $ORDER_ID = $postdata["ORDERID"];
		    // Create an array having all required parameters for status query.
                    $requestParamList = array("MID" => $paytm_merchant_mid , "ORDERID" => $ORDER_ID);
		    $PAYTM_DOMAIN = "pguat.paytm.com";
		    if ($paytm_environment == 'PROD') {
			$PAYTM_DOMAIN = 'secure.paytm.in';
		    }
		    define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus');
		    $StatusCheckSum = getChecksumFromArray($requestParamList,$paytm_merchant_key);
        
                    $requestParamList['CHECKSUMHASH'] = $StatusCheckSum;
                    // Call the PG's getTxnStatusNew() function for verifying the transaction status.
                    $responseParamList = getTxnStatusNew($requestParamList);
		}
	    }
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$total_amount,"name"=>$firstName,"email"=>$email,
                "mobile"=>$mobile,"payment_success"=>"1","trns_resp"=>json_encode($postdata),"tes_res"=>json_encode($responseParamList),"created_on"=>$tra_date);
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            //insert into sht_subscriber_billing table
            
            
            $bill_number = date('jnyHis');
            $this->db->query("insert into sht_subscriber_billing(payment_platform,subscriber_id, subscriber_uuid, plan_id, bill_number, bill_type, payment_mode, actual_amount,total_amount, alert_user, bill_generate_by, status, is_deleted, bill_added_on, receipt_received, bill_paid_on, transection_id)
                             values('$payment_platform','$userid', '$user_uid', '$topup_id', '$bill_number', 'topup', 'Net Banking', '$actual_amount', '$total_amount', '1', '0', '1', '0', now(), '1', now(), '$transection_id')");
	    
	    
	    
            //insert into sht_subscriber_wallet
            $this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$total_amount', now())");
	    $wlastid = $this->db->insert_id();
	    $wallet_billarr = array(
		  'isp_uid' => $isp_uid,
		  'subscriber_id' => '0',
		  'subscriber_uuid' => $user_uid,
		  'wallet_id' => $wlastid,
		  'bill_added_on' => date('Y-m-d H:i:s'),
		  'receipt_number' => $transection_id,
		  'bill_type' => 'addtowallet',
		  'payment_mode' => 'netbanking',
		  'actual_amount' => $total_amount,
		  'total_amount' => $total_amount,
		  'receipt_received' => '1',
		  'alert_user' => '0',
		  'bill_generate_by' => $isp_uid,
		  'bill_paid_on' => date('Y-m-d H:i:s'),
		  'wallet_amount_received' => '1'
	    );
	    $this->db->insert('sht_subscriber_billing', $wallet_billarr);
	    $wlast_billid = $this->db->insert_id();

	    $receiptArr = array();
	    $receiptArr['cheque_dd_paytm_number'] = $transection_id;
	    $receiptArr['isp_uid'] = $isp_uid;
	    $receiptArr['uid'] = $user_uid;
	    $receiptArr['bill_id'] = $wlast_billid;
	    $receiptArr['receipt_number'] = $transection_id;
	    $receiptArr['receipt_amount'] = $total_amount;
	    $receiptArr['added_on'] = date('Y-m-d H:i:s');
	    $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
	    if($checkrcpthistoryQ->num_rows() > 0){
		  $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
	    }else{
		  $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
	    }
	    
	    
            
        }else{
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$total_amount,"name"=>$firstName,"email"=>$email,
                "mobile"=>$mobile,"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>$tra_date);

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
        }
        $data = array();
        $data['insert_id'] = $id;
	$data['resultCode'] = '1';
	$data['resultMsg'] = 'success';
        return $data;
    }
    public function bill_payment($jsondata){
        $postdata = json_decode(json_encode($jsondata->postdata),true);
	$gateway_name = $jsondata->gateway_name;
	$payment_platform = 0;
	if(isset($jsondata->payment_platform)){
	    $payment_platform = $jsondata->payment_platform;
	}
	if($gateway_name == 'EBS'){
	    $firstName = '';
	    $email = '';
	    $mobile = '';
	    $tra_date = date('Y-m-d H:i:s');
	    $total_amount = $postdata['Amount'];
	    $transection_id = $postdata['BANKTXNID'];
	}elseif($gateway_name == 'paytm'){
	    $firstName = '';
	    $email = '';
	    $mobile = '';
	    $tra_date = date('Y-m-d H:i:s');
	    $total_amount = $postdata['TXNAMOUNT'];
	    $transection_id = $postdata['TXNID'];
	}else{
	     $firstName = $postdata['firstname'];
	     $email = $postdata['email'];
	     $mobile = $postdata['phone'];
	     $tra_date = date('Y-m-d H:i:s');
	     $total_amount = $postdata['amount'];
	     $transection_id = $postdata['txnid'];
	}
	
        $user_uid = $jsondata->user_uid;
        $addorder = $jsondata->addorder;
        $amount_payed = $jsondata->amount_payed;
        if($addorder == '1'){
	    $responseParamList = '';
	    if($gateway_name == 'paytm'){
		if (isset($postdata["ORDERID"]) && $postdata["ORDERID"] != "") {
		    $isp_uid = $jsondata->isp_uid;
		    $paytm_merchant_mid = '';$paytm_environment = '';$paytm_merchant_key = '';
		    $query = $this->db->query("select * from sht_merchant_account where isp_uid = '$isp_uid'");
		    if($query->num_rows() > 0){
			$row_payment = $query->row_array();
			$paytm_merchant_mid = $row_payment['paytm_merchant_mid'];
			$paytm_environment = $row_payment['paytm_environment'];
			$paytm_merchant_key = $row_payment['paytm_merchant_key'];
		    }
		    // In Test Page, we are taking parameters from POST request. In actual implementation these can be collected from session or DB. 
		    $ORDER_ID = $postdata["ORDERID"];
		    // Create an array having all required parameters for status query.
                    $requestParamList = array("MID" => $paytm_merchant_mid , "ORDERID" => $ORDER_ID);
		    $PAYTM_DOMAIN = "pguat.paytm.com";
		    if ($paytm_environment == 'PROD') {
			$PAYTM_DOMAIN = 'secure.paytm.in';
		    }
		    define('PAYTM_STATUS_QUERY_NEW_URL', 'https://'.$PAYTM_DOMAIN.'/oltp/HANDLER_INTERNAL/getTxnStatus');
		    $StatusCheckSum = getChecksumFromArray($requestParamList,$paytm_merchant_key);
        
                    $requestParamList['CHECKSUMHASH'] = $StatusCheckSum;
                    // Call the PG's getTxnStatusNew() function for verifying the transaction status.
                    $responseParamList = getTxnStatusNew($requestParamList);
		}
	    }
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$total_amount,"name"=>$firstName,"email"=>$email,
                "mobile"=>$mobile,"payment_success"=>"1","trns_resp"=>json_encode($postdata),"tes_res"=>json_encode($responseParamList),"created_on"=>$tra_date);
            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
            
            
            $bill_paid_till_date = '';
	    $query_date = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$user_uid' and bill_type != 'Advprepay' AND bill_type != 'installation' AND bill_type != 'security' ");
            if($query_date->num_rows() > 0){
                  foreach($query_date->result() as $query_date_row){
                        if($query_date_row->bill_type == 'montly_pay'){
                              $bill_paid_till_date = date('Y-m-d', strtotime($query_date_row->bill_added_on));
                        }
                        
                  }
            }
	    //$amount_payed = $total_amount;
	    $isp_uid = 0;
	    if(isset($jsondata->isp_uid)){
		$isp_uid = $jsondata->isp_uid;
	    }
	    $amount_payed = $this->userbalance_amount($user_uid);
	    $custom_amount = $total_amount - abs($amount_payed);
            //insert into sht_subscriber_wallet
	    if($amount_payed < 0){
		$amount_payed = abs($amount_payed);
		$this->db->query("insert into sht_subscriber_wallet (isp_uid,subscriber_uuid, wallet_amount, added_on) values('$isp_uid','$user_uid', '$amount_payed', now())");
		$wlastid = $this->db->insert_id();
		$wallet_billarr = array(
		      'isp_uid' => $isp_uid,
		      'subscriber_id' => '0',
		      'subscriber_uuid' => $user_uid,
		      'wallet_id' => $wlastid,
		      'bill_added_on' => date('Y-m-d H:i:s'),
		      'receipt_number' => $transection_id,
		      'bill_type' => 'addtowallet',
		      'payment_mode' => 'netbanking',
		      'actual_amount' => $amount_payed,
		      'total_amount' => $amount_payed,
		      'receipt_received' => '1',
		      'alert_user' => '0',
		      'bill_generate_by' => $isp_uid,
		      'bill_paid_on' => date('Y-m-d H:i:s'),
		      'wallet_amount_received' => '1',
		      "payment_platform" => $payment_platform,
		);
		$this->db->insert('sht_subscriber_billing', $wallet_billarr);
		$wlast_billid = $this->db->insert_id();
    
		$receiptArr = array();
		$receiptArr['cheque_dd_paytm_number'] = $transection_id;
		$receiptArr['isp_uid'] = $isp_uid;
		$receiptArr['uid'] = $user_uid;
		$receiptArr['bill_id'] = $wlast_billid;
		$receiptArr['receipt_number'] = $transection_id;
		$receiptArr['receipt_amount'] = $amount_payed;
		$receiptArr['added_on'] = date('Y-m-d H:i:s');
		$checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
		if($checkrcpthistoryQ->num_rows() > 0){
		      $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
		}else{
		      $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
		}
		// update bill
	       
		$this->db->query("update sht_subscriber_billing SET receipt_received = '1', transection_id = '$transection_id', payment_mode = 'Net Banking', bill_paid_on = now(),payment_platform = '$payment_platform' where subscriber_uuid = '$user_uid' and bill_type != 'advprepay' AND bill_type != 'installation' AND bill_type != 'security' and receipt_received = '0'");
	    }
	    // custom amount
	    if($custom_amount > 0){
		$this->addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id);
	    }
            
            $userassoc_details = $this->userassoc_details($user_uid);
            $user_credit_limit = $userassoc_details['user_credit_limit'];
	    $user_wallet_balance = $this->userbalance_amount($user_uid);
	    $plan_cost_perday = $userassoc_details['plan_cost_perday'];
            $paidtill_date = $userassoc_details['paidtill_date'];
            if($bill_paid_till_date != ''){
                 $paidtill_date = $bill_paid_till_date;
            }
            $expiration_acctdays = ceil(($user_credit_limit + $user_wallet_balance) / $plan_cost_perday);
            $expiration_date = date('Y-m-d', strtotime($paidtill_date . " +".$expiration_acctdays." days"));
            $expiration_date = $expiration_date.' 00:00:00';
            $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $user_uid));
            if($bill_paid_till_date != ''){
                  $this->db->update('sht_users', array('paidtill_date' => $paidtill_date), array('uid' => $user_uid));
            }
        }
	else{
            $tabledata=array("payment_platform" => $payment_platform,"user_uid"=>$user_uid,"total_amt"=>$total_amount,"name"=>$firstName,"email"=>$email,
                "mobile"=>$mobile,"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>$tra_date);

            $this->db->insert('sht_topup_payment_responce',$tabledata);
            $id = $this->db->insert_id();
        }
        $data = array();
        $data['insert_id'] = $id;
	$data['resultCode'] = '1';
	$data['resultMsg'] = 'success';
        return $data;
    }
    public function addcustombillrcpt($custom_amount,$user_uid,$isp_uid,$transection_id){
	// get unpaid custom bills
	$unpaid = $this->db->query("select * from sht_subscriber_custom_billing where is_deleted = '0' and receipt_received != '1' and uid = '$user_uid'");
	if($unpaid->num_rows() > 0){
	    foreach($unpaid->result() as $row){
		$billid = $row->id;
		$billno = $row->bill_number;
		$uuid = $user_uid;
		$rcpt_addedon = date('Y-m-d H:i:s');
		$rcpt_number = $transection_id;
		$receiptArr = array();
		$update_payment_mode = "netbanking";
		$receiptArr['cheque_dd_paytm_number'] = $transection_id;
		
		$receiptArr['payment_mode'] = $update_payment_mode;
		$receiptArr['bill_id'] = $billid;
		$receiptArr['bill_number'] = $billno;
		$receiptArr['receipt_number'] = $rcpt_number;
		$receiptArr['added_on'] = $rcpt_addedon;
		$receiptArr['receipt_amount'] = $row->total_amount;
		$receiptArr['uid'] = $uuid;
		
		$this->db->insert('sht_custom_receipt_list', $receiptArr);
		// paid custom bills
		
		$update = $this->db->query("update sht_subscriber_custom_billing set receipt_received = '1', bill_paid_on = now() where id = '$billid'");
	    }
	}
    }

    
          public function userassoc_details($uuid){
            $data = array();
            $userassocQ = $this->db->query("SELECT tb1.user_credit_limit,tb1.paidtill_date, tb1.baseplanid,tb3.net_total FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$uuid."'");
            if($userassocQ->num_rows() > 0){
                  $rowdata = $userassocQ->row();
                  $data['user_credit_limit'] = $rowdata->user_credit_limit;
                  $data['plan_cost_perday'] = round(($rowdata->net_total/30) ,2);
                  $data['paidtill_date'] = $rowdata->paidtill_date;
            }
            return $data;
      }
      
    public function usage_graph_date($jsondata){
	$subscriber_uid = $jsondata->user_uid;
	$data = array();
		$query = $this->db->query("select plan_activated_date from sht_users where uid = '$subscriber_uid'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$from = date('d-m-Y', strtotime($row['plan_activated_date']));
			$to = date('d-m-Y');
		}else{
			$from = date('d-m-Y');
			$to = date('d-m-Y');
		}
		$data['from'] = $from;
		$data['to'] = $to;
		return $data;
    }
    
    public function usage_graph($jsondata){
		$uuid = $jsondata->user_uid;
		$date_filter_from = date('Y-m-d',strtotime($jsondata->from));
		$date_filter_to = date('Y-m-d',strtotime($jsondata->to));
		$total_upload = 0;
		$total_download = 0;
		$data = array();
		$query = $this->db->query("SELECT *, DAY( start_time ) as date, MONTH( start_time ) as month,  TIME( start_time ) as starttime , TIME( end_time ) as endtime  FROM sht_user_daily_data_usage WHERE uid='".$uuid."' AND DATE(start_time) BETWEEN '$date_filter_from' AND '$date_filter_to'");
		//echo $this->db->last_query();die;
		if($query->num_rows() > 0){
		$i = 0;
		foreach($query->result() as $uobj){
			$total_upload = $total_upload + $uobj->upload;
			$total_download = $total_download + $uobj->download;
			$starttime = $uobj->starttime;
			$endtime = $uobj->endtime;
			$date = $uobj->date;
			$dataconsumed = round( ((($uobj->upload) + ($uobj->download)) / (1024*1024)), 2 );
			if(($starttime == '00:00:00') && ($endtime == '11:59:59')){
				$data['firsthalfdata'][] = array('y' => $dataconsumed, 'label' => $date, 'toolTipContent' => 'Data Used: '.$dataconsumed. "MB");
			}elseif(($starttime == '12:00:00') && ($endtime == '23:59:59')){
				$data['secondhalfdata'][] = array('y' => $dataconsumed, 'label' => $date, 'toolTipContent' => 'Data Used: '.$dataconsumed. "MB");
			}
			$i++;
		}
		//$data = array_values($data);
		}
		if($total_upload > 0){
			$total_upload = round($total_upload/(1024*1024*1024),2);
		}else{
			$total_upload = "0";
		}
		$data['total_upload'] = $total_upload. " GB";
		if($total_download > 0){
			$total_download = round($total_download/(1024*1024*1024),2);
		}else{
			$total_download = "0";
		}
		$data['total_download'] = $total_download. " GB";
		$data['total_used'] = ($total_upload+$total_download). " GB";
		//echo "<pre>";print_r($data);die;
		return $data;
    }
     public function speed_graph($jsondata){
		$db = array(
		    'dsn'	=> '',
		    'hostname' => '103.20.214.109',
		    'username' => 'decibelremote',
		    'password' => 'remote@giant',
		    'database' => 'decibelgraph',
		    'dbdriver' => 'mysqli',
		    'dbprefix' => '',
		    'pconnect' => FALSE,
		    'db_debug' => (ENVIRONMENT !== 'production'),
		    'cache_on' => FALSE,
		    'cachedir' => '',
		    'char_set' => 'utf8',
		    'dbcollat' => 'utf8_general_ci',
		    'swap_pre' => '',
		    'encrypt' => FALSE,
		    'compress' => FALSE,
		    'stricton' => FALSE,
		    'failover' => array(),
		    'save_queries' => TRUE
		);
		$dynamicDB = $this->load->database($db, TRUE);
		$data = array();
		$uuid = $jsondata->user_uid;
		//$uuid = '10000148';
		$date_filter  = $jsondata->date_range;
		$date_filter_from = '';
		$date_filter_to = '';
		if($date_filter == 'today'){
		    $date_filter_from = date("Y-m-d");
		    $date_filter_to = date("Y-m-d");
		    $query = $dynamicDB->query("select DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 300");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'yesterday'){
		    $date_filter_from = date('Y-m-d', strtotime('-1 days'));
		    $date_filter_to = date('Y-m-d', strtotime('-1 days'));
		    $query = $dynamicDB->query("select DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 300");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_week'){
		    $date_filter_from = date('Y-m-d', strtotime('-6 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $dynamicDB->query("select DATE_FORMAT(date, '%a') as dates,DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 1800");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates." ".$row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_month'){
		    $date_filter_from = date('Y-m-d', strtotime('-30 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $dynamicDB->query("select DATE_FORMAT(date, '%a %d %b') as dates,DATE_FORMAT(time, '%H:%i') as time,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY UNIX_TIMESTAMP(time) DIV 7200");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates." ".$row->time;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		elseif($date_filter == 'last_year'){
		    $date_filter_from = date('Y-m-d', strtotime('-365 days'));
		    $date_filter_to = date('Y-m-d');
		    $query = $dynamicDB->query("select DATE_FORMAT(date, '%a %d %b') as dates,sum(input_octat) as input, sum(output_octat)as output, sum(session_time) as session_time from speed_graph where uid = '$uuid' AND DATE(date) BETWEEN '$date_filter_from' AND '$date_filter_to' GROUP BY DATE(date)");
		    //echo "<pre>";print_r($query->result());
		    foreach($query->result() as $row){
			$lable = $row->dates;
			$input_speed = round(($row->input*8)/(1000*$row->session_time), 2);
			$data['input'][] = array('y' => -$input_speed, 'label' => $lable, 'toolTipContent' => 'Upload Speed: '.$input_speed. "KB/Sec");
			$output_speed = round(($row->output*8)/(1000*$row->session_time),2);
			$data['output'][] = array('y' => $output_speed, 'label' => $lable, 'toolTipContent' => 'Download Speed: '.$output_speed. "KB/Sec");
		    }
		}
		
		return $data;
		
	}
	
		public function get_logs($table,$where)
	{
		  $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
		$this->DB2->select('*');
		$this->DB2->from($table);
		$this->DB2->where($where);
		$this->DB2->order_by("id", "desc");
		$this->DB2->limit(10);
		$query = $this->DB2->get(); 
		echo "<pre>"; print_R($query->result());

	}
	
	public function sync_guest_session($table,$where)
	{
		  $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
		$this->DB2->select('*');
		$this->DB2->from($table);
		$this->DB2->where($where);
		$this->DB2->order_by("id", "desc");
		
		$query = $this->DB2->get();
		foreach($query->result() as $val)
		{
			$this->DB2->update('wifi_location_cptype',array("cp1_no_of_daily_session"=>$val->plan_hour),array("location_id"=>$val->location_id));
			echo $this->DB2->last_query()."<br/>";
		}		
		echo "<pre>"; print_R($query->result());
		
	}
   



    
}



?>
