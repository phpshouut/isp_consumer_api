<?php

class Captiveportal_model extends CI_Model {

	  public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
    }

    public function authenticateApiuser($user_id, $api_key) {
        $query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
        $num = count($query);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function authenticateApiGuestuser($guest_user_id) {
        $query = $this->mongo_db->get_where(TBL_GUEST_USER, array('_id' => new MongoId($guest_user_id)));
        $num = count($query);
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

	
    public function log($function, $request, $responce) {
        $log_data = array(
            'function_name' => $function,
            'request' => $request,
            'responce' => $responce,
            'time' => date("d-m-Y H:i:s"),
        );
        $this->mongo_db->insert('log_data', $log_data);
    }

    public function captive_ad_data($jsondata) {

        $datarr = $this->get_ad_id($jsondata);
		//echo "<pre>"; print_R($datarr); die;
		
       
        $data=array();
     
      
      $captivelogarr=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr['gender']),
	  "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr['age_group']);
        if($datarr['offer_type']=="image_add")
        {
            $data=$this->get_image_ad_data($datarr['cp_offer_id']);
             $this->add_cp_analytics($captivelogarr);
           //  $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
        else if($datarr['offer_type']=="video_add")
        {
            $data=$this->get_video_ad_data($datarr['cp_offer_id']);
             $this->add_cp_analytics($captivelogarr);
           //   $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
        
        else if($datarr['offer_type']=="poll_add")
        {
             $data=$this->get_poll_ad_data($datarr['cp_offer_id'],$jsondata->locationId,$jsondata->userId);
              $this->add_cp_analytics($captivelogarr);
            //    $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
       else if($datarr['offer_type']=="captcha_add")
        {
             $data=$this->get_brand_captcha_data($datarr['cp_offer_id']);
              $this->add_cp_analytics($captivelogarr);
              // $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
         else if($datarr['offer_type']=="redeemable_add")
        {
             $data=$this->get_redeemable_offer_data($datarr['cp_offer_id']);
              $this->add_cp_analytics($captivelogarr);
            //   $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
		 else if($datarr['offer_type']=="cost_per_lead_add")
        {
            $data=$this->get_cost_per_lead_add_data($datarr['cp_offer_id']);
            $this->add_cp_analytics($captivelogarr);
            //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
		
       else if($datarr['offer_type']=="default")
        {
           $defdatarr = $this->get_defaultad_id($jsondata);
		   
             $captivelogarr=array("cp_offer_id"=>$defdatarr['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($defdatarr['gender']),
	  "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$defdatarr['age_group']);
          
                    if($defdatarr['offer_type']=="image_add")
                {
                    $data=$this->get_image_ad_data($defdatarr['cp_offer_id']);
                     //$this->add_cp_analytics($captivelogarr);
                    
                }
                else if($defdatarr['offer_type']=="video_add")
                {
                    $data=$this->get_video_ad_data($defdatarr['cp_offer_id']);
                   
                     //$this->add_cp_analytics($captivelogarr);
                     
                }

                else if($defdatarr['offer_type']=="poll_add")
                {
                     $data=$this->get_poll_ad_data($defdatarr['cp_offer_id'],$jsondata->locationId,$jsondata->userId);
                      //$this->add_cp_analytics($captivelogarr);
                     
                }
               else if($defdatarr['offer_type']=="captcha_add")
                {
                     $data=$this->get_brand_captcha_data($defdatarr['cp_offer_id']);
                      //$this->add_cp_analytics($captivelogarr);
                      
                }
                 else if($defdatarr['offer_type']=="redeemable_add")
                {
                     $data=$this->get_redeemable_offer_data($defdatarr['cp_offer_id']);
                      //$this->add_cp_analytics($captivelogarr);
                      
                }
           
          
        }
        else
        {
             $data['resultCode']='0';
            $data['resultMessage']='Not Available for mobile';
        }
        //Api Cp Analytivs logs
         
        return $data;
    }




    public function get_ad_id($jsondata) {
        $data=array();
        $query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.cp_offer_id!='1'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add' AND co.offer_type!='survey_add'
                    AND co.offer_type!='app_install_add' and col.location_id='" . $jsondata->locationId . "' AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' and co.is_coke='0' and co.is_mtnl_default_ad='0' ORDER BY co.cp_offer_id desc");

        $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }


            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


               $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
                if($userdata[0]['gender']!="")
				{
					$gender=substr(strtoupper($userdata[0]['gender']),0,1);
				}	
				else{
					$gender="";
				}
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

               if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }


                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }


        }
		
         //echo "<pre>"; print_R($query->result());die;
		 

        if (!empty($liveofferarr)) {
            $query = $this->DB2->query("SELECT `cpoffer_id` FROM `cp_adlocation` WHERE location_id='" . $jsondata->locationId . "'");
            $offer_sceen_array = array();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $offer_sceens) {
                    $offer_sceen_array[] = $offer_sceens->cpoffer_id; // create array of offer sceen
                }
            }
			$offer_sceen_array=array_filter($offer_sceen_array);
		
			if(count(array_diff($liveofferarr,$offer_sceen_array))==0)
			{
				$delete = $this->DB2->query("delete from cp_adlocation WHERE
    location_id = '$jsondata->locationId'");
			}
			
            $userseenarr=array();
            $query1 = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='" . $jsondata->userId . "'");
            if ($query1->num_rows() > 0) {
                foreach ($query1->result() as $user_sceens) {
                    $userseenarr[] = $user_sceens->cp_offer_id; // create array of offer sceen
                }
            }
		
            $unseen_ids = '';
            $usercanseearr=array_values(array_diff(array_unique($liveofferarr),$userseenarr));
		
			$userseearr=array();
			
            if(!empty($usercanseearr))
            {
				if(count(array_diff($usercanseearr, $offer_sceen_array)) > 0){
					foreach($usercanseearr as $valu)
					{
						if(!in_array($valu,$offer_sceen_array))
						{
							$userseearr[]=$valu;
							break;
						}
					}
					$unseen_ids = $userseearr[0];
					
				}
				else{
					
					$unseen_ids = $usercanseearr[0];
					
				}
				

                

                $tabledata = array("cpoffer_id" => $unseen_ids, "location_id" => $jsondata->locationId);
                $this->DB2->insert("cp_adlocation", $tabledata);
                //   $tabledata1 = array("cp_offer_id" => $unseen_ids, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId);
               //   $this->DB2->insert("cp_adlocation_default", $tabledata1);
				 $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$unseen_ids."'");
            $rowarr=$query->row_array();
            $data['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offer_type'] = trim($rowarr['offer_type']);
            $data['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
            }
            else if(empty($liveofferarr)){
                $unseen_ids = '1';
				 $data['cp_offer_id'] = '1';
            $data['offer_type'] = 'default';
                
	
            }
			else{
				$delete = $this->DB2->query("delete from cp_adlocation_default WHERE
    location_id = '$jsondata->locationId' and user_id='$jsondata->userId'");
	$unseen_ids=$liveofferarr[0];
	 $tabledata = array("cpoffer_id" => $unseen_ids, "location_id" => $jsondata->locationId);
                $this->DB2->insert("cp_adlocation", $tabledata);
                //   $tabledata1 = array("cp_offer_id" => $unseen_ids, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId);
                //  $this->DB2->insert("cp_adlocation_default", $tabledata1);
				 $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$unseen_ids."'");
            $rowarr=$query->row_array();
            $data['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offer_type'] = trim($rowarr['offer_type']);
            $data['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
			}



           
        }
        else {
            $data['cp_offer_id'] = '';
            $data['offer_type'] = 'default';
        }


        return $data;
    }

    
    
     public function get_defaultad_id($jsondata) {
				$data=array();
       
			$query1=$this->DB2->query("select Provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarr1=$query1->row_array();
			if($rowarr1['Provider_id']==14)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where is_mtnl_default_ad='1' and offer_type='image_add' order by cp_offer_id desc limit 1");
			}
			else if($rowarr1['Provider_id']==39)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='89'");
			}
			else if($rowarr1['Provider_id']==42)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='102'");
				
			}
			else{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
			}
			 $userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
           // $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
            $rowarr=$query->row_array();
            $data['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offer_type'] = trim($rowarr['offer_type']);
            $data['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
        



        return $data;
    }
    
    public function allocate_budget_views($locid,$offerid)
    {
        $query=$this->DB2->query("SELECT * FROM `cp_budget_used` WHERE cp_offer_id='".$offerid."'");
        $query1=$this->DB2->query("SELECT `location_type` FROM `wifi_location` WHERE id='".$locid."'");
          $locationarr=$query1->row_array();
           $arr1=array(1=>"mass_budget",2=>"premium_budget",3=>"star_budget");
        if($query->num_rows()>0)
        {
             $query3=$this->DB2->query("select co.`total_budget`,co.`mass_budget`,co.`premium_budget`,co.`star_budget` FROM `cp_offers` co where co.cp_offer_id='".$offerid."'");
             $bugetarr=$query3->row_array();
             //$usedbudgetarr=$query->row_array();
             if($locationarr['location_type']==1)
             {
                 $totalvalue=$bugetarr['mass_budget']/100*$bugetarr['total_budget'];
                 //$assignedvalue=$usedbudgetarr['mass_budget'];
                 $assignedvalue = 0;
                 foreach($query->result() as $row_new){
                     $assignedvalue = $assignedvalue+ $row_new->mass_budget;
                 }
                 if($totalvalue>$assignedvalue)
                 {
                   
                    return true;

                 }
                 else
                 {
                     return false;
                 }
             }
             else if($locationarr['location_type']==2)
             {
                 $totalvalue=$bugetarr['premium_budget']/100*$bugetarr['total_budget'];
                 //$assignedvalue=$usedbudgetarr['premium_budget'];
                 $assignedvalue = 0;
                 foreach($query->result() as $row_new){
                     $assignedvalue = $assignedvalue+ $row_new->premium_budget;
                 }
                 if($totalvalue>$assignedvalue)
                 {
                    
                    return true;

                 }
                 else
                 {
                     return false;
                 }
             }
              else if($locationarr['location_type']==3)
             {
                 $totalvalue=$bugetarr['star_budget']/100*$bugetarr['total_budget'];
                 //$assignedvalue=$usedbudgetarr['star_budget'];
                 $assignedvalue = 0;
                 foreach($query->result() as $row_new){
                     $assignedvalue = $assignedvalue+ $row_new->star_budget;
                 }
                 if($totalvalue>$assignedvalue)
                 {
                    
                    return true;

                 }
                 else
                 {
                     return false;
                 }
             }
        }
 else {
    
    return true;
 }
        
        
    }
    
    
   public function insert_update_budget($locid,$offerid,$offertype)
    {
        $query=$this->DB2->query("SELECT * FROM `cp_budget_used` WHERE location_id='".$locid."' AND cp_offer_id='".$offerid."'");
        $query1=$this->DB2->query("SELECT `location_type` FROM `wifi_location` WHERE id='".$locid."'");
        $query4=$this->DB2->query("select star,premium,mass from cp_location_budget where offer_type='".$offertype."'");
        $viewmoneyarr=$query4->row_array();
        $locationarr=$query1->row_array();
        $arr1=array(1=>"mass_budget",2=>"premium_budget",3=>"star_budget");
        if($query->num_rows()>0)
        {
            $query3=$this->DB2->query("select co.`total_budget`,co.video_duration,co.`mass_budget`,co.`premium_budget`,co.`star_budget` FROM `cp_offers` co where co.cp_offer_id='".$offerid."'");
            $bugetarr=$query3->row_array();
            $usedbudgetarr=$query->row_array();
            if($locationarr['location_type']==1)
            {
                $totalvalue=$bugetarr['mass_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['mass_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['mass'];
					}
					else{
						$newval=$viewmoneyarr['mass'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("mass_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
            else if($locationarr['location_type']==2)
            {
                $totalvalue=$bugetarr['premium_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['premium_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['premium'];
					}
					else{
						$newval=$viewmoneyarr['premium'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("premium_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
            else if($locationarr['location_type']==3)
            {
                $totalvalue=$bugetarr['star_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['star_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['star'];
					}
					else{
						$newval=$viewmoneyarr['star'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("star_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
        }
        else {


 if($locationarr['location_type']==1)
     {
         $budget=$viewmoneyarr['mass'];
     }
     else if($locationarr['location_type']==2)
     {
         $budget=$viewmoneyarr['premium'];
     }
     else{
         $budget=$viewmoneyarr['star'];
     }
     
        $tabledata=array("location_id"=>$locid,$arr1[$locationarr['location_type']]=>$budget,"cp_offer_id"=>$offerid);
        $this->DB2->insert("cp_budget_used",$tabledata);

        }


    }


    public function get_app_download_ad_data($offer_id,  $location_type = null) {
        $data=array();
        $query=$this->DB2->query("SELECT co.incentive_desc,co.android_appurl, co.ios_appurl,co.cp_offer_id,co.offer_desc,
co.redirect_url,co.offer_title,co.brand_id,
        co.campaign_name,co.`campaign_end_date`,co.image_original,
co.image_small,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."' and co.is_mtnl_default_ad='0'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'app_install_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='app_install_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_name']=strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"";
            $data['brand_original_image']=($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"";
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['incentive_desc'];
            $data['android_app_url'] = $rowarr['android_appurl'];
            $data['ios_app_url'] = $rowarr['ios_appurl'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
        return $data;
    }

    public function get_image_ad_data($offer_id,  $location_type = null) {
        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,co.`campaign_end_date`,co.image_original,
co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'image_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='image';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
			$data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
        return $data;
    }

		public function get_survey_ad_data($offer_id,  $location_type = null) {
        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,co.`campaign_end_date`,co.image_original,
co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'survey_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
			
			$query3=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='survey_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
             $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
			 $data['time_slot']=$this->time_conversion($query3->num_rows()*10);
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Survey offer';

        }
        return $data;
    }

    public function get_video_ad_data($offer_id, $location_type = null) {
        $data=array();

        $query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.video_path,co.brand_id,co.campaign_name,co.`campaign_end_date`,co.image_original,
co.image_small,co.video_thumb_logo,co.video_thumb_original,co.video_thumb_medium,co.poll_brand_title,co.poll_original,co.poll_logo,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");

        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'video_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='video';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
			$data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']="";
            $data['offers_original_image']="";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']=CAMPAIGN_VIDEOPATH.$rowarr['video_path'];
			$data['video_thumb_image']=($rowarr['video_thumb_medium']!='')?REWARD_IMAGEPATH."campaign/video_thumb/medium/".$rowarr['video_thumb_medium']:REWARD_IMAGEPATH."campaign/video_thumb/original/".$rowarr['video_thumb_original'];
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {

            $data['resultCode']='0';

            $data['resultMessage']='No Video offer';


        }

        return $data;
    }


    public function get_poll_ad_data($offer_id,$loc_id,$userid, $location_type = null)
    {
        $quesid= $this->pickup_poll_questionid($offer_id,$loc_id,$userid);

        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.redirect_url,co.poll_logo,co.poll_original,co.poll_brand_title,co.offer_title,co.brand_id,co.campaign_name,co.image_original,
co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,cpq.id,cpq.question,cpq.option1,cpq.option2,cpq.option3,cpq.option4 FROM `cp_offers` co
INNER JOIN `cp_offers_question` cpq ON (co.cp_offer_id=cpq.cp_offer_id)
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."' and cpq.id='".$quesid."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='poll';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"";
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"";
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']='';
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['poll_question_id']=$rowarr['id'];
            $data['poll_question']=$rowarr['question'];
            $data['poll_option'][0]=$rowarr['option1'];
            $data['poll_option'][1]=$rowarr['option2'];
            $data['poll_option'][2]=$rowarr['option3'];
            $data['poll_option'][3]=$rowarr['option4'];
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['poll_logo']=$rowarr['poll_logo'];
            $data['poll_original']=$rowarr['poll_original'];
            $data['poll_brand_title']=$rowarr['poll_brand_title'];

            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Video offer';

        }
        return $data;

    }
    
    
    public function pickup_poll_questionid($offer_id,$locid,$user_id)
    {
       $livepollqusetarr=array();
       $query=$this->DB2->query("select id from cp_offers_question where cp_offer_id='".$offer_id."' ");
       foreach($query->result() as $val)
       {
           $livepollqusetarr[]=$val->id;
       }
       
         if (!empty($livepollqusetarr)) {
            $query = $this->DB2->query("SELECT `question_id` FROM `cp_poll_history` WHERE location_id='" . $locid . "' and user_id='".$user_id."' and offer_id='".$offer_id."'");
            $offer_sceen_array = array();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $offer_sceens) {
                    $offer_sceen_array[] = $offer_sceens->question_id; // create array of offer sceen
                }
            }

            $unseen_ids = '';
            foreach ($livepollqusetarr as $v1) {
                if (in_array($v1, $offer_sceen_array)) {
                    continue;
                } else {
                    $unseen_ids = $v1;
                    break;
                }
            }

            if ($unseen_ids == '') {
                // delete all seen ids from db
           
                $delete = $this->DB2->query("delete from cp_poll_history WHERE 
    location_id='".$locid."' and user_id='".$user_id."' and offer_id='".$offer_id."'");
              
                $unseen_ids = $livepollqusetarr[0];
            }
             $tabledata = array("question_id" => $unseen_ids, "location_id" => $locid, "user_id"=>$user_id,"offer_id"=>$offer_id);
             $this->DB2->insert("cp_poll_history", $tabledata);
             
             return $unseen_ids;
        }
        
    }


    public function get_brand_captcha_data($offer_id, $location_type = null)
    {
        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.campaign_name,co.image_original,
co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,co.offer_title,co.`captcha_text`,co.`captcha_accuracy_level`,co.`redirect_url` FROM `cp_offers` co

INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'captcha_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='captcha';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
           $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']='';
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['poll_question']='';
            $data['poll_option']=array();



            $data['captch_text']=$rowarr['captcha_text'];
            $data['captcha_authenticity']=$rowarr['captcha_accuracy_level'];
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Video offer';

        }
        return $data;

    }
	
	
	  public function get_offerlisting_data($jsondata)
    {
		$query=$this->DB2->query("select latitude,longitude from wifi_location where id='".$jsondata->locationId."'");
		if($query->num_rows()>0)
		{
			$latarr=$query->row_array();
			$userlat=$latarr['latitude'];
			$userlon=$latarr['longitude'];
			
			$query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.offer_title,co.offer_type,co.campaign_name,co.platform_filter,co.active_between,co.campaign_day_to_run,co.image_original,
co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,co.`captcha_text`,co.`captcha_accuracy_level`,co.`redirect_url` FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id)
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where co.offer_type='offer_add' and cpl.location_id='".$jsondata->locationId."' 
 AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW() order by co.cp_offer_id desc LIMIT 0 , 5");

			$query1=$this->DB2->query("
SELECT (
6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( $userlat ) ) * COS( RADIANS( s.longitude ) - RADIANS( $userlon) ) + SIN( RADIANS( $userlat ) ) * SIN( RADIANS( s.latitude ) ) )
) AS distance, co.brand_id,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_type, co.cp_offer_id, co.redirect_url,co.offer_title, co.video_path, co.campaign_name,co.platform_filter,co.active_between,co.campaign_day_to_run, co.campaign_end_date, co.image_original, co.image_small, b.brand_name, b.original_logo, b.original_logo_logoimage, s.city, s.geo_address, s.latitude, s.longitude
FROM cp_offers co
INNER JOIN cp_offers_store cps ON ( co.cp_offer_id = cps.cp_offer_id )
INNER JOIN store s ON ( cps.store_id = s.store_id )
INNER JOIN brand b ON ( co.brand_id = b.brand_id )
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where co.offer_type='offer_add' and cpl.location_id='".$jsondata->locationId."'  AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
GROUP BY co.cp_offer_id 
LIMIT 0 , 5");
			$array1=array();
			$fnlarray2=array();
			foreach($query1->result() as $val)
			{
				$dayarr = array_filter(explode(",", $val->campaign_day_to_run));
				$platformarr=explode(",",$val->platform_filter);
				$timearr=array();
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
					
				//filter for day
				 if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
					if($budget_allocate==true && $filter=true){
					$array1[$val->cp_offer_id]=$val;
					}
				
				
				
				
			}
			$x=0;
			foreach($query->result() as $val1)
			{
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
				$dayarr = array_filter(explode(",", $val1->campaign_day_to_run));
				$platformarr=explode(",",$val1->platform_filter);
				$timearr=array();
				
				
					
				//filter for day
				 if ($val1->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val1->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val1->cp_offer_id);
					if($budget_allocate==true && $filter=true){
								if($x<5){
						if(array_key_exists($val1->cp_offer_id,$array1))
						{
							$fnlarray2[]=$array1[$val1->cp_offer_id];
						}
						else{
							$fnlarray2[]=$val1;
						}
						}
						$x++;
					}
				
				
			
				
			}
		//	echo "<pre>"; print_R($fnlarray2);
		if(!empty($fnlarray2)){
			 $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='offer_add';
            $data['withoffer']='0';
			$i=0;
			foreach($fnlarray2 as $rowarr)
			{
				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
              $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr->original_logo_logoimage:"");
                 $data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr->poll_original:(($rowarr->original_logo!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr->original_logo:"");
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
					$datarr['osinfo']=$jsondata->platform_filter;
					$datarr['gender']='';
					$datarr['age_group']='';
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
				$datarr['usertype']='2';
				 $this->add_cp_offerlisting_analytics($datarr);
				  $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 
				
			}
		}
		else{
			 $data['resultCode']='0';
            $data['resultMessage']='No Offer on this location';
			
		}
		//$this->captive_portal_impression($jsondata);
			return $data;
			
			
			
			
			
			
			
			
		}

		
          
        
    }
	
	
	public function get_sponsered_listing_data($jsondata)
    {
		$query=$this->DB2->query("select latitude,longitude from wifi_location where id='".$jsondata->locationId."'");
		if($query->num_rows()>0)
		{
			$latarr=$query->row_array();
			$userlat=$latarr['latitude'];
			$userlon=$latarr['longitude'];
			
			$query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.brand_id,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter
			,co.active_between,co.image_original,co.offer_title,co.poll_brand_title,co.poll_original,co.poll_logo,co.`offer_type`,co.`voucher_expiry`,co.total_voucher,co.voucher_code_used,
co.image_small,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,co.`captcha_text`,co.`captcha_accuracy_level`,co.`redirect_url` FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id)
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where (co.offer_type='banner_add' or co.offer_type='flash_add') and co.is_mtnl_default_ad='0' and cpl.location_id='".$jsondata->locationId."'  AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW() and co.brand_id!='145' order by co.cp_offer_id  desc limit 0,10");



			$query1=$this->DB2->query("
SELECT (
6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( $userlat ) ) * COS( RADIANS( s.longitude ) - RADIANS( $userlon) ) + SIN( RADIANS( $userlat ) ) * SIN( RADIANS( s.latitude ) ) )
) AS distance, co.brand_id, co.cp_offer_id,co.offer_type,co.offer_desc,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_title, co.redirect_url, co.video_path, co.campaign_name,co.campaign_day_to_run,co.platform_filter,co.total_voucher,co.voucher_code_used,
co.active_between, co.campaign_end_date, co.image_original, co.image_small, b.brand_name, b.original_logo, b.original_logo_logoimage, 
s.city, s.geo_address, s.latitude, s.longitude, s.store_name
FROM cp_offers co
INNER JOIN cp_offers_store cps ON ( co.cp_offer_id = cps.cp_offer_id )
INNER JOIN store s ON ( cps.store_id = s.store_id )
INNER JOIN brand b ON ( co.brand_id = b.brand_id )
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where (co.offer_type='banner_add' or co.offer_type='flash_add') and co.is_mtnl_default_ad='0' and cpl.location_id='".$jsondata->locationId."'  AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW() and co.brand_id!='145' order by co.cp_offer_id desc limit 0,10
");


			$array1=array();
			$fnlarray2=array();
				
			foreach($query1->result() as $val)
			{
				$dayarr = array_filter(explode(",", $val->campaign_day_to_run));
				$platformarr=explode(",",$val->platform_filter);
				$timearr=array();
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
					
				//filter for day
				 if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
					if($budget_allocate==true && $filter=true){
					$array1[$val->cp_offer_id]=$val;
					}
				$array1[$val->cp_offer_id]=$val;
			}
			
			
			
				
			$x=0;
			foreach($query->result() as $val1)
			{
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
				$dayarr = array_filter(explode(",", $val1->campaign_day_to_run));
				$platformarr=explode(",",$val1->platform_filter);
				$timearr=array();
				
				
					
				//filter for day
				 if ($val1->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val1->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
			
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
			
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val1->cp_offer_id);
					if($budget_allocate==true && $filter=true){
						
											if($x<5){
							if(array_key_exists($val1->cp_offer_id,$array1))
							{
								$fnlarray2[]=$array1[$val1->cp_offer_id];
							}
							else{
								$fnlarray2[]=$val1;
							}
							}
							$x++;
					}
				
				
			}
			

		if(!empty($fnlarray2)){
			 $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='offer_add';
            $data['withoffer']='0';
			$i=0;
			$count=0;
			foreach($fnlarray2 as $rowarr)
			{
				
				//$closesin = $time_left1;
				if($count<5){
				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
            $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr->original_logo_logoimage:"");
			$data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr->poll_original:(($rowarr->original_logo!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr->original_logo:"");
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				$data['offerdata'][$i]['promotion_desc']=$rowarr->offer_desc;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				  $data['offerdata'][$i]['offer_type']=$rowarr->offer_type;
				  $data['offerdata'][$i]['voucher_expiry']=$rowarr->voucher_expiry;
				   $data['offerdata'][$i]['geo_address']=isset($rowarr->geo_address)?$rowarr->geo_address:'';
				    $data['offerdata'][$i]['store_name']=isset($rowarr->store_name)?$rowarr->store_name:'';
				  if($rowarr->total_voucher!=0)
			{
				 $data['offerdata'][$i]['voucherleft']=$rowarr->total_voucher-$rowarr->voucher_code_used;
				
			}else{
				 $data['offerdata'][$i]['voucherleft']='';
			}
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
					$datarr['osinfo']=$jsondata->platform_filter;
					$datarr['gender']='';
					$datarr['age_group']='';
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
				$datarr['usertype']='2';
				
				  $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 $this->add_cp_sponsorlisting_analytics($datarr);

				}
				 $count++;
			}
			//echo "<pre>"; print_R($data);die;
		}
		else{
			
				$query1=$this->DB2->query("select Provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarr1=$query1->row_array();
			if($rowarr1['Provider_id']==14)
			{
				//echo 'ssssss'; die;
				$query=$this->DB2->query("SELECT co.cp_offer_id, co.offer_type, co.campaign_day_to_run
						FROM `cp_offers` co
						WHERE co.offer_type = 'banner_add' and co.is_mtnl_default_ad='1'

						ORDER BY co.cp_offer_id DESC limit 1");
			}
			else{
				$query=$this->DB2->query("SELECT co.cp_offer_id, co.offer_type, co.campaign_day_to_run
						FROM `cp_offers` co
						WHERE co.brand_id = '145'
						AND co.offer_type = 'banner_add'

						ORDER BY co.cp_offer_id ASC limit 1");
			}

              
                $rowarr=$query->row_array();
                $data=array();
                $dataarr=$this->get_image_ad_data($rowarr['cp_offer_id']);


                $data['resultCode']=1;
                $data['offerdata'][0]['cp_offer_id']=$rowarr['cp_offer_id'];
                $data['offerdata'][0]['offer_type']=$dataarr['rewradtype'];
                $data['offerdata'][0]['brand_name']=strtoupper($dataarr['brand_name']);
                $data['offerdata'][0]['brand_logo_image']=$dataarr['brand_logo_image'];
                $data['offerdata'][0]['promotion_name']=$dataarr['promotion_name'];
                $data['offerdata'][0]['promotion_desc']=$dataarr['promotion_desc'];

             //   $this->captive_portal_impression($jsondata);
				
				
				 $datarr=array();
                        $datarr['cp_offer_id']=$rowarr->cp_offer_id;
                        $datarr['location_id']=$jsondata->locationId;
                        $datarr['user_id']=$jsondata->userId;
                        $datarr['platform']=$jsondata->via;
                        $datarr['osinfo']=$jsondata->platform_filter;
                        $datarr['gender']='';
                        $datarr['age_group']='';
                        $datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
                        $datarr['usertype']='2';

                      //  $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
                        $this->add_cp_sponsorlisting_analytics($datarr);
			
			
		}
            $this->captive_portal_impression($jsondata);
			return $data;
			
		}
        else{
            $data['resultCode']='0';
            $data['resultMessage']='Invalid location id';
            $this->captive_portal_impression($jsondata);
            return $data;
        }
		
          
        
    }
	
	
	public function app_download_data($jsondata)
	{
		$userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
		$query=$this->DB2->query("select latitude,longitude from wifi_location where id='".$jsondata->locationId."'");
		if($query->num_rows()>0)
		{
			$latarr=$query->row_array();
			$userlat=$latarr['latitude'];
			$userlon=$latarr['longitude'];
			
			$query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.android_appurl,co.ios_appurl,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter,
			co.active_between,co.image_original,co.offer_title,co.poll_brand_title,co.poll_original,co.poll_logo,co.incentive_desc,co.offer_desc,co.`offer_type`,co.`voucher_expiry`,
co.image_small,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,co.`captcha_text`,co.`captcha_accuracy_level`,co.`redirect_url` FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id)
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where co.offer_type='app_install_add' and co.is_mtnl_default_ad='0' and cpl.location_id='".$jsondata->locationId."'  AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW() order by co.cp_offer_id desc LIMIT 0 , 5");



			$query1=$this->DB2->query("
SELECT (
6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( $userlat ) ) * COS( RADIANS( s.longitude ) - RADIANS( $userlon) ) + SIN( RADIANS( $userlat ) ) * SIN( RADIANS( s.latitude ) ) )
) AS distance, co.brand_id, co.cp_offer_id,co.android_appurl,co.poll_brand_title,co.poll_original,co.poll_logo,co.incentive_desc,co.ios_appurl,co.offer_type,co.offer_title,co.offer_desc, co.redirect_url, co.video_path, co.campaign_name,co.campaign_day_to_run,co.platform_filter,co.active_between, co.campaign_end_date, co.image_original, co.image_small, b.brand_name, b.original_logo, b.original_logo_logoimage, s.city, s.geo_address, s.latitude, s.longitude
FROM cp_offers co
INNER JOIN cp_offers_store cps ON ( co.cp_offer_id = cps.cp_offer_id )
INNER JOIN store s ON ( cps.store_id = s.store_id )
INNER JOIN brand b ON ( co.brand_id = b.brand_id )
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where co.offer_type='app_install_add' and co.is_mtnl_default_ad='0' and cpl.location_id='".$jsondata->locationId."'  AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW() order by co.cp_offer_id desc LIMIT 0 , 5
");

			$array1=array();
			$fnlarray2=array();
			foreach($query1->result() as $val)
			{
				$dayarr = array_filter(explode(",", $val->campaign_day_to_run));
				$platformarr=explode(",",$val->platform_filter);
				$timearr=array();
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
					
				//filter for day
				 if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
					if($budget_allocate==true && $filter=true){
					$array1[$val->cp_offer_id]=$val;
					}
				$array1[$val->cp_offer_id]=$val;
			}
		
			$x=0;
			foreach($query->result() as $val1)
			{
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
				$dayarr = array_filter(explode(",", $val1->campaign_day_to_run));
				$platformarr=explode(",",$val1->platform_filter);
				$timearr=array();
				
				
					
				//filter for day
				 if ($val1->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
					
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val1->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val1->cp_offer_id);
					if($budget_allocate==true && $filter=true){
											if($x<5){
							if(array_key_exists($val1->cp_offer_id,$array1))
							{
								
								$fnlarray2[]=$array1[$val1->cp_offer_id];
							}
							else{
							
								$fnlarray2[]=$val1;
							}
							}
							$x++;
					}
                //app download seen array

                $query2=$this->DB2->query("select * from cp_dashboard_analytics where user_id='".$jsondata->userId."' and is_downloaded='1'");
                if($query2->num_rows()>0)
                {
                    $appseen_arr=array();
                    foreach($query2->result() as $vald)
                    {
                        if(in_array($vald->cp_offer_id,$fnlarray2))
                        {
                            $appseen_arr[]=$vald->cp_offer_id;
                        }
                    }
                    $fnlarray2=array_diff($fnlarray2,$appseen_arr);
                }
				
				
			}
				
		if(!empty($fnlarray2)){
			 $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='offer_add';
            $data['withoffer']='0';
			$i=0;
			foreach($fnlarray2 as $rowarr)
			{
				
				//$closesin = $time_left1;

				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
             $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr->original_logo_logoimage:"");
              $data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr->poll_original:(($rowarr->original_logo!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr->original_logo:"");
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				  $data['offerdata'][$i]['offer_type']=$rowarr->offer_type;
				  $data['offerdata'][$i]['voucher_expiry']=$rowarr->voucher_expiry;
				    $data['offerdata'][$i]['androidurl']=$rowarr->android_appurl;
					  $data['offerdata'][$i]['ios_appurl']=$rowarr->ios_appurl;
					   $data['offerdata'][$i]['app_title']=$rowarr->campaign_name;
					    $data['offerdata'][$i]['app_desc']=$rowarr->incentive_desc;
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				 $datarr['user_id']=$jsondata->userId;
				$datarr['platform']=$jsondata->via;
				$datarr['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
				$datarr['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
				$datarr['osinfo']=$jsondata->platform_filter;
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";;
				$datarr['usertype']='1';
				
				  //$this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 $this->add_cp_analytics($datarr);
			}
			//echo "<pre>"; print_R($data);die;
		}
		else{
			
				$query1=$this->DB2->query("select Provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarr1=$query1->row_array();
			if($rowarr1['Provider_id']==14)
			{
				//echo 'ssssss'; die;
				 $query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.android_appurl,co.ios_appurl,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter,
			co.active_between,co.image_original,co.incentive_desc,co.offer_title,co.offer_desc,co.`offer_type`,co.`voucher_expiry`,
co.image_small,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,co.`captcha_text`,co.`captcha_accuracy_level`,co.`redirect_url` FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id)
 where co.offer_type='app_install_add' and co.is_mtnl_default_ad='1' order by co.cp_offer_id desc limit 1");
			}
			else{
				 $query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.android_appurl,co.ios_appurl,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter,
			co.active_between,co.image_original,co.incentive_desc,co.offer_title,co.offer_desc,co.`offer_type`,co.`voucher_expiry`,
co.image_small,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,co.`captcha_text`,co.`captcha_accuracy_level`,co.`redirect_url` FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id)
 where co.offer_type='app_install_add' and co.is_mtnl_default_ad='0' and co.brand_id=145 order by co.cp_offer_id desc limit 1");
			}
$i=0;
foreach($query->result() as $rowarr)
			{
				
				//$closesin = $time_left1;

				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
              $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr->original_logo_logoimage:"");
                    $data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr->poll_original:(($rowarr->original_logo!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr->original_logo:"");
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				  $data['offerdata'][$i]['offer_type']=$rowarr->offer_type;
				  $data['offerdata'][$i]['voucher_expiry']=$rowarr->voucher_expiry;
				    $data['offerdata'][$i]['androidurl']=$rowarr->android_appurl;
					  $data['offerdata'][$i]['ios_appurl']=$rowarr->ios_appurl;
					   $data['offerdata'][$i]['app_title']=$rowarr->campaign_name;
					    $data['offerdata'][$i]['app_desc']=$rowarr->incentive_desc;
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				 $datarr['user_id']=$jsondata->userId;
				$datarr['platform']=$jsondata->via;
				$datarr['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
				$datarr['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
				$datarr['osinfo']=$jsondata->platform_filter;
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";;
				$datarr['usertype']='1';
				
				 // $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 $this->add_cp_analytics($datarr);
			}
			
			
		}
			return $data;
			
		}
		
	}
	
	
public function offer_ad_detail($jsondata)
	{
		    $userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
		$query=$this->DB2->query("SELECT (
6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( 28.5376320000 ) ) * COS( RADIANS( s.longitude ) - RADIANS( 77.2282860000) ) + SIN( RADIANS( 28.5376320000 ) ) * SIN( RADIANS( s.latitude ) ) )
) AS distance, co.brand_id, co.cp_offer_id,co.poll_brand_title,co.poll_original,co.poll_logo, co.redirect_url,co.voucher_expiry,co.flash_start_date, co.offer_title,co.image_medium,co.video_path,co.term_and_condition, co.campaign_name,
 co.campaign_end_date, co.image_original,co.offer_type,co.total_voucher,co.voucher_code_used, co.image_small,
 b.brand_name, b.original_logo, b.original_logo_logoimage, s.city, s.geo_address, s.latitude, s.longitude
FROM cp_offers co
INNER JOIN cp_offers_store cps ON ( co.cp_offer_id = cps.cp_offer_id )
INNER JOIN store s ON ( cps.store_id = s.store_id )
INNER JOIN brand b ON ( co.brand_id = b.brand_id )
inner join cp_offers_location cpl on (co.cp_offer_id=cpl.cp_offer_id) where cpl.location_id = '".$jsondata->locationId."'
				AND co.cp_offer_id ='".$jsondata->offer_id."'");
if($query->num_rows()>0)
{
	$i=0;
	
	$dataobj=$query->result();
	foreach($query->result() as $vald)
	{
			$is_close = '';
					$current_time =  date("Y-m-d H:i:s");
					$start_date = new DateTime($vald->flash_start_date);
					$since_start = $start_date->diff(new DateTime($current_time));
					$year 	=  $since_start->y;
					$month 	= $since_start->m;
					$day 	=  $since_start->d;
					$hr     =  $since_start->h;
					$min  	=  $since_start->i;
					$sec 	=  $since_start->s;
					$time_left = '';
					if($year > 0){
						$time_left = $year.'y : '.$month.'m: '. $day.'d';
					}elseif($month > 0){
						$time_left = $month.'m : '.$day.'d: '. $hr.'hr';
					}elseif($day > 0){
						$time_left = $day.'d : '.$hr.'h: '. $min.'m';
					}elseif($hr > 0){
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}else{
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}
					//check flash sale start or not
					$isopen = '';
					$startsin = '';
					$closesin = '';
					if(strtotime($current_time) > strtotime($vald->flash_start_date)){
						$isopen = '1';
						$statr =  $vald->campaign_end_date;
						$sale_end = date('Y-m-d H:i:s', strtotime($statr));
						if(strtotime($current_time) > strtotime($sale_end)){
					
							$is_close = '1';
						}else{
							$current_time = new DateTime($current_time);
							$since_end = $current_time->diff(new DateTime($sale_end));
							$time_left1 = '';
							$time_left1 = $since_end->h.'h: '.$since_end->i.'m: '. $since_end->s.'s';
							$closesin = $time_left1;
						
						}

					}else{
						$isopen = '0';
						$startsin = $time_left;
					}
		
		
		
		  $data['resultCode']='1';
            $data['resultMessage']='Success';
           
            $data['withoffer']='0';
              $data['brand_name']=($dataobj[0]->poll_brand_title!='')?strtoupper($dataobj[0]->poll_brand_title):strtoupper($dataobj[0]->brand_name);
			if($dataobj[0]->total_voucher!=0)
			{
				 $data['voucherleft']=$dataobj[0]->total_voucher-$dataobj[0]->voucher_code_used;
				
			}else{
				 $data['voucherleft']='';
			}
			if($dataobj[0]->offer_type=="offer_add")
			{
				 $data['rewradtype']='offer_detail';
				 $data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
				
			}else if($dataobj[0]->offer_type=="flash_add"){
				$data['rewradtype']='flash_detail';
				$data['is_start']=$isopen;
				$data['opens_in']=$startsin;
				$data['closes_in']=$closesin;
			}
			else if($dataobj[0]->offer_type=="banner_add")
			{
				$data['rewradtype']='sponsor_detail';
				$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
			else{
					$data['rewradtype']='';
					$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
            $data['offers_small_image']=($dataobj[0]->image_small='')?CAMPAIGN_IMAGEPATH."small/".$dataobj[0]->image_small:"";
             $data['offers_original_image']=($dataobj[0]->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$dataobj[0]->image_original:"";
			 $data['offers_medium_image']=($dataobj[0]->image_medium!='')?CAMPAIGN_IMAGEPATH."medium/".$dataobj[0]->image_medium:"";
              $data['brand_logo_image']=($dataobj[0]->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$dataobj[0]->poll_logo:(($dataobj[0]->original_logo_logoimage!='')?REWARD_IMAGEPATH."brand/logo/logo/".$dataobj[0]->original_logo_logoimage:"");
                $data['brand_original_image']=($dataobj[0]->poll_original!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$dataobj[0]->poll_original:(($dataobj[0]->original_logo!='')?REWARD_IMAGEPATH."brand/logo/original/".$dataobj[0]->original_logo:"");
              $data['validTill']='';
              
			  $data['voucher_expiry']=$dataobj[0]->voucher_expiry;
             
              $data['promotion_name']=$dataobj[0]->offer_title;
			  $data['temcond']=$dataobj[0]->term_and_condition;
			  $data['storedata'][$i]['address']=$vald->geo_address;
			  $data['storedata'][$i]['distance']=$vald->distance;
			  
			  
			   $datarr=array();
				 $datarr['cp_offer_id']=$dataobj[0]->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
		
		$i++;
	}
	
	
	
	
}
else{
	$query1=$this->DB2->query("SELECT co.brand_id, co.cp_offer_id,co.term_and_condition,co.total_voucher,co.voucher_code_used, 
	co.redirect_url, co.video_path,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_type,co.offer_type,co.flash_start_date, co.campaign_name,co.voucher_expiry,co.offer_title, co.campaign_end_date,co.image_medium, co.image_original, co.image_small,
	b.brand_name, b.original_logo, b.original_logo_logoimage
FROM cp_offers co
INNER JOIN brand b ON ( co.brand_id = b.brand_id )
INNER JOIN cp_offers_location cpl ON ( co.cp_offer_id = cpl.cp_offer_id )
			WHERE cpl.location_id = '".$jsondata->locationId."'
				AND co.cp_offer_id ='".$jsondata->offer_id."'");
				$rowarr=array();
				$rowarr=$query1->row_array();
				$is_close = '';
					$current_time =  date("Y-m-d H:i:s");
					$start_date = new DateTime($rowarr['flash_start_date']);
					$since_start = $start_date->diff(new DateTime($current_time));
					$year 	=  $since_start->y;
					$month 	= $since_start->m;
					$day 	=  $since_start->d;
					$hr     =  $since_start->h;
					$min  	=  $since_start->i;
					$sec 	=  $since_start->s;
					$time_left = '';
					if($year > 0){
						$time_left = $year.'y : '.$month.'m: '. $day.'d';
					}elseif($month > 0){
						$time_left = $month.'m : '.$day.'d: '. $hr.'hr';
					}elseif($day > 0){
						$time_left = $day.'d : '.$hr.'h: '. $min.'m';
					}elseif($hr > 0){
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}else{
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}
					//check flash sale start or not
					$isopen = '';
					$startsin = '';
					$closesin = '';
					if(strtotime($current_time) > strtotime($rowarr['flash_start_date'])){
						
						$isopen = '1';
						$statr =  $rowarr['campaign_end_date'];
						$sale_end = date('Y-m-d H:i:s', strtotime($statr));
						if(strtotime($current_time) > strtotime($sale_end)){
							$is_close = '1';
							
						}else{
							$current_time = new DateTime($current_time);
							$since_end = $current_time->diff(new DateTime($sale_end));
							$time_left1 = '';
							$time_left1 = $since_end->h.'h: '.$since_end->i.'m: '. $since_end->s.'s';
							$closesin = $time_left1;
							
						}

					}else{
						$isopen = '0';
						$startsin = $time_left;
					}
				
				 //$rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            
            $data['withoffer']='0';
				if($rowarr['total_voucher']!=0)
			{
				 $data['voucherleft']=$rowarr['total_voucher']-$rowarr['voucher_code_used'];
				
			}else{
				 $data['voucherleft']='';
			}
				if($rowarr['offer_type']=="offer_add")
			{
				 $data['rewradtype']='offer_detail';
				 $data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
				
			}else if($rowarr['offer_type']=="flash_add"){
				$data['rewradtype']='flash_detail';
				$data['is_start']=$isopen;
				$data['opens_in']=$startsin;
				$data['closes_in']=$closesin;
			}
			else if($rowarr['offer_type']=="banner_add")
			{
				$data['rewradtype']='sponsor_detail';
				$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
			else{
					$data['rewradtype']='';
					$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
             $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
			 $data['offers_medium_image']=($rowarr['image_medium']!='')?CAMPAIGN_IMAGEPATH."medium/".$rowarr['image_medium']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
     $data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
              $data['validTill']='';
              $data['validTill_fromTime']='';
			   $data['voucher_expiry']=$rowarr['voucher_expiry'];
              $data['store_count']='';
              $data['video_url']='';
              $data['promotion_name']=$rowarr['offer_title'];
			   $data['temcond']=$rowarr['term_and_condition'];
			  $data['storedata']=array();
			  
              $data['poll_question']='';
              $data['poll_option']=array();
			  
			   $datarr=array();
				 $datarr['cp_offer_id']=$rowarr['cp_offer_id'];
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
					 $datarr['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
					 $datarr['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
					 $datarr['osinfo']=$jsondata->platform_filter;
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
				$datarr['usertype']='1';
		
	
}

if($data['rewradtype']=="offer_detail")
			{
				$this->add_cp_offerdetail_analytics($datarr);
				
			}else if($data['rewradtype']=="flash_detail"){
				$this->add_cp_sponsordetail_analytics($datarr);
			}
			else if($data['rewradtype']=="sponsor_detail")
			{
				$this->add_cp_sponsordetail_analytics($datarr);
			}
 //$this->add_cp_offerdetail_analytics($datarr);
return $data;

		
	}
	
	
    
    public function get_redeemable_offer_data($offer_id)
    {
         $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.voucher_expiry,co.campaign_name,co.offer_title,co.`campaign_end_date`,co.image_original,
            co.image_small,br.brand_name,br.`original_logo`,br.original_logo_logoimage,co.redirect_url,co.poll_brand_title,co.poll_original,co.poll_logo,
            (SELECT COUNT(*) FROM `cp_offers_store` WHERE cp_offer_id='".$offer_id."') AS store_count
            FROM `cp_offers` co
             INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
       
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='redeem';
            $data['withoffer']='0';
			$data['brand_id']=$rowarr['brand_id'];
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
             $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
             $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
              $data['validTill']=$rowarr['voucher_expiry'];
              $data['validTill_fromTime']='';
              $data['store_count']=$rowarr['store_count'];
              $data['promotion_name']=$rowarr['offer_title'];
              $data['poll_question']="";
              $data['poll_option']=array();
              $data['captch_text']="";
              $data['captcha_authenticity']="";
			   $data['offer_id']=$rowarr['cp_offer_id'];
              $data['url']=$rowarr['redirect_url'];
        }
 else {
       $data['resultCode']='0';
            $data['resultMessage']='No Image offer';
     
 }
       return $data;
    }
	
	 public function get_cost_per_lead_add_data($offer_id, $location_type = null)
    {
        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.brand_id,co.voucher_expiry,co.campaign_name,co.offer_title,co.`campaign_end_date`,co.image_original,
            co.image_small,br.brand_name,br.`original_logo`,br.original_logo_logoimage,co.redirect_url,co.poll_brand_title,co.poll_original,co.poll_logo,
            (SELECT COUNT(*) FROM `cp_offers_store` WHERE cp_offer_id='".$offer_id."') AS store_count
            FROM `cp_offers` co
             INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'cost_per_lead_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='cost_per_lead_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']=$rowarr['voucher_expiry'];
            $data['validTill_fromTime']='';
            $data['store_count']=$rowarr['store_count'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
        return $data;
    }
	
	public function get_offertype($offerid)
	{
		
		$rowarr="";
		$query=$this->DB2->query("select offer_type,brand_id,cp_offer_id from cp_offers where cp_offer_id='".$offerid."' ");
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			
		}
		
		return $rowarr;
		
	}
	
    
    public function add_cp_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],"is_redirected"=>0,
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics",$tabledata);
    }
	
	 public function add_cp_offerlisting_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_offer_listing",$tabledata);
		
    }
	
	public function add_cp_sponsorlisting_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_bannerflash_listing",$tabledata);
    }
	
	
	 public function add_cp_offerdetail_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender'])
			,"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_offer_detail",$tabledata);
    }
	
	public function add_cp_sponsordetail_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender'])
			,"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_bannerflash_detail",$tabledata);
    }
    
    
    public function redirect_url_api($jsondata)
    {
		if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        if(isset($jsondata->view_counter) && $jsondata->view_counter == '1'){
            $this->DB2->query("update cp_dashboard_analytics set is_redirected='1' where cp_offer_id='".$jsondata->offer_id."' "
                . "and location_id='".$jsondata->locationId."' and user_id='".$jsondata->userId."' order by id desc limit 1");
        }

        
          $data['resultCode']='1';
            $data['resultMessage']='Success';
            return $data;
      
    }
	
	  public function captcha_accurate_api($jsondata)
    {
		if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        $this->DB2->query("update cp_dashboard_analytics set captcha_verified='1' where cp_offer_id='".$jsondata->offer_id."' "
                . "and location_id='".$jsondata->locationId."' and user_id='".$jsondata->userId."' order by id desc limit 1");
        
          $data['resultCode']='1';
            $data['resultMessage']='Success';
            return $data;
      
    }
    
    public function poll_answer($jsondata)
    {
		
	if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        $tabledata=array("question_id"=>$jsondata->ques_id,"option_id"=>$jsondata->option_id,"user_id"=>$jsondata->userId,
            "location_id"=>$jsondata->locationId,"cp_offer_id"=>$jsondata->offer_id,"answered_on"=>date("Y-m-d H:i:s"));
        $this->DB2->insert("cp_poll_answer",$tabledata);
        if($this->DB2->affected_rows()>0)
        {
                $data['resultCode']='1';
            $data['resultMessage']='Success';
        }
        else{
            $data['resultCode']='0';
            $data['resultMessage']='Fail';
        }
        return $data;
    }
    
    
    public function redeem_offer($jsondata)
    {
		if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        $query=$this->DB2->query("SELECT * FROM `cp_offer_reddem` WHERE 
		`cp_offer_id`='".$jsondata->offer_id."' AND `user_id`='".$jsondata->userId."'");
			$vouchercode	="";
  $userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
  $gender='';
  $age_group='';
  $platform=$jsondata->via;
  $osinfo=$jsondata->platform_filter;
  $macid=$jsondata->macid;
  if(is_array($userdata))
  {
	   $gender=$userdata[0]['gender'];
	   $age_group=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
  }

        if($query->num_rows()==0){
			
                $query1=$this->DB2->query("select voucher_type,voucher_static_type,offer_type,voucher_expiry,campaign_name,voucher_code,total_voucher,voucher_code_used from cp_offers where cp_offer_id='".$jsondata->offer_id."'");
                  
                $rowarr=$query1->row_array();
                if($rowarr['voucher_code_used']<$rowarr['total_voucher'] || $rowarr['total_voucher']==0)
                {
                    if($rowarr['voucher_type']=="static")
                    {
						if($rowarr['voucher_static_type']=="multiple")
						{
							$couponcode=$this->getvoucher_staticmultiple($jsondata->offer_id);
							if($couponcode=="0")
							{
								$data['resultCode']='0';
								$data['resultMessage']='Voucher finish'; 
								return $data;
							}
							else{
								$vouchercode=$couponcode;	
								$this->DB2->query("update cp_offer_coupon set is_used ='1',used_on=now()
					 WHERE cp_offer_id = '$jsondata->offer_id' and coupon_code='$couponcode'");
							}
						}else{
						$vouchercode=$rowarr['voucher_code'];	
						}
                        
                    }
                     else {
                     $vouchercode=$this->randon_voucher();
                     
                     }
                     $update_voucher_redeem_counter = $this->DB2->query("update cp_offers set voucher_code_used =
					(voucher_code_used + 1) WHERE cp_offer_id = '$jsondata->offer_id'");
				
					
					$data['couponCode'] = $vouchercode;
					$insert_redeem = $this->DB2->query("insert into cp_offer_reddem (cp_offer_id, user_id,
							coupon_code, redeemed_on,location_id,gender,platform,osinfo,macid,age_group) VALUES ('$jsondata->offer_id', '$jsondata->userId',
							'$vouchercode', now(),'$jsondata->locationId','$gender','$platform','$osinfo','$macid','$age_group')");
						if($rowarr['offer_type']=="banner_add")	
						{
							 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'banner_add_redeem');
						}
						else if($rowarr['offer_type']=="flash_add")
						{
							 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'flash_add_redeem');
						}
						else if($rowarr['offer_type']=="redeemable_add")
						{
							 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'redeemable_add_redeem');
						}
							
                     
                     $data_msg = array("is_login" => '1', 'login_userid' => $jsondata->userId,
						"apikey" => $jsondata->apikey, "offerId" => $jsondata->offer_id, "type" => 'sms',"store_location" => $jsondata->locationId);
					
					$data_msg = json_encode($data_msg);
					$this->coupon_code_send_wifioffer(json_decode($data_msg));

					$data_msg = array("is_login" => '1', 'login_userid' => $jsondata->userId,
						"apikey" => $jsondata->apikey, "offerId" => $jsondata->offer_id, "type" => 'email', "store_location" => $jsondata->locationId);
					
					$data_msg = json_encode($data_msg);
					$this->coupon_code_send_wifioffer(json_decode($data_msg));
                     $data['resultCode'] = 1;
                     $data['resultMessage'] = 'Success';
					 $data['couponCode'] = $vouchercode;
					 $data['voucher_expiry'] = $rowarr['voucher_expiry'];
            }
                else {
					
                    $data['resultCode']='0';
                          $data['resultMessage']='Voucher finish'; 
               }
     
 
            
        }
        else
        {
			
            $data['resultCode']='0';
            $data['resultMessage']='Already Purchased'; 
        }
		return $data;
    }
	
	
	public function getvoucher_staticmultiple($cp_offer_id)
	{
		$query=$this->DB2->query("select coupon_code from cp_offer_coupon where cp_offer_id='".$cp_offer_id."' and is_used='0'");
		if($query->num_rows()>0)
		{
			$couponcodearr=$query->result();
			return $couponcodearr[0]->coupon_code;
		}
		else{
			return '0';
		}
		
	}
    
    
    public function randon_voucher(){
		$random_number = mt_rand(1000000, 9999999);
		$query = $this->DB2->query("select voucher_code from cp_voucher WHERE voucher_code = '$random_number'");
		if($query->num_rows() > 0){
			$this->randon_voucher();
		}else{
			return $random_number;
		}
	}
        
        public function coupon_code_send_wifioffer($jsondata){
		$user_id = $jsondata->login_userid;
		$flahs_deal_id = $jsondata->offerId;
		$type = $jsondata->type;
		$store_address =  $jsondata->store_location;
		$data = array();
		$query = $this->DB2->query("select cor.coupon_code, co.campaign_name, co.voucher_expiry, b.brand_name, b
		.original_logo from cp_offer_reddem as cor INNER JOIN cp_offers as co on
		(cor.cp_offer_id = co.cp_offer_id)  INNER JOIN
		brand as b ON (co.brand_id = b.brand_id) WHERE cor.cp_offer_id = '$flahs_deal_id' AND cor.user_id =
		'$user_id'");
		

		if($query->num_rows() > 0){
			$row = $query->row_array();
		
			$coupon_code = $row['coupon_code'];
			/*$msg = '';
			$msg .= "SHOUUT FLASH SALE VOUCHER";
			$msg .= 'Brand Name:-'.$row['brand_name'];
			$msg .= 'Coupon Code:-'.$coupon_code.'';
			$msg .= 'Expires :-'.$coupon_code.'';
			$msg .= ". HOW TO REDEEM THIS COUPON.  \r";
			$msg .= "PRESENT THE COUPON CODE TO THE STORE MANAGER";*/
			$msg = urlencode("SHOUUT WIFI OFFER VOUCHER").'%0a'.urlencode('Brand Name:-'.$row['brand_name']).'%0a'.
				urlencode('Voucher code:-'.$coupon_code).'%0a'.urlencode('Expires :-'.$row['voucher_expiry']).'%0a'
				.urlencode("TO REDEEM THIS COUPON.").'%0a'.urlencode("PRESENT THE COUPON CODE TO THE STORE MANAGER.");
			// get user detail
			$this->mongo_db->select(array('mobile', 'email', 'screen_name'));
			$this->mongo_db->where(array('_id' => new MongoId($user_id)));
			$result = $this->mongo_db->get('sh_user');
			

			if($type == "sms"){
				$this->send_via_sms($result[0]['mobile'], $msg);
			}else{
				$this->send_via_email($result[0]['email'], $row['original_logo'], $row['brand_name'],
					$result[0]['screen_name'], $row['campaign_name'], $coupon_code, $row['voucher_expiry'], $store_address);
			}
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';

		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No deal id found';
		}
		return $data;
	}
        public function shouut_sms_link($jsondata)
		{
			$msg = urlencode("SHOUUT APP DOWNLOAD LINK").'%0a'.urlencode('ANDROID:- https://play.google.com/store/apps/details?id=com.shouut.discover').'%0a'.
				urlencode('IOS:- https://itunes.apple.com/in/app/shouut/id1089501174').'%0a';
				$this->send_via_sms($jsondata->mobile,$msg);
				$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
			return $data;
		}
		
    public function send_via_sms($mobile, $msg){
		//$msgdata = urlencode($msg);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://173.45.76.226:81/send.aspx?username=shouut&pass=Sh0uutg1ant&route=trans1&senderid=SHOUUT&numbers=".$mobile."&message=".$msg);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
		if($statusCode==200){
			return 1;
		}
		curl_close($ch);
	}

	public function send_via_email($email, $image, $brand_name, $username, $desc, $coupon_code, $voucher_expiry,
								   $store_location){
		$subject = "SHOUUT FLASH SALE: ".$brand_name." VOUCHER";
		/*$headers = 'From: SHOUUT <flashsale@shouut.com>' . "\r\n";
		//$headers .= 'Cc: aashish@shouut.com'. "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";*/
		$message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg.jpg);
                        background-repeat:no-repeat; background-position:top center; width:100%">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px"><strong>Hi '.$username.',</strong></h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Congrats !
                         You have successfully redeemed a Shouut Flash voucher for
                          <strong>'.$brand_name.'</strong></p>
                        <p style="margin-bottom:5px; margin-top:10px; font-weight:normal; font-size:14px;">Offer-
                            '.$desc.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Store
                         location- '.$store_location.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        code- '.$coupon_code.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        expires on- '.$voucher_expiry.'</p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">To avail this OFFER, please present this VOUCHER at the time of the payment in the STORE/VENUE.</p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"> For any
                         query  : Please write to us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>
                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT FLASH SALE</h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';
		//echo $message;die;
		//mail($email,$subject,$message,$headers);
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "flashsale@shouut.com";
		$mail->Password = "shouut@#123";
		$mail->setFrom('flashsale@shouut.com', 'Flash Sale');
		$mail->addAddress($email, $username);
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		$mail->AltBody = '';
		$mail->send();
		return 1;

	}
	
	
	  public function user_detail($user_id, $api_key){
		  
	$query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
	
	$num = count($query);
	if($num > 0){
	    return $query;
	}else{
	    return 0;
	}
    }

	public function get_age_group($mobileno)
	{
		 $CI = &get_instance();
//setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('db2', TRUE);
        $query = $this->db2->query("SELECT age_group FROM `wifi_register_users` wru1 WHERE mobileno='".$mobileno."'");
		$resultdata=$query->result();
		if($query->num_rows>0)
		{
			if($resultdata[0]->age_group!="" || $resultdata[0]->age_group!=0){
				return $resultdata[0]->age_group;
			}
			else{
					return 'all';
			}
			
		}
		else{
			return 'all';
		}
		
	}
	public function timearr($timeval){
			
$arrtime=explode(",",$timeval);
$fnltimearr=array();

foreach($arrtime as $val)
{
$hifentime=explode("-",$val);
$fnltimearr[]=$hifentime[0];
for($i=0;$i<1; $i++)
{
if($hifentime[0]<$hifentime[1] || $hifentime[0]==22){
$midtime=$hifentime[0]+1;
if(strlen($midtime)==1){
	$fnltimearr[]="0".$midtime;
}
else{
	$fnltimearr[]=$midtime;
}

$i++;
}
$fnltimearr[]=$hifentime[1];
}
}
return array_unique($fnltimearr); 
		
	}
	public function captive_portal_impression($jsondata)
	{
        $today_date = date('Y-m-d');
		$locationId=$jsondata->locationId;
        $macid = '';
        if(isset($jsondata->macid)){
            $macid = $jsondata->macid;
            $location_id = '';
            if(isset($jsondata->locationId)){
                $location_id = $jsondata->locationId;
            }
            $osinfo = '';
            if(isset($jsondata->platform_filter)){
                $osinfo = $jsondata->platform_filter;
            }
            $query = $this->DB2->query("select macid from channel_unique_daily_user where location_id = '$location_id' AND macid = '$macid' and DATE(visit_date) = '$today_date'");
            if($query->num_rows() < 1){
                $insert = $this->DB2->query("insert into channel_unique_daily_user(location_id, macid, osinfo, visit_date)
             values('$location_id', '$macid', '$osinfo', now())");
            }
        }

   

		
		   $check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
    location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
   if($check_impression->num_rows() > 0){
    $row = $check_impression->row_array();
    $temp_id = $row['id'];
    $update_cp = $this->DB2->query("update channel_captiveportal_impression set
     total_impression = (total_impression+1) WHERE id = '$temp_id'");
   }
   else{
    $insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
    (location_id, total_impression, visit_date) VALUES ('$locationId',
    '1', now())");
   }
   
	}
	
	public function app_install_click($jsondata)
	{
		$cpofferid=$jsondata->cpofferid;
		$userid=$jsondata->userid;

   
    $update_cp = $this->DB2->query("update cp_dashboard_analytics set
     is_downloaded = '1' WHERE cp_offer_id = '$cpofferid' and user_id='$userid' order by id desc limit 1");

        $tabledata1 = array("cp_offer_id" => $cpofferid, "location_id" => '', "user_id"=>$userid,"viewed_on"=>date("Y-m-d H:i:s"));
        //is_downloaded 1 in analytics
        $this->DB2->insert("cp_adlocation_default", $tabledata1);
        // cut the budget of app download in case of success page
        $location_id = 0;
        if(isset($jsondata->locationid)){
            $location_id = $jsondata->locationid;
        }
        $this->insert_update_budget($location_id,$cpofferid,"app_install_add");
	 
		$data['resultCode'] = '1';
		$data['resultMsg'] = 'Success';
			
			return $data;
   
   
	}

    //dineout api
    public function dineout_offer_click_analytics($jsondata)
    {
        $data = array();
        $locationId = 0;$rest_id = '';$offer_id = '';$offer_title = '';
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        if(isset($jsondata->rest_id)){
            $rest_id = $jsondata->rest_id;
        }
        if(isset($jsondata->offer_id)){
            $offer_id = $jsondata->offer_id;
        }
        if(isset($jsondata->offer_title)){
            $offer_title = $jsondata->offer_title;
        }
        $insert = $this->DB2->query("insert into dineout_offer_click (location_id, rest_id, offer_id, offer_title,
        click_on) VALUES ('$locationId', '$rest_id', '$offer_id', '$offer_title', now())");

        $data['resultCode'] = '1';
        return $data;
    }
    public function dineout_sponsor_click_analytics($jsondata)
    {
        $data = array();
        $locationId = 0;
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        if(count($jsondata->offer_detail) > 0){
            foreach($jsondata->offer_detail as $offer_detail1){
                $insert = $this->DB2->query("insert into dineout_sponsor_click (location_id, rest_id, offer_title,
        click_on) VALUES ('$locationId', '$offer_detail1->rest_id', '$offer_detail1->offer_title', now())");
            }
        }


        $data['resultCode'] = '1';
        return $data;
    }
    public function dineout_offer_click_booking($jsondata)
    {
        $data = array();
        $locationId = 0;
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        $rest_id = $jsondata->rest_id;
        $user_id = $jsondata->user_id;
        $booking_id = $jsondata->booking_id;

        $insert = $this->DB2->query("insert into dineout_offer_book (location_id, user_id, rest_id,
                booking_id, booked_on, responce) VALUES
                ('$locationId', '$user_id', '$rest_id', '$booking_id', now(), '$jsondata->responce')");


        $data['resultCode'] = '1';
        return $data;
    }
    public function dineout_sponsor_click_booking($jsondata)
    {
        $data = array();
        $locationId = 0;
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        $rest_id = $jsondata->rest_id;
        $user_id = $jsondata->user_id;
        $booking_id = $jsondata->booking_id;

        $insert = $this->DB2->query("insert into dineout_sponsor_book (location_id, user_id, rest_id,
                booking_id, booked_on, responce) VALUES
                ('$locationId', '$user_id', '$rest_id', '$booking_id', now(), '$jsondata->responce')");


        $data['resultCode'] = '1';
        return $data;
    }

    //cp2 api
    public function cp2_offers_totalmb($jsondata) {
        $data = array();
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
        $video_budget = 0;
        $image_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
		 $cpl_budget = 0;
		  $survey_budget = 0;
        if($datarr['resultCode'] == 1){
            foreach($datarr['offers'] as $datarr1){
                if($datarr1['offer_type']=="image_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'image_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $image_budget = $image_budget+$mb_cost;
                }
                if($datarr1['offer_type']=="video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="captcha_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'captcha_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $captcha_budget = $captcha_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="poll_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $poll_budget = $poll_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="redeemable_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'redeemable_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $redeem_budget = $redeem_budget+$mb_cost;
                }
				
				 else if($datarr1['offer_type']=="cost_per_lead_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'cost_per_lead_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $cpl_budget = $cpl_budget+$mb_cost;
                }
				 else if($datarr1['offer_type']=="survey_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'survey_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $survey_budget = $survey_budget+$mb_cost;
                }
            }
        }
        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'video_add';
        $offer_mb[0]['offer_mb'] = $video_budget+$image_budget;
        $offer_mb[1]['offer_type'] = 'captcha_add';
        $offer_mb[1]['offer_mb'] = $captcha_budget;
        $offer_mb[2]['offer_type'] = 'poll_add';
        $offer_mb[2]['offer_mb'] = $poll_budget;
        $offer_mb[3]['offer_type'] = 'redeemable_add';
          $offer_mb[3]['offer_mb'] = $redeem_budget+$cpl_budget;
		$offer_mb[4]['offer_type'] = 'survey_add';
        $offer_mb[4]['offer_mb'] = $survey_budget;

        $data['resultCode'] = 1;
        $data['resultMessage'] = 'Success';
        $data['offers_budget'] = $offer_mb;
        // echo "<pre>";print_r($data);die;
        return $data;
    }
	
	 public function vadodracp2_offers_totalmb($jsondata) {
        $data = array();
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
        $video_budget = 0;
        $image_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
		 $cpl_budget = 0;
		  $survey_budget = 0;
        if($datarr['resultCode'] == 1){
            foreach($datarr['offers'] as $datarr1){
                if($datarr1['offer_type']=="image_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'image_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $image_budget = $image_budget+$mb_cost;
                }
                if($datarr1['offer_type']=="video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="captcha_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'captcha_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $captcha_budget = $captcha_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="poll_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $poll_budget = $poll_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="redeemable_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'redeemable_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $redeem_budget = $redeem_budget+$mb_cost;
                }
				
				 else if($datarr1['offer_type']=="cost_per_lead_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'cost_per_lead_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $cpl_budget = $cpl_budget+$mb_cost;
                }
				 else if($datarr1['offer_type']=="survey_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'survey_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $survey_budget = $survey_budget+$mb_cost;
                }
            }
        }
        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'video_add';
        $offer_mb[0]['offer_mb'] = $video_budget;
		$offer_mb[1]['offer_type'] = 'image_add';
        $offer_mb[1]['offer_mb'] = $image_budget;
        $offer_mb[2]['offer_type'] = 'captcha_add';
        $offer_mb[2]['offer_mb'] = $captcha_budget;
        $offer_mb[3]['offer_type'] = 'poll_add';
        $offer_mb[3]['offer_mb'] = $poll_budget;
        $offer_mb[4]['offer_type'] = 'redeemable_add';
          $offer_mb[4]['offer_mb'] = $redeem_budget+$cpl_budget;
		$offer_mb[5]['offer_type'] = 'survey_add';
        $offer_mb[5]['offer_mb'] = $survey_budget;

        $data['resultCode'] = 1;
        $data['resultMessage'] = 'Success';
        $data['offers_budget'] = $offer_mb;
        // echo "<pre>";print_r($data);die;
        return $data;
    }
    // offer listing for cp-2 grid view
    public function captive_ad_data_grid($jsondata) {
// get location type(star, premium, mass)
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
//echo "<pre>"; print_r($datarr);die;
        $image_add = array();
        $i_add = 0;
        $video_add = array();
        $v_add = 0;
        $captcha_add = array();
        $c_add = 0;
        $poll_add = array();
        $p_add = 0;
        $redeem_add = array();
        $r_add = 0;
		 $cpl_add = array();
        $cp_add = 0;
		 $survey_add = array();
        $sur_add = 0;
        $data=array();
        //if any offer live(not shouut offer)
        if($datarr['resultCode'] == 1){

            foreach($datarr['offers'] as $datarr1){
                $captivelogarr=array("cp_offer_id"=>$datarr1['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr1['gender']),
                    "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr1['age_group']);
                if($datarr1['offer_type']=="image_add")
                {
                    $image_add[$i_add]=$this->get_image_ad_data($datarr1['cp_offer_id'], $location_type);
                    $i_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
                else if($datarr1['offer_type']=="video_add")
                {
                    $video_add[$v_add]=$this->get_video_ad_data($datarr1['cp_offer_id'], $location_type);
                    $v_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }

                else if($datarr1['offer_type']=="poll_add")
                {
                    $poll_add[$p_add]=$this->get_poll_ad_data($datarr1['cp_offer_id'],$jsondata->locationId,
                        $jsondata->userId, $location_type);
                    $p_add++;
                    // $this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
                else if($datarr1['offer_type']=="captcha_add")
                {
                    $captcha_add[$c_add]=$this->get_brand_captcha_data($datarr1['cp_offer_id'], $location_type);
                    $c_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
                else if($datarr1['offer_type']=="redeemable_add")
                {
                    $redeem_add[$r_add]=$this->get_redeemable_offer_data($datarr1['cp_offer_id'], $location_type);
                    $r_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
				 else if($datarr1['offer_type']=="cost_per_lead_add")
                {
                    $cpl_add[$cp_add]=$this->get_cost_per_lead_add_data($datarr1['cp_offer_id'], $location_type);
                    $cp_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
				 else if($datarr1['offer_type']=="survey_add")
                {
                    $survey_add[$sur_add]=$this->get_survey_ad_data($datarr1['cp_offer_id'], $location_type);
                    $sur_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
            }
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['image_add'] = $image_add;
            $data['video_add'] = $video_add;
            $data['poll_add'] = $poll_add;
            $data['captcha_add'] = $captcha_add;
            $data['redeemable_add'] = $redeem_add;
				$data['cpl_add'] = $cpl_add;
			 $data['survey_add'] = $survey_add;
        }

       /* elseif($datarr['resultCode'] == 0){
            $datarr = $this->get_defaultad_id_grid($jsondata);
            //echo "<pre>"; print_r($datarr);die;
            //if any shouut brand offer live
            if($datarr['resultCode'] == 1){
                foreach($datarr['offers'] as $datarr1){
                    $captivelogarr=array("cp_offer_id"=>$datarr1['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr1['gender']),
                        "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr1['age_group']);
                    if($datarr1['offer_type']=="image_add")
                    {
                        $image_add[$i_add]=$this->get_image_ad_data($datarr1['cp_offer_id']);
                        $i_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($datarr1['offer_type']=="video_add")
                    {
                        $video_add[$v_add]=$this->get_video_ad_data($datarr1['cp_offer_id']);
                        $v_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }

                    else if($datarr1['offer_type']=="poll_add")
                    {
                        $poll_add[$p_add]=$this->get_poll_ad_data($datarr1['cp_offer_id'],$jsondata->locationId,
                            $jsondata->userId);
                        $p_add++;
                        // $this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($datarr1['offer_type']=="captcha_add")
                    {
                        $captcha_add[$c_add]=$this->get_brand_captcha_data($datarr1['cp_offer_id']);
                        $c_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($datarr1['offer_type']=="redeemable_add")
                    {
                        $redeem_add[$r_add]=$this->get_redeemable_offer_data($datarr1['cp_offer_id']);
                        $r_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                }
                $data['resultCode'] = '1';
                $data['resultMessage'] = 'Success';
                $data['image_add'] = $image_add;
                $data['video_add'] = $video_add;
                $data['poll_add'] = $poll_add;
                $data['captcha_add'] = $captcha_add;
                $data['redeemable_add'] = $redeem_add;
            }
            else{
                $data['resultCode']='0';
                $data['resultMessage']='No Offer live';
            }

        }*/
        else
        {
            $data['resultCode']='0';
            $data['resultMessage']='No Offer live';
        }


        //Api Cp Analytivs logs
        //echo "<pre>"; print_r($data);die;
        return $data;
    }
    // get offer id which are sceen to user for live brand
    public function get_ad_id_grid($jsondata) {
        $data=array();
        /*$query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co
        .platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.brand_id!='145'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add'
                    AND co.offer_type!='app_install_add' and col.location_id='" . $jsondata->locationId . "' AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' ORDER BY co.cp_offer_id desc");*/
        $query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.cp_offer_id!='1'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add'
                     and col.location_id='" . $jsondata->locationId . "' AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' and co.is_coke='0' and co.is_mtnl_default_ad='0' ORDER BY co.cp_offer_id desc");

        $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }


            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


                $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
                if($userdata[0]['gender']!="")
                {
                    $gender=substr(strtoupper($userdata[0]['gender']),0,1);
                }
                else{
                    $gender="";
                }
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

                if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }


                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }


        }
        $liveofferarr = array_unique($liveofferarr);
        // echo "<pre>"; print_R($query->result());die;
        // get id who already sceen
        $query = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='" .
            $jsondata->userId . "'");

        $offer_sceen_array = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $offer_sceens) {
                $offer_sceen_array[] = $offer_sceens->cp_offer_id; // create array of offer sceen
            }
        }
		//echo "<pre>"; print_R($liveofferarr);
		//echo "<pre>"; print_R($offer_sceen_array);
		//die;
        $liveofferarr = array_diff($liveofferarr, $offer_sceen_array);
        if (!empty($liveofferarr)) {
            $i = 0;
            $data['resultCode'] = '1';
            $offers = array();
            foreach($liveofferarr as $liveofferarr1){
                $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$liveofferarr1."'");
                $rowarr=$query->row_array();
                $offers[$i]['cp_offer_id'] = $rowarr['cp_offer_id'];
                $offers[$i]['offer_type'] = trim($rowarr['offer_type']);
                $offers[$i]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
                ($userdata[0]['gender'],0,1):'';
                $offers[$i]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
                    ?$userdata[0]['age_group']:'';
                $i++;
            }
            $data['offers'] = $offers;
        }
        else {
            $data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
            $data['offer_type'] = 'default';
        }


        return $data;
    }
   //deprecated function not in use
    // get offer id which are scee to user for shouut brand
    public function get_defaultad_id_grid($jsondata) {
        $data=array();
        $userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
		$query1=$this->DB2->query("select Provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarr1=$query1->row_array();
			 if($rowarr1['Provider_id']==39)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='89'");
			}
			else{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
			}
            //$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
            $rowarr=$query->row_array();
			  $data['resultCode'] = '1';
            $data['offers'][0]['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offers'][0]['offer_type'] = trim($rowarr['offer_type']);
            $data['offers'][0]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['offers'][0]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';


        return $data;
    }
    // insert offer sceen by user , insert view in analytics
    public function cp_offer_sceen_user($jsondata){
        // insert offer view analytics
        $data=array();
        if(isset($jsondata->locationid) ){
            $user_detail = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($jsondata->userid)));
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }

            $captivelogarr=array("cp_offer_id"=>$jsondata->cpofferid,"location_id"=>$jsondata->locationid,
                "user_id"=>$jsondata->userid,'gender'=>strtoupper($gender),
                "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$age_group);
            $this->add_cp_analytics($captivelogarr);
        }

        //budget cut and user sceen entry in case of vide add and image add
        if(isset($jsondata->offertype)){
            if($jsondata->offertype == 'video_add' || $jsondata->offertype == 'video' ||
                $jsondata->offertype == 'image_add' || $jsondata->offertype == 'image' || $jsondata->offertype == 'app_install_add' || $jsondata->offertype == 'survey_add'){
                $this->insert_update_budget($jsondata->locationid,$jsondata->cpofferid,$jsondata->offertype);
                $offer_id = $jsondata->cpofferid;
                $user_id = $jsondata->userid;
                $location_id = '0';
                if(isset($jsondata->locationid) ){
                    $location_id = $jsondata->locationid;
                }
                $query = $this->DB2->query("insert into cp_adlocation_default(cp_offer_id, user_id, location_id) VALUES
				('$offer_id', '$user_id', '$location_id')");
            }

        }
        // app download case is downloaded = 1
        if($jsondata->offer_type == 'app_install_add'){
            $cpofferid=$jsondata->cpofferid;
            $userid=$jsondata->userid;
            $update_cp = $this->DB2->query("update cp_dashboard_analytics set
     is_downloaded = '1' WHERE cp_offer_id = '$cpofferid' and user_id='$userid' order by id desc limit 1");
        }
        $data['resultCode'] = '1';
        return $data;
    }
    // offer detail in grid view
    public function captive_ad_data_grid_detail($jsondata) {
        $offer_id = $jsondata->offer_id;
        $offer_type = $jsondata->offer_type;
        $location_id = $jsondata->locationId;
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $offer_data = array();
        $data=array();


        if($offer_type=="video_add" || $offer_type == 'video')
        {
            $offer_data=$this->get_video_ad_data($offer_id, $location_type);
        }
        else if($offer_type=="image_add" || $offer_type == 'image')
        {
            $offer_data = $this->get_image_ad_data($offer_id, $location_type);
        }
        else if($offer_type=="poll_add")
        {
            $query=$this->DB2->query("SELECT co.cp_offer_id,co.redirect_url,co.poll_logo,co.poll_original,co.poll_brand_title,co.offer_title,co.brand_id,co.campaign_name,co.image_original,
co.image_small,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
br.original_logo_logoimage,cpq.id,cpq.question,cpq.option1,cpq.option2,cpq.option3,cpq.option4 FROM `cp_offers` co
INNER JOIN `cp_offers_question` cpq ON (co.cp_offer_id=cpq.cp_offer_id)
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
            $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
            $mb_cost = '0';
            if($get_budget->num_rows() > 0){
                $budget_row = $get_budget->row_array();
                if($location_type == '1'){
                    $mb_cost = $budget_row['star'];
                    $mb_cost = ceil((1024*$mb_cost)/30);
                }
                elseif($location_type == '2'){
                    $mb_cost = $budget_row['premium'];
                    $mb_cost = ceil((1024*$mb_cost)/30);
                }
                elseif($location_type == '3'){
                    $mb_cost = $budget_row['mass'];
                    $mb_cost = ceil((1024*$mb_cost)/30);
                }
            }
            if($query->num_rows()>0) {
                $rowarr = $query->row_array();
                $offer_data['resultCode'] = '1';
                $offer_data['resultMessage'] = 'Success';
                $offer_data['rewradtype'] = 'poll';
                $offer_data['withoffer'] = '0';
                $offer_data['mb_cost'] = $mb_cost;
                $offer_data['brand_id'] = $rowarr['brand_id'];
                $offer_data['brand_name'] = strtoupper($rowarr['brand_name']);
                $offer_data['offers_small_image'] = ($rowarr['image_small'] != '') ? CAMPAIGN_IMAGEPATH . "small/" . $rowarr['image_small'] : "";
                $offer_data['offers_original_image'] = ($rowarr['image_original'] != '') ? CAMPAIGN_IMAGEPATH . "original/" . $rowarr['image_original'] : "";
                $offer_data['brand_logo_image'] = ($rowarr['original_logo_logoimage'] != '') ? REWARD_IMAGEPATH . "brand/logo/logo/" . $rowarr['original_logo_logoimage'] : "";
                $offer_data['brand_original_image'] = ($rowarr['original_logo'] != '') ? REWARD_IMAGEPATH . "brand/logo/original/" . $rowarr['original_logo'] : "";
                $offer_data['validTill'] = '';
                $offer_data['validTill_fromTime'] = '';
                $offer_data['store_count'] = '';
                $offer_data['video_url'] = '';
                $offer_data['offer_id'] = $rowarr['cp_offer_id'];
                $offer_data['promotion_name'] = $rowarr['offer_title'];
                $offer_data['poll_question_id'] = $rowarr['id'];
                $offer_data['poll_question'] = $rowarr['question'];
                $offer_data['poll_option'][0] = $rowarr['option1'];
                $offer_data['poll_option'][1] = $rowarr['option2'];
                $offer_data['poll_option'][2] = $rowarr['option3'];
                $offer_data['poll_option'][3] = $rowarr['option4'];
                $offer_data['offer_id'] = $rowarr['cp_offer_id'];
                $offer_data['poll_logo'] = $rowarr['poll_logo'];
                $offer_data['poll_original'] = $rowarr['poll_original'];
                $offer_data['poll_brand_title'] = $rowarr['poll_brand_title'];

                $offer_data['captch_text'] = "";
                $offer_data['captcha_authenticity'] = "";
                $offer_data['url'] = $rowarr['redirect_url'];
            }
        }
        else if($offer_type=="captcha_add")
        {
            $offer_data=$this->get_brand_captcha_data($offer_id, $location_type);

        }
        else if($offer_type=="redeemable_add")
        {
            $offer_data=$this->get_redeemable_offer_data($offer_id, $location_type);

        }


        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        $data['add_detail'][0] = $offer_data;
        //Api Cp Analytivs logs
        // echo "<pre>"; print_r($data);die;
        return $data;
    }

    public function get_mb_data($jsondata)
    {
        $data=array();
        $query=$this->DB2->query("SELECT * from cp_mb_data where amt='".$jsondata->amount."'");
        if($query->num_rows()>0)
        {
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $i=0;
            foreach($query->result() as $val)
            {
                $data['mbdata'][$i]['data_name']=$val->data_name;
                $data['mbdata'][$i]['data_mb']=$val->data_mb;
                $data['mbdata'][$i]['amt']=$val->amt;

            }
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'No such plan';
        }
        return $data;

    }
    public function insert_paytm_success($jsondata)
    {
        $data=array();
        $tabledata=array("user_id"=>$jsondata->MERC_UNQ_REF,"order_id"=>$jsondata->ORDERID,"amount"=>$jsondata->TXNAMOUNT,"gatewayname"=>$jsondata->GATEWAYNAME,
            "msg"=>$jsondata->RESPMSG,
            "txn_date"=>$jsondata->TXNDATE,"txnid"=>$jsondata->TXNID);
        $this->DB2->insert("paytm_sucess_user", $tabledata);
        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;


    }
    public function activate_voucher($jsondata)
    {
        $data=array();
        $query=$this->DB2->query("select vmv.vc_code,vmc.data_amnt from vm_assign_voucherid vmv inner join vm_mb_cost vmc ON ( vmv.cost_id = vmc.id ) where vmv.customer_mobile='".$jsondata->mobile."' and vmv.vc_code='".$jsondata->voucher."' and vmv.is_used=0");
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['mb']=$rowarr['data_amnt'];

        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
        }
        return $data;

    }
    public function voucher_availed($jsondata)
    {
        $data=array();
        $tabledata=array("is_used"=>1);
        $where_array = array('customer_mobile' => $jsondata->mobile, 'vc_code' => $jsondata->voucher);
        $this->DB2->where($where_array);
        $this->DB2->update('vm_assign_voucherid', $tabledata);

        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;

    }

    public function cp2_offers_totalmb_website($jsondata){
        $data = array();
        $video_budget = 0;
        $image_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
        $app_install_budget = 0;
        $location = $jsondata->location;
        $location = explode('::',$location);
        $user_latitude = $location[0];
        $user_longitude = $location['1'];
        //get nearest location id
        $location_query = $this->DB2->query("SELECT(
            6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
            )
        ) as distance, id from wifi_location HAVING distance <= 5");
        $live_offers = array();
        $live_offers_counter = 0;
        if($location_query->num_rows() > 0){
            $data['resultCode'] = 1;
            $data['resultMessage'] = 'Success';
            $addarr=array();
            foreach($location_query->result() as $location_query1){
                $location_id = $location_query1->id;
                $send_data = json_decode(json_encode(array("locationId"=>$location_id, "userId" => $jsondata->userId,
                    "apikey" => $jsondata->apikey)));
                $datarr = $this->get_ad_id_grid($send_data);

                if($datarr['resultCode'] == 1){

                    foreach($datarr['offers'] as $datarr1){
                        if(!in_array($datarr1['cp_offer_id'],$addarr)) {
                            $live_offers[$live_offers_counter]['cp_offer_id'] = $datarr1['cp_offer_id'];
                            $live_offers[$live_offers_counter]['offer_type'] = $datarr1['offer_type'];
                            $live_offers[$live_offers_counter]['gender'] = $datarr1['gender'];
                            $live_offers[$live_offers_counter]['age_group'] = $datarr1['age_group'];
                            $live_offers[$live_offers_counter]['location_id'] = $location_id;
                            $addarr[] = $datarr1['cp_offer_id'];
                            $live_offers_counter++;
                        }
                    }
                }
            }
            foreach($live_offers as $live_offers1){
                $location_type = '';
                $location_id = $live_offers1['location_id'];
                $get_location_type = $this->DB2->query("select location_type from wifi_location where id = '$location_id'");
                if($get_location_type->num_rows() > 0){
                    $rowarr=$get_location_type->row_array();
                    $location_type = $rowarr['location_type'];
                }
                if($live_offers1['offer_type']=="image_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'image_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $image_budget = $image_budget+$mb_cost;
                }
                if($live_offers1['offer_type']=="video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="captcha_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'captcha_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $captcha_budget = $captcha_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="poll_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $poll_budget = $poll_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="redeemable_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'redeemable_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $redeem_budget = $redeem_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="app_install_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'app_install_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $app_install_budget = $app_install_budget+$mb_cost;
                }
            }
        }
        else{
            $data['resultCode'] = 0;
            $data['resultMessage'] = 'No location found in 5 km';
        }

        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'video_add';
        $offer_mb[0]['offer_mb'] = $video_budget+$image_budget;
        $offer_mb[1]['offer_type'] = 'captcha_add';
        $offer_mb[1]['offer_mb'] = $captcha_budget;
        $offer_mb[2]['offer_type'] = 'poll_add';
        $offer_mb[2]['offer_mb'] = $poll_budget;
        $offer_mb[3]['offer_type'] = 'redeemable_add';
        $offer_mb[3]['offer_mb'] = $redeem_budget;
        $offer_mb[4]['offer_type'] = 'app_install_add';
        $offer_mb[4]['offer_mb'] = $app_install_budget;

        $data['offers_budget'] = $offer_mb;
        // echo "<pre>";print_r($data);die;
        return $data;
    }

    public function captive_ad_data_grid_website($jsondata) {
        $data = array();
        $image_add = array();
        $i_add = 0;
        $video_add = array();
        $v_add = 0;
        $captcha_add = array();
        $c_add = 0;
        $poll_add = array();
        $p_add = 0;
        $redeem_add = array();
        $r_add = 0;
        $app_install_add = array();
        $a_i_add = 0;
        $location = $jsondata->location;
        $location = explode('::',$location);
        $user_latitude = $location[0];
        $user_longitude = $location['1'];
        //get nearest location id
        $location_query = $this->DB2->query("SELECT(
            6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
            )
        ) as distance, id from wifi_location HAVING distance <= 5");
        $live_offers = array();
        $live_offers_counter = 0;
        if($location_query->num_rows() > 0){

            $addarr=array();
            foreach($location_query->result() as $location_query1){
                $location_id = $location_query1->id;
                $send_data = json_decode(json_encode(array("locationId"=>$location_id, "userId" => $jsondata->userId,
                    "apikey" => $jsondata->apikey)));
                $datarr = $this->get_ad_id_grid($send_data);
                if($datarr['resultCode'] == 1){
                    foreach($datarr['offers'] as $datarr1){
                        if(!in_array($datarr1['cp_offer_id'],$addarr)) {
                            $live_offers[$live_offers_counter]['cp_offer_id'] = $datarr1['cp_offer_id'];
                            $live_offers[$live_offers_counter]['offer_type'] = $datarr1['offer_type'];
                            $live_offers[$live_offers_counter]['gender'] = $datarr1['gender'];
                            $live_offers[$live_offers_counter]['age_group'] = $datarr1['age_group'];
                            $live_offers[$live_offers_counter]['location_id'] = $location_id;
                            $addarr[] = $datarr1['cp_offer_id'];
                            $live_offers_counter++;
                        }
                    }
                }
            }
            if(count($live_offers) > 0){
                $data['resultCode'] = 1;
                $data['resultMessage'] = 'Success';
                foreach($live_offers as $live_offers1){
                    $location_type = '';
                    $location_id = $live_offers1['location_id'];
                    $get_location_type = $this->DB2->query("select location_type from wifi_location where id = '$location_id'");
                    if($get_location_type->num_rows() > 0){
                        $rowarr=$get_location_type->row_array();
                        $location_type = $rowarr['location_type'];
                    }
                    if($live_offers1['offer_type']=="image_add")
                    {
                        $responce = $this->get_image_ad_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $image_add[$i_add]=$responce;
                        $i_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type']=="video_add")
                    {
                        $responce = $this->get_video_ad_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $video_add[$v_add]=$responce;
                        $v_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }

                    else if($live_offers1['offer_type']=="poll_add")
                    {
                        $responce = $this->get_poll_ad_data($live_offers1['cp_offer_id'],$location_id,
                            $jsondata->userId, $location_type);
                        $responce['location_id'] = $location_id;
                        $poll_add[$p_add]=$responce;
                        $p_add++;
                        // $this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type']=="captcha_add")
                    {
                        $responce = $this->get_brand_captcha_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $captcha_add[$c_add]=$responce;
                        $c_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type']=="redeemable_add")
                    {
                        $responce = $this->get_redeemable_offer_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $redeem_add[$r_add]=$responce;
                        $r_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type'] == "app_install_add"){
                        $responce = $this->get_app_download_ad_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $app_install_add[$a_i_add]=$responce;
                        $a_i_add++;
                    }
                }
                $data['image_add'] = $image_add;
                $data['video_add'] = $video_add;
                $data['poll_add'] = $poll_add;
                $data['captcha_add'] = $captcha_add;
                $data['redeemable_add'] = $redeem_add;
                $data['app_install_add'] = $app_install_add;
            }
            else{
                $data['resultCode'] = 0;
                $data['resultMessage'] = 'No Offer live';
            }

        }
        else{
            $data['resultCode'] = 0;
            $data['resultMessage'] = 'No location found in 5 km';
        }
        //  echo "<pre>";print_r($data);die;
        return $data;
    }

    public function skhotspot_voucher_validate($jsondata){
        $data = array();
        $mobile = '';
        if(isset($jsondata->voucher)){
            $mobile = $jsondata->mobile;
        }
        $voucher = '';
        if(isset($jsondata->voucher)){
            $voucher = $jsondata->voucher;
        }
        if($mobile != ''){

            if($voucher != ''){
                //check already assing voucher with this num
                $query_check = $this->DB2->query("select * from surajkund_voucher WHERE  is_used = '1' and mobile_no = '$mobile'");
                if($query_check->num_rows() > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Valid mobile";
                    $data['voucher']=$voucher;
                }
                else{
                    // for new voucher
                    $query = $this->DB2->query("select * from surajkund_voucher WHERE voucher = '$voucher' AND is_used = '0'");
                    if($query->num_rows() > 0){
                        $update = $this->DB2->query("update surajkund_voucher set is_used = '1', mobile_no = '$mobile',
                used_on = now() WHERE voucher = '$voucher'");
                        $data['resultCode'] = '1';
                        $data['resultMsg'] = "Successfully assign voucher with this mobile";
                        $data['voucher']=$voucher;
                    }
                    else{
                        $data['resultCode'] = '0';
                        $data['resultMsg'] = "Not valid voucher code or already used";
                    }
                }
            }
            //for already used voucher
            else{
                // check mobile num have already assing voucher then it is ok
                $query = $this->DB2->query("select * from surajkund_voucher WHERE mobile_no = '$mobile' order by id desc limit 1");
                if($query->num_rows() > 0){
                    $rowarr=$query->row_array();
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Mobile is valid";
                    $data['voucher']=$rowarr['voucher'];
                }
                else{
                    $data['resultCode'] = '2';
                    $data['resultMsg'] = "Not assign any vocher with this mobile";
                }
            }
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Invalid mobile";
        }

        return $data;
    }
	
	
		
	//Survey Api
	
	public function get_survey_question($jsondata)
	{
		$data=array();
		if($jsondata->offer_id!="")
		{
			$query=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$jsondata->offer_id."' order by ques_id asc limit 1");
			$query2=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$jsondata->offer_id."'");
			$query3=$this->DB2->query("select offer_title from cp_offers where cp_offer_id='".$jsondata->offer_id."'");
			$rowarr=$query->row_array();
			$rowarr1=$query3->row_array();
			
			//echo "<pre>";print_R($rowarr); die;
			
			$data['resultCode']=1;
			$data['resultMsg']='Success';
			$data['question_id']=$rowarr['ques_id'];
			$data['question']=$rowarr['question'];
			$data['question_id']=$rowarr['ques_id'];
			$data['total_question']=$query2->num_rows();
			$data['promotion_name']=$rowarr1['offer_title'];
			$data['option_choice']=$rowarr['option_choice'];
			$query1=$this->DB2->query("select option_id,`option_val` from cp_survey_option where ques_id='".$rowarr['ques_id']."'");
			

			$i=0;
			foreach($query1->result() as $val)
			{
				$data['option'][$i]['option_id']=$val->option_id;
				$data['option'][$i]['option_name']=$val->option_val;
				$i++;
			}
			$this->clear_user_option($jsondata);
			
		}
		else{
			$data['resultCode']=0;
			$data['resultMsg']='Fail';
		}
		return $data;
		
	}
	
	
	public function get_subsequent_question($jsondata)
	{
		$data=array();
		if($jsondata->ques_id!="")
		{
			$query=$this->DB2->query("select * from cp_survey_option where ques_id='".$jsondata->ques_id."' and option_id='".$jsondata->option_id."' ");
			$rowarr=$query->row_array();
			
			
			
		
			if($rowarr['next_ques']==0)
			{
				$query1=$this->DB2->query("select ques_id from cp_survey_question where cp_offer_id='".$jsondata->offer_id."'");
				$questarr=array();
				foreach($query1->result() as $val)
				{
					$questarr[]=$val->ques_id;
				}
				
				if(end($questarr)== $rowarr['ques_id'])
				{
						$data['resultCode']=2;
						$data['resultMsg']='This is end of Survey';
				}
				else{
					$nextkey=array_search($rowarr['ques_id'],$questarr)+1;
					$data=$this->get_question_data($questarr[$nextkey],$jsondata->offer_id);
				}
				$this->insert_user_option($jsondata);
				
			}
			else if($rowarr['next_ques']==1){
					$data['resultCode']=2;
						$data['resultMsg']='This is end of Survey';
						$this->insert_user_option($jsondata);
						//$this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'survey_add');
			}
			else{
				$quesid=$rowarr['next_ques'];
				
				$data=$this->get_question_data($quesid,$jsondata->offer_id);
				$this->insert_user_option($jsondata);
			}
			
			
		}
		else{
			
			$data['resultCode']=0;
			$data['resultMsg']='Fail';
		}
		
		return $data;
		
	}
	
	
	public function get_question_data($quesid,$offer_id)
	{
		
		$query3=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
		$query=$this->DB2->query("select * from cp_survey_question where ques_id='".$quesid."'");
		$query4=$this->DB2->query("select offer_title from cp_offers where cp_offer_id='".$offer_id."'");
		$rowarr=$query->row_array();
		$rowarr1=$query4->row_array();
		$data['resultCode']=1;
		$data['resultMsg']='Success';
		$data['total_question']=$query3->num_rows();
		$data['question_id']=$rowarr['ques_id'];
		$data['promotion_name']=$rowarr1['offer_title'];
		$data['option_choice']=$rowarr['option_choice'];
			
			$data['question']=$rowarr['question'];
			$data['question_id']=$rowarr['ques_id'];
			$query1=$this->DB2->query("select option_id,`option_val` from cp_survey_option where ques_id='".$rowarr['ques_id']."'");
			

			$i=0;
			foreach($query1->result() as $val)
			{
				$data['option'][$i]['option_id']=$val->option_id;
				$data['option'][$i]['option_name']=$val->option_val;
				$i++;
			}
			
			$query2=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
			$seqarr=array();
			$x=1;
			foreach($query2->result() as $val1)
			{
				$seqarr[$x]=$val1->ques_id;
				$x++;
			}
			
			$data['question_number']=array_search($quesid,$seqarr);
			
			return $data;
	}
	
	
	public function clear_user_option($jsondata)
	{
		
		 $this->DB2->where('user_id', $jsondata->userId);
		  $this->DB2->where('cp_offer_id', $jsondata->offer_id);
		 
      $this->DB2->delete('cp_survey_answer'); 
		
	}
	
	
	public function insert_user_option($jsondata)
	{
		 $user_detail = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($jsondata->userId)));
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
			
		
		
		$optionarr=array_filter(explode(",",$jsondata->option_id));
		foreach($optionarr as $val)
		{
		$tabledata=array("question_id"=>$jsondata->ques_id,"option_id"=>$val,"user_id"=>$jsondata->userId,"cp_offer_id"=>$jsondata->offer_id,"platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,"viewed_on"=>date("Y-m-d H:i:s"));
		$this->DB2->insert('cp_survey_answer',$tabledata);
		}
	}
	
	public function time_conversion($time)
	{
		

 // time duration in seconds

			$days = floor($time / (60 * 60 * 24));
			$time -= $days * (60 * 60 * 24);

			$hours = floor($time / (60 * 60));
			$time -= $hours * (60 * 60);

			$minutes = floor($time / 60);
			$time -= $minutes * 60;

			$seconds = floor($time);
			$time -= $seconds;
			if($minutes>1)
			{
				$time="{$minutes} Minutes {$seconds} Seconds";
			}
			else if($minutes==1)
			{
				$time="{$minutes} Minutes";
			}
			else{
				$time="{$seconds} Seconds";
			}

			return $time;

	}
}

?>
