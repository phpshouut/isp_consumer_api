<?php

class Cokeportal_model extends CI_Model {
	
	 public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
    }
	

    public function authenticateApiuser($user_id) {
	
		$query=$this->DB2->query("select id from sht_users where uid='".$user_id."'");
	//	echo "select id from sht_users where uid='".$user_id."'";
       // $query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
		
        $num =$query->num_rows();
		//echo $num;die;
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function authenticateApiGuestuser($guest_user_id) {
      $query=$this->DB2->query("select id from sht_users where uid='".$user_id."'");
		
       // $query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
		
        $num =$query->num_rows();
		//echo $num;die;
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function log($function, $request, $responce) {
        $log_data = array(
            'function_name' => $function,
            'request' => $request,
            'responce' => $responce,
            'time' => date("d-m-Y H:i:s"),
        );
        $this->mongo_db->insert('log_data', $log_data);
    }

   
    public function allocate_budget_views($locid,$offerid)
    {
        $query=$this->DB2->query("SELECT * FROM `cp_budget_used` WHERE  cp_offer_id='".$offerid."'");
        $query1=$this->DB2->query("SELECT `location_type` FROM `wifi_location` WHERE id='".$locid."'");
        $locationarr=$query1->row_array();
        $arr1=array(1=>"mass_budget",2=>"premium_budget",3=>"star_budget");
        if($query->num_rows()>0)
        {
            $query3=$this->DB2->query("select co.`total_budget`,co.`mass_budget`,co.`premium_budget`,co.`star_budget` FROM `cp_offers` co where co.cp_offer_id='".$offerid."'");
            $bugetarr=$query3->row_array();
            $usedbudgetarr=$query->row_array();
            if($locationarr['location_type']==1)
            {
                $totalvalue=$bugetarr['mass_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['mass_budget'];
                if($totalvalue>$assignedvalue)
                {

                    return true;

                }
                else
                {
                    return false;
                }
            }
            else if($locationarr['location_type']==2)
            {
                $totalvalue=$bugetarr['premium_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['premium_budget'];
                if($totalvalue>$assignedvalue)
                {

                    return true;

                }
                else
                {
                    return false;
                }
            }
            else if($locationarr['location_type']==3)
            {
                $totalvalue=$bugetarr['star_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['star_budget'];
                if($totalvalue>$assignedvalue)
                {

                    return true;

                }
                else
                {
                    return false;
                }
            }
        }
        else {

            return true;
        }


    }


    public function insert_update_budget($locid,$offerid,$offertype)
    {
        $query=$this->DB2->query("SELECT * FROM `cp_budget_used` WHERE location_id='".$locid."' AND cp_offer_id='".$offerid."'");
        $query1=$this->DB2->query("SELECT `location_type` FROM `wifi_location` WHERE id='".$locid."'");
        $query4=$this->DB2->query("select star,premium,mass from cp_location_budget where offer_type='".$offertype."'");
        $viewmoneyarr=$query4->row_array();
        $locationarr=$query1->row_array();
        $arr1=array(1=>"mass_budget",2=>"premium_budget",3=>"star_budget");
        if($query->num_rows()>0)
        {
            $query3=$this->DB2->query("select co.`total_budget`,co.video_duration,co.`mass_budget`,co.`premium_budget`,co.`star_budget` FROM `cp_offers` co where co.cp_offer_id='".$offerid."'");
            $bugetarr=$query3->row_array();
            $usedbudgetarr=$query->row_array();
            if($locationarr['location_type']==1)
            {
                $totalvalue=$bugetarr['mass_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['mass_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['mass'];
					}
					else{
						$newval=$viewmoneyarr['mass'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("mass_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
            else if($locationarr['location_type']==2)
            {
                $totalvalue=$bugetarr['premium_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['premium_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['premium'];
					}
					else{
						$newval=$viewmoneyarr['premium'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("premium_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
            else if($locationarr['location_type']==3)
            {
                $totalvalue=$bugetarr['star_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['star_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['star'];
					}
					else{
						$newval=$viewmoneyarr['star'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("star_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
        }
        else {


 if($locationarr['location_type']==1)
     {
         $budget=$viewmoneyarr['mass'];
     }
     else if($locationarr['location_type']==2)
     {
         $budget=$viewmoneyarr['premium'];
     }
     else{
         $budget=$viewmoneyarr['star'];
     }
     
        $tabledata=array("location_id"=>$locid,$arr1[$locationarr['location_type']]=>$budget,"cp_offer_id"=>$offerid);
        $this->DB2->insert("cp_budget_used",$tabledata);

        }


    }


 public function get_image_ad_data($offer_id,  $location_type = null) {
        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,
		co.`campaign_end_date`,co.contest_run_on,co.image_original,co.poll_brand_title,co.poll_original,co.poll_logo,
co.image_small,co.image_medium,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_contest_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='coke_contest_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
			$data['offers_medium_image']=($rowarr['image_medium']!='')?CAMPAIGN_IMAGEPATH."medium/".$rowarr['image_medium']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']=date('Y-m-d', strtotime($rowarr['campaign_end_date']));
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
			$data['social_platform']=$rowarr['contest_run_on'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
	
        return $data;
		
    }
	
	public function get_survey_ad_data($offer_id,  $location_type = null) {
        $data=array();
        $query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,co.`campaign_end_date`,
		co.image_original,co.poll_brand_title,co.poll_original,co.poll_logo,
co.image_small,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_survey_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
			
			$query3=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='coke_survey_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
          $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
           $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
             $data['validTill']=date('Y-m-d', strtotime($rowarr['campaign_end_date']));
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
			 $data['time_slot']=$this->time_conversion($query3->num_rows()*10);
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Survey offer';

        }
        return $data;
    }
	
	
	


    public function get_video_ad_data($offer_id, $location_type = null) {
        $data=array();

        $query=$this->DB2->query("SELECT co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.video_path,co.brand_id,co.campaign_name,
		co.`campaign_end_date`,co.video_thumb_logo,co.video_thumb_original,co.video_thumb_medium,co.image_original,co.poll_brand_title,co.poll_original,co.poll_logo,
co.image_small,co.image_medium,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."'");

        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_video_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='coke_video_add';
            $data['withoffer']='0';
            $data['mb_cost'] = 0;
            $data['brand_id']=$rowarr['brand_id'];
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']="";
            $data['offers_original_image']="";
			 $data['offers_medium_image']="";
           $data['brand_logo_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?REWARD_IMAGEPATH."brand/logo/logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?REWARD_IMAGEPATH."campaign/poll_logo/original/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?REWARD_IMAGEPATH."brand/logo/original/".$rowarr['original_logo']:"");
            $data['validTill']=date('Y-m-d', strtotime($rowarr['campaign_end_date']));
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']=CAMPAIGN_VIDEOPATH.$rowarr['video_path'];
			$data['video_thumb_image']=($rowarr['video_thumb_medium']!='')?REWARD_IMAGEPATH."campaign/video_thumb/medium/".$rowarr['video_thumb_medium']:REWARD_IMAGEPATH."campaign/video_thumb/original/".$rowarr['video_thumb_original'];
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
			
        }
        else {

            $data['resultCode']='0';

            $data['resultMessage']='No Video offer';


        }

        return $data;
    }


  
   

 

   public function coke_products_mbdata($jsondata)
	{
		$data=array();
		$query=$this->DB2->query("SELECT * FROM  `coke_code_type` where location_id='".$jsondata->locationId."'");
		$data['resultCode']='1';
        $data['resultMessage']='Success';
		$i=0;
		$rgb=0;$fountain=0;$can=0;$pet=0;
		$codearr=array();
		foreach($query->result() as $val)
		{
			
			$data['mb_budget'][$i]['offer_type']=strtolower(str_replace("/","_",$val->code_type_name));
			$data['mb_budget'][$i]['offer_mb']=strtolower($val->mb);
			$codearr[]=$val->code_type_name;
			$i++;
		}
		
		if(!in_array('RGB/FOUNTAIN/TETRA',$codearr))
		{
			$data['mb_budget'][5]['offer_type']='rgb_fountain_tetra';
			$data['mb_budget'][5]['offer_mb']="20";
		}
	if(!in_array('KINLEY',$codearr))
		{
			$data['mb_budget'][6]['offer_type']='kinley';
			$data['mb_budget'][6]['offer_mb']="20";
		}
	 if(!in_array('CAN',$codearr))
		{
			$data['mb_budget'][7]['offer_type']='can';
			$data['mb_budget'][7]['offer_mb']="50";
		}
		if(!in_array('PET',$codearr))
		{
			$data['mb_budget'][8]['offer_type']='pet';
			$data['mb_budget'][8]['offer_mb']="50";
		}
		$data['mb_budget']=array_values($data['mb_budget']);
		return $data;
	}



  

    public function get_offertype($offerid)
    {

        $rowarr="";
        $query=$this->DB2->query("select offer_type,brand_id,cp_offer_id from cp_offers where cp_offer_id='".$offerid."' ");

        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();

        }

        return $rowarr;

    }

    public function add_cp_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],"is_redirected"=>0,
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics",$tabledata);
    }
	
	
	public function add_coke_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],"is_redirected"=>0,
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
			
			
        $this->DB2->insert("coke_dashboard_analytics",$tabledata);
		
		 $tabledata1=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],"is_redirected"=>0,
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],
			"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group'],"mbcost"=>$datarr['mbcost']);
			 $this->DB2->insert("coke_mb_analytics",$tabledata1);
    }

   

    public function randon_voucher(){
        $random_number = mt_rand(1000000, 9999999);
        $query = $this->DB2->query("select voucher_code from cp_voucher WHERE voucher_code = '$random_number'");
        if($query->num_rows() > 0){
            $this->randon_voucher();
        }else{
            return $random_number;
        }
    }
	
	

    public function user_detail($user_id){

		$data=array();
        $query = $this->DB2->query("select firstname,middlename,lastname,email,gender,age_group from sht_users where uid='".$user_id."'");
		if($query->num_rows()>0)
		{
			$rowdata=$query->row_array();
		$data[0]['firstname']=$rowdata['firstname']	;
		$data[0]['middlename']=$rowdata['middlename']	;
		$data[0]['lastname']=$rowdata['lastname']	;
		$data[0]['email']=$rowdata['email']	;
		$data[0]['gender']=$rowdata['gender']	;
		$data[0]['age_group']=$rowdata['age_group']	;
		}
		else{
				$data[0]['firstname']=''	;
		$data[0]['middlename']='';
		$data[0]['lastname']='';
		$data[0]['email']='';
		$data[0]['gender']='';
		$data[0]['age_group']='';
		}
       return $data;
    }


    public function get_age_group($mobileno)
    {
        $CI = &get_instance();
//setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('db2', TRUE);
        $query = $this->db2->query("SELECT age_group FROM `wifi_register_users` wru1 WHERE mobileno='".$mobileno."'");
        $resultdata=$query->result();
        if($query->num_rows>0)
        {
            if($resultdata[0]->age_group!="" || $resultdata[0]->age_group!=0){
                return $resultdata[0]->age_group;
            }
            else{
                return 'all';
            }

        }
        else{
            return 'all';
        }

    }
    public function timearr($timeval){

        $arrtime=explode(",",$timeval);
        $fnltimearr=array();

        foreach($arrtime as $val)
        {
            $hifentime=explode("-",$val);
            $fnltimearr[]=$hifentime[0];
            for($i=0;$i<1; $i++)
            {
                if($hifentime[0]<$hifentime[1] || $hifentime[0]==22){
                    $midtime=$hifentime[0]+1;
                    if(strlen($midtime)==1){
                        $fnltimearr[]="0".$midtime;
                    }
                    else{
                        $fnltimearr[]=$midtime;
                    }

                    $i++;
                }
                $fnltimearr[]=$hifentime[1];
            }
        }
        return array_unique($fnltimearr);

    }


   

    public function cp2_offers_totalmb($jsondata) {
        $data = array();
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
        $video_budget = 0;
        $contest_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
        $survey_budget = 0;
        if($datarr['resultCode'] == 1){
            foreach($datarr['offers'] as $datarr1){
                if($datarr1['offer_type']=="coke_contest_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_contest_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $contest_budget = $contest_budget+$mb_cost;
                }
                if($datarr1['offer_type']=="coke_video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
             
                else if($datarr1['offer_type']=="coke_survey_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_survey_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $survey_budget = $survey_budget+$mb_cost;
                }
            }
        }
        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'coke_video_add';
        $offer_mb[0]['offer_mb'] = $video_budget;
        $offer_mb[1]['offer_type'] = 'coke_contest_add';
		 $offer_mb[1]['offer_mb'] = $contest_budget;
     
        $offer_mb[4]['offer_type'] = 'coke_survey_add';
        $offer_mb[4]['offer_mb'] = $survey_budget;
        $data['resultCode'] = 1;
        $data['resultMessage'] = 'Success';
        $data['offers_budget'] = $offer_mb;
        
        return $data;
    }
    // offer listing for cp-2 grid view
    public function captive_ad_data_grid($jsondata) {
// get location type(star, premium, mass)
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);

        $image_add = array();
        $i_add = 0;
        $video_add = array();
        $v_add = 0;
        $captcha_add = array();
        $c_add = 0;
        $poll_add = array();
        $p_add = 0;
        $redeem_add = array();
        $r_add = 0;
		 $survey_add = array();
        $sur_add = 0;
        $data=array();
        //if any offer live(not shouut offer)
        if($datarr['resultCode'] == 1){

            foreach($datarr['offers'] as $datarr1){
                $captivelogarr=array("cp_offer_id"=>$datarr1['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr1['gender']),
                    "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr1['age_group']);
                if($datarr1['offer_type']=="coke_contest_add")
                {
                    $image_add[$i_add]=$this->get_image_ad_data($datarr1['cp_offer_id'], $location_type);
					$image_add[$i_add]['is_earn']=$datarr1['is_earned'];
                    $i_add++;
                    
                }
                else if($datarr1['offer_type']=="coke_video_add")
                {
                    $video_add[$v_add]=$this->get_video_ad_data($datarr1['cp_offer_id'], $location_type);
					$video_add[$v_add]['is_earn']=$datarr1['is_earned'];
                    $v_add++;
                   
                }

              
				 else if($datarr1['offer_type']=="coke_survey_add")
                {
                    $survey_add[$sur_add]=$this->get_survey_ad_data($datarr1['cp_offer_id'], $location_type);
                    $sur_add++;
                   
                }
            }
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['contest_add'] = $image_add;
            $data['video_add'] = $video_add;
			$data['survey_add'] = $survey_add;
        }

       
        else
        {
            $data['resultCode']='0';
            $data['resultMessage']='No Offer live';
			$data['contest_add'] = array();
            $data['video_add'] = array();
			$data['survey_add'] = array();
        }


       
        return $data;
    }
    // get offer id which are sceen to user for live brand
    public function get_ad_id_grid($jsondata) {
        $data=array();
		if($jsondata->offer_type=="coke_contest_add")
		{
		$offertype="coke_contest_add";	
		}
		else if($jsondata->offer_type=="coke_survey_add")
		{
			$offertype="coke_survey_add";	
		}
		else{
			$offertype="coke_video_add";	
		}
		
       
        $query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.cp_offer_id!='1'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add' and co.offer_type='".$offertype."'
                     and col.location_id='" . $jsondata->locationId . "' AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' and co.is_coke='1' ORDER BY co.cp_offer_id desc");
					 
		
			

        $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }

			

            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


                $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
				
				if($userdata[0]['gender']!="")
				{
					$gender=substr(strtoupper($userdata[0]['gender']),0,1);
				}	
				else{
					$gender="";
				}
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

                if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }
				if($filter==true)
				{
					$liveofferarr[] = $val->cp_offer_id;	
				}
				

               

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

               
				  if($filter==true)
				{
					$liveofferarr[] = $val->cp_offer_id;	
				}

            }


        }
        $liveofferarr = array_unique($liveofferarr);
	
        
        // get id who already sceen
        $query = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='" .
            $jsondata->userId . "'");

        $offer_sceen_array = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $offer_sceens) {
                $offer_sceen_array[] = $offer_sceens->cp_offer_id; // create array of offer sceen
            }
        }
		
		if($jsondata->offer_type=="coke_contest_add")
		{
		 $liveofferarr = $liveofferarr;
		}
		else{
			 $liveofferarr = array_diff($liveofferarr, $offer_sceen_array);
			 
		}
		
 
		
        if (!empty($liveofferarr)) {
            $i = 0;
            $data['resultCode'] = '1';
            $offers = array();
            foreach($liveofferarr as $liveofferarr1){
                $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$liveofferarr1."'");
                $rowarr=$query->row_array();
                $offers[$i]['cp_offer_id'] = $rowarr['cp_offer_id'];
                $offers[$i]['offer_type'] = trim($rowarr['offer_type']);
                $offers[$i]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
                ($userdata[0]['gender'],0,1):'';
                $offers[$i]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
                    ?$userdata[0]['age_group']:'';
					if(in_array($liveofferarr1,$offer_sceen_array))
					{
						 $offers[$i]['is_earned'] = 1;
					}
					else
					{
						$offers[$i]['is_earned'] = 0;
					}
                $i++;
            }
            $data['offers'] = $offers;
        }
        else {
            $data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
            $data['offer_type'] = 'default';
        }


        return $data;
    }
	
    // insert offer sceen by user , insert view in analytics
    public function cp_offer_sceen_user($jsondata){
        // insert offer view analytics
        $data=array();
        if(isset($jsondata->locationid) ){
            $user_detail = $this->user_detail($jsondata->userid);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }

            $captivelogarr=array("cp_offer_id"=>$jsondata->cpofferid,"location_id"=>$jsondata->locationid,
                "user_id"=>$jsondata->userid,'gender'=>strtoupper($gender),
                "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$age_group,"mbcost"=>$jsondata->mbcost);
            $this->add_coke_analytics($captivelogarr);
        }

        //budget cut and user sceen entry in case of vide add and image add
        if(isset($jsondata->offertype)){
            if($jsondata->offertype == 'coke_video_add' || $jsondata->offertype == 'video' ||
                $jsondata->offertype == 'coke_contest_add' || $jsondata->offertype == 'image' || $jsondata->offertype == 'app_install_add' || $jsondata->offertype == 'coke_survey_add'){
          		$offer_id = $jsondata->cpofferid;
                $user_id = $jsondata->userid;
                $location_id = '0';
                if(isset($jsondata->locationid) ){
                    $location_id = $jsondata->locationid;
                }
				
				$query = $this->DB2->query("insert into cp_adlocation_default(cp_offer_id, user_id, location_id,coke_contest_earn) VALUES
				('$offer_id', '$user_id', '$location_id','$jsondata->is_earn')");
            }

        }
      
        $data['resultCode'] = '1';
        return $data;
    }

  
    // offer detail in grid view
    public function captive_ad_data_grid_detail($jsondata) {
        $offer_id = $jsondata->offer_id;
        $offer_type = $jsondata->offer_type;
        $location_id = $jsondata->locationId;
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $offer_data = array();
        $data=array();


        if($offer_type=="coke_video_add" || $offer_type == 'video')
        {
            $offer_data=$this->get_video_ad_data($offer_id, $location_type);
        }
        else if($offer_type=="coke_contest_add" || $offer_type == 'image')
        {
            $offer_data = $this->get_image_ad_data($offer_id, $location_type);
			$query1 = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='".$jsondata->userId."' and cp_offer_id='".$offer_id."'");
			if($query1->num_rows()>0)
			{
			$offer_data['is_earn']=1;	
			}
			else{
				$offer_data['is_earn']=0;
			}
        }
      


        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        $data['add_detail'][0] = $offer_data;
        //Api Cp Analytivs logs
       
        return $data;
    }

    public function get_mb_data($jsondata)
    {
        $data=array();
        $query=$this->DB2->query("SELECT * from cp_mb_data where amt='".$jsondata->amount."'");
        if($query->num_rows()>0)
        {
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $i=0;
            foreach($query->result() as $val)
            {
                $data['mbdata'][$i]['data_name']=$val->data_name;
                $data['mbdata'][$i]['data_mb']=$val->data_mb;
                $data['mbdata'][$i]['amt']=$val->amt;

            }
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'No such plan';
        }
        return $data;

    }

  


    /* public function activate_voucher($jsondata)
    {
		
		$query2=$this->DB2->query("select city_id from wifi_location where id='".$jsondata->locationId."'");
		if($query2->num_rows()>0 || $jsondata->locationId=="9999")
		{
		$datarr=$query2->row_array();
		if($jsondata->locationId=="9999")
		{
			$location_id=$datarr['city_id'];
		}
		else
		{
			$location_id=$datarr['city_id'];
		}
		
        $data=array();
        $query=$this->DB2->query("SELECT mb,voucher_code FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."' and location_id='".$location_id."' and is_redeem=0");
        if($query->num_rows()>0)
        {
			$query1=$this->DB2->query("SELECT voucher_code FROM `coke_voucher_code_availed` WHERE user_id='".$jsondata->userId."' and date(availed_on)=CURDATE()");
			if($query1->num_rows()>=5)
			{
				$data['resultCode'] = '2';
            $data['resultMessage'] = 'Quota Completed for Voucher';
			$data['mb']=0;
			}
			else{
				 $rowarr=$query->row_array();
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['mb']=$rowarr['mb'];
			}
           

        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
        }
		}
		else{
			  $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
		}
        return $data;

    }


    public function voucher_availed($jsondata)
    {
        $data=array();
		$user_detail = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($jsondata->userId)));
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
			if($jsondata->locationId=="9999")
			{
				$location_id=$jsondata->locationId;
			}
			else
			{
				$query2=$this->DB2->query("select city_id from wifi_location where id='".$jsondata->locationId."'");	
		$datarr=$query2->row_array();
		$location_id=$datarr['city_id'];
			}
		
			
        $tabledata=array("is_redeem"=>1,"redeemed_on"=>date("Y-m-d H:i:s"));
        $where_array = array('location_id' => $location_id, 'voucher_code' => $jsondata->voucher);
        $this->DB2->where($where_array);
        $this->DB2->update('coke_voucher_code', $tabledata);
		if ($this->DB2->affected_rows() == '1') {
    //insert analytics for voucher avail
		$query=$this->DB2->query("SELECT code_type FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."' and location_id='".$location_id."' and is_redeem=1");
		$rowarr=$query->row_array();
		 $tabledata1=array("voucher_code"=>$jsondata->voucher,"user_id"=>$jsondata->userId,"platform"=>$jsondata->via,
		 "osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,
		 "code_type"=>$rowarr['code_type'],"availed_on"=>date("Y-m-d h:i:s"),"location_id"=>$jsondata->locationId,"city_id"=>$location_id);
		 
		 $this->DB2->insert('coke_voucher_code_availed',$tabledata1);
			
			}
			
		$data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;

    }
 
	*/
	
	
	 public function activate_voucher($jsondata)
    {
		
	/*	$query2=$this->DB2->query("select city_id from wifi_location where id='".$jsondata->locationId."'");
		if($query2->num_rows()>0 || $jsondata->locationId=="9999")
		{
		$datarr=$query2->row_array();
		if($jsondata->locationId=="9999")
		{
			$location_id=$datarr['city_id'];
		}
		else
		{
			$location_id=$datarr['city_id'];
		}*/
		
        $data=array();
        $query=$this->DB2->query("SELECT mb,voucher_code FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."' and is_redeem=0");
        if($query->num_rows()>0)
        {
			$query1=$this->DB2->query("SELECT voucher_code FROM `coke_voucher_code_availed` WHERE user_id='".$jsondata->userId."' and date(availed_on)=CURDATE()");
			if($query1->num_rows()>=5)
			{
				$data['resultCode'] = '2';
            $data['resultMessage'] = 'Quota Completed for Voucher';
			$data['mb']=0;
			}
			else{
				 $rowarr=$query->row_array();
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['mb']=$rowarr['mb'];
			}
           

        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
        }
		
        return $data;

    }


    public function voucher_availed($jsondata)
    {
        $data=array();
		$user_detail = $this->user_detail($jsondata->userId);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
			/*if($jsondata->locationId=="9999")
			{
				$location_id=$jsondata->locationId;
			}
			else
			{
				$query2=$this->DB2->query("select city_id from wifi_location where id='".$jsondata->locationId."'");	
		$datarr=$query2->row_array();
		$location_id=$datarr['city_id'];
			}*/
		
			
        $tabledata=array("is_redeem"=>1,"redeemed_on"=>date("Y-m-d H:i:s"));
        $where_array = array('voucher_code' => $jsondata->voucher);
        $this->DB2->where($where_array);
        $this->DB2->update('coke_voucher_code', $tabledata);
		if ($this->DB2->affected_rows() == '1') {
    //insert analytics for voucher avail
		$query=$this->DB2->query("SELECT code_type FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."'  and is_redeem=1");
		$rowarr=$query->row_array();
		 $tabledata1=array("voucher_code"=>$jsondata->voucher,"user_id"=>$jsondata->userId,"platform"=>$jsondata->via,
		 "osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,
		 "code_type"=>$rowarr['code_type'],"availed_on"=>date("Y-m-d h:i:s"),"location_id"=>$jsondata->locationId,"city_id"=>$jsondata->locationId);
		 
		 $this->DB2->insert('coke_voucher_code_availed',$tabledata1);
			
			}
			
		$data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;

    }
	
	//Survey Api
	
	
	public function captive_ad_survey_data($jsondata)
	{
		$surveyarr=$this->get_survey_id_arr($jsondata);
		if($surveyarr['resultCode']==1)
		{
			$data=$this->get_survey_question($surveyarr['offers'][0]['cp_offer_id'],$jsondata->locationId);
			
		}
		else
		{
			$data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
			 $data['is_quiz'] = '0';
			$data['question_data'] = array();
		}
		return $data;
		
	}
	
	
	public function get_survey_id_arr($jsondata) {
        $data=array();
		
			$offertype="coke_survey_add";	
		 $query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.cp_offer_id!='1'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add' and co.offer_type='".$offertype."'
                     and col.location_id='" . $jsondata->locationId . "' AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' and co.is_coke='1' ORDER BY co.cp_offer_id asc");
					

        $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }

			

            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


                $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
				
				if($userdata[0]['gender']!="")
				{
					$gender=substr(strtoupper($userdata[0]['gender']),0,1);
				}	
				else{
					$gender="";
				}
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

                if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

				if($filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

               if($filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }


        }
		
        $liveofferarr = array_unique($liveofferarr);
		
       $query = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='" .
            $jsondata->userId . "'");
			

        $offer_sceen_array = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $offer_sceens) {
                $offer_sceen_array[] = $offer_sceens->cp_offer_id; // create array of offer sceen
            }
        }
		
		
        $liveofferarr = array_diff($liveofferarr, $offer_sceen_array);
		
        if (!empty($liveofferarr)) {
            $i = 0;
            $data['resultCode'] = '1';
            $offers = array();
            foreach($liveofferarr as $liveofferarr1){
                $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$liveofferarr1."'");
                $rowarr=$query->row_array();
                $offers[$i]['cp_offer_id'] = $rowarr['cp_offer_id'];
                $offers[$i]['offer_type'] = trim($rowarr['offer_type']);
                $offers[$i]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
                ($userdata[0]['gender'],0,1):'';
                $offers[$i]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
                    ?$userdata[0]['age_group']:'';
                $i++;
            }
            $data['offers'] = $offers;
        }
        else {
            $data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
           
        }


        return $data;
    }
	
	
	
	public function get_survey_question($offerid,$locid)
	{
		$data=array();
		if($offerid!="")
		{
			$query=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offerid."' order by ques_id asc limit 1");
			$query2=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offerid."'");
			$query3=$this->DB2->query("select offer_title,is_quiz from cp_offers where cp_offer_id='".$offerid."'");
			$rowarr=$query->row_array();
			$rowarr1=$query3->row_array();
			
			$get_location_type = $this->DB2->query("select location_type from wifi_location where id ='$locid'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
		
			
			 $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_survey_add'");
            $mb_cost = '0';
            if($get_budget->num_rows() > 0){
                $budget_row = $get_budget->row_array();
                if($location_type == '1'){
                    $mb_cost = $budget_row['star'];
                    $mb_cost = ceil((1000*$mb_cost)/30);
                }
                elseif($location_type == '2'){
                    $mb_cost = $budget_row['premium'];
                    $mb_cost = ceil((1000*$mb_cost)/30);
                }
                elseif($location_type == '3'){
                    $mb_cost = $budget_row['mass'];
                    $mb_cost = ceil((1000*$mb_cost)/30);
                }
            }
			
		
			
			$data['resultCode']=1;
			$data['resultMsg']='Success';
			
			
			$data['offer_id']=$offerid;
			$data['is_quiz']=$rowarr1['is_quiz'];
			$data['total_question']=$query2->num_rows();
			$data['mb_cost']=$mb_cost;
			$data['promotion_name']=$rowarr1['offer_title'];
			
			$x=0;
			
			foreach($query2->result() as $val1){
			
			$data['question_data'][$x]['option_choice']=$val1->option_choice;
			$query1=$this->DB2->query("select option_id,`option_val`,right_ans from cp_survey_option where ques_id='".$val1->ques_id."'");
			$data['question_data'][$x]['question']=$val1->question;
			$data['question_data'][$x]['question_id']=$val1->ques_id;

			$i=0;
			foreach($query1->result() as $val)
			{
				$data['question_data'][$x]['option'][$i]['option_id']=$val->option_id;
				$data['question_data'][$x]['option'][$i]['option_name']=$val->option_val;
				$data['question_data'][$x]['option'][$i]['right_answer']=$val->right_ans;
				$i++;
			}
			$x++;
			}
			
			
		}
		else{
			$data['resultCode']=0;
			$data['resultMsg']='Fail';
		}
		
		return $data;
		
	}
	
	
	
	
	public function clear_user_option($jsondata)
	{
		
		 $this->DB2->where('user_id', $jsondata->userId);
		  $this->DB2->where('cp_offer_id', $jsondata->offer_id);
		 
      $this->DB2->delete('cp_survey_answer'); 
		
	}
	
	
	public function insert_user_option($jsondata)
	{
		
	
		  $user_detail = $this->user_detail($jsondata->userId);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
		
		$data=array();
		
	
		foreach($jsondata->items as $val)
		{
			
				$tabledata=array("question_id"=>$val->question_id,"option_id"=>$val->option_id,"user_id"=>$jsondata->userId,
				"cp_offer_id"=>$jsondata->offer_id,"platform"=>$jsondata->via,"location_id"=>$jsondata->locationId,"osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,"viewed_on"=>date("Y-m-d H:i:s"));
				$this->DB2->insert('coke_survey_answer',$tabledata);
		}
		$data['resultCode']=1;
		$data['resultMsg']='Success';
		return $data;
	}
	
	public function time_conversion($time)
	{
		

 // time duration in seconds

$days = floor($time / (60 * 60 * 24));
$time -= $days * (60 * 60 * 24);

$hours = floor($time / (60 * 60));
$time -= $hours * (60 * 60);

$minutes = floor($time / 60);
$time -= $minutes * 60;

$seconds = floor($time);
$time -= $seconds;
if($minutes>1)
{
	$time="{$minutes} Minutes {$seconds} Seconds";
}
else if($minutes==1)
{
	$time="{$minutes} Minutes";
}
else{
	$time="{$seconds} Seconds";
}

return $time;

	}
	
	//website api
	
	public function cp2_offers_totalmb_web($jsondata) {
        $data = array();
        
		$location_type = 1;
        $datarr = $this->get_ad_id_grid_web($jsondata);
        $video_budget = 0;
        $contest_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
        $survey_budget = 0;
        if($datarr['resultCode'] == 1){
            foreach($datarr['offers'] as $datarr1){
                if($datarr1['offer_type']=="coke_contest_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_contest_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $contest_budget = $contest_budget+$mb_cost;
                }
                if($datarr1['offer_type']=="coke_video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
             
                else if($datarr1['offer_type']=="coke_survey_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_survey_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $survey_budget = $survey_budget+$mb_cost;
                }
            }
        }
        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'coke_video_add';
        $offer_mb[0]['offer_mb'] = $video_budget;
        $offer_mb[1]['offer_type'] = 'coke_contest_add';
		 $offer_mb[1]['offer_mb'] = $contest_budget;
     
        $offer_mb[4]['offer_type'] = 'coke_survey_add';
        $offer_mb[4]['offer_mb'] = $survey_budget;
        $data['resultCode'] = 1;
        $data['resultMessage'] = 'Success';
        $data['offers_budget'] = $offer_mb;
        
        return $data;
    }
	
	
	
	 public function get_ad_id_grid_web($jsondata) {
        $data=array();
		if($jsondata->offer_type=="coke_contest_add")
		{
		$offertype="coke_contest_add";	
		}
		else if($jsondata->offer_type=="coke_survey_add")
		{
			$offertype="coke_survey_add";	
		}
		else{
			$offertype="coke_video_add";	
		}
		
       
        $query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.cp_offer_id!='1'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add' and co.offer_type='".$offertype."'
                     and  co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' and co.is_coke='1' and co.is_web_offer='1' ORDER BY co.cp_offer_id desc");
			 $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }

			

            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


                $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
				
				if($userdata[0]['gender']!="")
				{
					$gender=substr(strtoupper($userdata[0]['gender']),0,1);
				}	
				else{
					$gender="";
				}
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

                if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }
				if($filter==true)
				{
					$liveofferarr[] = $val->cp_offer_id;	
				}
				

               

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

               
				  if($filter==true)
				{
					$liveofferarr[] = $val->cp_offer_id;	
				}

            }


        }
        $liveofferarr = array_unique($liveofferarr);
	
        
        // get id who already sceen
        $query = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='" .
            $jsondata->userId . "'");

        $offer_sceen_array = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $offer_sceens) {
                $offer_sceen_array[] = $offer_sceens->cp_offer_id; // create array of offer sceen
            }
        }
		
		if($jsondata->offer_type=="coke_contest_add")
		{
		 $liveofferarr = $liveofferarr;
		}
		else{
			 $liveofferarr = array_diff($liveofferarr, $offer_sceen_array);
			 
		}
		
 
		
        if (!empty($liveofferarr)) {
            $i = 0;
            $data['resultCode'] = '1';
            $offers = array();
            foreach($liveofferarr as $liveofferarr1){
                $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$liveofferarr1."'");
                $rowarr=$query->row_array();
                $offers[$i]['cp_offer_id'] = $rowarr['cp_offer_id'];
                $offers[$i]['offer_type'] = trim($rowarr['offer_type']);
                $offers[$i]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
                ($userdata[0]['gender'],0,1):'';
                $offers[$i]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
                    ?$userdata[0]['age_group']:'';
					if(in_array($liveofferarr1,$offer_sceen_array))
					{
						 $offers[$i]['is_earned'] = 1;
					}
					else
					{
						$offers[$i]['is_earned'] = 0;
					}
                $i++;
            }
            $data['offers'] = $offers;
        }
        else {
            $data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
            $data['offer_type'] = 'default';
        }


        return $data;
    }
	
	
	   public function captive_ad_data_grid_web($jsondata) {
// get location type(star, premium, mass)
        
		
		$location_type='1';
        $datarr = $this->get_ad_id_grid_web($jsondata);

        $image_add = array();
        $i_add = 0;
        $video_add = array();
        $v_add = 0;
        $captcha_add = array();
        $c_add = 0;
        $poll_add = array();
        $p_add = 0;
        $redeem_add = array();
        $r_add = 0;
		 $survey_add = array();
        $sur_add = 0;
        $data=array();
        //if any offer live(not shouut offer)
        if($datarr['resultCode'] == 1){

            foreach($datarr['offers'] as $datarr1){
                $captivelogarr=array("cp_offer_id"=>$datarr1['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr1['gender']),
                    "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr1['age_group']);
                if($datarr1['offer_type']=="coke_contest_add")
                {
                    $image_add[$i_add]=$this->get_image_ad_data($datarr1['cp_offer_id'], $location_type);
					$image_add[$i_add]['is_earn']=$datarr1['is_earned'];
                    $i_add++;
                    
                }
                else if($datarr1['offer_type']=="coke_video_add")
                {
                    $video_add[$v_add]=$this->get_video_ad_data($datarr1['cp_offer_id'], $location_type);
					$video_add[$v_add]['is_earn']=$datarr1['is_earned'];
                    $v_add++;
                   
                }

              
				 else if($datarr1['offer_type']=="coke_survey_add")
                {
                    $survey_add[$sur_add]=$this->get_survey_ad_data($datarr1['cp_offer_id'], $location_type);
                    $sur_add++;
                   
                }
            }
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['contest_add'] = $image_add;
            $data['video_add'] = $video_add;
			$data['survey_add'] = $survey_add;
        }

       
        else
        {
            $data['resultCode']='0';
            $data['resultMessage']='No Offer live';
			$data['contest_add'] = array();
            $data['video_add'] = array();
			$data['survey_add'] = array();
        }


       
        return $data;
    }
	
	
	  public function cp_offer_sceen_user_web($jsondata){
        // insert offer view analytics
        $data=array();
        if(isset($jsondata->locationid) ){
            $user_detail = $this->user_detail($jsondata->userid);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }

            $captivelogarr=array("cp_offer_id"=>$jsondata->cpofferid,"location_id"=>$jsondata->locationid,
                "user_id"=>$jsondata->userid,'gender'=>strtoupper($gender),
                "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$age_group,"mbcost"=>$jsondata->mbcost);
            $this->add_coke_analytics($captivelogarr);
        }

        //budget cut and user sceen entry in case of vide add and image add
        if(isset($jsondata->offertype)){
            if($jsondata->offertype == 'coke_video_add' || $jsondata->offertype == 'video' ||
                $jsondata->offertype == 'coke_contest_add' || $jsondata->offertype == 'image' || $jsondata->offertype == 'app_install_add' || $jsondata->offertype == 'coke_survey_add'){
          		$offer_id = $jsondata->cpofferid;
                $user_id = $jsondata->userid;
                $location_id = '0';
                if(isset($jsondata->locationid) ){
                    $location_id = $jsondata->locationid;
                }
				
				$query = $this->DB2->query("insert into cp_adlocation_default(cp_offer_id, user_id, location_id,coke_contest_earn) VALUES
				('$offer_id', '$user_id', '$location_id','$jsondata->is_earn')");
            }

        }
      
        $data['resultCode'] = '1';
        return $data;
    }
	
	 // offer detail in grid view
    public function captive_ad_data_grid_detail_web($jsondata) {
        $offer_id = $jsondata->offer_id;
        $offer_type = $jsondata->offer_type;
        $location_id = $jsondata->locationId;
       
		 $location_type = 1;
        $offer_data = array();
        $data=array();


        if($offer_type=="coke_video_add" || $offer_type == 'video')
        {
            $offer_data=$this->get_video_ad_data($offer_id, $location_type);
        }
        else if($offer_type=="coke_contest_add" || $offer_type == 'image')
        {
            $offer_data = $this->get_image_ad_data($offer_id, $location_type);
			$query1 = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='".$jsondata->userId."' and cp_offer_id='".$offer_id."'");
			if($query1->num_rows()>0)
			{
			$offer_data['is_earn']=1;	
			}
			else{
				$offer_data['is_earn']=0;
			}
        }
      


        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        $data['add_detail'][0] = $offer_data;
        //Api Cp Analytivs logs
       
        return $data;
    }
	
	
	
	public function captive_ad_survey_data_web($jsondata)
	{
		$surveyarr=$this->get_survey_id_arr_web($jsondata);
		//echo "<pre>"; print_R($surveyarr); die;
		if($surveyarr['resultCode']==1)
		{
			$data=$this->get_survey_question_web($surveyarr['offers'][0]['cp_offer_id'],$jsondata->locationId);
			
		}
		else
		{
			$data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
			 $data['is_quiz'] = '0';
			$data['question_data'] = array();
		}
		return $data;
		
	}
	
	
	public function get_survey_id_arr_web($jsondata) {
        $data=array();
		
			$offertype="coke_survey_add";	
		 $query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.cp_offer_id!='1'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add' and co.offer_type='".$offertype."'
                     AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' and co.is_coke='1' and  co.is_web_offer='1' ORDER BY co.cp_offer_id asc");
					 
					 //echo $this->DB2->last_query();die;
					
					

        $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }

			

            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


                $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
				
				if($userdata[0]['gender']!="")
				{
					$gender=substr(strtoupper($userdata[0]['gender']),0,1);
				}	
				else{
					$gender="";
				}
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

                if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

				if($filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

               if($filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }


        }
		//echo "<pre>"; print_R($liveofferarr);die;
        $liveofferarr = array_unique($liveofferarr);
       $query = $this->DB2->query("SELECT `cp_offer_id` FROM `cp_adlocation_default` WHERE user_id='" .
            $jsondata->userId . "'");

        $offer_sceen_array = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $offer_sceens) {
                $offer_sceen_array[] = $offer_sceens->cp_offer_id; // create array of offer sceen
            }
        }
		
        $liveofferarr = array_diff($liveofferarr, $offer_sceen_array);
		
        if (!empty($liveofferarr)) {
            $i = 0;
            $data['resultCode'] = '1';
            $offers = array();
            foreach($liveofferarr as $liveofferarr1){
                $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$liveofferarr1."'");
                $rowarr=$query->row_array();
                $offers[$i]['cp_offer_id'] = $rowarr['cp_offer_id'];
                $offers[$i]['offer_type'] = trim($rowarr['offer_type']);
                $offers[$i]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
                ($userdata[0]['gender'],0,1):'';
                $offers[$i]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
                    ?$userdata[0]['age_group']:'';
                $i++;
            }
            $data['offers'] = $offers;
        }
        else {
            $data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
           
        }


        return $data;
    }
	
	
	
	public function get_survey_question_web($offerid,$locid)
	{
		$data=array();
		if($offerid!="")
		{
			$query=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offerid."' order by ques_id asc limit 1");
			$query2=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offerid."'");
			$query3=$this->DB2->query("select offer_title,is_quiz from cp_offers where cp_offer_id='".$offerid."'");
			$rowarr=$query->row_array();
			$rowarr1=$query3->row_array();
			
			
			$location_type =1;
		
			
			 $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'coke_survey_add'");
            $mb_cost = '0';
            if($get_budget->num_rows() > 0){
                $budget_row = $get_budget->row_array();
                if($location_type == '1'){
                    $mb_cost = $budget_row['star'];
                    $mb_cost = ceil((1000*$mb_cost)/30);
                }
                elseif($location_type == '2'){
                    $mb_cost = $budget_row['premium'];
                    $mb_cost = ceil((1000*$mb_cost)/30);
                }
                elseif($location_type == '3'){
                    $mb_cost = $budget_row['mass'];
                    $mb_cost = ceil((1000*$mb_cost)/30);
                }
            }
			
		
			
			$data['resultCode']=1;
			$data['resultMsg']='Success';
			
			
			$data['offer_id']=$offerid;
			$data['is_quiz']=$rowarr1['is_quiz'];
			$data['total_question']=$query2->num_rows();
			$data['mb_cost']=$mb_cost;
			$data['promotion_name']=$rowarr1['offer_title'];
			
			$x=0;
			
			foreach($query2->result() as $val1){
			
			$data['question_data'][$x]['option_choice']=$val1->option_choice;
			$query1=$this->DB2->query("select option_id,`option_val`,right_ans from cp_survey_option where ques_id='".$val1->ques_id."'");
			$data['question_data'][$x]['question']=$val1->question;
			$data['question_data'][$x]['question_id']=$val1->ques_id;
			

			$i=0;
			foreach($query1->result() as $val)
			{
				$data['question_data'][$x]['option'][$i]['option_id']=$val->option_id;
				$data['question_data'][$x]['option'][$i]['option_name']=$val->option_val;
				$data['question_data'][$x]['option'][$i]['right_answer']=$val->right_ans;
				$i++;
			}
			$x++;
			}
			
			
		}
		else{
			$data['resultCode']=0;
			$data['resultMsg']='Fail';
		}
		
		return $data;
		
	}
	
	/*
	 public function activate_voucher_web($jsondata)
    {
        $data=array();
        $query=$this->DB2->query("SELECT mb,voucher_code FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."' and location_id='".$jsondata->locationId."' and is_redeem=0");
        if($query->num_rows()>0)
        {
			$query1=$this->DB2->query("SELECT voucher_code FROM `coke_voucher_code_availed` WHERE user_id='".$jsondata->userId."' and date(availed_on)=CURDATE()");
			if($query1->num_rows()>=5)
			{
				$data['resultCode'] = '2';
            $data['resultMessage'] = 'Quota Completed for Voucher';
			$data['mb']=0;
			}
			else{
				 $rowarr=$query->row_array();
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['mb']=$rowarr['mb'];
			}
           

        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
        }
        return $data;

    }
	
	
	 public function voucher_availed_web($jsondata)
    {
        $data=array();
		$user_detail = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($jsondata->userId)));
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
			
			
        $tabledata=array("is_redeem"=>1,"redeemed_on"=>date("Y-m-d H:i:s"));
        $where_array = array('location_id' => $jsondata->locationId, 'voucher_code' => $jsondata->voucher);
        $this->DB2->where($where_array);
        $this->DB2->update('coke_voucher_code', $tabledata);
		if ($this->DB2->affected_rows() == '1') {
    //insert analytics for voucher avail
		$query=$this->DB2->query("SELECT code_type FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."' and location_id='".$jsondata->locationId."' and is_redeem=1");
		$rowarr=$query->row_array();
		 $tabledata1=array("voucher_code"=>$jsondata->voucher,"user_id"=>$jsondata->userId,"platform"=>$jsondata->via,
		 "osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,
		 "code_type"=>$rowarr['code_type'],"availed_on"=>date("Y-m-d h:i:s"),"location_id"=>$jsondata->locationId);
		 
		 $this->DB2->insert('coke_voucher_code_availed',$tabledata1);
			
			}
			
		$data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;

    }*/
	
	
	 public function activate_voucher_web($jsondata)
    {
		
	/*	$query2=$this->DB2->query("select city_id from wifi_location where id='".$jsondata->locationId."'");
		if($query2->num_rows()>0 || $jsondata->locationId=="9999")
		{
		$datarr=$query2->row_array();
		if($jsondata->locationId=="9999")
		{
			$location_id=$datarr['city_id'];
		}
		else
		{
			$location_id=$datarr['city_id'];
		}*/
		
        $data=array();
        $query=$this->DB2->query("SELECT mb,voucher_code FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."' and is_redeem=0");
        if($query->num_rows()>0)
        {
			$query1=$this->DB2->query("SELECT voucher_code FROM `coke_voucher_code_availed` WHERE user_id='".$jsondata->userId."' and date(availed_on)=CURDATE()");
			if($query1->num_rows()>=5)
			{
				$data['resultCode'] = '2';
            $data['resultMessage'] = 'Quota Completed for Voucher';
			$data['mb']=0;
			}
			else{
				 $rowarr=$query->row_array();
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['mb']=$rowarr['mb'];
			}
           

        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
        }
		
        return $data;

    }


    public function voucher_availed_web($jsondata)
    {
        $data=array();
		$user_detail = $this->user_detail($jsondata->userId);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
			/*if($jsondata->locationId=="9999")
			{
				$location_id=$jsondata->locationId;
			}
			else
			{
				$query2=$this->DB2->query("select city_id from wifi_location where id='".$jsondata->locationId."'");	
		$datarr=$query2->row_array();
		$location_id=$datarr['city_id'];
			}*/
		
			
        $tabledata=array("is_redeem"=>1,"redeemed_on"=>date("Y-m-d H:i:s"));
        $where_array = array('voucher_code' => $jsondata->voucher);
        $this->DB2->where($where_array);
        $this->DB2->update('coke_voucher_code', $tabledata);
		if ($this->DB2->affected_rows() == '1') {
    //insert analytics for voucher avail
		$query=$this->DB2->query("SELECT code_type FROM `coke_voucher_code` WHERE voucher_code='".$jsondata->voucher."'  and is_redeem=1");
		$rowarr=$query->row_array();
		 $tabledata1=array("voucher_code"=>$jsondata->voucher,"user_id"=>$jsondata->userId,"platform"=>$jsondata->via,
		 "osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,
		 "code_type"=>$rowarr['code_type'],"availed_on"=>date("Y-m-d h:i:s"),"location_id"=>$jsondata->locationId,"city_id"=>$jsondata->locationId);
		 
		 $this->DB2->insert('coke_voucher_code_availed',$tabledata1);
			
			}
			
		$data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;

    }
	
	
	public function insert_user_option_web($jsondata)
	{
		
	
		  $user_detail = $this->user_detail($jsondata->userId);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
		
		$data=array();
		
	
		foreach($jsondata->items as $val)
		{
			
				$tabledata=array("question_id"=>$val->question_id,"option_id"=>$val->option_id,"user_id"=>$jsondata->userId,
				"cp_offer_id"=>$jsondata->offer_id,"platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,"viewed_on"=>date("Y-m-d H:i:s"));
		$this->DB2->insert('coke_survey_answer',$tabledata);
		}
		$data['resultCode']=1;
		$data['resultMsg']='Success';
		return $data;
	}
	
	
	 public function coke_products_mbdata_web($jsondata)
	{
		$data=array();
		$query=$this->DB2->query("SELECT * FROM  `coke_code_type` where location_id='".$jsondata->locationId."'");
		$data['resultCode']='1';
        $data['resultMessage']='Success';
		$i=0;
		$rgb=0;$fountain=0;$can=0;$pet=0;
		$codearr=array();
		foreach($query->result() as $val)
		{
			
			$data['mb_budget'][$i]['offer_type']=strtolower(str_replace("/","_",$val->code_type_name));
			$data['mb_budget'][$i]['offer_mb']=strtolower($val->mb);
			$codearr[]=$val->code_type_name;
			$i++;
		}
		
		if(!in_array('RGB/FOUNTAIN/TETRA',$codearr))
		{
			$data['mb_budget'][5]['offer_type']='rgb_fountain_tetra';
			$data['mb_budget'][5]['offer_mb']="0";
		}
	if(!in_array('KINLEY',$codearr))
		{
			$data['mb_budget'][6]['offer_type']='kinley';
			$data['mb_budget'][6]['offer_mb']="0";
		}
	 if(!in_array('CAN',$codearr))
		{
			$data['mb_budget'][7]['offer_type']='can';
			$data['mb_budget'][7]['offer_mb']="0";
		}
		if(!in_array('PET',$codearr))
		{
			$data['mb_budget'][8]['offer_type']='pet';
			$data['mb_budget'][8]['offer_mb']="0";
		}
		$data['mb_budget']=array_values($data['mb_budget']);
		return $data;
	}
	
	
	  public function coke_user_web($jsondata){
        // insert offer view analytics
        $data=array();
        
            $user_detail = $this->user_detail($jsondata->userId);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }

            $captivelogarr=array("user_id"=>$jsondata->userId,'gender'=>strtoupper($gender),
                "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"age_group"=>$age_group
				,"viewed_on"=>date("Y-m-d H:i:s"));
         $this->DB2->insert('coke_website_user',$captivelogarr);


       
      
        $data['resultCode'] = '1';
        return $data;
    }
	
	public function captive_portal_impression($jsondata)
	{
        $today_date = date('Y-m-d');
		$locationId=$jsondata->locationId;
        $macid = '';
        if(isset($jsondata->macid)){
            $macid = $jsondata->macid;
            $location_id = '';
            if(isset($jsondata->locationId)){
                $location_id = $jsondata->locationId;
            }
            $osinfo = '';
            if(isset($jsondata->platform_filter)){
                $osinfo = $jsondata->platform_filter;
            }
            $query = $this->DB2->query("select macid from channel_unique_daily_user where location_id = '$location_id' AND macid = '$macid' and DATE(visit_date) = '$today_date'");
            if($query->num_rows() < 1){
                $insert = $this->DB2->query("insert into channel_unique_daily_user(location_id, macid, osinfo, visit_date)
             values('$location_id', '$macid', '$osinfo', now())");
            }
        }

   

		
		   $check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
    location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
   if($check_impression->num_rows() > 0){
    $row = $check_impression->row_array();
    $temp_id = $row['id'];
    $update_cp = $this->DB2->query("update channel_captiveportal_impression set
     total_impression = (total_impression+1) WHERE id = '$temp_id'");
   }
   else{
    $insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
    (location_id, total_impression, visit_date) VALUES ('$locationId',
    '1', now())");
   }
   
	}

	
	
	
	
	
	
}

?>
