<?php
class Setup_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
    }
    
    public function authentication($authKey)
    {
        $data          = array();
        $check_authkey = $this->DB2->query("select * from third_party_apikey where authKey = '$authKey'");
        if ($check_authkey->num_rows() > 0) {
            $row                = $check_authkey->row_array();
            $data['authkey_id'] = $row['id'];
            $data['resultCode'] = '1';
            $data['resultMsg']  = 'success';
        } else {
            $data['resultCode'] = '0';
            $data['resultMsg']  = 'Invalid auth key';
        }
        
        return $data;
    }
    
    public function log($function, $request, $responce)
    {
        $log_data = array(
            'function_name' => $function,
            'request' => $request,
            'responce' => $responce,
            'time' => date("d-m-Y H:i:s")
        );
        $this->mongo_db->insert('log_data', $log_data);
    }
    
    
    public function add_plan($jsondata, $client_id)
    {
        $isp_uid            = ISPUID;
        $data               = array();
        $data['resultCode'] = 1;
        $data['resultMsg']  = "Success";
        
        if (isset($jsondata->plan)) {
            if (!isset($jsondata->plan->plan_name) || $jsondata->plan->plan_name == "") {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Plan Name";
            }
            if (!isset($jsondata->plan->plan_desc) || $jsondata->plan->plan_desc == "") {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Plan Desc";
            }
            if (!isset($jsondata->plan->dwnload_rate) || $jsondata->plan->dwnload_rate == "") {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Download rate";
            } else {
                if (!is_numeric($jsondata->plan->dwnload_rate)) {
                    $data['resultCode'] = 0;
                    $data['resultMsg']  = "Invalid Download rate";
                }
            }
            
            if (!isset($jsondata->plan->upload_rate) || $jsondata->plan->upload_rate == "") {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Upload rate";
            } else {
                if (!is_numeric($jsondata->plan->upload_rate)) {
                    $data['resultCode'] = 0;
                    $data['resultMsg']  = "Invalid Upload rate";
                }
            }
            
            
            
            $tabledata  = array();
            $tabledata1 = array();
            if (isset($jsondata->plan->plan_type) && $jsondata->plan->plan_type == "TP") {
                if (!isset($jsondata->plan->timelimit) || $jsondata->plan->timelimit == "") {
                    $data['resultCode'] = 0;
                    $data['resultMsg']  = "Invalid Time Limit";
                } else {
                    if (!is_numeric($jsondata->plan->timelimit)) {
                        $data['resultCode'] = 0;
                        $data['resultMsg']  = "Invalid Time Limit";
                    }
                }
                $timelimit  = $jsondata->plan->timelimit * 60;
                $dwnld_rate = $this->convertToBytes($jsondata->plan->dwnload_rate . "KB");
                $upld_rate  = $this->convertToBytes($jsondata->plan->upload_rate . "KB");
                
                $tabledata = array(
                    "srvname" => $jsondata->plan->plan_name,
                    "descr" => $jsondata->plan->plan_desc,
                    "enableplan" => 0,
                    "plantype" => 2,
                    "datacalc" => 0,
                    "fupuprate" => 0,
                    "fupdownrate" => 0,
                    "datalimit" => 0,
                    "downrate" => $dwnld_rate,
                    "isp_uid" => $isp_uid,
                    "uprate" => $upld_rate,
                    "timelimit" => $timelimit,
                    "timecalc" => 0,
                    "client_id" => $client_id,
                    "enableplan" => 1
                );
                
                
            } else if (isset($jsondata->plan->plan_type) && $jsondata->plan->plan_type == "DP") {
                
                if (!isset($jsondata->plan->data_limit) || $jsondata->plan->data_limit == "") {
                    $data['resultCode'] = 0;
                    $data['resultMsg']  = "Invalid Data Limit";
                } else {
                    if (!is_numeric($jsondata->plan->data_limit)) {
                        $data['resultCode'] = 0;
                        $data['resultMsg']  = "Invalid Data Limit";
                    }
                }
                
                $datalimit  = $this->convertToBytes($jsondata->plan->data_limit . "MB");
                $dwnld_rate = $this->convertToBytes($jsondata->plan->dwnload_rate . "KB");
                $upld_rate  = $this->convertToBytes($jsondata->plan->dwnload_rate . "KB");
                $datecal    = 2;
                $tabledata  = array(
                    "srvname" => $jsondata->plan->plan_name,
                    "descr" => $jsondata->plan->plan_desc,
                    "enableplan" => 0,
                    "plantype" => 4,
                    "fupuprate" => 0,
                    "fupdownrate" => 0,
                    "downrate" => $dwnld_rate,
                    "isp_uid" => $isp_uid,
                    "uprate" => $upld_rate,
                    "datalimit" => $datalimit,
                    "datacalc" => $datecal,
                    "enableplan" => 1,
                    "client_id" => $client_id
                );
                
            }
            
            else {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Plan Type";
                
            }
            
            
            
            if ($data['resultCode'] == 1) {
                if (isset($jsondata->burst_mode) && $jsondata->burst_mode == "1") {
                    
                    $datavalid = $this->validate_burst($jsondata);
                    if ($datavalid['resultCode'] == 1) {
                        // echo "<pre>"; print_R($postdata);die;
                        $dlburstlimit     = $this->convertToBytes($jsondata->burst_data->burst_download_speed . "KB");
                        $dlburstthreshold = $this->convertToBytes($jsondata->burst_data->burst_download_threshold . "KB");
                        $ulburstlimit     = $this->convertToBytes($jsondata->burst_data->burst_upload_speed . "KB");
                        $ulburstthreshold = $this->convertToBytes($jsondata->burst_data->burst_upload_threshold . "KB");
                        $bursttime        = $jsondata->burst_data->burst_time * 3600;
                        //
                        $tabledata1       = array(
                            "dlburstlimit" => $dlburstlimit,
                            "dlburstthreshold" => $dlburstthreshold,
                            "ulburstlimit" => $ulburstlimit,
                            "ulburstthreshold" => $ulburstthreshold,
                            "bursttime" => $bursttime,
                            "priority" => $jsondata->burst_data->burst_priority,
                            "enableburst" => 1
                        );
                    } else {
                        $data = $datavalid;
                    }
                    
                }
                
                $fnltabledata = array_merge($tabledata, $tabledata1);
                if ($data['resultCode'] == 1) {
                    $query = $this->DB2->query("select srvid from sht_services where srvname='" . $fnltabledata['srvname'] . "' and client_id='" . $client_id . "'");
                    if ($query->num_rows() > 0) {
                        $rowarr             = $query->row_array();
                        $data['resultCode'] = 0;
                        $data['resultMsg']  = "Plan already present";
                        $data['planid']     = $rowarr['srvid'];
                    } else {
                        $this->DB2->insert('sht_services', $fnltabledata);
                        $data['resultCode'] = 1;
                        $data['resultMsg']  = "Success";
                        $data['planid']     = $this->DB2->insert_id();
                    }
                }
                
                
            }
            
            
            //burst mode
            
            
        } else {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Plan Request";
        }
        return $data;
        
        
    }
    
    public function validate_burst($jsondata)
    {
        $data               = array();
        $data['resultCode'] = 1;
        
        if (!isset($jsondata->burst_data->burst_download_speed) || $jsondata->burst_data->burst_download_speed == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Burst Download rate";
        } else {
            if (!is_numeric($jsondata->burst_data->burst_download_speed)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Burst Download rate";
            }
        }
        
        
        if (!isset($jsondata->burst_data->burst_upload_speed) || $jsondata->burst_data->burst_upload_speed == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Burst Upload rate";
        } else {
            if (!is_numeric($jsondata->burst_data->burst_upload_speed)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Burst Download rate";
            }
        }
        
        if (!isset($jsondata->burst_data->burst_download_threshold) || $jsondata->burst_data->burst_download_threshold == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Burst Download threshold";
        } else {
            if (!is_numeric($jsondata->burst_data->burst_download_threshold)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Burst Download threshold";
            }
        }
        
        if (!isset($jsondata->burst_data->burst_upload_threshold) || $jsondata->burst_data->burst_upload_threshold == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Burst Upload threshold";
        } else {
            if (!is_numeric($jsondata->burst_data->burst_upload_threshold)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Burst Upload threshold";
            }
        }
        
        if (!isset($jsondata->burst_data->burst_time) || $jsondata->burst_data->burst_time == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid burst time";
        } else {
            if (!is_numeric($jsondata->burst_data->burst_time)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid  burst time";
            }
        }
        
        if (!isset($jsondata->burst_data->burst_priority) || $jsondata->burst_data->burst_priority == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Burst priority";
        } else {
            if (!is_numeric($jsondata->burst_data->burst_priority)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Burst priority";
            }
        }
        
        return $data;
        
    }
    
    public function add_nas($jsondata,$client_id)
    {
         $isp_uid            = ISPUID;
        $data=array();
         $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
         if (!isset($jsondata->nasname) || $jsondata->nasname == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Nas Name";
        }
       
           if (!isset($jsondata->nasip) || $jsondata->nasip == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Nas IP";
        } else {
            if (!filter_var($jsondata->nasip, FILTER_VALIDATE_IP)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Nas IP";
            }
        }
        
         if (!isset($jsondata->nassecret) || $jsondata->nassecret == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Nas Secret";
        }
        
        if( $data['resultCode'] == 1)
        {
           $query=$this->DB2->query("select id from nas where nasname='".$jsondata->nasip."'");
           if($query->num_rows()>0)
           {
            $rowarr=$query->row_array();
              $data['resultCode'] = 0;
            $data['resultMsg']  = "Nas already Added";
            $data['nasid']  = $rowarr['id'];
           }
           else{
             $tabledata=array("nasname"=>$jsondata->nasip,"shortname"=>$jsondata->nasname,
                             "secret"=>$jsondata->nassecret,"description"=>"RADIUS Client","nastype"=>1,"ports"=>0,
                            "isp_uid"=>$isp_uid,"created_on"=>date("Y-m-d H:i:s"),"type"=>0
                             ,"client_id"=>$client_id);
             $this->DB2->insert('nas',$tabledata);
                $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
            $data['nasid']  =  $this->DB2->insert_id();
              $url=NASCONF;
              $mobile="9873834499";
              $postdata=array("mobileno"=>$mobile,"ip"=>$jsondata->nasip,
                              "nas_name"=>$jsondata->nasname,"nas_secret"=>$jsondata->nassecret);
              //  $url="http://103.20.213.150/clientsconf/script.php";
               $this->nas_curlhit($url,$postdata);
               
           }
           
        }
        else
        {
        $data=$data;
        }
        
        
        return $data;
        
        
        
    }
    
    public function register_user($jsondata,$client_id)
    {
          $isp_uid            = ISPUID;
        $data=array();
         $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
         if (!isset($jsondata->mobileno) || $jsondata->mobileno == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
        }
        else{
            if(strlen(trim($jsondata->mobileno))!=10)
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
            }
        }
        
         
         if (!isset($jsondata->firstname) || $jsondata->firstname == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid First Name";
        }
        
        if (!isset($jsondata->lastname) || $jsondata->lastname == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid last Name";
        }
       
           if (!isset($jsondata->email) || $jsondata->email == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Email";
        } else {
            if (!filter_var($jsondata->email, FILTER_VALIDATE_EMAIL)) {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Email";
            }
        }
        if($data['resultCode'] == 1)
        {
            $query=$this->DB2->query("select id from sht_users where uid='".$jsondata->mobileno."'");
            if($query->num_rows()>0)
            {
                  $clrpwd="1234";
                $pwd=md5($clrpwd);
                $tabledata=array("password"=>$pwd,"mac"=>$jsondata->macid);
                 $this->DB2->update("sht_users",$tabledata,array("uid"=>$jsondata->mobileno));
                // echo "===>>".$this->DB2->last_query(); die;
                $tabledata1=array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password",
                                  "op"=>":=","value"=>$clrpwd);
                 $this->DB2->update("radcheck",$tabledata1,array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password"));
                $tabledata2=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use",
                                  "op"=>":=","value"=>1);
                $this->DB2->update("radcheck",$tabledata2,array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
             $data['resultCode'] = 0;
                $data['resultMsg']  = "User Already Registered";
                $data['pwd']=$clrpwd;
            }
            else{
                $clrpwd="1234";
                $pwd=md5($clrpwd);
                
                $tabledata=array("isp_uid"=>$isp_uid,"uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,
                                 "password"=>$pwd,
                                 "enableuser"=>1,"firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,
                                 "email"=>$jsondata->email,
                                 "mobile"=>$jsondata->mobileno,"createdon"=>date("Y-m-d H:i:s")
                                 ,"createdby"=>'admin',"owner"=>'admin',"mac"=>$jsondata->macid);
                $this->DB2->insert("sht_users",$tabledata);
                $tabledata1=array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password",
                                  "op"=>":=","value"=>$clrpwd);
                 $this->DB2->insert("radcheck",$tabledata1);
                $tabledata2=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use",
                                  "op"=>":=","value"=>1);
                $this->DB2->insert("radcheck",$tabledata2);
                
                $data['resultCode'] = 1;
                $data['resultMsg']  = "Success";
                $data['pwd']=$clrpwd;
                
            }
        }
        return $data;
      //  $query=$this->DB2->query("select id from sht_users where uid='".$jsondata->."'")
        
        
    }
    
    public function data_balance($jsondata)
    {
          $isp_uid            = ISPUID;
        $data=array();
         $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
         if (!isset($jsondata->mobileno) || $jsondata->mobileno == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
        }
        else{
            if(strlen(trim($jsondata->mobileno))!=10)
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
            }
        }
        if($data['resultCode']==1)
        {
            $query=$this->DB2->query("select baseplanid from sht_users where uid='".$jsondata->mobileno."'");
            if($query->num_rows()>0)
            {
                $rowarr=$query->row_array();
                if($rowarr['baseplanid']==0)
                {
                    $data['resultCode'] = 0;
            $data['resultMsg']  = "Plan not assignet to user"; 
                }
                else{
                    $datamb=$this->get_current_data($jsondata->mobileno);
                     $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
            $data['balance']=$datamb." mb";
                }
                
            }
            else
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "User Not exist";
            }
            
        }
        return $data;
    }
    
    
     public function data_expiry($jsondata)
    {
          $isp_uid            = ISPUID;
        $data=array();
         $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
         if (!isset($jsondata->mobileno) || $jsondata->mobileno == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
        }
        else{
            if(strlen(trim($jsondata->mobileno))!=10)
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
            }
        }
        if($data['resultCode']==1)
        {
            $query=$this->DB2->query("select baseplanid,expiration from sht_users where uid='".$jsondata->mobileno."'");
            if($query->num_rows()>0)
            {
                $rowarr=$query->row_array();
                if($rowarr['baseplanid']==0)
                {
                    $data['resultCode'] = 0;
            $data['resultMsg']  = "Plan not assignet to user"; 
                }
                else{
                    $datamb=$this->get_current_data($jsondata->mobileno);
                     $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
            $data['expirydate']=$rowarr['expiration'];
                }
                
            }
            else
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "User Not exist";
            }
            
        }
        return $data;
    }
    
    
     public function recharge_account($jsondata)
    {
          $isp_uid            = ISPUID;
        $data=array();
         $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
         if (!isset($jsondata->mobileno) || $jsondata->mobileno == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
        }
        else{
            if(strlen(trim($jsondata->mobileno))!=10)
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mobile No";
            }
        }
         if (!isset($jsondata->planid) || $jsondata->planid == "") {
                $data['resultCode'] = 0;
                $data['resultMsg']  = "Invalid Planid";
            } else {
                if (!is_numeric($jsondata->planid)) {
                    $data['resultCode'] = 0;
                    $data['resultMsg']  = "Invalid Planid";
                }
            }
        
        if($data['resultCode']==1)
        {
            $query=$this->DB2->query("select `plantype`,`datalimit`,`timelimit`,`srvid` from sht_services where srvid='".$jsondata->planid."'");
            if($query->num_rows()>0)
            {
                $rowarr=$query->row_array();
                if($rowarr['plantype']==2)
                {
                   
                    $tabledata=array("expiration"=> date('Y-m-d', strtotime("+30 days")),"uptimelimit"=>$rowarr['timelimit'],
                                     "baseplanid"=>$rowarr['srvid']);
                    $this->DB2->update('sht_users',$tabledata);
                    
                    $data['resultCode'] = 1;
                 $data['resultMsg']  = "Success"; 
                }
                else if($rowarr['plantype']==4){
                    $datamb=$this->get_current_data($jsondata->mobileno);
                   
                    if($datamb==0)
                    {
                       $tabledata=array("expiration"=> date('Y-m-d', strtotime("+30 days")),"comblimit"=>$rowarr['datalimit'],
                                     "baseplanid"=>$rowarr['srvid']);
                    $this->DB2->update('sht_users',$tabledata,array("uid"=>$jsondata->mobileno));
                   
                    }
                    else
                    {
                        $expiration=date('Y-m-d', strtotime("+30 days"));
                        $datalimit=$rowarr['datalimit'];
                        $baseid=$rowarr['srvid'];
                        $this->DB2->query("update sht_users set comblimit=comblimit+$datalimit ,expiration=$datalimit,baseplanid=$expiration where uid='".$jsondata->mobileno."'");
                    }
                    
                     $data['resultCode'] = 1;
                    $data['resultMsg']  = "Success";
           
                }
                else
                {
                    $data['resultCode'] = 1;
                     $data['resultMsg']  = "Recharge Unsuccesfull"; 
                }
                
            }
            else
            {
             $data['resultCode'] = 0;
            $data['resultMsg']  = "Plan Not exist";
            }
            
        }
        return $data;
    }
    
    
     public function get_current_data($username){
	
	$query1 = $this->DB2->query("SELECT comblimit FROM `sht_users` WHERE  `username`='".$username."'");
       
	//$num_rows1 = $query1->num_rows;
        
	$rowdata1 = $query1->row_array();
       
	if($rowdata1['comblimit']<0)
	{
		$availabledatamb=0;
	}
	else{
		$availabledatamb=$rowdata1['comblimit']/(1024*1024);
	}
	
        return ceil($availabledatamb);
    }
    
      public function nas_curlhit($url,$postdata)
    {
        $postData = array(
            'mobile' => $postdata['mobileno'],
            'clientip' => $postdata['ip'],
            'shortname' => $postdata['nas_name'],
            'secret'=>$postdata['nas_secret']
            
        );
        $url=$url;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
    }
    
    public function login($jsondata)
    {
        
          $isp_uid            = ISPUID;
        $data=array();
         $data['resultCode'] = 1;
            $data['resultMsg']  = "Success";
        if (!isset($jsondata->macid) || $jsondata->macid == "") {
            $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid Mac ID";
        }
        if($data['resultCode']==1)
        {
            $query=$this->DB2->query("select uid from sht_users where mac='".$jsondata->macid."' and uid='".$jsondata->mobileno."'");
            if($query->num_rows()>0)
            {
                $rowarr=$query->row_array();
                $query1=$this->DB2->query("select value from radcheck where username='".$rowarr['uid']."' and attribute='Cleartext-Password'");
                $rowarr1=$query1->row_array();
                $data['username']=$rowarr['uid'];
                $data['password']=$rowarr1['value'];
            }
            else{
                $data['resultCode'] = 0;
            $data['resultMsg']  = "Invalid User";
            }
        }
        else{
            $data=$data;
        }
        return $data;
        
        
    }
    
    
    
    public function convertToBytes($from)
    {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number * 1024;
            case "MB":
                return $number * 1024 * 1024;
            case "GB":
                return $number * 1024 * 1024 * 1024;
            case "TB":
                return $number * 1024 * 1024 * 1024 * 1024;
            case "PB":
                return $number * 1024 * 1024 * 1024 * 1024 * 1024;
            default:
                return $from;
        }
    }
    
    public function convertTodata($from)
    {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number / pow(1024, 2);
            case "GB":
                return $number / pow(1024, 3);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }
    
    function timeToSeconds($time)
    {
        
        return $time * 3600;
    }
    
    
    
    
         public function location_autogenerate_id($isp_uid){
        $data = array();
        //get last locationid
        $id = 0;
        $query = $this->DB2->query("select id from wifi_location order by id desc limit 1");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $id = $row['id'];
        }
        $id = $id+1;
        // id will be next id
        // check last next id assign from temp table
        $next_id_assigned = 0;
        $query1 = $this->DB2->query("select location_id from wifi_locationid_temp order by id desc limit 1");
        if($query1->num_rows() > 0){
            $row1 = $query1->row_array();
            $next_id_assigned = $row1['location_id'];
        }
        // which will be next id
        $next_id = '';
        if($next_id_assigned >= $id ){
            $next_id = $next_id_assigned+1;
        }else{
            $next_id = $id;
        }
        // insert in temp table
        $this->DB2->query("insert into wifi_locationid_temp(location_id, isp_uid, created_on) values('$next_id' , '$isp_uid', now())");
        $data['location_id'] = $isp_uid.$next_id;
        return $data;
    }
     
     public function add_location($jsondata, $authkey_id){
	  $data = array();
        $isp_uid = '';$location_type = '';$location_name = '';$geo_address = '';$placeid = '';$lat = '';$long = '';
	$address1 = '';$address2 = '';$pin = '';$state = '';$city = '';$zone = '';$location_id = '';$contact_person_name='';
	$email_id = '';$mobile_no = '';$city_name = '';$state_name = '';
	  $isp_uid = ISPUID;
	if(isset($jsondata->location_type)){
	  $location_type = $jsondata->location_type;
	}
	if(isset($jsondata->location_name)){
	  $location_name = $jsondata->location_name;
	}
	if(isset($jsondata->geo_address)){
	  $geo_address = $jsondata->geo_address;
	}
	if(isset($jsondata->placeid)){
	  $placeid = $jsondata->placeid;
	}
	if(isset($jsondata->lat)){
	  $lat = $jsondata->lat;
	}
	if(isset($jsondata->long)){
	  $long = $jsondata->long;
	}
	if(isset($jsondata->address1)){
	  $address1 = $jsondata->address1;
	}
	if(isset($jsondata->address2)){
	  $address2 = $jsondata->address2;
	}
	if(isset($jsondata->pin)){
	  $pin = $jsondata->pin;
	}
	if(isset($jsondata->contact_person_name)){
	  $contact_person_name = $jsondata->contact_person_name;
	}
	if(isset($jsondata->email_id)){
	  $email_id = $jsondata->email_id;
	}
	if(isset($jsondata->mobile_no)){
	  $mobile_no = $jsondata->mobile_no;
	}
	if($location_name != ''){
	  //check location name already exist
	  $check_loc = $this->DB2->query("select location_uid from wifi_location where location_name = '$location_name'");
	  if($check_loc->num_rows() <= '0'){
	       $autoid = $this->location_autogenerate_id($isp_uid);
	       $location_id = $autoid['location_id'];
	       $insert = $this->DB2->query("insert into wifi_location(location_uid,location_type, location_name, geo_address, placeid, latitude, longitude,region_id, address_1, address_2, state,state_id, city,city_id, pin, contact_person_name, mobile_number, status, is_deleted, created_on, isp_uid, user_email, location_brand, provider_id, client_id)
                                   values('$location_id','$location_type', '$location_name', '$geo_address', '$placeid', '$lat', '$long', '$zone', '$address1', '$address2', '$state_name', '$state', '$city_name', '$city', '$pin', '$contact_person_name', '$mobile_no', '1', '0', now(), '$isp_uid', '$email_id', 'sh', '$isp_uid', '$authkey_id')");
	       $data['resultCode'] = '1';
	       $data['resultMsg'] = 'Success';
	       $data['location_uid'] = $location_id;
	  }else{
	       $data['resultCode'] = '0';
	       $data['resultMsg'] = 'Location Already exist';
	  }
	 
	}else{
	       $data['resultCode'] = '0';
	       $data['resultMsg'] = 'Invalid parameters';
	}
	return $data;
     }
     
     public function get_locations($authkey_id){
	  $data = array();
	  $get = $this->DB2->query("select location_name, location_uid from wifi_location where client_id = '$authkey_id'");
	  if($get->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $data['resultMsg'] = 'Success';
	       $locations = array();
	       $i = 0;
	       foreach($get->result() as $row){
		    $locations[$i]['location_name'] = $row->location_name;
		    $locations[$i]['location_uid'] = $row->location_uid;
		    $i++;
	       }
	       $data['locations'] = $locations;
	  }else{
	      $data['resultCode'] = '0';
	      $data['resultMsg'] = 'No location created';
	  }
	  
	  return $data;
     }
     public function nas_plas_assoc($jsondata, $authkey_id){
	  $isp_uid = ISPUID;
	  $data = array();
	  $location_uid = ''; $planid = '';$nasid = '';
	  if(isset($jsondata->location_uid)){
	       $location_uid = $jsondata->location_uid;
	  }
	  if(isset($jsondata->planId)){
	       $planid = $jsondata->planId;
	  }
	  if(isset($jsondata->nasId)){
	       $nasid = $jsondata->nasId;
	  }
	  if($location_uid != '' && $planid != '' && $nasid != ''){
	       // check plan id is valid
	       $check_plan = $this->DB2->query("select srvid from sht_services where client_id = '$authkey_id' and srvid = '$planid'");
	       if($check_plan->num_rows() > 0){
		    // check nas valid nas id 
		    $check_nas = $this->DB2->query("select id, nasname, is_used from nas where client_id = '$authkey_id' and id = '$nasid'");
		    if($check_nas->num_rows() > 0){
			 $nas_row = $check_nas->row_array();
			 // check location with already configure with other nas or not
			 $check_location = $this->DB2->query("select id,nasipaddress,nasid,nasconfigure from wifi_location where location_uid = '$location_uid' and client_id = '$authkey_id'");
			 if($check_location->num_rows() > 0){
			      $row = $check_location->row_array();
			      $location_id = $row['id'];
			      if($row['nasconfigure'] == '1' && $nasid != $row['nasid']){
				   $data['resultCode'] = '0';
				   $data['resultMsg'] = 'Already other nas associated this location';
			      }else{
				   $nasipaddress = $nas_row['nasname'];
				   if($row['nasconfigure'] == '1'){// update only nas plan
					$update_cp_type = $this->DB2->query("update wifi_location_cptype set cp_client_planid = '$planid', updated_on = now() where uid_location = '$location_uid'");
					$sht_plan_check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$planid' AND nasid = '$nasid'");
					if($sht_plan_check->num_rows() <= 0){
					     $insert_sht_plan = $this->DB2->query("insert into sht_plannasassoc(srvid, nasid, created_on) values('$planid', '$nasid', now())");
					}
					$data['resultCode'] = '1';
					$data['resultMsg'] = 'Success';
				   }else{// firt time configure
					if($nas_row['is_used'] == '1'){
					     $data['resultCode'] = '0';
					     $data['resultMsg'] = 'Nas is already associated with other location';
					}else{
					     // insert into wifi_location
					     $update_loc = $this->DB2->query("update wifi_location set nasipaddress = '$nasipaddress', nasid = '$nasid', nasconfigure = '1', modified_on = now() where location_uid = '$location_uid'");
					     $Insert_cptype  = $this->DB2->query("insert into wifi_location_cptype(uid_location, isp_uid, location_id, cp_type, cp_client_planid, created_on) values('$location_uid', '$isp_uid', '$location_id', 'cp_client', '$planid', now())");
					     $sht_plan_check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$planid' AND nasid = '$nasid'");
					     if($sht_plan_check->num_rows() <= 0){
						  $insert_sht_plan = $this->DB2->query("insert into sht_plannasassoc(srvid, nasid, created_on) values('$planid', '$nasid', now())");
					     }
					     //update nas table
					     $update_nas = $this->DB2->query("update nas set is_used = '1' where id = '$nasid'");
					     $data['resultCode'] = '1';
					     $data['resultMsg'] = 'Success';
					}
					
				   }
				   
			      }
			 }else{
			      $data['resultCode'] = '0';
			      $data['resultMsg'] = 'Invalid location uid';
			 }
		    }else{
			 $data['resultCode'] = '0';
			 $data['resultMsg'] = 'Invalid nasid id';
		    }
	       }else{
		    $data['resultCode'] = '0';
		    $data['resultMsg'] = 'Invalid Plan id';
	       }
	  }else{
	       $data['resultCode'] = '0';
	       $data['resultMsg'] = 'Invalid parameters';
	  }
	  return $data;
     }
     public function configure_router($jsondata,$authkey_id){
	  $data = array();
	  $router_ip = '';$router_user = '';$router_password = '';$router_port = '';
	  $location_uid = '';$router_type = '';$ssid = 'SHOUUT_FREE_WIFI';
	  if(isset($jsondata->ssid)){
	       $ssid = $jsondata->ssid;
	  }
	  if(isset($jsondata->router_type)){
	       $router_type = strtolower($jsondata->router_type);
	  }
	  if(isset($jsondata->location_uid)){
	       $location_uid = $jsondata->location_uid;
	  }
	  if(isset($jsondata->router_ip)){
	       $router_ip = $jsondata->router_ip;
	  }
	  if(isset($jsondata->router_user)){
	       $router_user = $jsondata->router_user;
	  }
	  if(isset($jsondata->router_password)){
	       $router_password = $jsondata->router_password;
	  }
	  if(isset($jsondata->router_port)){
	       $router_port = $jsondata->router_port;
	  }
	  
	  $get_nasip = $this->DB2->query("select id,nasipaddress,nasid,nasconfigure from wifi_location where location_uid = '$location_uid' and client_id = '$authkey_id' and nasconfigure = '1'");
	  
	  if($get_nasip->num_rows() > 0){
	      $row_get_nasip = $get_nasip->row_array();
	       $this->load->library('routerlib');
	       if($router_port != ''){
		    $conn = $this->router_conn = RouterOS::connect("$router_ip", "$router_user", "$router_password", "$router_port");
	       }else{
		    //echo $router_ip."----".$router_user."---".$router_password;die;
		    $conn = $this->router_conn = RouterOS::connect("$router_ip", "$router_user", "$router_password");	
	       }
	       if($conn != ''){
		    //check router is valid and properly reset
		    $check_valid = $this->check_wap_router_reset_porperly($conn,$router_type);
		    // 3 mean not valid router, 2 mean router not reset properly, 1 mean ok
		    if($check_valid == '1'){
			 $nasid = $row_get_nasip['nasid'];
			 $get_nasname = $this->DB2->query("select nasname,shortname,secret from nas where id = '$nasid'");
			 $secret = '';
			 if($get_nasname->num_rows() > 0){
			     $row = $get_nasname->row_array();
			     $secret = $row['secret'];
			 }
			 $subdomain_ip = '';
			 if($router_type == 'wap'){
			      $data = $this->step_execute_wapac($conn, $ssid, $secret,$subdomain_ip);
			  }elseif($router_type == 'wapac'){
			      $data = $this->step_execute_wapac($conn, $ssid, $secret,$subdomain_ip);
			  }elseif($router_type == 'rb850gx2'){
			       $data = $this->step_execute_wapac($conn, $ssid, $secret,$subdomain_ip);
			  }
			  elseif($router_type == 'rb941-2nd'){
			       $data = $this->step_execute_wapac($conn, $ssid, $secret,$subdomain_ip);
			  }elseif($router_type == '2011il'){
			       $data = $this->step_execute_wapac($conn, $ssid, $secret,$subdomain_ip);
			  }
			  elseif($router_type == 'ccr1036-8g-2s'){
			       $data = $this->step_execute_wapac($conn, $ssid, $secret,$subdomain_ip);
			  }
			 $data['resultCode'] = '1';
			 $data['resultMsg'] = 'Success'; 
		    }else{
			 if($check_valid == '3'){
			      $data['resultCode'] = '0';
			      $data['resultMsg'] = 'Invalid router type'; 
			 }elseif($check_valid == '2'){
			      $data['resultCode'] = '0';
			      $data['resultMsg'] = 'Router not reset properly'; 
			 }
			 
		    }
		    
	       }else{
		   $data['resultCode'] = '0';
		   $data['resultMsg'] = 'Router not connected'; 
	       }
	  }else{
	       $data['resultCode'] = '0';
	       $data['resultMsg'] = 'Invalid location uid'; 
	  }
	  
	  
	  
	  return $data;
     }
     public function check_wap_router_reset_porperly($conn,$router_type){
	 
	  // check router type select and connected are match
	  $check_router_type = $conn->getall("/system/routerboard");
	  //print_r($check_router_type);
	  if(isset($check_router_type['model'])){
	       if($router_type == 'wap'){
		    if($check_router_type['model'] != 'RouterBOARD wAP 2nD'){
			 return 3;
		    }
	       }
	       elseif($router_type == 'wapac'){
		    if($check_router_type['model'] != 'RouterBOARD wAP G-5HacT2HnD'){
			 return 3;
		    }
	       }elseif($router_type == 'rb850gx2'){
		    if($check_router_type['model'] != '850Gx2'){
			 return 3;
		    }
	       }
	       elseif($router_type == 'rb941-2nd'){
		    if($check_router_type['model'] == '941-2nD' || $check_router_type['model'] == 'RouterBOARD 941-2nD'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == '2011il'){
		    if($check_router_type['model'] != '2011L'){
			 return 3;
		    }
	       }
	       elseif($router_type == 'ccr1036-8g-2s'){
		    if($check_router_type['model'] == 'CCR1036-8G-2S+'){
			
		    }else{
			  return 3;
		    }
	       }
	  }
	  // check router reset properly or not
	  //1. see if there is an IP address 192.168.88.1 in /ip address
	  $check_ip = $conn->getall("/ip/address");
	  foreach($check_ip as $check_ip1){
	       if($check_ip1['address'] == '192.168.88.1'){
		    return 2;
	       }
	  }
	  //2. check if there is any firewall rule in /ip firewall filter
	  $check_firewall = $conn->getall("/ip/firewall/filter");
	  if(count($check_firewall) > 0){
	       return 2;
	  }
	  //3. check if there is any interface by the name of bridge1 in /interface
	  $check_interface = $conn->getall("/interface");
	  foreach($check_interface as $check_interface1){
	       if($check_interface1['name'] == 'bridge1'){
		    return 2;
	       }
	  }
	  
	  // in 850gx2 check ip set on either 1
	  if($router_type == 'rb850gx2' || $router_type == '2011il' || $router_type == 'ccr1036-8g-2s'){
	       $is_either1_set = '0';
	       foreach($check_ip as $check_ip1){
		    if($check_ip1['interface'] == 'ether1'){
			 if($check_ip1['address'] != ''){
			      $is_either1_set = '1';
			 }
		    }
	       }
	       if($is_either1_set == '0'){
		    return 2;
	       }
	  }
	  return 1;
     }

     public function step_execute_wap($conn, $ssid, $secret,$subdomain_ip){
	
	  $new_ip = '';
		$Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
		if($Ip_address_get->num_rows() > 0){
			$row = $Ip_address_get->row_array();
			$Ip = $row['ip'];
			$Ip =  explode('.',$Ip);
			if($Ip[2] == '255'){
				$Ip[1]++;
				$Ip[2] =0;
			}
			else{
				$Ip[2]++;
			}
			$new_ip = implode('.',$Ip);
			$Ip1 =  explode('.',$new_ip);
			$Ip1[3] = 0;
			$new_address = implode('.',$Ip1);

			$Ip2 =  explode('.',$new_ip);
			$Ip2[3] = 10;
			$from = implode('.',$Ip2);
			$Ip3 =  explode('.',$new_ip);
			$Ip3[3] = 254;
			$to = implode('.',$Ip3);
			$poll_range = $from.'-'.$to;
		}
		else{
			$poll_range = "192.168.36.10-192.168.36.254";
			$new_ip = "192.168.36.1";
			$new_address = "192.168.36.0";
		}
	
	  //C. Main configuration:
	       //1. Setting ports:
	       $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	       $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	       $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	       $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
		
		//2. Setting Wireless:
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1", "mode" => "ap-bridge", "band" => "2ghz-b/g/n", "frequency" => "auto", "channel-width" => "20mhz", "radio-name" => "SHOUUT", "default-forwarding" => "no", "ssid" =>"$ssid", "name" => "shouut", "disabled"=> "no", "wireless-protocol"=> "802.11"));
	       
	       $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	       $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	      
	       //3. Setting up DHCP and Hotspot:
	       $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	       $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	       $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	       $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	       $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"2"));
	       
	       //4. Setting up RADIUS server:
	       $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	       $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"137.59.98.15","secret"=>"$secret","timeout"=>"2000ms"));
	       
	       //5. Preparing for logs:
	       $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"137.59.98.15", "remote-port"=>"514"));
	       $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	       $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	      
	       //6. Preparing access for first time login for users:
	       //whitelist subdomain
	       if($subdomain_ip != ''){
		    $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	       }
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"137.59.98.15","comment"=>"137.59.98.15"));
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"103.20.213.149","comment"=>"shouut.com"));
		
		$walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
		
		$walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	
		
		$walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
		
		$walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
		
		$walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
		
		$walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
		
		$walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
		
		$walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
		
		$walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
		
		$walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
		
		$walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
		
	       //7. Securing the router:
	       $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	       $insert_ip = $this->DB2->query("insert into wifi_router_ip (ip, created_on) values('$new_ip', now())");
		 
	       //9. User Permission
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	       $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));


    }





            public function step_execute_wapac($conn, $ssid, $secret,$subdomain_ip){
	 
	  $new_ip = '';
		$Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
		if($Ip_address_get->num_rows() > 0){
			$row = $Ip_address_get->row_array();
			$Ip = $row['ip'];
			$Ip =  explode('.',$Ip);
			if($Ip[2] == '255'){
				$Ip[1]++;
				$Ip[2] =0;
			}
			else{
				$Ip[2]++;
			}
			$new_ip = implode('.',$Ip);
			$Ip1 =  explode('.',$new_ip);
			$Ip1[3] = 0;
			$new_address = implode('.',$Ip1);

			$Ip2 =  explode('.',$new_ip);
			$Ip2[3] = 10;
			$from = implode('.',$Ip2);
			$Ip3 =  explode('.',$new_ip);
			$Ip3[3] = 254;
			$to = implode('.',$Ip3);
			$poll_range = $from.'-'.$to;
		}
		else{
			$poll_range = "192.168.36.10-192.168.36.254";
			$new_ip = "192.168.36.1";
			$new_address = "192.168.36.0";
		}
	
	  //C. Main configuration:
	       //1. Setting ports:
	       $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	       $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	       $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	       $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
		
		//2. Setting Wireless:
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1","mode"=>"ap-bridge","band"=>"2ghz-b/g/n","frequency"=>"auto","channel-width"=>"20mhz","radio-name"=> "SHOUUT","default-forwarding" => "no",   "ssid" => "$ssid", "disabled"=>"no", "wireless-protocol"=> "802.11"));
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan2", "mode"=>"ap-bridge","band"=>"5ghz-a/n/ac","frequency"=>"auto","channel-width"=>"20/40mhz-Ce", "radio-name"=> "SHOUUT","default-forwarding"=> "no","ssid"=>"SHOUUT_FREE_HISPEED", "disabled"=>"no", "wireless-protocol"=> "802.11"));
	       
	       $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut", "disabled"=>"no"));
	       $interface_bridge_port = $conn->add("/interface/bridge/port",array("interface"=>"wlan1", "bridge"=>"shouut"));
	       $interface_bridge_port1 = $conn->add("/interface/bridge/port",array("interface"=>"wlan2", "bridge"=>"shouut"));
	       
	       $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	       $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	      
	       //3. Setting up DHCP and Hotspot:
	       $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	       $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	       $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	       $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	       $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"2"));
	       
	       //4. Setting up RADIUS server:
	       $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	       $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"137.59.98.15","secret"=>"$secret","timeout"=>"2000ms"));
	       
	       //5. Preparing for logs:
	       $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"137.59.98.15", "remote-port"=>"514"));
	       $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	       $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	      
	       //6. Preparing access for first time login for users:
	       //whitelist subdomain
	       if($subdomain_ip != ''){
		    $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	       }
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"137.59.98.15","comment"=>"137.59.98.15"));
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"103.20.213.149","comment"=>"shouut.com"));
		
		$walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
		
		$walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	
		
		$walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
		
		$walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
		
		$walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
		
		$walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
		
		$walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
		
		$walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
		
		$walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
		
		$walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
		
		$walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
		
	       //7. Securing the router:
	       $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	       $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	       $insert_ip = $this->DB2->query("insert into wifi_router_ip (ip, created_on) values('$new_ip', now())");
		
	       //9. User Permission
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	       $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));


    }


         public function step_execute_Rb850Gx2($conn, $ssid, $secret,$subdomain_ip){
	 
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether5 = $conn->set("/interface/ethernet",array(".id"=>"ether5","master-port"=>"ether2"));
	  $interface_set_ether2 = $conn->set("/interface/ethernet",array(".id"=>"ether2","name"=>"shouut"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"2"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"137.59.98.15","secret"=>"$secret","timeout"=>"2000ms"));
	  
	  //5. Preparing for logs:
	  $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"137.59.98.15", "remote-port"=>"514"));
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  //whitelist subdomain
	  if($subdomain_ip != ''){
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	  }
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"137.59.98.15","comment"=>"137.59.98.15"));
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"103.20.213.149","comment"=>"shouut.com"));
	  $walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
	  $walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	  $walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
	  $walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
	  $walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
	  $walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
	  $walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
	  $walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
	  $walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
	  $walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
	  $walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_ip = $this->DB2->query("insert into wifi_router_ip (ip, created_on) values('$new_ip', now())");
	     //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
     }
	

        public function step_execute_Rb941_2nD($conn, $ssid, $secret,$subdomain_ip){
	 
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1","mode"=>"ap-bridge","band"=>"2ghz-b/g/n","frequency"=>"auto","channel-width"=>"20mhz","radio-name"=> "SHOUUT","default-forwarding" => "no",   "ssid" => "$ssid", "disabled"=>"no", "wireless-protocol"=> "802.11"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut","disabled"=>"no"));
	  $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut","disabled"=>"no"));
	  $interface_bridge_port_wan1 = $conn->add("/interface/bridge/port",array("interface"=>"wlan1", "bridge"=>"shouut","disabled"=>"no"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"2"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"137.59.98.15","secret"=>"$secret","timeout"=>"2000ms"));
	  
	  //5. Preparing for logs:
	  $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"137.59.98.15", "remote-port"=>"514"));
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  //whitelist subdomain
	  if($subdomain_ip != ''){
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	  }
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"137.59.98.15","comment"=>"137.59.98.15"));
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"103.20.213.149","comment"=>"shouut.com"));
	  $walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
	  $walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	  $walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
	  $walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
	  $walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
	  $walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
	  $walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
	  $walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
	  $walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
	  $walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
	  $walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_ip = $this->DB2->query("insert into wifi_router_ip (ip, created_on) values('$new_ip', now())");
	    
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	   
	  
     }


         public function step_execute_2011il($conn, $ssid, $secret,$subdomain_ip){
	 
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_set_ether7 = $conn->set("/interface/ethernet",array(".id"=>"ether7","master-port"=>"ether6"));
	  $interface_set_ether8 = $conn->set("/interface/ethernet",array(".id"=>"ether8","master-port"=>"ether6"));
	  $interface_set_ether9 = $conn->set("/interface/ethernet",array(".id"=>"ether9","master-port"=>"ether6"));
	  $interface_set_ether10 = $conn->set("/interface/ethernet",array(".id"=>"ether10","master-port"=>"ether6"));
	  $interface_set_ether6 = $conn->set("/interface/ethernet",array(".id"=>"ether6","name"=>"shouut"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"2"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"137.59.98.15","secret"=>"$secret","timeout"=>"2000ms"));
	  
	  //5. Preparing for logs:
	  $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"137.59.98.15", "remote-port"=>"514"));
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  //whitelist subdomain
	  if($subdomain_ip != ''){
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	  }
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"137.59.98.15","comment"=>"137.59.98.15"));
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"103.20.213.149","comment"=>"shouut.com"));
	  $walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
	  $walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	  $walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
	  $walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
	  $walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
	  $walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
	  $walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
	  $walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
	  $walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
	  $walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
	  $walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_ip = $this->DB2->query("insert into wifi_router_ip (ip, created_on) values('$new_ip', now())");
	 
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
     }



   	     public function step_execute_CCR1036_8G_2S($conn, $ssid, $secret,$subdomain_ip){
	 
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether3", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether4 = $conn->add("/interface/bridge/port",array("interface"=>"ether4", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether5 = $conn->add("/interface/bridge/port",array("interface"=>"ether5", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether6 = $conn->add("/interface/bridge/port",array("interface"=>"ether6", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether7 = $conn->add("/interface/bridge/port",array("interface"=>"ether7", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether8 = $conn->add("/interface/bridge/port",array("interface"=>"ether8", "bridge"=>"shouut", "disabled"=>"no"));
	
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"2"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"137.59.98.15","secret"=>"$secret","timeout"=>"2000ms"));
	  
	  //5. Preparing for logs:
	  $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"137.59.98.15", "remote-port"=>"514"));
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  //whitelist subdomain
	  if($subdomain_ip != ''){
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	  }
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"137.59.98.15","comment"=>"137.59.98.15"));
	  $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"103.20.213.149","comment"=>"shouut.com"));
	  $walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
	  $walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	  $walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
	  $walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
	  $walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
	  $walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
	  $walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
	  $walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
	  $walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
	  $walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
	  $walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_ip = $this->DB2->query("insert into wifi_router_ip (ip, created_on) values('$new_ip', now())");
	      
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
     }




}