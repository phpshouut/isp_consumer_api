<?php

class Captiveportal_model extends CI_Model {

	  public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
		$this->load->helper('date');
    }
	

	
    public function authenticateApiuser($user_id) {
	
		
		$this->DB2->select("id");
		$this->DB2->from("sht_users");
		$this->DB2->where(array("uid"=>$user_id));
		$query=$this->DB2->get();
		
	
	//	echo "select id from sht_users where uid='".$user_id."'";
       // $query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
		
        $num =$query->num_rows();
		//echo $num;die;
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function authenticateApiGuestuser($guest_user_id) {
      $this->DB2->select("id");
		$this->DB2->from("sht_users");
		$this->DB2->where(array("uid"=>$guest_user_id));
		$query=$this->DB2->get();
		
       // $query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
		
        $num =$query->num_rows();
		//echo $num;die;
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }

	
    public function log($function, $request, $responce) {
        $log_data = array(
            'function' => $function,
            'request' => $request,
            'response' => json_encode($responce),
            'added_on' => date("d-m-Y H:i:s"),
        );
        $this->DB2->insert('cp_logs', $log_data);
		//echo $this->DB2->last_query(); die;
    }
	
	public function get_loc_logo($jsondata)
	{
		$this->DB2->select("cp_type, original_image,logo_image");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$get_cp_type=$this->DB2->get();
		
               if($get_cp_type->num_rows() > 0){
                   $row = $get_cp_type->row_array();
                   $cp_type = $row['cp_type'];
            //logo
            if($row['logo_image'] != ''){
            $brand_logo = CDNPATH.'isp/isp_location_logo/logo/'.$row['logo_image'];
            }elseif($row['original_image'] != ''){
            $brand_logo  = CDNPATH.'isp/isp_location_logo/original/'.$row['original_image'];
            }
			else{
				$brand_logo="";
			}
               }
			   else{
				   $brand_logo="";
			   }
			   return $brand_logo;
	}

    public function captive_ad_data($jsondata) {

        $datarr = $this->get_ad_id($jsondata);
//echo "<pre>"; print_R($datarr);// die;
		
		$loc_logo=$this->get_loc_logo($jsondata);
        $data=array();
     
       //daily vissit entry
	  $this->user_dailyvisit_data($jsondata);
      $captivelogarr=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr['gender']),
	  "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr['age_group']);
        if($datarr['offer_type']=="image_add")
        {
            $data=$this->get_image_ad_data($datarr['cp_offer_id']);
			if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
             $this->add_cp_analytics($captivelogarr);
           //  $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
        else if($datarr['offer_type']=="video_add")
        {
            $data=$this->get_video_ad_data($datarr['cp_offer_id']);
			if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
             $this->add_cp_analytics($captivelogarr);
           //   $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
        
        else if($datarr['offer_type']=="poll_add")
        {
             $data=$this->get_poll_ad_data($datarr['cp_offer_id'],$jsondata->locationId,$jsondata->userId);
			 if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
              $this->add_cp_analytics($captivelogarr);
            //    $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
       else if($datarr['offer_type']=="captcha_add")
        {
             $data=$this->get_brand_captcha_data($datarr['cp_offer_id']);
			 if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
              $this->add_cp_analytics($captivelogarr);
              // $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
         else if($datarr['offer_type']=="redeemable_add")
        {
             $data=$this->get_redeemable_offer_data($datarr['cp_offer_id']);
			 if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
              $this->add_cp_analytics($captivelogarr);
            //   $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
		 else if($datarr['offer_type']=="cost_per_lead_add")
        {
            $data=$this->get_cost_per_lead_add_data($datarr['cp_offer_id']);
			if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
            $this->add_cp_analytics($captivelogarr);
            //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
		 else if($datarr['offer_type']=="survey_add")
                {
                    $data=$this->get_survey_ad_data($datarr['cp_offer_id']);
					//echo "<pre>"; print_R($data);die;
						if($loc_logo!="")
						{
							$data['brand_logo_image']=$loc_logo;
							$data['brand_original_image']=$loc_logo;
						}
                   // $sur_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
         }
		else if($datarr['offer_type']=="scratch_add")
        {
            $data=$this->get_scratch_ad_data($datarr['cp_offer_id'],$jsondata,$loc_logo);
			if($loc_logo!="")
			{
				$data['brand_logo_image']=$loc_logo;
            $data['brand_original_image']=$loc_logo;
			}
             $this->add_cp_analytics($captivelogarr);
           //   $this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
        }
		else if($datarr['offer_type']=="default")
        {
			$data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='image';
            $data['withoffer']='0';
            $data['mb_cost'] = '';
           $data['brand_name']='';
            $data['brand_id']='';
			$data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']='';
            $data['promotion_desc']='';
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']='';
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']='';
			 if($loc_logo!="")
			{
				
				$this->DB2->select("retail_small_image");
				$this->DB2->from("wifi_location_cptype");
				$this->DB2->where(array("location_id"=>$jsondata->locationId));
				$query1=$this->DB2->get();
				
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					if($rowarr['retail_small_image']!='')
					{
						$data['offers_small_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
						$data['offers_original_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
					}
					else{
						$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
					}
					
				}
				else{
					$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
				}
				
				  
				$data['brand_logo_image']=$loc_logo;
				$data['brand_original_image']=$loc_logo;
			}
			else{
				
				$this->db->select("logo_image");
				$this->db->from("sht_isp_detail");
				$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
				$query=$this->db->get();
				
				
				if($query->num_rows() > 0){
					$row = $query->row_array();
					$loc_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
				}
				$data['brand_logo_image']=$loc_logo;
				$data['brand_original_image']=$loc_logo;
				
				$this->DB2->select("retail_small_image");
				$this->DB2->from("wifi_location_cptype");
				$this->DB2->where(array("location_id"=>$jsondata->locationId));
				$query1=$this->DB2->get();
				
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					if($rowarr['retail_small_image']!='')
					{
						$data['offers_small_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
						$data['offers_original_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
					}
					else{
						$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
					}
					
				}
				else{
					$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
				}
			}
			
		}
        else
        {
           $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='image';
            $data['withoffer']='0';
            $data['mb_cost'] = '';
           $data['brand_name']='';
            $data['brand_id']='';
			$data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']='';
            $data['promotion_desc']='';
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']='';
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
			 if($loc_logo!="")
			{
				  $data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
				$data['brand_logo_image']=$loc_logo;
				$data['brand_original_image']=$loc_logo;
			}
			else{
				
				$this->DB2->select("small_image");
				$this->DB2->from("sht_isp_detail");
				$this->DB2->where(array("isp_uid"=>$jsondata->isp_uid));
				$query=$this->DB2->get();
				if($query->num_rows() > 0){
					$row = $query->row_array();
					$loc_logo = ISP_IMAGEPATH."logo/".$row['small_image'];
				}
				$data['brand_logo_image']=$loc_logo;
				$data['brand_original_image']=$loc_logo;
				$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
          
			}
        }
        //Api Cp Analytivs logs
         
        return $data;
    }




    public function get_ad_id($jsondata) {
        $data=array();
     
			$now=date("Y-m-d H:i:s");	
			$this->DB2->select('co.cp_offer_id,co.is_repeat,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id');
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array("co.cp_offer_id!="=>'1',"co.offer_type!="=>"offer_add" ,"co.offer_type!="=>"banner_add","co.offer_type!="=>"flash_add"
                    ,"co.offer_type!="=>"app_install_add","col.location_id"=>$jsondata->locationId,"co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now,
                    "co.is_deleted" => "0", "co.status" => '1',"co.is_coke"=>"0" ,"co.is_mtnl_default_ad"=>"0"));
			$this->DB2->join('cp_offers_location as col', 'co.cp_offer_id = col.cp_offer_id', 'INNER');
			 $this->DB2->order_by("co.cp_offer_id","desc");
			 $query=$this->DB2->get();
			//echo $this->DB2->last_query(); die;		 
		$liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }


            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


               $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
                if($userdata[0]['gender']!="")
				{
					$gender=substr(strtoupper($userdata[0]['gender']),0,1);
				}	
				else{
					$gender="";
				}
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

               if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

				$liveofferarr[] = $val->cp_offer_id;
                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                    
                }

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }
				$liveofferarr[] = $val->cp_offer_id;
                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                   // $liveofferarr[] = $val->cp_offer_id;
                }

            }


        }
		
         if (!empty($liveofferarr)) {
           
		   $this->DB2->select("cpoffer_id");
			$this->DB2->from("cp_adlocation");
			$this->DB2->where(array("location_id"=>$jsondata->locationId));
			$query=$this->DB2->get();
			
            $offer_sceen_array = array();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $offer_sceens) {
                    $offer_sceen_array[] = $offer_sceens->cpoffer_id; // create array of offer sceen
                }
            }
			$offer_sceen_array=array_filter($offer_sceen_array);
		//echo "<pre> offerseen"; print_R($offer_sceen_array);
			if(count(array_diff($liveofferarr,$offer_sceen_array))==0)
			{
				
				$this->DB2->where('location_id', $jsondata->locationId);
				$this->DB2->delete('cp_adlocation');
			}
			
            $userseenarr=array();
            
			$this->DB2->select("cp_offer_id");
			$this->DB2->from("cp_adlocation_default");
			$this->DB2->where(array("user_id"=>$jsondata->userId));
			$query1=$this->DB2->get();
			
			
            if ($query1->num_rows() > 0) {
                foreach ($query1->result() as $user_sceens) {
                    $userseenarr[] = $user_sceens->cp_offer_id; // create array of offer sceen
                }
            }
		//echo "<pre>userseen"; print_R($userseenarr);
            $unseen_ids = '';
            $usercanseearr=array_values(array_diff(array_unique($liveofferarr),$userseenarr));
		
			$userseearr=array();
			
            if(!empty($usercanseearr))
            {
				if(count(array_diff($usercanseearr, $offer_sceen_array)) > 0){
					foreach($usercanseearr as $valu)
					{
						if(!in_array($valu,$offer_sceen_array))
						{
							$userseearr[]=$valu;
							break;
						}
					}
					$unseen_ids = $userseearr[0];
					
				}
				else{
					
					$unseen_ids = $usercanseearr[0];
					
				}
				
				//echo "<pre>"; print_R($unseen_ids);//die;
				

                

                $tabledata = array("cpoffer_id" => $unseen_ids, "location_id" => $jsondata->locationId);
                $this->DB2->insert("cp_adlocation", $tabledata);
                //   $tabledata1 = array("cp_offer_id" => $unseen_ids, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId);
               //   $this->DB2->insert("cp_adlocation_default", $tabledata1);
				
				 $this->DB2->select("cp_offer_id,offer_type");
				$this->DB2->from("cp_offers");
				$this->DB2->where(array("cp_offer_id"=>$unseen_ids));
				$query=$this->DB2->get();
				 
            $rowarr=$query->row_array();
            $data['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offer_type'] = trim($rowarr['offer_type']);
            $data['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
            }
            else if(empty($liveofferarr)){
                $unseen_ids = '1';
				 $data['cp_offer_id'] = '1';
            $data['offer_type'] = 'default';
                
	
            }
			else{
				
				$repeatofferq=$this->DB2->query("select co.cp_offer_id from cp_offers co inner join cp_offers_location col on (co.cp_offer_id=col.cp_offer_id)
				where co.is_repeat=1 and col.location_id='".$jsondata->locationId."'");
				if($repeatofferq->num_rows()>0){
					$repeatofferarr=array();
					foreach($repeatofferq->result() as $val){
						$repeatofferarr[]=$val->cp_offer_id;
					}
					$repeatkey=array_rand($repeatofferarr);
					$unseen_ids=$repeatofferarr[$repeatkey];
				$delete = $this->DB2->query("delete from cp_adlocation_default WHERE
    location_id = '$jsondata->locationId' and user_id='$jsondata->userId'");
	
	 $tabledata = array("cpoffer_id" => $unseen_ids, "location_id" => $jsondata->locationId);
                $this->DB2->insert("cp_adlocation", $tabledata);
               
				 $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='".$unseen_ids."'");
            $rowarr=$query->row_array();
            $data['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offer_type'] = trim($rowarr['offer_type']);
            $data['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
				}else{
					$data['cp_offer_id'] = '1';
					$data['offer_type'] = 'default';
				}
			
			}



           
        }
        else {
            $data['cp_offer_id'] = '';
            $data['offer_type'] = 'default';
        }


        return $data;
    }

    
    
     public function get_defaultad_id($jsondata) {
				$data=array();
       
			$query1=$this->DB2->query("select Provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarr1=$query1->row_array();
			if($rowarr1['Provider_id']==14)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where is_mtnl_default_ad='1' and offer_type='image_add' order by cp_offer_id desc limit 1");
			}
			else if($rowarr1['Provider_id']==39)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='89'");
			}
			else if($rowarr1['Provider_id']==42)
			{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='102'");
				
			}
			else{
				$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
			}
			 $userdata=$this->user_detail($jsondata->userId);
           // $query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
            $rowarr=$query->row_array();
            $data['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offer_type'] = trim($rowarr['offer_type']);
            $data['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
        



        return $data;
    }
    
    public function allocate_budget_views($locid,$offerid)
    {
       
		$this->DB2->select("*");
		$this->DB2->from("cp_budget_used");
		$this->DB2->where(array("cp_offer_id"=>$offerid));
		$query=$this->DB2->get();
		
		
      
		$this->DB2->select("location_type");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$locid));
		$query1=$this->DB2->get();
		
		
          $locationarr=$query1->row_array();
           $arr1=array(1=>"mass_budget",2=>"premium_budget",3=>"star_budget");
        if($query->num_rows()>0)
        {
           
			$this->DB2->select("co.total_budget,co.mass_budget,co.premium_budget,co.star_budget");
			$this->DB2->from("cp_offers co");
			$this->DB2->where(array("co.cp_offer_id"=>$offerid));
			$query3=$this->DB2->get();
			
             $bugetarr=$query3->row_array();
             //$usedbudgetarr=$query->row_array();
             if($locationarr['location_type']==1)
             {
                 $totalvalue=$bugetarr['mass_budget']/100*$bugetarr['total_budget'];
                 //$assignedvalue=$usedbudgetarr['mass_budget'];
                 $assignedvalue = 0;
                 foreach($query->result() as $row_new){
                     $assignedvalue = $assignedvalue+ $row_new->mass_budget;
                 }
                 if($totalvalue>$assignedvalue)
                 {
                   
                    return true;

                 }
                 else
                 {
                     return false;
                 }
             }
             else if($locationarr['location_type']==2)
             {
                 $totalvalue=$bugetarr['premium_budget']/100*$bugetarr['total_budget'];
                 //$assignedvalue=$usedbudgetarr['premium_budget'];
                 $assignedvalue = 0;
                 foreach($query->result() as $row_new){
                     $assignedvalue = $assignedvalue+ $row_new->premium_budget;
                 }
                 if($totalvalue>$assignedvalue)
                 {
                    
                    return true;

                 }
                 else
                 {
                     return false;
                 }
             }
              else if($locationarr['location_type']==3)
             {
                 $totalvalue=$bugetarr['star_budget']/100*$bugetarr['total_budget'];
                 //$assignedvalue=$usedbudgetarr['star_budget'];
                 $assignedvalue = 0;
                 foreach($query->result() as $row_new){
                     $assignedvalue = $assignedvalue+ $row_new->star_budget;
                 }
                 if($totalvalue>$assignedvalue)
                 {
                    
                    return true;

                 }
                 else
                 {
                     return false;
                 }
             }
        }
 else {
    
    return true;
 }
        
        
    }
    
    
   public function insert_update_budget($locid,$offerid,$offertype)
    {
        $query=$this->DB2->query("SELECT * FROM `cp_budget_used` WHERE location_id='".$locid."' AND cp_offer_id='".$offerid."'");
        $query1=$this->DB2->query("SELECT `location_type` FROM `wifi_location` WHERE id='".$locid."'");
        $query4=$this->DB2->query("select star,premium,mass from cp_location_budget where offer_type='".$offertype."'");
        $viewmoneyarr=$query4->row_array();
        $locationarr=$query1->row_array();
        $arr1=array(1=>"mass_budget",2=>"premium_budget",3=>"star_budget");
        if($query->num_rows()>0)
        {
            $query3=$this->DB2->query("select co.`total_budget`,co.video_duration,co.`mass_budget`,co.`premium_budget`,co.`star_budget` FROM `cp_offers` co where co.cp_offer_id='".$offerid."'");
            $bugetarr=$query3->row_array();
            $usedbudgetarr=$query->row_array();
            if($locationarr['location_type']==1)
            {
                $totalvalue=$bugetarr['mass_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['mass_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['mass'];
					}
					else{
						$newval=$viewmoneyarr['mass'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("mass_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
            else if($locationarr['location_type']==2)
            {
                $totalvalue=$bugetarr['premium_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['premium_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['premium'];
					}
					else{
						$newval=$viewmoneyarr['premium'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("premium_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
            else if($locationarr['location_type']==3)
            {
                $totalvalue=$bugetarr['star_budget']/100*$bugetarr['total_budget'];
                $assignedvalue=$usedbudgetarr['star_budget'];
                if($totalvalue>$assignedvalue)
                {
					$newval=0;
					if($bugetarr['video_duration']>0){
						$mul=$bugetarr['video_duration']/10;
						$newval=$mul*$viewmoneyarr['star'];
					}
					else{
						$newval=$viewmoneyarr['star'];
					}
                    $newassign=$assignedvalue+$newval;
                    $tabledata=array("star_budget"=>$newassign);
                    $where_array = array('location_id' => $locid, 'cp_offer_id' => $offerid);
                    $this->DB2->where($where_array);
                    $this->DB2->update('cp_budget_used', $tabledata);


                }

            }
        }
        else {


 if($locationarr['location_type']==1)
     {
         $budget=$viewmoneyarr['mass'];
     }
     else if($locationarr['location_type']==2)
     {
         $budget=$viewmoneyarr['premium'];
     }
     else{
         $budget=$viewmoneyarr['star'];
     }
     
        $tabledata=array("location_id"=>$locid,$arr1[$locationarr['location_type']]=>$budget,"cp_offer_id"=>$offerid);
        $this->DB2->insert("cp_budget_used",$tabledata);

        }


    }


    public function get_app_download_ad_data($offer_id,  $location_type = null) {
        $data=array();
        $query=$this->DB2->query("SELECT co.incentive_desc,co.android_appurl, co.ios_appurl,co.cp_offer_id,co.offer_desc,
co.redirect_url,co.offer_title,co.brand_id,
        co.campaign_name,co.`campaign_end_date`,co.image_original,
co.image_small,br.`original_logo`,br.original_logo_logoimage,br.brand_name FROM `cp_offers` co
INNER JOIN `brand` br ON (co.brand_id=br.brand_id) WHERE co.cp_offer_id='".$offer_id."' and co.is_mtnl_default_ad='0'");
        $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'app_install_add'");
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='app_install_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_name']=strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"";
            $data['brand_original_image']=($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"";
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['incentive_desc'];
            $data['android_app_url'] = $rowarr['android_appurl'];
            $data['ios_app_url'] = $rowarr['ios_appurl'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
        return $data;
    }

    public function get_image_ad_data($offer_id,  $location_type = null) {
        $data=array();
       

		$this->DB2->select('co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,co.campaign_end_date,co.image_original,
		co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,br.original_logo,br.original_logo_logoimage,br.brand_name');
		$this->DB2->from('cp_offers co');
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();
			
		$this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>'image_add'));
		$get_budget=$this->DB2->get();
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='image';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
			$data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
        return $data;
    }
	
	public function get_scratch_ad_data($offer_id,$jsondata,$loc_logo) {
        $data=array();
      // echo "<pre>"; print_R($jsondata); die;

		$this->DB2->select('co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,co.campaign_end_date,co.image_original,
		co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,br.original_logo,br.original_logo_logoimage,br.brand_name');
		$this->DB2->from('cp_offers co');
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();
			//echo $this->DB2->last_query(); die;
		$this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>'scratch_add'));
		$get_budget=$this->DB2->get();
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
		
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='scratch';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
			$data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
			$scratchq=$this->DB2->query("select id,scratch_offer_title,visibility from cp_scratch_offer where offer_id='".$offer_id."'");
			
			$usersennq=$this->DB2->query("select count(id) as offerseen from cp_scratch_offer_seen where user='".$jsondata->userId."' and date(redeemed_on)=CURDATE()");
			$userseenarr=$usersennq->row_array();
			if($userseenarr['offerseen']<$scratchq->num_rows()){
				
				if($scratchq->num_rows()>0){
					$scratchofferarr=array();
					$i=0;
					foreach($scratchq->result() as $val){
						$scratchofferarr[$val->id]['scratchid']=$val->id;
						$scratchofferarr[$val->id]['scratchtitle']=$val->scratch_offer_title;
						$scratchofferarr[$val->id]['visibility']=$val->visibility;
						$scratchofferarr[$val->id]['offercount']=round($val->visibility/100*20);
						$scratchofferarr[$val->id]['offerid']=$offer_id;
						$i++;
					}
					$datarr=$this->random_scratch($scratchofferarr,$jsondata->userId);
					//echo "<pre>"; print_R($datarr); die;
					
					if(!empty($datarr)){
						$data['scratch_id']=$datarr['scratch_id'];
						$data['scratchtitle']=$datarr['scratchtitle'];
						$data['scratch_backgroundimg']=CAMPAIGN_IMAGEPATH."logo/scratch_img.jpg";
					}else{
						$data=array();
						$data=$this->get_default_image_data($loc_logo,$jsondata);
					}
				}
				
			}else{
				//default image ad
				$data=array();
				$data=$this->get_default_image_data($loc_logo,$jsondata);
			}
			
        }
        else {
			$data=array();
           $data=$this->get_default_image_data($loc_logo,$jsondata);

        }
        return $data;
    }
	
	
	public function get_default_image_data($loc_logo,$jsondata){
			$data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='image';
            $data['withoffer']='0';
            $data['mb_cost'] = '';
           $data['brand_name']='';
            $data['brand_id']='';
			$data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']='';
            $data['promotion_desc']='';
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']='';
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']='';
			 if($loc_logo!="")
			{
				
				$this->DB2->select("retail_small_image");
				$this->DB2->from("wifi_location_cptype");
				$this->DB2->where(array("location_id"=>$jsondata->locationId));
				$query1=$this->DB2->get();
				
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					if($rowarr['retail_small_image']!='')
					{
						$data['offers_small_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
						$data['offers_original_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
					}
					else{
						$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
					}
					
				}
				else{
					$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
				}
				
				  
				$data['brand_logo_image']=$loc_logo;
				$data['brand_original_image']=$loc_logo;
			}
			else{
				
				$this->db->select("logo_image");
				$this->db->from("sht_isp_detail");
				$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
				$query=$this->db->get();
				
				
				if($query->num_rows() > 0){
					$row = $query->row_array();
					$loc_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
				}
				$data['brand_logo_image']=$loc_logo;
				$data['brand_original_image']=$loc_logo;
				
				$this->DB2->select("retail_small_image");
				$this->DB2->from("wifi_location_cptype");
				$this->DB2->where(array("location_id"=>$jsondata->locationId));
				$query1=$this->DB2->get();
				
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					if($rowarr['retail_small_image']!='')
					{
						$data['offers_small_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
						$data['offers_original_image']=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
					}
					else{
						$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
					}
					
				}
				else{
					$data['offers_small_image']=$loc_logo;
				$data['offers_original_image']=$loc_logo;
				}
			}
			return $data;
		}
	
		public function random_scratch($scratchresult,$userid){
			//echo "<pre>"; print_R($scratchresult); die;
			$tempcountq=$this->DB2->query("select count(id) as tempcount from cp_scratch_offer_seen_temp");
			$tempcarr=$tempcountq->row_array();
			if($tempcarr['tempcount']>=20){
				$this->DB2->truncate('cp_scratch_offer_seen_temp');
			}
			shuffle($scratchresult);
			$scratchid=0;
			$fnlarr=array();
			foreach($scratchresult as $val){
				$scratchtemp=$this->DB2->query("select count(id) as scratchcount from cp_scratch_offer_seen_temp where scratch_id='".$val['scratchid']."'");
				$scratchtemparr=$scratchtemp->row_array();
				if($scratchtemparr['scratchcount']<$val['offercount']){
					$userseenq=$this->DB2->query("select id from cp_scratch_offer_seen where scratch_id='".$val['scratchid']."' and date(redeemed_on)=curdate()");
					if($userseenq->num_rows()==0){
						$tabledata=array("scratch_id"=>$val['scratchid'],"offer_id"=>$val['offerid'],"user"=>$userid,"redeemed_on"=>date("Y-m-d H:i:s"));
						//$this->DB2->insert("cp_scratch_offer_seen",$tabledata);
						//$this->DB2->insert("cp_scratch_offer_seen_temp",$tabledata);
						$scratchid=$val['scratchid'];
						$fnlarr['scratch_id']=$val['scratchid'];
						$fnlarr['scratchtitle']=$val['scratchtitle'];
						break;
					}
				}
			}
			return $fnlarr;
		}

		public function get_survey_ad_data($offer_id,  $location_type = null) {
        $data=array();
        
		 $this->DB2->select("co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.brand_id,co.campaign_name,co.campaign_end_date,co.image_original,
		co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,br.original_logo,br.original_logo_logoimage,br.brand_name");
		$this->DB2->from("cp_offers co");
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();
		
		
		$this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>"survey_add"));
		$get_budget=$this->DB2->get();
		
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
		$mb_cost=1024;
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
			
			$query3=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='survey_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
             $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['brand_id']=$rowarr['brand_id'];
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['promotion_name']=$rowarr['offer_title'];
			 $data['time_slot']=$this->time_conversion($query3->num_rows()*10);
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Survey offer';

        }
        return $data;
    }

    public function get_video_ad_data($offer_id, $location_type = null) {
        $data=array();
	
		$this->DB2->select('co.cp_offer_id,co.offer_desc,co.redirect_url,co.offer_title,co.video_path,co.brand_id,co.campaign_name,co.campaign_end_date,co.image_original,
		co.image_small,co.video_thumb_logo,co.video_thumb_original,co.video_thumb_medium,co.poll_brand_title,co.poll_original
		,co.poll_logo,br.original_logo,br.original_logo_logoimage,br.brand_name');
		$this->DB2->from('cp_offers co');
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();
		
		
		$this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>'video_add'));
		$get_budget=$this->DB2->get();
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='video';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
			$data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']="";
            $data['offers_original_image']="";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']=CAMPAIGN_VIDEOPATH.$rowarr['video_path'];
			$data['video_thumb_image']=($rowarr['video_thumb_medium']!='')?REWARD_IMAGEPATH."campaign/video_thumb/medium/".$rowarr['video_thumb_medium']:REWARD_IMAGEPATH."campaign/video_thumb/original/".$rowarr['video_thumb_original'];
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['promotion_desc']=$rowarr['offer_desc'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {

            $data['resultCode']='0';

            $data['resultMessage']='No Video offer';


        }

        return $data;
    }


    public function get_poll_ad_data($offer_id,$loc_id,$userid, $location_type = null)
    {
        $quesid= $this->pickup_poll_questionid($offer_id,$loc_id,$userid);

        $data=array();
        $this->DB2->select('co.cp_offer_id,co.redirect_url,co.poll_logo,co.poll_original,co.poll_brand_title,co.offer_title,co.brand_id,co.campaign_name,co.image_original,
		co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,co.campaign_end_date,br.brand_name,br.original_logo,
		br.original_logo_logoimage,cpq.id,cpq.question,cpq.option1,cpq.option2,cpq.option3,cpq.option4');
		$this->DB2->from('cp_offers co');
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id,"cpq.id"=>$quesid));
		$this->DB2->join('cp_offers_question as cpq', 'co.cp_offer_id=cpq.cp_offer_id', 'INNER');
		$this->DB2->join('brand as br', 'co.brand_id=br.brand_id', 'INNER');
		$query=$this->DB2->get();
		
		
		$this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>"poll_add"));
		$get_budget=$this->DB2->get();
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='poll';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"";
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"";
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']='';
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['poll_question_id']=$rowarr['id'];
            $data['poll_question']=$rowarr['question'];
            $data['poll_option'][0]=$rowarr['option1'];
            $data['poll_option'][1]=$rowarr['option2'];
            $data['poll_option'][2]=$rowarr['option3'];
            $data['poll_option'][3]=$rowarr['option4'];
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['poll_logo']=$rowarr['poll_logo'];
            $data['poll_original']=$rowarr['poll_original'];
            $data['poll_brand_title']=$rowarr['poll_brand_title'];

            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Video offer';

        }
        return $data;

    }
    
    
    public function pickup_poll_questionid($offer_id,$locid,$user_id)
    {
       $livepollqusetarr=array();
		$this->DB2->select("id");
	   $this->DB2->from("cp_offers_question");
	   $this->DB2->where(array("cp_offer_id"=>$offer_id));
	   $query=$this->DB2->get();
	   
       foreach($query->result() as $val)
       {
           $livepollqusetarr[]=$val->id;
       }
       
         if (!empty($livepollqusetarr)) {
           $this->DB2->select("question_id");
		   $this->DB2->from("cp_poll_history");
		   $this->DB2->where(array("location_id"=>$locid,"user_id"=>$user_id,"offer_id"=>$offer_id));
		   $query=$this->DB2->get();
		   $offer_sceen_array = array();
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $offer_sceens) {
                    $offer_sceen_array[] = $offer_sceens->question_id; // create array of offer sceen
                }
            }

            $unseen_ids = '';
            foreach ($livepollqusetarr as $v1) {
                if (in_array($v1, $offer_sceen_array)) {
                    continue;
                } else {
                    $unseen_ids = $v1;
                    break;
                }
            }

            if ($unseen_ids == '') {
                // delete all seen ids from db
           
               
				$this->DB2->delete('cp_poll_history', array('location_id' => $locid,'user_id'=>$user_id,'offer_id'=>$offer_id));
              
                $unseen_ids = $livepollqusetarr[0];
            }
             $tabledata = array("question_id" => $unseen_ids, "location_id" => $locid, "user_id"=>$user_id,"offer_id"=>$offer_id);
             $this->DB2->insert("cp_poll_history", $tabledata);
             
             return $unseen_ids;
        }
        
    }


    public function get_brand_captcha_data($offer_id, $location_type = null)
    {
        $data=array();
        
		$this->DB2->select("co.cp_offer_id,co.brand_id,co.campaign_name,co.image_original,
		co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,co.campaign_end_date,br.brand_name,br.original_logo,
		br.original_logo_logoimage,co.offer_title,co.captcha_text,co.captcha_accuracy_level,co.redirect_url");
		$this->DB2->from("cp_offers co");
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();
		
        $this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>"captcha_add"));
		$get_budget=$this->DB2->get();
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1024*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='captcha';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
           $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
            $data['validTill']='';
            $data['validTill_fromTime']='';
            $data['store_count']='';
            $data['video_url']='';
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['poll_question']='';
            $data['poll_option']=array();



            $data['captch_text']=$rowarr['captcha_text'];
            $data['captcha_authenticity']=$rowarr['captcha_accuracy_level'];
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Video offer';

        }
        return $data;

    }
	
	
	  public function get_offerlisting_data($jsondata)
    {
		$loc_logo=$this->get_loc_logo($jsondata);
		
		$this->DB2->select("latitude,longitude");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
		if($query->num_rows()>0)
		{
			$latarr=$query->row_array();
			$userlat=$latarr['latitude'];
			$userlon=$latarr['longitude'];
			$now=date("Y-m-d H:i:s");	
			
 
			$this->DB2->select('co.cp_offer_id,co.brand_id,co.offer_title,co.offer_type,co.campaign_name,co.platform_filter,co.active_between,co.campaign_day_to_run,co.image_original,
			co.image_small,co.poll_brand_title,co.poll_original,co.poll_logo,co.campaign_end_date,br.brand_name,br.original_logo,
			br.original_logo_logoimage,co.`captcha_text`,co.captcha_accuracy_level,co.redirect_url');
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array("co.offer_type"=>'offer_add',"cpl.location_id"=>$jsondata->locationId ," co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now));
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			$this->DB2->join('brand as br', 'co.brand_id=br.brand_id', 'INNER');
			 $this->DB2->order_by("co.cp_offer_id","desc");
			 $this->DB2->limit(5, 0);
			 $query=$this->DB2->get();
			
			$this->DB2->select("(
			6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( $userlat ) ) * COS( RADIANS( s.longitude ) - RADIANS( $userlon) ) + SIN( RADIANS( $userlat ) ) * SIN( RADIANS( s.latitude ) ) )
			) AS distance, co.brand_id,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_type, co.cp_offer_id, co.redirect_url,co.offer_title, co.video_path, co.campaign_name,co.platform_filter,
			co.active_between,co.campaign_day_to_run, co.campaign_end_date, co.image_original, co.image_small, b.brand_name, b.original_logo, b.original_logo_logoimage, s.city, s.geo_address, s.latitude, s.longitude");
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array("co.offer_type"=>'offer_add',"cpl.location_id"=>$jsondata->locationId ," co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now));
			$this->DB2->join('cp_offers_store as cps', 'co.cp_offer_id = cps.cp_offer_id ', 'INNER');
			$this->DB2->join('store as s', 'cps.store_id = s.store_id', 'INNER');
			$this->DB2->join('brand as b', 'co.brand_id = b.brand_id', 'INNER');
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			 $this->DB2->group_by('co.cp_offer_id');
			 $this->DB2->limit(5, 0);
			 $query1=$this->DB2->get();
			 
			 
			 
			 
			$array1=array();
			$fnlarray2=array();
			foreach($query1->result() as $val)
			{
				$dayarr = array_filter(explode(",", $val->campaign_day_to_run));
				$platformarr=explode(",",$val->platform_filter);
				$timearr=array();
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
					
				//filter for day
				 if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
					if($budget_allocate==true && $filter=true){
					$array1[$val->cp_offer_id]=$val;
					}
				
				
				
				
			}
			$x=0;
			foreach($query->result() as $val1)
			{
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
				$dayarr = array_filter(explode(",", $val1->campaign_day_to_run));
				$platformarr=explode(",",$val1->platform_filter);
				$timearr=array();
				
				
					
				//filter for day
				 if ($val1->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val1->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val1->cp_offer_id);
					if($budget_allocate==true && $filter=true){
								if($x<5){
						if(array_key_exists($val1->cp_offer_id,$array1))
						{
							$fnlarray2[]=$array1[$val1->cp_offer_id];
						}
						else{
							$fnlarray2[]=$val1;
						}
						}
						$x++;
					}
				
				
			
				
			}
		//	echo "<pre>"; print_R($fnlarray2);
		if(!empty($fnlarray2)){
			 $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='offer_add';
            $data['withoffer']='0';
			$i=0;
			foreach($fnlarray2 as $rowarr)
			{
				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
           
				 if($loc_logo!="")
					{
						$data['offerdata'][$i]['brand_logo_image']=$loc_logo;
					$data['offerdata'][$i]['brand_original_image']=$loc_logo;
					}
					else
					{
						   $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo_logoimage:"");
                 $data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_original:(($rowarr->original_logo!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo:"");
					}
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
					$datarr['osinfo']=$jsondata->platform_filter;
					$datarr['gender']='';
					$datarr['age_group']='';
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
				$datarr['usertype']='2';
				
				 $this->add_cp_offerlisting_analytics($datarr);
				//  $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 
				
			}
		}
		else{
			 $data['resultCode']='0';
            $data['resultMessage']='No Offer on this location';
			
		}
		//$this->captive_portal_impression($jsondata);
			return $data;
			
			
			
			
			
			
			
			
		}

		
          
        
    }
	
	
	public function get_sponsered_listing_data($jsondata)
    {
		$loc_logo=$this->get_loc_logo($jsondata);
		$query=$this->DB2->query("select latitude,longitude from wifi_location where id='".$jsondata->locationId."'");
		if($query->num_rows()>0)
		{
			$latarr=$query->row_array();
			$userlat=$latarr['latitude'];
			$userlon=$latarr['longitude'];
			
			
			$now=date("Y-m-d H:i:s");
			$this->DB2->select('co.cp_offer_id,co.offer_desc,co.brand_id,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter
			,co.active_between,co.image_original,co.offer_title,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_type,co.voucher_expiry,co.total_voucher,co.voucher_code_used,
			co.image_small,co.campaign_end_date,br.brand_name,br.original_logo,
			br.original_logo_logoimage,co.captcha_text,co.captcha_accuracy_level,co.redirect_url');
			$this->DB2->from('cp_offers co');
			$this->DB2->group_start();
			$this->DB2->or_where(array("co.offer_type"=>'banner_add'));
			$this->DB2->or_where(array("co.offer_type"=>'flash_add'));
			$this->DB2->group_end();
			$this->DB2->where(array("co.is_mtnl_default_ad"=>'0',"cpl.location_id"=>$jsondata->locationId," co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now));
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			$this->DB2->join('brand as br', 'co.brand_id=br.brand_id', 'INNER');
			 $this->DB2->order_by("co.cp_offer_id","desc");
			 $this->DB2->limit(10, 0);
			 $query=$this->DB2->get();
			 


			$this->DB2->select("(6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( $userlat ) ) * COS( RADIANS( s.longitude ) - RADIANS( $userlon) ) + SIN( RADIANS( $userlat ) ) * SIN( RADIANS( s.latitude ) ) )
			) AS distance, co.brand_id, co.cp_offer_id,co.offer_type,co.offer_desc,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_title, co.redirect_url, co.video_path, co.campaign_name,co.campaign_day_to_run,co.platform_filter,co.total_voucher,co.voucher_code_used,
			co.active_between, co.campaign_end_date, co.image_original, co.image_small, b.brand_name, b.original_logo, b.original_logo_logoimage, 
			s.city, s.geo_address, s.latitude, s.longitude, s.store_name");
			$this->DB2->from('cp_offers co');
			$this->DB2->group_start();
			$this->DB2->or_where(array("co.offer_type"=>'banner_add'));
			$this->DB2->or_where(array("co.offer_type"=>'flash_add'));
			$this->DB2->group_end();
			$this->DB2->where(array("co.is_mtnl_default_ad"=>'0',"cpl.location_id"=>$jsondata->locationId," co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now,"co.brand_id!="=>'145'));
			$this->DB2->join('cp_offers_store as cps', 'co.cp_offer_id = cps.cp_offer_id ', 'INNER');
			$this->DB2->join('store as s', 'cps.store_id = s.store_id', 'INNER');
			$this->DB2->join('brand as b', 'co.brand_id = b.brand_id', 'INNER');
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			 $this->DB2->order_by("co.cp_offer_id","desc");
			 $this->DB2->limit(10, 0);
			 $query1=$this->DB2->get();
			 


			$array1=array();
			$fnlarray2=array();
				
			foreach($query1->result() as $val)
			{
				$dayarr = array_filter(explode(",", $val->campaign_day_to_run));
				$platformarr=explode(",",$val->platform_filter);
				$timearr=array();
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
					
				//filter for day
				 if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
					if($budget_allocate==true && $filter=true){
					$array1[$val->cp_offer_id]=$val;
					}
				$array1[$val->cp_offer_id]=$val;
			}
			
			
			
				
			$x=0;
			foreach($query->result() as $val1)
			{
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
				$dayarr = array_filter(explode(",", $val1->campaign_day_to_run));
				$platformarr=explode(",",$val1->platform_filter);
				$timearr=array();
				
				
					
				//filter for day
				 if ($val1->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val1->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
			
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
			
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val1->cp_offer_id);
					if($budget_allocate==true && $filter=true){
						
											if($x<5){
							if(array_key_exists($val1->cp_offer_id,$array1))
							{
								$fnlarray2[]=$array1[$val1->cp_offer_id];
							}
							else{
								$fnlarray2[]=$val1;
							}
							}
							$x++;
					}
				
				
			}
			

		if(!empty($fnlarray2)){
			 $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='offer_add';
            $data['withoffer']='0';
			$i=0;
			$count=0;
			foreach($fnlarray2 as $rowarr)
			{
				
				//$closesin = $time_left1;
				if($count<5){
				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
           
			if($loc_logo!="")
					{
						 $data['offerdata'][$i]['brand_logo_image']=$loc_logo;
					 $data['offerdata'][$i]['brand_original_image']=$loc_logo;
					}else{
						 $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo_logoimage:"");
			$data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_original:(($rowarr->original_logo!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo:"");
					}
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				$data['offerdata'][$i]['promotion_desc']=$rowarr->offer_desc;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				  $data['offerdata'][$i]['offer_type']=$rowarr->offer_type;
				  $data['offerdata'][$i]['voucher_expiry']=$rowarr->voucher_expiry;
				   $data['offerdata'][$i]['geo_address']=isset($rowarr->geo_address)?$rowarr->geo_address:'';
				    $data['offerdata'][$i]['store_name']=isset($rowarr->store_name)?$rowarr->store_name:'';
				  if($rowarr->total_voucher!=0)
			{
				 $data['offerdata'][$i]['voucherleft']=$rowarr->total_voucher-$rowarr->voucher_code_used;
				
			}else{
				 $data['offerdata'][$i]['voucherleft']='';
			}
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
					$datarr['osinfo']=$jsondata->platform_filter;
					$datarr['gender']='';
					$datarr['age_group']='';
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
				$datarr['usertype']='2';
				
				//  $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 $this->add_cp_sponsorlisting_analytics($datarr);

				}
				 $count++;
			}
			//echo "<pre>"; print_R($data);die;
		}
		else{
			
				
				$this->DB2->select("Provider_id");
				$this->DB2->from("wifi_location");
				$this->DB2->where(array("id"=>$jsondata->locationId));
				$query1=$this->DB2->get();
			$rowarr1=$query1->row_array();
			if($rowarr1['Provider_id']==14)
			{
				//echo 'ssssss'; die;
				
				$this->DB2->select("co.cp_offer_id, co.offer_type, co.campaign_day_to_run");
				$this->DB2->from("cp_offers co");
				$this->DB2->where(array("co.offer_type"=>"banner_add","co.is_mtnl_default_ad"=>"1"));
				$this->DB2->order_by("co.cp_offer_id","desc");
				$this->DB2->limit(1, 0);
				$query=$this->DB2->get();
				
			}
			else{
				
				$this->DB2->select("co.cp_offer_id, co.offer_type, co.campaign_day_to_run");
				$this->DB2->from("cp_offers co");
				$this->DB2->where(array("co.offer_type"=>"banner_add","co.brand_id"=>"145"));
				$this->DB2->order_by("co.cp_offer_id","ASC");
				$this->DB2->limit(1, 0);
				$query=$this->DB2->get();
				
			}

				if($query->num_rows()>0){
                $rowarr=$query->row_array();
				
			
                $data=array();
                $dataarr=$this->get_image_ad_data($rowarr['cp_offer_id']);
				


                $data['resultCode']=1;
                $data['offerdata'][0]['cp_offer_id']=$rowarr['cp_offer_id'];
                $data['offerdata'][0]['offer_type']=$dataarr['rewradtype'];
                $data['offerdata'][0]['brand_name']=strtoupper($dataarr['brand_name']);
                $data['offerdata'][0]['brand_logo_image']=$dataarr['brand_logo_image'];
                $data['offerdata'][0]['promotion_name']=$dataarr['promotion_name'];
                $data['offerdata'][0]['promotion_desc']=$dataarr['promotion_desc'];

             //   $this->captive_portal_impression($jsondata);
				
				
				 $datarr=array();
                        $datarr['cp_offer_id']=$rowarr->cp_offer_id;
                        $datarr['location_id']=$jsondata->locationId;
                        $datarr['user_id']=$jsondata->userId;
                        $datarr['platform']=$jsondata->via;
                        $datarr['osinfo']=$jsondata->platform_filter;
                        $datarr['gender']='';
                        $datarr['age_group']='';
                        $datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
                        $datarr['usertype']='2';

                      //  $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
                        $this->add_cp_sponsorlisting_analytics($datarr);
				}else{
					 $data['resultCode']='0';
					$data['resultMessage']='No Offer Found';
				}
			
			
		}
            //$this->captive_portal_impression($jsondata);
			return $data;
			
		}
        else{
            $data['resultCode']='0';
            $data['resultMessage']='Invalid location id';
            //$this->captive_portal_impression($jsondata);
            return $data;
        }
		
          
        
    }
	
	
	public function app_download_data($jsondata)
	{
		$userdata=$this->user_detail($jsondata->userId);
		
		$this->DB2->select("latitude,longitude");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		$data['resultCode']='0';
            $data['resultMessage']='No App install Add';
		if($query->num_rows()>0)
		{
			$latarr=$query->row_array();
			$userlat=$latarr['latitude'];
			$userlon=$latarr['longitude'];
			
			
			$now=date("Y-m-d H:i:s");
			 $this->DB2->select("co.cp_offer_id,co.brand_id,co.android_appurl,co.ios_appurl,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter,
			co.active_between,co.image_original,co.offer_title,co.poll_brand_title,co.poll_original,co.poll_logo,co.incentive_desc,co.offer_desc,co.offer_type,co.voucher_expiry,
			co.image_small,co.campaign_end_date,br.brand_name,br.original_logo,
			br.original_logo_logoimage,co.captcha_text,co.captcha_accuracy_level,co.redirect_url");
			$this->DB2->from("cp_offers co");
			$this->DB2->where(array("co.offer_type"=>"app_install_add","co.is_mtnl_default_ad"=>"0","cpl.location_id"=>$jsondata->locationId,"co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now));
			$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			$this->DB2->order_by("co.cp_offer_id","desc");
			$this->DB2->limit(5, 0);
			$query=$this->DB2->get();


			 $this->DB2->select(" (6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( $userlat ) ) * COS( RADIANS( s.longitude ) - RADIANS( $userlon) ) + SIN( RADIANS( $userlat ) ) * SIN( RADIANS( s.latitude ) ) )
				) AS distance, co.brand_id, co.cp_offer_id,co.android_appurl,co.poll_brand_title,co.poll_original,co.poll_logo,co.incentive_desc,co.ios_appurl,co.offer_type,co.offer_title,co.offer_desc, co.redirect_url, co.video_path, 
			co.campaign_name,co.campaign_day_to_run,co.platform_filter,co.active_between, co.campaign_end_date, co.image_original, co.image_small, b.brand_name,
			b.original_logo, b.original_logo_logoimage, s.city, s.geo_address, s.latitude, s.longitude");
			$this->DB2->from("cp_offers co");
			$this->DB2->where(array("co.offer_type"=>"app_install_add","co.is_mtnl_default_ad"=>"0","cpl.location_id"=>$jsondata->locationId,"co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now));
			$this->DB2->join('cp_offers_store as cps', ' co.cp_offer_id = cps.cp_offer_id', 'INNER');
			$this->DB2->join('store as s', 'cps.store_id = s.store_id', 'INNER');
			$this->DB2->join('brand as b', ' co.brand_id = b.brand_id', 'INNER');
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			$this->DB2->order_by("co.cp_offer_id","desc");
			$this->DB2->limit(5, 0);
			$query1=$this->DB2->get();
			

			$array1=array();
			$fnlarray2=array();
			foreach($query1->result() as $val)
			{
				$dayarr = array_filter(explode(",", $val->campaign_day_to_run));
				$platformarr=explode(",",$val->platform_filter);
				$timearr=array();
				
				$filter=true;
				$curday = strtolower(date('l'));
				$curtime=date('H');
					
				//filter for day
				 if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
					if($budget_allocate==true && $filter=true){
					$array1[$val->cp_offer_id]=$val;
					}
				$array1[$val->cp_offer_id]=$val;
			}
		
			$x=0;
			foreach($query->result() as $val1)
			{
				
				$filter=true;
            $curday = strtolower(date('l'));
			$curtime=date('H');
				$dayarr = array_filter(explode(",", $val1->campaign_day_to_run));
				$platformarr=explode(",",$val1->platform_filter);
				$timearr=array();
				
				
					
				//filter for day
				 if ($val1->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
					$filter=true;
					
				}
				else{
					
					continue;
				}
				
				//filter for time
				if ($val1->active_between == "all" || in_array($curtime, $timearr)) {
					
					$filter=true;
				}
				else{
					
					continue;
				}
				
				
				
				//filter for Platform
				if(isset($jsondata->platform_filter))
				{
					
					if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {
					
					$filter=true;
				}
				else{
					
					continue;
				}
					
				}
				
				$budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val1->cp_offer_id);
					if($budget_allocate==true && $filter=true){
											if($x<5){
							if(array_key_exists($val1->cp_offer_id,$array1))
							{
								
								$fnlarray2[]=$array1[$val1->cp_offer_id];
							}
							else{
							
								$fnlarray2[]=$val1;
							}
							}
							$x++;
					}
                //app download seen array

                $query2=$this->DB2->query("select * from cp_dashboard_analytics where user_id='".$jsondata->userId."' and is_downloaded='1'");
				$this->DB2->select("*");
				$this->DB2->from("cp_dashboard_analytics");
				$this->DB2->where(array("user_id"=>$jsondata->userId,"is_downloaded"=>"1"));
				$query2=$this->DB2->get();
				
                if($query2->num_rows()>0)
                {
                    $appseen_arr=array();
                    foreach($query2->result() as $vald)
                    {
                        if(in_array($vald->cp_offer_id,$fnlarray2))
                        {
                            $appseen_arr[]=$vald->cp_offer_id;
                        }
                    }
                    $fnlarray2=array_diff($fnlarray2,$appseen_arr);
                }
				
				
			}
				
		if(!empty($fnlarray2)){
			 $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='offer_add';
            $data['withoffer']='0';
			$i=0;
			foreach($fnlarray2 as $rowarr)
			{
				
				//$closesin = $time_left1;

				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
             $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo_logoimage:"");
              $data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_original:(($rowarr->original_logo!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo:"");
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				  $data['offerdata'][$i]['offer_type']=$rowarr->offer_type;
				  $data['offerdata'][$i]['voucher_expiry']=$rowarr->voucher_expiry;
				    $data['offerdata'][$i]['androidurl']=$rowarr->android_appurl;
					  $data['offerdata'][$i]['ios_appurl']=$rowarr->ios_appurl;
					   $data['offerdata'][$i]['app_title']=$rowarr->campaign_name;
					    $data['offerdata'][$i]['app_desc']=$rowarr->incentive_desc;
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				 $datarr['user_id']=$jsondata->userId;
				$datarr['platform']=$jsondata->via;
				$datarr['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
				$datarr['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
				$datarr['osinfo']=$jsondata->platform_filter;
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";;
				$datarr['usertype']='1';
				
				  //$this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 $this->add_cp_analytics($datarr);
			}
			//echo "<pre>"; print_R($data);die;
		}
		else{
			
				$query1=$this->DB2->query("select Provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarr1=$query1->row_array();
			if($rowarr1['Provider_id']==14)
			{
				//echo 'ssssss'; die;
				
				$this->DB2->select("co.cp_offer_id,co.brand_id,co.android_appurl,co.ios_appurl,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter,
				co.active_between,co.image_original,co.incentive_desc,co.offer_title,co.offer_desc,co.`offer_type`,co.`voucher_expiry`,
				co.image_small,co.campaign_end_date,br.brand_name,br.original_logo,
				br.original_logo_logoimage,co.captcha_text,co.`captcha_accuracy_level,co.redirect_url");
				$this->DB2->from("cp_offers co");
				$this->DB2->where(array("co.offer_type"=>'app_install_add',"co.is_mtnl_default_ad"=>"1"));
				$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
				 $this->DB2->order_by("co.cp_offer_id","desc");
				$this->DB2->limit(1, 0);
				$query=$this->DB2->get();
				
			}
			else{
				 
				$this->DB2->select("co.cp_offer_id,co.brand_id,co.android_appurl,co.ios_appurl,co.offer_type,co.campaign_name,co.campaign_day_to_run,co.platform_filter,
				co.active_between,co.image_original,co.incentive_desc,co.offer_title,co.offer_desc,co.offer_type,co.`voucher_expiry`,
				co.image_small,co.campaign_end_date,br.brand_name,br.original_logo,
				br.original_logo_logoimage,co.captcha_text,co.captcha_accuracy_level,co.redirect_url");
				$this->DB2->from("cp_offers co");
				$this->DB2->where(array("co.offer_type"=>'app_install_add',"co.is_mtnl_default_ad"=>"1","co.brand_id"=>"145"));
				$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
				 $this->DB2->order_by("co.cp_offer_id","desc");
				$this->DB2->limit(1, 0);
				$query=$this->DB2->get();
				
			}
$i=0;
foreach($query->result() as $rowarr)
			{
				
				//$closesin = $time_left1;

				  $data['offerdata'][$i]['brand_name']=($rowarr->poll_brand_title!='')?strtoupper($rowarr->poll_brand_title):strtoupper($rowarr->brand_name);
            $data['offerdata'][$i]['offers_small_image']=($rowarr->image_small!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr->image_small:"";
             $data['offerdata'][$i]['offers_original_image']=($rowarr->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr->image_original:"";
              $data['offerdata'][$i]['brand_logo_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_logo:(($rowarr->original_logo_logoimage!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo_logoimage:"");
                    $data['offerdata'][$i]['brand_original_image']=($rowarr->poll_original!='')?ISP_IMAGEPATH."logo/".$rowarr->poll_original:(($rowarr->original_logo!='')?ISP_IMAGEPATH."logo/".$rowarr->original_logo:"");
			    $data['offerdata'][$i]['promotion_name']=$rowarr->offer_title;
				 $data['offerdata'][$i]['distance']=(isset($rowarr->distance))?$rowarr->distance:"";
				 $data['offerdata'][$i]['cp_offer_id']=$rowarr->cp_offer_id;
				 $data['offerdata'][$i]['city']=(isset($rowarr->city))?$rowarr->city:"";
				  $data['offerdata'][$i]['offer_type']=$rowarr->offer_type;
				  $data['offerdata'][$i]['voucher_expiry']=$rowarr->voucher_expiry;
				    $data['offerdata'][$i]['androidurl']=$rowarr->android_appurl;
					  $data['offerdata'][$i]['ios_appurl']=$rowarr->ios_appurl;
					   $data['offerdata'][$i]['app_title']=$rowarr->campaign_name;
					    $data['offerdata'][$i]['app_desc']=$rowarr->incentive_desc;
				 $i++;
				 $datarr=array();
				 $datarr['cp_offer_id']=$rowarr->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				 $datarr['user_id']=$jsondata->userId;
				$datarr['platform']=$jsondata->via;
				$datarr['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
				$datarr['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
				$datarr['osinfo']=$jsondata->platform_filter;
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";;
				$datarr['usertype']='1';
				
				 // $this->insert_update_budget($jsondata->locationId,$rowarr->cp_offer_id,$rowarr->offer_type);
				 $this->add_cp_analytics($datarr);
			}
			
			
		}
			return $data;
			
		}
		
	}
	
	
public function offer_ad_detail($jsondata)
	{
		$loc_logo=$this->get_loc_logo($jsondata);
		    $userdata=$this->user_detail($jsondata->userId);
		
				
			$this->DB2->select("(6371 * ACOS( COS( RADIANS( s.latitude ) ) * COS( RADIANS( 28.5376320000 ) ) * COS( RADIANS( s.longitude ) - RADIANS( 77.2282860000) ) + SIN( RADIANS( 28.5376320000 ) ) * SIN( RADIANS( s.latitude ) ) )
			) AS distance, co.brand_id, co.cp_offer_id,co.poll_brand_title,co.poll_original,co.poll_logo, co.redirect_url,co.voucher_expiry,co.flash_start_date, co.offer_title,co.image_medium,co.video_path,co.term_and_condition, co.campaign_name,
			co.campaign_end_date, co.image_original,co.offer_type,co.total_voucher,co.voucher_code_used, co.image_small,
			b.brand_name, b.original_logo, b.original_logo_logoimage, s.city, s.geo_address, s.latitude, s.longitude");
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array(" cpl.location_id"=>$jsondata->locationId,"co.cp_offer_id"=>$jsondata->offer_id));
			$this->DB2->join('cp_offers_store as cps', 'co.cp_offer_id = cps.cp_offer_id ', 'INNER');
			$this->DB2->join('store as s', 'cps.store_id = s.store_id', 'INNER');
			$this->DB2->join('brand as b', 'co.brand_id = b.brand_id', 'INNER');
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			$query=$this->DB2->get();
			
if($query->num_rows()>0)
{
	$i=0;
	
	$dataobj=$query->result();
	foreach($query->result() as $vald)
	{
			$is_close = '';
					$current_time =  date("Y-m-d H:i:s");
					$start_date = new DateTime($vald->flash_start_date);
					$since_start = $start_date->diff(new DateTime($current_time));
					$year 	=  $since_start->y;
					$month 	= $since_start->m;
					$day 	=  $since_start->d;
					$hr     =  $since_start->h;
					$min  	=  $since_start->i;
					$sec 	=  $since_start->s;
					$time_left = '';
					if($year > 0){
						$time_left = $year.'y : '.$month.'m: '. $day.'d';
					}elseif($month > 0){
						$time_left = $month.'m : '.$day.'d: '. $hr.'hr';
					}elseif($day > 0){
						$time_left = $day.'d : '.$hr.'h: '. $min.'m';
					}elseif($hr > 0){
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}else{
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}
					//check flash sale start or not
					$isopen = '';
					$startsin = '';
					$closesin = '';
					if(strtotime($current_time) > strtotime($vald->flash_start_date)){
						$isopen = '1';
						$statr =  $vald->campaign_end_date;
						$sale_end = date('Y-m-d H:i:s', strtotime($statr));
						if(strtotime($current_time) > strtotime($sale_end)){
					
							$is_close = '1';
						}else{
							$current_time = new DateTime($current_time);
							$since_end = $current_time->diff(new DateTime($sale_end));
							$time_left1 = '';
							$time_left1 = $since_end->h.'h: '.$since_end->i.'m: '. $since_end->s.'s';
							$closesin = $time_left1;
						
						}

					}else{
						$isopen = '0';
						$startsin = $time_left;
					}
		
		
		
		  $data['resultCode']='1';
            $data['resultMessage']='Success';
           
            $data['withoffer']='0';
              $data['brand_name']=($dataobj[0]->poll_brand_title!='')?strtoupper($dataobj[0]->poll_brand_title):strtoupper($dataobj[0]->brand_name);
			if($dataobj[0]->total_voucher!=0)
			{
				 $data['voucherleft']=$dataobj[0]->total_voucher-$dataobj[0]->voucher_code_used;
				
			}else{
				 $data['voucherleft']='';
			}
			if($dataobj[0]->offer_type=="offer_add")
			{
				 $data['rewradtype']='offer_detail';
				 $data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
				
			}else if($dataobj[0]->offer_type=="flash_add"){
				$data['rewradtype']='flash_detail';
				$data['is_start']=$isopen;
				$data['opens_in']=$startsin;
				$data['closes_in']=$closesin;
			}
			else if($dataobj[0]->offer_type=="banner_add")
			{
				$data['rewradtype']='sponsor_detail';
				$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
			else{
					$data['rewradtype']='';
					$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
            $data['offers_small_image']=($dataobj[0]->image_small='')?CAMPAIGN_IMAGEPATH."small/".$dataobj[0]->image_small:"";
             $data['offers_original_image']=($dataobj[0]->image_original!='')?CAMPAIGN_IMAGEPATH."original/".$dataobj[0]->image_original:"";
			 $data['offers_medium_image']=($dataobj[0]->image_medium!='')?CAMPAIGN_IMAGEPATH."medium/".$dataobj[0]->image_medium:"";
              
				if($loc_logo!="")
					{
						$data['brand_logo_image']=$loc_logo;
					$data['brand_original_image']=$loc_logo;
					}
					else
					{
						$data['brand_logo_image']=($dataobj[0]->poll_original!='')?ISP_IMAGEPATH."logo/".$dataobj[0]->poll_logo:(($dataobj[0]->original_logo_logoimage!='')?ISP_IMAGEPATH."logo/".$dataobj[0]->original_logo_logoimage:"");
                $data['brand_original_image']=($dataobj[0]->poll_original!='')?ISP_IMAGEPATH."logo/".$dataobj[0]->poll_original:(($dataobj[0]->original_logo!='')?ISP_IMAGEPATH."logo/".$dataobj[0]->original_logo:"");
					}
              $data['validTill']='';
              
			  $data['voucher_expiry']=$dataobj[0]->voucher_expiry;
             
              $data['promotion_name']=$dataobj[0]->offer_title;
			  $data['temcond']=$dataobj[0]->term_and_condition;
			  $data['storedata'][$i]['address']=$vald->geo_address;
			  $data['storedata'][$i]['distance']=$vald->distance;
			  
			  
			   $datarr=array();
				 $datarr['cp_offer_id']=$dataobj[0]->cp_offer_id;
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
		
		$i++;
	}
	
	
	
	
}
else{
		
			$this->DB2->select("co.brand_id, co.cp_offer_id,co.term_and_condition,co.total_voucher,co.voucher_code_used, 
			co.redirect_url, co.video_path,co.poll_brand_title,co.poll_original,co.poll_logo,co.offer_type,co.offer_type,co.flash_start_date, co.campaign_name,co.voucher_expiry,co.offer_title, co.campaign_end_date,co.image_medium, co.image_original, co.image_small,
			b.brand_name, b.original_logo, b.original_logo_logoimage");
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array(" cpl.location_id"=>$jsondata->locationId,"co.cp_offer_id"=>$jsondata->offer_id));
			$this->DB2->join('brand as b', 'co.brand_id = b.brand_id', 'INNER');
			$this->DB2->join('cp_offers_location as cpl', 'co.cp_offer_id=cpl.cp_offer_id', 'INNER');
			$query1=$this->DB2->get();
			
				$rowarr=array();
				$rowarr=$query1->row_array();
				$is_close = '';
					$current_time =  date("Y-m-d H:i:s");
					$start_date = new DateTime($rowarr['flash_start_date']);
					$since_start = $start_date->diff(new DateTime($current_time));
					$year 	=  $since_start->y;
					$month 	= $since_start->m;
					$day 	=  $since_start->d;
					$hr     =  $since_start->h;
					$min  	=  $since_start->i;
					$sec 	=  $since_start->s;
					$time_left = '';
					if($year > 0){
						$time_left = $year.'y : '.$month.'m: '. $day.'d';
					}elseif($month > 0){
						$time_left = $month.'m : '.$day.'d: '. $hr.'hr';
					}elseif($day > 0){
						$time_left = $day.'d : '.$hr.'h: '. $min.'m';
					}elseif($hr > 0){
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}else{
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}
					//check flash sale start or not
					$isopen = '';
					$startsin = '';
					$closesin = '';
					if(strtotime($current_time) > strtotime($rowarr['flash_start_date'])){
						
						$isopen = '1';
						$statr =  $rowarr['campaign_end_date'];
						$sale_end = date('Y-m-d H:i:s', strtotime($statr));
						if(strtotime($current_time) > strtotime($sale_end)){
							$is_close = '1';
							
						}else{
							$current_time = new DateTime($current_time);
							$since_end = $current_time->diff(new DateTime($sale_end));
							$time_left1 = '';
							$time_left1 = $since_end->h.'h: '.$since_end->i.'m: '. $since_end->s.'s';
							$closesin = $time_left1;
							
						}

					}else{
						$isopen = '0';
						$startsin = $time_left;
					}
				
				 //$rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            
            $data['withoffer']='0';
				if($rowarr['total_voucher']!=0)
			{
				 $data['voucherleft']=$rowarr['total_voucher']-$rowarr['voucher_code_used'];
				
			}else{
				 $data['voucherleft']='';
			}
				if($rowarr['offer_type']=="offer_add")
			{
				 $data['rewradtype']='offer_detail';
				 $data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
				
			}else if($rowarr['offer_type']=="flash_add"){
				$data['rewradtype']='flash_detail';
				$data['is_start']=$isopen;
				$data['opens_in']=$startsin;
				$data['closes_in']=$closesin;
			}
			else if($rowarr['offer_type']=="banner_add")
			{
				$data['rewradtype']='sponsor_detail';
				$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
			else{
					$data['rewradtype']='';
					$data['is_start']="";
				$data['opens_in']="";
				$data['closes_in']="";
			}
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
             $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
			 $data['offers_medium_image']=($rowarr['image_medium']!='')?CAMPAIGN_IMAGEPATH."medium/".$rowarr['image_medium']:"";
          
	 if($loc_logo!="")
					{
						$data['brand_logo_image']=$loc_logo;
					$data['brand_original_image']=$loc_logo;
					}
					else
					{
						  $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
     $data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
					}
              $data['validTill']='';
              $data['validTill_fromTime']='';
			   $data['voucher_expiry']=$rowarr['voucher_expiry'];
              $data['store_count']='';
              $data['video_url']='';
              $data['promotion_name']=$rowarr['offer_title'];
			   $data['temcond']=$rowarr['term_and_condition'];
			  $data['storedata']=array();
			  
              $data['poll_question']='';
              $data['poll_option']=array();
			  
			   $datarr=array();
				 $datarr['cp_offer_id']=$rowarr['cp_offer_id'];
				  $datarr['location_id']=$jsondata->locationId;
				   $datarr['user_id']=$jsondata->userId;
				    $datarr['platform']=$jsondata->via;
					 $datarr['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
					 $datarr['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
					 $datarr['osinfo']=$jsondata->platform_filter;
				$datarr['macid']=($jsondata->macid!="null" && $jsondata->macid!="")?$jsondata->macid:"";
				$datarr['usertype']='1';
		
	
}

if($data['rewradtype']=="offer_detail")
			{
				$this->add_cp_offerdetail_analytics($datarr);
				
			}else if($data['rewradtype']=="flash_detail"){
				$this->add_cp_sponsordetail_analytics($datarr);
			}
			else if($data['rewradtype']=="sponsor_detail")
			{
				$this->add_cp_sponsordetail_analytics($datarr);
			}
 //$this->add_cp_offerdetail_analytics($datarr);
return $data;

		
	}
	
	
    
    public function get_redeemable_offer_data($offer_id)
    {
        $data=array();
        $this->DB2->select("co.cp_offer_id,co.brand_id,co.voucher_expiry,co.campaign_name,co.offer_title,co.`campaign_end_date`,co.image_original,
            co.image_small,br.brand_name,br.`original_logo`,br.original_logo_logoimage,co.redirect_url,co.poll_brand_title,co.poll_original,co.poll_logo,
            (SELECT COUNT(*) FROM `cp_offers_store` WHERE cp_offer_id='".$offer_id."') AS store_count");
		$this->DB2->from("cp_offers co");
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();	 
		
		if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='redeem';
            $data['withoffer']='0';
			$data['brand_id']=$rowarr['brand_id'];
           $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
             $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
             $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
              $data['validTill']=$rowarr['voucher_expiry'];
              $data['validTill_fromTime']='';
              $data['store_count']=$rowarr['store_count'];
              $data['promotion_name']=$rowarr['offer_title'];
              $data['poll_question']="";
              $data['poll_option']=array();
              $data['captch_text']="";
              $data['captcha_authenticity']="";
			   $data['offer_id']=$rowarr['cp_offer_id'];
              $data['url']=$rowarr['redirect_url'];
        }
 else {
       $data['resultCode']='0';
            $data['resultMessage']='No Image offer';
     
 }
       return $data;
    }
	
	 public function get_cost_per_lead_add_data($offer_id, $location_type = null)
    {
        $data=array();
        $this->DB2->select("co.cp_offer_id,co.brand_id,co.voucher_expiry,co.campaign_name,co.offer_title,co.campaign_end_date,co.image_original,
            co.image_small,br.brand_name,br.original_logo,br.original_logo_logoimage,co.redirect_url,co.poll_brand_title,co.poll_original,co.poll_logo,
            (SELECT COUNT(*) FROM `cp_offers_store` WHERE cp_offer_id='".$offer_id."') AS store_count");
		$this->DB2->from("cp_offers co");
		$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
		$this->DB2->join('brand as br', 'co.brand_id = br.brand_id', 'INNER');
		$query=$this->DB2->get();
		
		$this->DB2->select("*");
		$this->DB2->from("cp_location_budget");
		$this->DB2->where(array("offer_type"=>"cost_per_lead_add"));
		$get_budget=$this->DB2->get();
		
        $mb_cost = '0';
        if($get_budget->num_rows() > 0){
            $budget_row = $get_budget->row_array();
            if($location_type == '1'){
                $mb_cost = $budget_row['star'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '2'){
                $mb_cost = $budget_row['premium'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
            elseif($location_type == '3'){
                $mb_cost = $budget_row['mass'];
                $mb_cost = ceil((1000*$mb_cost)/30);
            }
        }
        if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode']='1';
            $data['resultMessage']='Success';
            $data['rewradtype']='cost_per_lead_add';
            $data['withoffer']='0';
            $data['mb_cost'] = $mb_cost;
            $data['brand_id']=$rowarr['brand_id'];
            $data['brand_name']=($rowarr['poll_brand_title']!='')?strtoupper($rowarr['poll_brand_title']):strtoupper($rowarr['brand_name']);
            $data['offers_small_image']=($rowarr['image_small']!='')?CAMPAIGN_IMAGEPATH."small/".$rowarr['image_small']:"";
            $data['offers_original_image']=($rowarr['image_original']!='')?CAMPAIGN_IMAGEPATH."original/".$rowarr['image_original']:"";
            $data['brand_logo_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_logo']:(($rowarr['original_logo_logoimage']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo_logoimage']:"");
			$data['brand_original_image']=($rowarr['poll_original']!='')?ISP_IMAGEPATH."logo/".$rowarr['poll_original']:(($rowarr['original_logo']!='')?ISP_IMAGEPATH."logo/".$rowarr['original_logo']:"");
            $data['validTill']=$rowarr['voucher_expiry'];
            $data['validTill_fromTime']='';
            $data['store_count']=$rowarr['store_count'];
            $data['promotion_name']=$rowarr['offer_title'];
            $data['poll_question']="";
            $data['poll_option']=array();
            $data['captch_text']="";
            $data['captcha_authenticity']="";
            $data['offer_id']=$rowarr['cp_offer_id'];
            $data['url']=$rowarr['redirect_url'];
        }
        else {
            $data['resultCode']='0';
            $data['resultMessage']='No Image offer';

        }
        return $data;
    }
	
	public function get_offertype($offerid)
	{
		
		$rowarr="";
		
		$this->DB2->select("offer_type,brand_id,cp_offer_id");
		$this->DB2->from("cp_offers");
		$this->DB2->where(array("cp_offer_id"=>$offerid));
		$query=$this->DB2->get();
		
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			
		}
		
		return $rowarr;
		
	}
	
    
    public function add_cp_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],"is_redirected"=>0,
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics",$tabledata);
    }
	
	 public function add_cp_offerlisting_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_offer_listing",$tabledata);
		
    }
	
	public function add_cp_sponsorlisting_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender']),"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_bannerflash_listing",$tabledata);
    }
	
	
	 public function add_cp_offerdetail_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender'])
			,"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_offer_detail",$tabledata);
    }
	
	public function add_cp_sponsordetail_analytics($datarr)
    {
        $tabledata=array("cp_offer_id"=>$datarr['cp_offer_id'],"location_id"=>$datarr['location_id'],
            "user_id"=>$datarr['user_id'],"viewed_on"=>date("Y-m-d H:i:s"),"platform"=>$datarr['platform'],"gender"=>strtoupper($datarr['gender'])
			,"osinfo"=>$datarr['osinfo'],"macid"=>$datarr['macid'],"usertype"=>$datarr['usertype'],"age_group"=>$datarr['age_group']);
        $this->DB2->insert("cp_dashboard_analytics_bannerflash_detail",$tabledata);
    }
    
    
    public function redirect_url_api($jsondata)
    {
		if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				// $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        if(isset($jsondata->view_counter) && $jsondata->view_counter == '1'){
			
            
				$tabledata=array("is_redirected"=>"1");
				$this->DB2->order_by("id","desc");
				$this->DB2->limit(1, 0);
				$this->DB2->update('cp_dashboard_analytics',$tabledata,array("cp_offer_id"=>$jsondata->offer_id,"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId));
				
				
				
        }

        
          $data['resultCode']='1';
            $data['resultMessage']='Success';
            return $data;
      
    }
	
	  public function captcha_accurate_api($jsondata)
    {
		if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
			//	 $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        		
		$tabledata=array("captcha_verified"=>"1");
		$this->DB2->order_by("id","desc");
		$this->DB2->limit(1, 0);
		$this->DB2->update('cp_dashboard_analytics',$tabledata,array("cp_offer_id"=>$jsondata->offer_id,"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId));		
        
          $data['resultCode']='1';
            $data['resultMessage']='Success';
            return $data;
      
    }
    
    public function poll_answer($jsondata)
    {
		
	if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				// $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        $tabledata=array("question_id"=>$jsondata->ques_id,"option_id"=>$jsondata->option_id,"user_id"=>$jsondata->userId,
            "location_id"=>$jsondata->locationId,"cp_offer_id"=>$jsondata->offer_id,"answered_on"=>date("Y-m-d H:i:s"));
        $this->DB2->insert("cp_poll_answer",$tabledata);
        if($this->DB2->affected_rows()>0)
        {
                $data['resultCode']='1';
            $data['resultMessage']='Success';
        }
        else{
            $data['resultCode']='0';
            $data['resultMessage']='Fail';
        }
        return $data;
    }
    
    
    public function redeem_offer($jsondata)
    {
		if($jsondata->offer_id!='1'){
		$tabledata1 = array("cp_offer_id" => $jsondata->offer_id, "location_id" => $jsondata->locationId, "user_id"=>$jsondata->userId,"viewed_on"=>date("Y-m-d H:i:s"));
             $this->DB2->insert("cp_adlocation_default", $tabledata1);
			 
			 $budgetarr=$this->get_offertype($jsondata->offer_id);
			
			 if(!empty($budgetarr))
			 {
				 if($budgetarr['cp_offer_id']!='1'){
					 
				// $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,$budgetarr['offer_type']);
				 }
			 }
		}
        
		
		$this->DB2->select("*");
		$this->DB2->from("cp_offer_reddem");
		$this->DB2->where(array("cp_offer_id"=>$jsondata->offer_id,"user_id"=>$jsondata->userId));
		$query=$this->DB2->get();
		$vouchercode	="";
		$userdata=$this->user_detail($jsondata->userId);
		$gender='';
		$age_group='';
		$platform=$jsondata->via;
		$osinfo=$jsondata->platform_filter;
		$macid=$jsondata->macid;
		  if(is_array($userdata))
		  {
			   $gender=$userdata[0]['gender'];
			   $age_group=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';
		  }

        if($query->num_rows()==0){
			
              
                $this->DB2->select("voucher_type,voucher_static_type,offer_type,voucher_expiry,campaign_name,voucher_code,total_voucher,voucher_code_used");
				$this->DB2->from("cp_offers");
				$this->DB2->where(array("cp_offer_id"=>$jsondata->offer_id));
				$query1=$this->DB2->get();
				
				 
                $rowarr=$query1->row_array();
                if($rowarr['voucher_code_used']<$rowarr['total_voucher'] || $rowarr['total_voucher']==0)
                {
                    if($rowarr['voucher_type']=="static")
                    {
						if($rowarr['voucher_static_type']=="multiple")
						{
							$couponcode=$this->getvoucher_staticmultiple($jsondata->offer_id);
							if($couponcode=="0")
							{
								$data['resultCode']='0';
								$data['resultMessage']='Voucher finish'; 
								return $data;
							}
							else{
								$vouchercode=$couponcode;	
								$this->DB2->query("update cp_offer_coupon set is_used ='1',used_on=now()
					 WHERE cp_offer_id = '$jsondata->offer_id' and coupon_code='$couponcode'");
							}
						}else{
						$vouchercode=$rowarr['voucher_code'];	
						}
                        
                    }
                     else {
                     $vouchercode=$this->randon_voucher();
                     
                     }
                     $update_voucher_redeem_counter = $this->DB2->query("update cp_offers set voucher_code_used =
					(voucher_code_used + 1) WHERE cp_offer_id = '$jsondata->offer_id'");
				
					
					$data['couponCode'] = $vouchercode;
					$insert_redeem = $this->DB2->query("insert into cp_offer_reddem (cp_offer_id, user_id,
							coupon_code, redeemed_on,location_id,gender,platform,osinfo,macid,age_group) VALUES ('$jsondata->offer_id', '$jsondata->userId',
							'$vouchercode', now(),'$jsondata->locationId','$gender','$platform','$osinfo','$macid','$age_group')");
						if($rowarr['offer_type']=="banner_add")	
						{
							// $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'banner_add_redeem');
						}
						else if($rowarr['offer_type']=="flash_add")
						{
							// $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'flash_add_redeem');
						}
						else if($rowarr['offer_type']=="redeemable_add")
						{
							// $this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'redeemable_add_redeem');
						}
							
                     
                     $data_msg = array("is_login" => '1', 'login_userid' => $jsondata->userId,
						"apikey" => $jsondata->apikey, "offerId" => $jsondata->offer_id, "type" => 'sms',"store_location" => $jsondata->locationId);
					
					$data_msg = json_encode($data_msg);
					$this->coupon_code_send_wifioffer(json_decode($data_msg));

					$data_msg = array("is_login" => '1', 'login_userid' => $jsondata->userId,
						"apikey" => $jsondata->apikey, "offerId" => $jsondata->offer_id, "type" => 'email', "store_location" => $jsondata->locationId);
					
					$data_msg = json_encode($data_msg);
					$this->coupon_code_send_wifioffer(json_decode($data_msg));
                     $data['resultCode'] = 1;
                     $data['resultMessage'] = 'Success';
					 $data['couponCode'] = $vouchercode;
					 $data['voucher_expiry'] = $rowarr['voucher_expiry'];
            }
                else {
					
                    $data['resultCode']='0';
                          $data['resultMessage']='Voucher finish'; 
               }
     
 
            
        }
        else
        {
			
            $data['resultCode']='0';
            $data['resultMessage']='Already Purchased'; 
        }
		return $data;
    }
	
	
	public function getvoucher_staticmultiple($cp_offer_id)
	{
		
		$this->DB2->select("coupon_code");
		$this->DB2->from("cp_offer_coupon");
		$this->DB2->where(array("cp_offer_id"=>$cp_offer_id,"is_used"=>'0'));
		$query=$this->DB2->get();
		if($query->num_rows()>0)
		{
			$couponcodearr=$query->result();
			return $couponcodearr[0]->coupon_code;
		}
		else{
			return '0';
		}
		
	}
    
    
    public function randon_voucher(){
		$random_number = mt_rand(1000000, 9999999);
		$query = $this->DB2->query("select voucher_code from cp_voucher WHERE voucher_code = '$random_number'");
		if($query->num_rows() > 0){
			$this->randon_voucher();
		}else{
			return $random_number;
		}
	}
        
        public function coupon_code_send_wifioffer($jsondata){
		$user_id = $jsondata->login_userid;
		$flahs_deal_id = $jsondata->offerId;
		$type = $jsondata->type;
		$store_address =  $jsondata->store_location;
		$data = array();
		$query = $this->DB2->query("select cor.coupon_code, co.campaign_name, co.voucher_expiry, b.brand_name, b
		.original_logo from cp_offer_reddem as cor INNER JOIN cp_offers as co on
		(cor.cp_offer_id = co.cp_offer_id)  INNER JOIN
		brand as b ON (co.brand_id = b.brand_id) WHERE cor.cp_offer_id = '$flahs_deal_id' AND cor.user_id =
		'$user_id'");
		

		if($query->num_rows() > 0){
			$row = $query->row_array();
		
			$coupon_code = $row['coupon_code'];
			/*$msg = '';
			$msg .= "SHOUUT FLASH SALE VOUCHER";
			$msg .= 'Brand Name:-'.$row['brand_name'];
			$msg .= 'Coupon Code:-'.$coupon_code.'';
			$msg .= 'Expires :-'.$coupon_code.'';
			$msg .= ". HOW TO REDEEM THIS COUPON.  \r";
			$msg .= "PRESENT THE COUPON CODE TO THE STORE MANAGER";*/
			$msg = urlencode("SHOUUT WIFI OFFER VOUCHER").'%0a'.urlencode('Brand Name:-'.$row['brand_name']).'%0a'.
				urlencode('Voucher code:-'.$coupon_code).'%0a'.urlencode('Expires :-'.$row['voucher_expiry']).'%0a'
				.urlencode("TO REDEEM THIS COUPON.").'%0a'.urlencode("PRESENT THE COUPON CODE TO THE STORE MANAGER.");
			// get user detail
			$query=$this->db->query("select `email`,`mobile`,`firstname` from sht_users where uid='".$user_id."'");
			$result=$query->result();
			/*$this->mongo_db->select(array('mobile', 'email', 'screen_name'));
			$this->mongo_db->where(array('_id' => new MongoId($user_id)));
			$result = $this->mongo_db->get('sh_user');
			*/

			if($type == "sms"){
				$this->send_via_sms($result[0]->mobile, $msg);
			}else{
				$this->send_via_email($result[0]->email, $row['original_logo'], $row['brand_name'],
					$result[0]->screen_name, $row['campaign_name'], $coupon_code, $row['voucher_expiry'], $store_address);
			}
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';

		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No deal id found';
		}
		return $data;
	}
        public function shouut_sms_link($jsondata)
		{
			$msg = urlencode("SHOUUT APP DOWNLOAD LINK").'%0a'.urlencode('ANDROID:- https://play.google.com/store/apps/details?id=com.shouut.discover').'%0a'.
				urlencode('IOS:- https://itunes.apple.com/in/app/shouut/id1089501174').'%0a';
				$this->send_via_sms($jsondata->mobile,$msg);
				$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
			return $data;
		}
		
    public function send_via_sms($mobile, $msg,$isp_uid=''){
		
		if($isp_uid==193)
		{
			$infini_apikey="Afcf2226de1dca238ada3587431f6cc8a";
			$infini_senderid="ADWIFI";
			 $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $mobile, $msg);
		}else{
		 $postData = array(
            'authkey' => '106103ADQeqKxOvbT856d19deb',
            'mobiles' => $mobile,
            'message' => $msg,
            'sender' => 'SHOUUT',
            'route' => '4'
        );
        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
		}
	}

	public function send_via_email($email, $image, $brand_name, $username, $desc, $coupon_code, $voucher_expiry,
								   $store_location){
		$subject = "SHOUUT FLASH SALE: ".$brand_name." VOUCHER";
		/*$headers = 'From: SHOUUT <flashsale@shouut.com>' . "\r\n";
		//$headers .= 'Cc: aashish@shouut.com'. "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";*/
		$message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg.jpg);
                        background-repeat:no-repeat; background-position:top center; width:100%">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px"><strong>Hi '.$username.',</strong></h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Congrats !
                         You have successfully redeemed a Shouut Flash voucher for
                          <strong>'.$brand_name.'</strong></p>
                        <p style="margin-bottom:5px; margin-top:10px; font-weight:normal; font-size:14px;">Offer-
                            '.$desc.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Store
                         location- '.$store_location.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        code- '.$coupon_code.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        expires on- '.$voucher_expiry.'</p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">To avail this OFFER, please present this VOUCHER at the time of the payment in the STORE/VENUE.</p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"> For any
                         query  : Please write to us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>
                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT FLASH SALE</h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';
		//echo $message;die;
		//mail($email,$subject,$message,$headers);
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "flashsale@shouut.com";
		$mail->Password = "shouut@#123";
		$mail->setFrom('flashsale@shouut.com', 'Flash Sale');
		$mail->addAddress($email, $username);
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		$mail->AltBody = '';
		$mail->send();
		return 1;

	}
	
	
	  public function user_detail($user_id){
		  $data=array();
	$this->DB2->select("gender,age_group");
	$this->DB2->from("sht_users");
	$this->DB2->where(array("uid"=>$user_id));
	$query=$this->DB2->get();

	$num = $query->num_rows();
	if($num > 0){
	    $rowarr=$query->row_array();
		$data[0]['gender']=$rowarr['gender'];
		$data[0]['age_group']=$rowarr['age_group'];
	}
	return $data;
    }

	public function get_age_group($mobileno)
	{
		 $CI = &get_instance();
//setting the second parameter to TRUE (Boolean) the function will return the database object.
        $this->db2 = $CI->load->database('db2', TRUE);
        $query = $this->db2->query("SELECT age_group FROM `wifi_register_users` wru1 WHERE mobileno='".$mobileno."'");
		$resultdata=$query->result();
		if($query->num_rows>0)
		{
			if($resultdata[0]->age_group!="" || $resultdata[0]->age_group!=0){
				return $resultdata[0]->age_group;
			}
			else{
					return 'all';
			}
			
		}
		else{
			return 'all';
		}
		
	}
	public function timearr($timeval){
			
$arrtime=explode(",",$timeval);
$fnltimearr=array();

foreach($arrtime as $val)
{
$hifentime=explode("-",$val);
$fnltimearr[]=$hifentime[0];
for($i=0;$i<1; $i++)
{
if($hifentime[0]<$hifentime[1] || $hifentime[0]==22){
$midtime=$hifentime[0]+1;
if(strlen($midtime)==1){
	$fnltimearr[]="0".$midtime;
}
else{
	$fnltimearr[]=$midtime;
}

$i++;
}
$fnltimearr[]=$hifentime[1];
}
}
return array_unique($fnltimearr); 
		
	}
	public function captive_portal_impression($jsondata)
	{
        $today_date = date('Y-m-d');
		$locationId=$jsondata->locationId;
        $macid = '';
        if(isset($jsondata->macid)){
            $macid = $jsondata->macid;
            $location_id = '';
            if(isset($jsondata->locationId)){
                $location_id = $jsondata->locationId;
            }
            $osinfo = '';
            if(isset($jsondata->platform_filter)){
                $osinfo = $jsondata->platform_filter;
            }
            $query = $this->DB2->query("select macid from channel_unique_daily_user where location_id = '$location_id' AND macid = '$macid' and DATE(visit_date) = '$today_date'");
            if($query->num_rows() < 1){
                $insert = $this->DB2->query("insert into channel_unique_daily_user(location_id, macid, osinfo, visit_date)
             values('$location_id', '$macid', '$osinfo', now())");
            }
        }

   

		
		   $check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
    location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
   if($check_impression->num_rows() > 0){
    $row = $check_impression->row_array();
    $temp_id = $row['id'];
    $update_cp = $this->DB2->query("update channel_captiveportal_impression set
     total_impression = (total_impression+1) WHERE id = '$temp_id'");
   }
   else{
    $insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
    (location_id, total_impression, visit_date) VALUES ('$locationId',
    '1', now())");
   }
  
   
	}
	
	public function captive_portal_uniqueimpression($jsondata)
	{
        $today_date = date('Y-m-d');
		$locationId=$jsondata->locationId;
        $macid = '';
        if(isset($jsondata->macid)){
            $macid = $jsondata->macid;
            $location_id = '';
            if(isset($jsondata->locationId)){
                $location_id = $jsondata->locationId;
            }
            $osinfo = '';
            if(isset($jsondata->platform_filter)){
                $osinfo = $jsondata->platform_filter;
            }
            $query = $this->DB2->query("select macid from channel_unique_daily_user where location_id = '$location_id' AND macid = '$macid' and DATE(visit_date) = '$today_date'");
            if($query->num_rows() < 1){
                $insert = $this->DB2->query("insert into channel_unique_daily_user(location_id, macid, osinfo, visit_date)
             values('$location_id', '$macid', '$osinfo', now())");
            }
        }

   

		
		   $check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
    location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
   if($check_impression->num_rows() > 0){
    $row = $check_impression->row_array();
    $temp_id = $row['id'];
    $update_cp = $this->DB2->query("update channel_captiveportal_impression set
     unique_impression = (unique_impression+1) WHERE id = '$temp_id'");
	 
   }
   else{
    $insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
    (location_id, unique_impression, visit_date) VALUES ('$locationId',
    '1', now())");
	
   }
   //if($locationId == '145')
   //{
	  //$this->DB2->query("insert into wifi_offer_sceen (userid, wifioffer_id, locationid, sceen_on) values('$macid', '2', '$locationId', now())");
   //}
   
   
	}
	
	
	public function app_install_click($jsondata)
	{
		$cpofferid=$jsondata->cpofferid;
		$userid=$jsondata->userid;

		 $tabledata=array("is_downloaded"=>"1");
		 $this->DB2->order_by("id","desc");
		 $this->DB2->limit(1, 0);
		 $this->DB2->update('cp_dashboard_analytics',$tabledata,array("cp_offer_id"=>$jsondata->offer_id,"user_id"=>$jsondata->userId));
			 

        $tabledata1 = array("cp_offer_id" => $cpofferid, "location_id" => '', "user_id"=>$userid,"viewed_on"=>date("Y-m-d H:i:s"));
        //is_downloaded 1 in analytics
        $this->DB2->insert("cp_adlocation_default", $tabledata1);
        // cut the budget of app download in case of success page
        $location_id = 0;
        if(isset($jsondata->locationid)){
            $location_id = $jsondata->locationid;
        }
      //  $this->insert_update_budget($location_id,$cpofferid,"app_install_add");
	 
		$data['resultCode'] = '1';
		$data['resultMsg'] = 'Success';
			
			return $data;
   
   
	}

    //dineout api
    public function dineout_offer_click_analytics($jsondata)
    {
        $data = array();
        $locationId = 0;$rest_id = '';$offer_id = '';$offer_title = '';
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        if(isset($jsondata->rest_id)){
            $rest_id = $jsondata->rest_id;
        }
        if(isset($jsondata->offer_id)){
            $offer_id = $jsondata->offer_id;
        }
        if(isset($jsondata->offer_title)){
            $offer_title = $jsondata->offer_title;
        }
        $insert = $this->DB2->query("insert into dineout_offer_click (location_id, rest_id, offer_id, offer_title,
        click_on) VALUES ('$locationId', '$rest_id', '$offer_id', '$offer_title', now())");

        $data['resultCode'] = '1';
        return $data;
    }
    public function dineout_sponsor_click_analytics($jsondata)
    {
        $data = array();
        $locationId = 0;
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        if(count($jsondata->offer_detail) > 0){
            foreach($jsondata->offer_detail as $offer_detail1){
                $insert = $this->DB2->query("insert into dineout_sponsor_click (location_id, rest_id, offer_title,
        click_on) VALUES ('$locationId', '$offer_detail1->rest_id', '$offer_detail1->offer_title', now())");
            }
        }


        $data['resultCode'] = '1';
        return $data;
    }
    public function dineout_offer_click_booking($jsondata)
    {
        $data = array();
        $locationId = 0;
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        $rest_id = $jsondata->rest_id;
        $user_id = $jsondata->user_id;
        $booking_id = $jsondata->booking_id;

        $insert = $this->DB2->query("insert into dineout_offer_book (location_id, user_id, rest_id,
                booking_id, booked_on, responce) VALUES
                ('$locationId', '$user_id', '$rest_id', '$booking_id', now(), '$jsondata->responce')");


        $data['resultCode'] = '1';
        return $data;
    }
    public function dineout_sponsor_click_booking($jsondata)
    {
        $data = array();
        $locationId = 0;
        if(isset($jsondata->locationId)) {
            $locationId = $jsondata->locationId;
        }
        $rest_id = $jsondata->rest_id;
        $user_id = $jsondata->user_id;
        $booking_id = $jsondata->booking_id;

        $insert = $this->DB2->query("insert into dineout_sponsor_book (location_id, user_id, rest_id,
                booking_id, booked_on, responce) VALUES
                ('$locationId', '$user_id', '$rest_id', '$booking_id', now(), '$jsondata->responce')");


        $data['resultCode'] = '1';
        return $data;
    }

    //cp2 api
    public function cp2_offers_totalmb($jsondata) {
        $data = array();
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
		$this->DB2->select("location_type");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$get_location_type=$this->DB2->get();
		
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
			//echo "<pre>"; print_r($rowarr); die;
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
	//	echo "<pre>"; print_R($datarr); die;
        $video_budget = 0;
        $image_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
		 $cpl_budget = 0;
		  $survey_budget = 0;
        if($datarr['resultCode'] == 1){
            foreach($datarr['offers'] as $datarr1){
                if($datarr1['offer_type']=="image_add"){
                    $this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"image_add"));
					$get_budget=$this->DB2->get();
					
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $image_budget = $image_budget+$mb_cost;
                }
                if($datarr1['offer_type']=="video_add"){
                    
					$this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"video_add"));
					$get_budget=$this->DB2->get();
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="captcha_add"){
                    $this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"captcha_add"));
					$get_budget=$this->DB2->get();
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
					
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $captcha_budget = $captcha_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="poll_add"){
                    $this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"poll_add"));
					$get_budget=$this->DB2->get();
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $poll_budget = $poll_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="redeemable_add"){
                    $this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"redeemable_add"));
					$get_budget=$this->DB2->get();
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $redeem_budget = $redeem_budget+$mb_cost;
                }
				
				 else if($datarr1['offer_type']=="cost_per_lead_add"){
                    $this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"cost_per_lead_add"));
					$get_budget=$this->DB2->get();
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $cpl_budget = $cpl_budget+$mb_cost;
                }
				 else if($datarr1['offer_type']=="survey_add"){
                    $this->DB2->select("*");
					$this->DB2->from("cp_location_budget");
					$this->DB2->where(array("offer_type"=>"survey_add"));
					$get_budget=$this->DB2->get();
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $survey_budget = $survey_budget+$mb_cost;
                }
            }
        }
        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'video_add';
        $offer_mb[0]['offer_mb'] = $video_budget+$image_budget;
        $offer_mb[1]['offer_type'] = 'captcha_add';
        $offer_mb[1]['offer_mb'] = $captcha_budget;
        $offer_mb[2]['offer_type'] = 'poll_add';
        $offer_mb[2]['offer_mb'] = $poll_budget;
        $offer_mb[3]['offer_type'] = 'redeemable_add';
          $offer_mb[3]['offer_mb'] = $redeem_budget+$cpl_budget;
		$offer_mb[4]['offer_type'] = 'survey_add';
        $offer_mb[4]['offer_mb'] = $survey_budget;

        $data['resultCode'] = 1;
        $data['resultMessage'] = 'Success';
        $data['offers_budget'] = $offer_mb;
        // echo "<pre>";print_r($data);die;
        return $data;
    }
	
	 public function vadodracp2_offers_totalmb($jsondata) {
        $data = array();
        $get_location_type = $this->DB2->query("select location_type from wifi_location where id =
		'$jsondata->locationId'");
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
        $video_budget = 0;
        $image_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
		 $cpl_budget = 0;
		  $survey_budget = 0;
        if($datarr['resultCode'] == 1){
            foreach($datarr['offers'] as $datarr1){
                if($datarr1['offer_type']=="image_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'image_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $image_budget = $image_budget+$mb_cost;
                }
                if($datarr1['offer_type']=="video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="captcha_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'captcha_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $captcha_budget = $captcha_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="poll_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $poll_budget = $poll_budget+$mb_cost;
                }
                else if($datarr1['offer_type']=="redeemable_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'redeemable_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $redeem_budget = $redeem_budget+$mb_cost;
                }
				
				 else if($datarr1['offer_type']=="cost_per_lead_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'cost_per_lead_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1000*$mb_cost)/30);
                        }
                    }
                    $cpl_budget = $cpl_budget+$mb_cost;
                }
				 else if($datarr1['offer_type']=="survey_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'survey_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $survey_budget = $survey_budget+$mb_cost;
                }
            }
        }
        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'video_add';
        $offer_mb[0]['offer_mb'] = $video_budget;
		$offer_mb[1]['offer_type'] = 'image_add';
        $offer_mb[1]['offer_mb'] = $image_budget;
        $offer_mb[2]['offer_type'] = 'captcha_add';
        $offer_mb[2]['offer_mb'] = $captcha_budget;
        $offer_mb[3]['offer_type'] = 'poll_add';
        $offer_mb[3]['offer_mb'] = $poll_budget;
        $offer_mb[4]['offer_type'] = 'redeemable_add';
          $offer_mb[4]['offer_mb'] = $redeem_budget+$cpl_budget;
		$offer_mb[5]['offer_type'] = 'survey_add';
        $offer_mb[5]['offer_mb'] = $survey_budget;

        $data['resultCode'] = 1;
        $data['resultMessage'] = 'Success';
        $data['offers_budget'] = $offer_mb;
        // echo "<pre>";print_r($data);die;
        return $data;
    }
    // offer listing for cp-2 grid view
    public function captive_ad_data_grid($jsondata) {

		$loc_logo=$this->get_loc_logo($jsondata);
		$this->DB2->select("location_type");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$get_location_type=$this->DB2->get();
		
        $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $datarr = $this->get_ad_id_grid($jsondata);
		$image_add = array();
        $i_add = 0;
        $video_add = array();
        $v_add = 0;
        $captcha_add = array();
        $c_add = 0;
        $poll_add = array();
        $p_add = 0;
        $redeem_add = array();
        $r_add = 0;
		 $cpl_add = array();
        $cp_add = 0;
		 $survey_add = array();
        $sur_add = 0;
        $data=array();
        //if any offer live(not shouut offer)
        if($datarr['resultCode'] == 1){

            foreach($datarr['offers'] as $datarr1){
                $captivelogarr=array("cp_offer_id"=>$datarr1['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr1['gender']),
                    "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr1['age_group']);
                if($datarr1['offer_type']=="image_add")
                {
                    $image_add[$i_add]=$this->get_image_ad_data($datarr1['cp_offer_id'], $location_type);
					if($loc_logo!="")
					{
						$image_add[$i_add]['brand_logo_image']=$loc_logo;
					$image_add[$i_add]['brand_original_image']=$loc_logo;
					}
                    $i_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
                else if($datarr1['offer_type']=="video_add")
                {
                    $video_add[$v_add]=$this->get_video_ad_data($datarr1['cp_offer_id'], $location_type);
					if($loc_logo!="")
					{
						$video_add[$v_add]['brand_logo_image']=$loc_logo;
					$video_add[$v_add]['brand_original_image']=$loc_logo;
					}
                    $v_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }

                else if($datarr1['offer_type']=="poll_add")
                {
                    $poll_add[$p_add]=$this->get_poll_ad_data($datarr1['cp_offer_id'],$jsondata->locationId,
                        $jsondata->userId, $location_type);
						if($loc_logo!="")
					{
						$poll_add[$p_add]['brand_logo_image']=$loc_logo;
					$poll_add[$p_add]['brand_original_image']=$loc_logo;
					}
                    $p_add++;
                    // $this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
                else if($datarr1['offer_type']=="captcha_add")
                {
                    $captcha_add[$c_add]=$this->get_brand_captcha_data($datarr1['cp_offer_id'], $location_type);
						if($loc_logo!="")
						{
							$captcha_add[$c_add]['brand_logo_image']=$loc_logo;
							$captcha_add[$c_add]['brand_original_image']=$loc_logo;
						}
                    $c_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
                else if($datarr1['offer_type']=="redeemable_add")
                {
                    $redeem_add[$r_add]=$this->get_redeemable_offer_data($datarr1['cp_offer_id'], $location_type);
					if($loc_logo!="")
						{
							$redeem_add[$r_add]['brand_logo_image']=$loc_logo;
							$redeem_add[$r_add]['brand_original_image']=$loc_logo;
						}
                    $r_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
				 else if($datarr1['offer_type']=="cost_per_lead_add")
                {
                    $cpl_add[$cp_add]=$this->get_cost_per_lead_add_data($datarr1['cp_offer_id'], $location_type);
					if($loc_logo!="")
						{
							$cpl_add[$cp_add]['brand_logo_image']=$loc_logo;
							$cpl_add[$cp_add]['brand_original_image']=$loc_logo;
						}
                    $cp_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
				 else if($datarr1['offer_type']=="survey_add")
                {
                    $survey_add[$sur_add]=$this->get_survey_ad_data($datarr1['cp_offer_id'], $location_type);
						if($loc_logo!="")
						{
							$survey_add[$sur_add]['brand_logo_image']=$loc_logo;
							$survey_add[$sur_add]['brand_original_image']=$loc_logo;
						}
                    $sur_add++;
                    //$this->add_cp_analytics($captivelogarr);
                    //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                }
            }
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['image_add'] = $image_add;
            $data['video_add'] = $video_add;
            $data['poll_add'] = $poll_add;
            $data['captcha_add'] = $captcha_add;
            $data['redeemable_add'] = $redeem_add;
				$data['cpl_add'] = $cpl_add;
			 $data['survey_add'] = $survey_add;
        }

       /* elseif($datarr['resultCode'] == 0){
            $datarr = $this->get_defaultad_id_grid($jsondata);
            //echo "<pre>"; print_r($datarr);die;
            //if any shouut brand offer live
            if($datarr['resultCode'] == 1){
                foreach($datarr['offers'] as $datarr1){
                    $captivelogarr=array("cp_offer_id"=>$datarr1['cp_offer_id'],"location_id"=>$jsondata->locationId,"user_id"=>$jsondata->userId,'gender'=>strtoupper($datarr1['gender']),
                        "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$datarr1['age_group']);
                    if($datarr1['offer_type']=="image_add")
                    {
                        $image_add[$i_add]=$this->get_image_ad_data($datarr1['cp_offer_id']);
                        $i_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($datarr1['offer_type']=="video_add")
                    {
                        $video_add[$v_add]=$this->get_video_ad_data($datarr1['cp_offer_id']);
                        $v_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }

                    else if($datarr1['offer_type']=="poll_add")
                    {
                        $poll_add[$p_add]=$this->get_poll_ad_data($datarr1['cp_offer_id'],$jsondata->locationId,
                            $jsondata->userId);
                        $p_add++;
                        // $this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($datarr1['offer_type']=="captcha_add")
                    {
                        $captcha_add[$c_add]=$this->get_brand_captcha_data($datarr1['cp_offer_id']);
                        $c_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($datarr1['offer_type']=="redeemable_add")
                    {
                        $redeem_add[$r_add]=$this->get_redeemable_offer_data($datarr1['cp_offer_id']);
                        $r_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                }
                $data['resultCode'] = '1';
                $data['resultMessage'] = 'Success';
                $data['image_add'] = $image_add;
                $data['video_add'] = $video_add;
                $data['poll_add'] = $poll_add;
                $data['captcha_add'] = $captcha_add;
                $data['redeemable_add'] = $redeem_add;
            }
            else{
                $data['resultCode']='0';
                $data['resultMessage']='No Offer live';
            }

        }*/
        else
        {
            $data['resultCode']='0';
            $data['resultMessage']='No Offer live';
			$data['image_add'] = array();
            $data['video_add'] = array();
            $data['poll_add'] = array();
            $data['captcha_add'] = array();
            $data['redeemable_add'] = array();
			$data['cpl_add'] = array();
			$data['survey_add'] = array();
        }


        //Api Cp Analytivs logs
        //echo "<pre>"; print_r($data);die;
        return $data;
    }
    // get offer id which are sceen to user for live brand
    public function get_ad_id_grid($jsondata) {
        $data=array();
        /*$query = $this->DB2->query("SELECT co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co
        .platform_filter,co.gender,co.campaign_day_to_run,col.location_id FROM `cp_offers` co
                    INNER JOIN `cp_offers_location` col ON (co.cp_offer_id=col.cp_offer_id) WHERE co.brand_id!='145'
                     AND co.offer_type!='offer_add' AND co.offer_type!='banner_add' AND co.offer_type!='flash_add'
                    AND co.offer_type!='app_install_add' and col.location_id='" . $jsondata->locationId . "' AND co.`campaign_end_date`>=NOW() and co.campaign_start_date<=NOW()
                     AND co.is_deleted = '0' AND co.status = '1' ORDER BY co.cp_offer_id desc");*/
       
			$now=date("Y-m-d H:i:s");		 
			$this->DB2->select('co.cp_offer_id,co.active_between,co.age_group,co.offer_type,co.platform_filter,co.gender,co.campaign_day_to_run,col.location_id');
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array("co.cp_offer_id!="=>'1',"co.offer_type!="=>"offer_add" ,"co.offer_type!="=>"banner_add","co.offer_type!="=>"flash_add"
                    ,"col.location_id"=>$jsondata->locationId,"co.campaign_end_date>="=>$now,"co.campaign_start_date<="=>$now,
                    "co.is_deleted" => "0", "co.status" => '1',"co.is_coke"=>"0" ,"co.is_mtnl_default_ad"=>"0"));
			$this->DB2->join('cp_offers_location as col', 'co.cp_offer_id = col.cp_offer_id', 'INNER');
			$this->DB2->order_by("co.cp_offer_id","desc");
			$this->DB2->order_by("co.cp_offer_id","desc");
			$query=$this->DB2->get();	
				
		//echo "<pre>"; print_r($query->result()); 
        $liveofferarr = array();
        $userdata=$this->user_detail($jsondata->userId);
        foreach($query->result() as $val) {


            $dayarr = array_filter(explode(",", $val->campaign_day_to_run));
            $platformarr=explode(",",$val->platform_filter);
            $timearr=array();
            if($val->active_between!='all')
            {
                $timearr=$this->timearr($val->active_between);
            }
            if($val->age_group=="<18")
            {
                $offagegrp="18";
            }
            else if($val->age_group=="45+")
            {
                $offagegrp="45";
            }
            else{
                $offagegrp=$val->age_group;
            }


            $filter=true;
            $curday = strtolower(date('l'));
            $curtime=date('H');

            if(is_array($userdata))
            {


                $agegroup=$userdata[0]['age_group'];

                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }
                //filter for gender
                if($userdata[0]['gender']!="")
                {
                    $gender=substr(strtoupper($userdata[0]['gender']),0,1);
                }
                else{
                    $gender="";
                }
                if ($val->gender == "A" || $val->gender==$gender || $gender=="") {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for age group

                if ($offagegrp == "all" || $agegroup==$offagegrp) {

                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr) || $jsondata->platform_filter == "") {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }


                $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
                if($budget_allocate==true && $filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
                }

            }
            else{


                //filter for day
                if ($val->campaign_day_to_run == "all" || in_array($curday, $dayarr)) {
                    $filter=true;
                }
                else{

                    continue;
                }

                //filter for time
                if ($val->active_between == "all" || in_array($curtime, $timearr)) {

                    $filter=true;
                }
                else{

                    continue;
                }


                //filter for Platform
                if(isset($jsondata->platform_filter))
                {

                    if ($jsondata->platform_filter == "all" || in_array($jsondata->platform_filter,$platformarr || $jsondata->platform_filter == "")) {

                        $filter=true;
                    }
                    else{

                        continue;
                    }

                }

               // $budget_allocate=$this->allocate_budget_views($jsondata->locationId,$val->cp_offer_id);
               // if($budget_allocate==true && $filter=true){
                    $liveofferarr[] = $val->cp_offer_id;
               // }

            }


        }
        $liveofferarr = array_unique($liveofferarr);
		
        // echo "<pre>"; print_R($query->result());die;
        // get id who already sceen
        $this->DB2->select("cp_offer_id");
		$this->DB2->from("cp_adlocation_default");
		$this->DB2->where(array("user_id"=> $jsondata->userId));
		$query=$this->DB2->get();	
		
        $offer_sceen_array = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $offer_sceens) {
                $offer_sceen_array[] = $offer_sceens->cp_offer_id; // create array of offer sceen
            }
        }
		//echo "<pre>"; print_R($liveofferarr);
		//echo "<pre>"; print_R($offer_sceen_array);
		//die;
        $liveofferarr = array_diff($liveofferarr, $offer_sceen_array);
        if (!empty($liveofferarr)) {
            $i = 0;
            $data['resultCode'] = '1';
            $offers = array();
            foreach($liveofferarr as $liveofferarr1){
               
				$this->DB2->select("cp_offer_id,offer_type");
				$this->DB2->from("cp_offers");
				$this->DB2->where(array("cp_offer_id"=> $liveofferarr1));
				$query=$this->DB2->get();	
                $rowarr=$query->row_array();
                $offers[$i]['cp_offer_id'] = $rowarr['cp_offer_id'];
                $offers[$i]['offer_type'] = trim($rowarr['offer_type']);
                $offers[$i]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
                ($userdata[0]['gender'],0,1):'';
                $offers[$i]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
                    ?$userdata[0]['age_group']:'';
                $i++;
            }
            $data['offers'] = $offers;
        }
        else {
            $data['resultCode'] = '0';
            $data['cp_offer_id'] = '';
            $data['offer_type'] = 'default';
        }


        return $data;
    }
   //deprecated function not in use
    // get offer id which are scee to user for shouut brand
    public function get_defaultad_id_grid($jsondata) {
        $data=array();
        $userdata=$this->user_detail($jsondata->userId);
		
		$this->DB2->select("Provider_id");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query1=$this->DB2->get();
			$rowarr1=$query1->row_array();
			 if($rowarr1['Provider_id']==39)
			{
				
				$this->DB2->select("cp_offer_id,offer_type");
				$this->DB2->from("cp_offers");
				$this->DB2->where(array("cp_offer_id"=>"89"));
				$query=$this->DB2->get();
			}
			else{
				
				$this->DB2->select("cp_offer_id,offer_type");
				$this->DB2->from("cp_offers");
				$this->DB2->where(array("cp_offer_id"=>"1"));
				$query=$this->DB2->get();
			}
            //$query=$this->DB2->query("select cp_offer_id,offer_type from cp_offers where cp_offer_id='1'");
            $rowarr=$query->row_array();
			  $data['resultCode'] = '1';
            $data['offers'][0]['cp_offer_id'] = $rowarr['cp_offer_id'];
            $data['offers'][0]['offer_type'] = trim($rowarr['offer_type']);
            $data['offers'][0]['gender']=(isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr($userdata[0]['gender'],0,1):'';
            $data['offers'][0]['age_group']=(isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')?$userdata[0]['age_group']:'';


        return $data;
    }
    // insert offer sceen by user , insert view in analytics
    public function cp_offer_sceen_user($jsondata){
        // insert offer view analytics
        $data=array();
        if(isset($jsondata->locationid) ){
            $user_detail = $this->user_detail($jsondata->userid);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }

            $captivelogarr=array("cp_offer_id"=>$jsondata->cpofferid,"location_id"=>$jsondata->locationid,
                "user_id"=>$jsondata->userid,'gender'=>strtoupper($gender),
                "platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"macid"=>$jsondata->macid,"usertype"=>1,"age_group"=>$age_group);
            $this->add_cp_analytics($captivelogarr);
        }

        //budget cut and user sceen entry in case of vide add and image add
        if(isset($jsondata->offertype)){
            if($jsondata->offertype == 'video_add' || $jsondata->offertype == 'video' ||
                $jsondata->offertype == 'image_add' || $jsondata->offertype == 'image' || $jsondata->offertype == 'app_install_add' || $jsondata->offertype == 'survey_add' || $jsondata->offertype == 'scratch'){
              //  $this->insert_update_budget($jsondata->locationid,$jsondata->cpofferid,$jsondata->offertype);
                $offer_id = $jsondata->cpofferid;
                $user_id = $jsondata->userid;
                $location_id = '0';
                if(isset($jsondata->locationid) ){
                    $location_id = $jsondata->locationid;
                }
				$curtime=date("Y-m-d H:i:s");
                $query = $this->DB2->query("insert into cp_adlocation_default(cp_offer_id, user_id, location_id,viewed_on) VALUES
				('$offer_id', '$user_id', '$location_id','$curtime')");
				
				//scratch offer seen
				if(isset($jsondata->scratchid) && $jsondata->scratchid!=''){
					$tabledata=array("scratch_id"=>$jsondata->scratchid,"offer_id"=>$offer_id,"user"=>$user_id,"redeemed_on"=>date("Y-m-d H:i:s"),"loc_id"=>$location_id,"scratch_coupon"=>$jsondata->scratch_coupon);
					$tabledata1=array("scratch_id"=>$jsondata->scratchid,"offer_id"=>$offer_id,"user"=>$user_id,"redeemed_on"=>date("Y-m-d H:i:s"));
					$this->DB2->insert("cp_scratch_offer_seen",$tabledata);
					$this->DB2->insert("cp_scratch_offer_seen_temp",$tabledata1);
					$query=$this->DB2->query("select scratch_offer_title from cp_scratch_offer where id='".$jsondata->scratchid."'");
					$rowarr=$query->row_array();
					$msg='';
					$msg.="You have recieved ".$rowarr['scratch_offer_title'].". Voucher Code is ".$jsondata->scratch_coupon;
					$this->send_iands_sms($user_id,$msg);
				}
            }

        }
        // app download case is downloaded = 1
        if($jsondata->offertype == 'app_install_add'){
            $cpofferid=$jsondata->cpofferid;
            $userid=$jsondata->userid;
            $update_cp = $this->DB2->query("update cp_dashboard_analytics set
     is_downloaded = '1' WHERE cp_offer_id = '$cpofferid' and user_id='$userid' order by id desc limit 1");
        }
        $data['resultCode'] = '1';
        return $data;
    }
    // offer detail in grid view
   public function captive_ad_data_grid_detail($jsondata) {
		$loc_logo=$this->get_loc_logo($jsondata);
        $offer_id = $jsondata->offer_id;
        $offer_type = $jsondata->offer_type;
        $location_id = $jsondata->locationId;
        
		$this->DB2->select("location_type");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$get_location_type=$this->DB2->get();
		 $location_type = '';
        if($get_location_type->num_rows() > 0){
            $rowarr=$get_location_type->row_array();
            $location_type = $rowarr['location_type'];
        }
        $offer_data = array();
        $data=array();


        if($offer_type=="video_add" || $offer_type == 'video')
        {
            $offer_data=$this->get_video_ad_data($offer_id, $location_type);
			if($loc_logo!="")
			{
					$offer_data['brand_logo_image']=$loc_logo;
					$offer_data['brand_original_image']=$loc_logo;
			}
        }
        else if($offer_type=="image_add" || $offer_type == 'image')
        {
            $offer_data = $this->get_image_ad_data($offer_id, $location_type);
			if($loc_logo!="")
			{
					$offer_data['brand_logo_image']=$loc_logo;
					$offer_data['brand_original_image']=$loc_logo;
			}
        }
        else if($offer_type=="poll_add")
        {
          
			 $this->DB2->select('co.cp_offer_id,co.redirect_url,co.poll_logo,co.poll_original,co.poll_brand_title,co.offer_title,co.brand_id,co.campaign_name,co.image_original,
			co.image_small,co.`campaign_end_date`,br.brand_name,br.`original_logo`,
			br.original_logo_logoimage,cpq.id,cpq.question,cpq.option1,cpq.option2,cpq.option3,cpq.option4');
			$this->DB2->from('cp_offers co');
			$this->DB2->where(array("co.cp_offer_id"=>$offer_id));
			$this->DB2->join('cp_offers_question as cpq', 'co.cp_offer_id=cpq.cp_offer_id', 'INNER');
			$this->DB2->join('brand as br', 'co.brand_id=br.brand_id', 'INNER');
			$query=$this->DB2->get();
			$this->DB2->select("*");
			$this->DB2->from("cp_location_budget");
			$this->DB2->where(array("offer_type"=>"poll_add"));
			$get_budget=$this->DB2->get();
			
            $mb_cost = '0';
            if($get_budget->num_rows() > 0){
                $budget_row = $get_budget->row_array();
                if($location_type == '1'){
                    $mb_cost = $budget_row['star'];
                    $mb_cost = ceil((1024*$mb_cost)/30);
                }
                elseif($location_type == '2'){
                    $mb_cost = $budget_row['premium'];
                    $mb_cost = ceil((1024*$mb_cost)/30);
                }
                elseif($location_type == '3'){
                    $mb_cost = $budget_row['mass'];
                    $mb_cost = ceil((1024*$mb_cost)/30);
                }
            }
            if($query->num_rows()>0) {
                $rowarr = $query->row_array();
                $offer_data['resultCode'] = '1';
                $offer_data['resultMessage'] = 'Success';
                $offer_data['rewradtype'] = 'poll';
                $offer_data['withoffer'] = '0';
                $offer_data['mb_cost'] = $mb_cost;
                $offer_data['brand_id'] = $rowarr['brand_id'];
                $offer_data['brand_name'] = strtoupper($rowarr['brand_name']);
                $offer_data['offers_small_image'] = ($rowarr['image_small'] != '') ? CAMPAIGN_IMAGEPATH . "small/" . $rowarr['image_small'] : "";
                $offer_data['offers_original_image'] = ($rowarr['image_original'] != '') ? CAMPAIGN_IMAGEPATH . "original/" . $rowarr['image_original'] : "";
				if($loc_logo!="")
					{
						$offer_data['brand_logo_image']=$loc_logo;
					$offer_data['brand_original_image']=$loc_logo;
					}
					else{
						 $offer_data['brand_logo_image'] = ($rowarr['original_logo_logoimage'] != '') ?ISP_IMAGEPATH."logo/" . $rowarr['original_logo_logoimage'] : "";
                $offer_data['brand_original_image'] = ($rowarr['original_logo'] != '') ? ISP_IMAGEPATH."logo/". $rowarr['original_logo'] : "";
					}
               
                $offer_data['validTill'] = '';
                $offer_data['validTill_fromTime'] = '';
                $offer_data['store_count'] = '';
                $offer_data['video_url'] = '';
                $offer_data['offer_id'] = $rowarr['cp_offer_id'];
                $offer_data['promotion_name'] = $rowarr['offer_title'];
                $offer_data['poll_question_id'] = $rowarr['id'];
                $offer_data['poll_question'] = $rowarr['question'];
                $offer_data['poll_option'][0] = $rowarr['option1'];
                $offer_data['poll_option'][1] = $rowarr['option2'];
                $offer_data['poll_option'][2] = $rowarr['option3'];
                $offer_data['poll_option'][3] = $rowarr['option4'];
                $offer_data['offer_id'] = $rowarr['cp_offer_id'];
                $offer_data['poll_logo'] = $rowarr['poll_logo'];
                $offer_data['poll_original'] = $rowarr['poll_original'];
                $offer_data['poll_brand_title'] = $rowarr['poll_brand_title'];

                $offer_data['captch_text'] = "";
                $offer_data['captcha_authenticity'] = "";
                $offer_data['url'] = $rowarr['redirect_url'];
            }
        }
        else if($offer_type=="captcha_add")
        {
            $offer_data=$this->get_brand_captcha_data($offer_id, $location_type);
			if($loc_logo!="")
			{
					$offer_data['brand_logo_image']=$loc_logo;
					$offer_data['brand_original_image']=$loc_logo;
			}

        }
        else if($offer_type=="redeemable_add")
        {
            $offer_data=$this->get_redeemable_offer_data($offer_id, $location_type);
			if($loc_logo!="")
			{
					$offer_data['brand_logo_image']=$loc_logo;
					$offer_data['brand_original_image']=$loc_logo;
			}

        }


        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        $data['add_detail'][0] = $offer_data;
        //Api Cp Analytivs logs
        // echo "<pre>"; print_r($data);die;
        return $data;
    }

   public function get_mb_data($jsondata)
    {
		//echo "<pre>"; print_R($jsondata);
		
		$this->DB2->select("location_uid");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query1=$this->DB2->get();
		$rowarr=$query1->row_array();
		
        $data=array();
		$this->DB2->select("*");
		$this->DB2->from("wifi_location_voucher");
		$this->DB2->where(array("voucher_selling_price"=>$jsondata->amount,"location_uid"=>$rowarr['location_uid']));
		$query=$this->DB2->get();
		
        if($query->num_rows()>0)
        {
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $i=0;
            foreach($query->result() as $val)
            {
                $data['mbdata'][$i]['data_name']=$val->voucher_name;
                $data['mbdata'][$i]['data_mb']=$val->voucher_mb;
                $data['mbdata'][$i]['amt']=$val->voucher_selling_price;

            }
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'No such plan';
        }
        return $data;

    }
	
	 public function get_mb_list($jsondata)
    {
		
		
		$this->DB2->select("location_uid");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationid));
		$query1=$this->DB2->get();
		$rowarr=$query1->row_array();
        $data=array();
        
		$this->DB2->select("id");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationid,"is_voucher_used"=>"1"));
		$query=$this->DB2->get();
		
        if($query->num_rows()>0)
        {
			
			$this->DB2->select("*");
			$this->DB2->from("wifi_location_voucher");
			$this->DB2->where(array("location_uid"=>$rowarr['location_uid']));
			$query1=$this->DB2->get();
			if($query1->num_rows()>0)
			{
				 $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $i=0;
            foreach($query1->result() as $val)
            {
                $data['mbdata'][$i]['data_name']=$val->voucher_name;
                $data['mbdata'][$i]['data_mb']=$val->voucher_mb;
                $data['mbdata'][$i]['amt']=$val->voucher_selling_price;
				$i++;
            }
			}
			else{
				 $data['resultCode'] = '0';
            $data['resultMessage'] = 'No such plan';
			}
           
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'No such plan';
        }
        return $data;

    }
    public function insert_paytm_success($jsondata)
    {
        $data=array();
        $tabledata=array("user_id"=>$jsondata->MERC_UNQ_REF,"order_id"=>$jsondata->ORDERID,"amount"=>$jsondata->TXNAMOUNT,"gatewayname"=>$jsondata->GATEWAYNAME,
            "msg"=>$jsondata->RESPMSG,
            "txn_date"=>$jsondata->TXNDATE,"txnid"=>$jsondata->TXNID);
        $this->DB2->insert("paytm_sucess_user", $tabledata);
        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;


    }
    public function activate_voucher($jsondata)
    {
        $data=array();
		$this->DB2->select("vmv.vc_code,vmc.data_amnt");
		$this->DB2->from("vm_assign_voucherid vmv");
		$this->DB2->where(array("vmv.customer_mobile"=>$jsondata->mobile,"vmv.vc_code"=>$jsondata->voucher,"vmv.is_used"=>"0"));
		$this->DB2->join('vm_mb_cost as vmc', ' vmv.cost_id = vmc.id', 'INNER');
		$query=$this->DB2->get();
		

	   if($query->num_rows()>0)
        {
            $rowarr=$query->row_array();
            $data['resultCode'] = '1';
            $data['resultMessage'] = 'Success';
            $data['mb']=$rowarr['data_amnt'];

        }
        else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = 'Voucher Not Available';
            $data['mb']='';
        }
        return $data;

    }
    public function voucher_availed($jsondata)
    {
        $data=array();
        $tabledata=array("is_used"=>1);
        $where_array = array('customer_mobile' => $jsondata->mobile, 'vc_code' => $jsondata->voucher);
        $this->DB2->where($where_array);
        $this->DB2->update('vm_assign_voucherid', $tabledata);

        $data['resultCode'] = '1';
        $data['resultMessage'] = 'Success';
        return $data;

    }

    public function cp2_offers_totalmb_website($jsondata){
        $data = array();
        $video_budget = 0;
        $image_budget = 0;
        $captcha_budget = 0;
        $poll_budget = 0;
        $redeem_budget = 0;
        $app_install_budget = 0;
        $location = $jsondata->location;
        $location = explode('::',$location);
        $user_latitude = $location[0];
        $user_longitude = $location['1'];
        //get nearest location id
        $location_query = $this->DB2->query("SELECT(
            6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
            )
        ) as distance, id from wifi_location HAVING distance <= 5");
        $live_offers = array();
        $live_offers_counter = 0;
        if($location_query->num_rows() > 0){
            $data['resultCode'] = 1;
            $data['resultMessage'] = 'Success';
            $addarr=array();
            foreach($location_query->result() as $location_query1){
                $location_id = $location_query1->id;
                $send_data = json_decode(json_encode(array("locationId"=>$location_id, "userId" => $jsondata->userId,
                    "apikey" => $jsondata->apikey)));
                $datarr = $this->get_ad_id_grid($send_data);

                if($datarr['resultCode'] == 1){

                    foreach($datarr['offers'] as $datarr1){
                        if(!in_array($datarr1['cp_offer_id'],$addarr)) {
                            $live_offers[$live_offers_counter]['cp_offer_id'] = $datarr1['cp_offer_id'];
                            $live_offers[$live_offers_counter]['offer_type'] = $datarr1['offer_type'];
                            $live_offers[$live_offers_counter]['gender'] = $datarr1['gender'];
                            $live_offers[$live_offers_counter]['age_group'] = $datarr1['age_group'];
                            $live_offers[$live_offers_counter]['location_id'] = $location_id;
                            $addarr[] = $datarr1['cp_offer_id'];
                            $live_offers_counter++;
                        }
                    }
                }
            }
            foreach($live_offers as $live_offers1){
                $location_type = '';
                $location_id = $live_offers1['location_id'];
                $get_location_type = $this->DB2->query("select location_type from wifi_location where id = '$location_id'");
                if($get_location_type->num_rows() > 0){
                    $rowarr=$get_location_type->row_array();
                    $location_type = $rowarr['location_type'];
                }
                if($live_offers1['offer_type']=="image_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'image_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $image_budget = $image_budget+$mb_cost;
                }
                if($live_offers1['offer_type']=="video_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'video_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $video_budget = $video_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="captcha_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'captcha_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $captcha_budget = $captcha_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="poll_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'poll_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $poll_budget = $poll_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="redeemable_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'redeemable_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $redeem_budget = $redeem_budget+$mb_cost;
                }
                else if($live_offers1['offer_type']=="app_install_add"){
                    $get_budget = $this->DB2->query("select * from cp_location_budget where offer_type = 'app_install_add'");
                    $mb_cost = '0';
                    if($get_budget->num_rows() > 0){
                        $budget_row = $get_budget->row_array();
                        if($location_type == '1'){
                            $mb_cost = $budget_row['star'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '2'){
                            $mb_cost = $budget_row['premium'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                        elseif($location_type == '3'){
                            $mb_cost = $budget_row['mass'];
                            $mb_cost = ceil((1024*$mb_cost)/30);
                        }
                    }
                    $app_install_budget = $app_install_budget+$mb_cost;
                }
            }
        }
        else{
            $data['resultCode'] = 0;
            $data['resultMessage'] = 'No location found in 5 km';
        }

        $offer_mb = array();
        $offer_mb[0]['offer_type'] = 'video_add';
        $offer_mb[0]['offer_mb'] = $video_budget+$image_budget;
        $offer_mb[1]['offer_type'] = 'captcha_add';
        $offer_mb[1]['offer_mb'] = $captcha_budget;
        $offer_mb[2]['offer_type'] = 'poll_add';
        $offer_mb[2]['offer_mb'] = $poll_budget;
        $offer_mb[3]['offer_type'] = 'redeemable_add';
        $offer_mb[3]['offer_mb'] = $redeem_budget;
        $offer_mb[4]['offer_type'] = 'app_install_add';
        $offer_mb[4]['offer_mb'] = $app_install_budget;

        $data['offers_budget'] = $offer_mb;
        // echo "<pre>";print_r($data);die;
        return $data;
    }

    public function captive_ad_data_grid_website($jsondata) {
        $data = array();
        $image_add = array();
        $i_add = 0;
        $video_add = array();
        $v_add = 0;
        $captcha_add = array();
        $c_add = 0;
        $poll_add = array();
        $p_add = 0;
        $redeem_add = array();
        $r_add = 0;
        $app_install_add = array();
        $a_i_add = 0;
        $location = $jsondata->location;
        $location = explode('::',$location);
        $user_latitude = $location[0];
        $user_longitude = $location['1'];
        //get nearest location id
        $location_query = $this->DB2->query("SELECT(
            6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
            )
        ) as distance, id from wifi_location HAVING distance <= 5");
		
		
        $live_offers = array();
        $live_offers_counter = 0;
        if($location_query->num_rows() > 0){

            $addarr=array();
            foreach($location_query->result() as $location_query1){
                $location_id = $location_query1->id;
                $send_data = json_decode(json_encode(array("locationId"=>$location_id, "userId" => $jsondata->userId,
                    "apikey" => $jsondata->apikey)));
                $datarr = $this->get_ad_id_grid($send_data);
                if($datarr['resultCode'] == 1){
                    foreach($datarr['offers'] as $datarr1){
                        if(!in_array($datarr1['cp_offer_id'],$addarr)) {
                            $live_offers[$live_offers_counter]['cp_offer_id'] = $datarr1['cp_offer_id'];
                            $live_offers[$live_offers_counter]['offer_type'] = $datarr1['offer_type'];
                            $live_offers[$live_offers_counter]['gender'] = $datarr1['gender'];
                            $live_offers[$live_offers_counter]['age_group'] = $datarr1['age_group'];
                            $live_offers[$live_offers_counter]['location_id'] = $location_id;
                            $addarr[] = $datarr1['cp_offer_id'];
                            $live_offers_counter++;
                        }
                    }
                }
            }
            if(count($live_offers) > 0){
                $data['resultCode'] = 1;
                $data['resultMessage'] = 'Success';
                foreach($live_offers as $live_offers1){
                    $location_type = '';
                    $location_id = $live_offers1['location_id'];
                    $get_location_type = $this->DB2->query("select location_type from wifi_location where id = '$location_id'");
                    if($get_location_type->num_rows() > 0){
                        $rowarr=$get_location_type->row_array();
                        $location_type = $rowarr['location_type'];
                    }
                    if($live_offers1['offer_type']=="image_add")
                    {
                        $responce = $this->get_image_ad_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $image_add[$i_add]=$responce;
                        $i_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type']=="video_add")
                    {
                        $responce = $this->get_video_ad_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $video_add[$v_add]=$responce;
                        $v_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }

                    else if($live_offers1['offer_type']=="poll_add")
                    {
                        $responce = $this->get_poll_ad_data($live_offers1['cp_offer_id'],$location_id,
                            $jsondata->userId, $location_type);
                        $responce['location_id'] = $location_id;
                        $poll_add[$p_add]=$responce;
                        $p_add++;
                        // $this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type']=="captcha_add")
                    {
                        $responce = $this->get_brand_captcha_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $captcha_add[$c_add]=$responce;
                        $c_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type']=="redeemable_add")
                    {
                        $responce = $this->get_redeemable_offer_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $redeem_add[$r_add]=$responce;
                        $r_add++;
                        //$this->add_cp_analytics($captivelogarr);
                        //$this->insert_update_budget($jsondata->locationId,$datarr['cp_offer_id'],$datarr['offer_type']);
                    }
                    else if($live_offers1['offer_type'] == "app_install_add"){
                        $responce = $this->get_app_download_ad_data($live_offers1['cp_offer_id'], $location_type);
                        $responce['location_id'] = $location_id;
                        $app_install_add[$a_i_add]=$responce;
                        $a_i_add++;
                    }
                }
                $data['image_add'] = $image_add;
                $data['video_add'] = $video_add;
                $data['poll_add'] = $poll_add;
                $data['captcha_add'] = $captcha_add;
                $data['redeemable_add'] = $redeem_add;
                $data['app_install_add'] = $app_install_add;
            }
            else{
                $data['resultCode'] = 0;
                $data['resultMessage'] = 'No Offer live';
            }

        }
        else{
            $data['resultCode'] = 0;
            $data['resultMessage'] = 'No location found in 5 km';
        }
        //  echo "<pre>";print_r($data);die;
        return $data;
    }

    public function skhotspot_voucher_validate($jsondata){
        $data = array();
        $mobile = '';
        if(isset($jsondata->voucher)){
            $mobile = $jsondata->mobile;
        }
        $voucher = '';
        if(isset($jsondata->voucher)){
            $voucher = $jsondata->voucher;
        }
        if($mobile != ''){

            if($voucher != ''){
                //check already assing voucher with this num
                $query_check = $this->DB2->query("select * from surajkund_voucher WHERE  is_used = '1' and mobile_no = '$mobile'");
                if($query_check->num_rows() > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Valid mobile";
                    $data['voucher']=$voucher;
                }
                else{
                    // for new voucher
                    $query = $this->DB2->query("select * from surajkund_voucher WHERE voucher = '$voucher' AND is_used = '0'");
                    if($query->num_rows() > 0){
                        $update = $this->DB2->query("update surajkund_voucher set is_used = '1', mobile_no = '$mobile',
                used_on = now() WHERE voucher = '$voucher'");
                        $data['resultCode'] = '1';
                        $data['resultMsg'] = "Successfully assign voucher with this mobile";
                        $data['voucher']=$voucher;
                    }
                    else{
                        $data['resultCode'] = '0';
                        $data['resultMsg'] = "Not valid voucher code or already used";
                    }
                }
            }
            //for already used voucher
            else{
                // check mobile num have already assing voucher then it is ok
                $query = $this->DB2->query("select * from surajkund_voucher WHERE mobile_no = '$mobile' order by id desc limit 1");
                if($query->num_rows() > 0){
                    $rowarr=$query->row_array();
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Mobile is valid";
                    $data['voucher']=$rowarr['voucher'];
                }
                else{
                    $data['resultCode'] = '2';
                    $data['resultMsg'] = "Not assign any vocher with this mobile";
                }
            }
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Invalid mobile";
        }

        return $data;
    }
	
	
		
	//Survey Api
	
	public function get_survey_question($jsondata)
	{
		$data=array();
		if($jsondata->offer_id!="")
		{
			$this->DB2->select("*");
			$this->DB2->from("cp_survey_question");
			$this->DB2->where(array("cp_offer_id"=>$jsondata->offer_id));
			$this->DB2->order_by("ques_id","asc");
			 $this->DB2->limit(1, 0);
			$query=$this->DB2->get();
			
			$this->DB2->select("*");
			$this->DB2->from("cp_survey_question");
			$this->DB2->where(array("cp_offer_id"=>$jsondata->offer_id));
			$query2=$this->DB2->get();
		
		
			$this->DB2->select("offer_title");
			$this->DB2->from("cp_offers");
			$this->DB2->where(array("cp_offer_id"=>$jsondata->offer_id));
			$query3=$this->DB2->get();
			
			
			$rowarr=$query->row_array();
			$rowarr1=$query3->row_array();
			
			//echo "<pre>";print_R($rowarr); die;
			
			$data['resultCode']=1;
			$data['resultMsg']='Success';
			$data['question_id']=$rowarr['ques_id'];
			$data['question']=$rowarr['question'];
			$data['question_id']=$rowarr['ques_id'];
			$data['total_question']=$query2->num_rows();
			$data['promotion_name']=$rowarr1['offer_title'];
			$data['option_choice']=$rowarr['option_choice'];
			$query1=$this->DB2->query("select option_id,`option_val` from cp_survey_option where ques_id='".$rowarr['ques_id']."'");
			
			$this->DB2->select("option_id,`option_val`");
			$this->DB2->from("cp_survey_option");
			$this->DB2->where(array("ques_id"=>$rowarr['ques_id']));
			$query1=$this->DB2->get();
			

			$i=0;
			foreach($query1->result() as $val)
			{
				$data['option'][$i]['option_id']=$val->option_id;
				$data['option'][$i]['option_name']=$val->option_val;
				$i++;
			}
			$this->clear_user_option($jsondata);
			
		}
		else{
			$data['resultCode']=0;
			$data['resultMsg']='Fail';
		}
		return $data;
		
	}
	
	
	public function get_subsequent_question($jsondata)
	{
		$data=array();
		if($jsondata->ques_id!="")
		{
			
			$this->DB2->select("*");
			$this->DB2->from("cp_survey_option");
			$this->DB2->where(array("ques_id"=>$jsondata->ques_id,"option_id"=>$jsondata->option_id));
			$query=$this->DB2->get();
			$rowarr=$query->row_array();
			
			if($rowarr['next_ques']==0)
			{
				
				$this->DB2->select("ques_id");
				$this->DB2->from("cp_survey_question");
				$this->DB2->where(array("cp_offer_id"=>$jsondata->offer_id));
				$query1=$this->DB2->get();
				$rowarr=$query->row_array();
				$questarr=array();
				foreach($query1->result() as $val)
				{
					$questarr[]=$val->ques_id;
				}
				
				if(end($questarr)== $rowarr['ques_id'])
				{
						$data['resultCode']=2;
						$data['resultMsg']='This is end of Survey';
				}
				else{
					$nextkey=array_search($rowarr['ques_id'],$questarr)+1;
					$data=$this->get_question_data($questarr[$nextkey],$jsondata->offer_id);
				}
				$this->insert_user_option($jsondata);
				
			}
			else if($rowarr['next_ques']==1){
					$data['resultCode']=2;
						$data['resultMsg']='This is end of Survey';
						$this->insert_user_option($jsondata);
						//$this->insert_update_budget($jsondata->locationId,$jsondata->offer_id,'survey_add');
			}
			else{
				$quesid=$rowarr['next_ques'];
				
				$data=$this->get_question_data($quesid,$jsondata->offer_id);
				$this->insert_user_option($jsondata);
			}
			
			
		}
		else{
			
			$data['resultCode']=0;
			$data['resultMsg']='Fail';
		}
		
		return $data;
		
	}
	
	
	public function get_question_data($quesid,$offer_id)
	{
		
		$query3=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
		$query=$this->DB2->query("select * from cp_survey_question where ques_id='".$quesid."'");
		$query4=$this->DB2->query("select offer_title from cp_offers where cp_offer_id='".$offer_id."'");
		$rowarr=$query->row_array();
		$rowarr1=$query4->row_array();
		$data['resultCode']=1;
		$data['resultMsg']='Success';
		$data['total_question']=$query3->num_rows();
		$data['question_id']=$rowarr['ques_id'];
		$data['promotion_name']=$rowarr1['offer_title'];
		$data['option_choice']=$rowarr['option_choice'];
			
			$data['question']=$rowarr['question'];
			$data['question_id']=$rowarr['ques_id'];
			$query1=$this->DB2->query("select option_id,`option_val` from cp_survey_option where ques_id='".$rowarr['ques_id']."'");
			

			$i=0;
			foreach($query1->result() as $val)
			{
				$data['option'][$i]['option_id']=$val->option_id;
				$data['option'][$i]['option_name']=$val->option_val;
				$i++;
			}
			
			$query2=$this->DB2->query("select * from cp_survey_question where cp_offer_id='".$offer_id."'");
			$seqarr=array();
			$x=1;
			foreach($query2->result() as $val1)
			{
				$seqarr[$x]=$val1->ques_id;
				$x++;
			}
			
			$data['question_number']=array_search($quesid,$seqarr);
			
			return $data;
	}
	
	
	public function clear_user_option($jsondata)
	{
		
		$this->DB2->where('user_id', $jsondata->userId);
		$this->DB2->where('cp_offer_id', $jsondata->offer_id);
		$this->DB2->delete('cp_survey_answer'); 
		
	}
	
	
	public function insert_user_option($jsondata)
	{
		 $user_detail = $this->user_detail($jsondata->userId);
            $gender = '';
            $age_group = '';
            if(count($user_detail) > 0) {
                if(isset($user_detail[0]['gender'])){
                    $gender = $user_detail[0]['gender'];
                }
                if(isset($user_detail[0]['age_group'])){
                    $age_group = $user_detail[0]['age_group'];
                }
            }
			
		
		
		$optionarr=array_filter(explode(",",$jsondata->option_id));
		foreach($optionarr as $val)
		{
		$tabledata=array("question_id"=>$jsondata->ques_id,"option_id"=>$val,"user_id"=>$jsondata->userId,"cp_offer_id"=>$jsondata->offer_id,"platform"=>$jsondata->via,"osinfo"=>$jsondata->platform_filter,"gender"=>$gender,"age_group"=>$age_group,"viewed_on"=>date("Y-m-d H:i:s"));
		$this->DB2->insert('cp_survey_answer',$tabledata);
		}
	}
	
	public function time_conversion($time)
	{
		

 // time duration in seconds

			$days = floor($time / (60 * 60 * 24));
			$time -= $days * (60 * 60 * 24);

			$hours = floor($time / (60 * 60));
			$time -= $hours * (60 * 60);

			$minutes = floor($time / 60);
			$time -= $minutes * 60;

			$seconds = floor($time);
			$time -= $seconds;
			if($minutes>1)
			{
				$time="{$minutes} Minutes {$seconds} Seconds";
			}
			else if($minutes==1)
			{
				$time="{$minutes} Minutes";
			}
			else{
				$time="{$seconds} Seconds";
			}

			return $time;

	}
	
	
	public function wifi_location_macid($jsondata){
		$data = array();
		$locationid = $jsondata->locationId;
		$this->DB2->select("wl.id, wl.access_point_macid,wl.latitude, wl.longitude");
		$this->DB2->from("wifi_location wl");
		$this->DB2->where(array("wl.id"=>$locationid));
		$query=$this->DB2->get();
		
		
		if($query->num_rows() > 0){
			
			$query1=$this->db->query(" SELECT `logo_image`  from sht_isp_detail where isp_uid='". $jsondata->isp_uid."'");
			$this->db->select("logo_image");
			$this->db->from("sht_isp_detail");
			$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
			$query1=$this->db->get();
			
			$rowarr=$query1->row_array();
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$row = $query->row_array();
			$data['locationid'] = $row['id'];
			$data['APMAC'] = $row['access_point_macid'];
			$data['lat'] = $row['latitude'];
			$data['long'] = $row['longitude'];
			$provider_image_logo = '';
			if($rowarr['logo_image'] != ''){
				$provider_image_logo = ISPPATH."logo/".$rowarr['logo_image'];
			}
			  $loc_logo=$this->get_loc_logo($jsondata);
			if($loc_logo!="")
			{
				
           $data['provider_image_logo'] = $loc_logo;
			}
			else{
				$data['provider_image_logo'] = $provider_image_logo;
			}
			
			$data['provider_id'] = $jsondata->isp_uid;


		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}
	
	
		public function cp_top_risht_isp_logo($jsondata){
		$data = array();
		$locationid = $jsondata->locationId;
		
		
		$this->DB2->select("wl.id, wl.access_point_macid,wl.latitude, wl.longitude");
		$this->DB2->from("wifi_location wl");
		$this->DB2->where(array("wl.id"=>$locationid));
		$query=$this->DB2->get();
		
		if($query->num_rows() > 0){
			
			
			$this->db->select("logo_image");
			$this->db->from("sht_isp_detail");
			$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
			$query1=$this->db->get();
			$rowarr=$query1->row_array();
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$row = $query->row_array();
			$data['locationid'] = $row['id'];
			$data['APMAC'] = $row['access_point_macid'];
			$data['lat'] = $row['latitude'];
			$data['long'] = $row['longitude'];
			$provider_image_logo = '';
			if($rowarr['logo_image'] != ''){
				$provider_image_logo = ISPPATH."logo/".$rowarr['logo_image'];
			}
			
				$data['provider_image_logo'] = $provider_image_logo;
			
			
			$data['provider_id'] = $jsondata->isp_uid;


		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}
	
	

	public function captive_portal_location_wifi_offer($jsondata){
		$data = array();
		$today_date = date('Y-m-d');
		$locationId = 0;
		if(isset($jsondata->locationId) && $jsondata->locationId != ''){
			$locationId = $jsondata->locationId;
		}
		//get live offerid for this location not for brand num 104 (104 is shouut brand)
		$offer_live = $this->DB2->query("select  wl.geo_address,wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage,
			b.original_logo from wifi_offer as wo  INNER JOIN wifi_offer_location as wol ON
 (wo.id = wol.wifi_offer_id) INNER JOIN brand as b ON (wo.brand_id= b.brand_id)
 INNER JOIN wifi_location as wl ON (wol.location_id = wl.id) WHERE wo.status = 1 AND wo.is_draft = 0 AND wo.is_deleted= 0 AND now()
		 BETWEEN wo.start_date AND wo.end_date AND wol.location_id = '$locationId'  AND (wo.no_of_session - wo.session_used >0)
 AND wo.brand_id != '145'");
		if($offer_live->num_rows() > 0) {//if offer find
			$data['resultCode'] = '1';
			$data['resultMessage'] = "Success";
			$i = 0;
			foreach($offer_live->result() as $offers){
				$promotion_type = 'image';
				if($offers->promotion_type == 2){
					$promotion_type = "video";
				}
				$data['offers'][$i]['offer_place'] = $offers->geo_address;
				$data['offers'][$i]['rewradtype'] = $promotion_type;
				$data['offers'][$i]['withoffer'] = $offers->including_offer;
				$data['offers'][$i]['brand_name'] = $offers->brand_name;
				$deal_small_path = '';
				//offer image
				if($offers->image_small != ''){
					$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
				}
				$data['offers'][$i]['offers_small_image'] = $deal_small_path;
				$deal_original_path = '';
				if($offers->image_original != ''){
					$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
				}
				$data['offers'][$i]['offers_original_image'] = $deal_original_path;

				//brand image
				$logo_path = '';
				if($offers->original_logo_logoimage != ''){
					$logo_path = ISP_IMAGEPATH."logo/".$offers->original_logo_logoimage;
				}
				$original_path = '';
				if($offers->original_logo != ''){
					$original_path = ISP_IMAGEPATH."logo/".$offers->original_logo;
				}
				$data['offers'][$i]['brand_logo_image'] = $logo_path;
				$data['offers'][$i]['brand_original_image'] = $original_path;
				$data['offers'][$i]['validTill'] = $offers->end_date;
				$time = strtotime($offers->end_date);
				$format_month = date('D, j M Y g:i A', $time);
				$final = $format_month;
				$data['offers'][$i]['validTill_fromTime'] = $final;
				$data['offers'][$i]['promotion_name'] = $offers->promotion_name;
				$data['offers'][$i]['promotion_desc'] = $offers->promotion_description;
				$data['offers'][$i]['video_url'] = $offers->video_path;
				$data['offers'][$i]['offer_id'] = $offers->id;
				$i++;
				//captive portal analytics
				$macid = '0';
				if(isset($jsondata->macid)){
					$macid = $jsondata->macid;
				}
				$os_info = '';
				if(isset($jsondata->os_info)){
					$os_info = $jsondata->os_info;
				}
				$today_date = date('Y-m-d');
				//check macid entry already exist or not
				$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationId' AND offer_id = '$offers->id' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
				if($check_id->num_rows() > 0){
					$row = $check_id->row_array();
					$temp_id = $row['cpwoid'];
					$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					impression_counter = (impression_counter+1) WHERE cpwoid = '$temp_id'");
				}
				else{
					$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, impression_counter, os_info) VALUES ('$locationId',
				'$offers->id', now(), '$macid', '1', '$os_info')");
				}
			}
			//total impression table
			/*$check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
				location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
			if($check_impression->num_rows() > 0){
				$row = $check_impression->row_array();
				$temp_id = $row['id'];
				$update_cp = $this->DB2->query("update channel_captiveportal_impression set
					total_impression = (total_impression+1) WHERE id = '$temp_id'");
			}
			else{
				$insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
				(location_id, total_impression, visit_date) VALUES ('$locationId',
				'1', now())");
			}*/
		}
		else{
			//shouut brand offer show
//get live offerid for this location not for brand num 104 (104 is shouut brand)
			$offer_live = $this->DB2->query("select  wl.geo_address,wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage,
			b.original_logo from wifi_offer as wo  INNER JOIN wifi_offer_location as wol ON
 (wo.id = wol.wifi_offer_id) INNER JOIN wifi_location as wl ON (wol.location_id = wl.id)
 INNER JOIN brand as b ON (wo.brand_id= b.brand_id) WHERE wo.status = 1 AND wo.is_draft = 0 AND wo.is_deleted= 0 AND now() BETWEEN
 wo.start_date AND wo.end_date   AND (wo.no_of_session - wo.session_used >0)
 AND wo.brand_id = '145' GROUP BY wo.id");
			if($offer_live->num_rows() > 0) {//if offer find
				$data['resultCode'] = '1';
				$data['resultMessage'] = "Success";
				$i = 0;
				foreach($offer_live->result() as $offers){
					$promotion_type = 'image';
					if($offers->promotion_type == 2){
						$promotion_type = "video";
					}
					$data['offers'][$i]['offer_place'] = $offers->geo_address;
					$data['offers'][$i]['rewradtype'] = $promotion_type;
					$data['offers'][$i]['withoffer'] = $offers->including_offer;
					$data['offers'][$i]['brand_name'] = $offers->brand_name;
					$deal_small_path = '';
					//offer image
					if($offers->image_small != ''){
						$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
					}
					$data['offers'][$i]['offers_small_image'] = $deal_small_path;
					$deal_original_path = '';
					if($offers->image_original != ''){
						$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
					}
					$data['offers'][$i]['offers_original_image'] = $deal_original_path;

					//brand image
					$logo_path = '';
					if($offers->original_logo_logoimage != ''){
						$logo_path = ISP_IMAGEPATH."logo/".$offers->original_logo_logoimage;
					}
					$original_path = '';
					if($offers->original_logo != ''){
						$original_path = ISP_IMAGEPATH."logo/".$offers->original_logo;
					}
					$data['offers'][$i]['brand_logo_image'] = $logo_path;
					$data['offers'][$i]['brand_original_image'] = $original_path;
					$data['offers'][$i]['validTill'] = $offers->end_date;
					$time = strtotime($offers->end_date);
					$format_month = date('D, j M Y g:i A', $time);
					$final = $format_month;
					$data['offers'][$i]['validTill_fromTime'] = $final;
					$data['offers'][$i]['promotion_name'] = $offers->promotion_name;
					$data['offers'][$i]['promotion_desc'] = $offers->promotion_description;
					$data['offers'][$i]['video_url'] = $offers->video_path;
					$data['offers'][$i]['offer_id'] = $offers->id;
					$i++;
					//captive portal analytics
					$macid = '0';
					if(isset($jsondata->macid)){
						$macid = $jsondata->macid;
					}
					$os_info = '';
					if(isset($jsondata->os_info)){
						$os_info = $jsondata->os_info;
					}
					$today_date = date('Y-m-d');
					//check macid entry already exist or not
					$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationId' AND offer_id = '$offers->id' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
					if($check_id->num_rows() > 0){
						$row = $check_id->row_array();
						$temp_id = $row['cpwoid'];
						$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					impression_counter = (impression_counter+1) WHERE cpwoid = '$temp_id'");
					}
					else{
						$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, impression_counter, os_info) VALUES ('$locationId',
				'$offers->id', now(), '$macid', '1', '$os_info')");
					}
				}
				//total impression table
				/*$check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
				location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
				if($check_impression->num_rows() > 0){
					$row = $check_impression->row_array();
					$temp_id = $row['id'];
					$update_cp = $this->DB2->query("update channel_captiveportal_impression set
					total_impression = (total_impression+1) WHERE id = '$temp_id'");
				}
				else{
					$insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
				(location_id, total_impression, visit_date) VALUES ('$locationId',
				'1', now())");
				}*/
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMessage'] = "No offer";
			}
		}
		return $data;
	}
	
	public function wifi_user_otp($jsondata){
		$data = array();
		$macid = '';
		if(isset($jsondata->macid)){
		$macid = $jsondata->macid;
		}
		$mobile = '';
		if(isset($jsondata->mobile)){
		$mobile = $jsondata->mobile;
		}
		$otp = '';
		if(isset($jsondata->otp)){
		$otp = $jsondata->otp;
		}
		$locationid = '';
		if(isset($jsondata->locationid)){
		$locationid = $jsondata->locationid;
		}
		$tabledata=array("location_id"=>$locationid,"macid"=>$macid,"mobile"=>$mobile,"otp"=>$otp,"is_verified"=>"0","added_on"=>date("Y-m-d H:i:s"));
		$this->DB2->insert("channel_wifi_user_otp",$tabledata);
		
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}
	
	public function wifi_user_otp_verify($jsondata){
		$data = array();
		$mobile = '';
		if(isset($jsondata->mobile)){
		$mobile = $jsondata->mobile;	
		}
		$otp = '';
		if(isset($jsondata->otp)){
		$otp = $jsondata->otp;
		}
		$tabledata=array("is_verified"=>"1");
		$this->DB2->order_by("id","desc");
		$this->DB2->limit(1, 0);
		$this->DB2->update("channel_wifi_user_otp",$tabledata,array("mobile"=>$mobile,"otp"=>$otp));
		
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}
	
	public function wifi_user_new_registration($jsondata){
		$data = array();
		$mobile = '';
		if(isset($jsondata->mobile)){
		$mobile = $jsondata->mobile;	
		}
		$tabledata=array("new_user"=>"1");
		$this->DB2->order_by("id","desc");
		$this->DB2->limit(1, 0);
		$this->DB2->update("channel_wifi_user_otp",$tabledata,array("mobile"=>$mobile));
		/*$query = $this->DB2->query("update channel_wifi_user_otp set new_user = '1' where mobile = '$mobile'
					 order by id DESC LIMIT 1");*/
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}
	
	public function wifi_user_free_session_add($jsondata){
		$data = array();
		$userid = $jsondata->userId;
//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_user_free_session_add', $userid, $usertype, $jsondata);
		$locationid = '0';
		if(isset($jsondata->locationid)){
			$locationid = $jsondata->locationid;
		}
		$gender = '';
		$platform_filter = '';
		$macid = '';
		$age_group = '';
		$via = '';
		$phone_num = '';
		if(isset($jsondata->macid)){
			$macid = $jsondata->macid;
		}
		$platform_filter = '';
		if(isset($jsondata->platform_filter) && $jsondata->platform_filter!= ''){
			$platform_filter = $jsondata->platform_filter;
		}
		else{
			//get from previous
			$previous = $this->DB2->query("select osinfo,id from wifi_user_free_session where userid = '$jsondata->userId' and osinfo != '' order by id desc limit 1");
			if($previous->num_rows() > 0){
				$row_previous = $previous->row_array();
				$platform_filter = $row_previous['osinfo'];
			}
		}
		$userdata=$this->user_detail($jsondata->userId);
		$gender = (isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
		($userdata[0]['gender'],0,1):'';
		$age_group = (isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
			?$userdata[0]['age_group']:'';
		if(isset($jsondata->via)){
			$via = $jsondata->via;
		}
		$phone_num = (isset($userdata[0]['mobile']) && $userdata[0]['mobile']!='')
			?$userdata[0]['mobile']:'';
		$insert = $this->DB2->query("insert into wifi_user_free_session (userid, session_date, location_id, gender,
		osinfo, macid, age_group, via, phone_num) VALUES
		('$userid', now(), '$locationid', '$gender', '$platform_filter', '$macid', '$age_group', '$via', '$phone_num')");
		
		// send sms in case of imperial blue
		// send sms in case of imperial blue
		    $isp_uid = 0;
		    $get_isp = $this->DB2->query("select isp_uid,success_message from wifi_location where id='$locationid'");
		    if($get_isp->num_rows() > 0){
			      $row_isp = $get_isp->row_array();
			      $isp_uid = $row_isp['isp_uid'];
				  $msg=$row_isp['success_message'];
				  if($msg!=''){
					   $this->send_iands_sms($userid,$msg);
				  }
		    }
		   /* if($isp_uid == 189 || $locationid == '1049'){
			 $this->send_iands_sms($userid);     
		    }*/
		
		$data['resultCode'] = '1';
		$data['resultmessage'] = 'Success';
		return $data;
	}
	  public function send_iands_sms($mobile,$msg){
		    //$msg = "You're connected to Imperial Blue Wi-Fi. Give a missed call on 9815198151 & stand a chance to win exciting prizes!";
		    $postData = array(
			 'authkey' => '106103ADQeqKxOvbT856d19deb',
			 'mobiles' => $mobile,
			 'message' => $msg,
			 'sender' => 'SHOUUT',
			 'route' => '4'
		    );
		    //print_r($postData);die;
		    $url="https://control.msg91.com/api/sendhttp.php";
		    $ch = curl_init();
		    curl_setopt_array($ch, array(
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => $postData
		    //,CURLOPT_FOLLOWLOCATION => true
		    ));
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    $output = curl_exec($ch);
		    curl_close($ch);
		    return 1;
	  }
	
	public function api_analytics_log($function_name, $userid, $usertype, $request){
		$platform=(isset($request->via))?$request->via:"";
		$request = json_encode($request);
		
		$tabledata=array("api_name"=>$function_name,"userid"=>$userid,"usertype"=>$usertype,"created_on"=>date("Y-m-d H:i:s"),"request"=>$request,"platform"=>$platform);
	$inserted=$this->DB2->insert('api_analytics_log',$tabledata);
	}
	
		public function user_free_wifi_voucher_code_use($jsondata){
		$data = array();
		$voucher_id = $jsondata->voucherid;
		$query = $this->DB2->query("update brand_voucher_code_list set is_used = '1', used_on = now() WHERE id = '$voucher_id'");
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}
	
	public function user_free_wifi_voucher_codes($jsondata){
		$data = array();
		$phone = $jsondata->phone;
		$query = $this->DB2->query("select bvcl.id as voucher_id, bvcl.redeemed_on, b.brand_name,b.original_logo_logoimage, b.original_logo, bvc.duration
 from brand_voucher_code_list as bvcl INNER JOIN brand_voucher_code as bvc ON (bvcl.brand_voucher_code_id = bvc.id)
 INNER JOIN brand as b ON (bvc.brand_id = b.brand_id) WHERE
		bvcl.redeem_by ='$phone' AND bvcl.is_used = '0' AND now() BETWEEN bvc.valid_from AND bvc.valid_till");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = "Success";
			$i = 0;
			foreach($query->result() as $codes){
				$data['vouchers'][$i]['voucher_id'] = $codes->voucher_id;
				$duration_unite = $codes->duration;
				if($duration_unite > 12){
					$duration_unite = "mins";
				}
				else{
					$duration_unite = "hours";
				}
				$data['vouchers'][$i]['voucher_duration'] = $codes->duration;
				$data['vouchers'][$i]['voucher_duration_unite'] = $duration_unite;
				$redeemed_on = date_create($codes->redeemed_on);
				//$redeemed_on =  date_format($redeemed_on, 'jS F, G:i');;
				$redeemed_on =  date_format($redeemed_on, 'd M, G:i');;
				$data['vouchers'][$i]['redeemed_on'] = $redeemed_on;
				$data['vouchers'][$i]['brand_name'] = $codes->brand_name;
				//brand image
				$logo_path = '';
				if($codes->original_logo_logoimage != ''){
					$logo_path = ISP_IMAGEPATH."logo/".$codes->original_logo_logoimage;
				}
				$original_path = '';
				if($codes->original_logo != ''){
					$original_path = ISP_IMAGEPATH."logo/".$codes->original_logo;
				}
				$data['vouchers'][$i]['brand_logo_image'] = $logo_path;
				$data['vouchers'][$i]['brand_original_image'] = $original_path;
				$i++;
			}
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = "No voucher activated";
		}
		return $data;
	}
	
	public function check_user_last_seen_wifioffer($jsondata){
		$data = array();
		$userid = '';
		if(isset($jsondata->userid)){
			$userid = $jsondata->userid;
		}
		$query = $this->DB2->query("select wos.wifioffer_id, wo.brand_id from wifi_offer_sceen as wos inner join
		wifi_offer as wo on (wos.wifioffer_id = wo.id) WHERE wos.userid = '$userid' ORDER BY wos.id desc limit 1");

		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'success';
			foreach($query->result() as $row){
				$data['brandid'] = $row->brand_id;
			}
		}else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}


		return $data;
	}
	
	public function user_free_wifi_voucher_offer($jsondata){
	
		$data = array();
		$voucherid = $jsondata->voucherid;
	
		$locationId = 0;
		if(isset($jsondata->locationId) && $jsondata->locationId != ''){
			$locationId = $jsondata->locationId;
		}
		//query to get attaced offer(wifi offer or promotion) and also get the id of offer
		$get_offer_id = $this->DB2->query("select bvc.duration, bvc.offer_type, bvc.offer_id, bvcl.redeem_by from
		brand_voucher_code_list as bvcl INNER JOIN brand_voucher_code as bvc ON (bvcl.brand_voucher_code_id = bvc.id
		) WHERE bvcl.id = '$voucherid' AND bvcl.is_redeem = '1' AND bvcl.is_used = '0'");
		
		if($get_offer_id->num_rows() > 0){
			$row = $get_offer_id->row_array();
			$offer_id = $row['offer_id'];
			$offer_type = $row['offer_type'];
			$phone = $row['redeem_by'];
			$duration = $row['duration'];
			//get userid from mongodb
			/*$this->mongo_db->select(array("_id"));
			$this->mongo_db->where(array("mobile"=>$phone));
			$user_info = $this->mongo_db->get("sh_user");*/
			$query=$this->DB2->query("select uid from sht_users where mobile='".$phone."'");
			$userid = '';
			if($query->num_rows() > 0){
				$rowarr=$query->row_array();
				$userid = $rowarr['uid'];
			}

			if($offer_type == 1){
				// offer type is promotion
				$offer = $this->DB2->query("select  b.brand_id,b.brand_name, b.original_logo, b.original_logo_logoimage, b
				.original_logo_smallimage, ge.event_id, ge
   .offer_short_desc,ge.action,ge.coupon_code,
   ge.offer_long_desc, ge.end_date
   FROM `get_event` ge  INNER JOIN brand b ON (ge.brand_id = b.brand_id) WHERE ge.event_id = '$offer_id'
    GROUP BY event_id");
				if($offer->num_rows() > 0){
					$data['resultCode'] = '1';
					$data['resultMessage'] = "Success";
					$data['offer_type'] = $offer_type;
					$data['phone'] = $phone;
					$i = 0;
					foreach($offer->result() as $offers){
						$i++;
						$duration_unite = $duration;
						if($duration_unite > 12){
							$duration_unite = "mins";
						}
						else{
							$duration_unite = "hours";
						}
						$data['brand_id'] = $offers->brand_id;
						$data['voucher_duration'] = $duration;
						$data['voucher_duration_unite'] = $duration_unite;
						$data['rewradtype'] = "image";
						$including_offer = 0;
						if($offers->action == 'coupon_code'){
							$including_offer = '1';
						}
						$data['coupon_code'] = $offers->coupon_code;
						$data['withoffer'] = $including_offer;
						$data['brand_name'] = $offers->brand_name;
						$deal_small_path = '';
						//offer image
						$get_images = $this->DB2->query("select * from get_event_image WHERE event_id = '$offers->event_id'");
						$image_detail = array();
						foreach($get_images->result() as $get_images1){
							for($k = 0; $k < 4; $k++){
								$event_image_original = '';
								$event_image_logo = '';
								$event_image_small = '';
								$event_image_medium = '';
								$event_image_large = '';
								if($k == 0){
									$get_images_list = $get_images1->image_name1;
									$myArray = explode(',', $get_images_list);
									if(isset($myArray[0]) && $myArray[0] != ''){
										$event_image_original = WIFI_IMAGEPATH.'cards/original/'.$myArray[0];
									}if(isset($myArray[2]) && $myArray[2] != ''){
										$event_image_small = WIFI_IMAGEPATH.'cards/small/'.$myArray[2];
									}
									$data['offers_original_image'] = $event_image_original;
									$data['offers_small_image'] = $event_image_small;

								}

							}

						}
						//brand image
						$logo_path = '';
						if($offers->original_logo_logoimage != ''){
							$logo_path = ISP_IMAGEPATH."logo/".$offers->original_logo_logoimage;
						}
						$original_path = '';
						if($offers->original_logo != ''){
							$original_path = ISP_IMAGEPATH."logo/".$offers->original_logo;
						}
						$data['brand_logo_image'] = $logo_path;
						$data['brand_original_image'] = $original_path;
						$data['validTill'] = $offers->end_date;

						$time = strtotime($offers->end_date);
						$format_month = date('D, j M Y g:i A', $time);
						$final = $format_month;
						$data['validTill_fromTime'] = $final;
						$data['promotion_name'] = $offers->offer_short_desc;
						$data['promotion_desc'] = $offers->offer_long_desc;
						$data['video_url'] = '';
						$data['offer_id'] = $offers->event_id;
						//insert last sceen offer id

						if(isset($jsondata->via)){
							$via = $jsondata->via;
							$insert_daily_log = $this->DB2->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$offers->brand_id', '$via', '$userid', now())");
							$insert_offer_daily_log = $this->DB2->query("insert into channel_offer_daily_analysis_log(offer_id, visit_via,
		 action_perform, visit_date) VALUES ('$offers->event_id','$via', '', now())");
						}


					}
				}
				else{
					$data['resultCode'] = '0';
					$data['resultMessage'] = "No offer found";
				}
			}
			else{
				//offer type is wifi offer
				$offer = $this->DB2->query("select wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage, b.original_logo from wifi_offer as wo INNER JOIN brand as b ON (wo.brand_id
		 = b.brand_id) INNER JOIN wifi_offer_location as wol ON (wo.id = wol.wifi_offer_id) WHERE  wo.id =
		 '$offer_id' GROUP BY wo.id");
				if($offer->num_rows() > 0){
					$data['resultCode'] = '1';
					$data['resultMessage'] = "Success";
					$data['offer_type'] = $offer_type;
					$data['phone'] = $phone;
					$i = 0;
					foreach($offer->result() as $offers){
						$i++;
						$duration_unite = $duration;
						if($duration_unite > 12){
							$duration_unite = "mins";
						}
						else{
							$duration_unite = "hours";
						}
						$data['brand_id'] = $offers->brand_id;
						$data['voucher_duration'] = $duration;
						$data['voucher_duration_unite'] = $duration_unite;
						$promotion_type = 'image';
						if($offers->promotion_type == 2){
							$promotion_type = "video";
						}
						$data['coupon_code'] = '';
						$data['rewradtype'] = $promotion_type;
						$data['withoffer'] = $offers->including_offer;
						$data['brand_name'] = $offers->brand_name;
						$deal_small_path = '';
						//offer image
						if($offers->image_small != ''){
							$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
						}
						$data['offers_small_image'] = $deal_small_path;
						$deal_original_path = '';
						if($offers->image_original != ''){
							$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
						}
						$data['offers_original_image'] = $deal_original_path;

						//brand image
						$logo_path = '';
						if($offers->original_logo_logoimage != ''){
							$logo_path = ISP_IMAGEPATH."logo/".$offers->original_logo_logoimage;
						}
						$original_path = '';
						if($offers->original_logo != ''){
							$original_path =ISP_IMAGEPATH."logo/".$offers->original_logo;
						}
						$data['brand_logo_image'] = $logo_path;
						$data['brand_original_image'] = $original_path;
						$data['validTill'] = $offers->end_date;

						$time = strtotime($offers->end_date);
						$format_month = date('D, j M Y g:i A', $time);
						$final = $format_month;
						$data['validTill_fromTime'] = $final;
						$data['promotion_name'] = $offers->promotion_name;
						$data['promotion_desc'] = $offers->promotion_description;
						$data['video_url'] = $offers->video_path;
						$data['offer_id'] = $offers->id;
						//insert last sceen offer id

						/*$insert = $this->db->query("insert into wifi_offer_sceen (userid, wifioffer_id,locationid,
						sceen_on)
				VALUES ('$userid', '$offers->id', '$locationId', now())");
						$update = $this->db->query("update wifi_offer set session_used = (session_used+1) WHERE id =
						'$offers->id'");*/
					}
					/*if(isset($jsondata->via) && $jsondata->via != ''){
						$brand_id = $offers->brand_id;
						$via = $jsondata->via;
						$user_id = $userid;
						//analytics code end
						$insert_daily_log = $this->db->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$brand_id', '$via', '$user_id', now())");
						$insert_wifioffer_daily_log = $this->db->query("insert into channel_wifioffer_daily_analysis_log
					(offer_id, visit_via, action_perform, visit_date) VALUES('$offers->id','$via', '', now())");
					}*/
					//captive portal analytics
					$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, action_type, visit_date) VALUES ('$locationId', '$offers->id',
				'click', now())");
				}
				else{
					$data['resultCode'] = '0';
					$data['resultMessage'] = "No offer found";
				}
			}
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'Invalid voucher id';
		}
		return $data;
	}
	
	public function get_locationid_mac($jsondata)
	{
		$data=array();
			/*$_POST['requestData'] = '{"userid": "56979e3c01fcff5c4d8b4567"}';*/
			$macid=$jsondata->macid;
			if($macid=="")
			{
				
				$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
			}
			else{
		
			$this->DB2->select("location_uid");
			$this->DB2->from("wifi_location_access_point");
			$this->DB2->where(array("macid"=>$macid));
			$query=$this->DB2->get();
			if($query->num_rows()>0)
			{
				$rowarr=$query->row_array();
				$locuid=$rowarr['location_uid'];
				$this->DB2->select("location_uid,id,isp_uid");
				$this->DB2->from("wifi_location");
				$this->DB2->where(array("location_uid"=>$locuid));
				$query1=$this->DB2->get();
				if($query1->num_rows()>0)
				{
					$rowarr1=$query1->row_array();
					$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$data['locid']=$rowarr1['id'];
			$data['isp_uid']=$rowarr1['isp_uid'];
			$data['location_uid']=$rowarr1['location_uid'];
				}
				else{
					$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
				}
				
			}
			else{
				$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
			}
			}
		return $data;
	}
	
	public function isp_payment_gateways($jsondata)
	{
		$isp_uid=$jsondata->isp_uid;
		$this->db->select("*");
		$this->db->from("sht_merchant_account");
		$this->db->where(array("isp_uid"=>$isp_uid,"status"=>"1"));
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$rowarr=$query->row_array();
			if($rowarr['citrus_secret_key']!='')
			{
				$data['citrus']['citrus_secret_key']=$rowarr['citrus_secret_key'];
				$data['citrus']['citrus_post_url']=$rowarr['citrus_post_url'];
				$data['citrus']['citrus_vanity_url']=$rowarr['citrus_vanity_url'];
			}
			if($rowarr['paytm_merchant_key']!='')
			{
				$data['paytm']['paytm_merchant_key']=$rowarr['paytm_merchant_key'];
				$data['paytm']['paytm_merchant_mid']=$rowarr['paytm_merchant_mid'];
				$data['paytm']['paytm_merchant_web']=$rowarr['paytm_merchant_web'];
			}
			if($rowarr['paytm_merchant_key']!='')
			{
				$data['payu']['payu_user']=$rowarr['payu_user'];
				$data['payu']['payu_pwd']=$rowarr['payu_pwd'];
				$data['payu']['payu_merchantid']=$rowarr['payu_merchantid'];
				$data['payu']['payu_merchantkey']=$rowarr['payu_merchantkey'];
				$data['payu']['payu_merchantsalt']=$rowarr['payu_merchantsalt'];
				$data['payu']['payu_authheader']=$rowarr['payu_authheader'];
			}
			
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}
	
	public function isp_sms_gateways($jsondata)
	{
		$isp_uid=$jsondata->isp_uid;
		$this->db->select("*");
		$this->db->from("sht_sms_gateway");
		$this->db->where(array("isp_uid"=>$isp_uid,"status"=>1));
		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$rowarr=$query->row_array();
			if($rowarr['gupshup_user']!='')
			{
				$data['gupshup']['gupshup_user']=$rowarr['gupshup_user'];
				$data['gupshup']['gupshup_pwd']=$rowarr['gupshup_pwd'];
				
			}
			
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}
	
	public function isp_email_gateways($jsondata)
	{
		$isp_uid=$jsondata->isp_uid;
		
		$this->db->select("*");
		$this->db->from("sht_email_setup");
		$this->db->where(array("isp_uid"=>$isp_uid,"status"=>1));
		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$rowarr=$query->row_array();
			if($rowarr['host']!='')
			{
				$data['email']['host']=$rowarr['host'];
				$data['email']['port']=$rowarr['port'];
				$data['email']['user']=$rowarr['user'];
				$data['email']['password']=$rowarr['password'];
				
			}
			
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}
	
	
	public function get_htlocation_logo($jsondata)
	{
		
		$data=array();
	$this->DB2->select("wl.location_name,wl.location_display_name,wlc.original_image,wlc.logo_image,wlc.is_white_theme ");
	$this->DB2->from("wifi_location wl");
	$this->DB2->where(array("wl.id"=>$jsondata->locationId));
	$this->DB2->join('wifi_location_cptype as wlc', 'wl.id=wlc.location_id', 'INNER');
	$query=$this->DB2->get();
	$brand_logo='';
	if($query->num_rows()>0)
	{
		
		$this->db->select("logo_image,isp_name");
		$this->db->from("sht_isp_detail");
		$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
		$query1=$this->db->get();
		
		$row = $query1->row_array();
		
		$rowarr=$query->row_array();
		 if($rowarr['logo_image'] != ''){
            $brand_logo = CDNPATH.'isp/isp_location_logo/logo/'.$rowarr['logo_image'];
            }elseif($rowarr['original_image'] != ''){
            $brand_logo  = CDNPATH.'isp/isp_location_logo/original/'.$rowarr['original_image'];
            }
			if($brand_logo=='')
			{
				
				if($query->num_rows() > 0){
					
					$brand_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
					
				}
			}
			//echo "<pre>"; print_R($row); die;
			$data['resultCode']=1;
			$data['resultMessage']="Success";
			$data['location_name']=($rowarr['location_display_name']!='')?$rowarr['location_display_name']:$rowarr['location_name'];
			$data['logo']=$brand_logo;
			$data['powered_by']=$row['isp_name'];
			$data['is_white_theme']=$rowarr['is_white_theme'];
			//echo "<pre>"; print_R($data); die;
			
	}
	else{
		$data['resultCode']=0;
			$data['resultMessage']="No record Found";
			
	}
		return $data;
	}
	
	
	
	public function get_location_frommac($jsondata)
	{
		if($jsondata->macid=="")
		{
				$data['resultCode']=0;
			$data['resultMessage']="No record Found";
		}
		else{
		$this->DB2->select("wl.id,wl.isp_uid ");
		$this->DB2->from("wifi_location_access_point wls");
		$this->DB2->where(array("wls.macid"=>$jsondata->macid));
		$this->DB2->where('wls.macid is not NULL', NULL, FALSE);
		$this->DB2->join('wifi_location as wl', 'wls.location_uid=wl.location_uid', 'INNER');
		$query=$this->DB2->get();
		
		
		//echo $this->DB2->last_query(); die;
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['resultMessage']="Success";
			$data['isp_uid']=$rowarr['isp_uid'];
			$data['location_id']=$rowarr['id'];
			
		}else{
				$this->DB2->select("isp_uid,location_id");
				$this->DB2->from("wifi_location_cptype");
				$this->DB2->where(array("voda_uniqueid"=>$jsondata->macid));
				$query1=$this->DB2->get();
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					$data['resultCode']=1;
					$data['resultMessage']="Success";
					$data['isp_uid']=$rowarr['isp_uid'];
					$data['location_id']=$rowarr['location_id'];
					
				}else{
					$data['resultCode']=0;
					$data['resultMessage']="No record Found";
				}
		}
		}
		
		
		
		return $data;
	}
	
	public function get_location_type($jsondata)
	{
		
		$this->DB2->select("entrprise_usertype");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['enterprisetype']=$rowarr['entrprise_usertype'];
			
		}else{
				$data['resultCode']=0;
			$data['resultMessage']="No record Found";
		}
		
		return $data;
		
	}
	
	public function guest_setting($jsondata)
	{
		if($jsondata->type=="hotel")
		{
			$tablename="ht_guest_setting";
		}
		else if($jsondata->type=="hospital")
		{
			$tablename="hspt_guest_setting";
		}
		else if($jsondata->type=="office")
		{
			$tablename="entr_guest_setting";
		}
		
	
		$this->DB2->select("plan_hour,plan_id");
		$this->DB2->from($tablename);
		$this->DB2->where(array("location_id"=>$jsondata->locationId,"is_enabled"=>"1"));
		$query=$this->DB2->get();
		
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['time']=$rowarr['plan_hour']*60*60;
			$data['plan_id']=$rowarr['plan_id'];
		}
		else
		{
			$data['resultCode']=0;
			$data['resultMessage']="No record Found";
		}
		return $data;
		
		
	}
	
		public function wifi_maruti_suzuki_campaign_analytics($jsondata){
		$data = array();
		$userid = $jsondata->userId;
//api analytics code
		$usertype = 'Register';
		$locationid = '0';
		if(isset($jsondata->locationid)){
			$locationid = $jsondata->locationid;
		}
		$gender = '';
		$platform_filter = '';
		$macid = '';
		$age_group = '';
		
		$phone_num = '';
		if(isset($jsondata->macid)){
			$macid = $jsondata->macid;
		}
		$platform_filter = '';
		if(isset($jsondata->platform_filter) && $jsondata->platform_filter!= ''){
			$platform_filter = $jsondata->platform_filter;
		}
		else{
			//get from previous
			$previous = $this->DB2->query("select osinfo,id from wifi_user_free_session where userid = '$jsondata->userId' and osinfo != '' order by id desc limit 1");
			if($previous->num_rows() > 0){
				$row_previous = $previous->row_array();
				$platform_filter = $row_previous['osinfo'];
			}
		}
		$userdata=$this->user_detail($jsondata->userId);
		$gender = (isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
		($userdata[0]['gender'],0,1):'';
		$age_group = (isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
			?$userdata[0]['age_group']:'';
		
		$phone_num = (isset($userdata[0]['mobile']) && $userdata[0]['mobile']!='')
			?$userdata[0]['mobile']:'';
		$insert = $this->DB2->query("insert into maruti_suzuki_campaign (userid, session_date, location_id, gender,
		osinfo, macid, age_group) VALUES
		('$userid', now(), '$locationid', '$gender', '$platform_filter', '$macid', '$age_group')");
		$data['resultCode'] = '1';
		$data['resultmessage'] = 'Success';
		return $data;
	}
	
	public function cafe_signup_pervisit($jsondata)
	{
		$data=array();
		if($jsondata->locationId=="")
		{
			$data['resultcode']=0;
			$data['resultmsg']='No Location Id registered';
		}
		else{
			
			$this->DB2->select("gender,age_group");
			$this->DB2->from("sht_users");
			$this->DB2->where(array("uid"=>$jsondata->userId));
			$uquery=$this->DB2->get();
			$uarr = $uquery->row_array();
			
			$this->DB2->select("location_uid,location_name");
			$this->DB2->from("wifi_location");
			$this->DB2->where(array("id"=>$jsondata->locationId));
			$query=$this->DB2->get();
			
			$rowarr=$query->row_array();
			$location_uid=$rowarr['location_uid'];
			
			$this->DB2->select("`signup_point`,`pervisit_point`");
			$this->DB2->from("cafe_signup_point");
			$this->DB2->where(array("loc_uid"=>$location_uid));
			$signupq=$this->DB2->get();
			
			$signuppoint=0;
			if($signupq->num_rows()>0)
			{
				$signuparr=$signupq->row_array();
				$signuppoint=$signuparr['signup_point'];
				$pervpoint=$signuparr['pervisit_point'];
			}
			else{
				$signuppoint=200;
				$pervpoint=100;
			}
			
			$signupq=$this->DB2->query("select id from cafe_earned_point where uid='".$jsondata->userId."' and loc_uid='".$location_uid."'");
			$this->DB2->select("id");
			$this->DB2->from("cafe_earned_point");
			$this->DB2->where(array("uid"=>$jsondata->userId,"loc_uid"=>$location_uid));
			$signupq=$this->DB2->get();
			if($signupq->num_rows()>0)
			{
				$curdata=date("Y-m-d");
				$pervisitq=$this->DB2->query("select id from cafe_earned_point where uid='".$jsondata->userId."' and loc_uid='".$location_uid."' and date(added_on)=CURDATE()");
				if($pervisitq->num_rows()>0)
				{
					$data['resultcode']=2;
				$data['type']='';
			$data['resultmsg']='Already Availed';
				}
				else{
					$tabledata=array("uid"=>$jsondata->userId,"loc_uid"=>$location_uid,"points"=>$pervpoint,
				"point_method"=>'pervisit_point',"added_on"=>date("Y-m-d H:i:s"),"gender"=>$uarr['gender'],"osinfo"=>$jsondata->device);
				$this->DB2->insert('cafe_earned_point',$tabledata);
				$data['resultcode']=1;
				$data['type']='pervisit';
				$data['point']=$pervpoint;
				$data['location_name']=$rowarr['location_name'];;
				$data['resultmsg']='Per Visit Success';
				}
				
			}
			else{
				$tabledata=array("uid"=>$jsondata->userId,"loc_uid"=>$location_uid,"points"=>$signuppoint,
				"point_method"=>'signup_point',"added_on"=>date("Y-m-d H:i:s"),"gender"=>$uarr['gender'],"osinfo"=>$jsondata->device);
				$this->DB2->insert('cafe_earned_point',$tabledata);
				$data['resultcode']=1;
				$data['type']='signup';
				$data['point']=$signuppoint;
				$data['location_name']=$rowarr['location_name'];;
			$data['resultmsg']='Sign Up Success';
			}
			
		}
		return $data;
		
	}
	
	public function user_reward_list($jsondata)
	{
		$data=array();
		if($jsondata->locationId=="")
		{
			$data['resultcode']=0;
			$data['resultmsg']='No Location Id registered';
		}
		else{
			
			$this->DB2->select("location_uid,location_name");
			$this->DB2->from("wifi_location");
			$this->DB2->where(array("id"=>$jsondata->locationId));
			$query=$this->DB2->get();
			$rowarr=$query->row_array();
			$loc_uid=$rowarr['location_uid'];
			
			$pointaccount=0;
			 
			  $this->DB2->select("SUM(points) AS totalpoint");
				$this->DB2->from("cafe_earned_point");
				$this->DB2->where(array("uid"=>$jsondata->userId,"loc_uid"=>$loc_uid));
				$query1=$this->DB2->get();
				
		  if($query1->num_rows()>0)
		  {
			   $rowarr1=$query1->row_array();
			   $totalpoint=$rowarr1['totalpoint'];
			 
			   	$this->DB2->select("SUM(points) AS redeemedpoint");
				$this->DB2->from("cafe_redeemed_point");
				$this->DB2->where(array("uid"=>$jsondata->userId,"loc_uid"=>$loc_uid));
				$redeemedq=$this->DB2->get();
				$rowarr2=$redeemedq->row_array();
			   $pointaccount=$totalpoint-$rowarr2['redeemedpoint'];
			   }
			   
			
			
			
			  $query=$this->DB2->query("select id,`item_name`,`quantity`,`points` from cafe_rewards where status='1' and loc_uid='".$loc_uid."' and is_deleted='0' and curdate() between start_date and end_date order by added_on desc");
			  
	  if($query->num_rows()>0)
	  {
		  $i=0;
		  $redeemlist=array();
		  $i=0;
		 foreach($query->result() as $val)
		 {
			 $redeemlist[$i]['quantity']=$val->quantity;
			 $redeemlist[$i]['item_name']=$val->item_name;
			 $redeemlist[$i]['points']=$val->points;
			 $redeemlist[$i]['rewardid']=$val->id;
		
		 $i++;
		 }
		  $data['resultcode']=1;
	  $data['user_balance']=$pointaccount;
	  $data['userid']=$jsondata->userId;
	  $data['redeemlist']=$redeemlist;
	  }
	  else
	  {
	  $data['resultcode']=1;
	  $data['user_balance']=$pointaccount;
	  $data['userid']=$jsondata->userId;
	  $data['redeemlist']=array();
	  }
			}
		return $data;
		
	}
	
	public function voucher_redeem_generate($jsondata)
	{
		$data=array();
		if($jsondata->locationId=="")
		{
			$data['resultcode']=0;
			$data['resultmsg']='No Location Id registered';
		}
		else{
			$query=$this->DB2->query("select location_uid,location_name from wifi_location where id='".$jsondata->locationId."'");
			$rowarr=$query->row_array();
			$loc_uid=$rowarr['location_uid'];
			
			$vcode=$this->randon_voucher_cafe();
			$query1=$this->DB2->query("select email,mobile,firstname,lastname from sht_users where uid='".$jsondata->userId."'");
			$rowdata=$query1->row_array();
	
			
		
		//	$this->send_via_sms($jsondata->userId,$vcode);
			foreach($jsondata->rewards as $val)
			{
				$datarr=explode("::",$val);
				$rewardid=$datarr[0];
				$quantity=$datarr[1];
				$point=$datarr[2];
				$queryn=$this->DB2->query("select id from cafe_redeemed_point where uid='".$jsondata->userId."'
				and loc_uid='".$loc_uid."' and is_redeem='0' and reward_id='".$rewardid."'");
				
				
				$tabledata=array("uid"=>$jsondata->userId,"loc_uid"=>$loc_uid,"reward_id"=>$rewardid
				,"quantity"=>$quantity,"points"=>$point,"is_redeem"=>0,"voucher_id"=>$vcode,"added_on"=>date("Y-m-d H:i:s"));
				if($queryn->num_rows()>0)
				{
					$rowarr1=$queryn->row_array();
					$this->DB2->update('cafe_redeemed_point',$tabledata,array("id"=>$rowarr1['id']));
				}
				else{
					$this->DB2->insert('cafe_redeemed_point',$tabledata);
				}
				
			}
			$data['resultcode']=1;
			$data['resultmsg']='Point Redeemed By user';
			$data['vccode']=$vcode;
			$data['email']=$rowdata['email'];
			$data['uid']=$jsondata->userId;
			
			
		
			
		}
		return $data;
		
	}
	
	 public function randon_voucher_cafe(){
		 $this->load->helper('string');
		$random_number = strtoupper(random_string('alnum', 8));
		$query = $this->DB2->query("select voucher_id from cafe_redeemed_point WHERE voucher_id = '$random_number'");
		if($query->num_rows() > 0){
			$this->randon_voucher_cafe();
		}else{
			return $random_number;
		}
	}
	
	public function cafe_pin($jsondata)
	{
		$data=array();
		
		$this->DB2->select("location_uid,location_name");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
		$rowarr=$query->row_array();
		$loc_uid=$rowarr['location_uid'];
		$curdate=date("Y-m-d");
		
		$this->DB2->select("pin");
		$this->DB2->from("cafe_pin");
		$this->DB2->where(array("loc_uid"=>$loc_uid,"date(pindate)"=>$curdate));
		$query=$this->DB2->get();
		
		//echo $this->DB2->last_query(); die;
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['pin']=$rowarr['pin'];
			
		}
		else{
			$data['pin']='';
		}
		return $data;
	}
	
	public function is_otp_disabled($jsondata)
	{
		$data=array();
	
		
		$this->DB2->select("is_otpdisabled");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			
			$data['is_otpdisabled']=$rowarr['is_otpdisabled'];
			
		}
		else{
			$data['is_otpdisabled']=1;
		}
		return $data;
	}
	
	public function get_country_code($jsondata)
	{
		$data=array();
		$this->db->select("sc.phonecode");
		$this->db->from("sht_countries sc");
		$this->db->where(array("sia.isp_uid"=>$jsondata->isp_uid));
		$this->db->join('sht_isp_admin as sia', 'sia.country_id=sc.id', 'INNER');
		$query=$this->db->get();
		
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['phone_code']=$rowarr['phonecode'];
		}
		else{
			$data['phone_code']=0;
		}
	return $data;
		
	}
	
	public function is_voucher_available($jsondata)
	{
		$data=array();
		
		$this->DB2->select("is_voucher_used,location_uid");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query=$this->DB2->get();
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			if($rowarr['is_voucher_used']==1)
			{
				
				$this->DB2->select("*");
				$this->DB2->from("wifi_location_voucher");
				$this->DB2->where(array("location_uid"=>$rowarr['location_uid']));
				$query1=$this->DB2->get();
				
				if($query1->num_rows()>0)
				{
					$data['buydata']=1;
				}
				else{
					$data['buydata']=0;
				}
				
			}
			else{
				$data['buydata']=0;
			}
		}
		else{
			$data['buydata']=0;
		}
	return $data;
		
	}
	
	public function otp_gateway_type($jsondata)
	{
		$data=array();
		if($jsondata->isp_uid=="111")
		{
			$data['gateway_type']='cloud';
			$this->db->select("cloudplace_user,cloudplace_pwd,scope");
			$this->db->from("sht_sms_gateway");
			$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
			$query=$this->db->get();
			
	
			$rowarr=$query->row_array();
			$data['cloudplace_user']=$rowarr['cloudplace_user'];
			$data['cloudplace_pwd']=$rowarr['cloudplace_pwd'];
			$data['scope']=$rowarr['scope'];
			$data['poweredby']='Vnetworks';
		}
		else{
			$data['gateway_type']='msg91';
		}
		return $data;
	}
	
	/*public function onehop_traffic_analytics($jsondata){
		$data=array();
		//echo "<pre>"; print_R($jsondata);// die;
		if($jsondata->Service=="ClientDetails")
		{
			
			//$dataval= $jsondata->Data[0];
			//echo "<pre>"; print_R($jsondata->Data[0]);die;
			foreach($jsondata->Data as $row){
			$timestam = date("Y-m-d H:i:s",strtotime($row->Timestamp));
			$tabledata=array("APMAC"=>$row->APMAC,"ClientMAC"=>$row->ClientMAC,"Timestamp"=>$timestam,
			"Band"=>$row->Band,"Channel"=>$row->Channel,"Signal"=>$row->Signal,"Connected"=>$row->Connected,
			"added_on"=>date("Y-m-d H:i:s"));
			$this->DB2->insert('onehop_clientdetail',$tabledata);
			}
			 if ($this->DB2->affected_rows() >= 0)
			 {
				 $data['resultCode']=1;
				 $data['resultMsg']="Success";
			 }
			 else{
				 $data['resultCode']=0;
				 $data['resultMsg']="Fail";
			 }
			
		}
		else if($jsondata->Service=="TrafficLogs")
		{
		    foreach($jsondata->Data as $row){
			      $timestam = date("Y-m-d H:i:s",strtotime($row->Timestamp));
				$tabledata=array("APMAC"=>$row->APMAC,"APPubicIP"=>$row->APPubicIP,"APLANIP"=>$row->APLANIP,
			"ClientMAC"=>$row->ClientMAC,"SourceIP"=>$row->SourceIP,"SourcePort"=>$row->SourcePort,"DestIP"=>$row->DestIP,"DestPort"=>$row->DestPort,"NATSourceIP"=>$row->NATSourceIP,"NATSourcePort"=>$row->NATSourcePort,"Protocol"=>$row->Protocol,"Timestamp"=>$timestam,
			"added_on"=>date("Y-m-d H:i:s"));
			$this->DB2->insert('onehop_trafficlog',$tabledata);
		    }
			 if ($this->DB2->affected_rows() >= 0)
			 {
				 $data['resultCode']=1;
				 $data['resultMsg']="Success";
			 }
			 else{
				 $data['resultCode']=0;
				 $data['resultMsg']="Faissl";
			 }
			
		}
		else{
			$data['resultCode']=0;
				 $data['resultMsg']="Fail";
		}
		return $data;
		
	}*/
	
	  public function onehop_traffic_analytics($jsondata){
		    $data=array();
		    //echo "<pre>"; print_R($jsondata);// die;
		    if($jsondata['Service'] == "ClientDetails"){
			      $jsondata['addedon'] = date('Y-m-d H:i:s');
			      $this->mongo_db->insert('onehop_demologs', $jsondata);
			      $data['resultCode'] = 1;
			      $data['resultMsg'] = "Success";
		    }
		    else{
			      $data['resultCode'] = 0;
			      $data['resultMsg'] = "Fail";
		    }
		    return $data;
		
	  }
	
	public function retail_offer_list($jsondata)
	{
		$data=array();
		$curdate=date("Y-m-d");
		
		$this->DB2->select("id,`offer_name`,`offer_desc`,`offer_value`,`start_date`,`end_date`");
		$this->DB2->from("retail_offer");
		$this->DB2->where(array("DATE(start_date)<="=>$curdate,"DATE(end_date)>="=>$curdate,"location_id"=>$jsondata->locationId,"is_deleted"=>"0"));
		$this->DB2->order_by("id","desc");
		$query=$this->DB2->get();
	
		$this->DB2->select("offer_id");
		$this->DB2->from("retail_offer_reddem");
		$this->DB2->where(array("macid"=>$jsondata->macid));
		$query1=$this->DB2->get();
		$redeemedoffer=array();
		foreach($query1->result() as $val)
		{
			$redeemedoffer[]=array();
		}
		
		if($query->num_rows()>0)
		{
			$data['resultCode']=1;
			$data['resultMsg']="Success";
			$i=0;
			foreach($query->result() as $val)
			{
				if(!in_array($val->id,$redeemedoffer))
				{
				$data['offer'][$i]['id']=$val->id;
				$data['offer'][$i]['offer_name']=$val->offer_name;
				$data['offer'][$i]['offer_desc']=$val->offer_desc;
				$data['offer'][$i]['offer_value']=$val->offer_value;
				$data['offer'][$i]['start_date']=date("d/m/Y",strtotime($val->start_date));
				$data['offer'][$i]['end_date']=date("d/m/Y",strtotime($val->end_date));
				$i++;
				}
			}
		}
		else{
			$data['resultCode']=0;
			$data['resultMsg']="No Offer";
		}
		return $data;
		
	}
	
		public function retail_offer_redeem($jsondata)
	{
		
		$query1=$this->DB2->query("SELECT wl.location_name,wl.isp_uid,wlc.original_image,wlc.logo_image  FROM wifi_location wl
INNER JOIN `wifi_location_cptype` wlc ON (wl.id=wlc.location_id) where wl.id='".$jsondata->locationId."'");
//echo $this->DB2->last_query(); die;
	$brand_logo='';
	$locname='';
	$vcpath=VOUCHERPATH;
	if($query1->num_rows()>0)
	{
		
		$rowarr=$query1->row_array();
		$isp_uid=$rowarr['isp_uid'];
			if($isp_uid==193)
		{
			$vcpath='http://ads360network.in/';
			
		}
		 if($rowarr['logo_image'] != ''){
            $brand_logo = 'http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/logo/'.$rowarr['logo_image'];
            }elseif($rowarr['original_image'] != ''){
            $brand_logo  = 'http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/original/'.$rowarr['original_image'];
            }
			
			
			if($brand_logo=='')
			{
				
				$query = $this->db->query("SELECT `logo_image` FROM `sht_isp_detail` WHERE isp_uid = '$isp_uid'");
				if($query->num_rows() > 0){
				
					$row = $query->row_array();
					$brand_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
				}
			}
			$locname=$rowarr['location_name'] ;
			
	}
		
		
		$data=array();
		//$query=$this->DB->query("select ")
		$vc_code=$this->randon_voucher_retail();
		$query=$this->DB2->query("select id from retail_offer_reddem where offer_id='".$jsondata->offerid."' and customer_id='".$jsondata->uid."' and is_redeem='1'");
		if($query->num_rows()>0)
		{
			$data['resultCode']=0;
			$data['resultMsg']="Offer Already Availed";
		}
		else{
			$quey2=$this->DB2->query("select id,offer_name,offer_value from retail_offer where id='".$jsondata->offerid."'");
			$rowadata1=$quey2->row_array();
			$offer_name=$rowadata1['offer_name'];
			$offer_value=$rowadata1['offer_value'];
			
			
			
			
			$query1=$this->DB2->query("select email,mobile,firstname,lastname from sht_users where uid='".$jsondata->uid."'");
			$rowdata=$query1->row_array();
			$username=$rowdata['firstname']." ".$rowdata['lastname'];
			$msgurl=$vcpath."voucherRedeem/index.php?voucher_code=".$vc_code."&loctype=7";
			$bitlurl=$this->get_bitly_short_url($msgurl);
			$msg = $vc_code.'%0a'.urlencode("Use the voucher code above to
redeem the offer").'%0a'.$bitlurl;
$msgmail='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>SHOUUT</title>
<style type="text/css">
    html,  body {
	margin: 0 !important;
	padding: 0 !important;
	height: 100% !important;
	width: 100% !important;
   }
	body{
	 background-color: #ffffff;
	}
	/* What it does: Stops email clients resizing small text. */
	* {
		-ms-text-size-adjust: 100%;
		-webkit-text-size-adjust: 100%;
	}
</style>
</head>

<body>
	<div style="width:700px; height: auto; margin: 0px auto; padding: 15px">
	  <table width="700" border="0" cellspacing="0" cellpadding="0">
	  <tbody>
		<tr bgcolor="#f1f1f2">
			<td style="text-align: center; color: #808184; font-size: 11px;padding:5px; font-family: \'Open Sans\', sans-serif; font-weight:600; font-style: italic">Unable to view the email? <a href="#" style="color:#25a9e0; text-decoration: none">Click here</a></td>
		</tr>
		<tr>
		  <td>
			  <div style="width:300px; height:100px;margin: 15px auto; background-color:#f2f2f2;">
			    <img src="'.$brand_logo.'" width="100%"/>
			  </div>
		  </td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Hi '.$username.'&#44;</p></td>
		</tr>
		<tr>
		<td>
		  <p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Thank you for requesting a voucher at<strong> '.$locname.'</strong>.</p></td>
		</tr>
		<tr>
		  <td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Voucher Details: <strong>'.$offer_name.' &#40;'.$offer_value.' &#41;</strong>.</p></td>
		</tr>
		  <tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Please click below to view your voucher and redeem it.</p></td>
		</tr>
		<tr>
		  <td>
			  <a href="'.$vcpath.'voucherRedeem/index.php?voucher_code='.$vc_code.'&loctype=7" target="_blank" style="background: #f00f64; padding:10px 20px;color: #ffffff; font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height:25px; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: 400; height:25px; width: 180px; margin: 15px 0px 5px 0px"> 
              VIEW VOUCHER
          </a>
		  </td>
		</tr>
		<tr>
			<td><span style="color:#929497; font-size: 13px; font-style:italic;font-family: \'Open Sans\', sans-serif;font-weight:400;">Please note that the voucher can be redeemed only once.</span></td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Thanks,</p></td>
		</tr>
		<tr>
		  <td style="border-bottom: 1px solid #e6e7e8"><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 10px 0px;">'.$locname.'</p></td>
		</tr>
		  <tr>
		  <td style="text-align: center; color: #808184; font-size: 13px;padding:5px; font-family: \'Open Sans\', sans-serif; font-weight:600; font-style: italic">Any issue with he vouchr? reach us at <a href="#" style="color:#25a9e0; text-decoration: none">help@shouut.com</a></td>
		</tr>
	  </tbody>
	</table>

	</div>
</body>
</html>';
/*$msgmail="Hi ".$rowdata['firstname']." ".$rowdata['lastname']."<br/><br/>".$vc_code."<br/> Use the voucher code above to
redeem the offer";*/
$sub=$locname." ".$offer_name;
			if($jsondata->is_sendvcemail==0)
			{
				//echo "ssssssss";die;
				$this->send_via_sms($rowdata['mobile'],$msg,$rowarr['isp_uid']);
			}
			else if($jsondata->is_sendvcemail==1)
			{
				//echo "ssssxsssssss";die;
				$this->send_via_vcemail($rowdata['email'],$msgmail,$sub);
			}
			else{
				//echo "ssssxsssssssssssffffffffffssss";die;
				$this->send_via_sms($rowdata['mobile'],$msg,$rowarr['isp_uid']);
				//echo $msgmail; die;
				$this->send_via_vcemail($rowdata['email'],$msgmail,$sub);
			}
			
			
			

			
	
		$tabledata=array("offer_id"=>$jsondata->offerid,"customer_id"=>$jsondata->uid
		,"voucher_code"=>$vc_code,"location_id"=>$jsondata->locationId,"added_on"=>date("Y-m-d H:i:s"),"macid"=>$jsondata->macid);
		$this->DB2->insert('retail_offer_reddem',$tabledata);
		if ($this->DB2->affected_rows() > 0)
			 {
				 $data['resultCode']=1;
				 $data['resultMsg']="Success";
				 $data['vc_code']=$vc_code;
			 }
			 else{
				 $data['resultCode']=0;
				 $data['resultMsg']="Sorry Unable to redeem Voucher";
			 }
		
		}
		return $data;
		
		
		
		
	}
	
	public function randon_voucher_retail(){
		 $this->load->helper('string');
		$random_number = strtoupper(random_string('alnum', 8));
		$query = $this->DB2->query("select voucher_code from retail_offer_reddem WHERE voucher_code = '$random_number'");
		if($query->num_rows() > 0){
			$this->randon_voucher_retail();
		}else{
			return $random_number;
		}
	}
	
	public function retail_background_wifisetting($jsondata)
	{
		$data=array();
		
		$this->DB2->select("retail_original_image,	is_wifidisabled");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['background_image']=($rowarr['retail_original_image']!='')?"http://cdn101.shouut.com/shouutmedia/isp/retail_background_image/original/".$rowarr['retail_original_image']:"";
			$data['is_wifidisabled']=$rowarr['is_wifidisabled'];
		}
		else{
			$data['resultCode']=0;
		}
		return $data;
	}
	
	public function get_captivedata_param($jsondata)
	{
		$data=array();
		$this->DB2->select("is_sendvcemail,is_loginemail,is_sociallogin,is_enablepin,colour_code,	terms_of_use,main_ssid,cp1_no_of_daily_session,	slider_background_color,uid_location");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
		if($query->num_rows()>0)
		{
			
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['is_sendvcemail']=$rowarr['is_sendvcemail'];
			$data['is_loginemail']=$rowarr['is_loginemail'];
			$data['is_sociallogin']=$rowarr['is_sociallogin'];
			$data['is_enablepin']=$rowarr['is_enablepin'];
			$data['colour_code']=$rowarr['colour_code'];
			$data['terms_of_use']=$rowarr['terms_of_use'];
			$data['main_ssid']=$rowarr['main_ssid'];
			$data['daily_session']=$rowarr['cp1_no_of_daily_session'];
			$data['slider_background_color']=$rowarr['slider_background_color'];
			$data['uid_location']=$rowarr['uid_location'];
			$data['country_code']=$this->get_countryphone_code($jsondata->isp_uid);
		}
		else{
			$data['resultCode']=0;
		}
		return $data;
		
	}
	
	public function get_countryphone_code($isp_uid){
      $country_code = '91';
      $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
      if($ispcountryQ->num_rows() > 0){
           $crowdata = $ispcountryQ->row();
           $countryid = $crowdata->country_id;
           $countryQ = $this->db->query("SELECT phonecode FROM sht_countries WHERE id='".$countryid."'");
           if($countryQ->num_rows() > 0){
            $rowdata = $countryQ->row();
            $country_code = $rowdata->phonecode;
           }
           
      }
      return $country_code;
   }
   
   public function send_via_vcemail($email,$message,$sub)
	{
		$this->load->library('email');
		 $from = SMTP_USER;
	    $fromname = 'SHOUUT';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => SMTP_USER,
	    'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
		   $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
                $this->email->subject($sub);
                $this->email->message($message);	
                $this->email->send();
               // echo 'Email has been send successfully.';
			   return true;
	}
	 public function get_userid_fromac($jsondata)
	{
		$query=$this->DB2->query("select userid from wifi_user_mac where macid='".$jsondata->macid."' order by logged_in desc limit 1");
		$this->DB2->select("userid");
		$this->DB2->from("wifi_user_mac");
		$this->DB2->where(array("macid"=>$jsondata->macid));
		$this->DB2->order_by("logged_in","desc");
		$this->DB2->limit(1, 0);
		$query=$this->DB2->get();
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['userid']=$rowarr['userid'];
		}
		else{
			$data['userid']='';
		}
		return $data;
	}
	
	public function cafe_rewardlist($jsondata)
	{
		$data=array();
		$this->DB2->select("location_uid,location_name");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$query=$this->DB2->get();
		
			$rowarr=$query->row_array();
			$loc_uid=$rowarr['location_uid'];
			$query1=$this->DB2->query("Select item_name,points from cafe_rewards where loc_uid='".$loc_uid."' and is_deleted='0' and curdate() between start_date and end_date  order by added_on desc limit 2");
			if($query1->num_rows()>0)
			{
				$data['resultCode']=1;
				$i=0;
				foreach($query1->result() as $val)
				{
					$data['reward'][$i]['itemname']=$val->item_name;
					$data['reward'][$i]['points']=$val->points;
					$i++;
				}
					
				
			}
			else{
				$data['resultCode']=1;
			}
			
		return $data;
		
	}
	
	public function send_vc_email_msg($jsondata)
	{
		//vcode generate
		$query=$this->DB2->query("select location_uid,location_name,provider_id from wifi_location where id='".$jsondata->locationId."'");
			$rowarrloc=$query->row_array();
			$loc_uid=$rowarrloc['location_uid'];
			$vcpath=VOUCHERPATH;
			
			
		$vcode=$this->randon_voucher_cafe();
		if($jsondata->isp_uid==193)
		{
			$vcpath='http://ads360network.in/';
		}
		
		$datarr=explode("::",$jsondata->rewardstring);
				$rewardid=$datarr[0];
				$quantity=$datarr[1];
				$point=$datarr[2];
				$queryn=$this->DB2->query("select id from cafe_redeemed_point where uid='".$jsondata->userId."'
				and loc_uid='".$loc_uid."' and is_redeem='0' and reward_id='".$rewardid."'");
				
				
				$tabledata=array("uid"=>$jsondata->userId,"loc_uid"=>$loc_uid,"reward_id"=>$rewardid
				,"quantity"=>$quantity,"points"=>$point,"is_redeem"=>0,"voucher_id"=>$vcode,"added_on"=>date("Y-m-d H:i:s"));
				if($queryn->num_rows()>0)
				{
					$rowarr1=$queryn->row_array();
					$this->DB2->update('cafe_redeemed_point',$tabledata,array("id"=>$rowarr1['id']));
				}
				else{
					$this->DB2->insert('cafe_redeemed_point',$tabledata);
				}
		
		$query1=$this->DB2->query("SELECT wl.location_name,wlc.original_image,wlc.logo_image  FROM wifi_location wl
INNER JOIN `wifi_location_cptype` wlc ON (wl.id=wlc.location_id) where wl.id='".$jsondata->locationId."'");
//echo $this->DB2->last_query(); die;
	$brand_logo='';
	$locname='';
	if($query1->num_rows()>0)
	{
		$rowarr=$query1->row_array();
		 if($rowarr['logo_image'] != ''){
            $brand_logo = 'http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/logo/'.$rowarr['logo_image'];
            }elseif($rowarr['original_image'] != ''){
            $brand_logo  = 'http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/original/'.$rowarr['original_image'];
            }
			
			
			if($brand_logo=='')
			{
				
				$query = $this->db->query("SELECT `logo_image` FROM `sht_isp_detail` WHERE isp_uid = '$jsondata->isp_uid'");
				if($query->num_rows() > 0){
				
					$row = $query->row_array();
					$brand_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
				}
			}
			$locname=$rowarr['location_name'] ;
			
	}
		
		
		
		
			
			
			$query1=$this->DB2->query("select email,mobile,firstname,lastname from sht_users where uid='".$jsondata->userId."'");
			$rowdata=$query1->row_array();
			$username=$rowdata['firstname']." ".$rowdata['lastname'];
			
			$query2=$this->DB2->query("select cr.item_name,cr.points,crp.voucher_id from cafe_rewards cr 
			inner join cafe_redeemed_point crp on (crp.reward_id=cr.id) where crp.voucher_id='".$vcode."'");
			$rowdata2=$query2->row_array();
			$itemname=$rowdata2['item_name'];
			$points=$rowdata2['points'];
			$msgurl=$vcpath."voucherRedeem/index.php?voucher_code=".$vcode."&loctype=6";
			$bitlurl=$this->get_bitly_short_url($msgurl);
			$msg = $vcode.'%0a'.urlencode("Use the voucher code above to
redeem the offer").'%0a'.$bitlurl;
$msgmail='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<style type="text/css">
    html,  body {
	margin: 0 !important;
	padding: 0 !important;
	height: 100% !important;
	width: 100% !important;
   }
	body{
	 background-color: #ffffff;
	}
	/* What it does: Stops email clients resizing small text. */
	* {
		-ms-text-size-adjust: 100%;
		-webkit-text-size-adjust: 100%;
	}
</style>
</head>

<body>
	<div style="width:700px; height: auto; margin: 0px auto; padding: 15px">
	  <table width="700" border="0" cellspacing="0" cellpadding="0">
	  <tbody>
		<tr bgcolor="#f1f1f2">
			<td style="text-align: center; color: #808184; font-size: 11px;padding:5px; font-family: \'Open Sans\', sans-serif; font-weight:600; font-style: italic">Unable to view the email? <a href="#" style="color:#25a9e0; text-decoration: none">Click here</a></td>
		</tr>
		<tr>
		  <td>
			  <div style="width:300px; height:100px;margin: 15px auto; background-color:#f2f2f2;">
			    <img src="'.$brand_logo.'" width="100%"/>
			  </div>
		  </td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Hi '.$username.'&#44;</p></td>
		</tr>
		<tr>
		<td>
		  <p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Thank you for requesting a voucher at<strong> '.$locname.'</strong>.</p></td>
		</tr>
		<tr>
		  <td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Voucher Details: <strong>'.$itemname.' &#40;'.$points.' Points&#41;</strong>.</p></td>
		</tr>
		  <tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Please click below to view your voucher and redeem it.</p></td>
		</tr>
		<tr>
		  <td>
			  <a href="'.$vcpath.'voucherRedeem/index.php?voucher_code='.$vcode.'&loctype=6" target="_blank" style="background: #f00f64; padding:10px 20px;color: #ffffff; font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height:25px; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: 400; height:25px; width: 180px; margin: 15px 0px 5px 0px"> 
              VIEW VOUCHER
          </a>
		  </td>
		</tr>
		<tr>
			<td><span style="color:#929497; font-size: 13px; font-style:italic;font-family: \'Open Sans\', sans-serif;font-weight:400;">Please note that the voucher can be redeemed only once.</span></td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		</tr>
		<tr>
			<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Thanks,</p></td>
		</tr>
		<tr>
		  <td style="border-bottom: 1px solid #e6e7e8"><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 10px 0px;">'.$locname.'</p></td>
		</tr>
		  <tr>
		  <td style="text-align: center; color: #808184; font-size: 13px;padding:5px; font-family: \'Open Sans\', sans-serif; font-weight:600; font-style: italic">Any issue with he vouchr? reach us at <a href="#" style="color:#25a9e0; text-decoration: none">help@shouut.com</a></td>
		</tr>
	  </tbody>
	</table>

	</div>
</body>
</html>';
$sub=$locname." ".$itemname;
if($jsondata->is_sendvcemail==0)
			{
				$this->send_via_sms($rowdata['mobile'],$msg,$jsondata->isp_uid);
			}
			else if($jsondata->is_sendvcemail==1)
			{
				$this->send_via_vcemail($jsondata->email,$msgmail,$sub);
			}
			else{
				$this->send_via_sms($rowdata['mobile'],$msg,$jsondata->isp_uid);
				$this->send_via_vcemail($jsondata->email,$msgmail,$sub);
			}
			 $data['resulcode']=1;
		return $data;
	}
	
	
	public function get_bitly_short_url($url) {
      $login = 'shouut';
      $appkey = 'R_495a520806484396941bd1a37d70228b';
      $query = array(
           "version" => "2.0.1",
           "longUrl" => $url,
           "login" => $login, // replace with your login
           "apiKey" => $appkey // replace with your api key
      );
      $query = http_build_query($query);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, "http://api.bit.ly/shorten?".$query);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($ch);
      curl_close($ch);
      $response = json_decode($response);
      if($response->errorCode == 0 && $response->statusCode == "OK") {
           return $response->results->{$url}->shortUrl;
      }else {
           return '';
      }
    }
	
	
	public function get_defaultlocation_logo($jsondata)
	{
		$loc_logo='';
		
		$this->DB2->select("retail_small_image");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$query1=$this->DB2->get();
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					if($rowarr['retail_small_image']!='')
					{
						$loc_logo=CDNPATH."isp/retail_background_image/small/".$rowarr['retail_small_image'];
						
					}
					else{
						
						$this->db->select("logo_image");
						$this->db->from("sht_isp_detail");
						$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
						$query=$this->db->get();
					if($query->num_rows() > 0){
						$row = $query->row_array();
						$loc_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
					}
					}
					
				}
				else{
						
						$this->db->select("logo_image");
						$this->db->from("sht_isp_detail");
						$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
						$query=$this->db->get();
					if($query->num_rows() > 0){
						$row = $query->row_array();
						$loc_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
					}
					
				}
				return $loc_logo;
		
	}
	
	public function get_customslideshow_logo($jsondata)
	{
		$data=array();
		$imagepath=CDNPATH."isp/retail_background_image/";
		$loc_logo='';
		$loclogo1='';
		
		$this->DB2->select("custom_slider_image1,custom_slider_image2");
		$this->DB2->from("wifi_location_cptype");
		$this->DB2->where(array("location_id"=>$jsondata->locationId));
		$query1=$this->DB2->get();
		
				if($query1->num_rows()>0)
				{
					$rowarr=$query1->row_array();
					if($rowarr['custom_slider_image1']!='')
					{
						$loc_logo=$imagepath."original/".$rowarr['custom_slider_image1'];
						
					}
				
					
					if($rowarr['custom_slider_image2']!='')
					{
						$loclogo1=$imagepath."original/".$rowarr['custom_slider_image2'];
					}
					$data['images'][0]=$loc_logo;
					$data['images'][1]=$loclogo1;
					
					
					
				}
				else{
					$data['images'][0]=$loc_logo;
					$data['images'][1]=$loclogo1;
				}
				
				return $data;
		
	}
	
	
	public function user_dailyvisit_data($jsondata)
	{
		
		$this->DB2->select("gender");
		$this->DB2->from("sht_users");
		$this->DB2->where(array("uid"=>$jsondata->userId));
		$userquery=$this->DB2->get();
		
		$this->DB2->select("location_uid");
		$this->DB2->from("wifi_location");
		$this->DB2->where(array("id"=>$jsondata->locationId));
		$locq=$this->DB2->get();
		
		$loc=0;
		$gender='';
		if($userquery->num_rows()>0)
		{
			$genderarr=$userquery->row_array();
			$locarr=$locq->row_array();
			$gender=$genderarr['gender'];
			$loc=$locarr['location_uid'];
		}
		
		
		$curdate=date("Y-m-d");
		$this->DB2->select("id");
		$this->DB2->from("cp_location_user_daily_visit");
		$this->DB2->where(array("uid"=>$jsondata->userId,"loc_uid"=>$loc,"date(added_on)"=>$curdate));
		$userq=$this->DB2->get();
		
		if($userq->num_rows()==0)
		{
			$tabledata=array("uid"=>$jsondata->userId,"loc_uid"=>$loc,"added_on"=>date("Y-m-d H:i:s"),"gender"=>$gender,"osinfo"=>$jsondata->platform_filter);
			$this->DB2->insert('cp_location_user_daily_visit',$tabledata);
			
		}
	}
	
		public function send_otp($jsondata)
	{
		$data['resultcode']=1;
		$otpno = mt_rand(1000,9999);
		$curdate=date("Y-m-d");
		
		$this->DB2->select("otp");
		$this->DB2->from("wifi_cp2_users");
		$this->DB2->where(array("mobileno"=>$jsondata->mobile,"date(added_on)"=>$curdate,"isp_uid"=>$jsondata->isp_uid));
		$this->DB2->order_by("id","desc");
		$this->DB2->limit(1, 0);
		$query=$this->DB2->get();
		
		if($query->num_rows()>0)
		{
			$otparr=$query->row_array();
			//$otpno=$otparr['otp'];
		}
		$user_mobileno=$jsondata->mobile;
		$user_message="Your Wifi Otp is ".$otpno;
		 
		 $this->db->select("*");
		$this->db->from("sht_sms_gateway");
		$this->db->where(array("isp_uid"=>$jsondata->isp_uid));
		$this->db->order_by("id","desc");
		$this->db->limit(1, 0);
		$txtsmsQ=$this->db->get();
		
        if($txtsmsQ->num_rows() > 0){
			//echo "<pre>"; print_r($txtsmsQ->result()); die;
            foreach($txtsmsQ->result() as $txtsmsobj){
                $gupshup_user = $txtsmsobj->gupshup_user;
                $gupshup_pwd = $txtsmsobj->gupshup_pwd;
                $msg91authkey = $txtsmsobj->msg91authkey;
                $bulksmsuser = $txtsmsobj->bulksmsuser;
                $bulksms_pwd = $txtsmsobj->bulksms_pwd;
                $rightsms_user = $txtsmsobj->rightsms_user;
                $rightsms_pwd = $txtsmsobj->rightsms_pwd;
                $rightsms_sender = $txtsmsobj->rightsms_sender;
                $cloudplace_user = $txtsmsobj->cloudplace_user;
                $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
                $cloudplace_sender = $txtsmsobj->scope;
                $sender = $txtsmsobj->scope;
				$infini_apikey = $txtsmsobj->infini_apikey;
				$infini_senderid = $txtsmsobj->infini_senderid;
				$ssl_user = $txtsmsobj->ssl_user;
				$ssl_pwd = $txtsmsobj->ssl_password;
				$ssl_sid = $txtsmsobj->ssl_sid;
                
                if(($gupshup_user != '') && ($gupshup_pwd != '')){
                  //  $user_message = "Dear customer, Your bill for Internet connection is due. Account id: xxx, Due amount: Rs xxx Kindly pay the bill at www.vayudoot.in";
                    $this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message);
					$data['otp']=$otpno;
        
                }elseif($msg91authkey != ''){
					
                    $this->msg91_txtsmsgateway($msg91authkey, $user_mobileno, $user_message, $sender);
					$data['otp']=$otpno;
        
                }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
                    $this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $user_mobileno, $user_message);
					$data['otp']=$otpno;
        
                }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
                    $this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $user_mobileno, $user_message, $rightsms_sender);
					$data['otp']=$otpno;
        
                }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
                    $this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender);
					$data['otp']=$otpno;
        
                }elseif(($ssl_user != '') && ($ssl_pwd != '') && ($ssl_sid != '')){
			
                    $this->sslbangladesh_txtsmsgateway($ssl_user, $ssl_pwd, $user_mobileno, $user_message, $ssl_sid);
					$data['otp']=$otpno;
        
                }elseif(($infini_apikey != '') && ($infini_senderid != '')){
					if($jsondata->isp_uid==193)
					{
						$user_message="Welcome to Ads360 Wi-Fi, your OTP is ".$otpno;
					}
					
                   /*$infini_apikey = 'A921b98066097485eddeb9388665a8946';
					$infini_senderid = 'SHOUUT';
					$this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
					$krauss_user = 'mankomalshouut';
					$krauss_pwd = 'e3bkopzx';
					$this->krauss_txtsmsgateway($krauss_user, $krauss_pwd, $user_mobileno, $user_message);
			
					$data['otp']=$otpno;
					$senderid='SHOUUT';
					$keyid='45C4ABB19A931A';
					$this->host6sms_gateway($senderid,$keyid, $user_mobileno, $user_message);
					$data['otp']=$otpno;*/
					$infini_apikey = 'A921b98066097485eddeb9388665a8946';
					$infini_senderid = 'SHOUUT';
					$this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
					$data['otp']=$otpno;
        
                }else{
					/*$infini_apikey = 'A921b98066097485eddeb9388665a8946';
					$infini_senderid = 'SHOUUT';
					$this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
					$krauss_user = 'mankomalshouut';
					$krauss_pwd = 'e3bkopzx';
					$this->krauss_txtsmsgateway($krauss_user, $krauss_pwd, $user_mobileno, $user_message);
			
					$data['otp']=$otpno;
					$senderid='SHOUUT';
					$keyid='45C4ABB19A931A';
					$this->host6sms_gateway($senderid,$keyid, $user_mobileno, $user_message);
					$data['otp']=$otpno;*/
					$infini_apikey = 'A921b98066097485eddeb9388665a8946';
					$infini_senderid = 'SHOUUT';
					$this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
					$data['otp']=$otpno;
				}
            }
        }
		else{
			/*$infini_apikey = 'A921b98066097485eddeb9388665a8946';
					$infini_senderid = 'SHOUUT';
					$this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
					$krauss_user = 'mankomalshouut';
					$krauss_pwd = 'e3bkopzx';
					$this->krauss_txtsmsgateway($krauss_user, $krauss_pwd, $user_mobileno, $user_message);
			
					$data['otp']=$otpno;
					$senderid='SHOUUT';
					$keyid='45C4ABB19A931A';
					$this->host6sms_gateway($senderid,$keyid, $user_mobileno, $user_message);
					$data['otp']=$otpno;
					*/
					$infini_apikey = 'A921b98066097485eddeb9388665a8946';
					$infini_senderid = 'SHOUUT';
					$this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
					$data['otp']=$otpno;
		}
		return $data;		

	}
	
	public function host6sms_gateway($senderid,$keyid ,$user_mobileno, $user_message)
	{
		 $param = array();
        $request =""; //initialise the request variable
        $param['key']= $keyid;
        $param['campaign'] = 0;
        $param['routeid'] = 5;
        $param['type'] = 'text';
        $param['contacts'] = $user_mobileno;
        $param['senderid'] = $senderid;
        $param['msg'] = $user_message; //Can be "FLASH”/"UNICODE_TEXT"/”BINARY”
      
        
        //Have to URL encode the values
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://host6.hemsmedia.com/app/smsapi/index.php?".$request;
       // echo $url;
        //die;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        //echo $curl_scraped_page;
        
        return 1;
	}
	
	public function sslbangladesh_txtsmsgateway($ssl_user, $ssl_pwd, $user_mobileno, $user_message, $ssl_sid)
	{
		$user = $ssl_user; 
		$pass = $ssl_pwd; 
		$sid = $ssl_sid; 
		$url="http://sms.sslwireless.com/pushapi/dynamic/server.php";
		$randomcid=mt_rand(1000000,9999999);
		$param="user=$user&pass=$pass&sms[0][0]=$user_mobileno&sms[0][1]=".urlencode("$user_message")."&sms[0][2]=$randomcid&sid=$sid";
		$crl = curl_init();
		curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
		curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
		curl_setopt($crl,CURLOPT_URL,$url); 
		curl_setopt($crl,CURLOPT_HEADER,0);
		curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($crl,CURLOPT_POST,1);
		curl_setopt($crl,CURLOPT_POSTFIELDS,$param); 
		$response = curl_exec($crl);
		curl_close($crl);
		//echo $response;
		
	}
	
	public function krauss_txtsmsgateway($krauss_user, $krauss_pwd, $user_mobileno, $user_message){
        $param = array();
        $request =""; //initialise the request variable
        $param['sender']= "SHOUUT";
        $param['phone'] = $user_mobileno;
        $param['text'] = $user_message;
        $param['user'] = $krauss_user;
        $param['pass'] = $krauss_pwd;
        $param['priority'] = "ndnd";
        $param['stype'] = "normal"; //Can be "FLASH”/"UNICODE_TEXT"/”BINARY”
      
        
        //Have to URL encode the values
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://sms.krauss.co.in/api/sendmsg.php?".$request;
        //echo $url;
        //die;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        //echo $curl_scraped_page;
        
        return 1;
    }
	
	public function gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message){
        $param = array();
        $request =""; //initialise the request variable
        $param['method']= "SendMessage";
        $param['send_to'] = "91".$user_mobileno;
        $param['msg'] = $user_message;
        $param['userid'] = $gupshup_user;
        $param['password'] = $gupshup_pwd;
        $param['v'] = "1.1";
        $param['msg_type'] = "TEXT"; //Can be "FLASH”/"UNICODE_TEXT"/”BINARY”
        $param['auth_scheme'] = "PLAIN";
        $param['format'] = "text";
        
        //Have to URL encode the values
        foreach($param as $key=>$val) {
            $request.= $key."=".urlencode($val);
            $request.= "&";
        }
        
        $request = substr($request, 0, strlen($request)-1);
        //remove final (&) sign from the request
        $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
        //echo $url;
        //die;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        //echo $curl_scraped_page;
        
        return 1;
    }
    
    public function msg91_txtsmsgateway($msg91authkey, $phone, $msg, $sender) {
        $postData = array(
           'authkey' => $msg91authkey, //'106103ADQeqKxOvbT856d19deb',
           'mobiles' => $phone,
           'message' => $msg,
           'sender' => $sender, //'SHOUUT',
           'route' => '4'
        );
		//echo "<pre>"; print_R($postData); die;

        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
           CURLOPT_URL => $url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_POST => true,
           CURLOPT_POSTFIELDS => $postData
           //,CURLOPT_FOLLOWLOCATION => true
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }

    public function bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $phone, $msg){
        $bulksmsurl = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
        $postvalues = "user=".$bulksmsuser."&pwd=".$bulksms_pwd."&senderid=ABC&mobileno=".$phone."&msgtext=".$msg."&smstype=0/4/3";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
    
    public function decibel_txtsmsgateway($mobile, $msg){
        $postData = array(
           'authkey' => '106103ADQeqKxOvbT856d19deb',
           'mobiles' => $mobile,
           'message' => $msg,
           'sender' => 'SHOUUT',
           'route' => '4'
        );
 
        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        var_dump($output); die;
        
        curl_close($ch);
        return 1;
    }
    public function rightchoice_txtsmsgateway($rcuser, $rc_pwd, $phone, $msg, $sender){
        $rcsmsurl = "http://www.sms.rightchoicesms.com/sendsms/groupsms.php?";
        $postvalues = "username=".$rcuser."&password=".$rc_pwd."&type=TEXT&mobile=".$phone."&sender=".$sender."&message=".$msg;
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($rcsmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }

    public function cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender){
        $bulksmsurl = "http://203.212.70.200/smpp/sendsms?";
        $postvalues = "username=".$cloudplace_user."&password=".$cloudplace_pwd."&to=".$user_mobileno."&from=".$cloudplace_sender."&udh=&text=".$user_message."&dlr-mask=19";
        //print_r($bulksmsurl); die;
        
        $ch = curl_init($bulksmsurl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return 1;
    }
	
	public function shouut_default_paytm($phone)
	{
		 $baseUrl = "http://sendotp.msg91.com/api";
		 $data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
	$data_string = json_encode($data);
	$ch = curl_init($baseUrl.'/generateOTP');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',
	    'Content-Length: ' . strlen($data_string),
	    'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
	));
	$result = curl_exec($ch);
	curl_close($ch);
	//echo '<pre>'; print_r($result);
	$response = json_decode($result,true);
	if($response["status"] == "error"){
	   // return $response["response"]["code"];
            return 0;
	}else{
		$this->verifyBySendOtp($phone,$response["response"]["oneTimePassword"]);
	    return $response["response"]["oneTimePassword"];
	}
	}
	
	public function infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message)
	{
		
		 $baseUrl = "https://alerts.solutionsinfini.com/api/v4/?api_key=".$infini_apikey;
		$user_message=urlencode($user_message); 
		  
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl."&method=sms&message=$user_message&to=$user_mobileno&sender=$infini_senderid");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
		
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well

	if($statusCode==200){
	    return 1;
	}
	else{
		return 0;
	}
	curl_close($ch);
	}
	
	public function verifyBySendOtp($phone,$otp){

    $data = array("countryCode" => "91", "mobileNumber" => "$phone", "oneTimePassword" => "$otp");

    $data_string = json_encode($data);

    $ch = curl_init($this->baseUrl . '/verifyOTP');

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_AUTOREFERER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(

      'Content-Type: application/json',

      'Content-Length: ' . strlen($data_string),

      'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='

    ));

    $result = curl_exec($ch);

    curl_close($ch);

    $response = json_decode($result, true);

    if ($response["status"] == "error") {

     //customize this as per your framework

     //$resp['message'] =  $response["response"]["code"];

      return 0;

    }else {

      //$resp['message'] =  "NUMBER VERIFIED SUCCESSFULLY";

      return 1;

    }

   }
   
    public function validate_blacklisted($jsondata)
   {
	   $data=array();
	   if($jsondata->param=="")
	   {
		   $data['resultCode']=1;
		   $data['resultMessage']="Validated";
		   return $data;
	   }
	
	   	$this->DB2->select("id");
		$this->DB2->from("blacklist_user");
		$this->DB2->where(array("block_num"=>$jsondata->param,"loc_id"=>$jsondata->locid,"is_deleted"=>"0"));
		$query=$this->DB2->get();
		
	   if($query->num_rows()>0)
	   {
		     $data['resultCode']=0;
		   $data['resultMessage']="Not Validated";
	   }else{
		    $data['resultCode']=1;
		   $data['resultMessage']="Validated";
	   }
	   
	   return $data;
   }
   
    public function user_registration($jsondata){
	   $data=array();
	   $query=$this->DB2->select("uid")->from("sht_users")->where(array("uid"=>$jsondata->shmobileno))->get();
	   if($query->num_rows()==0){
		  
		   $tabledata=array("isp_uid"=>$jsondata->isp_uid,"uid"=>$jsondata->shmobileno,"username"=>$jsondata->shmobileno,"password"=>$jsondata->passord,
		   "gender"=>$jsondata->gender,"age_group"=>$jsondata->age_group,"enableuser"=>'1',"downlimit"=>'0',"comblimit"=>'0',
		   "firstname"=>'',"lastname"=>'',"mobile"=>$jsondata->shmobileno,"gpslat"=>'0.00000000000000',"gpslong"=>'0.00000000000000',
		   "usermacauth"=>'0',"expiration"=>$jsondata->expiration,"createdon"=>date('Y-m-d'),"acctype"=>'0'
		   ,"credits"=>'0.00',"createdby"=>'admin',"owner"=>'admin',"email"=>$jsondata->uemail);
		   $this->DB2->insert('sht_users',$tabledata);
	   }
	    $data['resultCode']=1;
		$data['resultMessage']="Success";
		return $data;
	   
   }
   
    public function score_api($jsondata){
	    if($jsondata->locid!='' && $jsondata->date!=''){
		
	    $complaincecat=$this->DB2->query("select id,category_name,weight from retailaudit_compliance_category where is_deleted=0");
	   // echo $this->db->last_query(); die;
	    $compliance_catdetarr=array();
	    $compliance_catarr=array();
	    foreach($complaincecat->result() as $val){
		$compliance_catdetarr[$val->id]=$val;
		$compliance_catarr[]=$val->id;
	    }
	    
	    $locid=$jsondata->locid;
	    $compliance_ids = '"'.implode('", "', $compliance_catarr).'"';
	    $locidx='"'.implode('", "', explode(",",$locid)).'"';
	    $surveycomploc=$this->DB2->query("SELECT `loc_id` FROM `retail_audit_status` where loc_id in ({$locidx}) GROUP BY `loc_id`");
	    foreach($surveycomploc->result() as $val){
	       $locidarr[] =$val->loc_id;
	    }
	    $datex=date("Y-m-d",strtotime($jsondata->date));
	    $locids='"'.implode('", "', $locidarr).'"';
	    $locarr=$locidarr;
	   // $locids='"'.implode('", "', explode(",",$locid)).'"';
	    $datequery=$this->DB2->query("SELECT DATE(completed_on) as date,loc_id FROM retail_audit_status  where loc_id IN ({$locids}) and date(completed_on)='".$datex."'");
	    $lastupadtearr=$datequery->row_array();
	  
	    $datearr=array();
	    foreach($datequery->result() as $val2){
	       $datearr[]= $val2->date;
	       $datecond[$val2->loc_id]['locid']=$val2->loc_id;
	       $datecond[$val2->loc_id]['date']=$val2->date;
	    }
	    $datearr[]=date("Y-m-d",strtotime($jsondata->date));
	    $dates='"'.implode('", "', $datearr).'"';
	    $compcatarr=array();
	   $catarr=$this->DB2->query("SELECT omc.id,omc.location_id,rpc.compliance_id FROM offline_main_category omc
	    INNER JOIN retailaudit_is_promoter_category rpc ON (omc.id=rpc.category_id AND rpc.is_deleted=0 AND rpc.is_promoter=0)
	    WHERE  omc.location_id IN ({$locids}) and omc.is_deleted='0'");
	     
	     $i=0;
	     
	    foreach($catarr->result() as $valx){
		$compcatarr[$valx->compliance_id][$valx->location_id][$i]=$valx->id;
		
		$i++;
	    }
	    
	    
	  
	    $deductarr=array();
	    foreach($compcatarr as $keycomp=> $val){
		foreach($val as $keyloc => $valx){
		  $deductarr[$keycomp][$keyloc]= $compliance_catdetarr[$keycomp]->weight/count($valx);
		}  
	    }
	    $surveyq=$this->DB2->query("SELECT omc.category_name,rsa.mid,rsa.cid,rsa.`ques_id`,rpc.`compliance_id`,rsa.loc_id,rsa.`is_flag`,rsa.is_flag_resolved,DATE(rsa.added_on) as `date` FROM retailaudit_survey_answer rsa
	    INNER JOIN `retailaudit_is_promoter_category` rpc ON (rsa.mid=rpc.`category_id` AND rpc.`is_promoter`=0 AND rpc.`compliance_id` IN ({$compliance_ids}))
	    INNER JOIN offline_main_category omc ON (omc.id=rsa.mid AND omc.is_deleted=0)
	    WHERE DATE(added_on) IN ({$dates}) AND  rsa.loc_id IN ({$locids})");
		// echo $this->db->last_query(); //die;
	    
		 
		  
	    $compintarr=array();
	    $i=0;
	    foreach($surveyq->result() as $val){
		 if($val->loc_id==$datecond[$val->loc_id]['locid'] && $val->date==$datecond[$val->loc_id]['date']){
		$compintarr[$val->loc_id][$val->compliance_id][$val->mid][$i]=$val;
		$locidm=$val->loc_id;
		$i++;
		 }
	    }
	    
	    //echo "<pre>"; print_R($compintarr); die;
	    
	    $comparr=array();
	    foreach($compliance_catdetarr as $compkey =>$compval){
		if(isset($compintarr[$locidm][$compkey]))
		{
		    $comparr=$compintarr;
		}else{
		    //$comparr[$locidm][$compkey][]=(object) array();;
		}
		
	    }
	    
	     //echo "<pre>"; print_R($comparr); die;
	 
	  
	    $fnlcompliancearr=array(); 
	     foreach($comparr as $keyloc => $vall){
	    $totalloc=count($vall);
	   foreach($vall as $keycomp => $valcomp){
		 $deduct=$deductarr[$keycomp][$keyloc];
		 $totalpoint=$compliance_catdetarr[$keycomp]->weight;
		foreach($valcomp as $valz){
		    $checkf=0;
		    foreach($valz as $valy){
			$tmpvalloc = (array)$valy;
			$deduct=(empty($tmpvalloc))?0:$deduct;
			if($valy->is_flag==1 && $valy->is_flag_resolved==0){
			    if($checkf==0){
			    $totalpoint=$totalpoint-$deduct;
			    $checkf=1;
			    }
			    
			}
		    }
		}
		$fnlcompliancearr[$keyloc][$keycomp]['compliance_score']=round($totalpoint,2);
		$fnlcompliancearr[$keyloc][$keycomp]['compliance_name']=$compliance_catdetarr[$keycomp]->category_name;
	    }
	 }
	 $totalscore=0;
	    foreach($fnlcompliancearr[$locid] as $val){
		$totalscore=$totalscore+$val['compliance_score'];
	    }
	    $query=$this->DB2->query("select id from retailaudit_log_info where slug='AUDIT_COMPLETED' and date(added_on)='".$datex."'");
	    
	    if($query->num_rows()==0){
			
	    $tabledata=array("slug"=>"AUDIT_COMPLETED","loc_id"=>$locid,"added_on"=>date("Y-m-d H:i:s"),"score"=>$totalscore);
	    $this->DB2->insert('retailaudit_log_info',$tabledata);
	    }
     
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		
	    }else{
		$data['resultcode']=0;
		$data['resultmsg']="Success";
	    }
	    return $data;
	    
	}
	
	
	

	
	
	
	
	


}

?>
