<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class Signage_model extends CI_Model {
    private $db2; private $currsymbol = '';
    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2_publicwifi', TRUE);
    }
	
	 public function log($function, $request, $responce) {
        $log_data = array(
            'function' => $function,
            'request' => $request,
            'response' => $responce,
            'added_on' => date("Y-m-d H:i:s"),
        );
		$this->db2->insert("signage_logs",$log_data);
      //  $this->mongo_db->insert('log_data', $log_data);
    }
	
	
	public function signage_login($jsondata){
		$data=array();
		$query=$this->db2->query("select id,loc_id,loc_uid from signage_detailinfo where signage_code='".$jsondata->signage_id."' and is_deleted='0'");
		if($query->num_rows()>0){
			$rowarr=$query->row_array();
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			$data['locid']=$rowarr['loc_id'];
			$data['loc_uid']=$rowarr['loc_uid'];
		}else{
			$data['resultcode']=400;
			$data['resultmsg']="Login Failed";
		}
		return $data;
	}
	
	public function signage_offer($jsondata)
	{
		$data=array();
		$query=$this->db2->query("select id,loc_id,loc_uid from signage_detailinfo where signage_code='".$jsondata->signage_id."' and is_deleted='0' and status='1'");
		if($query->num_rows()>0){
			$rowdata=$query->row_array();
			$smartcashdata=array();
			$datarr=$this->get_currentgroup($rowdata['id']);
			$groupid='';
			if($datarr['resultcode']==200){
				$groupid=$datarr['groupid'];
			}else{
				return $datarr;
			}
			
			$query1=$this->db2->query("SELECT sdi.`signage_name`,sgi.`group_name`,fs.`campaign_name`,fs.`smartcash_value`,soi.screen_time ,fs.id as smartcashid,fsi.menu_id,
GROUP_CONCAT(fmm.`product_titlename` SEPARATOR '+') AS producttitle,
GROUP_CONCAT(fmm.`food_item_price` SEPARATOR '@') AS Menu,fs.end_date,
SUM(fmm.`food_item_price`) AS total
 FROM `signage_detailinfo` sdi
INNER JOIN `signage_group_info` sgi ON (sdi.id=sgi.`signage_id` and sgi.id='".$groupid."')
INNER JOIN `signage_offer_info` soi ON (soi.`group_id`=sgi.id)
INNER JOIN `foodiq_smartcash` fs ON (soi.`smartcash_id`=fs.id)
INNER JOIN `foodiq_smartcash_item` fsi ON (fsi.`smartcash_id`=fs.id)
INNER JOIN  `foodiq_menulist` fm ON (fsi.menu_id=fm.id)
INNER JOIN  `foodiq_master_menulist` fmm ON (fm.`master_menu_id`=fmm.id) WHERE 
sdi.`signage_code`='".$jsondata->signage_id."' AND CURDATE() BETWEEN DATE(fs.`start_date`) AND DATE(fs.end_date) GROUP BY fsi.`smartcash_id`
ORDER BY soi.priority ASC");
		if($query1->num_rows()>0){
			$i=0;
			foreach($query1->result() as $val){
				$smartcashdata[$val->smartcashid]['offer_type']='smartcash';
				$smartcashdata[$val->smartcashid]['offer_title']=$val->campaign_name;
				$smartcashdata[$val->smartcashid]['offer_name']=$val->producttitle;
				$smartcashdata[$val->smartcashid]['offer_price']=$val->total;
				$smartcashdata[$val->smartcashid]['smart_cash_price']=$val->smartcash_value;
				$smartcashdata[$val->smartcashid]['final_payment']=$val->total-$val->smartcash_value;
				$smartcashdata[$val->smartcashid]['expires_on']=strtotime($val->end_date);
				$smartcashdata[$val->smartcashid]['screen_time']=$val->screen_time;
				$smartcashdata[$val->smartcashid]['offer_text_desc']="";
				$smartcashdata[$val->smartcashid]['offer_image_path']="";
				$smartcashdata[$val->smartcashid]['offer_video_path']="";
				$i++;
			}
		}
		
		
		$query=$this->db2->query("SELECT soi.`offer_title`,soi.`offer_desc`,soi.`offertype`,soi.`image_original`,
soi.`video_original`,soi.`smartcash_id`,soi.`created_on`,soi.updated_on,soi.`screen_time`,soi.start_date,soi.end_date FROM `signage_offer_info` soi
INNER JOIN `signage_group_info` sgi ON (sgi.id=soi.group_id and sgi.status='1')
INNER JOIN `signage_detailinfo` sdi ON (sgi.signage_id=sdi.id) WHERE soi.`status`='1' and soi.group_id='".$groupid."' AND sdi.`signage_code`='".$jsondata->signage_id."' order by priority asc");
		
		if($query->num_rows()>0){
			$i=0;
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			//echo "<pre>"; print_R($query->result());//die;
			foreach($query->result() as $val){
				if($val->offertype=="smartcash"){
					if(array_key_exists($val->smartcash_id,$smartcashdata))
					{
						$data['offer'][$i]=$smartcashdata[$val->smartcash_id];
						$i++;
					}
					
				}else{
					$fpath='';
					if($val->updated_on!=''){
						$fpath=date("Y-m-d",strtotime($val->updated_on));
					}else{
						$fpath=date("Y-m-d",strtotime($val->created_on));
					}
					$fpath=date("Y-m-d",strtotime($val->created_on));
					$resourcepath='https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/signage/';
					$imagepath=($val->offertype=="image")?$resourcepath.$fpath."/".$val->image_original:"";
					$videopath=($val->offertype=="video")?$resourcepath.$fpath."/".$val->video_original:"";
					
					
					$curtimestamp=strtotime(date("Y-m-d H:i:s"));
					$startdatestamp=strtotime($val->start_date);
					$enddatestamp=strtotime($val->end_date);
					//echo "===>>".$curtimestamp.":::".$startdatestamp.":::".$enddatestamp;
					if($curtimestamp>=$startdatestamp && $curtimestamp<=$enddatestamp){
						$data['offer'][$i]['offer_type']=$val->offertype;
						$data['offer'][$i]['offer_title']=$val->offer_title;
						$data['offer'][$i]['offer_name']='';
						$data['offer'][$i]['offer_price']='';
						$data['offer'][$i]['smart_cash_price']='';
						$data['offer'][$i]['final_payment']='';
						$data['offer'][$i]['expires_on']='';
						$data['offer'][$i]['screen_time']=$val->screen_time;
						$data['offer'][$i]['offer_text_desc']=($val->offer_desc!="")?$val->offer_desc:"";
						$data['offer'][$i]['offer_image_path']=$imagepath;
						$data['offer'][$i]['offer_video_path']=$videopath;
						$i++;
					}
					
					
					
				}
			}
			
			
		}else{
			
			$data['resultcode']=400;
			$data['resultmsg']="No offer Found";
		}
		
		
			
		}else{
			$data['resultcode']=400;
			$data['resultmsg']="Login Failed";
		}
		return $data;
	}
	
	public function get_currentgroup($signageid){
		$data=array();
		$query=$this->db2->query("select `is_default`,`days`,`start_time`,`stop_time`,group_id from signage_group_timeline where signage_id='".$signageid."' and status='1'");
		
		if($query->num_rows()>0){
			$groupid='';
			
			foreach($query->result() as $val){
				if($val->is_default==0){
					
				$daysarr=explode(",",$val->days);
				$day=date("D");
				$currtimestamp=strtotime(date("H:i"));
				$startstamp=strtotime($val->start_time);
				$endstamp=strtotime($val->stop_time);
				if(in_array($day,$daysarr)){
					if($currtimestamp>=$startstamp && $currtimestamp<=$endstamp)
					{
						$groupid=$val->group_id;
						break;
					}
				}
				}
			}
			
			if($groupid==""){
				$query1=$this->db2->query("select `is_default`,`days`,`start_time`,`stop_time`,group_id from signage_group_timeline where signage_id='".$signageid."' and is_default='1'");
				
				$rowdarr=$query1->row_array();
				$groupid=$rowdarr['group_id'];
			}
			
			$data['resultcode']=200;
			$data['groupid']=$groupid;
			
		}else{
			echo "sssssss";die;
			$data['resultcode']=400;
			$data['resultcode']="No Offers Live";
		}
		return $data;
	}
	
	public function signage_sync($jsondata){
		$data=array();
		$squery=$this->db2->query("select id,loc_id,loc_uid from signage_detailinfo where signage_code='".$jsondata->signage_id."' and is_deleted='0'");
		
		if($squery->num_rows()>0){
			$data['resultcode']=200;
			$data['sync_available']=0;
			$rdata=$squery->row_array();
			
			$signage_id=$rdata['id'];
		$query=$this->db2->query("select id,is_default,synced_on from signage_sync_info where signage_id='".$signage_id."' and status='1' order by id desc limit 1");
		if($query->num_rows()>0){
			$rowdata=$query->row_array();
			$syncedon=$rowdata['synced_on'];
			$lastsyncedstamp=strtotime($rowdata['synced_on']);
			$isdefault=$rowdata['is_default'];
			
			//new group_id
			$query=$this->db2->query("select `is_default`,`days`,`start_time`,`stop_time`,group_id from signage_group_timeline where signage_id='".$signage_id."' and status='1'");
			$newdataarr=array();
			
			if($query->num_rows()>0){
				$groupid='';
				
				foreach($query->result() as $val){
					if($val->is_default==0){
						
					$daysarr=explode(",",$val->days);
					$day=date("D");
					$currtimestamp=strtotime(date("H:i"));
					$startstamp=strtotime($val->start_time);
					$endstamp=strtotime($val->stop_time);
					if(in_array($day,$daysarr)){
						if($currtimestamp>=$startstamp && $currtimestamp<=$endstamp)
						{
							$groupid=$val->group_id;
							$start_time=date("Y-m-d")." ".$val->start_time;
							break;
						}
					}
					}
				}
				
				if($groupid==""){
					$query1=$this->db2->query("select `is_default`,`days`,`start_time`,`stop_time`,group_id from signage_group_timeline where signage_id='".$signage_id."' and is_default='1'");
					$rowdarr=$query1->row_array();
					
					$groupid=$rowdarr['group_id'];
					$start_time=date("Y-m-d")." ".$rowdarr['start_time'];
					if($isdefault==0){
						$data['resultcode']=200;
						$data['sync_available']=1;
						$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"),"is_default"=>1);
						$this->db2->insert("signage_sync_info",$tabledata);
						return $data;
					}
					
				}
				
				$newdataarr['resultcode']=200;
				$newdataarr['groupid']=$groupid;
				$newdataarr['start_time']=$start_time;
				if(strtotime($start_time)>$lastsyncedstamp)
				{
					$data['resultcode']=200;
					$data['sync_available']=1;
					$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
					$this->db2->insert("signage_sync_info",$tabledata);
					return $data;
				}
				
			}
			
			
			//signage_detailinfo
			$query1=$this->db2->query("select id from signage_detailinfo where id='".$signage_id."' and (created_on>='".$syncedon."' OR updated_on>='".$syncedon."')");
			if($query1->num_rows()>0){
				$data['resultcode']=200;
				$data['sync_available']=1;
				$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
					$this->db2->insert("signage_sync_info",$tabledata);
					return $data;
			}
			
			//signage_group_info
			
			$query1=$this->db2->query("select id from signage_group_info where signage_id='".$signage_id."' and (created_on>='".$syncedon."' OR updated_on>='".$syncedon."')");
			if($query1->num_rows()>0){
				$data['resultcode']=200;
				$data['sync_available']=1;
				$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
					$this->db2->insert("signage_sync_info",$tabledata);
					return $data;
			}
			
			//signage_group_timeline
			
			$query1=$this->db2->query("select id from signage_group_timeline where signage_id='".$signage_id."' and (added_on>='".$syncedon."' OR updated_on>='".$syncedon."')");
			if($query1->num_rows()>0){
				$data['resultcode']=200;
				$data['sync_available']=1;
				$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
					$this->db2->insert("signage_sync_info",$tabledata);
					return $data;
			}
			
			//signage_offer_info
			
			$query1=$this->db2->query("select id from signage_offer_info where signage_id='".$signage_id."' and (created_on>='".$syncedon."' OR updated_on>='".$syncedon."')");
			if($query1->num_rows()>0){
				$data['resultcode']=200;
				$data['sync_available']=1;
				$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
				$this->db2->insert("signage_sync_info",$tabledata);
			return $data;
			}
			
			
			
		}	
		else{
			$data['resultcode']=200;
			$data['sync_available']=1;
			$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
			$this->db2->insert("signage_sync_info",$tabledata);
		return $data;
		}
		if($data['sync_available']==0){
			$tabledata=array("signage_id"=>$signage_id,"status"=>$data['sync_available'],"synced_on"=>date("Y-m-d H:i:s"));
			$this->db2->insert("signage_sync_info",$tabledata);
		return $data;
		}
		
		}else{
			$data['resultcode']=400;
			$data['resultmsg']="Login Failed";
			return $data;
		}
		
		
			
	}
    
   
}

?>