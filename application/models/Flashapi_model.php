<?php
class Flashapi_model extends CI_Model{

  public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
    }
	

	public function api_analytics_log($function_name, $userid, $usertype, $request){
		$platform=(isset($request->via))?$request->via:"";
		$request = json_encode($request);
		
		$tabledata=array("api_name"=>$function_name,"userid"=>$userid,"usertype"=>$usertype,"created_on"=>date("Y-m-d H:i:s"),"request"=>$request,"platform"=>$platform);
	$inserted=$this->DB2->insert('api_analytics_log',$tabledata);
	}
	public function authenticateApiuser($user_id, $api_key){
		$query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));
		$num = count($query);
		if($num > 0){
			return 1;
		}else{
			return 0;
		}
	}

	public function authenticateApiGuestuser($guest_user_id){
		$query = $this->mongo_db->get_where(TBL_GUEST_USER, array('_id' => new MongoId($guest_user_id)));
		$num = count($query);
		if($num > 0){
			return 1;
		}else{
			return 0;
		}
	}

	public function log($function, $request, $responce, $processTime){
		$log_data = array(
			'function_name' => $function,
			'request' => $request,
			'responce' => $responce,
			'time' => date("d-m-Y H:i:s"),
			'processTime' => $processTime

		);
		$this->mongo_db->insert('log_data', $log_data);
	}

	public function flash_listing($jsondata){
		//api analytics code
		$userid = '';
		$usertype = '';
		if($jsondata->is_login == '1'){
			$userid = $jsondata->login_userid;
			$usertype = 'Register';
		}
		else{
			$userid = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('flash_listing', $userid, $usertype, $jsondata);

		$data = array();

		if($jsondata->change_location != ''){
			$latlong = explode('::',$jsondata->change_location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}else{
			$latlong = explode('::',$jsondata->location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}

		$query = $this->DB2->query("select fs.flash_id,fs.sale_name,fs.brand_id, fs.sale_start_date, fs.sale_duration, b
		.brand_name, b.original_logo, b
.original_logo_logoimage, b.original_logo_smallimage, b.original_logo_mediumimage, b.original_logo_largeimage, CASE
WHEN fs.sale_start_date < now()
     THEN DATE_ADD(fs.sale_start_date, INTERVAL fs.sale_duration HOUR )
     ELSE fs.sale_start_date
END as flash_start_date from flash_sale as fs INNER
		JOIN brand as b ON fs
		.brand_id = b .brand_id WHERE fs.is_expire = '0' AND fs.is_draft = '0' AND fs.is_temp = '0' AND fs.is_deleted =
		'0'
		AND fs
		.status =
		'1' AND fs.sale_publish_date <= now()    ORDER BY flash_start_date ASC");
		if($query->num_rows() > 0){
			$i = 0;
			foreach($query->result() as $flash){
				$total_deal = '0';
				$deal_check = $this->DB2->query("select * from (select (
       6371 * ACOS (
         COS ( RADIANS( s.latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( s.longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( s.latitude ) )
       )
   ) AS distance, fsd.flash_deal_id from flash_sale_deal as fsd INNER JOIN flash_sale_store as fss ON (fsd
   .flash_deal_id = fss
   .flash_deal_id) INNER JOIN store as s ON (fss.store_id = s.store_id) WHERE fsd.flash_id = '$flash->flash_id'
   HAVING distance < 50) as temp GROUP BY flash_deal_id");
				$total_deal = $deal_check->num_rows();
				if($total_deal > 0){
					$is_close = '';
					$current_time =  date("Y-m-d H:i:s");
					$start_date = new DateTime($flash->sale_start_date);
					$since_start = $start_date->diff(new DateTime($current_time));
					$year 	=  $since_start->y;
					$month 	= $since_start->m;
					$day 	=  $since_start->d;
					$hr     =  $since_start->h;
					$min  	=  $since_start->i;
					$sec 	=  $since_start->s;
					$time_left = '';
					if($year > 0){
						$time_left = $year.'y : '.$month.'m: '. $day.'d';
					}elseif($month > 0){
						$time_left = $month.'m : '.$day.'d: '. $hr.'hr';
					}elseif($day > 0){
						$time_left = $day.'d : '.$hr.'h: '. $min.'m';
					}elseif($hr > 0){
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}else{
						$time_left = $hr.'h : '.$min.'m: '. $sec.'s';
					}
					//check flash sale start or not
					$isopen = '';
					$startsin = '';
					$closesin = '';
					if(strtotime($current_time) > strtotime($flash->sale_start_date)){
						$isopen = '1';
						$statr =  $flash->sale_start_date;
						$sale_end = date('Y-m-d H:i:s', strtotime($statr .'+' .$flash->sale_duration.' hours'));
						if(strtotime($current_time) > strtotime($sale_end)){
							$is_close = '1';
						}else{
							$current_time = new DateTime($current_time);
							$since_end = $current_time->diff(new DateTime($sale_end));
							$time_left1 = '';
							$time_left1 = $since_end->h.'h: '.$since_end->i.'m: '. $since_end->s.'s';
							$closesin = $time_left1;
						}

					}else{
						$isopen = '0';
						$startsin = $time_left;
					}
					if($is_close == ''){
						$data['deals'][$i]['flash_name'] = $flash->sale_name;
						$data['deals'][$i]['sale_start_date'] = $flash->sale_start_date;
						$sale_end_format = date('Y-m-d H:i:s', strtotime($flash->sale_start_date .'+'
							.$flash->sale_duration.' hours'));
						$end_time = strtotime($sale_end_format);
						$time = strtotime($flash->sale_start_date);
						$format_month = date('D, j M, g:i A', $time);
						$format_end_time = date('g:i A', $end_time);
						$final = $format_month. ' - '. $format_end_time;
						$data['deals'][$i]['sale_start_date_format'] = $final;
						$data['deals'][$i]['flashId'] = $flash->flash_id;
						$data['deals'][$i]['flashId'] = $flash->flash_id;
						$data['deals'][$i]['brandId'] = $flash->brand_id;
						$data['deals'][$i]['brandname'] = $flash->brand_name;
						$data['deals'][$i]['isopen'] =$isopen;
						$data['deals'][$i]['startsin'] =$startsin;
						$data['deals'][$i]['closesin'] =$closesin;
						$data['deals'][$i]['number'] = $total_deal;
						$logo_path = '';
						if($flash->original_logo_logoimage != ''){
							$logo_path = REWARD_IMAGEPATH."brand/logo/logo/".$flash->original_logo_logoimage;
						}
						$small_path = '';
						if($flash->original_logo_smallimage != ''){
							$small_path = REWARD_IMAGEPATH."brand/logo/small/".$flash->original_logo_smallimage;
						}
						$medium_path = '';
						if($flash->original_logo_mediumimage != ''){
							$medium_path = REWARD_IMAGEPATH."brand/logo/medium/".$flash->original_logo_mediumimage;
						}
						$large_path = '';
						if($flash->original_logo_largeimage != ''){
							$large_path = REWARD_IMAGEPATH."brand/logo/large/".$flash->original_logo_largeimage;
						}
						$original_path = '';
						if($flash->original_logo != ''){
							$original_path = REWARD_IMAGEPATH."brand/logo/original/".$flash->original_logo;
						}
						$data['deals'][$i]['brand_logo_image'] = $logo_path;
						$data['deals'][$i]['brand_small_image'] = $small_path;
						$data['deals'][$i]['brand_medium_image'] = $medium_path;
						$data['deals'][$i]['brand_large_image'] = $large_path;
						$data['deals'][$i]['brand_original_image'] = $original_path;
						$brand_deal_array = array();

						$deal_check = $this->DB2->query("select * from (select * from (select (
		     6371 * ACOS (
		       COS ( RADIANS( s.latitude ) )
		       * COS( RADIANS( $user_latitude ) )
		       * COS( RADIANS( s.longitude ) - RADIANS( $user_longitude ) )
		       + SIN ( RADIANS( $user_latitude ) )
		       * SIN( RADIANS( s.latitude ) )
		     )
		 ) AS distance, fsd.*, s.store_id from flash_sale_deal as fsd INNER JOIN flash_sale_store as fss ON (fsd
		 .flash_deal_id = fss.flash_deal_id) INNER JOIN store as s ON (fss.store_id = s.store_id) WHERE fsd.flash_id
		 = '$flash->flash_id' HAVING distance < 50) as temp ORDER BY distance) as temp1 GROUP BY flash_deal_id");
						$total_deal = $deal_check->num_rows();
						if($total_deal > 0){
							$j = 0;
							foreach($deal_check->result() as $flash1){
								//echo '<pre>'; print_r($flash1);
								if(isset($jsondata->login_userid) && $jsondata->login_userid != ''){
									$user_id = $jsondata->login_userid;
								}else{
									$user_id = $jsondata->guest_userid;
								}
								$check_already_redeem = $this->DB2->query("select id from my_flash WHERE flash_deal_id =
						'$flash1->flash_deal_id' AND user_id = '$user_id'");
								$isRedeemed = '';
								if($check_already_redeem->num_rows() > 0){
									$isRedeemed = '1';
								}else{
									$isRedeemed = '0';
								}

								$brand_deal_array[$j]['dealid'] = $flash1->flash_deal_id;
								if($flash1->distance*1000 < 500){
									$brand_deal_array[$j]['islocked'] = '0';
								}else{
									$brand_deal_array[$j]['islocked'] = '1';
								}
								if($flash1->distance > 1){
									$distance = round($flash1->distance,1).'Km';
								}else{
									$distance = round($flash1->distance*1000,1).'m';
								}
								$brand_deal_array[$j]['distance'] = $distance;
								$brand_deal_array[$j]['distance_for_me'] = round($flash1->distance, 2);
								$brand_deal_array[$j]['dealtext'] = $flash1->flash_sale_title;
								$expiry = '';
								if($flash1->voucher_static_type =='multiple'){
									$expiry = '0';
									$deal_id = $flash1->flash_deal_id;
									$no_of_code = $this->DB2->query("select coupon_code from flash_deal_coupon WHERE flash_deal_id = '$deal_id'");
									$num_voucher = $no_of_code->num_rows() - $flash1->voucher_redeemed;
								}
								else{
									if($flash1->no_of_voucher == '' || $flash1->no_of_voucher == '0'){
										$num_voucher = 'Till Expiry';
										$expiry = '1';
									}else{
										$expiry = '0';
										$num_voucher = $flash1->no_of_voucher - $flash1->voucher_redeemed;
									}
								}

								$brand_deal_array[$j]['isRedeemed'] = $isRedeemed;
								$brand_deal_array[$j]['expiry'] = $expiry;
								$brand_deal_array[$j]['remaining'] = $num_voucher;
								$brand_deal_array[$j]['voucher_type'] = $flash1->voucher_type;
								$brand_deal_array[$j]['voucher_code'] = $flash1->voucher_code;
								$deal_logo_path = '';
								if($flash1->image_logo != ''){
									$deal_logo_path = REWARD_IMAGEPATH."flash/logo/".$flash1->image_logo;
								}
								$brand_deal_array[$j]['deal_logo_image'] = $deal_logo_path;
								$deal_small_path = '';
								if($flash1->image_small != ''){
									$deal_small_path = REWARD_IMAGEPATH."flash/small/".$flash1->image_small;
								}
								$brand_deal_array[$j]['deal_small_image'] = $deal_small_path;
								$deal_medium_path = '';
								if($flash1->image_medium != ''){
									$deal_medium_path = REWARD_IMAGEPATH."flash/medium/".$flash1->image_medium;
								}
								$brand_deal_array[$j]['deal_medium_image'] = $deal_medium_path;
								$deal_large_path = '';
								if($flash1->image_large != ''){
									$deal_large_path = REWARD_IMAGEPATH."flash/large/".$flash1->image_large;
								}
								$brand_deal_array[$j]['deal_large_image'] = $deal_large_path;
								$deal_original_path = '';
								if($flash1->image_original != ''){
									$deal_original_path = REWARD_IMAGEPATH."flash/original/".$flash1->image_original;
								}
								$brand_deal_array[$j]['deal_original_image'] = $deal_original_path;
								$brand_deal_array[$j]['isFbShared'] = $flash1->fb_share;
								$j++;
							}
						}
						$data['deals'][$i]['branddeals']=$brand_deal_array;

					}//end of if .. it check deal is not close
					else{
						//flash sale is close so update the status is_close 1
						$update = $this->DB2->query("update flash_sale set is_expire = '1' where flash_id =
						'$flash->flash_id'");
					}
					$i++;
				}//if total deal is more then 0 end


			}//flash query foreach  loop end

			if(count($data)> 0){
				$data['resultCode'] = '1';
				$data['resultMsg'] = 'Success';
				$newarr = $data['deals'];
				$closearr=array();
				$openarr=array();
				$fnlarr=array();
				foreach($newarr as $val)
				{
					if($val['isopen']==1)
					{
						$closearr[]=$val;
					}
					else{
						$openarr[]=$val;

					}
				}
				usort( $closearr,  function($a, $b){
					$result = 0;
					if ($a['branddeals'][0]['distance_for_me'] > $b['branddeals'][0]['distance_for_me']) {
						$result = 1;
					} else if ($a['branddeals'][0]['distance_for_me'] < $b['branddeals'][0]['distance_for_me']) {
						$result = -1;
					}
					return $result;
				});

				$fnlarr=array_merge($closearr,$openarr);


				$sllicearr = array_slice($fnlarr,$jsondata->start,$jsondata->offset,TRUE);
				$sllicearr = array_values($sllicearr);
				if(count($sllicearr) == 0){
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'No More flash.';
				}
				$data['deals'] = $sllicearr;
				//echo '<pre>'; print_r($data['deals']); die;
				//$data['deals']=$fnlarr;
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'No Flash Found.';
			}
		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No Flash Found.';
		}


		//echo "<pre>";print_r($data);die;
		return $data;

	}

	public function flash_deal_detail_new($jsondata){
		//api analytics code
		$user_id = '';
		$usertype = '';
		if(isset($jsondata->login_userid) && $jsondata->login_userid != ''){
			$user_id = $jsondata->login_userid;
			$usertype = 'Register';
		}else{
			$user_id = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('flash_deal_detail_new', $user_id, $usertype, $jsondata);

		$query=$this->DB2->query("SELECT  b.original_logo, b.original_logo_logoimage, b.original_logo_smallimage, b.original_logo_mediumimage, b
.original_logo_largeimage, b.brand_name, fs.brand_id, fsd.*, fs.sale_name,fs.sale_start_date,fs.sale_publish_date,fs
.sale_duration, GROUP_CONCAT(fss.store_id) store_id
 FROM flash_sale_deal as fsd
INNER JOIN flash_sale as fs ON (fsd.flash_id=fs.flash_id)
INNER JOIN flash_sale_store as fss ON (fsd.flash_deal_id=fss.flash_deal_id) INNER JOIN brand as b ON
 (fs.brand_id = b.brand_id) WHERE fsd
.flash_deal_id='".$jsondata->dealid."'");
		if($query->num_rows() > 0){
			$i = 0;
			$flash=$query->row_array();
			//echo "<pre>"; print_R($flash);
			//die;

			//foreach($datarr as $flash){
			//echo "<pre>"; print_R($flash);
			$is_close = '';
			$current_time =  date("Y-m-d H:i:s");
			$start_date = new DateTime($flash['sale_start_date']);
			$since_start = $start_date->diff(new DateTime($current_time));
			//echo "<pre>"; print_R($since_start);
			$year 	=  $since_start->y;
			$month 	= $since_start->m;
			$day 	=  $since_start->d;
			$hr     =  $since_start->h;
			$min  	=  $since_start->i;
			$sec 	=  $since_start->s;
			$time_left = '';
			if($year > 0){
				$time_left = $year.' y : '.$month.' m : '. $day.' d';
			}elseif($month > 0){
				$time_left = $month.' m : '.$day.' d : '.$hr.' h';
			}elseif($day > 0){
				$time_left = $day.' d : '.$hr.' h : '. $min.' m';
			}elseif($hr > 0){
				$time_left = $hr.' h : '.$min.' m : '. $sec.' s';
			}else{
				$time_left = $hr.' h : '.$min.' m : '. $sec.' s';
			}
			//	echo "======>>>".$time_left;
			//check flash sale start or not
			$isopen = '';
			$startsin = '';
			$closesin = '';
			if(strtotime($current_time) > strtotime($flash['sale_start_date'])){
				$isopen = '1';
				$statr =  $flash['sale_start_date'];
				$sale_end = date('Y-m-d H:i:s', strtotime($statr .'+' .$flash['sale_duration'].' hours'));
				if(strtotime($current_time) > strtotime($sale_end)){
					$is_close = '1';
				}else{
					$current_time = new DateTime($current_time);
					$since_end = $current_time->diff(new DateTime($sale_end));
					$time_left1 = '';
					$time_left1 = $since_end->h.'h: '.$since_end->i.'m: '. $since_end->s.'s';

					$closesin = $time_left1;
				}

			}else{
				$isopen = '0';
				$startsin = $time_left;
			}
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
			$logo_path = '';
			if($flash['original_logo_logoimage'] != ''){
				$logo_path = REWARD_IMAGEPATH."brand/logo/logo/".$flash['original_logo_logoimage'];
			}
			$small_path = '';
			if($flash['original_logo_smallimage'] != ''){
				$small_path = REWARD_IMAGEPATH."brand/logo/small/".$flash['original_logo_smallimage'];
			}
			$medium_path = '';
			if($flash['original_logo_mediumimage'] != ''){
				$medium_path = REWARD_IMAGEPATH."brand/logo/medium/".$flash['original_logo_mediumimage'];
			}
			$large_path = '';
			if($flash['original_logo_largeimage'] != ''){
				$large_path = REWARD_IMAGEPATH."brand/logo/large/".$flash['original_logo_largeimage'];
			}
			$original_path = '';
			if($flash['original_logo'] != ''){
				$original_path = REWARD_IMAGEPATH."brand/logo/original/".$flash['original_logo'];
			}
			$data['brand_logo_image'] = $logo_path;
			$data['brand_small_image'] = $small_path;
			$data['brand_medium_image'] = $medium_path;
			$data['brand_large_image'] = $large_path;
			$data['brand_original_image'] = $original_path;
			$data['brandId'] = $flash['brand_id'];
			$data['brandname'] = $flash['brand_name'];
			$deal_logo_path = '';
			if($flash['image_logo'] != ''){
				$deal_logo_path = REWARD_IMAGEPATH."flash/logo/".$flash['image_logo'];
			}
			$data['deal_logo_image'] = $deal_logo_path;
			$deal_small_path = '';
			if($flash['image_small'] != ''){
				$deal_small_path = REWARD_IMAGEPATH."flash/small/".$flash['image_small'];
			}
			$data['deal_small_image'] = $deal_small_path;
			$deal_medium_path = '';
			if($flash['image_medium'] != ''){
				$deal_medium_path = REWARD_IMAGEPATH."flash/medium/".$flash['image_medium'];
			}
			$data['deal_medium_image'] = $deal_medium_path;
			$deal_large_path = '';
			if($flash['image_large'] != ''){
				$deal_large_path = REWARD_IMAGEPATH."flash/large/".$flash['image_large'];
			}
			$data['deal_large_image'] = $deal_large_path;
			$deal_original_path = '';
			if($flash['image_original'] != ''){
				$deal_original_path = REWARD_IMAGEPATH."flash/original/".$flash['image_original'];
			}
			$data['deal_original_image'] = $deal_original_path;
			$data['sale_name'] = $flash['sale_name'];
			$data['isFbShared'] = $flash['fb_share'];

			$remaining_voucher = '';
			$expiry = '';
			if($flash['voucher_static_type'] =='multiple'){
				$expiry = '0';
				$deal_id = $flash['flash_deal_id'];
				$no_of_code = $this->DB2->query("select coupon_code from flash_deal_coupon WHERE flash_deal_id = '$deal_id'");
				$remaining_voucher = $no_of_code->num_rows() - $flash['voucher_redeemed'];
			} else{
				if($flash['no_of_voucher'] == '' || $flash['no_of_voucher'] == '0'){
					$remaining_voucher = '';
					$expiry = '1';
				}else{
					$expiry = '0';
					$remaining_voucher = $flash['no_of_voucher']-$flash['voucher_redeemed'];
				}
			}
			$data['expiry'] = $expiry;
			$data['remaining'] = $remaining_voucher;
			$data['validtill'] = $flash['voucher_expiry'];
			$data['sale_start_date'] =  $flash['sale_start_date'];
			$data['deal_title'] =  $flash['flash_sale_title'];
			$data['description'] =  $flash['flash_sale_desc'];
			$data['term_condition'] =  $flash['term_condition'];
			$data['isopen'] = $isopen;
			$data['endtime'] = $closesin;
			$data['starttime'] = $startsin;
//get the all deal relate this flash sale
			$any_deal_purchase = $this->DB2->query("select flash_deal_id from flash_sale_deal WHERE flash_id = ( SELECT flash_id from flash_sale_deal WHERE flash_deal_id
 = '$jsondata->dealid')");
			$flash_deals = array();
			foreach($any_deal_purchase->result() as $any_deal_purchase1){
				$flash_deals[] = $any_deal_purchase1->flash_deal_id;
			}
			$tagid='"'.implode('", "', $flash_deals).'"';
			//check any deal purchase or not
			$check_already_redeem = $this->DB2->query("select id from my_flash WHERE flash_deal_id =
						'$jsondata->dealid' AND user_id = '$user_id'");
			$isRedeemed = '';

			if($check_already_redeem->num_rows() > 0){
				$isRedeemed = '1';
			}else{

				$check_any_deal_purchase = $this->DB2->query("select id from my_flash WHERE flash_deal_id IN ($tagid)
			AND user_id = '$user_id'");
				if($check_any_deal_purchase->num_rows()> 0){
					$isRedeemed = '2';
				}else{
					$isRedeemed = '0';
				}
			}


			$data['isRedeemed'] = $isRedeemed;


			if($jsondata->change_location != ''){
				$latlong = explode('::',$jsondata->change_location);
				$user_latitude = $latlong[0];
				$user_longitude = $latlong[1];
			}else{
				$latlong = explode('::',$jsondata->location);
				$user_latitude = $latlong[0];
				$user_longitude = $latlong[1];
			}


			$query111 = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( s.latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( s.longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( s.latitude ) )
       )
   ) AS distance from flash_sale_deal as fsd INNER
   JOIN
   flash_sale_store
    as fss ON (fsd.flash_deal_id= fss
   .flash_deal_id) INNER JOIN store as s ON (fss.store_id = s.store_id)  WHERE fsd.flash_deal_id =
   '$jsondata->dealid' HAVING distance <= 50 ORDER BY distance ");
			$row = $query111->row_array();
			if($row['distance'] > 1){
				$nearest_store_dist = round( $row['distance'],1).'Km';
			}else{
				$nearest_store_dist = round( $row['distance']*1000,1).'m';
			}
			if($row['distance']*1000 < 500){
				$data['islocked'] = '0';
			}else{
				$data['islocked'] = '1';
			}
			$data['total_store'] = $query111->num_rows();
			$data['distance'] = $nearest_store_dist;
			//}
		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Fail';
		}
		$brand_id = $flash['brand_id'];
		$via = 'android';
		if(isset($jsondata->via)){
			$via = $jsondata->via;
		}

		$insert_daily_log = $this->DB2->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$brand_id', '$via', '$user_id', now())");
		$insert_offer_daily_log = $this->DB2->query("insert into channel_flash_daily_analysis_log(deal_id, visit_via,
		 action_perform, visit_date) VALUES ('$jsondata->dealid','$via', '', now())");
//echo "<pre>";print_r($data);die;
		return $data;



	}

	public function check_distance($jsondata){
		//api analytics code
		$userid = '';
		$usertype = '';
		if($jsondata->is_login == '1'){
			$userid = $jsondata->login_userid;
			$usertype = 'Register';
		}
		else{
			$userid = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('check_distance', $userid, $usertype, $jsondata);

		$data = array();
		$latlong = explode('::',$jsondata->location);
		$user_latitude = $latlong[0];
		$user_longitude = $latlong[1];
		$query = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( s.latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( s.longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( s.latitude ) )
       )
   ) AS distance from flash_sale_deal as fsd INNER JOIN  flash_sale_store as fss ON (fsd.flash_deal_id= fss
   .flash_deal_id) INNER JOIN store as s ON (fss.store_id = s.store_id)  WHERE fsd.flash_deal_id =
   '$jsondata->dealid' ORDER BY distance limit 1");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'success';
			foreach($query->result() as $store_dis){
				$dis = $store_dis->distance;
			}
			if($dis*1000 < 500){
				$withinrange = '1';
			}else{
				$withinrange = '0';
			}
			$data['withinrange'] = $withinrange;
		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Deal id not match';
		}
		return $data;
	}

	public function share_deal_redemption($jsondata){
		$dealid = $jsondata->dealid;
		$share_via = $jsondata->sharevia;
		//api analytics code
		$user_id = '';
		$usertype = '';
		if($jsondata->login_userid != ''){
			$user_id = $jsondata->login_userid;
			$usertype = 'Register';
		}else{
			$user_id = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('share_deal_redemption', $user_id, $usertype, $jsondata);

		$shareQ = $this->DB2->query("select counter, share_via from flash_deal_redemtion_share WHERE deal_id = '$dealid' AND share_via =
'$share_via' AND user_id = '$user_id'");
		$data = array();
		if($shareQ->num_rows() > 0 ){
			$row = $shareQ->row_array();
			$counter = $row['counter']+1;
			$this->DB2->query("update flash_deal_redemtion_share set counter = '$counter', share_via = '$share_via' WHERE
			deal_id = '$dealid' AND share_via = '$share_via' AND user_id = '$user_id'");
			$data['resultCode'] = '1';
			$data['resultmsg'] = "success";
		}else{
			$this->DB2->query("insert into flash_deal_redemtion_share(user_id, deal_id, share_via, counter) VALUES ('$user_id','$dealid',
'$share_via','1')");
			$data['resultCode'] = '1';
			$data['resultmsg'] = "success";
		}
		return $data;
	}


	public function generate_voucher_code($jsondata){
		//api analytics code
		$userid = '';
		$usertype = '';
		if($jsondata->is_login == '1'){
			$userid = $jsondata->login_userid;
			$usertype = 'Register';
		}
		else{
			$userid = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('generate_voucher_code', $userid, $usertype, $jsondata);

		$data = array();

		if($jsondata->change_location != ''){
			$latlong = explode('::',$jsondata->change_location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}else{
			$latlong = explode('::',$jsondata->location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}

		$check_deal_already_purchase = $this->DB2->query("select user_id from my_flash WHERE user_id = '$jsondata->login_userid' AND flash_deal_id = '$jsondata->dealid'");
		if($check_deal_already_purchase->num_rows() > 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'You already purchase this deal';
		}else{
			$query = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( s.latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( s.longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( s.latitude ) )
       )
   ) AS distance, s.latitude,s.longitude,s.store_id, s.store_name,s.geo_address,s.mobile_number, fsd.* from flash_sale_deal as fsd INNER JOIN
   flash_sale_store as fss ON
   (fsd
   .flash_deal_id = fss
   .flash_deal_id) INNER JOIN store as s ON (fss.store_id = s.store_id) WHERE fsd.flash_deal_id =
   '$jsondata->dealid' HAVING distance <= 0.5 ORDER BY distance");
			if($query->num_rows() > 0){
				$row = $query->row_array();
				$voucher_left = '';
				if($row['no_of_voucher'] =='' || $row['no_of_voucher'] == '0'){
					$voucher_left = '1000000';//unlimitied voucher
				}else{
					$voucher_left = $row['no_of_voucher'] - $row['voucher_redeemed'];
				}
				if($voucher_left > 0){
					$voucher_code = '';
					if($row['voucher_type'] == 'static'){
						if($row['voucher_static_type'] == 'multiple'){
							$voucher = $this->DB2->query("select id,coupon_code from flash_deal_coupon WHERE is_used =
							 '0' AND flash_deal_id = '$jsondata->dealid'");
							if($voucher->num_rows() > 0){
								$row1 = $voucher->row_array();
								$voucher_code = $row1['coupon_code'];
								$id = $row1['id'];
								$update_coupon = $this->DB2->query("update flash_deal_coupon set is_used = '1',
								used_on = now() WHERE id = '$id' ");
							}else{
								$dynamic_voucher_expire = '1';
							}

						}else{
							$voucher_code = $row['voucher_code'];
						}
					}else{
						$voucher_code = $this->randon_voucher();
					}
					if(isset($dynamic_voucher_expire) && $dynamic_voucher_expire == 1){
						$data['resultCode'] = '';
						$data['resultMsg'] = 'No more voucher';
					}else{
						$data['resultCode'] = '1';
						$data['resultMsg'] = 'success';
						$data['validtill'] = $row['voucher_expiry'];
						$data['deal_title'] = $row['flash_sale_title'];
						$data['vouchercode'] = $voucher_code;
						$increase_coupon_code = $this->DB2->query("update flash_sale_deal set voucher_redeemed =
						voucher_redeemed+1 WHERE flash_deal_id = '$jsondata->dealid'");
						$store_id = $row['store_id'];
						$insert_voucher = $this->DB2->query("insert into my_flash(flash_deal_id, user_id, store_id,
						voucher_code, created_on) VALUES ('$jsondata->dealid',
'$jsondata->login_userid', '$store_id', '$voucher_code' , now())");
						$my_flash_id = $this->DB2->insert_id();
						$j = 0;
						$store = array();
						foreach($query->result() as $voucher){
							$insert = $this->DB2->query("insert into my_flash_store(my_flash_id, store_id) VALUES
							('$my_flash_id', '$voucher->store_id')");
							$store[$j]['storename'] = $voucher->store_name;
							$store[$j]['storeaddress'] = $voucher->geo_address;
							$store[$j]['storecontact'] = $voucher->mobile_number;
							$store[$j]['storeopentime'] = '';
							$store[$j]['storeoclosetime'] = '';
							$store[$j]['lat'] = $voucher->latitude;
							$store[$j]['long'] = $voucher->longitude;
							$j++;
						}
						$data['stores'] = $store;

						$data_msg = array("is_login" => $jsondata->is_login, 'login_userid' =>
							$jsondata->login_userid,
							"apikey" => $jsondata->apikey, "dealid" => $row['flash_deal_id'], "type" => 'sms',"store_address" => $voucher->geo_address);
						$data_msg = json_encode($data_msg);
						$this->coupon_code_send(json_decode($data_msg));


						$data_msg = array("is_login" => $jsondata->is_login, 'login_userid' => $jsondata->login_userid,
							"apikey" => $jsondata->apikey, "dealid" => $row['flash_deal_id'], "type" => 'email', "store_address" => $voucher->geo_address);
						$data_msg = json_encode($data_msg);
						$this->coupon_code_send(json_decode($data_msg));

						$via = 'android';
						if(isset($jsondata->via)){
							$via = $jsondata->via;
						}
						$insert_offer_daily_log = $this->DB2->query("insert into channel_flash_daily_analysis_log(deal_id, visit_via,
		 action_perform, visit_date) VALUES ('$jsondata->dealid','$via', 'coupon_claim', now())");
					}

				}else{
					$data['resultCode'] = '';
					$data['resultMsg'] = 'No more voucher';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'fail';
			}
		}



		//echo "<pre>";print_r($data);die;
		return $data;
	}

	public function randon_voucher(){
		$random_number = mt_rand(1000000, 9999999);
		$query = $this->DB2->query("select voucher_code from my_flash WHERE voucher_code = '$random_number'");
		if($query->num_rows() > 0){
			$this->randon_voucher();
		}else{
			return $random_number;
		}
	}

	// function to send coupon code
	public function coupon_code_send($jsondata){
		$user_id = $jsondata->login_userid;

		//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('coupon_code_sendF', $user_id, $usertype, $jsondata);

		$flahs_deal_id = $jsondata->dealid;
		$type = $jsondata->type;
		$store_address = '';

		$data = array();
		$query = $this->DB2->query("select mf.voucher_code, fsd.flash_sale_title, DATE_FORMAT(fsd.voucher_expiry, '%d-%m-%Y %H:%i') voucher_expiry, b.brand_name, b
		.original_logo from
		my_flash as mf INNER JOIN flash_sale_deal as fsd on
		(mf.flash_deal_id=fsd.flash_deal_id) INNER JOIN flash_sale as fs ON (fsd.flash_id = fs.flash_id) INNER JOIN
		brand as b ON (fs.brand_id = b.brand_id) WHERE mf.flash_deal_id = '$flahs_deal_id' AND mf.user_id = '$user_id'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$coupon_code = $row['voucher_code'];
			if(isset($jsondata->store_address)){
				$store_address ='Store location-'. $jsondata->store_address;
			}else{
				$store_id_add = $row['store_id'];
				$store_name_get = $this->DB2->query("select geo_address from store WHERE store_id = '$store_id_add'");
				$row1 = $store_name_get->row_array();
				$store_address = $row1['geo_address'];
			}
			/*$msg = urlencode("SHOUUT FLASH SALE VOUCHER").'%0a'.urlencode('Brand Name:-'.$row['brand_name']).'%0a'.
				urlencode('Voucher code:-'.$coupon_code).'%0a'.urlencode('Expires :-'.$row['voucher_expiry']).'%0a'
				.urlencode("TO REDEEM THIS COUPON.").'%0a'.urlencode("PRESENT THE COUPON CODE TO THE STORE MANAGE.");*/

			// get user detail
			$this->mongo_db->select(array('mobile', 'email', 'screen_name'));
			$this->mongo_db->where(array('_id' => new MongoId($user_id)));
			$result = $this->mongo_db->get('sh_user');

			$msg = urlencode('Hi '.$result[0]['screen_name'].',').'%0a'.'%0a'.
				urlencode('Congrats! You have successfully redeemed a Shouut Flash Sale voucher for '.$row['brand_name'])
				.'%0a'.urlencode('Offer - ' .$row['flash_sale_title'])
				.'%0a'.urlencode($store_address).'%0a'.urlencode('Voucher code:-'
					.$coupon_code).'%0a' .urlencode
				('Voucher expires on -'.$row['voucher_expiry']).'%0a'.'%0a'.
				urlencode('To avail this OFFER, please present this VOUCHER at the time of the payment in the STORE/VENUE.')
				.'%0a'.'%0a'.urlencode("For any query, please call us at 01165636400").'%0a'.'%0a'.urlencode("Cheers").'%0a'.urlencode
				("Team Shouut");

			if($type == "sms"){
				$this->send_via_sms($result[0]['mobile'], $msg);
			}else{
				$this->send_via_email($result[0]['email'], $row['original_logo'], $row['brand_name'],
					$result[0]['screen_name'], $row['flash_sale_title'], $coupon_code, $row['voucher_expiry'],$store_address);
			}
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';

		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No deal id found';
		}
		return $data;
	}

	public function send_via_sms($mobile, $msg){
		//$msgdata = urlencode($msg);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://173.45.76.226:81/send.aspx?username=shouut&pass=Sh0uutg1ant&route=trans1&senderid=SHOUUT&numbers=".$mobile."&message=".$msg);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
		if($statusCode==200){
			return 1;
		}
		curl_close($ch);
	}

	public function send_via_email($email, $image, $brand_name, $username, $desc, $coupon_code, $voucher_expiry,
								   $store_location){
		$subject = "SHOUUT FLASH SALE: ".$brand_name." VOUCHER";
		/*$headers = 'From: SHOUUT <flashsale@shouut.com>' . "\r\n";
		//$headers .= 'Cc: aashish@shouut.com'. "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";*/
		$message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg.jpg);
                        background-repeat:no-repeat; background-position:top center; width:100%">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px"><strong>Hi '.$username.',</strong></h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Congrats !
                         You have successfully redeemed an offer from
                          <strong>'.$brand_name.'</strong></p>
                        <p style="margin-bottom:5px; margin-top:10px; font-weight:normal; font-size:14px;">Offer-
                            '.$desc.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Store
                         location- '.$store_location.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        code- '.$coupon_code.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        expires on- '.$voucher_expiry.'</p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">To avail this OFFER, please present this VOUCHER at the time of the payment in the STORE/VENUE.</p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"> For any
                         query  : Please write to us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>
                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT FLASH SALE</h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';
		//echo $message;die;
		//mail($email,$subject,$message,$headers);
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "flashsale@shouut.com";
		$mail->Password = "shouut@#123";
		$mail->setFrom('flashsale@shouut.com', 'Flash Sale');
		$mail->addAddress($email, $username);
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		$mail->AltBody = '';
		$mail->send();
		return 1;

	}


	public function send_via_email_wifioffer($email, $image, $brand_name, $username, $desc, $longdesc, $coupon_code, $voucher_expiry,
											 $store_location){
		$subject = "SHOUUT Offers_".$brand_name." Voucher";
		/*$headers = 'From: SHOUUT <flashsale@shouut.com>' . "\r\n";
		//$headers .= 'Cc: aashish@shouut.com'. "\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";*/
		$message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg.jpg);
                        background-repeat:no-repeat; background-position:top center; width:100%">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px font-size:14px;"><strong>Hi '.$username.',</strong></h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Congrats !
                         You have successfully redeemed an offer from
                          <strong>'.$brand_name.'</strong></p>
                        <p style="margin-bottom:5px; margin-top:10px; font-weight:normal; font-size:14px;">Offer-
                            '.$desc.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">'.$longdesc.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        code- '.$coupon_code.'</p>
                        <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Voucher
                        expires on- '.$voucher_expiry.'</p>

                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"> For any
                         query  : Please write to us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>
                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                        <h5  style="margin-bottom:5px; margin-top:10px; font-size:14px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px; font-size:14px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT </h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong> AWSOME NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://itunes.apple.com/in/app/shouut/id1089501174" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';

		//mail($email,$subject,$message,$headers);
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 25;
		$mail->SMTPAuth = true;
		$mail->Username = "shouutoffers@shouut.com";
		$mail->Password = "Shouut@123#";
		$mail->setFrom('shouutoffers@shouut.com', 'Shouut Offers');

		$mail->addAddress($email, $username);
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->msgHTML($message);
		$mail->AltBody = '';
		$mail->send();
		return 1;

	}


	public function my_deals($jsondata){
		$data = array();
		//api analytics code
		$user_id = '';
		$usertype = '';
		if($jsondata->is_login == ''){
			$user_id = $jsondata->guest_userid;
			$usertype = 'Guest';
		}else{
			$user_id = $jsondata->login_userid;
			$usertype = 'Register';
		}
		$this->api_analytics_log('my_deals', $user_id, $usertype, $jsondata);

		$query = $this->DB2->query("select fs.sale_name, mf.id,mf.voucher_code as my_code, fsd.*,b.brand_name, b.original_logo_logoimage, b
.original_logo_smallimage, b.original_logo_mediumimage, b.original_logo_largeimage, b.original_logo from
		my_flash as mf INNER JOIN flash_sale_deal as
fsd ON (mf.flash_deal_id = fsd.flash_deal_id)  INNER JOIN flash_sale as fs ON (fsd.flash_id = fs.flash_id) INNER JOIN
 brand as b ON (fs.brand_id = b.brand_id) WHERE mf.user_id ='$user_id' AND CASE WHEN mf.is_single = 0 THEN mf.is_redeem
 = '0' ELSE TRUE END  AND mf.is_expire = '0' ");

		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'success';
			$i = '0';

			foreach($query->result() as $myflash){
				$data['deals'][$i]['dealid'] = $myflash->flash_deal_id;
				$data['deals'][$i]['voucher_code'] = $myflash->my_code;
				$data['deals'][$i]['deal_title'] = $myflash->flash_sale_title;
				$brand_logo_img = '';
				if($myflash->original_logo_logoimage != ''){
					$brand_logo_img = REWARD_IMAGEPATH."brand/logo/logo/".$myflash->original_logo_logoimage;
				}
				$brand_small_img = '';
				if($myflash->original_logo_smallimage != ''){
					$brand_small_img =REWARD_IMAGEPATH."brand/logo/small/".$myflash->original_logo_smallimage;
				}
				$brand_medium_img = '';
				if($myflash->original_logo_mediumimage != ''){
					$brand_medium_img =REWARD_IMAGEPATH."brand/logo/medium/".$myflash->original_logo_mediumimage;
				}
				$brand_large_img = '';
				if($myflash->original_logo_largeimage != ''){
					$brand_large_img = REWARD_IMAGEPATH."brand/logo/large/".$myflash->original_logo_largeimage;
				}
				$brand_original_img = '';
				if($myflash->original_logo != ''){
					$brand_original_img = REWARD_IMAGEPATH."brand/logo/original/".$myflash->original_logo;
				}
				$data['deals'][$i]['brand_logo_img'] = $brand_logo_img;
				$data['deals'][$i]['brand_small_img'] = $brand_small_img;
				$data['deals'][$i]['brand_medium_img'] = $brand_medium_img;
				$data['deals'][$i]['brand_large_img'] = $brand_large_img;
				$data['deals'][$i]['brand_original_img'] = $brand_original_img;
				$logo_img = '';
				if($myflash->image_logo != ''){
					$logo_img = REWARD_IMAGEPATH."flash/logo/".$myflash->image_logo;
				}
				$small_img = '';
				if($myflash->image_small != ''){
					$small_img = REWARD_IMAGEPATH."flash/small/".$myflash->image_small;
				}
				$medium_img = '';
				if($myflash->image_medium != ''){
					$medium_img = REWARD_IMAGEPATH."flash/medium/".$myflash->image_medium;
				}
				$original_img = '';
				if($myflash->image_original != ''){
					$original_img = REWARD_IMAGEPATH."flash/original/".$myflash->image_original;
				}

				$data['deals'][$i]['logo_img'] = $logo_img;
				$data['deals'][$i]['small_img'] = $small_img;
				$data['deals'][$i]['medium_img'] = $medium_img;
				$data['deals'][$i]['original_img'] = $original_img;
				$is_expire = '';
				$voucher_validity = '';
				$current_time =  date("Y-m-d H:i:s");
				$expire_time = '';
				if(strtotime($current_time) > strtotime($myflash->voucher_expiry)){
					// mydeal coupon code expire change the is_expire status
					$change_expiry_status = $this->DB2->query("update my_flash set is_expire = '1' WHERE user_id = '$user_id'
					AND flash_deal_id = '$myflash->flash_deal_id'");
					$is_expire = '1';
					$current_time =  date("Y-m-d H:i:s");
					$start_date = new DateTime($myflash->voucher_expiry);
					$since_start = $start_date->diff(new DateTime($current_time));
					$year 	=  $since_start->y;
					$month 	= $since_start->m;
					$day 	=  $since_start->d;
					$hr     =  $since_start->h;
					$min  	=  $since_start->i;
					$sec 	=  $since_start->s;
					if($year > 0){
						$expire_time = $year.' y :'.$month.' m : '.$day.' d';
					}elseif($month > 0){
						$expire_time = $month.' m :'.$day.' d : '.$hr.' h';
					}elseif($day > 0){
						$expire_time = $day.' d :'.$hr.' h : '. $min.' m';
					}elseif($hr > 0){
						$expire_time = $hr.' h :'.$min.' m : '.$sec.' s';
					}elseif($min > 0){
						$expire_time = $hr.' h :'.$min.' m : '.$sec.' s';
					}else{
						$expire_time = $hr.' h :'.$min.' m : '.$sec.' s';
					}
				}else{
					$is_expire = '0';
					$voucher_validity = $myflash->voucher_expiry;
				}
				$data['deals'][$i]['expiry'] = $is_expire;
				$data['deals'][$i]['expiry_time'] = $expire_time;
				$data['deals'][$i]['voucher_expiry'] = $voucher_validity;
				$store_detail = $this->DB2->query("select s.latitude,s.longitude,s.store_name,s.geo_address, s.mobile_number from
				my_flash_store as mfs INNER JOIN store as s ON (mfs.store_id = s.store_id) WHERE my_flash_id =
				'$myflash->id'");
				$j = 0;
				$store= array();
				foreach($store_detail->result() as $stores){
					$store[$j]['storename'] = $stores->store_name;
					$store[$j]['storeaddress'] = $stores->geo_address;
					$store[$j]['storecontact'] = $stores->mobile_number;
					$store[$j]['storeopentime'] = '';
					$store[$j]['storeoclosetime'] = '';
					$store[$j]['lat'] = $stores->latitude;
					$store[$j]['long'] = $stores->longitude;
					$j++;
				}
				$data['deals'][$i]['stores'] = $store;
				$data['deals'][$i]['brand_name'] = $myflash->brand_name;
				$data['deals'][$i]['title'] = $myflash->sale_name;
				$i++;
			}
		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No deal found';
		}
		//echo "<pre>";print_r($data);die;


		return $data;
	}

	public function store_list($jsondata){

		//api analytics code
		$userid = '';
		$usertype = '';
		if($jsondata->is_login == '1'){
			$userid = $jsondata->login_userid;
			$usertype = 'Register';
		}
		else{
			$userid = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('store_list', $userid, $usertype, $jsondata);

		$data = array();

		if($jsondata->change_location != ''){
			$latlong = explode('::',$jsondata->change_location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}else{
			$latlong = explode('::',$jsondata->location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}


		$query = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( s.latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( s.longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( s.latitude ) )
       )
   ) AS distance,s.store_name,s.geo_address,s.mobile_number, s.latitude, s.longitude from flash_sale_deal as fsd INNER
   JOIN
   flash_sale_store
    as fss ON (fsd.flash_deal_id= fss
   .flash_deal_id) INNER JOIN store as s ON (fss.store_id = s.store_id)  WHERE fsd.flash_deal_id =
   '$jsondata->dealid' HAVING distance <= 50 ORDER BY distance ");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'success';
			$i = 0;
			foreach($query->result() as $store_dis){
				$store_dist_in = '';
				if($store_dis->distance > 1){
					$store_dist_in = round( $store_dis->distance,1).'Km';
				}
				else{
					$store_dist_in = round( $store_dis->distance*1000,1).'m';
				}
				$data['store'][$i]['store_distance'] = $store_dist_in;
				$data['store'][$i]['store_name'] = $store_dis->store_name;
				$data['store'][$i]['store_address'] = $store_dis->geo_address;
				$data['store'][$i]['storecontact'] = $store_dis->mobile_number;
				$data['store'][$i]['storeopentime'] = '';
				$data['store'][$i]['storeoclosetime'] = '';
				$data['store'][$i]['latlong'] = $store_dis->latitude."::".$store_dis->longitude;

				$i++;
			}
		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No store found';
		}
		return $data;
	}

	public function share_deal($jsondata){
		$dealid = $jsondata->dealid;
		$share_via = $jsondata->sharevia;
		$shareQ = $this->DB2->query("select counter, share_via from flash_deal_share WHERE deal_id = '$dealid' AND share_via = '$share_via'");
		$data = array();
		if($shareQ->num_rows() > 0 ){
			$row = $shareQ->row_array();
			$counter = $row['counter']+1;
			$this->DB2->query("update flash_deal_share set counter = '$counter', share_via = '$share_via' WHERE
			deal_id = '$dealid' AND share_via = '$share_via'");
			$data['resultCode'] = '1';
			$total_share = $this->DB2->query("select counter from flash_deal_share WHERE deal_id = '$dealid'");
			$total_share_count = 0;
			foreach($total_share->result() as $row){
				$total_share_count = $row->counter + $total_share_count;
			}
			$data['share_count'] = $total_share_count;
		}else{
			$this->DB2->query("insert into flash_deal_share(deal_id, share_via, counter) VALUES ('$dealid', '$share_via',
			'1')");
			$data['resultCode'] = '1';
			$total_share = $this->DB2->query("select counter from flash_deal_share WHERE deal_id = '$dealid'");
			$total_share_count = 0;
			foreach($total_share->result() as $row){
				$total_share_count = $row->counter + $total_share_count;
			}

			$data['share_count'] = $total_share_count;
		}
		return $data;
	}

	public function getDistanceFromLatLonInMeter($lat1, $lon1, $lat2, $lon2, $unit){
		/*
           'Mi' is statute miles (default)
               'K' is kilometers
           'N' is nautical miles
           'M' is meter
        */
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else if ($unit == "M") {
			return round($miles * 1609.344);
		} else {
			return $miles;
		}
	}

	public function sms_flash_redeem($jsondata){
		$data = array();
		$mobile = $jsondata->mobile;


		//check mobile number register or not
		$this->mongo_db->where(array('mobile' => $mobile));
		$check_is_register = $this->mongo_db->get('sh_user');
		if(count($check_is_register) == 0){// user not register first register the user
			$tabledata = array(
				'user_type'  => '',
				'login_via'  => '',
				'screen_name'   => '',
				'f_name'     => '',
				'l_name'     => '',
				'email'      => '',
				'salt'       => '',
				'password'   => '',
				'apikey'     => '',
				'mobile'     => $mobile,
				'about_yourself' => '',
				'gender'	=> '',
				'dob'		=> '',
				'latitude'   => '',
				'longitude'  => '',
				'created_on' => date('Y-m-d h:i:s'),
				'status'     => '1',
				'is_verified' => '0',
				'privilege'	=> '0',
				'mobile_platform' => '',
				'super_admin' => '0',
				'do_permission' => '0',
				'article_permission' => '0',
				'suspended' => '0',
				'flash_user' => '1'
			);
			$userQ = $this->mongo_db->insert('sh_user', $tabledata);
			// now get record
			$this->mongo_db->where(array('mobile' => $mobile));
			$check_is_register = $this->mongo_db->get('sh_user');
		}
		//get the deal id from deal_Code send in msg

		$deal_id = $jsondata->deal_id;
		$user_id = $check_is_register[0]['_id']->{'$id'};
		//check user already redeem this deal or not
		$check_deal_already_purchase = $this->DB2->query("select user_id from my_flash WHERE user_id =
			'$user_id' AND flash_deal_id = '$deal_id'");
		if($check_deal_already_purchase->num_rows() > 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'You already purchase this deal';
		}else{
			$query = $this->DB2->query("select fsd.* from flash_sale_deal as fsd INNER JOIN flash_sale as fs ON (fsd
			.flash_id = fs.flash_id) WHERE fsd.flash_deal_id ='$deal_id'
			AND now() BETWEEN fs.sale_start_date AND DATE_ADD(fs.sale_start_date, INTERVAL fs.sale_duration HOUR )");

			if($query->num_rows() > 0){
				$row = $query->row_array();
				$voucher_left = '';
				if($row['no_of_voucher'] =='' || $row['no_of_voucher'] == '0'){
					$voucher_left = '1000000';//unlimitied voucher
				}else{
					$voucher_left = $row['no_of_voucher'] - $row['voucher_redeemed'];
				}
				if($voucher_left > 0){
					$voucher_code = '';
					if($row['voucher_type'] == 'static'){
						if($row['voucher_static_type'] == 'multiple'){
							$voucher = $this->DB2->query("select id,coupon_code from flash_deal_coupon WHERE is_used =
							 '0' AND flash_deal_id = '$deal_id'");
							if($voucher->num_rows() > 0){
								$row1 = $voucher->row_array();
								$voucher_code = $row1['coupon_code'];
								$id = $row1['id'];
								$update_coupon = $this->DB2->query("update flash_deal_coupon set is_used = '1',
								used_on = now() WHERE id = '$id' ");
							}else{
								$dynamic_voucher_expire = '1';
							}

						}else{
							$voucher_code = $row['voucher_code'];
						}
					}else{
						$voucher_code = $this->randon_voucher();
					}
					if(isset($dynamic_voucher_expire) && $dynamic_voucher_expire == 1){
						$data['resultCode'] = '';
						$data['resultMsg'] = 'No more voucher';
					}
					else{
						$data['resultCode'] = '1';
						$data['resultMsg'] = 'success';
						$data['validtill'] = $row['voucher_expiry'];
						$data['deal_title'] = $row['flash_sale_title'];
						$data['vouchercode'] = $voucher_code;
						$increase_coupon_code = $this->DB2->query("update flash_sale_deal set voucher_redeemed =
						voucher_redeemed+1 WHERE flash_deal_id = '$deal_id'");
						$store_id = '';
						$insert_voucher = $this->DB2->query("insert into my_flash(flash_deal_id, user_id, store_id,
						voucher_code, created_on) VALUES ('$deal_id',
'$user_id', '$store_id', '$voucher_code' , now())");
						$via = 'store_flash';
						$insert_offer_daily_log = $this->DB2->query("insert into channel_flash_daily_analysis_log(deal_id, visit_via,
		 action_perform, visit_date) VALUES ('$deal_id','$via', 'coupon_claim', now())");
					}
				}
				else{
					$data['resultCode'] = '';
					$data['resultMsg'] = 'No more voucher';
				}
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Wrong Deal ID';
			}
		}
		$result_code = $data['resultCode'];
		if($result_code == 0){
			$msg = urlencode($data['resultMsg']);
			$this->send_via_sms($mobile, $msg);
		}
		else{
			//send sms
			$data_msg = array("is_login" => '1', 'login_userid' => $user_id, "apikey" => '', "dealid" => $deal_id, "type" => 'sms');
			$data_msg = json_encode($data_msg);
			$this->coupon_code_send(json_decode($data_msg));
			//send email
			$data_msg = array("is_login" => '1', 'login_userid' => $user_id, "apikey" => '', "dealid" => $deal_id,
				"type" => 'email');
			$data_msg = json_encode($data_msg);
			$this->coupon_code_send(json_decode($data_msg));
		}

//echo "<pre>"; print_r($data);die;
		return $data;
	}

	public function sms_brand_voucher_code_redeem($jsondata){
		$data = array();
		$mobile = $jsondata->mobile;
		$voucher_code = $jsondata->voucher_code;
		//check user already used or not
		$check_already_used = $this->DB2->query("select id from brand_voucher_code_list where voucher_code =
		'$voucher_code' AND redeem_by = '$mobile'");
		if($check_already_used->num_rows() > 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Voucher Code Already Used';
		}
		else{
			$query = $this->DB2->query("select * from brand_voucher_code WHERE voucher_code = '$voucher_code'");
			if($query->num_rows() > 0){
				$row = $query->row_array();
				$duration = $row['duration'];
				$current_time =  date("Y-m-d H:i:s");
				if($row['no_of_voucher'] <= $row['no_of_voucher_used']){
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Allotted Number of vouchers are over.';
				}
				elseif(strtotime($current_time)< strtotime($row['valid_from'])){
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Please use voucher code between '.date('d-m-Y H:i', strtotime
						($row['valid_from'])). " to ". date('d-m-Y H:i', strtotime($row['valid_till']));
				}
				elseif(strtotime($current_time) > strtotime($row['valid_till'])){
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Voucher Code Expired';
				}
				else{
					$brand_voucher_id = $row['id'];
					$insert = $this->DB2->query("insert into brand_voucher_code_list(brand_voucher_code_id,
					voucher_code, is_redeem, redeemed_on, redeem_by) VALUES ('$brand_voucher_id',
					'$voucher_code', '1', now(), '$mobile')");
					$update = $this->DB2->query("update brand_voucher_code set no_of_voucher_used =
					(no_of_voucher_used + 1) WHERE id = '$brand_voucher_id'");
					if($duration > 12){
						$duration = $duration."mins";
					}
					else{
						$duration = $duration."hours";
					}
					$data['resultCode'] = '1';
					$messg = "You've got ".$duration.". Free WiFi. To use your voucher connect to your nearest SHOUUT
				Free WiFi network. Download SHOUUT and discover awesome near you.";
					$messg = str_replace(array( "\n", "\t", "\r"), '', $messg);
					$data['resultMsg'] = $messg;

					//redeem offer
					$get_offer_id = $this->DB2->query("select bvc.offer_type, bvc.offer_id from brand_voucher_code_list as
				 bvcl INNER JOIN brand_voucher_code as bvc ON (bvcl.brand_voucher_code_id = bvc.id) WHERE
				 bvcl.voucher_code = $voucher_code");
					if($get_offer_id->num_rows() > 0){
						$row = $get_offer_id->row_array();
						$offer_type = $row['offer_type'];
						$offer_id = $row['offer_id'];
						$this->mongo_db->where(array('mobile' => $mobile));
						$check_is_register = $this->mongo_db->get('sh_user');
						if(count($check_is_register) == 0){// user not register first register the user
							$random_salt = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,2 ) .substr( md5( time() ), 1, 5);
							$random_salt = md5(mt_rand(100000, 999999).$random_salt);
							$tabledata = array(
								'user_type'  => '',
								'login_via'  => '',
								'screen_name'   => '',
								'f_name'     => '',
								'l_name'     => '',
								'email'      => '',
								'salt'       => '',
								'password'   => '',
								'apikey'     => time().md5($random_salt),
								'mobile'     => $mobile,
								'about_yourself' => '',
								'gender'	=> '',
								'dob'		=> '',
								'latitude'   => '',
								'longitude'  => '',
								'created_on' => date('Y-m-d h:i:s'),
								'status'     => '1',
								'is_verified' => '0',
								'privilege'	=> '0',
								'mobile_platform' => '',
								'super_admin' => '0',
								'do_permission' => '0',
								'article_permission' => '0',
								'suspended' => '0',
								'wifi_voucher_user' => '1'
							);
							$userQ = $this->mongo_db->insert('sh_user', $tabledata);
							// now get record
							$this->mongo_db->where(array('mobile' => $mobile));
							$check_is_register = $this->mongo_db->get('sh_user');
						}
						$user_id = $check_is_register[0]['_id']->{'$id'};
						$api_key = $check_is_register[0]['apikey'];
						// wifi offer redeem
						if($offer_type == '2'){
							// get location if of offer
							$get_location_id = $this->DB2->query("select location_id from wifi_offer_location WHERE
						wifi_offer_id = '$offer_id'");
							$location_id = $get_location_id->row_array();
							$location_id = $location_id['location_id'];
							$data_msg = array("userId" => $user_id, "apikey" => $api_key, 'locationid' => $location_id,
								"offerId"=> $offer_id, "via"=> "web");
							$data_msg = json_encode($data_msg);
							$msg_return = $this->wifi_offer_redemption(json_decode($data_msg));
						}
					}
				}
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Invalid voucher code';
			}
		}

		//echo "<pre>";print_r($data);die;
		$result_code = $data['resultCode'];
		$msg = urlencode($data['resultMsg']);
		$this->send_via_sms($mobile, $msg);
		return $data;
	}

	public function wifi_locations($jsondata){
		$data = array();
		if($jsondata->location != ''){
			$latlong = explode('::',$jsondata->location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}
		$start = $jsondata->start;
		$offset = $jsondata->offset;
		//$start = 0;
		//$offset = 10;
		$query = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
       )
   ) AS distance,wl.mobile_number, wl.id, wl.location_name, wl.geo_address, wl.city, wl.state, wl.latitude, wl.longitude,
   wl.access_point_name, wl.access_point_macid, wp.provider_name, wp.id as provider_id, wp.image_logo
   from
   wifi_location as
    wl INNER JOIN wifi_provider as wp ON (wl.provider_id = wp.id) WHERE wl.status = '1'  AND wl.is_deleted = '0' AND wp.id != '14' HAVING distance < 50 ORDER BY
   distance LIMIT $start,$offset");
		$i=0;
		if($query->num_rows()>0){
			foreach($query->result() as $location){
				$macid_query = $this->DB2->query("select ssid from wifi_location_ssid WHERE locationid = '$location->id'");
				if($macid_query->num_rows() > 0){
					$data['providersList'][$i]['providerId'] = $location->provider_id;
					$data['providersList'][$i]['mobile'] = $location->mobile_number;
					$data['providersList'][$i]['providername'] = $location->provider_name;
					$data['providersList'][$i]['providerLogo'] = REWARD_IMAGEPATH."provider/logo/"
						.$location->image_logo;

					if($location->distance > 1){
						$distance = round($location->distance,1).'Km';
					}else{
						$distance = round($location->distance*1000,1).'m';
					}

					$data['providersList'][$i]['distance'] = $distance;
					$data['providersList'][$i]['locationid'] = $location->id;
					$data['providersList'][$i]['apLocationName'] = $location->location_name;
					$data['providersList'][$i]['apAddress'] = $location->geo_address;
					$data['providersList'][$i]['apCity'] = $location->city;
					$data['providersList'][$i]['apState'] = $location->state;
					$data['providersList'][$i]['location'] = $location->latitude."::".$location->longitude;
					$data['providersList'][$i]['apname'] = $location->access_point_name;
					$data['providersList'][$i]['apMACId'] = $location->access_point_macid;
					$k = 0;
					$ssid_array = array();
					foreach($macid_query->result() as $ssids){
						$ssid_array[$k]['ssid'] = $ssids->ssid;
						$k++;
					}
					$data['providersList'][$i]['SSID'] = $ssid_array;
					$i++;
				}
			}
		}else{
			$data['providersList'] = array();
		}

//echo "<pre>"; print_r($data);die;
		return $data;
	}

	public function wifi_locations1(){
		$data = array();
		$provider_query = $this->DB2->query("select wp.id, wp.provider_name, wp.image_logo from wifi_provider as wp
		WHERE is_deleted = 0");
		if($provider_query->num_rows() > 0){
			$i = 0;
			foreach($provider_query->result() as $providers){
				$location_query = $this->DB2->query("select id, location_name, geo_address, latitude, longitude, city,
				 state, access_point_name, access_point_macid from wifi_location WHERE status = 1 AND is_deleted = 0 AND
				 provider_id =
				 '$providers->id'");

				if($location_query->num_rows() > 0){
					$appList = array();
					$j = 0;
					foreach($location_query->result() as $locations) {
						$macid_query = $this->DB2->query("select ssid from wifi_location_ssid WHERE locationid = '$locations->id'");
						$ssid_array = array();
						if($macid_query->num_rows() > 0){
							$data['providersList'][$i]['providerId'] = $providers->id;
							$data['providersList'][$i]['providername'] = $providers->provider_name;
							$data['providersList'][$i]['providerLogo'] = REWARD_IMAGEPATH."provider/logo/"
								.$providers->image_logo;

							$appList[$j]['locationid'] = $locations->id;
							$appList[$j]['apLocationName'] = $locations->location_name;
							$appList[$j]['apAddress'] = $locations->geo_address;
							$appList[$j]['apCity'] = $locations->city;
							$appList[$j]['apState'] = $locations->state;
							$appList[$j]['location'] = $locations->latitude."::".$locations->longitude;
							$appList[$j]['apname'] = $locations->access_point_name;
							$appList[$j]['apMACId'] = $locations->access_point_macid;

							$k = 0;
							foreach($macid_query->result() as $ssids){
								$ssid_array[$k]['ssid'] = $ssids->ssid;
								$k++;
							}
							$appList[$j]['SSID'] = $ssid_array;
							$j++;
						}

					}
					$data['providersList'][$i]['appList'] = $appList;
					$i++;
				}
			}
		}else{
			$data['resultCode'] = 0;
			$data['resultMessage'] = 'No provider found';
		}


		//echo "<pre>"; print_r($data);die;
		return $data;
	}

	public function wifi_locations2($jsondata){
		$data = array();
		if($jsondata->location != ''){
			$latlong = explode('::',$jsondata->location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}

		$query = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
       )
   ) AS distance, wl.id, wl.location_name, wl.geo_address, wl.city, wl.state, wl.latitude, wl.longitude,
   wl.access_point_name, wl.access_point_macid, wp.provider_name, wp.id as provider_id, wp.image_logo
   from
   wifi_location as
    wl INNER JOIN wifi_provider as wp ON (wl.provider_id = wp.id) WHERE wl.status = '1'  AND wl.is_deleted = '0' HAVING distance < 5 ORDER BY
   distance ");
		$i=0;
		if($query->num_rows()>0){
			foreach($query->result() as $location){
				$macid_query = $this->DB2->query("select ssid from wifi_location_ssid WHERE locationid = '$location->id'");
				if($macid_query->num_rows() > 0){
					$data['providersList'][$i]['providerId'] = $location->provider_id;
					$data['providersList'][$i]['providername'] = $location->provider_name;
					$data['providersList'][$i]['providerLogo'] = REWARD_IMAGEPATH."provider/logo/"
						.$location->image_logo;

					if($location->distance > 1){
						$distance = round($location->distance,1).'Km';
					}else{
						$distance = round($location->distance*1000,1).'m';
					}

					$data['providersList'][$i]['distance'] = $distance;
					$data['providersList'][$i]['locationid'] = $location->id;
					$data['providersList'][$i]['apLocationName'] = $location->location_name;
					$data['providersList'][$i]['apAddress'] = $location->geo_address;
					$data['providersList'][$i]['apCity'] = $location->city;
					$data['providersList'][$i]['apState'] = $location->state;
					$data['providersList'][$i]['location'] =$location->latitude."::".$location->longitude;
					$data['providersList'][$i]['apname'] =  $location->access_point_name;
					$data['providersList'][$i]['apMACId'] = $location->access_point_macid;
					$k = 0;
					$ssid_array = array();
					foreach($macid_query->result() as $ssids){
						$ssid_array[$k]['ssid'] = $ssids->ssid;
						$k++;
					}
					$data['providersList'][$i]['SSID'] = $ssid_array;
					$i++;
				}
			}
		}else{
			$query = $this->DB2->query("select (
       6371 * ACOS (
         COS ( RADIANS( latitude ) )
         * COS( RADIANS( $user_latitude ) )
         * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
         + SIN ( RADIANS( $user_latitude ) )
         * SIN( RADIANS( latitude ) )
       )
   ) AS distance, wl.id, wl.location_name, wl.geo_address, wl.city, wl.state, wl.latitude, wl.longitude,
   wl.access_point_name, wl.access_point_macid, wp.provider_name, wp.id as provider_id, wp.image_logo
   from
   wifi_location as
    wl INNER JOIN wifi_provider as wp ON (wl.provider_id = wp.id) WHERE wl.status = '1'  AND wl.is_deleted = '0' HAVING distance < 50 ORDER BY
   distance limit 1 ");
			$i=0;
			if($query->num_rows()>0){
				foreach($query->result() as $location){
					$macid_query = $this->DB2->query("select ssid from wifi_location_ssid WHERE locationid = '$location->id'");
					if($macid_query->num_rows() > 0){
						$data['providersList'][$i]['providerId'] = $location->provider_id;
						$data['providersList'][$i]['providername'] = $location->provider_name;
						$data['providersList'][$i]['providerLogo'] = REWARD_IMAGEPATH."provider/logo/"
							.$location->image_logo;

						if($location->distance > 1){
							$distance = round($location->distance,1).'Km';
						}else{
							$distance = round($location->distance*1000,1).'m';
						}

						$data['providersList'][$i]['distance'] = $distance;
						$data['providersList'][$i]['locationid'] = $location->id;
						$data['providersList'][$i]['apLocationName'] = $location->location_name;
						$data['providersList'][$i]['apAddress'] = $location->geo_address;
						$data['providersList'][$i]['apCity'] = $location->city;
						$data['providersList'][$i]['apState'] = $location->state;
						$data['providersList'][$i]['location'] = $location->latitude."::".$location->longitude;
						$data['providersList'][$i]['apname'] =  $location->access_point_name;
						$data['providersList'][$i]['apMACId'] = $location->access_point_macid;
						$k = 0;
						$ssid_array = array();
						foreach($macid_query->result() as $ssids){
							$ssid_array[$k]['ssid'] = $ssids->ssid;
							$k++;
						}
						$data['providersList'][$i]['SSID'] = $ssid_array;
						$i++;
					}
				}
			}else{
				$data['providersList'] = array();
			}
		}

//echo "<pre>"; print_r($data);die;
		return $data;
	}

	public function wifi_new_bssid($jsondata){
		$data = array();
		//api analytics code
		$userid = '';
		$usertype = '';
		if($jsondata->is_login == 1){
			$userid = $jsondata->login_userid;
			$usertype = 'Register';
		}
		else{
			$userid = $jsondata->guest_userid;
			$usertype = 'Guest';
		}
		$this->api_analytics_log('wifi_new_bssid', $userid, $usertype, $jsondata);
		$new_bssid = array();
		$deleted_bssid = array();
		$check_user = $this->DB2->query("select * from wifi_user_bssid WHERE userid = '$userid'");
		if($check_user->num_rows() > 0){
			//existing user
			$row = $check_user->row_array();
			$last_date = $row['updated_on'];
			$get_bssid = $this->DB2->query("select ssid, is_deleted from wifi_location_ssid WHERE is_deleted = '0' AND created_on > '$last_date'");
			$j = 0;
			foreach($get_bssid->result() as $get_bssid1){
				$new_bssid[$j] = $get_bssid1->ssid;
				$j++;
			}
			$get_bssid_deleted = $this->DB2->query("select ssid, is_deleted from wifi_location_ssid WHERE is_deleted = '1' AND updated_on > '$last_date'");
			$i = 0;
			foreach($get_bssid_deleted->result() as $get_bssid_deleted1){
				$deleted_bssid[$i] = $get_bssid_deleted1->ssid;
				$i++;
			}
			//get bssid for mtnl
			$get_mtnl_bssid = $this->DB2->query("select wls.ssid from wifi_location as wl inner join wifi_location_ssid as wls ON ( wl.id = wls.locationid ) where wl.provider_id = '14' AND wls.is_deleted = '1'");
			foreach($get_mtnl_bssid->result() as $get_mtnl_bssid1){
				$deleted_bssid[$i] = $get_mtnl_bssid1->ssid;
				$i++;
			}
			//update user
			$update_user = $this->DB2->query("update wifi_user_bssid set updated_on = now() WHERE userid = '$userid'");
		}
		else{
			//new user
			$last_date = "2016-05-15 00:00:00";
			$get_bssid = $this->DB2->query("select ssid, is_deleted from wifi_location_ssid WHERE is_deleted = '0' AND created_on > '$last_date'");
			$j = 0;
			foreach($get_bssid->result() as $get_bssid1){
				$new_bssid[$j] = $get_bssid1->ssid;
				$j++;
			}
			$get_bssid_deleted = $this->DB2->query("select ssid, is_deleted from wifi_location_ssid WHERE is_deleted = '1' AND updated_on > '$last_date'");
			$i = 0;
			foreach($get_bssid_deleted->result() as $get_bssid_deleted1){
				$deleted_bssid[$i] = $get_bssid_deleted1->ssid;
				$i++;
			}
			//get bssid for mtnl
			$get_mtnl_bssid = $this->DB2->query("select wls.ssid from wifi_location as wl inner join wifi_location_ssid as wls ON ( wl.id = wls.locationid ) where wl.provider_id = '14' AND wls.is_deleted = '1'");
			foreach($get_mtnl_bssid->result() as $get_mtnl_bssid1){
				$deleted_bssid[$i] = $get_mtnl_bssid1->ssid;
				$i++;
			}
			//insert user
			$insert_user = $this->DB2->query("insert into wifi_user_bssid(userid, created_on, updated_on) VALUES
			('$userid', now(), now())");
		}
		$data['resultCode'] = '1';
		$data['deleted_ssid'] = $deleted_bssid;
		$data['new_ssid'] = $new_bssid;
		//echo "<pre>";print_r($data);die;
		return $data;
	}

	public function wifi_location_provider_id($jsondata){
		$data = array();
		$bssid = $jsondata->bssid;
		$query = $this->DB2->query("select wls.locationid,wl.location_ip, wl.provider_id from wifi_location_ssid as wls INNER JOIN
		wifi_location as wl on(wls.locationid = wl.id) WHERE wls.ssid = '$bssid' AND wls.is_deleted = '0' AND
		wl.is_deleted = '0' AND wl.status = '1'");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$row = $query->row_array();
			$data['locationid'] = $row['locationid'];
			$data['providerid'] = $row['provider_id'];
			$data['mobileapp_ip'] = $row['location_ip'];
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}

	public function wifi_offers2($jsondata){
		//api analytics code
		$userid = $jsondata->userId;
		$usertype = 'Register';
		$this->api_analytics_log('wifi_offers2', $userid, $usertype, $jsondata);

		//code with loop
		$data = array();
		$locationId = 0;
		if(isset($jsondata->locationId)){
			$locationId = $jsondata->locationId;
		}
		if(isset($jsondata->macid)){
			$macid = $jsondata->macid;
			$get_locationid = $this->DB2->query("select locationid from wifi_location_ssid WHERE ssid = '$macid'");
			if($get_locationid->num_rows() > 0){
				$row = $get_locationid->row_array();
				$locationId = $row['locationid'];
			}
		}
		//get live offerid for this location not for brand num 104 (104 is shouut brand)
		$offer_live = $this->DB2->query("select wo.id from wifi_offer as wo  INNER JOIN wifi_offer_location as wol ON
 (wo.id = wol.wifi_offer_id) WHERE wo.status = 1 AND wo.is_draft = 0 AND wo.is_deleted= 0 AND now() BETWEEN
 wo.start_date AND wo.end_date AND wol.location_id = '$locationId'  AND (wo.no_of_session - wo.session_used >0)
 AND wo.brand_id != '145'");
		$offer_live_array = array();
		if($offer_live->num_rows() > 0){//if offer find
			foreach($offer_live->result() as $offer_lives){
				$offer_live_array[] = $offer_lives->id;//create array of live offer id
			}

			//offer viewed for this location
			$offer_sceen = $this->DB2->query("select offerid from wifi_offer_sceen_sequence WHERE
locationid = '$locationId'");
			$offer_sceen_array = array();
			if($offer_sceen->num_rows() > 0){
				foreach($offer_sceen->result() as $offer_sceens){
					$offer_sceen_array[] = $offer_sceens->offerid;// create array of offer sceen
				}
			}
			$unseen_ids = '';
			foreach($offer_live_array as $v1){
				if(in_array($v1,$offer_sceen_array)){
					continue;
				}else{
					$unseen_ids = $v1;
					break;
				}
			}

			if($unseen_ids == ''){
				// delete all seen ids from db
				$delete = $this->DB2->query("delete from wifi_offer_sceen_sequence WHERE
				locationid = '$locationId'");
				$unseen_ids = $offer_live_array[0];
			}

			$offer = $this->DB2->query("select wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage, b.original_logo from wifi_offer as wo INNER JOIN brand as b ON (wo.brand_id
		 = b.brand_id) INNER JOIN wifi_offer_location as wol ON (wo.id = wol.wifi_offer_id) WHERE  wo.id =
		 '$unseen_ids' GROUP BY wo.id");
			if($offer->num_rows() > 0){
				$data['resultCode'] = '1';
				$data['resultMessage'] = "Success";
				$i = 0;
				foreach($offer->result() as $offers){
					$i++;
					$promotion_type = 'image';
					if($offers->promotion_type == 2){
						$promotion_type = "video";
					}
					$data['rewradtype'] = $promotion_type;
					$data['withoffer'] = $offers->including_offer;
					$data['brand_name'] = $offers->brand_name;
					$deal_small_path = '';
					//offer image
					if($offers->image_small != ''){
						$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
					}
					$data['offers_small_image'] = $deal_small_path;
					$deal_original_path = '';
					if($offers->image_original != ''){
						$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
					}
					$data['offers_original_image'] = $deal_original_path;

					//brand image
					$logo_path = '';
					if($offers->original_logo_logoimage != ''){
						$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
					}
					$original_path = '';
					if($offers->original_logo != ''){
						$original_path = WIFI_IMAGEPATH."brand/logo/original/".$offers->original_logo;
					}
					$data['brand_logo_image'] = $logo_path;
					$data['brand_original_image'] = $original_path;
					$data['validTill'] = $offers->end_date;

					$time = strtotime($offers->end_date);
					$format_month = date('D, j M Y g:i A', $time);
					$final = $format_month;
					$data['validTill_fromTime'] = $final;
					$data['promotion_name'] = $offers->promotion_name;
					$data['promotion_desc'] = $offers->promotion_description;
					$data['video_url'] = $offers->video_path;
					$data['offer_id'] = $offers->id;
					//insert last sceen offer id
					$insert_last_sceen = $this->DB2->query("insert into wifi_offer_sceen_sequence (userid, offerid, locationid)
				VALUES ('$jsondata->userId', '$offers->id', '$locationId')");

					$insert = $this->DB2->query("insert into wifi_offer_sceen (userid, wifioffer_id,locationid, sceen_on)
				VALUES
				('$jsondata->userId', '$offers->id', '$locationId', now())");
					$update = $this->DB2->query("update wifi_offer set session_used = (session_used+1) WHERE id = '$offers->id'");
				}
				if(isset($jsondata->via) && $jsondata->via != ''){
					$brand_id = $offers->brand_id;
					$via = $jsondata->via;
					$user_id = $jsondata->userId;
					//analytics code end
					$insert_daily_log = $this->DB2->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$brand_id', '$via', '$user_id', now())");
					$insert_wifioffer_daily_log = $this->DB2->query("insert into channel_wifioffer_daily_analysis_log
					(offer_id, visit_via, action_perform, visit_date) VALUES('$offers->id','$via', '', now())");
				}
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMessage'] = "No offer found";
			}
		}
		else{
			//default offer
			//get live offer for our brand
			$offer_live = $this->DB2->query("select wo.id from wifi_offer as wo WHERE wo.status = 1 AND wo.is_draft = 0
			AND wo.is_deleted= 0 AND now() BETWEEN
 wo.start_date AND wo.end_date  AND (wo.no_of_session - wo.session_used >0) AND wo.brand_id = '145'");
			$offer_live_array = array();
			if($offer_live->num_rows() > 0){
				foreach($offer_live->result() as $offer_lives){
					$offer_live_array[] = $offer_lives->id;
				}
				//offer viewed for this location
				$offer_sceen = $this->DB2->query("select offerid from wifi_offer_sceen_sequence WHERE userid =
		'$jsondata->userId' AND locationid = '$locationId'");
				$offer_sceen_array = array();
				if($offer_sceen->num_rows() > 0){
					foreach($offer_sceen->result() as $offer_sceens){
						$offer_sceen_array[] = $offer_sceens->offerid;
					}
				}
				$unseen_ids = '';
				foreach($offer_live_array as $v1){
					if(in_array($v1,$offer_sceen_array)){
						continue;
					}else{
						$unseen_ids = $v1;
						break;
					}
				}
				if($unseen_ids == ''){
					// delete all seen ids from db
					$delete = $this->DB2->query("delete from wifi_offer_sceen_sequence WHERE '$jsondata->userId' AND
				locationid = '$locationId'");
					$unseen_ids = $offer_live_array[0];
				}
				$offer = $this->DB2->query("select wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage, b.original_logo from wifi_offer as wo INNER JOIN brand as b ON (wo.brand_id
		 = b.brand_id) INNER JOIN wifi_offer_location as wol ON (wo.id = wol.wifi_offer_id) WHERE  wo.id =
		 '$unseen_ids' GROUP BY wo.id");
				if($offer->num_rows() > 0){
					$data['resultCode'] = '1';
					$data['resultMessage'] = "Success";
					$i = 0;
					foreach($offer->result() as $offers){
						$i++;
						$promotion_type = 'image';
						if($offers->promotion_type == 2){
							$promotion_type = "video";
						}
						$data['rewradtype'] = $promotion_type;
						$data['withoffer'] = $offers->including_offer;
						$data['brand_name'] = $offers->brand_name;
						$deal_small_path = '';
						//offer image
						if($offers->image_small != ''){
							$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
						}
						$data['offers_small_image'] = $deal_small_path;
						$deal_original_path = '';
						if($offers->image_original != ''){
							$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
						}
						$data['offers_original_image'] = $deal_original_path;

						//brand image
						$logo_path = '';
						if($offers->original_logo_logoimage != ''){
							$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
						}
						$original_path = '';
						if($offers->original_logo != ''){
							$original_path = WIFI_IMAGEPATH."brand/logo/original/".$offers->original_logo;
						}
						$data['brand_logo_image'] = $logo_path;
						$data['brand_original_image'] = $original_path;
						$data['validTill'] = $offers->end_date;

						$time = strtotime($offers->end_date);
						$format_month = date('D, j M Y g:i A', $time);
						$final = $format_month;
						$data['validTill_fromTime'] = $final;
						$data['promotion_name'] = $offers->promotion_name;
						$data['promotion_desc'] = $offers->promotion_description;
						$data['video_url'] = $offers->video_path;
						$data['offer_id'] = $offers->id;
						//insert last sceen offer id
						$insert_last_sceen = $this->DB2->query("insert into wifi_offer_sceen_sequence (userid, offerid, locationid)
				VALUES ('$jsondata->userId', '$offers->id', '$locationId')");

						$insert = $this->DB2->query("insert into wifi_offer_sceen (userid, wifioffer_id,locationid, sceen_on)
				VALUES
				('$jsondata->userId', '$offers->id', '$locationId', now())");
						$update = $this->DB2->query("update wifi_offer set session_used = (session_used+1) WHERE id = '$offers->id'");
					}
					if(isset($jsondata->via) && $jsondata->via != ''){
						$brand_id = $offers->brand_id;
						$via = $jsondata->via;
						$user_id = $jsondata->userId;
						//analytics code end
						$insert_daily_log = $this->DB2->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$brand_id', '$via', '$user_id', now())");
						$insert_wifioffer_daily_log = $this->DB2->query("insert into channel_wifioffer_daily_analysis_log
					(offer_id, visit_via, action_perform, visit_date) VALUES('$offers->id','$via', '', now())");
					}
				}
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMessage'] = "No offer";
			}

		}

		return $data;
	}

	public function wifi_default_offer(){
		$data = array();
		$unseen_ids = 5;
		$offer = $this->DB2->query("select wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage, b.original_logo from wifi_offer as wo INNER JOIN brand as b ON (wo.brand_id
		 = b.brand_id) INNER JOIN wifi_offer_location as wol ON (wo.id = wol.wifi_offer_id) WHERE  wo.id =
		 '$unseen_ids' GROUP BY wo.id");
		if($offer->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = "Success";
			$i = 0;
			foreach($offer->result() as $offers){
				$i++;
				$promotion_type = 'image';
				if($offers->promotion_type == 2){
					$promotion_type = "video";
				}
				$data['rewradtype'] = $promotion_type;
				$data['withoffer'] = $offers->including_offer;
				$data['brand_name'] = $offers->brand_name;
				$deal_small_path = '';
				//offer image
				if($offers->image_small != ''){
					$deal_small_path = REWARD_IMAGEPATH."flash/small/".$offers->image_small;
				}
				$data['offers_small_image'] = $deal_small_path;
				$deal_original_path = '';
				if($offers->image_original != ''){
					$deal_original_path = REWARD_IMAGEPATH."flash/original/".$offers->image_original;
				}
				$data['offers_original_image'] = $deal_original_path;

				//brand image
				$logo_path = '';
				if($offers->original_logo_logoimage != ''){
					$logo_path = REWARD_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
				}
				$original_path = '';
				if($offers->original_logo != ''){
					$original_path = REWARD_IMAGEPATH."brand/logo/original/".$offers->original_logo;
				}
				$data['brand_logo_image'] = $logo_path;
				$data['brand_original_image'] = $original_path;
				$data['validTill'] = $offers->end_date;

				$time = strtotime($offers->end_date);
				$format_month = date('D, j M Y g:i A', $time);
				$final = $format_month;
				$data['validTill_fromTime'] = $final;
				$data['promotion_name'] = $offers->promotion_name;
				$data['promotion_desc'] = $offers->promotion_description;
				$data['video_url'] = $offers->video_path;
				$data['offer_id'] = $offers->id;
			}

		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = "No offer found";
		}

		return $data;
	}

	public function wifi_registration($jsondata){
		$data = array();
		$userid = $jsondata->Userid;
		//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_registration', $userid, $usertype, $jsondata);

		$location = $jsondata->locationid;
		if($userid != '' && $location != ''){
			$query = $this->DB2->query("select id from wifi_register_user WHERE userid = '$userid' AND location_id = '$location'");
			if($query->num_rows() > 0){
				$data['result_code'] = 0;
				$data['result_message'] = "Already register for this wifi";
			}else{
				$register = $this->DB2->query("insert into wifi_register_user (userid, location_id, created_on) VALUES
			('$userid', '$location', now())");
				$data['result_code'] = 1;
				$data['result_message'] = "Success";
			}
		}else{
			$data['result_code'] = 0;
			$data['result_message'] = "userid or location id blank";
		}


		return $data;
	}

	public function wifi_offer_redemption($jsondata){
		$locationid = 0;
		if(isset($jsondata->locationid) && $jsondata->locationid != ''){
			$locationid = $jsondata->locationid;
		}
		//api analytics code
		$userid = $jsondata->userId;
		$usertype = 'Register';
		$this->api_analytics_log('wifi_offer_redemption', $userid, $usertype, $jsondata);
		$data = array();
		$user_latitude = '';
		$user_longitude = '';
		$store_location = '';
		if(isset($jsondata->location) && $jsondata->location != ''){
			$latlong = explode('::',$jsondata->location);
			$user_latitude = $latlong[0];
			$user_longitude = $latlong[1];
		}
		else{
			if(isset($jsondata->locationid)){
				$latlong = $this->DB2->query("select latitude, longitude from wifi_location WHERE id =
				'$jsondata->locationid'");
				if($latlong->num_rows() > 0){
					$row = $latlong->row_array();
					$user_latitude = $row['latitude'];
					$user_longitude = $row['longitude'];
				}
			}
		}
		$already_purchase = 0;
		$voucher_finish = 0;
		$check = $this->DB2->query("select id from wifi_offer_redeem WHERE userid = '$jsondata->userId' AND wifioffer_id = '$jsondata->offerId'");
		if($check->num_rows() > 0){
			$already_purchase = 1;
		}
		$query = $this->DB2->query("select wo.no_of_voucher, wo.voucher_redeemed,wo.promotion_name,wo.including_offer, wo.voucher_type, wo.voucher_static_type, wo.voucher_code,wo.image_small,
 wo.image_original, wo.voucher_expiry,b.brand_id, b.brand_name, b.original_logo_logoimage, b.original_logo from
 wifi_offer
 as wo
  INNER JOIN brand as b ON (wo.brand_id = b.brand_id) WHERE id ='$jsondata->offerId'");
		if($query->num_rows() > 0){
			$row = $query->row_array();
			if($row['including_offer'] == 1){
				$voucher_code = '';
				if($already_purchase == 0){
					if($row['voucher_type'] == 'static'){
						if($row['voucher_static_type'] == 'multiple'){
							$voucher = $this->DB2->query("select id,coupon_code from wifioffer_coupon WHERE is_used =
							 '0' AND wifi_offer_id = '$jsondata->offerId'");
							if($voucher->num_rows() > 0){
								$row1 = $voucher->row_array();
								$voucher_code = $row1['coupon_code'];
								$id = $row1['id'];
								$update_coupon = $this->DB2->query("update wifioffer_coupon set is_used = '1',
								used_on = now() WHERE id = '$id' ");
							}else{
								$dynamic_voucher_expire = '1';
							}

						}else{
							$voucher_code = $row['voucher_code'];
						}

					}else{
						$voucher_code = $this->randon_voucher();
					}
				}

				if(isset($dynamic_voucher_expire) && $dynamic_voucher_expire == 1){
					$voucher_finish = 1;
				}
				if($already_purchase == 1){
					$data['resultCode'] = 0;
					$data['resultMessage'] = 'Already Purchased';
				}elseif($voucher_finish == 1){
					$data['resultCode'] = 0;
					$data['resultMessage'] = 'Voucher finish';
				}elseif($row['no_of_voucher'] <= $row['voucher_redeemed']){
					$voucher_finish = 1;
					$data['resultCode'] = 0;
					$data['resultMessage'] = 'Voucher finish';
				}else{
					$data['resultCode'] = 1;
					$data['resultMessage'] = 'Success';
				}

				$data['promotion_name'] = $row['promotion_name'];
				$data['brand_name'] = $row['brand_name'];
				$deal_small_path = '';
				//offer image
				if($row['image_small'] != ''){
					$deal_small_path = WIFI_IMAGEPATH."flash/small/".$row['image_small'];
				}
				$data['offers_small_image'] = $deal_small_path;
				$deal_original_path = '';
				if($row['image_original'] != ''){
					$deal_original_path = WIFI_IMAGEPATH."flash/original/".$row['image_original'];
				}
				$data['offers_original_image'] = $deal_original_path;

				//brand image
				$logo_path = '';
				if($row['original_logo_logoimage'] != ''){
					$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$row['original_logo_logoimage'];
				}
				$original_path = '';
				if($row['original_logo'] != ''){
					$original_path = WIFI_IMAGEPATH."brand/logo/original/".$row['original_logo'];
				}
				$data['brand_logo_image'] = $logo_path;
				$data['brand_original_image'] = $original_path;
				$data['validTill'] = $row['voucher_expiry'];
				$time = strtotime($row['voucher_expiry']);
				$format_month = date('D, j M Y g:i A', $time);
				$final = $format_month;
				$data['validTill_fromTime'] = $final;
				$offer_location =  $this->DB2->query("select (6371 * ACOS (
								 COS ( RADIANS( latitude ) )
								 * COS( RADIANS( $user_latitude ) )
								 * COS( RADIANS( longitude ) - RADIANS( $user_longitude ) )
								 + SIN ( RADIANS( $user_latitude ) )
								 * SIN( RADIANS( latitude ) )
							   )
						   ) AS distance, wl.id,wl.location_name, wl.geo_address, wl.mobile_number, wl.latitude,
						   wl.longitude from wifi_offer_location as wol INNER JOIN wifi_location
						   as wl ON(wol.location_id = wl.id) WHERE wol.wifi_offer_id = '$jsondata->offerId' HAVING
						   distance < 50 ORDER BY distance");
				$locations = array();
				if($offer_location->num_rows() > 0){
					$i = 0;
					foreach($offer_location->result() as $offer_location1){
						if($i == 0){
							$store_location =$offer_location1->geo_address;
						}
						$locations[$i]['location_id'] = $offer_location1->id;
						$locations[$i]['location_name'] = $offer_location1->location_name;
						$locations[$i]['location_address'] = $offer_location1->geo_address;
						$locations[$i]['mobile'] = $offer_location1->mobile_number;
						$locations[$i]['location_latlong'] = $offer_location1->latitude."::"
							.$offer_location1->longitude;
						$i++;
					}
				}
				$data['location'] = $locations;
				if($already_purchase == 0 && $voucher_finish == 0){
					$update_voucher_redeem_counter = $this->DB2->query("update wifi_offer set voucher_redeemed =
					(voucher_redeemed + 1) WHERE id = '$jsondata->offerId'");
					$data['couponCode'] = $voucher_code;
					$insert_redeem = $this->DB2->query("insert into wifi_offer_redeem (wifioffer_id, userid,
							coupon_code, redeem_on) VALUES ('$jsondata->offerId', '$jsondata->userId',
							'$voucher_code', now())");
					$data['location'] = $locations;

					if(isset($jsondata->via) && $jsondata->via != ''){
						$brand_id = $row['brand_id'];
						$via = $jsondata->via;
						$user_id = $jsondata->userId;
						//analytics code end
						$insert_wifioffer_daily_log = $this->DB2->query("insert into channel_wifioffer_daily_analysis_log
					(offer_id, visit_via, action_perform, visit_date) VALUES('$jsondata->offerId','$via', 'coupon_claim', now())");

						//captive portal analysis
						$macid = '0';
						$today_date = date('Y-m-d');
						//check macid entry already exist or not
						$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationid' AND offer_id = '$jsondata->offerId' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
						if($check_id->num_rows() > 0){
							$row = $check_id->row_array();
							$temp_id = $row['cpwoid'];
							$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					redeem_counter = (redeem_counter+1) WHERE cpwoid = '$temp_id'");
						}
						else{
							$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, redeem_counter) VALUES ('$locationid',
				'$jsondata->offerId', now(), '$macid', '1')");
						}
					}
					//send mail and sms

					$data_msg = array("is_login" => '1', 'login_userid' => $jsondata->userId,
						"apikey" => $jsondata->apikey, "offerId" => $jsondata->offerId, "type" => 'sms',"store_location" => "$store_location");
					$data_msg = json_encode($data_msg);
					$this->coupon_code_send_wifioffer(json_decode($data_msg));

					$data_msg = array("is_login" => '1', 'login_userid' => $jsondata->userId,
						"apikey" => $jsondata->apikey, "offerId" => $jsondata->offerId, "type" => 'email', "store_location" => "$store_location");
					$data_msg = json_encode($data_msg);
					$this->coupon_code_send_wifioffer(json_decode($data_msg));
				}

			}else{
				$data['resultCode'] = 0;
				$data['resultMessage'] = 'no offer included';
			}
		}else{
			$data['resultCode'] = 0;
			$data['resultMessage'] = 'invalid offer id';
		}

		return $data;
	}


	// function to send coupon code
	public function coupon_code_send_wifioffer($jsondata){
		$user_id = $jsondata->login_userid;
		$flahs_deal_id = $jsondata->offerId;
		$type = $jsondata->type;
		$store_address =  $jsondata->store_location;
		$data = array();
		$query = $this->DB2->query("select wor.coupon_code, wo.promotion_name, wo.promotion_description, DATE_FORMAT(wo.voucher_expiry, '%d-%m-%Y %H:%i')
 voucher_expiry,
 b.brand_name, b.original_logo from wifi_offer_redeem as wor INNER JOIN wifi_offer as wo on
		(wor.wifioffer_id=wo.id)  INNER JOIN
		brand as b ON (wo.brand_id = b.brand_id) WHERE wor.wifioffer_id = '$flahs_deal_id' AND wor.userid =
		'$user_id'");

		if($query->num_rows() > 0){
			$row = $query->row_array();
			$coupon_code = $row['coupon_code'];

			// get user detail
			$this->mongo_db->select(array('mobile', 'email', 'screen_name'));
			$this->mongo_db->where(array('_id' => new MongoId($user_id)));
			$result = $this->mongo_db->get('sh_user');


			$msg = urlencode('Hi '.ucwords(strtolower($result[0]['screen_name'])).',').'%0a'.'%0a'.
				urlencode('Congrats! You have successfully redeemed an offer from '.$row['brand_name']).'%0a'.'%0a'.
				urlencode('Offer :-'.$row['promotion_name']).'%0a'.
				urlencode($row['promotion_description']).'%0a'.
				urlencode('Voucher Code:-'.$coupon_code).'%0a'.
				urlencode('Voucher Expiry :-'.$row['voucher_expiry']).'%0a'.'%0a'.
				urlencode("Cheers").'%0a'.urlencode("Team Shouut");
			// get user detail
			$this->mongo_db->select(array('mobile', 'email', 'screen_name'));
			$this->mongo_db->where(array('_id' => new MongoId($user_id)));
			$result = $this->mongo_db->get('sh_user');

			if($type == "sms"){
				$this->send_via_sms($result[0]['mobile'], $msg);
			}else{
				$this->send_via_email_wifioffer($result[0]['email'], $row['original_logo'], $row['brand_name'],
					ucwords(strtolower($result[0]['screen_name'])), $row['promotion_name'], $row['promotion_description'],
				$coupon_code, $row['voucher_expiry'], $store_address);
			}
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';

		}else{
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No deal id found';
		}
		return $data;
	}


	public function wifi_plans($jsondata){
		//api analytics code
		$userid = $jsondata->userId;
		$usertype = 'Register';
		$this->api_analytics_log('wifi_plans', $userid, $usertype, $jsondata);
		$data = array();
		$data['Plans']['0']['Planid'] = '1';
		$data['Plans']['0']['Planname'] = 'plan 1';
		$data['Plans']['0']['Plantime'] = '1 Hour';
		$data['Plans']['0']['plancost'] = '100';

		$data['Plans']['1']['Planid'] = '2';
		$data['Plans']['1']['Planname'] = 'plan 2';
		$data['Plans']['1']['Plantime'] = '2 Hours';
		$data['Plans']['1']['plancost'] = '180';

		$data['Plans']['2']['Planid'] = '3';
		$data['Plans']['2']['Planname'] = 'plan 3';
		$data['Plans']['2']['Plantime'] = '4 Hours';
		$data['Plans']['2']['plancost'] = '300';

		$data['Plans']['3']['Planid'] = '4';
		$data['Plans']['3']['Planname'] = 'plan 4';
		$data['Plans']['3']['Plantime'] = '8 Hours';
		$data['Plans']['3']['plancost'] = '800';
		//echo "<pre>"; print_r($data);die;
		return $data;
	}

	public function wifi_user_ipaddress_registration($jsondata){
		$data = array();
		$userid = $jsondata->Userid;
		//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_user_ipaddress_registration', $userid, $usertype, $jsondata);

		$APMAC = $jsondata->APMAC;
		$UEMAC = $jsondata->UEMAC;
		$IpAddress = $jsondata->IpAddress;
		$register = $this->DB2->query("insert into wifi_user_ipaddress (user_id, appmac, uemac, ipaddress,
				created_on) VALUES
			('$userid', '$APMAC', '$UEMAC', '$IpAddress', now())");
		$data['result_code'] = 1;
		$data['result_message'] = "Success";
		return $data;
	}

	public function wifi_user_session_add($jsondata){
		$data = array();
		$userid = $jsondata->userId;
		//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_user_session_add', $userid, $usertype, $jsondata);
		$locationid = '';
		if(isset($jsondata->locationid)){
			$locationid = $jsondata->locationid;
		}
		if(isset($jsondata->macid)){
			$macid = $jsondata->macid;
			$get_locationid = $this->DB2->query("select locationid from wifi_location_ssid WHERE ssid = '$macid'");
			if($get_locationid->num_rows() > 0){
				$row = $get_locationid->row_array();
				$locationid = $row['locationid'];
			}
		}
		$query = $this->DB2->query("select provider_id from wifi_location WHERE id = '$locationid'");
		$providerid = '';
		if($query->num_rows() > 0){
			$row = $query->row_array();
			$providerid = $row['provider_id'];
		}
		$session_date =  date("Y.m.d");
		$sessionstart = date("H:i:s");
		$sessionend = strtotime("+15 minutes", strtotime($sessionstart));
		$sessionend = date('H:i:s', $sessionend);
		$via = '';
		if(isset($jsondata->via)){
			$via = $jsondata->via;
		}

		$gender = '';
		$platform_filter = '';
		$macid = '';
		$age_group = '';
		if(isset($jsondata->macid)){
			$macid = $jsondata->macid;
		}
		$platform_filter = '';
		if(isset($jsondata->platform_filter)){
			$platform_filter = $jsondata->platform_filter;
		}
		$userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
		$gender = (isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
		($userdata[0]['gender'],0,1):'';
		$age_group = (isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
			?$userdata[0]['age_group']:'';
		$insert = $this->DB2->query("insert into wifi_user_session_info(userid,location_id, providerid, session_date,
		sessionstart,
		sessionend, via, gender,
		osinfo, macid, age_group) VALUES ('$userid', '$locationid', '$providerid', '$session_date',
		'$sessionstart',
		'$sessionend', '$via', '$gender', '$platform_filter', '$macid', '$age_group')");
		$data['resultCode'] = '1';
		$data['resultmessage'] = 'Success';
		return $data;
	}

	public function wifi_user_free_session_add($jsondata){
		$data = array();
		$userid = $jsondata->userId;
//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_user_free_session_add', $userid, $usertype, $jsondata);
		$locationid = '0';
		if(isset($jsondata->locationid)){
			$locationid = $jsondata->locationid;
		}
		$gender = '';
		$platform_filter = '';
		$macid = '';
		$age_group = '';
		$via = '';
		$phone_num = '';
		if(isset($jsondata->macid)){
			$macid = $jsondata->macid;
		}
		$platform_filter = '';
		if(isset($jsondata->platform_filter) && $jsondata->platform_filter!= ''){
			$platform_filter = $jsondata->platform_filter;
		}
		else{
			//get from previous
			$previous = $this->DB2->query("select osinfo,id from wifi_user_free_session where userid = '$jsondata->userId' and osinfo != '' order by id desc limit 1");
			if($previous->num_rows() > 0){
				$row_previous = $previous->row_array();
				$platform_filter = $row_previous['osinfo'];
			}
		}
		$userdata=$this->user_detail($jsondata->userId,$jsondata->apikey);
		$gender = (isset($userdata[0]['gender']) && $userdata[0]['gender']!='')?substr
		($userdata[0]['gender'],0,1):'';
		$age_group = (isset($userdata[0]['age_group']) && $userdata[0]['age_group']!='')
			?$userdata[0]['age_group']:'';
		if(isset($jsondata->via)){
			$via = $jsondata->via;
		}
		$phone_num = (isset($userdata[0]['mobile']) && $userdata[0]['mobile']!='')
			?$userdata[0]['mobile']:'';
		$insert = $this->DB2->query("insert into wifi_user_free_session (userid, session_date, location_id, gender,
		osinfo, macid, age_group, via, phone_num) VALUES
		('$userid', now(), '$locationid', '$gender', '$platform_filter', '$macid', '$age_group', '$via', '$phone_num')");
		$data['resultCode'] = '1';
		$data['resultmessage'] = 'Success';
		return $data;
	}
	public function user_detail($user_id, $api_key){

		$query = $this->mongo_db->get_where(TBL_USER, array('_id' => new MongoId($user_id), 'apikey' => $api_key));

		$num = count($query);
		if($num > 0){
			return $query;
		}else{
			return 0;
		}
	}
	public function wifi_user_session_list($jsondata){
		$data = array();
		$userid = $jsondata->userId;
		//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_user_session_list', $userid, $usertype, $jsondata);

		$start = $jsondata->start;
		$offset = $jsondata->offset;
		$query = $this->DB2->query("select DATE_FORMAT(wusi.session_date,'%d.%m.%Y')session_date,DATE_FORMAT
		(wusi.sessionstart,'%H:%i')sessionstart,DATE_FORMAT(wusi.sessionend,'%H:%i')sessionend,wl.location_name, wp.provider_name
		from wifi_user_session_info as wusi
		INNER JOIN wifi_location as wl ON (wusi.location_id = wl.id) INNER JOIN wifi_provider as wp ON (wl.provider_id  = wp.id)
		WHERE userid ='$userid' LIMIT $start,$offset");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultmessage'] = 'Success';
			$i = 0;
			foreach($query->result() as $sessions){
				$data['sessionlist'][$i]['name'] = $sessions->provider_name;
				$data['sessionlist'][$i]['address'] = $sessions->location_name;
				$data['sessionlist'][$i]['date'] = $sessions->session_date;
				$data['sessionlist'][$i]['sessionstart'] = $sessions->sessionstart;
				$data['sessionlist'][$i]['sessionend'] = $sessions->sessionend;
				$i++;
			}
		}
		else{
			$data['resultCode'] = '0';
			$data['resultmessage'] = 'No Record';
		}
		return $data;
	}

	public function wifi_user_available_session($jsondata){
		$data = array();
		$userid = $jsondata->userId;
		//api analytics code
		$usertype = 'Register';
		$this->api_analytics_log('wifi_user_available_session', $userid, $usertype, $jsondata);
		$providerId = '';
		if(isset($jsondata->providerId)){
			$providerId = $jsondata->providerId;
		}
		if(isset($jsondata->bssid) && $jsondata->bssid != ''){
			$macid = $jsondata->bssid;
			$get_providerid = $this->DB2->query("select wl.provider_id from wifi_location_ssid as wlss INNER JOIN
			wifi_location as wl on(wlss.locationid = wl.id) WHERE wlss.ssid ='$macid' AND wlss.is_deleted = '0'");
			if($get_providerid->num_rows() > 0){
				$row = $get_providerid->row_array();
				$providerId = $row['provider_id'];
			}
		}
		if($providerId == ''){
			$data['resuldCode'] = '0';
			$data['resuldMessage'] = 'Wrong BSSID';
		}
		else{
			//get provider no of session
			$no_of_session = 0;
			$get_no_of_session = $this->DB2->query("select no_of_session from wifi_provider WHERE id = '$providerId'");
			if($get_no_of_session->num_rows() > 0){
				$row = $get_no_of_session->row_array();
				$no_of_session = $row['no_of_session'];
			}
			//get user already used session
			$already_userd = 0;
			$get_already_used = $this->DB2->query("select id from wifi_user_session_info WHERE userid = '$userid' AND
		providerid = '$providerId' AND session_date = CURDATE()");
			$already_userd = $get_already_used->num_rows();
			if($no_of_session > $already_userd){
				$data['resuldCode'] = '1';
				$data['resuldMessage'] = 'Success';
			}
			else{
				$data['resuldCode'] = '0';
				$data['resuldMessage'] = 'Your All Session Expire';
			}
		}

		return $data;
	}
	public function wifi_location_macid($jsondata){
		$data = array();
		$locationid = $jsondata->locationId;
		$query = $this->DB2->query("select wl.id, wl.access_point_macid,wl.latitude, wl.longitude, wp.isp_logo,wp.id
		as provider_id
		from
		wifi_location as wl INNER JOIN wifi_provider as wp ON (wl.provider_id = wp.id) WHERE wl.id = '$locationid'");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'Success';
			$row = $query->row_array();
			$data['locationid'] = $row['id'];
			$data['APMAC'] = $row['access_point_macid'];
			$data['lat'] = $row['latitude'];
			$data['long'] = $row['longitude'];
			$provider_image_logo = '';
			if($row['isp_logo'] != ''){
				$provider_image_logo = REWARD_IMAGEPATHCACHE."provider/isp_logo/".$row['isp_logo'];
			}
			$data['provider_image_logo'] = $provider_image_logo;
			$data['provider_id'] = $row['provider_id'];;


		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}
		return $data;
	}


	public function captive_portal_location_wifi_offer($jsondata){
		$data = array();
		$today_date = date('Y-m-d');
		$locationId = 0;
		if(isset($jsondata->locationId) && $jsondata->locationId != ''){
			$locationId = $jsondata->locationId;
		}
		//get live offerid for this location not for brand num 104 (104 is shouut brand)
		$offer_live = $this->DB2->query("select  wl.geo_address,wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage,
			b.original_logo from wifi_offer as wo  INNER JOIN wifi_offer_location as wol ON
 (wo.id = wol.wifi_offer_id) INNER JOIN brand as b ON (wo.brand_id= b.brand_id)
 INNER JOIN wifi_location as wl ON (wol.location_id = wl.id) WHERE wo.status = 1 AND wo.is_draft = 0 AND wo.is_deleted= 0 AND now()
		 BETWEEN wo.start_date AND wo.end_date AND wol.location_id = '$locationId'  AND (wo.no_of_session - wo.session_used >0)
 AND wo.brand_id != '145'");
		if($offer_live->num_rows() > 0) {//if offer find
			$data['resultCode'] = '1';
			$data['resultMessage'] = "Success";
			$i = 0;
			foreach($offer_live->result() as $offers){
				$promotion_type = 'image';
				if($offers->promotion_type == 2){
					$promotion_type = "video";
				}
				$data['offers'][$i]['offer_place'] = $offers->geo_address;
				$data['offers'][$i]['rewradtype'] = $promotion_type;
				$data['offers'][$i]['withoffer'] = $offers->including_offer;
				$data['offers'][$i]['brand_name'] = $offers->brand_name;
				$deal_small_path = '';
				//offer image
				if($offers->image_small != ''){
					$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
				}
				$data['offers'][$i]['offers_small_image'] = $deal_small_path;
				$deal_original_path = '';
				if($offers->image_original != ''){
					$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
				}
				$data['offers'][$i]['offers_original_image'] = $deal_original_path;

				//brand image
				$logo_path = '';
				if($offers->original_logo_logoimage != ''){
					$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
				}
				$original_path = '';
				if($offers->original_logo != ''){
					$original_path = WIFI_IMAGEPATH."brand/logo/original/".$offers->original_logo;
				}
				$data['offers'][$i]['brand_logo_image'] = $logo_path;
				$data['offers'][$i]['brand_original_image'] = $original_path;
				$data['offers'][$i]['validTill'] = $offers->end_date;
				$time = strtotime($offers->end_date);
				$format_month = date('D, j M Y g:i A', $time);
				$final = $format_month;
				$data['offers'][$i]['validTill_fromTime'] = $final;
				$data['offers'][$i]['promotion_name'] = $offers->promotion_name;
				$data['offers'][$i]['promotion_desc'] = $offers->promotion_description;
				$data['offers'][$i]['video_url'] = $offers->video_path;
				$data['offers'][$i]['offer_id'] = $offers->id;
				$i++;
				//captive portal analytics
				$macid = '0';
				if(isset($jsondata->macid)){
					$macid = $jsondata->macid;
				}
				$os_info = '';
				if(isset($jsondata->os_info)){
					$os_info = $jsondata->os_info;
				}
				$today_date = date('Y-m-d');
				//check macid entry already exist or not
				$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationId' AND offer_id = '$offers->id' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
				if($check_id->num_rows() > 0){
					$row = $check_id->row_array();
					$temp_id = $row['cpwoid'];
					$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					impression_counter = (impression_counter+1) WHERE cpwoid = '$temp_id'");
				}
				else{
					$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, impression_counter, os_info) VALUES ('$locationId',
				'$offers->id', now(), '$macid', '1', '$os_info')");
				}
			}
			//total impression table
			$check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
				location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
			if($check_impression->num_rows() > 0){
				$row = $check_impression->row_array();
				$temp_id = $row['id'];
				$update_cp = $this->DB2->query("update channel_captiveportal_impression set
					total_impression = (total_impression+1) WHERE id = '$temp_id'");
			}
			else{
				$insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
				(location_id, total_impression, visit_date) VALUES ('$locationId',
				'1', now())");
			}
		}
		else{
			//shouut brand offer show
//get live offerid for this location not for brand num 104 (104 is shouut brand)
			$offer_live = $this->DB2->query("select  wl.geo_address,wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage,
			b.original_logo from wifi_offer as wo  INNER JOIN wifi_offer_location as wol ON
 (wo.id = wol.wifi_offer_id) INNER JOIN wifi_location as wl ON (wol.location_id = wl.id)
 INNER JOIN brand as b ON (wo.brand_id= b.brand_id) WHERE wo.status = 1 AND wo.is_draft = 0 AND wo.is_deleted= 0 AND now() BETWEEN
 wo.start_date AND wo.end_date   AND (wo.no_of_session - wo.session_used >0)
 AND wo.brand_id = '145' GROUP BY wo.id");
			if($offer_live->num_rows() > 0) {//if offer find
				$data['resultCode'] = '1';
				$data['resultMessage'] = "Success";
				$i = 0;
				foreach($offer_live->result() as $offers){
					$promotion_type = 'image';
					if($offers->promotion_type == 2){
						$promotion_type = "video";
					}
					$data['offers'][$i]['offer_place'] = $offers->geo_address;
					$data['offers'][$i]['rewradtype'] = $promotion_type;
					$data['offers'][$i]['withoffer'] = $offers->including_offer;
					$data['offers'][$i]['brand_name'] = $offers->brand_name;
					$deal_small_path = '';
					//offer image
					if($offers->image_small != ''){
						$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
					}
					$data['offers'][$i]['offers_small_image'] = $deal_small_path;
					$deal_original_path = '';
					if($offers->image_original != ''){
						$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
					}
					$data['offers'][$i]['offers_original_image'] = $deal_original_path;

					//brand image
					$logo_path = '';
					if($offers->original_logo_logoimage != ''){
						$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
					}
					$original_path = '';
					if($offers->original_logo != ''){
						$original_path = WIFI_IMAGEPATH."brand/logo/original/".$offers->original_logo;
					}
					$data['offers'][$i]['brand_logo_image'] = $logo_path;
					$data['offers'][$i]['brand_original_image'] = $original_path;
					$data['offers'][$i]['validTill'] = $offers->end_date;
					$time = strtotime($offers->end_date);
					$format_month = date('D, j M Y g:i A', $time);
					$final = $format_month;
					$data['offers'][$i]['validTill_fromTime'] = $final;
					$data['offers'][$i]['promotion_name'] = $offers->promotion_name;
					$data['offers'][$i]['promotion_desc'] = $offers->promotion_description;
					$data['offers'][$i]['video_url'] = $offers->video_path;
					$data['offers'][$i]['offer_id'] = $offers->id;
					$i++;
					//captive portal analytics
					$macid = '0';
					if(isset($jsondata->macid)){
						$macid = $jsondata->macid;
					}
					$os_info = '';
					if(isset($jsondata->os_info)){
						$os_info = $jsondata->os_info;
					}
					$today_date = date('Y-m-d');
					//check macid entry already exist or not
					$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationId' AND offer_id = '$offers->id' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
					if($check_id->num_rows() > 0){
						$row = $check_id->row_array();
						$temp_id = $row['cpwoid'];
						$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					impression_counter = (impression_counter+1) WHERE cpwoid = '$temp_id'");
					}
					else{
						$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, impression_counter, os_info) VALUES ('$locationId',
				'$offers->id', now(), '$macid', '1', '$os_info')");
					}
				}
				//total impression table
				$check_impression = $this->DB2->query("select id from channel_captiveportal_impression WHERE
				location_id = '$locationId' AND DATE(visit_date) = '$today_date' ");
				if($check_impression->num_rows() > 0){
					$row = $check_impression->row_array();
					$temp_id = $row['id'];
					$update_cp = $this->DB2->query("update channel_captiveportal_impression set
					total_impression = (total_impression+1) WHERE id = '$temp_id'");
				}
				else{
					$insert_cp = $this->DB2->query("insert into channel_captiveportal_impression
				(location_id, total_impression, visit_date) VALUES ('$locationId',
				'1', now())");
				}
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMessage'] = "No offer";
			}
		}
		return $data;
	}

	public function user_free_wifi_voucher_codes($jsondata){
		$data = array();
		$phone = $jsondata->phone;
		$query = $this->DB2->query("select bvcl.id as voucher_id, bvcl.redeemed_on, b.brand_name,b.original_logo_logoimage, b.original_logo, bvc.duration
 from brand_voucher_code_list as bvcl INNER JOIN brand_voucher_code as bvc ON (bvcl.brand_voucher_code_id = bvc.id)
 INNER JOIN brand as b ON (bvc.brand_id = b.brand_id) WHERE
		bvcl.redeem_by ='$phone' AND bvcl.is_used = '0' AND now() BETWEEN bvc.valid_from AND bvc.valid_till");
		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = "Success";
			$i = 0;
			foreach($query->result() as $codes){
				$data['vouchers'][$i]['voucher_id'] = $codes->voucher_id;
				$duration_unite = $codes->duration;
				if($duration_unite > 12){
					$duration_unite = "mins";
				}
				else{
					$duration_unite = "hours";
				}
				$data['vouchers'][$i]['voucher_duration'] = $codes->duration;
				$data['vouchers'][$i]['voucher_duration_unite'] = $duration_unite;
				$redeemed_on = date_create($codes->redeemed_on);
				//$redeemed_on =  date_format($redeemed_on, 'jS F, G:i');;
				$redeemed_on =  date_format($redeemed_on, 'd M, G:i');;
				$data['vouchers'][$i]['redeemed_on'] = $redeemed_on;
				$data['vouchers'][$i]['brand_name'] = $codes->brand_name;
				//brand image
				$logo_path = '';
				if($codes->original_logo_logoimage != ''){
					$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$codes->original_logo_logoimage;
				}
				$original_path = '';
				if($codes->original_logo != ''){
					$original_path = WIFI_IMAGEPATH."brand/logo/original/".$codes->original_logo;
				}
				$data['vouchers'][$i]['brand_logo_image'] = $logo_path;
				$data['vouchers'][$i]['brand_original_image'] = $original_path;
				$i++;
			}
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = "No voucher activated";
		}
		return $data;
	}

	public function user_free_wifi_voucher_offer($jsondata){
		$data = array();
		$voucherid = $jsondata->voucherid;
		$locationId = 0;
		if(isset($jsondata->locationId) && $jsondata->locationId != ''){
			$locationId = $jsondata->locationId;
		}
		//query to get attaced offer(wifi offer or promotion) and also get the id of offer
		$get_offer_id = $this->DB2->query("select bvc.duration, bvc.offer_type, bvc.offer_id, bvcl.redeem_by from
		brand_voucher_code_list as bvcl INNER JOIN brand_voucher_code as bvc ON (bvcl.brand_voucher_code_id = bvc.id
		) WHERE bvcl.id = '$voucherid' AND bvcl.is_redeem = '1' AND bvcl.is_used = '0'");
		if($get_offer_id->num_rows() > 0){
			$row = $get_offer_id->row_array();
			$offer_id = $row['offer_id'];
			$offer_type = $row['offer_type'];
			$phone = $row['redeem_by'];
			$duration = $row['duration'];
			//get userid from mongodb
			$this->mongo_db->select(array("_id"));
			$this->mongo_db->where(array("mobile"=>$phone));
			$user_info = $this->mongo_db->get("sh_user");
			$userid = '';
			if(count($user_info) > 0){
				$userid = $user_info[0]['_id'];
			}

			if($offer_type == 1){
				// offer type is promotion
				$offer = $this->DB2->query("select  b.brand_id,b.brand_name, b.original_logo, b.original_logo_logoimage, b
				.original_logo_smallimage, ge.event_id, ge
   .offer_short_desc,ge.action,ge.coupon_code,
   ge.offer_long_desc, ge.end_date
   FROM `get_event` ge  INNER JOIN brand b ON (ge.brand_id = b.brand_id) WHERE ge.event_id = '$offer_id'
    GROUP BY event_id");
				if($offer->num_rows() > 0){
					$data['resultCode'] = '1';
					$data['resultMessage'] = "Success";
					$data['offer_type'] = $offer_type;
					$data['phone'] = $phone;
					$i = 0;
					foreach($offer->result() as $offers){
						$i++;
						$duration_unite = $duration;
						if($duration_unite > 12){
							$duration_unite = "mins";
						}
						else{
							$duration_unite = "hours";
						}
						$data['brand_id'] = $offers->brand_id;
						$data['voucher_duration'] = $duration;
						$data['voucher_duration_unite'] = $duration_unite;
						$data['rewradtype'] = "image";
						$including_offer = 0;
						if($offers->action == 'coupon_code'){
							$including_offer = '1';
						}
						$data['coupon_code'] = $offers->coupon_code;
						$data['withoffer'] = $including_offer;
						$data['brand_name'] = $offers->brand_name;
						$deal_small_path = '';
						//offer image
						$get_images = $this->DB2->query("select * from get_event_image WHERE event_id = '$offers->event_id'");
						$image_detail = array();
						foreach($get_images->result() as $get_images1){
							for($k = 0; $k < 4; $k++){
								$event_image_original = '';
								$event_image_logo = '';
								$event_image_small = '';
								$event_image_medium = '';
								$event_image_large = '';
								if($k == 0){
									$get_images_list = $get_images1->image_name1;
									$myArray = explode(',', $get_images_list);
									if(isset($myArray[0]) && $myArray[0] != ''){
										$event_image_original = WIFI_IMAGEPATH.'cards/original/'.$myArray[0];
									}if(isset($myArray[2]) && $myArray[2] != ''){
										$event_image_small = WIFI_IMAGEPATH.'cards/small/'.$myArray[2];
									}
									$data['offers_original_image'] = $event_image_original;
									$data['offers_small_image'] = $event_image_small;

								}

							}

						}
						//brand image
						$logo_path = '';
						if($offers->original_logo_logoimage != ''){
							$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
						}
						$original_path = '';
						if($offers->original_logo != ''){
							$original_path = WIFI_IMAGEPATH."brand/logo/original/".$offers->original_logo;
						}
						$data['brand_logo_image'] = $logo_path;
						$data['brand_original_image'] = $original_path;
						$data['validTill'] = $offers->end_date;

						$time = strtotime($offers->end_date);
						$format_month = date('D, j M Y g:i A', $time);
						$final = $format_month;
						$data['validTill_fromTime'] = $final;
						$data['promotion_name'] = $offers->offer_short_desc;
						$data['promotion_desc'] = $offers->offer_long_desc;
						$data['video_url'] = '';
						$data['offer_id'] = $offers->event_id;
						//insert last sceen offer id

						if(isset($jsondata->via)){
							$via = $jsondata->via;
							$insert_daily_log = $this->DB2->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$offers->brand_id', '$via', '$userid', now())");
							$insert_offer_daily_log = $this->DB2->query("insert into channel_offer_daily_analysis_log(offer_id, visit_via,
		 action_perform, visit_date) VALUES ('$offers->event_id','$via', '', now())");
						}


					}
				}
				else{
					$data['resultCode'] = '0';
					$data['resultMessage'] = "No offer found";
				}
			}
			else{
				//offer type is wifi offer
				$offer = $this->DB2->query("select wo.*,b.brand_id, b.brand_name, b.original_logo_logoimage, b.original_logo from wifi_offer as wo INNER JOIN brand as b ON (wo.brand_id
		 = b.brand_id) INNER JOIN wifi_offer_location as wol ON (wo.id = wol.wifi_offer_id) WHERE  wo.id =
		 '$offer_id' GROUP BY wo.id");
				if($offer->num_rows() > 0){
					$data['resultCode'] = '1';
					$data['resultMessage'] = "Success";
					$data['offer_type'] = $offer_type;
					$data['phone'] = $phone;
					$i = 0;
					foreach($offer->result() as $offers){
						$i++;
						$duration_unite = $duration;
						if($duration_unite > 12){
							$duration_unite = "mins";
						}
						else{
							$duration_unite = "hours";
						}
						$data['brand_id'] = $offers->brand_id;
						$data['voucher_duration'] = $duration;
						$data['voucher_duration_unite'] = $duration_unite;
						$promotion_type = 'image';
						if($offers->promotion_type == 2){
							$promotion_type = "video";
						}
						$data['coupon_code'] = '';
						$data['rewradtype'] = $promotion_type;
						$data['withoffer'] = $offers->including_offer;
						$data['brand_name'] = $offers->brand_name;
						$deal_small_path = '';
						//offer image
						if($offers->image_small != ''){
							$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
						}
						$data['offers_small_image'] = $deal_small_path;
						$deal_original_path = '';
						if($offers->image_original != ''){
							$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
						}
						$data['offers_original_image'] = $deal_original_path;

						//brand image
						$logo_path = '';
						if($offers->original_logo_logoimage != ''){
							$logo_path = WIFI_IMAGEPATH."brand/logo/logo/".$offers->original_logo_logoimage;
						}
						$original_path = '';
						if($offers->original_logo != ''){
							$original_path = WIFI_IMAGEPATH."brand/logo/original/".$offers->original_logo;
						}
						$data['brand_logo_image'] = $logo_path;
						$data['brand_original_image'] = $original_path;
						$data['validTill'] = $offers->end_date;

						$time = strtotime($offers->end_date);
						$format_month = date('D, j M Y g:i A', $time);
						$final = $format_month;
						$data['validTill_fromTime'] = $final;
						$data['promotion_name'] = $offers->promotion_name;
						$data['promotion_desc'] = $offers->promotion_description;
						$data['video_url'] = $offers->video_path;
						$data['offer_id'] = $offers->id;
						//insert last sceen offer id

						/*$insert = $this->DB2->query("insert into wifi_offer_sceen (userid, wifioffer_id,locationid,
						sceen_on)
				VALUES ('$userid', '$offers->id', '$locationId', now())");
						$update = $this->DB2->query("update wifi_offer set session_used = (session_used+1) WHERE id =
						'$offers->id'");*/
					}
					/*if(isset($jsondata->via) && $jsondata->via != ''){
						$brand_id = $offers->brand_id;
						$via = $jsondata->via;
						$user_id = $userid;
						//analytics code end
						$insert_daily_log = $this->DB2->query("insert into channel_daily_analysis_log(brand_id, visit_via, user_id,
		visit_date)
		values('$brand_id', '$via', '$user_id', now())");
						$insert_wifioffer_daily_log = $this->DB2->query("insert into channel_wifioffer_daily_analysis_log
					(offer_id, visit_via, action_perform, visit_date) VALUES('$offers->id','$via', '', now())");
					}*/
					//captive portal analytics
					$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, action_type, visit_date) VALUES ('$locationId', '$offers->id',
				'click', now())");
				}
				else{
					$data['resultCode'] = '0';
					$data['resultMessage'] = "No offer found";
				}
			}
		}
		else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'Invalid voucher id';
		}
		return $data;
	}

	public function user_free_wifi_voucher_code_use($jsondata){
		$data = array();
		$voucher_id = $jsondata->voucherid;
		$query = $this->DB2->query("update brand_voucher_code_list set is_used = '1', used_on = now() WHERE id = '$voucher_id'");
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}

	public function wifi_offer_detail($jsondata){
		$data = array();
		$locationid = 0;
		if(isset($jsondata->locationId) && $jsondata->locationId != ''){
			$locationid = $jsondata->locationId;
		}
		$offer_id = $jsondata->offerId;
		$offer_live = $this->DB2->query("select b.brand_name, wo.*, wl.* from wifi_offer as wo INNER JOIN
	wifi_offer_location as wol ON (wo.id = wol.wifi_offer_id) INNER JOIN wifi_location as wl ON (wol.location_id = wl.id)
	 INNER JOIN brand as b ON (wo.brand_id = b.brand_id)
        WHERE wo.id ='$offer_id' AND wl.id = $locationid");
		if($offer_live->num_rows() > 0){
			foreach($offer_live->result() as $offers){
				$data['resultCode'] = '1';
				$data['resultMessage'] = "Success";
				$data['brand_name'] = $offers->brand_name;
				$promotion_type = 'image';
				if($offers->promotion_type == 2){
					$promotion_type = "video";
				}
				$data['rewradtype'] = $promotion_type;
				$data['withoffer'] = $offers->including_offer;
				//offer image
				if($offers->image_small != ''){
					$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
				}
				$data['offers_small_image'] = $deal_small_path;
				$deal_original_path = '';
				if($offers->image_original != ''){
					$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
				}
				$data['offers_original_image'] = $deal_original_path;
				$data['promotion_name'] = $offers->promotion_name;
				$data['promotion_desc'] = $offers->promotion_description;
				$data['video_url'] = $offers->video_path;
				$data['offer_id'] = $offers->id;
				$data['location_name'] = $offers->location_name;
				$data['location_address'] = $offers->geo_address;
				$data['phone'] = $offers->mobile_number;
				$voucher_expiry = date_create($offers->voucher_expiry);
				$data['voucher_expiry'] = date_format($voucher_expiry,"d/m/Y h:iA");
				//captive portal analytics
				$macid = '0';
				if(isset($jsondata->macid)){
					$macid = $jsondata->macid;
				}
				$today_date = date('Y-m-d');
				//check macid entry already exist or not
				$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationid' AND offer_id = '$offer_id' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
				if($check_id->num_rows() > 0){
					$row = $check_id->row_array();
					$temp_id = $row['cpwoid'];
					$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					click_counter = (click_counter+1) WHERE cpwoid = '$temp_id'");
				}
				else{
					$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, click_counter) VALUES ('$locationid',
				'$offer_id', now(), '$macid', '1')");
				}
			}
		}
		else{
			$offer_live = $this->DB2->query("select b.brand_name,wo.*, wl.* from wifi_offer as wo INNER JOIN wifi_offer_location as wol ON
        (wo.id = wol.wifi_offer_id) INNER JOIN wifi_location as wl ON (wol.location_id = wl.id) INNER JOIN brand as b ON (wo.brand_id = b.brand_id) WHERE  wo.id =
        '$offer_id' GROUP BY wo.id");
			if($offer_live->num_rows() > 0){
				foreach($offer_live->result() as $offers){
					$data['resultCode'] = '1';
					$data['resultMessage'] = "Success";
					$data['brand_name'] = $offers->brand_name;
					$promotion_type = 'image';
					if($offers->promotion_type == 2){
						$promotion_type = "video";
					}
					$data['rewradtype'] = $promotion_type;
					$data['withoffer'] = $offers->including_offer;
					//offer image
					if($offers->image_small != ''){
						$deal_small_path = WIFI_IMAGEPATH."flash/small/".$offers->image_small;
					}
					$data['offers_small_image'] = $deal_small_path;
					$deal_original_path = '';
					if($offers->image_original != ''){
						$deal_original_path = WIFI_IMAGEPATH."flash/original/".$offers->image_original;
					}
					$data['offers_original_image'] = $deal_original_path;
					$data['promotion_name'] = $offers->promotion_name;
					$data['promotion_desc'] = $offers->promotion_description;
					$data['video_url'] = $offers->video_path;
					$data['offer_id'] = $offers->id;
					$data['location_name'] = $offers->location_name;
					$data['location_address'] = $offers->geo_address;
					$data['phone'] = $offers->mobile_number;
					$voucher_expiry = date_create($offers->voucher_expiry);
					$data['voucher_expiry'] = date_format($voucher_expiry,"d/m/Y h:iA");
					//captive portal analytics
					$macid = '0';
					if(isset($jsondata->macid)){
						$macid = $jsondata->macid;
					}
					$today_date = date('Y-m-d');
					//check macid entry already exist or not
					$check_id = $this->DB2->query("select cpwoid from channel_wifioffer_daily_captiveportal_analysis_log
				WHERE location_id = '$locationid' AND offer_id = '$offer_id' AND macid = '$macid' AND DATE(visit_date) = '$today_date'");
					if($check_id->num_rows() > 0){
						$row = $check_id->row_array();
						$temp_id = $row['cpwoid'];
						$update_cp = $this->DB2->query("update channel_wifioffer_daily_captiveportal_analysis_log set
					click_counter = (click_counter+1) WHERE cpwoid = '$temp_id'");
					}
					else{
						$insert_cp = $this->DB2->query("insert into channel_wifioffer_daily_captiveportal_analysis_log
				(location_id, offer_id, visit_date, macid, click_counter) VALUES ('$locationid',
				'$offer_id', now(), '$macid', '1')");
					}
				}
			}
			else{
				$data['resultCode'] = '0';
				$data['resultMessage'] = 'Invalide offerId';
			}

		}
		//echo "<pre>";print_r($data);die;
		return $data;
	}
	
	public function wifi_user_otp($jsondata){
		$data = array();
		$macid = '';
		if(isset($jsondata->macid)){
		$macid = $jsondata->macid;
		}
		$mobile = '';
		if(isset($jsondata->mobile)){
		$mobile = $jsondata->mobile;
		}
		$otp = '';
		if(isset($jsondata->otp)){
		$otp = $jsondata->otp;
		}
		$locationid = '';
		if(isset($jsondata->locationid)){
		$locationid = $jsondata->locationid;
		}
		
		$query = $this->DB2->query("insert into channel_wifi_user_otp(location_id,macid, mobile, otp, is_verified, added_on)
					  values('$locationid','$macid', '$mobile', '$otp', '0', now())");
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}
	
	public function wifi_user_otp_verify($jsondata){
		$data = array();
		$mobile = '';
		if(isset($jsondata->mobile)){
		$mobile = $jsondata->mobile;	
		}
		$otp = '';
		if(isset($jsondata->otp)){
		$otp = $jsondata->otp;
		}
		
		$query = $this->DB2->query("update channel_wifi_user_otp set is_verified = '1' where mobile = '$mobile' AND
					  otp = '$otp' order by id DESC LIMIT 1");
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}
	
	public function wifi_user_new_registration($jsondata){
		$data = array();
		$mobile = '';
		if(isset($jsondata->mobile)){
		$mobile = $jsondata->mobile;	
		}
		
		$query = $this->DB2->query("update channel_wifi_user_otp set new_user = '1' where mobile = '$mobile'
					 order by id DESC LIMIT 1");
		$data['resultCode'] = '1';
		$data['resultMessage'] = 'Success';
		return $data;
	}

	public function check_user_last_seen_wifioffer($jsondata){
		$data = array();
		$userid = '';
		if(isset($jsondata->userid)){
			$userid = $jsondata->userid;
		}
		$query = $this->DB2->query("select wos.wifioffer_id, wo.brand_id from wifi_offer_sceen as wos inner join
		wifi_offer as wo on (wos.wifioffer_id = wo.id) WHERE wos.userid = '$userid' ORDER BY wos.id desc limit 1");

		if($query->num_rows() > 0){
			$data['resultCode'] = '1';
			$data['resultMessage'] = 'success';
			foreach($query->result() as $row){
				$data['brandid'] = $row->brand_id;
			}
		}else{
			$data['resultCode'] = '0';
			$data['resultMessage'] = 'No record found';
		}


		return $data;
	}
}

?>
