<?php

class Naswifi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
    }

    public function listing_nas($jsondata) {
        //$condarr=array('status'=>1,"is_deleted"=>0);
       
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
         // $this->DB2->order_by("id","desc");
         // $condarr=array("isp_uid"=>$isp_uid);
         // $query = $this->DB2->get_where(SHTNAS,$condarr);
          $query=$this->DB2->query("select na.*,nt.nasname as nastypname from nas na inner join nas_type nt on(na.nastype=nt.id) where na.isp_uid='".$isp_uid."' order by na.id desc");
        //  echo $this->db->last_query(); die;
           return $query->result();
    }
    
      public function viewall_search_results($jsondata) {
          $postdata=$jsondata->postdata;
        $data = array();
        $gen = '';
        $total_search = 0;
        
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" isp_uid='".$isp_uid."'";
         $where ="$ispcond";
        $search_user = $postdata->search_user;
        $where.= "and ((nasname LIKE '%" . $search_user . "%') OR (shortname LIKE '%" . $search_user . "%') ) ";
        $user_state =$postdata->state;
        $user_city = $postdata->city;
        $user_zone = $postdata->zone;
        

        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(nasstate='" . $user_state . "' and nascity='" . $user_city . "' and naszone='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(nasstate='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(nasstate='" . $user_state . "' and nascity='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $where .= "and ($cond1)";
        }

        if (isset($search_user) && $search_user != '') {
            $stateidarr = array();
            $cityidarr = array();
            $zoneidarr = array();
            $condarr = array();
            $stquery = $this->db->query("select id from sht_states where state like '%" . $search_user . "%'");
            if ($stquery->num_rows() > 0) {
                foreach ($stquery->result() as $val) {
                    $stateidarr[] = $val->id;
                }
                $sid = '"' . implode('", "', $stateidarr) . '"';
                $condarr[] = "(nasstate in ({$sid}))";
            }

            $ctquery = $this->db->query("select city_id from sht_cities where city_name like '%" . $search_user . "%'");
            if ($ctquery->num_rows() > 0) {
                foreach ($ctquery->result() as $val) {
                    $cityidarr[] = $val->city_id;
                }
                $cid = '"' . implode('", "', $cityidarr) . '"';
                $condarr[] = "(nascity in ({$cid}))";
            }

            $znquery = $this->db->query("select id from sht_zones where zone_name like '%" . $search_user . "%'");
            if ($znquery->num_rows() > 0) {
                foreach ($znquery->result() as $val) {
                    $zoneidarr[] = $val->id;
                }
                $zid = '"' . implode('", "', $zoneidarr) . '"';
                $condarr[] = "(naszone in ({$zid}))";
            }

            $cond = implode("OR", $condarr);
            if ($cond != '') {
                $cond = "OR ({$cond})";
            }
            $where .= $cond;
        }

        $this->DB2->select('*');
        $this->DB2->from(SHTNAS);
        $this->DB2->where($where);
        $query = $this->DB2->get();
      //  echo $this->db->last_query(); die;
        $total_search = $query->num_rows();
        if ($total_search > 0) {
            $i = 1;
            foreach ($query->result() as $sobj) {
                $state = $this->planpublicwifi_model->getstatename($sobj->nasstate);
                $city = $this->planpublicwifi_model->getcityname($sobj->nasstate, $sobj->nascity);
                $zone = $this->planpublicwifi_model->getzonename($sobj->naszone);
                //$status = ($sobj->status == 1)?'active':'inactive';
                $nastype = ($sobj->nastype == 1) ? "Microtik" : "Others";
                $up = $this->ping($sobj->nasname);
                if ($up == 0) {

                    $nsimg = '<img src="' . base_url() . 'assets/images/green.png" > ';
                } else {
                    $nsimg = '<img src="' . base_url() . 'assets/images/red.png" >';
                }


                $gen .= '
				<tr>
					<td>' . $i . '.</td>
					<td>' . $sobj->shortname . '</td>
					<td>' . $nastype . '</td>
					<td>' . $sobj->nasname . '</td>
					<td>' . $state . '</td>
					<td>' . $city . '</td>
					<td>' . $zone . '</td>
                                            <td>' . $nsimg . '</td>
					
					<td><a href="javascript:void(0);" class="editnas" rel="' . $sobj->id . '">EDIT</a></td>
					
				</tr>
				';
                $i++;
            }
        }

        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;

        return $data;
    }
    
    public function nas_type($jsondata)
    {
        $query=$this->DB2->query("select * from nas_type where status='1'");
         return $query->result();
    }


    public function add_nas_data($jsondata) {
       // echo "<pre>"; print_R($jsondata); die;

        $postdata = $jsondata->postdata;
      
         $isp_uid = $jsondata->isp_uid;
        //  echo "<pre>"; print_R($postdata); die;
        $nasfqdn = (isset($postdata->fqdn) && $postdata->fqdn != '' ) ? $postdata->fqdn: "";
        // $nasip1=(isset($postdata['ip1']) && $postdata['ip1']!=''  )?$postdata['ip1'].".".$postdata['ip2'].".".$postdata['ip3'].".".$postdata['ip4']:"";
        $nasip = (isset($postdata->ip)) ? $postdata->ip : '';
        $tabledata = array('nasname' => $nasip,"isp_uid"=>$isp_uid, "shortname" => $postdata->nas_name, 'type' => 0, 'ports' => 0, 'secret' => $postdata->nas_secret, 'nastype' => $postdata->nastype, 'nasstate' => '', 'nascity' => '', 'naszone' => '', 'created_on' => date("Y-m-d H:i:s"));
        // $tabledata=array('nasipformat'=>$postdata['nasipformat'],"nasfqdn"=>$nasfqdn,'nasipaddr'=>$nasip,'nasname'=>$postdata['nas_name'],'nastype'=>$postdata['nastype'],'nassecret'=>$postdata['nas_secret'], 'nasstate'=>$postdata['state'],'nascity'=>$postdata['city'],'naszone'=>$postdata['zone'],'created_on'=>date("Y-m-d H:i:s"));
        if (isset($postdata->nasid) && $postdata->nasid != '') {
            $this->DB2->update(SHTNAS, $tabledata, array('id' => $postdata->nasid));
           // $this->nas_plan_association($jsondata, $postdata->nasid);
            return $postdata->nasid;
        } else {
            $this->DB2->insert(SHTNAS, $tabledata);
            $nasid = $this->DB2->insert_id();
           // $this->nas_plan_association($jsondata, $nasid);
            return $nasid;
        }
    }
    
      public function checknas_exist($jsondata) {
        $postdata = $jsondata->postdata;
        $nasname = $postdata->nasname;
        if (isset($postdata->nasid) && $postdata->nasid != '') {
            $this->DB2->where('id !=', $postdata->nasid);
        }


        $this->DB2->where('shortname', $nasname);
        $query = $this->DB2->get(SHTNAS);
        //  echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
      public function checknasip_exist($jsondata) {
        $postdata = $jsondata->postdata;
        $nasip = $postdata->nasip;
        if (isset($postdata->nasid) && $postdata->nasid != '') {
            $this->DB2->where('id !=', $postdata->nasid);
        }
        $this->DB2->where('nasname', $nasip);
        //$condarr=array('nasname'=>$nasip);
        $query = $this->DB2->get(SHTNAS);
        //  echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

  
    // public function 

    public function delete_nas() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTNAS, $tabledata, array('nasid' => $postdata['nasid']));
        return $postdata['nasid'];
    }

    public function edit_nas($jsondata) {
        $postdata=$jsondata->postdata;
        $nasdata=$this->nas_type($jsondata);
        $id = $postdata->nasid;
        $condarr = array('id' => $id);
        $disable = "";
        $query = $this->DB2->get_where(SHTNAS, $condarr);
        $rowarr = $query->row_array();
        $gen = "";
        // $checkst=($rowarr['nasipformat']=="Static")?"checked":"";
        //$chekdns=($rowarr['nasipformat']=="DDNS")?"checked":"";
        // $fqdn=$rowarr['nasfqdn'];
        // $fqdndis=($rowarr['nasipformat']=="Static")?"disabled":"";
        // $ipdis=($rowarr['nasipformat']=="DDNS")?"disabled":"";
        $gen .= ' <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">
               <strong>Edit Nas</strong>
            </h4>
         </div>
         <div class="modal-body">
            <form action="" method="post" id="edit_nas" autocomplete="off" onsubmit="edit_nas(); return false;">
            <input type="hidden" name="nasid" id="editnasid" value="' . $rowarr['id'] . '">
                <div class="row"><div id="ip1"></div></div>
               <div class="row">
                  
                    <input type="hidden" name="mobileno" value="9873834499">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <label>IP Address<sup>*</sup></label>
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nomargin-right">';

        $iparr = explode(".", $rowarr['nasname']);

        $selother = ($rowarr['nastype'] == "0") ? "selected" : "";
        $selmicro = ($rowarr['nastype'] == "1") ? "selected" : "";
        $ip1 = $iparr[0];
        $ip2 = $iparr[1];
        $ip3 = $iparr[2];
        $ip4 = $iparr[3];
        $state = $this->state_list($rowarr['nasstate'],$jsondata);


        $gen .= '<div class="mui-textfield mui-textfield--float-label" style="padding-right:10px">
                                      
                                           <input type="text"  id="editip" name="ip" value="' . $rowarr['nasname'] . '" required>
                                      
                                    </div>
                                     <span class="errorip" style="color:red;">
                                 </div>
                              
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" name="nas_name" ' . $disable . ' id="editnasname" value="' . $rowarr['shortname'] . '" required>
                              <label>Nas Name</label>
                           </div>
                            <span class="errornas" style="color:red;"></span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-select">
                              <select name="nastype" ' . $disable . ' required>';
                              foreach($nasdata as $val)
                              {
                                $selected=($val->id==$rowarr['nastype'])?"selected":"";
                                 $gen .= '<option value="'.$val->id.'" ' . $selected . '>'.$val->nasname.'</option>';
                              }
                               
                               
                 $gen.=             '</select>
                              <label>NAS Type<sup>*</sup></label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                           <div class="mui-textfield mui-textfield--float-label">
                              <input type="text" ' . $disable . ' name="nas_secret" id="editnas_secret" value="' . $rowarr['secret'] . '" required>
                              <label>Nas Secret</label>
                           </div>
                           <span class="errornassecret" style="color:red;"></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="state" ' . $disable . ' required class="statelist form-control">
                                 <option value="all">All States</option>
                               ' . $state . '
                            </select>
                              <input type="hidden" class="statesel" name="statesel" value="' . $rowarr['nasstate'] . '">
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select name="city" ' . $disable . ' class="search_citylist form-control" required >
                                 ';
        $gen .= $this->getcitylist($rowarr['nasstate'], $rowarr['nascity'],$jsondata);
        $gen .= '    </select><input type="hidden" class="citysel" name="citysel" value="' . $rowarr['nascity'] . '">
                            
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                           <div class="">
                              <select class="zone_list form-control" name="zone" ' . $disable . ' required>
                               ';
        $gen .= $this->zone_list($rowarr['nascity'], $rowarr['naszone'], $rowarr['nasstate'],$jsondata);
        $gen .= ' </select>
                              
                           </div>
                        </div>
                     </div>-->
                  </div>
                  <div class="form-group">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="modal-footer">
                           <button type="button" class="mui-btn mui-btn--large mui-btn--primary mui-btn--flat" data-dismiss="modal" style="background-color:#4d4d4d; color:#fff ">CANCEL</button>
                           <input type="submit" ' . $disable . ' class="mui-btn mui-btn--large mui-btn--accent" value="ADD NAS">
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <!-- /.modal-content -->
      </div>
   </div>';
        $data['html'] = $gen;
        $data['nasip'] = $rowarr['nasname'];
        return $data;
        //  echo "<pre>"; print_R($rowarr); die;
    }

    public function state_list($stateid = '',$jsondata) {
        
        $superadmin = $jsondata->super_admin;
        $dept_id =  $jsondata->dept_id;
        $isp_uid= $jsondata->isp_uid;
        $regiontype = 'region';
        if ( $superadmin == 1) {
             $query = $this->db->query("select state from sht_isp_admin_region where isp_uid='" . $isp_uid . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select state_id from sht_dept_region where dept_id='" . $dept_id . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state_id;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        }
        return $gen;
    }

    public function zone_list($cityid, $zoneid = '', $stateid = '',$jsondata) {

   $superadmin = $jsondata->super_admin;
        $dept_id =  $jsondata->dept_id;
        $isp_uid= $jsondata->isp_uid;
        $regiontype = 'region';
       
        // $stateid=$this->input->post('stateid');
        $allzone = '<option value="all">All Zone</option>';
        $gen = '';
        if ($superadmin == 1) {
            $zoneQ = $this->db->query("SELECT * FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            $gen .= $allzone;
            $num_rows = $zoneQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allzone;
                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            }
        } else {

            $query = $this->db->query("select zone_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $zonearr = array();
            foreach ($query->result() as $val) {
                $zonearr[] = $val->zone_id;
            }
            //  echo "<pre>"; print_R($zonearr);
            if (in_array('all', $zonearr)) {
                //   echo "sssss"; die;
                $allzone = '<option value="all">All Zone</option>';
                 $zoneQ = $this->db->query("SELECT * FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            } else {
                $allzone = '<option value="">Select Zone</option>';
                $zid = '"' . implode('", "', $zonearr) . '"';
                $zoneQ = $this->db->query("select * from sht_zones where id IN ({$zid})");
            }
            $num_rows = $zoneQ->num_rows();
            $gen .= $allzone;
            if ($num_rows > 0) {

                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            }
        }
        return $gen;
    }
    
      public function add_citydata($jsondata){
             $postdata=$jsondata->postdata;
         $isp_uid = $jsondata->isp_uid;
		$city_name = $postdata->city_add;
		$state_id = $postdata->add_city_state;
                 $tabledata=array("city_name"=>$city_name,"city_state_id"=>$state_id,"isp_uid"=>$isp_uid);
		$this->db->insert("sht_cities",$tabledata);
               
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$city_name."</option>";
                $data['state_id']=$state_id;
		return $data;
	}
        
         public function add_zonedata($jsondata){
           $postdata=$jsondata->postdata;
         $isp_uid = $jsondata->isp_uid;
		$zone_name = $postdata->zone_name;
		$city_id = $postdata->add_zone_city;
                $tabledata=array("zone_name"=>$zone_name,"city_id"=>$city_id,"isp_uid"=>$isp_uid);
		$this->db->insert("sht_zones",$tabledata);
                $id = $this->db->insert_id();
                $data=array();
                $data['html']="<option value='".$id."' selected>".$zone_name."</option>";
                $data['city_id']=$city_id;
             //   $this->db->last_query(); die;
		return $data;
	}

    public function getcitylist($stateid, $cityid = '',$jsondata) {
         $superadmin = $jsondata->super_admin;
        $dept_id =  $jsondata->dept_id;
        $isp_uid= $jsondata->isp_uid;
        $regiontype = 'region';
        $allcity = '<option value="all">All Cities</option>';
        $gen = '';
        if ($regiontype == "allindia" || $superadmin == 1) {


          $cityQ = $this->db->query("SELECT * FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
            $num_rows = $cityQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allcity;
                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select city_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $cityarr = array();
            foreach ($query->result() as $val) {
                $cityarr[] = $val->city_id;
            }

            if (in_array('all', $cityarr)) {

                $allcity = '<option value="all">All Cities</option>';
               $cityQ = $this->db->query("SELECT * FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
            } else {

                $allcity = '<option value="">Select City</option>';
                $cid = '"' . implode('", "', $cityarr) . '"';
                $cityQ = $this->db->query("select * from sht_cities where city_id IN ({$cid})");
            }
            //  $this->db->last_query(); die;
            $num_rows = $cityQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allcity;
                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            }
        }
        return $gen;
    }

  

  

    public function ping($jsondata) {
        $host=$jsondata->host;
        exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
        return $rval;
    }

    public function nas_plan_association($jsondata, $nasid) {
        $postdata= $jsondata->postdata;
         $condarr = array();
        
         $isp_uid = $jsondata->isp_uid;
         $sczcond='';
       $permicond='';
                    $stateid = $postdata->state;
                    $cityid = $postdata->city;
                    $zoneid = $postdata->zone;
                    if ($cityid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state_id='" . $stateid . "' AND city_id='" . $cityid . "' AND zone_id='" . $zoneid . "') ";
                    }
                   
                
            
            if($permicond!='')
            {
            $sczcond .= 'and (' . $permicond . ')';
            }
              $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
        
        
      


       

        $paramarr = array();
        $query = $this->DB2->query("SELECT distinct(plan_id) FROM sht_plan_region WHERE status='1' $sczcond  AND is_deleted='0' {$ispcond}");

        foreach ($query->result() as $val) {
            $paramarr[] = $val->plan_id;
        }

        $query2 = $this->DB2->query("select srvid from sht_plan_pricing where region_type='allindia'");
        $paramarr1 = array();
        if ($query2->num_rows() > 0) {
            foreach ($query2->result() as $val2) {
                $paramarr1[] = $val2->srvid;
            }
        }

        $paramarr = array_unique(array_merge($paramarr, $paramarr1));
        foreach ($paramarr as $pval) {
            $query1 = $this->DB2->query("select srvid from sht_plannasassoc where srvid='" . $pval . "' and nasid='" . $nasid . "'");
            if ($query1->num_rows() == 0) {
                $tabledata = array('srvid' => $pval, 'nasid' => $nasid);
                $this->DB2->insert('sht_plannasassoc', $tabledata);
            }
        }
    }
    
    
    public function add_nas_data_dynamic($jsondata) {
        $isp_uid = $jsondata->isp_uid;
        $name = '10005';//(incremental) 
        $nasip = '172.16.10.5';//(incremental)
        $check_previous_dynamic = $this->DB2->query("select * from nas_dynamic_record ORDER BY id DESC");
        if($check_previous_dynamic->num_rows() > 0){
            $row = $check_previous_dynamic->row_array();
            $name = $row['name'];
            $name = $name+1;
            $Ip = $row['dynamic_ip'];
            $Ip =  explode('.',$Ip);
            
            if($Ip[3] == '255'){
                    $Ip[2]++;
                    $Ip[3] = 0;
            }
            else{
                    $Ip[3]++;
            }
            $new_ip = implode('.',$Ip);
            $Ip1 =  explode('.',$new_ip);
            $nasip = implode('.',$Ip1);
        }
        $shortname = 'DynamicNas_'.$name;
        $secret = 'testing123';
        $nastype = '1';
        $this->load->library('routerlib');
        $router_id = '103.20.214.162';$router_user = 'shouutadmin';$router_password = 'shouut@123';
        $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
        $ip_service_www = $conn->add("/ppp/secret",array("comment"=>$shortname,"name"=>$name, 'password' => "shouutdynamic", 'remote-address' => $nasip, 'service' => 'sstp', "disabled"=>"no"));
        $tabledata = array('is_dynamic'=> '1','nasname' => $nasip,"isp_uid"=>$isp_uid, "shortname" => $shortname, 'type' => 0, 'ports' => 0, 'secret' => $secret, 'nastype' => $nastype, 'nasstate' => '', 'nascity' => '', 'naszone' => '', 'created_on' => date("Y-m-d H:i:s"));
        
        $this->DB2->insert(SHTNAS, $tabledata);
        $nasid = $this->DB2->insert_id();
        // insert for future record
        $this->DB2->query("insert into nas_dynamic_record(name, dynamic_ip, created_on) values('$name','$nasip',now())");
        $command_one = "/interface sstp-client add name=shouut-vpn connect-to=103.20.214.162 disabled=no user=".$name. " password=shouutdynamic add-default-route=no profile=default authentication=pap,chap";
        $command_two = "/ip route add dst-address=172.16.20.0/22 gateway=172.16.254.0";
        $command_three = "/ip route add dst-address=172.16.25.0/24 gateway=172.16.254.0";
        $data = array();
        $data['resultCode'] = '1';
        $data['nasid'] = $nasid;
        $data['nasip'] = $nasip;
        $data['shortname'] = $shortname;
        $data['secret'] = $secret;
        $data['command_one'] = $command_one;
        $data['command_two'] = $command_two;
        $data['command_three'] = $command_three;
        return $data;
    }
	
	public function add_apmac_country($jsondata)
	{
		//echo "<pre>"; print_R($jsondata); die;
		$data=array();
		if(!isset($jsondata->macId)|| $jsondata->macId=="")
		{
			$data['resultCode']=0;
			$data['resultMsg']="Please Enter Mac ID";
			return $data;
		}
		if(!isset($jsondata->country_id) || $jsondata->country_id=="")
		{
			$data['resultCode']=0;
			$data['resultMsg']="Please Enter Country ID";
			return $data;
		}
		if(!isset($jsondata->added_on) || $jsondata->added_on=="")
		{
			$data['resultCode']=0;
			$data['resultMsg']="Please Enter Time of Adding mac";
			return $data;
		}
		if($jsondata->country_id==157){
			$timezone="Pacific/Auckland";
			
		}else if($jsondata->country_id==38)
		{
			$timezone="America/Toronto";
		}else if($jsondata->country_id==182){
			$timezone="Africa/Kigali";
		}else{
			$timezone="Asia/Kolkata";
		}
		
		
		$tabledata=array("macid"=>$jsondata->macId,"country_id"=>$jsondata->country_id,"added_on"=>$jsondata->added_on,"is_deleted"=>0,"timezone"=>$timezone);
		$this->DB2->insert("offline_apmac_country",$tabledata);
		if($this->DB2->affected_rows() > 0)
		{
			$data['resultCode']=1;
			$data['resultMsg']="Success";
			return $data;
		}
		else{
			$data['resultCode']=0;
			$data['resultMsg']="Unable to add MAC";
			return $data;
			
		}
		
	}

}

?>