<?php
class Locationportal_model extends CI_Model{
     private $DB2;
     public function __construct(){
	 $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
     }
     
     public function log($function, $request, $responce) {
		$log_data = array(
			'function_name' => $function,
			'request' => $request,
			'responce' => $responce,
			'time' => date("d-m-Y H:i:s"),
		);
		//$this->mongo_db->insert('log_data_new', $log_data);
    }
     public function login($jsondata){
	  $data = array();
	  $locationinfo = array();
	  $user_email = $jsondata->user_email;
	  $password = $jsondata->password;
	  $location_uid = '';
	  $isp_uid = '';
	  $query = $this->DB2->query("select id,isp_uid, location_uid, location_type, location_name, provider_id, geo_address, contact_person_name, mobile_number, user_email, password,pwd_updated from wifi_location where user_email = '$user_email' and location_uid = '$password'");
	  if($query->num_rows() > 0){// first time login
	       $data['resultCode'] = '1';
	       $data['resultMsg'] = 'Success';
	       $row = $query->row_array();
	       $location_uid = $row['location_uid'];
	       $isp_uid = $row['isp_uid'];
	       $locationinfo['locationid'] = $row['id'];
	       $locationinfo['location_uid'] = $row['location_uid'];
	       $locationinfo['location_type'] = $row['location_type'];
	       $locationinfo['location_name'] = $row['location_name'];
	       $locationinfo['provider_id'] = $row['provider_id'];
	       $locationinfo['geo_address'] = $row['geo_address'];
	       $locationinfo['contact_person_name'] = $row['contact_person_name'];
	       $locationinfo['mobile_number'] = $row['mobile_number'];
	       $data['locationinfo'] = $locationinfo;
	  }else{
	       $query1 = $this->DB2->query("select id,isp_uid, location_uid, location_type, location_name, provider_id, geo_address, contact_person_name, mobile_number, user_email, password,pwd_updated from wifi_location where user_email = '$user_email' and password = '$password'");
	       if($query1->num_rows() > 0){
		    $data['resultCode'] = '1';
		    $data['resultMsg'] = 'Success';
		    $row1 = $query1->row_array();
		    $location_uid = $row1['location_uid'];
		    $isp_uid = $row1['isp_uid'];
		    $locationinfo['locationid'] = $row1['id'];
		    $locationinfo['location_uid'] = $row1['location_uid'];
		    $locationinfo['location_type'] = $row1['location_type'];
		    $locationinfo['location_name'] = $row1['location_name'];
		    $locationinfo['provider_id'] = $row1['provider_id'];
		    $locationinfo['geo_address'] = $row1['geo_address'];
		    $locationinfo['contact_person_name'] = $row1['contact_person_name'];
		    $locationinfo['mobile_number'] = $row1['mobile_number'];
		    $data['locationinfo'] = $locationinfo;
	       }else{
		    $data['resultCode'] = '0';
		    $data['resultMsg'] = 'Invalid username password';
	       }
	  }
	  if($location_uid != ''){
	       $logo_url = '';
	       $get_logo = $this->DB2->query("select original_image,logo_image from wifi_location_cptype where uid_location = '$location_uid'");
	       if($get_logo->num_rows() > 0){
		    $row_logo = $get_logo->row_array();
		    if($row_logo['logo_image'] != ''){
			 $logo_url = 'http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/logo/'.$row_logo['logo_image'];
		    }elseif($row_logo['original_image'] != ''){
			 $logo_url = 'http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/original/'.$row_logo['original_image'];
		    }
	       }
	       if($logo_url == ''){
		    $get_isp_logo = $this->db->query("select small_image,logo_image from sht_isp_detail where status='1' AND isp_uid='$isp_uid'");
		    $row_logo = $get_isp_logo->row_array();
		    if($row_logo['logo_image'] != ''){
			 $logo_url = 'http://cdn101.shouut.com/shouutmedia/isp/isp_logo/logo/'.$row_logo['logo_image'];
		    }elseif($row_logo['original_image'] != ''){
			 $logo_url = 'http://cdn101.shouut.com/shouutmedia/isp/isp_logo/original/'.$row_logo['original_image'];
		    }
	       }
	       $data['logo_url'] = $logo_url;
	  }
	  return $data;
     }
     
     public function insights($jsondata){
	  $data = array();
	  $traffic_data = array();
	  $trafic_type = explode(",",$jsondata->traffic_type);
	  $location_id = $jsondata->location_id;
	  $visitor_type = $jsondata->visitor_type;
	  $day_filter = $jsondata->day_filter;
	  if($day_filter != ''){
	      $day_filter = explode(",",$this->input->post('day_filter'));    
	  }else{
	      $day_filter = array();
	  }
	  $previous_start_date = ''; $previous_end_date = '';
	  $start_date = $jsondata->start_date;
	  $end_date = $jsondata->end_date;
	  $no_of_days = 0;
	  $datediff = strtotime($end_date) - strtotime($start_date);
	  $no_of_days = floor($datediff / (60 * 60 * 24));
	  $no_of_days = $no_of_days+1;
	  $previous_start_date = date('Y-m-d', strtotime('-'.$no_of_days.' days', strtotime($start_date))); 
	  $previous_end_date = date('Y-m-d', strtotime('-1 days', strtotime($start_date)));
	  
	  $current_insights = $this->current_traffic($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days);
	  $previous_insights = $this->previous_traffic($previous_start_date,$previous_end_date,$visitor_type,$day_filter,$location_id,$no_of_days);
	  $traffic_data['total_session'] = $current_insights['total_session'];
	  $traffic_data['total_session_avg_inc'] = $this->count_percent_increment($current_insights['total_session'],$previous_insights['total_session']);
	  $traffic_data['new_user'] = $current_insights['new_user'];
	  $traffic_data['new_user_avg_inc'] = $this->count_percent_increment($current_insights['new_user'],$previous_insights['new_user']);
	  $traffic_data['repeat_visitor'] = $current_insights['repeat_visitor'];
	  $traffic_data['repeat_visitor_avg_inc'] = $this->count_percent_increment($current_insights['repeat_visitor'],$previous_insights['repeat_visitor']);
	  $traffic_data['daily_avg'] = $current_insights['daily_avg'];
	  $traffic_data['daily_avg_avg_inc'] = $this->count_percent_increment($current_insights['daily_avg'],$previous_insights['daily_avg']);
	  $traffic_data['total_male'] = $current_insights['total_male'];
	  $traffic_data['total_male_avg_inc'] = $this->count_percent_increment($current_insights['total_male'],$previous_insights['total_male']);
	  $traffic_data['total_female'] = $current_insights['total_female'];
	  $traffic_data['total_female_avg_inc'] = $this->count_percent_increment($current_insights['total_female'],$previous_insights['total_female']);
	  $traffic_data['below18'] = $current_insights['below18'];
	  $traffic_data['below18_avg_inc'] = $this->count_percent_increment($current_insights['below18'],$previous_insights['below18']);
	  $traffic_data['user18_30'] = $current_insights['user18_30'];
	  $traffic_data['user18_30_avg_inc'] = $this->count_percent_increment($current_insights['user18_30'],$previous_insights['user18_30']);
	  $traffic_data['user31_45'] = $current_insights['user31_45'];
	  $traffic_data['user31_45_avg_inc'] = $this->count_percent_increment($current_insights['user31_45'],$previous_insights['user31_45']);
	  $traffic_data['above45'] = $current_insights['above45'];
	  $traffic_data['above45_avg_inc'] = $this->count_percent_increment($current_insights['above45'],$previous_insights['above45']);
	  $traffic_data['android'] = $current_insights['android'];
	  $traffic_data['android_avg_inc'] = $this->count_percent_increment($current_insights['android'],$previous_insights['android']);
	  $traffic_data['ios'] = $current_insights['ios'];
	  $traffic_data['ios_avg_inc'] = $this->count_percent_increment($current_insights['ios'],$previous_insights['ios']);
	  $traffic_data['laptop'] = $current_insights['laptop'];
	  $traffic_data['laptop_avg_inc'] = $this->count_percent_increment($current_insights['laptop'],$previous_insights['laptop']);
	  $traffic_data['other_device'] = $current_insights['other_device'];
	  $traffic_data['other_device_avg_inc'] = $this->count_percent_increment($current_insights['other_device'],$previous_insights['other_device']);
	  $traffic_data['morning'] = $current_insights['morning'];
	  $traffic_data['morning_avg_inc'] = $this->count_percent_increment($current_insights['morning'],$previous_insights['morning']);
	  $traffic_data['noon'] = $current_insights['noon'];
	  $traffic_data['noon_avg_inc'] = $this->count_percent_increment($current_insights['noon'],$previous_insights['noon']);
	  $traffic_data['afternoon'] = $current_insights['afternoon'];
	  $traffic_data['afternoon_avg_inc'] = $this->count_percent_increment($current_insights['afternoon'],$previous_insights['afternoon']);
	  $traffic_data['evening'] = $current_insights['evening'];
	  $traffic_data['evening_avg_inc'] = $this->count_percent_increment($current_insights['evening'],$previous_insights['evening']);
	  $data['traffic_data'] = $traffic_data;
	  if(in_array('session_time',$trafic_type)){
	      $data['sessionTime_data'] = $this->sessionTime_detail($start_date,$end_date,$previous_start_date,$previous_end_date,$no_of_days,$visitor_type,$day_filter,$location_id);
	  }
	  if(in_array('data_use',$trafic_type)){
	    $data['dataUse_data'] = $this->dataUse_detail($start_date,$end_date,$previous_start_date,$previous_end_date,$no_of_days,$visitor_type,$day_filter,$location_id);
	  }
	  $data['resultCode'] = '1';
	  $data['resultMsg'] = 'Success';
	  //echo "<pre>";print_r($data);die;
	  return $data;
     }
     public function current_traffic($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days){
	$data = array();
	$total_session = 0; $new_user = 0; $repeat_visitor = 0;$daily_avg = 0;$total_male = 0;$total_female = 0;
	$below18 = 0; $user18_30 = 0; $user31_45 = 0;$above45 = 0;$android = 0;$ios = 0;$laptop = 0;$other_device = 0;
	$morning = 0; $noon = 0; $afternoon = 0; $evening = 0;
	$previous_user = array();
	$previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$start_date'   group by userid");
	foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	}
	$query = $this->DB2->query("select *  from wifi_user_free_session   WHERE location_id = '$location_id' AND date(session_date) BETWEEN '$start_date' AND '$end_date'");
	foreach($query->result() as $row){
	    $session_time = $row->session_date;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session = $total_session+1;
		if(in_array($row->userid,$previous_user))
		{
		    $repeat_visitor = $repeat_visitor+1;
		    
		}else{
		    $new_user = $new_user+1;
		}
		$previous_user[] = $row->userid;
		if(strtolower($row->gender) == 'f' || strtolower($row->gender) == 'female'){
		    
		    $total_female = $total_female+1;
		}else{
		    $total_male = $total_male+1;
		}
		if($row->age_group == '18'){
		    $below18 = $below18+1;
		}elseif($row->age_group == '31-35'){
		    $user31_45 = $user31_45+1;
		}elseif($row->age_group == '45'){
		    $above45 = $above45+1;
		}else{
		    $user18_30 = $user18_30+1;
		}
		if($row->osinfo == 'Android Phone'){
		    $android = $android+1;
		}elseif($row->osinfo == 'Apple Phone'){
		    $ios = $ios+1;
		}elseif($row->osinfo == 'Windows Desktop'){
		    $laptop = $laptop+1;
		}else{
		    $other_device = $other_device+1;
		}
		
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning = $morning+1;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon = $noon+1;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon = $afternoon+1;
		}else{
		    $evening = $evening+1;
		}
	    }
	    
	}
	$daily_avg = ceil($total_session/$no_of_days);
	$data['total_session'] = $total_session;
	$data['new_user'] = $new_user;
	$data['repeat_visitor'] = $repeat_visitor;
	$data['daily_avg'] = $daily_avg;
	$data['total_male'] = $total_male;
	$data['total_female'] = $total_female;
	$data['below18'] = $below18;
	$data['user18_30'] = $user18_30;
	$data['user31_45'] = $user31_45;
	$data['above45'] = $above45;
	$data['android'] = $android;
	$data['ios'] = $ios;
	$data['laptop'] = $laptop;
	$data['other_device'] = $other_device;
	$data['morning'] = $morning;
	$data['noon'] = $noon;
	$data['afternoon'] = $afternoon;
	$data['evening'] = $evening;
	//echo "<pre>";print_r($data);die;
	return $data;
    }
     public function previous_traffic($previous_start_date,$previous_end_date,$visitor_type,$day_filter,$location_id,$no_of_days){
	$data = array();
	$total_session = 0; $new_user = 0; $repeat_visitor = 0;$daily_avg = 0;$total_male = 0;$total_female = 0;
	$below18 = 0; $user18_30 = 0; $user31_45 = 0;$above45 = 0;$android = 0;$ios = 0;$laptop = 0;$other_device = 0;
	$morning = 0; $noon = 0; $afternoon = 0; $evening = 0;
	$previous_user = array();
	$previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$previous_start_date'   group by userid");
	foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	}
	$query = $this->DB2->query("select *  from wifi_user_free_session   WHERE location_id = '$location_id' AND date(session_date) BETWEEN '$previous_start_date' AND '$previous_end_date'");
		foreach($query->result() as $row){
	    $session_time = $row->session_date;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session = $total_session+1;
		if(in_array($row->userid,$previous_user))
		{
		    $repeat_visitor = $repeat_visitor+1;
		    
		}else{
		    $new_user = $new_user+1;
		}
		$previous_user[] = $row->userid;
		if(strtolower($row->gender) == 'f' || strtolower($row->gender) == 'female'){
		    
		    $total_female = $total_female+1;
		}else{
		    $total_male = $total_male+1;
		}
		if($row->age_group == '18'){
		    $below18 = $below18+1;
		}elseif($row->age_group == '31-35'){
		    $user31_45 = $user31_45+1;
		}elseif($row->age_group == '45'){
		    $above45 = $above45+1;
		}else{
		    $user18_30 = $user18_30+1;
		}
		if($row->osinfo == 'Android Phone'){
		    $android = $android+1;
		}elseif($row->osinfo == 'Apple Phone'){
		    $ios = $ios+1;
		}elseif($row->osinfo == 'Windows Desktop'){
		    $laptop = $laptop+1;
		}else{
		    $other_device = $other_device+1;
		}
		
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning = $morning+1;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon = $noon+1;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon = $afternoon+1;
		}else{
		    $evening = $evening+1;
		}
	    }
	    
	}

	$daily_avg = ceil($total_session/$no_of_days);
	$data['total_session'] = $total_session;
	$data['new_user'] = $new_user;
	$data['repeat_visitor'] = $repeat_visitor;
	$data['daily_avg'] = $daily_avg;
	$data['total_male'] = $total_male;
	$data['total_female'] = $total_female;
	$data['below18'] = $below18;
	$data['user18_30'] = $user18_30;
	$data['user31_45'] = $user31_45;
	$data['above45'] = $above45;
	$data['android'] = $android;
	$data['ios'] = $ios;
	$data['laptop'] = $laptop;
	$data['other_device'] = $other_device;
	$data['morning'] = $morning;
	$data['noon'] = $noon;
	$data['afternoon'] = $afternoon;
	$data['evening'] = $evening;
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    

     public function count_percent_increment($x,$y){
	$percent = '';
	if($y > 0){
	    $diff = $x - $y ;
	    $percent = ($diff/$y)*100;
	}elseif($x == '0' && $y =='0'){
	    $percent = 0;
	}
	else{
	    $percent = 100;
	}
	
	
	return round($percent,2);
     }

     
     public function sessionTime_detail($start_date,$end_date,$previous_start_date,$previous_end_date,$no_of_days,$visitor_type,$day_filter,$location_id){
	$data = array();
	
	
	$current_insights = $this->current_sessionTime($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days);
	$previous_insights = $this->previous_sessionTime($previous_start_date,$previous_end_date,$visitor_type,$day_filter,$location_id,$no_of_days);
	$data['total_session'] = $current_insights['total_session'];
	$data['total_session_avg_inc'] = $this->count_percent_increment($current_insights['total_session'],$previous_insights['total_session']);
	$data['new_user'] = $current_insights['new_user'];
	$data['new_user_avg_inc'] = $this->count_percent_increment($current_insights['new_user'],$previous_insights['new_user']);
	$data['repeat_visitor'] = $current_insights['repeat_visitor'];
	$data['repeat_visitor_avg_inc'] = $this->count_percent_increment($current_insights['repeat_visitor'],$previous_insights['repeat_visitor']);
	$data['daily_avg'] = $current_insights['daily_avg'];
	$data['daily_avg_avg_inc'] = $this->count_percent_increment($current_insights['daily_avg'],$previous_insights['daily_avg']);
	$data['total_male'] = $current_insights['total_male'];
	$data['total_male_avg_inc'] = $this->count_percent_increment($current_insights['total_male'],$previous_insights['total_male']);
	$data['total_female'] = $current_insights['total_female'];
	$data['total_female_avg_inc'] = $this->count_percent_increment($current_insights['total_female'],$previous_insights['total_female']);
	$data['below18'] = $current_insights['below18'];
	$data['below18_avg_inc'] = $this->count_percent_increment($current_insights['below18'],$previous_insights['below18']);
	$data['user18_30'] = $current_insights['user18_30'];
	$data['user18_30_avg_inc'] = $this->count_percent_increment($current_insights['user18_30'],$previous_insights['user18_30']);
	$data['user31_45'] = $current_insights['user31_45'];
	$data['user31_45_avg_inc'] = $this->count_percent_increment($current_insights['user31_45'],$previous_insights['user31_45']);
	$data['above45'] = $current_insights['above45'];
	$data['above45_avg_inc'] = $this->count_percent_increment($current_insights['above45'],$previous_insights['above45']);
	$data['android'] = $current_insights['android'];
	$data['android_avg_inc'] = $this->count_percent_increment($current_insights['android'],$previous_insights['android']);
	$data['ios'] = $current_insights['ios'];
	$data['ios_avg_inc'] = $this->count_percent_increment($current_insights['ios'],$previous_insights['ios']);
	$data['laptop'] = $current_insights['laptop'];
	$data['laptop_avg_inc'] = $this->count_percent_increment($current_insights['laptop'],$previous_insights['laptop']);
	$data['other_device'] = $current_insights['other_device'];
	$data['other_device_avg_inc'] = $this->count_percent_increment($current_insights['other_device'],$previous_insights['other_device']);
	$data['morning'] = $current_insights['morning'];
	$data['morning_avg_inc'] = $this->count_percent_increment($current_insights['morning'],$previous_insights['morning']);
	$data['noon'] = $current_insights['noon'];
	$data['noon_avg_inc'] = $this->count_percent_increment($current_insights['noon'],$previous_insights['noon']);
	$data['afternoon'] = $current_insights['afternoon'];
	$data['afternoon_avg_inc'] = $this->count_percent_increment($current_insights['afternoon'],$previous_insights['afternoon']);
	$data['evening'] = $current_insights['evening'];
	$data['evening_avg_inc'] = $this->count_percent_increment($current_insights['evening'],$previous_insights['evening']);
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    public function current_sessionTime($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days){
	
	$data = array();
	$total_session = 0; $new_user = 0; $repeat_visitor = 0;$daily_avg = 0;$total_male = 0;$total_female = 0;
	$below18 = 0; $user18_30 = 0; $user31_45 = 0;$above45 = 0;$android = 0;$ios = 0;$laptop = 0;$other_device = 0;
	$morning = 0; $noon = 0; $afternoon = 0; $evening = 0;
	$previous_user = array();
	$previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$start_date'   group by userid");
	foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	}
	
	$where_con = '';
	$location_check = $this->DB2->query("select wl.location_uid,wl.nasipaddress,ns.nastype from wifi_location as wl left join nas as ns on(wl.nasid = ns.id) where wl.id = '$location_id'");
	if($location_check->num_rows() > 0){
	    foreach($location_check->result() as $row){
		if($row->nastype == '6'){
		    $onehop_macid = array();
		    $onehop_location_uid = $row->location_uid;
		    $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid= '$onehop_location_uid'");
		    foreach($get_macid->result() as $row_mac){
			$onehop_macid[] = $row_mac->macid;
		    }
		    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		    $where_con = " AND calledstationid IN($onehop_macid) ";
		}else{
		    $nasip = $row->nasipaddress;
		    $where_con = " AND nasipaddress = '$nasip'";    
		}
	    }
	}
	
	$user_deviceinfo = array();
	$user_info = $this->DB2->query("select session_date,userid,gender, osinfo, age_group from wifi_user_free_session where location_id = '$location_id' and date(session_date) BETWEEN '$start_date' AND '$end_date'");
	$i=0;
	$previous_user_temp = $previous_user;
	$total_session_i = 0;$new_user_i = 0;$repeat_visitor_i = 0;$total_female_i = 0;$total_male_i = 0;
	$below18_i = 0;$user31_45_i = 0;$above45_i = 0;$user18_30_i = 0;
	$android_i = 0; $ios_i = 0; $laptop_i = 0; $other_device_i = 0;
	$morning_i = 0;$noon_i = 0;$afternoon_i = 0;$evening_i = 0;
	foreach($user_info->result() as $datav){
	   // echo "<pre>"; print_R($data);
	    $user_deviceinfo[$datav->userid][$i]= $datav;
	    $i++;
	    
	    $session_time = $datav->session_date;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session_i = $total_session_i+1;
		if(in_array($datav->userid,$previous_user_temp))
		{
		    $repeat_visitor_i = $repeat_visitor_i+1;
		    
		}else{
		    $new_user_i = $new_user_i+1;
		}
		$previous_user_temp[] = $datav->userid;
		if(strtolower($datav->gender) == 'f' || strtolower($datav->gender) == 'female'){
		    
		    $total_female_i = $total_female_i+1;
		}else{
		    $total_male_i = $total_male_i+1;
		}
		if($datav->age_group == '18'){
		    $below18_i = $below18_i+1;
		}elseif($datav->age_group == '31-35'){
		    $user31_45_i = $user31_45_i+1;
		}elseif($datav->age_group == '45'){
		    $above45_i = $above45_i+1;
		}else{
		    $user18_30_i = $user18_30_i+1;
		}
		if($datav->osinfo == 'Android Phone'){
		    $android_i = $android_i+1;
		}elseif($datav->osinfo == 'Apple Phone'){
		    $ios_i = $ios_i+1;
		}elseif($datav->osinfo == 'Windows Desktop'){
		    $laptop_i = $laptop_i+1;
		}else{
		    $other_device_i = $other_device_i+1;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning_i = $morning_i+1;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon_i = $noon_i+1;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon_i = $afternoon_i+1;
		}else{
		    $evening_i = $evening_i+1;
		}
	    }
	}
	//$query = $this->db->query("select r.username,r.acctsessiontime,r.acctstarttime,su.gender,su.age_group  from radacct as r inner join sht_users as su on (r.username = su.username)   WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	$user_detail = array();
	$userids = array();
	$query = $this->DB2->query("select r.username,r.acctsessiontime,r.acctstarttime from radacct as r    WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	foreach($query->result() as $row_ids){
	    $userids[] = $row_ids->username;
	}
	$userids='"'.implode('", "', $userids).'"';
	$get_user_detail = $this->DB2->query("select username,gender,age_group from sht_users where username in ($userids)");
	foreach($get_user_detail->result() as $row_detail){
	    $user_detail[$row_detail->username] = $row_detail;
	}
	
	
	
	foreach($query->result() as $row_data){
	    
	    $session_time = $row_data->acctstarttime;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session = $total_session+$row_data->acctsessiontime;
	    
		if(in_array($row_data->username,$previous_user))
		{
		    $repeat_visitor = $repeat_visitor+$row_data->acctsessiontime;
		}else{
		    $new_user = $new_user+$row_data->acctsessiontime;
		}
		$previous_user[] = $row_data->username;
		$gender = '';$age_group = '';
		if(isset($user_detail[$row_data->username])){
		    $gender = $user_detail[$row_data->username]->gender;
		    $age_group = $user_detail[$row_data->username]->age_group;
		}
		if(strtolower($gender) == 'f' || strtolower($gender) == 'female'){
		    
		    $total_female = $total_female+$row_data->acctsessiontime;
		}else{
		    $total_male = $total_male+$row_data->acctsessiontime;
		}
		if($age_group == '18'){
		    $below18 = $below18+$row_data->acctsessiontime;
		}elseif($age_group == '31-35'){
		    $user31_45 = $user31_45+$row_data->acctsessiontime;
		}elseif($age_group == '45'){
		    $above45 = $above45+$row_data->acctsessiontime;
		}else{
		    $user18_30 = $user18_30+$row_data->acctsessiontime;
		}
		if(array_key_exists($row_data->username,$user_deviceinfo))
		{
		    $keyarr=array_keys($user_deviceinfo[$row_data->username]);
		    if(count($keyarr)>0)
		    {
			$keyval=$keyarr[0];
			$osinfo = $user_deviceinfo[$row_data->username][$keyval]->osinfo;
			if($osinfo == 'Android Phone'){
			    $android = $android+$row_data->acctsessiontime;
			}elseif($osinfo == 'Apple Phone'){
			    $ios = $ios+$row_data->acctsessiontime;
			}elseif($osinfo == 'Windows Desktop'){
			    $laptop = $laptop+$row_data->acctsessiontime;
			}else{
			    $other_device = $other_device+$row_data->acctsessiontime;
			}
			unset($user_deviceinfo[$row_data->username][$keyval]);
		    }else{
			$android = $android+$row_data->acctsessiontime;
		    }
		}else{
		    $android = $android+$row_data->acctsessiontime;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning = $morning+$row_data->acctsessiontime;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon = $noon+$row_data->acctsessiontime;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon = $afternoon+$row_data->acctsessiontime;
		}else{
		    $evening = $evening+$row_data->acctsessiontime;
		}
	    }
	}
	//echo "<pre>";print_r($user_deviceinfo);die;
	//$total_session = ceil($total_session/60);
	
	if($total_session_i != '0'){
	    $total_session = round(($total_session/60)/$total_session_i,2);
	}else{
	  $total_session = round(($total_session/60),2);  
	}
	$daily_avg = round(($total_session/$no_of_days),2);
	$data['total_session'] = $total_session;
	$data['new_user'] = round(($new_user/60),2);
	if($new_user_i != '0'){
	    $data['new_user'] = round(($new_user/60)/$new_user_i,2);
	}
	$data['repeat_visitor'] = round(($repeat_visitor/60),2);
	if($repeat_visitor_i != '0'){
	    $data['repeat_visitor'] = round(($repeat_visitor/60)/$repeat_visitor_i,2);
	}
	$data['daily_avg'] = $daily_avg;
	$data['total_male'] = round(($total_male/60),2);
	if($total_male_i != '0'){
	    $data['total_male'] = round(($total_male/60)/$total_male_i,2);
	}
	$data['total_female'] = round(($total_female/60),2);
	if($total_female_i != '0'){
	    $data['total_female'] = round(($total_female/60)/$total_female_i,2);
	}
	$data['below18'] = round(($below18/60),2);
	if($below18_i != '0'){
	    $data['below18'] = round(($below18/60)/$below18_i,2);
	}
	$data['user18_30'] = round(($user18_30/60),2);
	if($user18_30_i != '0'){
	    $data['user18_30'] = round(($user18_30/60)/$user18_30_i,2);
	}
	$data['user31_45'] = round(($user31_45/60),2);
	if($user31_45_i != '0'){
	    $data['user31_45'] = round(($user31_45/60)/$user31_45_i,2);
	}
	$data['above45'] = round(($above45/60),2);
	if($above45_i != '0'){
	    $data['above45'] = round(($above45/60)/$above45_i,2);
	}
	$data['android'] = round(($android/60),2);
	if($android_i != '0'){
	    $data['android'] = round(($android/60)/$android_i,2);
	}
	$data['ios'] = round(($ios/60),2);
	if($ios_i != '0'){
	    $data['ios'] = round(($ios/60)/$ios_i,2);
	}
	$data['laptop'] = round(($laptop/60),2);
	if($laptop_i != '0'){
	    $data['laptop'] = round(($laptop/60)/$laptop_i,2);
	}
	$data['other_device'] = round(($other_device/60),2);
	if($other_device_i != '0'){
	    $data['other_device'] = round(($other_device/60)/$other_device_i,2);
	}
	$data['morning'] = round(($morning/60),2);
	if($morning_i != '0'){
	    $data['morning'] = round(($morning/60)/$morning_i,2);
	}
	$data['noon'] = round(($noon/60),2);
	if($noon_i != '0'){
	    $data['noon'] = round(($noon/60)/$noon_i,2);
	}
	$data['afternoon'] = round(($afternoon/60),2);
	if($afternoon_i != '0'){
	    $data['afternoon'] = round(($afternoon/60)/$afternoon_i,2);
	}
	$data['evening'] = round(($evening/60),2);
	if($evening_i != '0'){
	    $data['evening'] = round(($evening/60)/$evening_i,2);
	}
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    public function previous_sessionTime($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days){
	
	$data = array();
	$total_session = 0; $new_user = 0; $repeat_visitor = 0;$daily_avg = 0;$total_male = 0;$total_female = 0;
	$below18 = 0; $user18_30 = 0; $user31_45 = 0;$above45 = 0;$android = 0;$ios = 0;$laptop = 0;$other_device = 0;
	$morning = 0; $noon = 0; $afternoon = 0; $evening = 0;
	$previous_user = array();
	$previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$start_date'   group by userid");
	foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	}
	
	$where_con = '';
	$location_check = $this->DB2->query("select wl.location_uid,wl.nasipaddress,ns.nastype from wifi_location as wl left join nas as ns on(wl.nasid = ns.id) where wl.id = '$location_id'");
	if($location_check->num_rows() > 0){
	    foreach($location_check->result() as $row){
		if($row->nastype == '6'){
		    $onehop_macid = array();
		    $onehop_location_uid = $row->location_uid;
		    $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid= '$onehop_location_uid'");
		    foreach($get_macid->result() as $row_mac){
			$onehop_macid[] = $row_mac->macid;
		    }
		    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		    $where_con = " AND calledstationid IN($onehop_macid) ";
		}else{
		    $nasip = $row->nasipaddress;
		    $where_con = " AND nasipaddress = '$nasip'";    
		}
	    }
	}
	
	$user_deviceinfo = array();
	$user_info = $this->DB2->query("select session_date,userid,gender, osinfo, age_group from wifi_user_free_session where location_id = '$location_id' and date(session_date) BETWEEN '$start_date' AND '$end_date'");
	$i=0;
	$previous_user_temp = $previous_user;
	$total_session_i = 0;$new_user_i = 0;$repeat_visitor_i = 0;$total_female_i = 0;$total_male_i = 0;
	$below18_i = 0;$user31_45_i = 0;$above45_i = 0;$user18_30_i = 0;
	$android_i = 0; $ios_i = 0; $laptop_i = 0; $other_device_i = 0;
	$morning_i = 0;$noon_i = 0;$afternoon_i = 0;$evening_i = 0;
	foreach($user_info->result() as $datav){
	   // echo "<pre>"; print_R($data);
	    $user_deviceinfo[$datav->userid][$i]= $datav;
	    $i++;
	    
	    $session_time = $datav->session_date;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session_i = $total_session_i+1;
		if(in_array($datav->userid,$previous_user_temp))
		{
		    $repeat_visitor_i = $repeat_visitor_i+1;
		    
		}else{
		    $new_user_i = $new_user_i+1;
		}
		$previous_user_temp[] = $datav->userid;
		if(strtolower($datav->gender) == 'f' || strtolower($datav->gender) == 'female'){
		    
		    $total_female_i = $total_female_i+1;
		}else{
		    $total_male_i = $total_male_i+1;
		}
		if($datav->age_group == '18'){
		    $below18_i = $below18_i+1;
		}elseif($datav->age_group == '31-35'){
		    $user31_45_i = $user31_45_i+1;
		}elseif($datav->age_group == '45'){
		    $above45_i = $above45_i+1;
		}else{
		    $user18_30_i = $user18_30_i+1;
		}
		if($datav->osinfo == 'Android Phone'){
		    $android_i = $android_i+1;
		}elseif($datav->osinfo == 'Apple Phone'){
		    $ios_i = $ios_i+1;
		}elseif($datav->osinfo == 'Windows Desktop'){
		    $laptop_i = $laptop_i+1;
		}else{
		    $other_device_i = $other_device_i+1;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning_i = $morning_i+1;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon_i = $noon_i+1;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon_i = $afternoon_i+1;
		}else{
		    $evening_i = $evening_i+1;
		}
	    }
	}
	//$query = $this->db->query("select r.username,r.acctsessiontime,r.acctstarttime,su.gender,su.age_group  from radacct as r inner join sht_users as su on (r.username = su.username)   WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	$user_detail = array();
	$userids = array();
	$query = $this->DB2->query("select r.username,r.acctsessiontime,r.acctstarttime from radacct as r    WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	foreach($query->result() as $row_ids){
	    $userids[] = $row_ids->username;
	}
	$userids='"'.implode('", "', $userids).'"';
	$get_user_detail = $this->DB2->query("select username,gender,age_group from sht_users where username in ($userids)");
	foreach($get_user_detail->result() as $row_detail){
	    $user_detail[$row_detail->username] = $row_detail;
	}
	
	
	
	foreach($query->result() as $row_data){
	    
	    $session_time = $row_data->acctstarttime;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session = $total_session+$row_data->acctsessiontime;
	    
		if(in_array($row_data->username,$previous_user))
		{
		    $repeat_visitor = $repeat_visitor+$row_data->acctsessiontime;
		}else{
		    $new_user = $new_user+$row_data->acctsessiontime;
		}
		$previous_user[] = $row_data->username;
		$gender = '';$age_group = '';
		if(isset($user_detail[$row_data->username])){
		    $gender = $user_detail[$row_data->username]->gender;
		    $age_group = $user_detail[$row_data->username]->age_group;
		}
		if(strtolower($gender) == 'f' || strtolower($gender) == 'female'){
		    
		    $total_female = $total_female+$row_data->acctsessiontime;
		}else{
		    $total_male = $total_male+$row_data->acctsessiontime;
		}
		if($age_group == '18'){
		    $below18 = $below18+$row_data->acctsessiontime;
		}elseif($age_group == '31-35'){
		    $user31_45 = $user31_45+$row_data->acctsessiontime;
		}elseif($age_group == '45'){
		    $above45 = $above45+$row_data->acctsessiontime;
		}else{
		    $user18_30 = $user18_30+$row_data->acctsessiontime;
		}
		if(array_key_exists($row_data->username,$user_deviceinfo))
		{
		    $keyarr=array_keys($user_deviceinfo[$row_data->username]);
		    if(count($keyarr)>0)
		    {
			$keyval=$keyarr[0];
			$osinfo = $user_deviceinfo[$row_data->username][$keyval]->osinfo;
			if($osinfo == 'Android Phone'){
			    $android = $android+$row_data->acctsessiontime;
			}elseif($osinfo == 'Apple Phone'){
			    $ios = $ios+$row_data->acctsessiontime;
			}elseif($osinfo == 'Windows Desktop'){
			    $laptop = $laptop+$row_data->acctsessiontime;
			}else{
			    $other_device = $other_device+$row_data->acctsessiontime;
			}
			unset($user_deviceinfo[$row_data->username][$keyval]);
		    }else{
			$android = $android+$row_data->acctsessiontime;
		    }
		}else{
		    $android = $android+$row_data->acctsessiontime;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning = $morning+$row_data->acctsessiontime;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon = $noon+$row_data->acctsessiontime;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon = $afternoon+$row_data->acctsessiontime;
		}else{
		    $evening = $evening+$row_data->acctsessiontime;
		}
	    }
	}
	//echo "<pre>";print_r($user_deviceinfo);die;
	//$total_session = ceil($total_session/60);
	
	if($total_session_i != '0'){
	    $total_session = round(($total_session/60)/$total_session_i,2);
	}else{
	  $total_session = round(($total_session/60),2);  
	}
	$daily_avg = round(($total_session/$no_of_days),2);
	$data['total_session'] = $total_session;
	$data['new_user'] = round(($new_user/60),2);
	if($new_user_i != '0'){
	    $data['new_user'] = round(($new_user/60)/$new_user_i,2);
	}
	$data['repeat_visitor'] = round(($repeat_visitor/60),2);
	if($repeat_visitor_i != '0'){
	    $data['repeat_visitor'] = round(($repeat_visitor/60)/$repeat_visitor_i,2);
	}
	$data['daily_avg'] = $daily_avg;
	$data['total_male'] = round(($total_male/60),2);
	if($total_male_i != '0'){
	    $data['total_male'] = round(($total_male/60)/$total_male_i,2);
	}
	$data['total_female'] = round(($total_female/60),2);
	if($total_female_i != '0'){
	    $data['total_female'] = round(($total_female/60)/$total_female_i,2);
	}
	$data['below18'] = round(($below18/60),2);
	if($below18_i != '0'){
	    $data['below18'] = round(($below18/60)/$below18_i,2);
	}
	$data['user18_30'] = round(($user18_30/60),2);
	if($user18_30_i != '0'){
	    $data['user18_30'] = round(($user18_30/60)/$user18_30_i,2);
	}
	$data['user31_45'] = round(($user31_45/60),2);
	if($user31_45_i != '0'){
	    $data['user31_45'] = round(($user31_45/60)/$user31_45_i,2);
	}
	$data['above45'] = round(($above45/60),2);
	if($above45_i != '0'){
	    $data['above45'] = round(($above45/60)/$above45_i,2);
	}
	$data['android'] = round(($android/60),2);
	if($android_i != '0'){
	    $data['android'] = round(($android/60)/$android_i,2);
	}
	$data['ios'] = round(($ios/60),2);
	if($ios_i != '0'){
	    $data['ios'] = round(($ios/60)/$ios_i,2);
	}
	$data['laptop'] = round(($laptop/60),2);
	if($laptop_i != '0'){
	    $data['laptop'] = round(($laptop/60)/$laptop_i,2);
	}
	$data['other_device'] = round(($other_device/60),2);
	if($other_device_i != '0'){
	    $data['other_device'] = round(($other_device/60)/$other_device_i,2);
	}
	$data['morning'] = round(($morning/60),2);
	if($morning_i != '0'){
	    $data['morning'] = round(($morning/60)/$morning_i,2);
	}
	$data['noon'] = round(($noon/60),2);
	if($noon_i != '0'){
	    $data['noon'] = round(($noon/60)/$noon_i,2);
	}
	$data['afternoon'] = round(($afternoon/60),2);
	if($afternoon_i != '0'){
	    $data['afternoon'] = round(($afternoon/60)/$afternoon_i,2);
	}
	$data['evening'] = round(($evening/60),2);
	if($evening_i != '0'){
	    $data['evening'] = round(($evening/60)/$evening_i,2);
	}
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    

    
   

     public function dataUse_detail($start_date,$end_date,$previous_start_date,$previous_end_date,$no_of_days,$visitor_type,$day_filter,$location_id){
	$data = array();
	
	
	$current_insights = $this->current_dataUse($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days);
	$previous_insights = $this->previous_dataUse($previous_start_date,$previous_end_date,$visitor_type,$day_filter,$location_id,$no_of_days);
	$data['total_session'] = $current_insights['total_session'];
	$data['total_session_avg_inc'] = $this->count_percent_increment($current_insights['total_session'],$previous_insights['total_session']);
	$data['new_user'] = $current_insights['new_user'];
	$data['new_user_avg_inc'] = $this->count_percent_increment($current_insights['new_user'],$previous_insights['new_user']);
	$data['repeat_visitor'] = $current_insights['repeat_visitor'];
	$data['repeat_visitor_avg_inc'] = $this->count_percent_increment($current_insights['repeat_visitor'],$previous_insights['repeat_visitor']);
	$data['daily_avg'] = $current_insights['daily_avg'];
	$data['daily_avg_avg_inc'] = $this->count_percent_increment($current_insights['daily_avg'],$previous_insights['daily_avg']);
	$data['total_male'] = $current_insights['total_male'];
	$data['total_male_avg_inc'] = $this->count_percent_increment($current_insights['total_male'],$previous_insights['total_male']);
	$data['total_female'] = $current_insights['total_female'];
	$data['total_female_avg_inc'] = $this->count_percent_increment($current_insights['total_female'],$previous_insights['total_female']);
	$data['below18'] = $current_insights['below18'];
	$data['below18_avg_inc'] = $this->count_percent_increment($current_insights['below18'],$previous_insights['below18']);
	$data['user18_30'] = $current_insights['user18_30'];
	$data['user18_30_avg_inc'] = $this->count_percent_increment($current_insights['user18_30'],$previous_insights['user18_30']);
	$data['user31_45'] = $current_insights['user31_45'];
	$data['user31_45_avg_inc'] = $this->count_percent_increment($current_insights['user31_45'],$previous_insights['user31_45']);
	$data['above45'] = $current_insights['above45'];
	$data['above45_avg_inc'] = $this->count_percent_increment($current_insights['above45'],$previous_insights['above45']);
	$data['android'] = $current_insights['android'];
	$data['android_avg_inc'] = $this->count_percent_increment($current_insights['android'],$previous_insights['android']);
	$data['ios'] = $current_insights['ios'];
	$data['ios_avg_inc'] = $this->count_percent_increment($current_insights['ios'],$previous_insights['ios']);
	$data['laptop'] = $current_insights['laptop'];
	$data['laptop_avg_inc'] = $this->count_percent_increment($current_insights['laptop'],$previous_insights['laptop']);
	$data['other_device'] = $current_insights['other_device'];
	$data['other_device_avg_inc'] = $this->count_percent_increment($current_insights['other_device'],$previous_insights['other_device']);
	$data['morning'] = $current_insights['morning'];
	$data['morning_avg_inc'] = $this->count_percent_increment($current_insights['morning'],$previous_insights['morning']);
	$data['noon'] = $current_insights['noon'];
	$data['noon_avg_inc'] = $this->count_percent_increment($current_insights['noon'],$previous_insights['noon']);
	$data['afternoon'] = $current_insights['afternoon'];
	$data['afternoon_avg_inc'] = $this->count_percent_increment($current_insights['afternoon'],$previous_insights['afternoon']);
	$data['evening'] = $current_insights['evening'];
	$data['evening_avg_inc'] = $this->count_percent_increment($current_insights['evening'],$previous_insights['evening']);
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    public function current_dataUse($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days){
	
	$data = array();
	$total_session = 0; $new_user = 0; $repeat_visitor = 0;$daily_avg = 0;$total_male = 0;$total_female = 0;
	$below18 = 0; $user18_30 = 0; $user31_45 = 0;$above45 = 0;$android = 0;$ios = 0;$laptop = 0;$other_device = 0;
	$morning = 0; $noon = 0; $afternoon = 0; $evening = 0;
	$previous_user = array();
	$previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$start_date'   group by userid");
	foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	}
	
	$where_con = '';
	$location_check = $this->DB2->query("select wl.location_uid,wl.nasipaddress,ns.nastype from wifi_location as wl left join nas as ns on(wl.nasid = ns.id) where wl.id = '$location_id'");
	if($location_check->num_rows() > 0){
	    foreach($location_check->result() as $row){
		if($row->nastype == '6'){
		    $onehop_macid = array();
		    $onehop_location_uid = $row->location_uid;
		    $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid= '$onehop_location_uid'");
		    foreach($get_macid->result() as $row_mac){
			$onehop_macid[] = $row_mac->macid;
		    }
		    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		    $where_con = " AND calledstationid IN($onehop_macid) ";
		}else{
		    $nasip = $row->nasipaddress;
		    $where_con = " AND nasipaddress = '$nasip'";    
		}
	    }
	}
	
	$user_deviceinfo = array();
	$user_info = $this->DB2->query("select session_date,userid,gender, osinfo, age_group from wifi_user_free_session where location_id = '$location_id' and date(session_date) BETWEEN '$start_date' AND '$end_date'");
	$i=0;
	$previous_user_temp = $previous_user;
	$total_session_i = 0;$new_user_i = 0;$repeat_visitor_i = 0;$total_female_i = 0;$total_male_i = 0;
	$below18_i = 0;$user31_45_i = 0;$above45_i = 0;$user18_30_i = 0;
	$android_i = 0; $ios_i = 0; $laptop_i = 0; $other_device_i = 0;
	$morning_i = 0;$noon_i = 0;$afternoon_i = 0;$evening_i = 0;
	foreach($user_info->result() as $datav){
	   // echo "<pre>"; print_R($data);
	    $user_deviceinfo[$datav->userid][$i]= $datav;
	    $i++;
	    
	    $session_time = $datav->session_date;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session_i = $total_session_i+1;
		if(in_array($datav->userid,$previous_user_temp))
		{
		    $repeat_visitor_i = $repeat_visitor_i+1;
		    
		}else{
		    $new_user_i = $new_user_i+1;
		}
		$previous_user_temp[] = $datav->userid;
		if(strtolower($datav->gender) == 'f' || strtolower($datav->gender) == 'female'){
		    
		    $total_female_i = $total_female_i+1;
		}else{
		    $total_male_i = $total_male_i+1;
		}
		if($datav->age_group == '18'){
		    $below18_i = $below18_i+1;
		}elseif($datav->age_group == '31-35'){
		    $user31_45_i = $user31_45_i+1;
		}elseif($datav->age_group == '45'){
		    $above45_i = $above45_i+1;
		}else{
		    $user18_30_i = $user18_30_i+1;
		}
		if($datav->osinfo == 'Android Phone'){
		    $android_i = $android_i+1;
		}elseif($datav->osinfo == 'Apple Phone'){
		    $ios_i = $ios_i+1;
		}elseif($datav->osinfo == 'Windows Desktop'){
		    $laptop_i = $laptop_i+1;
		}else{
		    $other_device_i = $other_device_i+1;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning_i = $morning_i+1;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon_i = $noon_i+1;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon_i = $afternoon_i+1;
		}else{
		    $evening_i = $evening_i+1;
		}
	    }
	}
	//$query = $this->db->query("select r.username,r.acctsessiontime,r.acctstarttime,su.gender,su.age_group  from radacct as r inner join sht_users as su on (r.username = su.username)   WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	$user_detail = array();
	$userids = array();
	$query = $this->DB2->query("select r.username,r.acctstarttime,r.acctinputoctets,r.acctoutputoctets from radacct as r    WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	foreach($query->result() as $row_ids){
	    $userids[] = $row_ids->username;
	}
	$userids='"'.implode('", "', $userids).'"';
	$get_user_detail = $this->DB2->query("select username,gender,age_group from sht_users where username in ($userids)");
	foreach($get_user_detail->result() as $row_detail){
	    $user_detail[$row_detail->username] = $row_detail;
	}
	
	
	
	foreach($query->result() as $row_data){
	    $session_time = $row_data->acctstarttime;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session = $total_session+$row_data->acctinputoctets+$row_data->acctoutputoctets;
	    
		if(in_array($row_data->username,$previous_user))
		{
		    $repeat_visitor = $repeat_visitor+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $new_user = $new_user+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		$previous_user[] = $row_data->username;
		$gender = '';$age_group = '';
		if(isset($user_detail[$row_data->username])){
		    $gender = $user_detail[$row_data->username]->gender;
		    $age_group = $user_detail[$row_data->username]->age_group;
		}
		if(strtolower($gender) == 'f' || strtolower($gender) == 'female'){
		    
		    $total_female = $total_female+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $total_male = $total_male+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		if($age_group == '18'){
		    $below18 = $below18+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($age_group == '31-35'){
		    $user31_45 = $user31_45+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($age_group == '45'){
		    $above45 = $above45+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $user18_30 = $user18_30+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		if(array_key_exists($row_data->username,$user_deviceinfo))
		{
		    $keyarr=array_keys($user_deviceinfo[$row_data->username]);
		    if(count($keyarr)>0)
		    {
			$keyval=$keyarr[0];
			$osinfo = $user_deviceinfo[$row_data->username][$keyval]->osinfo;
			if($osinfo == 'Android Phone'){
			    $android = $android+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}elseif($osinfo == 'Apple Phone'){
			    $ios = $ios+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}elseif($osinfo == 'Windows Desktop'){
			    $laptop = $laptop+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}else{
			    $other_device = $other_device+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}
			unset($user_deviceinfo[$row_data->username][$keyval]);
		    }else{
			$android = $android+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		    }
		}else{
		    $android = $android+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning = $morning+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon = $noon+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon = $afternoon+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $evening = $evening+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
	    }
	}
	//echo "<pre>";print_r($user_deviceinfo);die;
	if($total_session_i != '0'){
	    $total_session = round(($total_session/(1024*1024*1024))/$total_session_i,2);
	}else{
	    $total_session = round(($total_session/(1024*1024*1024)),2);
	}
	
	$daily_avg =	round(($total_session/$no_of_days),2);
	$data['total_session'] = $total_session;
	$data['new_user'] = round(($new_user/(1024*1024*1024)),2);
	if($new_user_i != '0'){
	    $data['new_user'] = round(($new_user/(1024*1024*1024))/$new_user_i,2);
	}
	$data['repeat_visitor'] = round(($repeat_visitor/(1024*1024*1024)),2);
	if($repeat_visitor_i != '0'){
	    $data['repeat_visitor'] = round(($repeat_visitor/(1024*1024*1024))/$repeat_visitor_i,2);
	}
	$data['daily_avg'] = $daily_avg;
	$data['total_male'] = round(($total_male/(1024*1024*1024)),2);
	if($total_male_i != '0'){
	    $data['total_male'] = round(($total_male/(1024*1024*1024))/$total_male_i,2);
	}
	$data['total_female'] = round(($total_female/(1024*1024*1024)),2);
	if($total_female_i != '0'){
	    $data['total_female'] = round(($total_female/(1024*1024*1024))/$total_female_i,2);
	}
	$data['below18'] = round(($below18/(1024*1024*1024)),2);
	if($below18_i != '0'){
	    $data['below18'] = round(($below18/(1024*1024*1024))/$below18_i,2);
	}
	$data['user18_30'] = round(($user18_30/(1024*1024*1024)),2);
	if($user18_30_i != '0'){
	    $data['user18_30'] = round(($user18_30/(1024*1024*1024))/$user18_30_i,2);
	}
	$data['user31_45'] = round(($user31_45/(1024*1024*1024)),2);
	if($user31_45_i != '0'){
	    $data['user31_45'] = round(($user31_45/(1024*1024*1024))/$user31_45_i,2);
	}
	$data['above45'] = round(($above45/(1024*1024*1024)),2);
	if($above45_i != '0'){
	    $data['above45'] = round(($above45/(1024*1024*1024))/$above45_i,2);
	}
	$data['android'] = round(($android/(1024*1024*1024)),2);
	if($android_i != '0'){
	    $data['android'] = round(($android/(1024*1024*1024))/$android_i,2);
	}
	$data['ios'] = round(($ios/(1024*1024*1024)),2);
	if($ios_i != '0'){
	    $data['ios'] = round(($ios/(1024*1024*1024))/$ios_i,2);
	}
	$data['laptop'] = round(($laptop/(1024*1024*1024)),2);
	if($laptop_i != '0'){
	    $data['laptop'] = round(($laptop/(1024*1024*1024))/$laptop_i,2);
	}
	$data['other_device'] = round(($other_device/(1024*1024*1024)),2);
	if($other_device_i != '0'){
	    $data['other_device'] = round(($other_device/(1024*1024*1024))/$other_device_i,2);
	}
	$data['morning'] = round(($morning/(1024*1024*1024)),2);
	if($morning_i != '0'){
	    $data['morning'] = round(($morning/(1024*1024*1024))/$morning_i,2);
	}
	$data['noon'] = round(($noon/(1024*1024*1024)),2);
	if($noon_i != '0'){
	    $data['noon'] = round(($noon/(1024*1024*1024))/$noon_i,2);
	}
	$data['afternoon'] = round(($afternoon/(1024*1024*1024)),2);
	if($afternoon_i != '0'){
	    $data['afternoon'] = round(($afternoon/(1024*1024*1024))/$afternoon_i,2);
	}
	$data['evening'] = round(($evening/(1024*1024*1024)),2);
	if($evening_i != '0'){
	    $data['evening'] = round(($evening/(1024*1024*1024))/$evening_i,2);
	}
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    public function previous_dataUse($start_date,$end_date,$visitor_type,$day_filter,$location_id,$no_of_days){
	
	$data = array();
	$total_session = 0; $new_user = 0; $repeat_visitor = 0;$daily_avg = 0;$total_male = 0;$total_female = 0;
	$below18 = 0; $user18_30 = 0; $user31_45 = 0;$above45 = 0;$android = 0;$ios = 0;$laptop = 0;$other_device = 0;
	$morning = 0; $noon = 0; $afternoon = 0; $evening = 0;
	$previous_user = array();
	$previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$start_date'   group by userid");
	foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	}
	
	$where_con = '';
	$location_check = $this->DB2->query("select wl.location_uid,wl.nasipaddress,ns.nastype from wifi_location as wl left join nas as ns on(wl.nasid = ns.id) where wl.id = '$location_id'");
	if($location_check->num_rows() > 0){
	    foreach($location_check->result() as $row){
		if($row->nastype == '6'){
		    $onehop_macid = array();
		    $onehop_location_uid = $row->location_uid;
		    $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid= '$onehop_location_uid'");
		    foreach($get_macid->result() as $row_mac){
			$onehop_macid[] = $row_mac->macid;
		    }
		    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		    $where_con = " AND calledstationid IN($onehop_macid) ";
		}else{
		    $nasip = $row->nasipaddress;
		    $where_con = " AND nasipaddress = '$nasip'";    
		}
	    }
	}
	
	$user_deviceinfo = array();
	$user_info = $this->DB2->query("select session_date,userid,gender, osinfo, age_group from wifi_user_free_session where location_id = '$location_id' and date(session_date) BETWEEN '$start_date' AND '$end_date'");
	$i=0;
	$previous_user_temp = $previous_user;
	$total_session_i = 0;$new_user_i = 0;$repeat_visitor_i = 0;$total_female_i = 0;$total_male_i = 0;
	$below18_i = 0;$user31_45_i = 0;$above45_i = 0;$user18_30_i = 0;
	$android_i = 0; $ios_i = 0; $laptop_i = 0; $other_device_i = 0;
	$morning_i = 0;$noon_i = 0;$afternoon_i = 0;$evening_i = 0;
	foreach($user_info->result() as $datav){
	   // echo "<pre>"; print_R($data);
	    $user_deviceinfo[$datav->userid][$i]= $datav;
	    $i++;
	    
	    $session_time = $datav->session_date;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session_i = $total_session_i+1;
		if(in_array($datav->userid,$previous_user_temp))
		{
		    $repeat_visitor_i = $repeat_visitor_i+1;
		    
		}else{
		    $new_user_i = $new_user_i+1;
		}
		$previous_user_temp[] = $datav->userid;
		if(strtolower($datav->gender) == 'f' || strtolower($datav->gender) == 'female'){
		    
		    $total_female_i = $total_female_i+1;
		}else{
		    $total_male_i = $total_male_i+1;
		}
		if($datav->age_group == '18'){
		    $below18_i = $below18_i+1;
		}elseif($datav->age_group == '31-35'){
		    $user31_45_i = $user31_45_i+1;
		}elseif($datav->age_group == '45'){
		    $above45_i = $above45_i+1;
		}else{
		    $user18_30_i = $user18_30_i+1;
		}
		if($datav->osinfo == 'Android Phone'){
		    $android_i = $android_i+1;
		}elseif($datav->osinfo == 'Apple Phone'){
		    $ios_i = $ios_i+1;
		}elseif($datav->osinfo == 'Windows Desktop'){
		    $laptop_i = $laptop_i+1;
		}else{
		    $other_device_i = $other_device_i+1;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning_i = $morning_i+1;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon_i = $noon_i+1;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon_i = $afternoon_i+1;
		}else{
		    $evening_i = $evening_i+1;
		}
	    }
	}
	//$query = $this->db->query("select r.username,r.acctsessiontime,r.acctstarttime,su.gender,su.age_group  from radacct as r inner join sht_users as su on (r.username = su.username)   WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	$user_detail = array();
	$userids = array();
	$query = $this->DB2->query("select r.username,r.acctstarttime,r.acctinputoctets,r.acctoutputoctets from radacct as r    WHERE date(acctstarttime) BETWEEN '$start_date' AND '$end_date' $where_con");
	foreach($query->result() as $row_ids){
	    $userids[] = $row_ids->username;
	}
	$userids='"'.implode('", "', $userids).'"';
	$get_user_detail = $this->DB2->query("select username,gender,age_group from sht_users where username in ($userids)");
	foreach($get_user_detail->result() as $row_detail){
	    $user_detail[$row_detail->username] = $row_detail;
	}
	
	
	
	foreach($query->result() as $row_data){
	    $session_time = $row_data->acctstarttime;
	    $day = date('D', strtotime($session_time));
	    if(count($day_filter) == '0' || in_array($day,$day_filter)){
		$total_session = $total_session+$row_data->acctinputoctets+$row_data->acctoutputoctets;
	    
		if(in_array($row_data->username,$previous_user))
		{
		    $repeat_visitor = $repeat_visitor+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $new_user = $new_user+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		$previous_user[] = $row_data->username;
		$gender = '';$age_group = '';
		if(isset($user_detail[$row_data->username])){
		    $gender = $user_detail[$row_data->username]->gender;
		    $age_group = $user_detail[$row_data->username]->age_group;
		}
		if(strtolower($gender) == 'f' || strtolower($gender) == 'female'){
		    
		    $total_female = $total_female+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $total_male = $total_male+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		if($age_group == '18'){
		    $below18 = $below18+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($age_group == '31-35'){
		    $user31_45 = $user31_45+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($age_group == '45'){
		    $above45 = $above45+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $user18_30 = $user18_30+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		if(array_key_exists($row_data->username,$user_deviceinfo))
		{
		    $keyarr=array_keys($user_deviceinfo[$row_data->username]);
		    if(count($keyarr)>0)
		    {
			$keyval=$keyarr[0];
			$osinfo = $user_deviceinfo[$row_data->username][$keyval]->osinfo;
			if($osinfo == 'Android Phone'){
			    $android = $android+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}elseif($osinfo == 'Apple Phone'){
			    $ios = $ios+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}elseif($osinfo == 'Windows Desktop'){
			    $laptop = $laptop+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}else{
			    $other_device = $other_device+$row_data->acctinputoctets+$row_data->acctoutputoctets;
			}
			unset($user_deviceinfo[$row_data->username][$keyval]);
		    }else{
			$android = $android+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		    }
		}else{
		    $android = $android+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
		$time = date('H', strtotime($session_time));
		if($time == '00' || $time == '01' || $time == '02' ||$time == '03' ||$time == '04' ||$time == '05' ||$time == '06' ||$time == '07' ||$time == '08' ||$time == '09'){
		    $morning = $morning+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($time == '10' ||$time == '11' ||$time == '12' ||$time == '13'){
		    $noon = $noon+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}elseif($time == '14' ||$time == '15' ||$time == '16' ||$time == '17'){
		    $afternoon = $afternoon+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}else{
		    $evening = $evening+$row_data->acctinputoctets+$row_data->acctoutputoctets;
		}
	    }
	}
	//echo "<pre>";print_r($user_deviceinfo);die;
	if($total_session_i != '0'){
	    $total_session = round(($total_session/(1024*1024*1024))/$total_session_i,2);
	}else{
	    $total_session = round(($total_session/(1024*1024*1024)),2);
	}
	
	$daily_avg =	round(($total_session/$no_of_days),2);
	$data['total_session'] = $total_session;
	$data['new_user'] = round(($new_user/(1024*1024*1024)),2);
	if($new_user_i != '0'){
	    $data['new_user'] = round(($new_user/(1024*1024*1024))/$new_user_i,2);
	}
	$data['repeat_visitor'] = round(($repeat_visitor/(1024*1024*1024)),2);
	if($repeat_visitor_i != '0'){
	    $data['repeat_visitor'] = round(($repeat_visitor/(1024*1024*1024))/$repeat_visitor_i,2);
	}
	$data['daily_avg'] = $daily_avg;
	$data['total_male'] = round(($total_male/(1024*1024*1024)),2);
	if($total_male_i != '0'){
	    $data['total_male'] = round(($total_male/(1024*1024*1024))/$total_male_i,2);
	}
	$data['total_female'] = round(($total_female/(1024*1024*1024)),2);
	if($total_female_i != '0'){
	    $data['total_female'] = round(($total_female/(1024*1024*1024))/$total_female_i,2);
	}
	$data['below18'] = round(($below18/(1024*1024*1024)),2);
	if($below18_i != '0'){
	    $data['below18'] = round(($below18/(1024*1024*1024))/$below18_i,2);
	}
	$data['user18_30'] = round(($user18_30/(1024*1024*1024)),2);
	if($user18_30_i != '0'){
	    $data['user18_30'] = round(($user18_30/(1024*1024*1024))/$user18_30_i,2);
	}
	$data['user31_45'] = round(($user31_45/(1024*1024*1024)),2);
	if($user31_45_i != '0'){
	    $data['user31_45'] = round(($user31_45/(1024*1024*1024))/$user31_45_i,2);
	}
	$data['above45'] = round(($above45/(1024*1024*1024)),2);
	if($above45_i != '0'){
	    $data['above45'] = round(($above45/(1024*1024*1024))/$above45_i,2);
	}
	$data['android'] = round(($android/(1024*1024*1024)),2);
	if($android_i != '0'){
	    $data['android'] = round(($android/(1024*1024*1024))/$android_i,2);
	}
	$data['ios'] = round(($ios/(1024*1024*1024)),2);
	if($ios_i != '0'){
	    $data['ios'] = round(($ios/(1024*1024*1024))/$ios_i,2);
	}
	$data['laptop'] = round(($laptop/(1024*1024*1024)),2);
	if($laptop_i != '0'){
	    $data['laptop'] = round(($laptop/(1024*1024*1024))/$laptop_i,2);
	}
	$data['other_device'] = round(($other_device/(1024*1024*1024)),2);
	if($other_device_i != '0'){
	    $data['other_device'] = round(($other_device/(1024*1024*1024))/$other_device_i,2);
	}
	$data['morning'] = round(($morning/(1024*1024*1024)),2);
	if($morning_i != '0'){
	    $data['morning'] = round(($morning/(1024*1024*1024))/$morning_i,2);
	}
	$data['noon'] = round(($noon/(1024*1024*1024)),2);
	if($noon_i != '0'){
	    $data['noon'] = round(($noon/(1024*1024*1024))/$noon_i,2);
	}
	$data['afternoon'] = round(($afternoon/(1024*1024*1024)),2);
	if($afternoon_i != '0'){
	    $data['afternoon'] = round(($afternoon/(1024*1024*1024))/$afternoon_i,2);
	}
	$data['evening'] = round(($evening/(1024*1024*1024)),2);
	if($evening_i != '0'){
	    $data['evening'] = round(($evening/(1024*1024*1024))/$evening_i,2);
	}
	//echo "<pre>";print_r($data);die;
	return $data;
    }
    
    public function current_pland_detail_retail_cafe($jsondata){
	  $data = array();
	  $location_uid = $jsondata->location_uid;
	  $query = $this->DB2->query("select cafe_plan,cp1_no_of_daily_session from wifi_location_cptype where uid_location = '$location_uid'");
	  if($query->num_rows()){
	       $row_plan = $query->row_array();
	       $plan_id = $row_plan['cafe_plan'];
	       $get = $this->DB2->query("select srvname,descr, plantype, downrate, uprate, timelimit, datalimit from sht_services where srvid = '$plan_id'");
	       if($get->num_rows() > 0){
		    $row = $get->row_array();
		    $data['plan_id'] = $plan_id;
		    $data['no_of_session'] = $row_plan['cp1_no_of_daily_session'];
		    $data['plan_type'] = $row['plantype'];
		    $data['plan_name'] = $row['srvname'];
		    $data['plan_desc'] = $row['descr'];
		    $data['download_rate'] = ceil($row['downrate']/(1024*1024));
		    $data['upload_rate'] = ceil($row['uprate']/(1024*1024));
		    if($row['timelimit'] != ''){
			 $data['timelimit'] = $row['timelimit']/60;
		    }else{
			 $data['timelimit'] = '0';   
		    }
		    $data['resultCode'] = '1';
		    $data['resultMsg'] = 'Success';
	       }else{
		    $data['resultCode'] = '0';
		    $data['resultMsg'] = 'No record Found';  
	       }  
	  }else{
	       $data['resultCode'] = '0';
	       $data['resultMsg'] = 'No record Found';
	  }
	  return $data;
     }
     public function update_plan_bandwidth_retail_cafe($jsondata){
	  $data = array();
	  $location_uid = $jsondata->location_uid;
	  $plan_id = $jsondata->plan_id;
	  $download_rate = $jsondata->download_rate;
	  $upload_rate = $jsondata->upload_rate;
	  $time_limit = $jsondata->time_limit;
	  $no_of_session = $jsondata->no_of_session;
	  
	  $downrate = $download_rate*1024*1024;
	  $uprate = $upload_rate*1024*1024;
	  $time_limit = $time_limit*60;
	  
	  $query = $this->DB2->query("update sht_services set downrate = '$downrate', uprate = '$uprate', timelimit = '$time_limit' where srvid = '$plan_id'");
	  $query = $this->DB2->query("update  wifi_location_cptype set cp1_no_of_daily_session = '$no_of_session' where uid_location = '$location_uid'");
	  $data['resultCode'] = '1';
	  $data['resultMsg'] = 'Success';  
	  return $data;
     }
     
     public function get_plan_list($jsondata){
	  $data = array();
	  $location_uid = $jsondata->location_uid;
	  $query = $this->DB2->query("select hp.plan_id, s.srvname from ht_planassociation as hp inner join sht_services as s on (hp.plan_id = s.srvid) where hp.location_uid = '$location_uid'");
	  if($query->num_rows() > 0){
	      $i = 0;
	      foreach($query->result() as $row){
		  $data[$i]['plan_id'] = $row->plan_id;
		  $data[$i]['plan_name'] = $row->srvname;
		  $i++;
	      }
	  }
	  return $data;
     }
     public function get_plan_detail($jsondata){
	  $plantype = '';
	  $srvname = '';$descr = '';
	  $download_rate = '';$upload_rate = '';$timelimit = 0;
	  $data = array();
	  $plan_id  = $jsondata->plan_id;
	  $get = $this->db->query("select srvname,descr, plantype, downrate, uprate, timelimit, datalimit from sht_services where srvid = '$plan_id'");
	  if($get->num_rows() > 0){
	      $row = $get->row_array();
	      $plan_id = $plan_id;
	      $plantype = $row['plantype'];
	      $srvname = $row['srvname'];
	      $descr = $row['descr'];
	      $download_rate = ceil($row['downrate']/(1024*1024));
	      $upload_rate = ceil($row['uprate']/(1024*1024));
	      if($row['timelimit'] != ''){
		   $timelimit = $row['timelimit']/60;
	      }else{
		$timelimit = '0';   
	      }
	  }  
	  
	  $data['plan_id'] = $plan_id;
	  $data['plan_type'] = $plantype;
	  $data['plan_name'] = $srvname;
	  $data['plan_desc'] = $descr;
	  $data['download_rate'] = $download_rate;
	  $data['upload_rate'] = $upload_rate;
	  
	  $data['timelimit'] = $timelimit;   
	  
	  return $data;
     }
     public function current_image_path($jsondata){
	  $location_uid = $jsondata->location_uid;
		  $data = array();
		  $query_cp = $this->DB2->query("select original_image,retail_original_image from wifi_location_cptype where uid_location = '$location_uid'");
		  if($query_cp->num_rows() > 0){
			   $row_cp = $query_cp->row_array();
			   $original_image_path = '';
			   if($row_cp['original_image'] != ''){
				    $original_image_path = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_location_logo/original/'.$row_cp['original_image'];
			   }
			   $data['logo_image'] = $original_image_path;
			   $retail_original_image_path = '';
			   if($row_cp['retail_original_image'] != ''){
				    $retail_original_image_path = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$row_cp['retail_original_image'];
			   }
			   $data['offer_image'] = $retail_original_image_path;
			   $data['code'] = 1;
		  }else{
			   $data['code'] = 0;
		  }
	
		  return $data;
     }
	 
     
     public function get_nasip($locid){
      
		
        $query=$this->DB2->query("select nasipaddress from wifi_location where id='".$locid."'");
		$dataarr=$query->row_array();
		
        return $dataarr['nasipaddress'];
     }
     public function reseller_traffic_report($jsondata){
		 
	$data = array();
	$cond="";
	if($jsondata->startdate==$jsondata->enddate)
	{
	    $cond.="and date(acctstarttime)='$jsondata->startdate'";
	      $calenderdate=date("d M Y");
	}
	else{
	    $cond.="and date(acctstarttime) between '$jsondata->startdate' and '$jsondata->enddate'";
	     $calenderdate=date("d M Y",strtotime($jsondata->startdate))." - ".date("d M Y",strtotime($jsondata->enddate));
	}
	$nasip=$this->get_nasip($jsondata->location_id);
	  $location_uid = $jsondata->location_uid;
	  if($nasip=="104.155.209.8")
	  {
		  $macquery=$this->DB2->query("select macid from wifi_location_access_point where location_uid='".$location_uid."'");
		  if($macquery->num_rows()>0)
		  {
			   $nas_where = '';
			   $onehop_macid = array();
			   foreach($macquery->result() as $row_mac){
				    $onehop_macid[] = $row_mac->macid;
			   }
			   if(count($onehop_macid) <= '0'){
				    $nas_where = $nas_where." calledstationid IN ('notFound') ";
			   }else{
				    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
				    $nas_where = $nas_where." calledstationid IN ($onehop_macid) ";
			   }
			  $macresult=$macquery->row_array();
			 $macid=$macresult['macid'];
			 $chech_user = $this->DB2->query("select * from radacct WHERE $nas_where {$cond}");
		  }
		  else{
			  $macid='AAAAAAAAAAAAAA';
			   $chech_user = $this->DB2->query("select * from radacct WHERE calledstationid = '$macid' {$cond}");
		  }
		  
		  
	  }
	  else{
		    $chech_user = $this->DB2->query("select * from radacct WHERE nasipaddress = '$nasip' {$cond}");
	  }
	
	//$chech_user = $this->db->query("select * from radacct WHERE nasipaddress = '$jsondata->nasip' {$cond}");
	
    $totaluser=0;
		$totalunique=0;
		$totalrepeat=0;
		$totaldata=0;
		$lifetimeuser=0;
		$countuser=0;
	
	if($chech_user->num_rows() > 0){
    
	$data=array();
	$datarr=array();
	$i=0;
	
	  
		
		foreach($chech_user->result() as $vald)
		{
			$datarr[$i]['username']=$vald->username;
		$datarr[$i]['nasipaddress']=$vald->nasipaddress;
		$datarr[$i]['acctstarttime']=$vald->acctstarttime;
		$datarr[$i]['acctstoptime']=$vald->acctstoptime;
		$datarr[$i]['acctinputoctets']=$vald->acctinputoctets;
		$datarr[$i]['acctoutputoctets']=$vald->acctoutputoctets;
	    
	    $i++;
		}
		
	    
	
	    
	  if($nasip=="104.155.209.8")
	  {
		  $macquery=$this->DB2->query("select macid from wifi_location_access_point where location_uid='".$location_uid."' and router_type='6'");
		  if($macquery->num_rows()>0)
		  {
			  $macidarr=$macquery->row_array();
			  $macid=$macidarr['macid'];
			 $count_user = $this->DB2->query("select count(*) as count from radacct WHERE calledstationid = '$macid' and acctstoptime is null");
			 $countarr=$count_user->row_array(); 
			 $countuser= $countarr['count'];
		  }
		  else{
			  $countuser= 0;
		  }
		  
		  
	  }
	  else{
		    $count_user = $this->DB2->query("select count(*) as count from radacct WHERE nasipaddress = '$nasip' and acctstoptime is null");
			 $countarr=$count_user->row_array();
			$countuser= $countarr['count'];
	  }
	   
		
		
		$lifetime_user = $this->DB2->query("select count(*) as count from radacct WHERE nasipaddress = '$nasip'");
		$lifetmarr=$lifetime_user->row_array();
		$lifetimeuser=$lifetmarr['count'];
		
    
	    $timedatearr=array();
	    $totalunq_userarr=array();
	    if($jsondata->startdate==$jsondata->enddate)
	    {
		$x=0;
		$alreadyuser=array();
		$timearr=array();
		//echo "<pre>"; print_R($timedatearr); die;
		foreach($datarr as $val)
		{
			
		    $timearr=explode(":",end(explode(" ", $val['acctstarttime'])));
			//echo "<pre>"; print_R($timearr);
		    $timedatearr[$timearr[0]][$x]=$val;
		    $x++;
		    
		}
		
		$fnlarr=array();
		$alreadyuser=array();
		
		    
		    
		foreach($timedatearr as $key=> $valx)
		{
		    
		    $repeateduser=0;
		    $uniqueuser=0;
		    $datamb=0;
		    foreach($valx as $vald)
		    {
			if(in_array($vald['username'],$alreadyuser))
			{
			    $repeateduser=$repeateduser+1;
			}
			else{
			    $alreadyuser[]=$vald['username'];
			    $uniqueuser=$uniqueuser+1;
			}
			$mbtotal=$vald['acctinputoctets']+$vald['acctoutputoctets'];
			$datamb=$datamb+($mbtotal/(1024*1024*1024));
			$totalunq_userarr[]=$vald['username'];
			
		    }
		    $fnlarr[$key]['uniqueuser']=$uniqueuser;
		    $fnlarr[$key]['repeateduser']=$repeateduser;
		    $fnlarr[$key]['dataused']=round($datamb,2);
			$fnlarr[$key]['time_or_date']=(int)$key;
		    
		    
		    $totalunique=$totalunique+$uniqueuser;
		    $totalrepeat=$totalrepeat+$repeateduser;
		    $totaldata=$totaldata+$datamb;
		    
		}
		$totaluser=count(array_unique($totalunq_userarr));
		
		
		
		
	    }
	    else{
		
		    $x=0;
		    $alreadyuser=array();
		
		    foreach($datarr as $val)
		    {
			$timearr=array();
			$timearr=explode(" ", $val['acctstarttime']);
			$datetime=date("d M",strtotime($timearr[0]));
			
			$timedatearr[$datetime][$x]=$val;
			$x++;
			
		    }
		    
		    $fnlarr=array();
		    $alreadyuser=array();
		    $totaluser=0;
		    $totalunique=0;
		$totalrepeat=0;
		$totaldata=0;
		    foreach($timedatearr as $key=> $valx)
		    {
			
			$repeateduser=0;
			$uniqueuser=0;
			$datamb=0;
			foreach($valx as $vald)
			{
			    if(in_array($vald['username'],$alreadyuser))
			    {
				$repeateduser=$repeateduser+1;
			    }
			    else{
				$alreadyuser[]=$vald['username'];
				$uniqueuser=$uniqueuser+1;
			    }
			    $mbtotal=$vald['acctinputoctets']+$vald['acctoutputoctets'];
			    $datamb=$datamb+($mbtotal/(1024*1024*1024));
			    $totalunq_userarr[]=$vald['username'];
			    
			}
			
			$fnlarr[$key]['uniqueuser']=$uniqueuser;
			$fnlarr[$key]['repeateduser']=$repeateduser;
			$fnlarr[$key]['dataused']=round($datamb, 2);
			$fnlarr[$key]['time_or_date']=$key;
			
			
			$totalunique=$totalunique+$uniqueuser;
			$totalrepeat=$totalrepeat+$repeateduser;
			$totaldata=$totaldata+$datamb;
			
		    }
		    $totaluser=count(array_unique($totalunq_userarr));
		
		
	    }
	    //echo 'ssss'; die;
		
	      $data['resultCode'] = '1';
	    $data['resultMsg'] = 'Success';
	    $data['online_user']=$countuser;
	    $data['lifetime_user']=$lifetimeuser;
	    $data['total_user']=$totaluser;
	    $data['unique_user']=$totalunique;
	    $data['repeated_user']=$totalrepeat;
	    $data['calenderdate']=$calenderdate;
	    $data['totaldata']=round($totaldata, 2);
	    $data['datetimedata']=$fnlarr;
    
	}
	else{
		//echo 'aaaassss'; die;
	    $data['resultCode'] = '0';
	    $data['resultMsg'] = 'No session taken';
	    $data['online_user']=$countuser;
	    $data['lifetime_user']=$lifetimeuser;
	    $data['total_user']=$totaluser;
	    $data['unique_user']=$totalunique;
	    $data['repeated_user']=$totalrepeat;
	    $data['calenderdate']=$calenderdate;
	      $data['totaldata']=$totaldata;
		  
	}
	
	return $data;
     }
     public function user_report($jsondata){
	 // print_r($jsondata);die;
         $location_id = $jsondata->location_id;
          $nasip = $this->get_nasip($location_id);
         
	 $location_uid = $jsondata->location_uid;
            $startdate = $jsondata->startdate;
            $enddate = $jsondata->enddate;
	 //print_r($session_data);die;
	 //$startdate = '2017-09-15';
	 //$enddate = '2017-09-21';
	 $requestData['requestData'] = array("nasip" => $nasip,"location_id" => "$location_id","from"=>$startdate,"to"=>$enddate,"location_uid" => "$location_uid");
	 $data=$this->traffic_summary_total_user(json_decode(json_encode($requestData['requestData'])));
         //$data= json_decode($this->hitApi($requestData,RADIUSAPI));
	 //echo "<pre>"; print_R($data);die;
	 return json_decode(json_encode($data));
     }
     public function traffic_summary_total_user($jsondata){
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $location_id = $jsondata->location_id;
	 
	  
	  
	  $location_filter_query = '';;
	  $total_unique_user_array=array();
	  $total_new_user=0;
	  $total_returning_user=array();
	  $dataarr=array();
	  $previous_user = array();
	  $previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$from'   group by userid");
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  
	  //condition
	  $cond = "and date(wufs.session_date) between '$from' and '$to'";
	 
	  $chech_user = $this->DB2->query("select wufs.*, date(session_date) as date_field  from wifi_user_free_session as wufs  WHERE wufs.location_id = '$location_id'   $cond
	  order by wufs.session_date");
	  if($chech_user->num_rows() > 0){
		$dataarr["resultCode"] = '1';
		  $dataarr["resultMsg"] = 'Success';  
	  }else{
		  $dataarr["resultCode"] = '0';
		  $dataarr["resultMsg"] = 'fail';
	  }
	  
	  $date_wise = array();
	  
	  foreach($chech_user->result() as $chech_user_row){
	       $date_wise[$chech_user_row->date_field][]= $chech_user_row;
	  }
	  $fnlarr=array();
	  
	  $daywisearr=array();
	 // echo "<pre>";print_r($date_wise_arr);die;
	  foreach($date_wise as $keydate => $date_wise_row){
	       
	       $date_wise_arr=array();
	       $repeatuser=0;
	       $newuser=0;
	       foreach($date_wise_row as $valnew)
	       {
		    
		    
		     if(!in_array($valnew->userid,$date_wise_arr)){
			 if(in_array($valnew->userid,$previous_user))
			 {
			   $repeatuser=$repeatuser+1;
			   $total_returning_user[] = $valnew->userid;
			 }
			 else
			 {
			     $newuser=$newuser+1;
			     $total_new_user = $total_new_user+1;
			 }
			 $total_unique_user_array[] = $valnew->userid;
		  $date_wise_arr[]=$valnew->userid;
		  $previous_user[]=$valnew->userid;
	         }
		 $fnlarr[$keydate]['newuser']= $newuser;
		 $fnlarr[$keydate]['repeatuser']= $repeatuser;
	       }
	      
	  }
	  $x=0;
	  foreach($fnlarr as $key=>$valdate)
	  {
	       
	       $dataarr['datetimedata'][date("d M",strtotime($key))]['uniqueuser']=$valdate['newuser'];
	       $dataarr['datetimedata'][date("d M",strtotime($key))]['repeateduser']=$valdate['repeatuser'];
	       $dataarr['datetimedata'][date("d M",strtotime($key))]['time_or_date']=date("d M",strtotime($key));
	       $x++;
	       
	  }
	  $dataarr["unique_user"] = $total_new_user;
	  $dataarr["repeated_user"] = count(array_unique($total_returning_user));
	  $dataarr["total_user"] = count(array_unique($total_unique_user_array));
	  $dataarr["calenderdate"] = $calenderdate=date("d M Y",strtotime($from))." - ".date("d M Y",strtotime($to));
	   
	 $location_uid = $jsondata->location_uid;
	  if($jsondata->nasip=="104.155.209.8")
	  {
		  $macquery=$this->DB2->query("select macid from wifi_location_access_point where location_uid='".$location_uid."' and router_type='6'");
		  if($macquery->num_rows()>0)
		  {
			  $macidarr=$macquery->row_array();
			  $macid=$macidarr['macid'];
			 $count_user = $this->DB2->query("select count(*) as count from radacct WHERE calledstationid = '$macid' and acctstoptime is null");
			 $countarr=$count_user->row_array(); 
			 $countuser= $countarr['count'];
		  }
		  else{
			  $countuser= 0;
		  }
		  
		  
	  }
	  else{
		    $count_user = $this->DB2->query("select count(*) as count from radacct WHERE nasipaddress = '$jsondata->nasip' and acctstoptime is null");
			 $countarr=$count_user->row_array();
			$countuser= $countarr['count'];
	  }
	
	
	 $dataarr["online_user"] = $countuser;
	 $lifetime_user = $this->DB2->query("select count(distinct(userid)) as count from wifi_user_free_session WHERE location_id = '$location_id'");
	 $lifetmarr=$lifetime_user->row_array();
	 $lifetimeuser=$lifetmarr['count'];
	 $dataarr["lifetime_user"] = $lifetimeuser;
	  //echo "<pre>";print_r($dataarr);die;
	  return $dataarr;
        
     }
     
     public function traffic_report($jsondata){
	  $location_id = $jsondata->location_id;
          $nasip = $this->get_nasip($location_id);
	  $startdate = $jsondata->startdate;
          $enddate = $jsondata->enddate;
	  
          $location_uid = $jsondata->location_uid;
	 //$startdate = '2017-09-15';
	 //$enddate = '2017-09-21';
	 $requestData['requestData'] = array("nasip" => $nasip,"location_id" => "$location_id","from"=>$startdate,"to"=>$enddate,"location_uid" => "$location_uid");
	 $data=$this->traffic_summary_total_session(json_decode(json_encode($requestData['requestData'])));
         //$data= json_decode($this->hitApi($requestData,RADIUSAPI));
	 //echo "<pre>"; print_R($data);die;
	 return json_decode(json_encode($data));
     }
     public function traffic_summary_total_session($jsondata){
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $location_id = $jsondata->location_id;
	  $location_filter_query = '';;
	  $total_session =0 ;
	  $total_new_user=0;
	  $total_returning_session =0;
	  $dataarr=array();
	  $previous_user = array();
	  $previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$from'   group by userid");
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  
	  //condition
	  $cond = "and date(wufs.session_date) between '$from' and '$to'";
	 
	  $chech_user = $this->DB2->query("select wufs.*, date(session_date) as date_field  from wifi_user_free_session as wufs  WHERE wufs.location_id = '$location_id'  $cond
          order by wufs.session_date");
	  if($chech_user->num_rows() > 0){
		$dataarr["resultCode"] = '1';
		  $dataarr["resultMsg"] = 'Success';  
	  }else{
		  $dataarr["resultCode"] = '0';
		  $dataarr["resultMsg"] = 'fail';
	  }
	  $date_wise = array();
	  foreach($chech_user->result() as $chech_user_row){
	       $date_wise[$chech_user_row->date_field][]= $chech_user_row;
	  }
	  $fnlarr=array();
	  
	  $daywisearr=array();
	 // echo "<pre>";print_r($date_wise_arr);die;
	  foreach($date_wise as $keydate => $date_wise_row){
	       
	       $date_wise_arr=array();
	       $repeatuser=0;
	       $newuser=0;
	       foreach($date_wise_row as $valnew)
	       {
		    
		    
		    //if(!in_array($valnew->userid,$date_wise_arr)){
			 if(in_array($valnew->userid,$previous_user))
			 {
			   $repeatuser=$repeatuser+1;
			   $total_returning_session = $total_returning_session+1;;
			 }
			 else
			 {
			     $newuser=$newuser+1;
			     $total_new_user = $total_new_user+1;
			 }
			 $total_session = $total_session+1;
			 //$total_unique_user_array[] = $valnew->userid;
			 //$date_wise_arr[]=$valnew->userid;
			 $previous_user[]=$valnew->userid;
		    //}
		    $fnlarr[$keydate]['newuser']= $newuser;
		    $fnlarr[$keydate]['repeatuser']= $repeatuser;
	       }
	      
	  }
	  $x=0;
	  foreach($fnlarr as $key=>$valdate)
	  {
	       $dataarr['datetimedata'][date("d M",strtotime($key))]['uniqueuser']=$valdate['newuser'];
	       $dataarr['datetimedata'][date("d M",strtotime($key))]['repeateduser']=$valdate['repeatuser'];
	       $dataarr['datetimedata'][date("d M",strtotime($key))]['time_or_date']=date("d M",strtotime($key));
	       $x++;
	       
	  }
	  $dataarr["unique_user"] = $total_new_user;
	  $dataarr["repeated_user"] = $total_returning_session;
	  $dataarr["total_user"] = $total_session;
	   $dataarr["calenderdate"] = $calenderdate=date("d M Y",strtotime($from))." - ".date("d M Y",strtotime($to));
	  
	 $location_uid = $jsondata->location_uid;
	  if($jsondata->nasip=="104.155.209.8")
	  {
		  $macquery=$this->DB2->query("select macid from wifi_location_access_point where location_uid='".$location_uid."' and router_type='6'");
		  if($macquery->num_rows()>0)
		  {
			  
			  $macidarr=$macquery->row_array();
			  $macid=$macidarr['macid'];
			 $count_user = $this->DB2->query("select count(*) as count from radacct WHERE calledstationid = '$macid' and acctstoptime is null");
			 $countarr=$count_user->row_array(); 
			 $countuser= $countarr['count'];
		  }
		  else{
			  $countuser= 0;
		  }
		  
		  
	  }
	  else{
		    $count_user = $this->DB2->query("select count(*) as count from radacct WHERE nasipaddress = '$jsondata->nasip' and acctstoptime is null");
			 $countarr=$count_user->row_array();
			$countuser= $countarr['count'];
	  }
	 $dataarr["online_user"] = $countuser;
	 $lifetime_user = $this->DB2->query("select count(*) as count from wifi_user_free_session WHERE location_id = '$location_id'");
	 $lifetmarr=$lifetime_user->row_array();
	 $lifetimeuser=$lifetmarr['count'];
	 $dataarr["lifetime_user"] = $lifetimeuser;
	  //echo "<pre>";print_r($dataarr);die;
	  return $dataarr;
        
     }
     
     public function terms_of_use_get($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $terms = '';
	  $query = $this->DB2->query("select terms_of_use from wifi_location_cptype where uid_location = '$location_uid'");
	  if($query->num_rows() > 0){
	       $row = $query->row_array();
	       $terms = $row['terms_of_use'];
	  }
	  $data['terms'] = $terms;
	  return $data;
     }
     public function update_terms_of_use($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $terms = $jsondata->terms;
	  $data = array();
	  $query = $this->DB2->query("select terms_of_use from wifi_location_cptype where uid_location = '$location_uid'");
	
		  if($query->num_rows() > 0){
		    $tabledata=array("terms_of_use"=>$terms);
		    $this->DB2->update('wifi_location_cptype', $tabledata,array("uid_location"=>$location_uid));
		  }else{
		    $tabledata=array("terms_of_use"=>$terms, "uid_location"=>$location_uid);
		    $this->DB2->insert('wifi_location_cptype',$tabledata);
		  }
	  $data['resultMessage'] = "success";
	  return $data;
     }
     
     public function update_password($jsondata){
	  $data = array();
	  $location_uid = $jsondata->location_uid;
	  $current_password= $jsondata->current_password;
	  $new_password = $jsondata->new_password;
	  $resultCode = 0;
	  $resultMessages = '';
	  $get = $this->DB2->query("select password, pwd_updated from wifi_location where location_uid = '$location_uid'");
	       if($get->num_rows() > 0){
		    $row = $get->row_array();
		    if($row['pwd_updated'] == '1'){
			 $old_pass = $row['password'];
			 if($old_pass == $current_password){
			      $resultCode = '1';
			      $resultMessages = 'Successfully updated';
			 }
		    }else{
			 if($location_uid == $current_password){
			      $resultCode = '1';
			      $resultMessages = 'Successfully updated';
			 }
		    }
	       }
	       if($resultCode == '1'){
		    $this->DB2->query("update wifi_location set password = '$new_password', pwd_updated = '1' where location_uid = '$location_uid'");
	       }else{
		    $resultCode = '0';
		    $resultMessages = 'Invalid current Password';
	       }
	  $data['resultCode'] = $resultCode;
	  $data['resultMessages'] = $resultMessages;
	  return $data;
     }
	 
	 public function pin_signupdata($jsondata)
     {
	 $data=array();
	 $data['resultcode']=1;
	 $query=$this->DB2->query("select pin from cafe_pin WHERE date(pindate) = '".date('Y-m-d')."'
				 and loc_uid='".$jsondata->location_uid."'");
	 if($query->num_rows()>0)
	 {
		  $rowarr=$query->row_array();
		  $data['pin']=$rowarr['pin'];
	}
	 else
	 {
		  $pin=$this->randon_pin();
		  $data['pin']=$pin;
		  $tabledata=array("loc_uid"=>$jsondata->location_uid,"pin"=>$pin,"pindate"=>date("Y-m-d H:i:s"));
		  $this->DB2->insert('cafe_pin',$tabledata);
		 
	 }
	 $query1=$this->DB2->query("select signup_point,pervisit_point from cafe_signup_point WHERE loc_uid='".$jsondata->location_uid."'");
	 if($query1->num_rows()>0)
	 {
		  $rowarr1=$query1->row_array();
		  $data['pointdata']['signup_point']=$rowarr1['signup_point'];
		   $data['pointdata']['pervisit_point']=$rowarr1['pervisit_point'];
	 }
	 else
	 {
		 $data['pointdata']['signup_point']='200';
		   $data['pointdata']['pervisit_point']='100';
		   $tabledata=array("loc_uid"=>$jsondata->location_uid,"signup_point"=>'200',"pervisit_point"=>'100');
		  $this->DB2->insert('cafe_signup_point',$tabledata);
		 
	 }
	 return $data;
	 
	 
      
     }
	 
	 
	  public function update_signupdata($jsondata)
	 {
		 
		 $data=array();
		 $data['resultcode']=1;
	   $loc_uid=$jsondata->location_uid;
	   $tabledata=array("signup_point"=>$jsondata->signuppoint);
	   $this->DB2->update("cafe_signup_point",$tabledata,array("loc_uid"=>$loc_uid));
	   $data['signup']=$jsondata->signuppoint;
		return $data;
	   
	  }
	  
	    public function update_pervisitdata($jsondata)
	 {
		 
		 $data=array();
		 $data['resultcode']=1;
	   $loc_uid=$jsondata->location_uid;
	   $tabledata=array("pervisit_point"=>$jsondata->pervisitpoint);
	   $this->DB2->update("cafe_signup_point",$tabledata,array("loc_uid"=>$loc_uid));
	   $data['pervist']=$jsondata->pervisitpoint;
		return $data;
	   
	  }
	  
	  public function pin_reset($jsondata)
	{
	 $pin=$this->randon_pin();
	
	 //echo "<pre>"; print_R($session_data); die;
	  $loc_uid=$jsondata->location_uid;
	 
	  $tabledata=array("pin"=>$pin);
	  $currentdate=date("Y-m-d");
	  $this->DB2->query("update cafe_pin set pin='".$pin."' where loc_uid='".$loc_uid."' and date(pindate)='".$currentdate."'");
	 // echo $this->db->last_query();die;
	  if ($this->DB2->affected_rows() >= 0) {
		  $data['resultcode']=1;
		  $data['pin']=$pin;
	  }
	  else
	  {
		  $data['resultcode']=0;
		  $data['pin']='';
		  
	  }
	  return $data;
	 
	 }
	 
	 
	  public function randon_pin(){
		$random_number = mt_rand(1000, 9999);
		$query = $this->DB2->query("select pin from cafe_pin WHERE pin = '$random_number'");
		if($query->num_rows() > 0){
			$this->randon_voucher();
		}else{
			return $random_number;
		}
	}
	
	
		public function loyalty_membercafe($jsondata)
	{
			 $cond='';
	 $data=array();
	 $data['resultcode']=1;
	 $postdata=$this->input->post();
	 $search=$jsondata->search;
	  $loc_uid=$jsondata->location_uid;
	 if($search!='')
	 {
		$query=$this->DB2->query("select uid from sht_users where lastname  ='".$search."'" ) ;
		//echo $this->db->last_query(); die;
		if($query->num_rows()>0)
		{
		  foreach($query->result() as $val)
		  {
			$userarr[]= $val->uid;  
		  }
		  	     // $collection = $mongo_dbname->sh_user;
	       $paramid = '"' . implode('", "', $userarr) . '"';
                 //   $cond .= "AND uid IN ({$paramid})";
     $cond .= "AND crp.uid IN ({$paramid})";
	     
		  
		}
		else
		{
		  $userarr[]=$search;
		 $paramid = '"' . implode('", "', $userarr) . '"';
                 //   $cond .= "AND uid IN ({$paramid})";
     $cond .= "AND crp.uid IN ({$paramid})";
	     
		}
	 }
	 $redeemedquery=$this->DB2->query("select su.lastname,su.firstname,su.email,crp.`uid`,crp.`loc_uid`,crp.`points`,crp.`added_on` from cafe_redeemed_point crp
					 inner join sht_users su on (su.uid=crp.uid) where crp.loc_uid='".$loc_uid."'   {$cond} ");
	
	 $redeemarr= array();
	 $redlastactivty=array();
	 $i=0;
	
	 foreach($redeemedquery->result() as $valred)
	 {
		 
	 $redlastactivty[$valred->uid][]=strtotime($valred->added_on);	 // echo "<pre>"; print_R($earnarr);
	 $previouspoint= ($i==0)?0:(isset($redeemarr[$valred->uid]['points']))?$redeemarr[$valred->uid]['points']:0; 
	 $redeemarr[$valred->uid]['points']= $previouspoint+$valred->points;
	 $redeemarr[$valred->uid]['mobile']= $valred->uid;
	  $redeemarr[$valred->uid]['lastname']= $valred->lastname;
	   $redeemarr[$valred->uid]['firstname']= $valred->firstname;
	    $redeemarr[$valred->uid]['email']= $valred->email;
	 $redeemarr[$valred->uid]['lastactivity']= date("d-m-Y H:i",max($redlastactivty[$valred->uid]));

	 $i++;
	 }
	// echo "<pre>"; print_R($redeemarr); die;
	 
	 
	 
	 $earnquery=$this->DB2->query("select su.mobile,su.uid,su.lastname,su.firstname,su.email,crp.`uid`,crp.`loc_uid`,crp.`points`,crp.`point_method`,crp.`added_on` from cafe_earned_point crp
				     inner join sht_users su on (su.uid=crp.uid)  where crp.loc_uid='".$loc_uid."' {$cond}  order by crp.added_on desc");
	 
	 $earnarr=array();
	 $lastactivty=array();
	 $i=0;
	// echo "<pre>"; print_R($earnquery->result());
	// echo "<pre>"; print_R($earnquery->result());die;
	
	 foreach($earnquery->result() as $valearn)
	 {
		  $visitq=$this->DB2->query("select id from cafe_earned_point where uid='".$valearn->uid."' and loc_uid='".$loc_uid."'");
		  $visitcnt=$visitq->num_rows();
		 
	 $lastactivty[$valearn->uid][]=strtotime($valearn->added_on);	 // echo "<pre>"; print_R($earnarr);
	 $previouspoint= ($i==0)?0:(isset($earnarr[$valearn->uid]['points']))?$earnarr[$valearn->uid]['points']:0; 
	 $earnarr[$valearn->uid]['points']= $previouspoint+$valearn->points;
	  $earnarr[$valearn->uid]['mobile']= ($valearn->mobile!='')?$valearn->uid:$valearn->email;
	  $earnarr[$valearn->uid]['mobiledisp']= ($valearn->mobile!='')?$valearn->mobile:'';
	  
	  $earnarr[$valearn->uid]['lastname']= $valearn->lastname;
	  $earnarr[$valearn->uid]['firstname']= $valearn->firstname;
	   $earnarr[$valearn->uid]['email']= $valearn->email;
	    $earnarr[$valearn->uid]['visits']= $visitcnt;
	 $earnarr[$valearn->uid]['lastactivity']= date("d-m-Y H:i",max($lastactivty[$valearn->uid]));
	//$visit++;
	 $i++;
	 }
	
	 
	 $fnldata=array();
	 foreach($earnarr as $key => $vald)
	 {
		
			  $redeempoint=(isset($redeemarr[$key]['points']))? $redeemarr[$key]['points']:0;
			$fnldata[$key]['pointbalnce']= $vald['points']-$redeempoint;
		//  $fnldata[$key]['lastactivity']=(!isset($redeemarr[$key]))?$vald['lastactivity']:(strtotime($redeemarr[$key]['lastactivity'])>strtotime($vald['lastactivity']))?$redeemarr[$key]['lastactivity']:$vald['lastactivity'];
		if(!isset($redeemarr[$key]))
		{
			$fnldata[$key]['lastactivity']=$vald['lastactivity'];
		}
		else{
			$fnldata[$key]['lastactivity']=(strtotime($redeemarr[$key]['lastactivity'])>strtotime($vald['lastactivity']))?$redeemarr[$key]['lastactivity']:$vald['lastactivity'];
		}
	
		 $fnldata[$key]['mobile']=$vald['mobile'];
		  $fnldata[$key]['mobiledisp']=$vald['mobiledisp'];
		  $fnldata[$key]['lastname']=$vald['lastname'];
		   $fnldata[$key]['firstname']=$vald['firstname'];
		    $fnldata[$key]['email']=$vald['email'];
			 $fnldata[$key]['total_visit']=$vald['visits'];
		     $fnldata[$key]['fullname']=ucwords($vald['firstname'])." ".ucwords($vald['lastname']);
			  $fnldata[$key]['uid']=$key;
		
	 
	 }
	
        
         $data['listingrecord']=$fnldata;
		 return $data;
	}
	
	public function loyalty_membercafe_detail($jsondata)
	{
		 $data=array();
		 $data['resultcode']=1;
	 $postdata=array();
	  $redeemarr=array();
	  if(is_numeric($jsondata->uid))
	  {
		  $postdata['uid']=$jsondata->uid;
	  }
	  else{
		  $query1=$this->DB2->query("select uid from sht_users where email='".$jsondata->uid."'");
		  $rowarr=$query1->row_array();
		   $postdata['uid']=$rowarr['uid'];
	  }
	
	  $loc_uid=$jsondata->location_uid;
	  
	 $query=$this->DB2->query("select crp.quantity,crp.reward_id,crp.points,crp.added_on,crp.is_redeem,cr.item_name,cr.points from cafe_redeemed_point crp 
inner join cafe_rewards cr on (cr.id=crp.reward_id)	where crp.uid='".$postdata['uid']."' and crp.loc_uid='".$loc_uid."'");

	 if($query->num_rows()>0)
	 {
		 $i=0;
		  foreach($query->result() as $val)
		  {
			   $datetime=strtotime($val->added_on);
			   $previouspoint= ($i==0)?0:(isset($redeemarr[$datetime]['points']))?$redeemarr[$datetime]['points']:0;
			   $quantity= ($i==0)?0:(isset($redeemarr[$datetime]['quantity']))?$redeemarr[$datetime]['quantity']:0;
	 $redeemarr[$datetime]['points']= $previouspoint+$val->points;
	  $redeemarr[$datetime]['quantity']= $quantity+$val->quantity;
	   $redeemarr[$datetime]['type']= 'redeem';
	   $redeemarr[$datetime]['is_redeemed']= $val->is_redeem;
	   $redeemarr[$datetime]['item_name']= $val->item_name;
	   $redeemarr[$datetime]['points']= $val->points;
	   $i++;
	   
		  }
		  
	 }
	
	 $earnarr=array();
	 $earnquery=$this->DB2->query("select point_method,points,added_on from cafe_earned_point where uid='".$postdata['uid']."' and loc_uid='".$loc_uid."'");
	 if($earnquery->num_rows()>0)
	 {
		  
		  foreach($earnquery->result() as $valr)
		  {
			   $datetime=strtotime($valr->added_on);
			  
	 $earnarr[$datetime]['points']= $valr->points;
	  $earnarr[$datetime]['pointmethod']= $valr->point_method;
	   $earnarr[$datetime]['type']= 'earned';
	   $earnarr[$datetime]['item_name']= '';
	   
	   $earnarr[$datetime]['is_redeemed']= '';
	   
		  }
		  
	 }
	
	  $fnlarr=$redeemarr+$earnarr;//array_merge($redeemarr,$earnarr);
	  krsort($fnlarr);
	  $html="";
	
	 
	 $datarr=array();
	 $i=0;
	 
	  foreach($fnlarr as $keyd=>$vald)
	  {
		  $datarr[$i]=$vald;
		  $datarr[$i]['datetime']=date("d-m-y H:i",$keyd);
		  $datah=($vald['type']=="redeem")?$vald['item_name']." ".$vald['points']." Points":$vald['points']." points earned (".$vald['pointmethod'].")";
		  $datas='';
		  if($vald['type']=="redeem")
		  {
			$datas=  ($vald['is_redeemed']=="1")?"(Offer redeemed)":"(offer availed)";
		  }
                  else{
                      $datas=  ($vald['pointmethod']=="pervisit_point")?"(returning bonus)":"(signup bonus)";
                  }
              $datarr[$i]['pointtext']=$datah;     
			 $datarr[$i]['pointway']=$datas;
                  
           $html.='<div class="list-group"><div class="list-group-item">
                                                   <div class="row"><div class="col-sm-8"><h4 class="list-group-item-heading">'.$vald['points']." points ".$vald['type'].' </h4>
										<small class="members_small">'.date("d-m-y H:i",$keyd).'</small>
														</div>
														<div class="col-sm-4">
														  <div class="row">
															 <h4>'.$datas.'</h4>
														  </div>
														</div>
													</div>
                                                </div>
                                             </div>
' ;      
		  $i++;
		 // $datah=($vald['type']=="redeem")?$vald['points']." points redeemed ( ".$vald['quantity']." items ordered)":$vald['points']." points earned (".$vald['pointmethod'].")";
	
	  }
	 //  echo "<pre>"; print_R($datarr);die;
	  $data['datalist']=$datarr;
	  return $data;
	}
	
	public function list_reward_cafe($jsondata)
	{
	
	  $session_data = $jsondata->location_uid;
	  $loc_uid=$jsondata->location_uid;
	  $listarr=array();
	  $query=$this->DB2->query("select id,`item_name`,`quantity`,`points` from cafe_rewards where status='1' and loc_uid='".$loc_uid."' and is_deleted=0 and curdate() between start_date and end_date  order by added_on desc");
	  if($query->num_rows()>0)
	  {
            
		  $i=0;
		 foreach($query->result() as $val)
		 {
                    $listarr[]=$val;
					$data['resultcode']=1;
					$data['resultmsg']="Success";
			
		 }
		 $data['list']=$listarr;
	  }
	  else
	  {
		$data['resultcode']=1;
		$data['resultmsg']="No Reward Added";
	  }
	  
	  return $data;
	}
	
	  public function add_update_cafereward($jsondata)
	{
		$data=array();
		  $data['resultcode']=1;
		if($jsondata->item_name=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Item Name";
			  return $data;
		}
		if($jsondata->loyalty_points=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Loyalty Points";
			  return $data;
		}
		if($jsondata->start_date=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Start Date";
			 return $data;
		}
		if($jsondata->end_date=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter End Date";
			 return $data;
		}
	 
	 $html='';
       
		   $startdate=date("Y-m-d H:i:s",strtotime($jsondata->start_date));
		  $enddate=date("Y-m-d H:i:s",strtotime($jsondata->end_date));
		   
		  $tabledata=array("item_name"=>$jsondata->item_name,"quantity"=>1,
				   "points"=>$jsondata->loyalty_points,"added_on"=>date("Y-m-d H:i:s"), "start_date"=>$startdate,
				    "end_date"=>$enddate,
				   "loc_uid"=>$jsondata->location_uid);
                  if($jsondata->itemid!='')
                  {
                    
                     $this->DB2->update("cafe_rewards",$tabledata,array("id"=>$jsondata->itemid));
                     $id=$jsondata->itemid;
                      $is_update=1;
                  
                  }
                  else{
                     $this->DB2->insert("cafe_rewards",$tabledata);
                     $id=$this->DB2->insert_id();
                     $is_update=0;
                  }
		  
		 
		 

								  
			
								  $data['resultcode']=1;
								  $data['item']=$jsondata->item_name;
                                                                  $data['id']=$id;
                                                                  $data['points']=$jsondata->loyalty_points;
                                                                
								 
		       
		       return $data;
	}
	
     public function edit_cafereward($jsondata)
	{
	 $data=array();
	 if($jsondata->id=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Itemid";
			 return $data;
		}
	 $id=$jsondata->id;
	 $query=$this->DB2->query("select id,`item_name`,`quantity`,`points`,start_date,end_date from cafe_rewards where id='".$id."'");
	 if($query->num_rows()>0)
	 {
		 $data['resultcode']=1;
		  $rowarr=$query->row_array();
		$data['id']=  $rowarr['id'];
		$data['item_name']=  $rowarr['item_name'];
		$data['quantity']=  $rowarr['quantity'];
		$data['points']=  $rowarr['points'];
		$data['start_date']=  ($rowarr['start_date']!='')? date("d-m-Y",strtotime($rowarr['start_date'])):"";
		$data['end_date']=  ($rowarr['start_date']!='')?date("d-m-Y",strtotime($rowarr['end_date'])):"";
	 }
	 else{
		  $data['resultcode']=0;
			 $data['resultmsg']="No Item present with this id";
	 }
	 return $data;
	}
	
	 public function delete_cafereward($jsondata)
	 {
		 $data=array();
		if($jsondata->delid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Itemid to delete";
			 return $data;
		}
		 $tabledata=array("is_deleted"=>1);
		 $this->DB2->update('cafe_rewards',$tabledata,array("id"=>$jsondata->delid));
		  if ($this->DB2->affected_rows() >= 0) {
		  $data['resultcode']=1;
		  $data['resultmsg']="Item Deleted Successfully";
		}
	  else
	  {
		  $data['resultcode']=0;
		  $data['resultmsg']="Unable to Deleted Item";
		}
		return $data;
		 
	 }
	 
	 public function loyalty_memberretail($jsondata)
	 {
		  $cond='';
	 $data=array();
	 $postdata=$this->input->post();
	 $search=$jsondata->search;
	  $loc_uid=$jsondata->location_id;
	   $data['resultcode']=1;
	 if($search!='')
	 {
		$query=$this->DB2->query("select uid from sht_users where lastname  ='".$search."'" ) ;
		//echo $this->db->last_query(); die;
		if($query->num_rows()>0)
		{
		  foreach($query->result() as $val)
		  {
			$userarr[]= $val->uid;  
		  }
		  	     // $collection = $mongo_dbname->sh_user;
	       $paramid = '"' . implode('", "', $userarr) . '"';
                 //   $cond .= "AND uid IN ({$paramid})";
     $cond .= "AND crp.customer_id IN ({$paramid})";
	     
		  
		}
		else
		{
		  $userarr[]=$search;
		 $paramid = '"' . implode('", "', $userarr) . '"';
                 //   $cond .= "AND uid IN ({$paramid})";
     $cond .= "AND crp.customer_id IN ({$paramid})";
	     
		}
	 }
	 $redeemedquery=$this->DB2->query("select su.lastname,su.firstname,su.email,su.mobile,su.uid,crp.`customer_id`,ro.offer_name,crp.`location_id`,crp.`added_on`,crp.redeemed_on,crp.is_redeem,crp.offer_id from retail_offer_reddem crp
					 inner join sht_users su on (su.uid=crp.customer_id)
inner join retail_offer ro on(ro.id=crp.offer_id) 					 where crp.location_id='".$loc_uid."'  {$cond} ");
	//echo $this->DB2->last_query(); die;
	 $redeemarr= array();
	 $redlastactivty=array();
	 $i=0;
	
	
	 
	 $fnldata=array();
	 $i=0;
	 foreach($redeemedquery->result() as $vald)
	 {
		 
		
			$fnldata[$i]['lastactivity']=($vald->is_redeem==1)?date("d-m-Y",strtotime($vald->redeemed_on)):date("d-m-Y",strtotime($vald->added_on));
			$fnldata[$i]['status']=($vald->is_redeem==1)?"Coupon Redeemed":"Coupon Generated";
			$fnldata[$i]['status_color']=($vald->is_redeem==1)?"#4CAF50":"#FF9800";
			$fnldata[$i]['offer_name']=$vald->offer_name;
			$fnldata[$i]['mobile']=$vald->uid;
			$fnldata[$i]['mobile']=$vald->uid;
		  $fnldata[$i]['lastname']=$vald->lastname;
		   $fnldata[$i]['firstname']=$vald->firstname;
		    $fnldata[$i]['email']=$vald->email;
		     $fnldata[$i]['fullname']=ucwords($vald->firstname)." ".ucwords($vald->lastname);
		$i++;
	 
	 }
	// echo "<pre>"; print_R($fnldata); die;
	 
	 
	 $data['listingrecord']=$fnldata;
	
	
	return $data;
		 
	 }
	 
	 public function add_update_retailreward($jsondata)
	 {
		  $data=array();
		  if($jsondata->offer_name=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Offer Name";
			  return $data;
		}
		if($jsondata->offer_desc=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Offer Description";
			  return $data;
		}
		if($jsondata->offer_value=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Offer Value";
			  return $data;
		}
		if($jsondata->start_date=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Start Date";
			 return $data;
		}
		if($jsondata->end_date=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter End Date";
			 return $data;
		}
		if($jsondata->location_id=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Location ID";
			 return $data;
		}
	 
	 $html='';
       
		  $postdata=$this->input->post();
                   // echo "<pre>"; print_R($postdata); die;
		   $startdate=date("Y-m-d H:i:s",strtotime($jsondata->start_date));
		  $enddate=date("Y-m-d H:i:s",strtotime($jsondata->end_date));
		   $session_data = $this->session->userdata('cafelogged_in');
		   $locid=$jsondata->location_id;
		  $tabledata=array("offer_name"=>$jsondata->offer_name,"offer_desc"=>$jsondata->offer_desc,
				   "offer_value"=>$jsondata->offer_value, "start_date"=>$startdate,
				    "end_date"=>$enddate,"created_on"=>date("Y-m-d H:i:s"), "location_id"=>$locid);
                  if($jsondata->itemid!='')
                  {
                    
                     $this->DB2->update("retail_offer",$tabledata,array("id"=>$jsondata->itemid));
                     $id=$jsondata->itemid;
                      $is_update=1;
                  
                  }
                  else{
                     $this->DB2->insert("retail_offer",$tabledata);
                     $id=$this->DB2->insert_id();
                     $is_update=0;
                  }
		  
		 
					$data['resultcode']=1;
					 $data['id']=$id;
                    $data['is_update']=$is_update;
								  
		       
		       return $data;
	 }
	 
	 public function list_reward_retail($jsondata)
	 {
		  $html='';
	 
	  $loc_uid=$jsondata->location_id;
		$data['resultcode']=1;
	  
	  $query=$this->DB2->query("select id,`offer_name`,`offer_desc`,`offer_value`,start_date,end_date from retail_offer where  location_id='".$loc_uid."' and is_deleted='0' and curdate() between start_date and end_date order by created_on desc");
	   $listarr=array();
	  if($query->num_rows()>0)
	  {
		  $i=0;
		 foreach($query->result() as $val)
		 {
		    $listarr[]=$val;
			 
		 
		 }
	  }
	  $data['list']=$listarr;
	  return $data;
	 }
	 
	 public function edit_retailreward($jsondata)
	 {
		  $data=array();
	 $postdata=$this->input->post();
	 $id=$jsondata->id;
	 $data['resultcode']=1;
	 $query=$this->DB2->query("select id,`offer_name`,`offer_desc`,`offer_value`,start_date,end_date from retail_offer where id='".$id."'");
	 if($query->num_rows()>0)
	 {
		  $rowarr=$query->row_array();
		$data['id']=  $rowarr['id'];
		$data['offer_name']=  $rowarr['offer_name'];
		$data['offer_desc']=  $rowarr['offer_desc'];
		$data['offer_value']=  $rowarr['offer_value'];
		$data['start_date']=  date("d-m-Y",strtotime($rowarr['start_date']));
		$data['end_date']=  date("d-m-Y",strtotime($rowarr['end_date']));
	 }
	 return $data;
		 
	 }
	 
	 public function delete_retailreward($jsondata)
	 {
		 $postdata=$this->input->post();
		 $tabledata=array("is_deleted"=>1);
		 $this->DB2->update('retail_offer',$tabledata,array("id"=>$jsondata->delid));
		  if ($this->DB2->affected_rows() >= 0) {
		  $data['resultcode']=1;
		}
	  else
	  {
		  $data['resultcode']=0;
		}
		return $data; 
	 }
	 
	 public function hotel_active_guestcount($jsondata)
	 {
		 $data=array();
		   $loc_uid=$jsondata->location_uid;
	 $query=$this->DB2->query("select id from ht_user_detail WHERE `end_date`>=now() and location_uid='".$loc_uid."' ");
	 $data['resultcode']=1;
	  $data['usercount']=$query->num_rows();
	 return $data;
	 }
	 
	 public function hotel_active_guestlist($jsondata)
	{
	  
	   $loc_uid=$jsondata->location_uid;
	 $postdata=$this->input->post();
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.room_no='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`room_no`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN date(hud.start_date) AND date(hud.end_date)) AS countuser 
FROM `ht_user_detail` hud WHERE hud.`end_date`>=now() and hud.location_uid='".$loc_uid."' {$cond}");
$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
										      $i=1;	
	    
	    
		  foreach($query->result() as $vald)
		  {
			  $class=($vald->countuser>0)?"in_use":"small_unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   $date=date("M d Y, H:i",strtotime($vald->activated_on));
			  $listarr[$i]['text']=$txt;
			   $listarr[$i]['date']=$date;
			   $listarr[$i]['id']=$vald->id;
			    $listarr[$i]['lastname']=$vald->lastname;
				 $listarr[$i]['mobile']=$vald->mobile;
				  $listarr[$i]['room_no']=$vald->room_no;
			  
			 $i++;  
			  
		  }
		   
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		   $data['list']=$listarr;
		 
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Active Guest";
		  
		  
	 }
	 return $data;
	 
	}
	
	public function hotel_editguest($jsondata)
	{
		$postdata=$this->input->post();
	$query=$this->DB2->query("SELECT id,`firstname`,`lastname`,`mobile`,`plan_id`,`room_no`,`start_date`,`end_date`,devices,age,gender,email FROM ht_user_detail where id='".$jsondata->id."'");
	//echo $this->db->last_query(); die;
	$rowarr=$query->row_array();
	 $data['resultcode']=1;
	 $data['id']=$rowarr['id'];
	$data['firstname']=$rowarr['firstname'];
	$data['lastname']=$rowarr['lastname'];
	$data['mobile']=$rowarr['mobile'];
	$data['plan_id']=$rowarr['plan_id'];
	$data['room_no']=$rowarr['room_no'];
	$data['devices']=$rowarr['devices'];
	$data['email']=$rowarr['email'];
	$data['age']=$rowarr['age'];
	$data['gender']=$rowarr['gender'];
	$data['start_date']=date("d-m-Y H:i",strtotime($rowarr['start_date']));
	$data['end_date']=date("d-m-Y H:i",strtotime($rowarr['end_date']));
	
	$loc_uid=$jsondata->location_uid;
	 $query= $this->DB2->query("SELECT ss.`srvid`,ss.`srvname` FROM `sht_services` ss
	 INNER JOIN `ht_planassociation` hp ON (ss.srvid=hp.plan_id) WHERE hp.location_uid='".$loc_uid."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['planid']=$val->srvid;
		 $planlistarr[$i]['planname']=$val->srvname;
		 $i++;
	 }
	}
	$data['planlist']=$planlistarr;
	return $data;
	}
	
	public function hotelplan_list($jsondata)
	{
		 $data['resultcode']=1;
		$loc_uid=$jsondata->location_uid;
	 $query= $this->DB2->query("SELECT ss.`srvid`,ss.`srvname` FROM `sht_services` ss
	 INNER JOIN `ht_planassociation` hp ON (ss.srvid=hp.plan_id) WHERE hp.location_uid='".$loc_uid."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['planid']=$val->srvid;
		 $planlistarr[$i]['planname']=$val->srvname;
		 $i++;
	 }
	}
	$data['planlist']=$planlistarr;
	return $data;
	}
	
	public function hoteladdupdate_guest_detail($jsondata)
	{
		
		 $data=array();
		  if($jsondata->planid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Assign aplan";
			  return $data;
		}
		if($jsondata->email=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Email";
			  return $data;
		}
		if($jsondata->lastname=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Lastname";
			  return $data;
		}
		if($jsondata->age=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Select Age group";
			 return $data;
		}
		if($jsondata->gender=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Select Gender";
			 return $data;
		}
		if($jsondata->roomno=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter Room no";
			 return $data;
		}
		if($jsondata->devices=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Select no of devices";
			 return $data;
		}
		if($jsondata->checkin=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter CheckIN date";
			 return $data;
		}
		if($jsondata->checkout=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter CheckOut date";
			 return $data;
		}
		
		if($jsondata->location_id=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Location ID";
			 return $data;
		}
	
	
	   $loc_uid=$jsondata->location_uid;
	   
	   $ispque=$this->DB2->query("select isp_uid from wifi_location where location_uid='".$loc_uid."'");
	   $isparr=$ispque->row_array();
	   $ispuid=$isparr['isp_uid'];
	 $postdata=$this->input->post();
	 $checkin = date("Y-m-d H:i:s",strtotime($jsondata->checkin.":00"));
         $checkout = date("Y-m-d H:i:s",strtotime($jsondata->checkout.":00"));
		 $time=strtotime($jsondata->checkout)-strtotime($jsondata->checkin);
		 if($jsondata->mobileno!=''){
			 $jsondata->mobileno=$jsondata->mobileno;
		 }
		 else{
			  $jsondata->mobileno=$this->random_uid();
		 }
		

	 $planquery=$this->DB2->query("select plantype,datalimit from sht_services where srvid='".$jsondata->planid."'");
	// echo $this->db->last_query();die;
	if($planquery->num_rows()>0)
	{
	 
	 $rowarr=$planquery->row_array();
	 if($rowarr['plantype']==2)
	 {
				$daysno = ceil(abs($checkin - $checkout) / 86400)+1;
				$time=strtotime($jsondata->checkout)-strtotime($jsondata->checkin);
				$tabledata1=array("uptimelimit"=>$time,"comblimit"=>"0");
	 }
	 else
	 {
			$datalimit=$rowarr['datalimit'];
			$tabledata1=array("comblimit"=>$datalimit,"uptimelimit"=>0);
	 }
	 $pwd=md5($jsondata->roomno);
	 $userquery=$this->DB2->query("select id from sht_users where username='".$jsondata->mobileno."'");
	// echo $this->db->last_query(); die;
	$tabledata=array("isp_uid"=>$ispuid,"uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,
			  "password"=>$pwd,"enableuser"=>1,"baseplanid"=>$jsondata->planid,"email"=>$jsondata->email, "firstname"=>$jsondata->firstname
			  ,"lastname"=>$jsondata->lastname,"age_group"=>$jsondata->age,"gender"=>$jsondata->gender,"mobile"=>$jsondata->mobileno,
			  "createdby"=>"admin","owner"=>"admin","expiration"=>$checkout);
	$tabledata=array_merge($tabledata,$tabledata1);
	 if($userquery->num_rows()>0)
	 {
		 $userarr= $userquery->row_array();
		 $this->DB2->where('id', $userarr['id']);
            $this->DB2->update('sht_users', $tabledata);
	    //echo $this->db->last_query(); die;
	    $tabledata2=array("value"=>$jsondata->roomno);
	     $this->DB2->update('radcheck', $tabledata2, array('username' => $jsondata->mobileno,"attribute"=>"Cleartext-Password"));
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->update("radcheck",$tabledata3, array('username' => $jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	 }
	 else
	 {
		  $this->DB2->insert("sht_users",$tabledata);
		  $tabledata2=array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password","op"=>":=",
				      "value"=>$jsondata->roomno);
		   $this->DB2->insert("radcheck",$tabledata2);
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->insert("radcheck",$tabledata3);
	 }
	
	// `firstname`,`lastname`,`mobile`,`plan_id`,`room_no`,`isp_uid`,`location_uid`,`start_date`,`end_date`,`created_on`
	 $tabledata4=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "plan_id"=>$jsondata->planid,"age"=>$jsondata->age,"gender"=>$jsondata->gender,"email"=>$jsondata->email,"room_no"=>$jsondata->roomno,
			  "location_uid"=>$loc_uid,"location_id"=>$jsondata->location_id,"start_date"=>$checkin,"end_date"=>$checkout,"devices"=>$jsondata->devices
			  ,"created_on"=>date("Y-m-d H:i:s"));
	 if($jsondata->userid!='')
	 {
		   $id=$this->DB2->update("ht_user_detail",$tabledata4,array("id"=>$jsondata->userid));  
		  
	 }
	 else
	 {
	    
		 $id=$this->DB2->insert("ht_user_detail",$tabledata4);
	}
	
	 if($id)
	 {
		  $data['resultcode']=1;
		  $data['resultmsg']="Added Guest";
		  $data['username']=$jsondata->lastname;
	 }
	 else
	 {
		  $data['resultcode']=0;
		  $data['resultmsg']="Unable to add guest";
		  $data['username']=$jsondata->lastname;
	 
	 }
	 
	 
	 
	}
	else
	{
	 $data['resultcode']=0;
	 $data['resultmsg']="No Such plan exists";
	}
	 
	return $data;
	}
	
	
	public function hotelupdate_exit_detail($jsondata)
	{
		
		 $postdata=$this->input->post();
	 
	 $tabledata=array("firstname"=>$jsondata->firstname,"age"=>$jsondata->age,"gender"=>$jsondata->gender,"lastname"=>$jsondata->lastname,
			  "mobile"=>$jsondata->mobileno,"room_no"=>$jsondata->roomno,"email"=>$jsondata->email);
	 $this->DB2->update("ht_user_detail",$tabledata,array("id"=>$jsondata->userid));
	
	 $pwd=md5($jsondata->roomno);
	 $tabledata1=array("uid"=>$jsondata->mobileno,"gender"=>$jsondata->gender,"age_group"=>$jsondata->age,"username"=>$jsondata->mobileno,"password"=>$pwd,
			   "mobile"=>$jsondata->mobileno,"email"=>$jsondata->email);
	  $this->DB2->update("sht_users",$tabledata1,array("uid"=>$jsondata->mobileno));
	  
	   $tabledata2=array("username"=>$jsondata->mobileno,"value"=>$jsondata->roomno,
			    "op"=>":=","attribute"=>"Cleartext-Password");
	  $this->DB2->update("radcheck",$tabledata2,array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password"));
	  
	  $tabledata3=array("username"=>$jsondata->mobileno,"value"=>1,
			    "op"=>":=","attribute"=>"Simultaneous-Use");
	  $this->DB2->update("radcheck",$tabledata3,array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	  
		  $data['resultcode']=1;
		$data['resultmsg']="Guest Updated";
		return $data;
		
	}
	
	public function hotel_guest_setting_add($jsondata)
	{
		$data=array();
		 $locid=$jsondata->location_id;
		$isenable=$jsondata->is_enable;
		 $tabledata=array("plan_id"=>$jsondata->plan_id,"plan_hour"=>$jsondata->plan_hour,
				  "is_enabled"=>$isenable,"location_id"=>$locid);
		 $query=$this->DB2->query("select id from ht_guest_setting where location_id='".$locid."'");
		 if($query->num_rows()>0)
		 {
			  $this->DB2->update('ht_guest_setting',$tabledata,array("location_id"=>$locid));
		 $data['resultcode']=1;
		 
		 }else
		 {
			  $this->DB2->insert('ht_guest_setting',$tabledata);
			 $data['resultcode']=1;
		 }
		 return $data;
	}
	
	 public function hotel_guest_setting($jsondata)
	{
	 $data=array();
	
	$locid=$jsondata->location_id;
	$query=$this->DB2->query("select `plan_id`,`plan_hour`,`is_enabled` from ht_guest_setting where location_id='".$locid."'");
	 if($query->num_rows()>0)
	 {
		   $rowarr=$query->row_array();
		  $data['resultcode']=1;
		  $data['plan_id']=$rowarr['plan_id'];
		  $data['plan_hour']=$rowarr['plan_hour'];
		  $data['is_enabled']=$rowarr['is_enabled'];
	}
	 else
	 {
		   $data['resultcode']=0;
	}
	 return $data;
	}
	
	public function hotel_expired_user($jsondata)
	{
		 
	 $data=array();
	   $loc_uid=$jsondata->location_uid;
	 
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.room_no='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`room_no`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND date(ra.`acctstarttime`)
 BETWEEN date(hud.start_date) AND date(hud.end_date)) AS countuser 
FROM `ht_user_detail` hud WHERE hud.`end_date`< NOW() and hud.location_uid='".$loc_uid."' {$cond}");

$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
		$i=1;	
		  foreach($query->result() as $vald)
		  {
			   $class=($vald->countuser>0)?"in_use":"unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   
			      $listarr[$i]['txt']=$txt;
			      $listarr[$i]['lastname']=$vald->lastname;
			      $listarr[$i]['mobile']=$vald->mobile;
			      $listarr[$i]['room_no']=$vald->room_no;
			      $listarr[$i]['activated_on']=$vald->activated_on;
			      if($class == 'in_use'){
				  $listarr[$i]['color']='#39b54a'; 
			      }else{
				   $listarr[$i]['color']='#f4474a'; 
			      }
			   
			   
			   
			 $i++;
		  }
		
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		  $data['list']=$listarr;
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Expired Guests";
		 $data['list']=$listarr;
		  
	 }
	 
	 return $data;
	}
	
	public function hotel_delete_user($jsondata)
	{
		$location_uid=$jsondata->location_uid;
		$this->DB2->delete('ht_user_detail',array('mobile'=>$jsondata->mobile,'location_uid'=>$location_uid));
		
		$this->DB2->delete('sht_users',array('mobile'=>$jsondata->mobile));
		
		$this->DB2->delete('radcheck',array('username'=>$jsondata->mobile));
		
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		return $data;
	}
	
	public function mail_chimp_settingdata($jsondata)
	{
		 $data=array();
	
	$locid=$jsondata->location_id;
	$query=$this->DB2->query("select `apikey`,`listid`,`is_enable` from location_mailchimp_detail where locationid='".$locid."'");
	 $listarr=array();
	 if($query->num_rows()>0)
	 {
		   $rowarr=$query->row_array();
		  $data['resultcode']=1;
		  $data['apikey']=$rowarr['apikey'];
		  $data['listid']=$rowarr['listid'];
		  $url = 'https://' . substr($data['apikey'],strpos($data['apikey'],'-')+1) . '.api.mailchimp.com/3.0/lists/';
	 $result = json_decode($this->mailchimp_curl_connect( $url, 'GET', $data['apikey'], $data) );
	 //print_r( $result);
	
	  $i=0;
	 if( !empty($result->lists) ) {
		
		 foreach( $result->lists as $list ){
		    $listarr[$i]['listid']=$list->id ;
		    $listarr[$i]['listname']=$list->name ;
		$i++;
		 }
		 
	 }
		  $data['listarr']=$listarr;
		  $data['is_enable']=$rowarr['is_enable'];
		 
	 }
	 else
	 {
		   $data['resultcode']=0;
		   $data['apikey']='';
		  $data['listid']='';
		  $data['listarr']=$listarr;
		  $data['is_enable']='';
		
	 }
	 return $data;
	}
	
	
	public function mailchimp_curl_connect( $url, $request_type, $api_key, $data = array() ) {
	if( $request_type == 'GET' )
		$url .= '?' . http_build_query($data);
 
	$mch = curl_init();
	$headers = array(
		'Content-Type: application/json',
		'Authorization: Basic '.base64_encode( 'user:'. $api_key )
	);
	curl_setopt($mch, CURLOPT_URL, $url );
	curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
	//curl_setopt($mch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($mch, CURLOPT_RETURNTRANSFER, true); // do not echo the result, write it into variable
	curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
	curl_setopt($mch, CURLOPT_TIMEOUT, 10);
	curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false); // certificate verification for TLS/SSL connection
 
	if( $request_type != 'GET' ) {
		curl_setopt($mch, CURLOPT_POST, true);
		curl_setopt($mch, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
	}
 
	return curl_exec($mch);
}

public function mailchimp_list($jsondata)
	{
	 $data=array();
	 
	 $api_key=$jsondata->apikey;
	  $data=array();
	$session_data = $this->session->userdata('logged_in');
	$locid=$jsondata->location_id;
	$listid="";
	$query=$this->DB2->query("select `apikey`,`listid`,`is_enable` from location_mailchimp_detail where locationid='".$locid."'");
	 if($query->num_rows()>0)
	 {
		   $rowarr=$query->row_array();
		  $listid=$rowarr['listid'];
		  
		
		 
	 }
	 
	 $url = 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/';
	 $result = json_decode($this->mailchimp_curl_connect( $url, 'GET', $api_key, $data) );
	 //print_r( $result);
	  $html="";
	  $listarr=array();
	   $html.="<option value=''>Select Listname</option>";
	 if( !empty($result->lists) ) {
		$i=1;
		 foreach( $result->lists as $list ){
			 $listarr[$i]['listid']=$list->id;
			 $listarr[$i]['listname']=$list->name;
		$i++;
		 }
		 
	 }
	 $data['resultcode']=1;
	 $data['list']=$listarr;
	
	 return $data;
	}
	
	 public function mail_chimp_settingadd($jsondata)
	{
	 
	 $locid=$jsondata->location_id;
	 $location_uid=$jsondata->location_uid;
	
	 $tabledata=array("apikey"=>$jsondata->apikey,"listid"=>$jsondata->listid,
			  "is_enable"=>$jsondata->is_enable,"locationid"=>$locid,"location_uid"=>$location_uid);
	 $query=$this->DB2->query("select id from location_mailchimp_detail where locationid='".$locid."'");
	 if($query->num_rows()>0)
	 {
		  $this->DB2->update('location_mailchimp_detail',$tabledata,array("locationid"=>$locid));
	 $data['resultcode']=1;
	 
	 }else
	 {
		  $this->DB2->insert('location_mailchimp_detail',$tabledata);
		 // echo $this->db->last_query(); die;
		  $data['resultcode']=1;
	 }
	 return $data;
	}
	
	 public function hospital_active_guestcount($jsondata)
	 {
		 $data=array();
		   $loc_uid=$jsondata->location_uid;
	 $query=$this->DB2->query("select id from hsptl_user_detail WHERE `end_date`>NOW()  and location_uid='".$loc_uid."' ");
	 $data['resultcode']=1;
	  $data['usercount']=$query->num_rows();
	 return $data;
	 }
	 
	 public function hospital_active_guestlist($jsondata)
	{
	  
	   $loc_uid=$jsondata->location_uid;
	 $postdata=$this->input->post();
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$postdata['search']}%' or hud.room_no='".$postdata['search']."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`room_no`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN hud.start_date AND hud.end_date) AS countuser 
FROM `hsptl_user_detail` hud WHERE hud.`end_date`>NOW() and hud.location_uid='".$loc_uid."' {$cond}");
$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
										      $i=1;	
	    
	    
		  foreach($query->result() as $vald)
		  {
			  $class=($vald->countuser>0)?"in_use":"small_unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   $date=$vald->activated_on;
			  $listarr[$i]['text']=$txt;
			   $listarr[$i]['date']=$date;
			   $listarr[$i]['id']=$vald->id;
			    $listarr[$i]['lastname']=$vald->lastname;
				 $listarr[$i]['mobile']=$vald->mobile;
				  $listarr[$i]['room_no']=$vald->room_no;
			  
			 $i++;  
			  
		  }
		   
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		   $data['list']=$listarr;
		 
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Active Guest";
		  
		  
	 }
	 return $data;
	 
	}
	
	public function hospital_editguest($jsondata)
	{
		$postdata=$this->input->post();
	$query=$this->DB2->query("SELECT id,`firstname`,`lastname`,`mobile`,`plan_id`,`room_no`,`start_date`,`end_date`,devices FROM hsptl_user_detail where id='".$jsondata->id."'");
	//echo $this->db->last_query(); die;
	$rowarr=$query->row_array();
	 $data['resultcode']=1;
	 $data['id']=$rowarr['id'];
	$data['firstname']=$rowarr['firstname'];
	$data['lastname']=$rowarr['lastname'];
	$data['mobile']=$rowarr['mobile'];
	$data['plan_id']=$rowarr['plan_id'];
	$data['room_no']=$rowarr['room_no'];
	$data['devices']=$rowarr['devices'];
	$data['start_date']=date("d-m-Y H:i",strtotime($rowarr['start_date']));
	$data['end_date']=date("d-m-Y H:i",strtotime($rowarr['end_date']));
	
	$loc_uid=$jsondata->location_uid;
	 $query= $this->DB2->query("SELECT ss.`srvid`,ss.`srvname` FROM `sht_services` ss
	 INNER JOIN `ht_planassociation` hp ON (ss.srvid=hp.plan_id) WHERE hp.location_uid='".$loc_uid."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['planid']=$val->srvid;
		 $planlistarr[$i]['planname']=$val->srvname;
		 $i++;
	 }
	}
	$data['planlist']=$planlistarr;
	return $data;
	}
	
	public function hospitalplan_list($jsondata)
	{
		 $data['resultcode']=1;
		$loc_uid=$jsondata->location_uid;
	 $query= $this->DB2->query("SELECT ss.`srvid`,ss.`srvname` FROM `sht_services` ss
	 INNER JOIN `ht_planassociation` hp ON (ss.srvid=hp.plan_id) WHERE hp.location_uid='".$loc_uid."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['planid']=$val->srvid;
		 $planlistarr[$i]['planname']=$val->srvname;
		 $i++;
	 }
	}
	$data['planlist']=$planlistarr;
	return $data;
	}
	
	
	public function hospitaladdupdate_guest_detail($jsondata)
	{
		
		 $data=array();
		  if($jsondata->planid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Assign aplan";
			  return $data;
		}
		
		if($jsondata->lastname=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Lastname";
			  return $data;
		}
		
		if($jsondata->roomno=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter Room no";
			 return $data;
		}
		if($jsondata->devices=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Select no of devices";
			 return $data;
		}
		if($jsondata->checkin=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter CheckIN date";
			 return $data;
		}
		if($jsondata->checkout=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter CheckOut date";
			 return $data;
		}
		
		if($jsondata->location_id=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Location ID";
			 return $data;
		}
	
	
	   $loc_uid=$jsondata->location_uid;
	   
	   $ispque=$this->DB2->query("select isp_uid from wifi_location where location_uid='".$loc_uid."'");
	   $isparr=$ispque->row_array();
	   $ispuid=$isparr['isp_uid'];
	 $postdata=$this->input->post();
	 $checkin = date("Y-m-d",strtotime($jsondata->checkin));
         $checkout = date("Y-m-d",strtotime($jsondata->checkout));
		 $time=strtotime($jsondata->checkout)-strtotime($jsondata->checkin);
		 if($jsondata->mobileno!=''){
			 $jsondata->mobileno=$jsondata->mobileno;
		 }
		 else{
			  $jsondata->mobileno=$this->random_uid();
		 }
		

	 $planquery=$this->DB2->query("select plantype,datalimit from sht_services where srvid='".$jsondata->planid."'");
	// echo $this->db->last_query();die;
	if($planquery->num_rows()>0)
	{
	 
	 $rowarr=$planquery->row_array();
	 if($rowarr['plantype']==2)
	 {
				$daysno = ceil(abs($checkin - $checkout) / 86400)+1;
		  $time=$daysno*24*3600;
				$tabledata1=array("uptimelimit"=>$time,"comblimit"=>"0");
	 }
	 else
	 {
			$datalimit=$rowarr['datalimit'];
			$tabledata1=array("comblimit"=>$datalimit,"uptimelimit"=>0);
	 }
	 $pwd=md5($jsondata->roomno);
	 $userquery=$this->DB2->query("select id from sht_users where username='".$jsondata->mobileno."'");
	// echo $this->db->last_query(); die;
	$tabledata=array("isp_uid"=>$ispuid,"uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,
			  "password"=>$pwd,"enableuser"=>1,"baseplanid"=>$jsondata->planid, "firstname"=>$jsondata->firstname
			  ,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "createdby"=>"admin","owner"=>"admin","expiration"=>$checkout);
	$tabledata=array_merge($tabledata,$tabledata1);
	 if($userquery->num_rows()>0)
	 {
		 $userarr= $userquery->row_array();
		 $this->DB2->where('id', $userarr['id']);
            $this->DB2->update('sht_users', $tabledata);
	    //echo $this->db->last_query(); die;
	    $tabledata2=array("value"=>$jsondata->roomno);
	     $this->DB2->update('radcheck', $tabledata2, array('username' => $jsondata->mobileno,"attribute"=>"Cleartext-Password"));
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->update("radcheck",$tabledata3, array('username' => $jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	 }
	 else
	 {
		  $this->DB2->insert("sht_users",$tabledata);
		  $tabledata2=array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password","op"=>":=",
				      "value"=>$jsondata->roomno);
		   $this->DB2->insert("radcheck",$tabledata2);
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->insert("radcheck",$tabledata3);
	 }
	
	// `firstname`,`lastname`,`mobile`,`plan_id`,`room_no`,`isp_uid`,`location_uid`,`start_date`,`end_date`,`created_on`
	 $tabledata4=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "plan_id"=>$jsondata->planid,"room_no"=>$jsondata->roomno,
			  "location_uid"=>$loc_uid,"location_id"=>$jsondata->location_id,"start_date"=>$checkin,"end_date"=>$checkout,"devices"=>$jsondata->devices
			  ,"created_on"=>date("Y-m-d H:i:s"));
	 if($jsondata->userid!='')
	 {
		   $id=$this->DB2->update("hsptl_user_detail",$tabledata4,array("id"=>$jsondata->userid));  
		  
	 }
	 else
	 {
	    
		 $id=$this->DB2->insert("hsptl_user_detail",$tabledata4);
	}
	
	 if($id)
	 {
		  $data['resultcode']=1;
		  $data['resultmsg']="Added Guest";
		  $data['username']=$jsondata->lastname;
	 }
	 else
	 {
		  $data['resultcode']=0;
		  $data['resultmsg']="Unable to add guest";
		  $data['username']=$jsondata->lastname;
	 
	 }
	 
	 
	 
	}
	else
	{
	 $data['resultcode']=0;
	 $data['resultmsg']="No Such plan exists";
	}
	 
	return $data;
	}
	
	public function hospitalupdate_exit_detail($jsondata)
	{
		
		 $postdata=$this->input->post();
	 
	 $tabledata=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,
			  "mobile"=>$jsondata->mobileno,"room_no"=>$jsondata->roomno);
	 $this->DB2->update("hsptl_user_detail",$tabledata,array("id"=>$jsondata->userid));
	
	 $pwd=md5($jsondata->roomno);
	 $tabledata1=array("uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,"password"=>$pwd,
			   "mobile"=>$jsondata->mobileno);
	  $this->DB2->update("sht_users",$tabledata1,array("uid"=>$jsondata->mobileno));
	  
	   $tabledata2=array("username"=>$jsondata->mobileno,"value"=>$jsondata->roomno,
			    "op"=>":=","attribute"=>"Cleartext-Password");
	  $this->DB2->update("radcheck",$tabledata2,array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password"));
	  
	  $tabledata3=array("username"=>$jsondata->mobileno,"value"=>1,
			    "op"=>":=","attribute"=>"Simultaneous-Use");
	  $this->DB2->update("radcheck",$tabledata3,array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	  
		  $data['resultcode']=1;
		$data['resultmsg']="Patient Updated";
		return $data;
		
	}
	
	public function hospital_guest_setting_add($jsondata)
	{
		$data=array();
		 $locid=$jsondata->location_id;
		$isenable=$jsondata->is_enable;
		 $tabledata=array("plan_id"=>$jsondata->plan_id,"plan_hour"=>$jsondata->plan_hour,
				  "is_enabled"=>$isenable,"location_id"=>$locid);
		 $query=$this->DB2->query("select id from hspt_guest_setting where location_id='".$locid."'");
		 if($query->num_rows()>0)
		 {
			
			  $this->DB2->update('hspt_guest_setting',$tabledata,array("location_id"=>$locid));
			
		 $data['resultcode']=1;
		 
		 }else
		 {
			  $this->DB2->insert('hspt_guest_setting',$tabledata);
			 $data['resultcode']=1;
		 }
		 return $data;
	}
	
	 public function hospital_guest_setting($jsondata)
	{
	 $data=array();
	
	$locid=$jsondata->location_id;
	$query=$this->DB2->query("select `plan_id`,`plan_hour`,`is_enabled` from hspt_guest_setting where location_id='".$locid."'");
	 if($query->num_rows()>0)
	 {
		   $rowarr=$query->row_array();
		  $data['resultcode']=1;
		  $data['plan_id']=$rowarr['plan_id'];
		  $data['plan_hour']=$rowarr['plan_hour'];
		  $data['is_enabled']=$rowarr['is_enabled'];
	}
	 else
	 {
		   $data['resultcode']=0;
	}
	 return $data;
	}
	
	public function hospital_expired_user($jsondata)
	{
		 
	 $data=array();
	   $loc_uid=$jsondata->location_uid;
	 
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.room_no='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`room_no`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN hud.start_date AND hud.end_date) AS countuser 
FROM `hsptl_user_detail` hud WHERE hud.`end_date`< NOW() and hud.location_uid='".$loc_uid."' {$cond}");

$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
		$i=1;	
		  foreach($query->result() as $vald)
		  {
			   $class=($vald->countuser>0)?"in_use":"unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   
			      $listarr[$i]['txt']=$txt;
			      $listarr[$i]['lastname']=$vald->lastname;
			      $listarr[$i]['mobile']=$vald->mobile;
			      $listarr[$i]['room_no']=$vald->room_no;
			      $listarr[$i]['activated_on']=$vald->activated_on;
			      if($class == 'in_use'){
				  $listarr[$i]['color']='#39b54a'; 
			      }else{
				   $listarr[$i]['color']='#f4474a'; 
			      }
			   
			   
			   
			 $i++;
		  }
		
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		  $data['list']=$listarr;
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Expired Guests";
		 $data['list']=$listarr;
		  
	 }
	 
	 return $data;
	}
	
	public function hospital_delete_user($jsondata)
	{
		$location_uid=$jsondata->location_uid;
		$this->DB2->delete('hsptl_user_detail',array('mobile'=>$jsondata->mobile,'location_uid'=>$location_uid));
		
		$this->DB2->delete('sht_users',array('mobile'=>$jsondata->mobile));
		
		$this->DB2->delete('radcheck',array('username'=>$jsondata->mobile));
		
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		return $data;
	}
	
	 public function enterprise_active_guestcount($jsondata)
	 {
		 $data=array();
		   $loc_uid=$jsondata->location_uid;
	 $query=$this->DB2->query("select id from entr_user_detail WHERE `end_date`>NOW() and location_uid='".$loc_uid."' ");
	 $data['resultcode']=1;
	  $data['usercount']=$query->num_rows();
	 return $data;
	 }
	 
	  public function enterprise_active_guestlist($jsondata)
	{
	  
	   $loc_uid=$jsondata->location_uid;
	 $postdata=$this->input->post();
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.employeeid='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`employeeid`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN hud.start_date AND hud.end_date) AS countuser 
FROM `entr_user_detail` hud WHERE hud.`end_date`>NOW() and hud.location_uid='".$loc_uid."' {$cond}");
$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
										      $i=1;	
	    
	    
		  foreach($query->result() as $vald)
		  {
			  $class=($vald->countuser>0)?"in_use":"small_unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   $date=$vald->activated_on;
			  $listarr[$i]['text']=$txt;
			   $listarr[$i]['date']=$date;
			   $listarr[$i]['id']=$vald->id;
			    $listarr[$i]['lastname']=$vald->lastname;
				 $listarr[$i]['mobile']=$vald->mobile;
				  $listarr[$i]['employeeid']=$vald->employeeid;
			  
			 $i++;  
			  
		  }
		   
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		   $data['list']=$listarr;
		 
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Active Guest";
		  
		  
	 }
	 return $data;
	 
	}
	
	public function enterprise_editguest($jsondata)
	{
		//$postdata=$this->input->post();
	$query=$this->DB2->query("SELECT id,`firstname`,`lastname`,`mobile`,`plan_id`,`usertype`,`department`,`employeeid`,`start_date`,`end_date`,devices FROM entr_user_detail where id='".$jsondata->id."'");
	//echo $this->db->last_query(); die;
	$rowarr=$query->row_array();
	$rowarr=$query->row_array();
	 $data['resultcode']=1;
	 $data['id']=$rowarr['id'];
	$data['firstname']=$rowarr['firstname'];
	$data['lastname']=$rowarr['lastname'];
	$data['mobile']=$rowarr['mobile'];
	$data['plan_id']=$rowarr['plan_id'];
	$data['employeeid']=$rowarr['employeeid'];
	$data['usertype']=$rowarr['usertype'];
	$data['department']=$rowarr['department'];
	$data['devices']=$rowarr['devices'];
	$data['start_date']=date("d-m-Y",strtotime($rowarr['start_date']));
	$data['end_date']=date("d-m-Y",strtotime($rowarr['end_date']));
	
	
	$loc_uid=$jsondata->location_uid;
	 $query= $this->DB2->query("SELECT ss.`srvid`,ss.`srvname` FROM `sht_services` ss
	 INNER JOIN `ht_planassociation` hp ON (ss.srvid=hp.plan_id) WHERE hp.location_uid='".$loc_uid."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['planid']=$val->srvid;
		 $planlistarr[$i]['planname']=$val->srvname;
		 $i++;
	 }
	}
	$data['planlist']=$planlistarr;
	return $data;
	}
	
	public function enterpriseaddupdate_guest_detail($jsondata)
	{
		
		 $data=array();
		  if($jsondata->planid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Assign aplan";
			  return $data;
		}
		
		if($jsondata->lastname=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Lastname";
			  return $data;
		}
		
		if($jsondata->employeeid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter employee";
			 return $data;
		}
		if($jsondata->devices=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Select no of devices";
			 return $data;
		}
		
		
		if($jsondata->location_id=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Location ID";
			 return $data;
		}
	
	
	   $loc_uid=$jsondata->location_uid;
	   
	   $ispque=$this->DB2->query("select isp_uid from wifi_location where location_uid='".$loc_uid."'");
	   $isparr=$ispque->row_array();
	   $ispuid=$isparr['isp_uid'];
	 $postdata=$this->input->post();
	 $checkin = date("Y-m-d");
         $checkout =  date("Y-m-d",strtotime('+10 year'));
		$time=3650*24*3600;
		 if($jsondata->mobileno!=''){
			 $jsondata->mobileno=$jsondata->mobileno;
		 }
		 else{
			  $jsondata->mobileno=$this->random_uid();
		 }
		

	 $planquery=$this->DB2->query("select plantype,datalimit from sht_services where srvid='".$jsondata->planid."'");
	// echo $this->db->last_query();die;
	if($planquery->num_rows()>0)
	{
	 
	 $rowarr=$planquery->row_array();
	 if($rowarr['plantype']==2)
	 {
				$daysno = ceil(abs($checkin - $checkout) / 86400)+1;
		 $time=3650*24*3600;
				$tabledata1=array("uptimelimit"=>$time,"comblimit"=>"0");
	 }
	 else
	 {
			$datalimit=$rowarr['datalimit'];
			$tabledata1=array("comblimit"=>$datalimit,"uptimelimit"=>0);
	 }
	 $pwd=md5($jsondata->employeeid);
	 $userquery=$this->DB2->query("select id from sht_users where username='".$jsondata->mobileno."'");
	// echo $this->db->last_query(); die;
	$tabledata=array("isp_uid"=>$ispuid,"uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,
			  "password"=>$pwd,"enableuser"=>1,"baseplanid"=>$jsondata->planid, "firstname"=>$jsondata->firstname
			  ,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "createdby"=>"admin","owner"=>"admin","expiration"=>$checkout);
	$tabledata=array_merge($tabledata,$tabledata1);
	 if($userquery->num_rows()>0)
	 {
		 $userarr= $userquery->row_array();
		 $this->DB2->where('id', $userarr['id']);
            $this->DB2->update('sht_users', $tabledata);
	    //echo $this->db->last_query(); die;
	    $tabledata2=array("value"=>$jsondata->employeeid);
	     $this->DB2->update('radcheck', $tabledata2, array('username' => $jsondata->mobileno,"attribute"=>"Cleartext-Password"));
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->update("radcheck",$tabledata3, array('username' => $jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	 }
	 else
	 {
		  $this->DB2->insert("sht_users",$tabledata);
		  $tabledata2=array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password","op"=>":=",
				      "value"=>$jsondata->roomno);
		   $this->DB2->insert("radcheck",$tabledata2);
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->insert("radcheck",$tabledata3);
	 }
	
	// `firstname`,`lastname`,`mobile`,`plan_id`,`room_no`,`isp_uid`,`location_uid`,`start_date`,`end_date`,`created_on`
	 $tabledata4=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "plan_id"=>$jsondata->planid,"employeeid"=>$jsondata->employeeid,"usertype"=>$jsondata->usertype,
			  "location_uid"=>$loc_uid,"location_id"=>$jsondata->location_id,"start_date"=>$checkin,"end_date"=>$checkout,"devices"=>$jsondata->devices
			  ,"created_on"=>date("Y-m-d H:i:s"));
	 if($jsondata->userid!='')
	 {
		   $id=$this->DB2->update("entr_user_detail",$tabledata4,array("id"=>$jsondata->userid));  
		  
	 }
	 else
	 {
	    
		 $id=$this->DB2->insert("entr_user_detail",$tabledata4);
	}
	
	 if($id)
	 {
		  $data['resultcode']=1;
		  $data['resultmsg']="Added Guest";
		  $data['username']=$jsondata->lastname;
	 }
	 else
	 {
		  $data['resultcode']=0;
		  $data['resultmsg']="Unable to add guest";
		  $data['username']=$jsondata->lastname;
	 
	 }
	 
	 
	 
	}
	else
	{
	 $data['resultcode']=0;
	 $data['resultmsg']="No Such plan exists";
	}
	 
	return $data;
	}
	
	public function enterpriseupdate_exit_detail($jsondata)
	{
		
		 $postdata=$this->input->post();
	 
	 $tabledata=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,
			  "mobile"=>$jsondata->mobileno,"employeeid"=>$jsondata->employeeid);
	 $this->DB2->update("entr_user_detail",$tabledata,array("id"=>$jsondata->userid));
	
	 $pwd=md5($jsondata->employeeid);
	 $tabledata1=array("uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,"password"=>$pwd,
			   "mobile"=>$jsondata->mobileno);
	  $this->DB2->update("sht_users",$tabledata1,array("uid"=>$jsondata->mobileno));
	  
	   $tabledata2=array("username"=>$jsondata->mobileno,"value"=>$jsondata->employeeid,
			    "op"=>":=","attribute"=>"Cleartext-Password");
	  $this->DB2->update("radcheck",$tabledata2,array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password"));
	  
	  $tabledata3=array("username"=>$jsondata->mobileno,"value"=>1,
			    "op"=>":=","attribute"=>"Simultaneous-Use");
	  $this->DB2->update("radcheck",$tabledata3,array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	  
		  $data['resultcode']=1;
		$data['resultmsg']="User Updated";
		return $data;
		
	}
	
	public function enterprise_guest_setting($jsondata)
	{
		$data=array();
	
	$locid=$jsondata->location_id;
	$query=$this->DB2->query("select `plan_id`,`plan_hour`,`is_enabled` from entr_guest_setting where location_id='".$locid."'");
	 if($query->num_rows()>0)
	 {
		   $rowarr=$query->row_array();
		  $data['resultcode']=1;
		  $data['plan_id']=$rowarr['plan_id'];
		  $data['plan_hour']=$rowarr['plan_hour'];
		  $data['is_enabled']=$rowarr['is_enabled'];
	}
	 else
	 {
		   $data['resultcode']=0;
	}
	 return $data;
	}
	
	public function enterprise_guest_setting_add($jsondata)
	{
		$data=array();
		 $locid=$jsondata->location_id;
		$isenable=$jsondata->is_enable;
		 $tabledata=array("plan_id"=>$jsondata->plan_id,"plan_hour"=>$jsondata->plan_hour,
				  "is_enabled"=>$isenable,"location_id"=>$locid);
		 $query=$this->DB2->query("select id from entr_guest_setting where location_id='".$locid."'");
		 if($query->num_rows()>0)
		 {
			
			  $this->DB2->update('entr_guest_setting',$tabledata,array("location_id"=>$locid));
			
		 $data['resultcode']=1;
		 
		 }else
		 {
			  $this->DB2->insert('entr_guest_setting',$tabledata);
			 $data['resultcode']=1;
		 }
		 return $data;
	}
	
	public function enterprise_expired_user($jsondata)
	{
		 
	 $data=array();
	   $loc_uid=$jsondata->location_uid;
	 
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.employeeid='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`employeeid`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN hud.start_date AND hud.end_date) AS countuser 
FROM `entr_user_detail` hud WHERE hud.`end_date`< NOW() and hud.location_uid='".$loc_uid."' {$cond}");

$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
		$i=1;	
		  foreach($query->result() as $vald)
		  {
			   $class=($vald->countuser>0)?"in_use":"unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   
			      $listarr[$i]['txt']=$txt;
			      $listarr[$i]['lastname']=$vald->lastname;
			      $listarr[$i]['mobile']=$vald->mobile;
			      $listarr[$i]['employeeid']=$vald->employeeid;
			      $listarr[$i]['activated_on']=$vald->activated_on;
			      if($class == 'in_use'){
				  $listarr[$i]['color']='#39b54a'; 
			      }else{
				   $listarr[$i]['color']='#f4474a'; 
			      }
			   
			   
			   
			 $i++;
		  }
		
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		  $data['list']=$listarr;
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Expired Guests";
		 $data['list']=$listarr;
		  
	 }
	 
	 return $data;
	}
	
	public function enterprise_usertype($jsondata)
	{
		$loc_id=$jsondata->location_id;
	 $query= $this->DB2->query("SELECT id,usertype FROM `location_usertype` WHERE location_id='".$loc_id."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['id']=$val->id;
		 $planlistarr[$i]['usertype']=$val->usertype;
		 $i++;
	 }
	}
	$data['usertypelist']=$planlistarr;
	return $data;
	}
	
	public function enterprise_delete_user($jsondata)
	{
		$location_uid=$jsondata->location_uid;
		$this->DB2->delete('entr_user_detail',array('mobile'=>$jsondata->mobile,'location_uid'=>$location_uid));
		
		$this->DB2->delete('sht_users',array('mobile'=>$jsondata->mobile));
		
		$this->DB2->delete('radcheck',array('username'=>$jsondata->mobile));
		
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		return $data;
	}
	
	 public function institute_active_guestcount($jsondata)
	 {
		 $data=array();
		   $loc_uid=$jsondata->location_uid;
	 $query=$this->DB2->query("select id from inst_user_detail WHERE `end_date`>NOW() and location_uid='".$loc_uid."'  ");
	 $data['resultcode']=1;
	  $data['usercount']=$query->num_rows();
	 return $data;
	 }
	
	 public function institute_active_guestlist($jsondata)
	{
	  
	   $loc_uid=$jsondata->location_uid;
	 $postdata=$this->input->post();
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.idcard='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`idcard`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN hud.start_date AND hud.end_date) AS countuser 
FROM `inst_user_detail` hud WHERE hud.`end_date`>NOW() and hud.location_uid='".$loc_uid."' {$cond}");
$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
										      $i=1;	
	    
	    
		  foreach($query->result() as $vald)
		  {
			  $class=($vald->countuser>0)?"in_use":"small_unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   $date=$vald->activated_on;
			  $listarr[$i]['text']=$txt;
			   $listarr[$i]['date']=$date;
			   $listarr[$i]['id']=$vald->id;
			    $listarr[$i]['lastname']=$vald->lastname;
				 $listarr[$i]['mobile']=$vald->mobile;
				  $listarr[$i]['idcard']=$vald->idcard;
			  
			 $i++;  
			  
		  }
		   
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		   $data['list']=$listarr;
		 
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Active Guest";
		  
		  
	 }
	 return $data;
	 
	}
	
	public function institute_editguest($jsondata)
	{
		//$postdata=$this->input->post();
	$query=$this->DB2->query("SELECT id,`firstname`,`lastname`,`mobile`,`plan_id`,`usertype`,`department`,`idcard`,`start_date`,`end_date`,devices FROM inst_user_detail where id='".$jsondata->id."'");
	//echo $this->db->last_query(); die;
	$rowarr=$query->row_array();
	$rowarr=$query->row_array();
	 $data['resultcode']=1;
	 $data['id']=$rowarr['id'];
	$data['firstname']=$rowarr['firstname'];
	$data['lastname']=$rowarr['lastname'];
	$data['mobile']=$rowarr['mobile'];
	$data['plan_id']=$rowarr['plan_id'];
	$data['idcard']=$rowarr['idcard'];
	$data['usertype']=$rowarr['usertype'];
	$data['department']=$rowarr['department'];
	
	$data['start_date']=date("d-m-Y",strtotime($rowarr['start_date']));
	$data['end_date']=date("d-m-Y",strtotime($rowarr['end_date']));
	
	
	$loc_uid=$jsondata->location_uid;
	 $query= $this->DB2->query("SELECT ss.`srvid`,ss.`srvname` FROM `sht_services` ss
	 INNER JOIN `ht_planassociation` hp ON (ss.srvid=hp.plan_id) WHERE hp.location_uid='".$loc_uid."'");
	 $gen="";
	 $planlistarr=array();
	 if($query->num_rows()>0)
	 {
		 $i=0;
	 foreach($query->result() as $val)
	 {
		 $planlistarr[$i]['planid']=$val->srvid;
		 $planlistarr[$i]['planname']=$val->srvname;
		 $i++;
	 }
	}
	$data['planlist']=$planlistarr;
	return $data;
	}
	
	public function instituteaddupdate_guest_detail($jsondata)
	{
		
		 $data=array();
		  if($jsondata->planid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Assign aplan";
			  return $data;
		}
		
		if($jsondata->lastname=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Lastname";
			  return $data;
		}
		
		if($jsondata->idcardid=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Enter employee";
			 return $data;
		}
		if($jsondata->devices=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please Select no of devices";
			 return $data;
		}
		
		
		if($jsondata->location_id=="")
		{
			 $data['resultcode']=0;
			 $data['resultmsg']="Please enter Location ID";
			 return $data;
		}
	
	
	   $loc_uid=$jsondata->location_uid;
	   
	   $ispque=$this->DB2->query("select isp_uid from wifi_location where location_uid='".$loc_uid."'");
	   $isparr=$ispque->row_array();
	   $ispuid=$isparr['isp_uid'];
	 $postdata=$this->input->post();
	 $checkin = date("Y-m-d");
         $checkout =  date("Y-m-d",strtotime('+10 year'));
		$time=3650*24*3600;
		 if($jsondata->mobileno!=''){
			 $jsondata->mobileno=$jsondata->mobileno;
		 }
		 else{
			  $jsondata->mobileno=$this->random_uid();
		 }
		

	 $planquery=$this->DB2->query("select plantype,datalimit from sht_services where srvid='".$jsondata->planid."'");
	// echo $this->db->last_query();die;
	if($planquery->num_rows()>0)
	{
	 
	 $rowarr=$planquery->row_array();
	 if($rowarr['plantype']==2)
	 {
				$daysno = ceil(abs($checkin - $checkout) / 86400)+1;
		 $time=3650*24*3600;
				$tabledata1=array("uptimelimit"=>$time,"comblimit"=>"0");
	 }
	 else
	 {
			$datalimit=$rowarr['datalimit'];
			$tabledata1=array("comblimit"=>$datalimit,"uptimelimit"=>0);
	 }
	 $pwd=md5($jsondata->idcardid);
	 $userquery=$this->DB2->query("select id from sht_users where username='".$jsondata->mobileno."'");
	// echo $this->db->last_query(); die;
	$tabledata=array("isp_uid"=>$ispuid,"uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,
			  "password"=>$pwd,"enableuser"=>1,"baseplanid"=>$jsondata->planid, "firstname"=>$jsondata->firstname
			  ,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "createdby"=>"admin","owner"=>"admin","expiration"=>$checkout);
	$tabledata=array_merge($tabledata,$tabledata1);
	 if($userquery->num_rows()>0)
	 {
		 $userarr= $userquery->row_array();
		 $this->DB2->where('id', $userarr['id']);
            $this->DB2->update('sht_users', $tabledata);
	    //echo $this->db->last_query(); die;
	    $tabledata2=array("value"=>$jsondata->idcardid);
	     $this->DB2->update('radcheck', $tabledata2, array('username' => $jsondata->mobileno,"attribute"=>"Cleartext-Password"));
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->update("radcheck",$tabledata3, array('username' => $jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	 }
	 else
	 {
		  $this->DB2->insert("sht_users",$tabledata);
		  $tabledata2=array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password","op"=>":=",
				      "value"=>$jsondata->roomno);
		   $this->DB2->insert("radcheck",$tabledata2);
		   $tabledata3=array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use","op"=>":=",
				      "value"=>$jsondata->devices);
		  $this->DB2->insert("radcheck",$tabledata3);
	 }
	
	// `firstname`,`lastname`,`mobile`,`plan_id`,`room_no`,`isp_uid`,`location_uid`,`start_date`,`end_date`,`created_on`
	 $tabledata4=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,"mobile"=>$jsondata->mobileno,
			  "plan_id"=>$jsondata->planid,"idcard"=>$jsondata->idcardid,"usertype"=>$jsondata->usertype,
			  "location_uid"=>$loc_uid,"location_id"=>$jsondata->location_id,"start_date"=>$checkin,"end_date"=>$checkout,"devices"=>$jsondata->devices
			  ,"created_on"=>date("Y-m-d H:i:s"));
	 if($jsondata->userid!='')
	 {
		   $id=$this->DB2->update("inst_user_detail",$tabledata4,array("id"=>$jsondata->userid));  
	 }
	 else
	 {
	    $id=$this->DB2->insert("inst_user_detail",$tabledata4);
	 }
	
	 if($id)
	 {
		  $data['resultcode']=1;
		  $data['resultmsg']="Added Student";
		  $data['username']=$jsondata->lastname;
	 }
	 else
	 {
		  $data['resultcode']=0;
		  $data['resultmsg']="Unable to add Student";
		  $data['username']=$jsondata->lastname;
	 
	 }
	 
	 
	 
	}
	else
	{
	 $data['resultcode']=0;
	 $data['resultmsg']="No Such plan exists";
	}
	 
	return $data;
	}
	
	public function instituteupdate_exit_detail($jsondata)
	{
		
		 $postdata=$this->input->post();
	 
	 $tabledata=array("firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,
			  "mobile"=>$jsondata->mobileno,"idcard"=>$jsondata->idcardid);
	 $this->DB2->update("inst_user_detail",$tabledata,array("id"=>$jsondata->userid));
	
	 $pwd=md5($jsondata->idcardid);
	 $tabledata1=array("uid"=>$jsondata->mobileno,"username"=>$jsondata->mobileno,"password"=>$pwd,
			   "mobile"=>$jsondata->mobileno);
	  $this->DB2->update("sht_users",$tabledata1,array("uid"=>$jsondata->mobileno));
	  
	   $tabledata2=array("username"=>$jsondata->mobileno,"value"=>$jsondata->idcardid,
			    "op"=>":=","attribute"=>"Cleartext-Password");
	  $this->DB2->update("radcheck",$tabledata2,array("username"=>$jsondata->mobileno,"attribute"=>"Cleartext-Password"));
	  
	  $tabledata3=array("username"=>$jsondata->mobileno,"value"=>1,
			    "op"=>":=","attribute"=>"Simultaneous-Use");
	  $this->DB2->update("radcheck",$tabledata3,array("username"=>$jsondata->mobileno,"attribute"=>"Simultaneous-Use"));
	  
		  $data['resultcode']=1;
		$data['resultmsg']="User Updated";
		return $data;
		
	}
	
	
	public function institute_expired_user($jsondata)
	{
		 
	 $data=array();
	   $loc_uid=$jsondata->location_uid;
	 
	 $cond='';
	 if(isset($jsondata->search) && $jsondata->search!='')
	 {
      		$cond.="and (hud.lastname like '%{$jsondata->search}%' or hud.idcard='".$jsondata->search."')" ; 
	 }
	 $gen='';
	 $query=$this->DB2->query("SELECT hud.id,hud.`lastname`,hud.`mobile`,hud.`idcard`,hud.`created_on` AS activated_on,
(SELECT COUNT(`radacctid`) FROM `radacct` ra WHERE ra.username=hud.`mobile` AND DATE(ra.`acctstarttime`)
 BETWEEN hud.start_date AND hud.end_date) AS countuser 
FROM `inst_user_detail` hud WHERE hud.`end_date`< NOW() and hud.location_uid='".$loc_uid."' {$cond}");

$listarr=array();
	 if($query->num_rows()>0)
	 {
	   
		$i=1;	
		  foreach($query->result() as $vald)
		  {
			   $class=($vald->countuser>0)?"in_use":"unused";
			   $txt=($vald->countuser>0)?"Used":"Unused";
			   
			      $listarr[$i]['txt']=$txt;
			      $listarr[$i]['lastname']=$vald->lastname;
			      $listarr[$i]['mobile']=$vald->mobile;
			      $listarr[$i]['idcard']=$vald->idcard;
			      $listarr[$i]['activated_on']=$vald->activated_on;
			      if($class == 'in_use'){
				  $listarr[$i]['color']='#39b54a'; 
			      }else{
				   $listarr[$i]['color']='#f4474a'; 
			      }
			   
			   
			   
			 $i++;
		  }
		
		  
		  $data['resultcode']=1;
		   $data['resultmsg']="Success";
		  $data['list']=$listarr;
	 }
	 else
	 {
		  
		$data['resultcode']=0;
		 $data['resultmsg']="No Expired Guests";
		 $data['list']=$listarr;
		  
	 }
	 
	 return $data;
	}
	
	public function institute_delete_user($jsondata)
	{
		$location_uid=$jsondata->location_uid;
		$this->DB2->delete('inst_user_detail',array('mobile'=>$jsondata->mobile,'location_uid'=>$location_uid));
		
		$this->DB2->delete('sht_users',array('mobile'=>$jsondata->mobile));
		
		$this->DB2->delete('radcheck',array('username'=>$jsondata->mobile));
		
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		return $data;
	}
	
     public function fild_boy_login($jsondata)
     {
	  $data = array();
	  $locationinfo = array();
	  $user_email = $jsondata->username;
	  $password = $jsondata->password;
	  $isp_uid = $jsondata->isp_uid;
	  $chek = $this->db->query("select * from sht_isp_users where username = '$user_email' AND orig_pwd = '$password' AND isp_uid = '$isp_uid' AND status = '1' AND is_deleted = '0'");
	  if($chek->num_rows() > 0)
	  {
	       $data['resultCode'] = 1;
	       $data['resultMsg'] = "Success";
	       $row = $chek->row_array();
	       $data['field_boy_id'] = $row['id'];
	  }
	  else
	  {
	       $data['resultCode'] = 0;
	       $data['resultMsg'] = "Invalid Credential";
	  }
	  
	  return $data;
     }
     
     public function user_current_kyc($jsondata)
     {
	  $S3_imag_base_path = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp_homeusers/kyc_docs/";
	  $isp_uid = $jsondata->isp_uid;
	  $user_uid = $jsondata->user_uid;
	  $user_profile_image = '';
	  $user_signature_image = '';
	  $data = array();
	  // user images
	  $result_code = 0;
	  $get_user_image = $this->db->query("select user_profile_image, user_signature_image from sht_users where isp_uid = '$isp_uid' AND uid = '$user_uid'");
	  if($get_user_image->num_rows() > 0)
	  {
	       $row_user_prof = $get_user_image->row_array();
	       if($row_user_prof['user_profile_image'] != '')
	       {
		   $user_profile_image =  $S3_imag_base_path.$user_uid."/".$row_user_prof['user_profile_image'];
	       }
	       if($row_user_prof['user_signature_image'] != '')
	       {
		   $user_signature_image =  $S3_imag_base_path.$user_uid."/".$row_user_prof['user_signature_image'];
	       }
	       $result_code = 1;
	  }
	  // get current_kyc
	  $kyc = array();
	  $get_kyc = $this->db->query("select * from sht_subscriber_documents where isp_uid = '$isp_uid' AND subscriber_uuid = '$user_uid' AND is_deleted = '0'");
	  if($get_kyc->num_rows() > 0)
	  {
	       $i = 0;
	       foreach($get_kyc->result() as $row)
	       {
		    $kyc[$i]['doctype'] = $row->doctype;
		    $kyc[$i]['doc_prooftype'] = $row->doc_prooftype;
		    $kyc[$i]['document_number'] = $row->document_number;
		    $document_image = '';
		    if($row->filename != '')
		    {
			$document_image =  $S3_imag_base_path.$user_uid."/".$row->filename;
		    }
		    $kyc[$i]['document_image'] = $document_image;
		    $i++;
	       }
	       $result_code = 1;
	  }
	  if($result_code == 0)
	  {
	       $data['resultCode'] = $result_code;
	  }
	  else
	  {
	       $data['resultCode'] = $result_code;
	       $data['user_profile_image'] = $user_profile_image;
	       $data['user_signature_image'] = $user_signature_image;
	       $data['kyc'] = $kyc;
	  }
	  
	  //echo "<pre>";print_r($data);die;
	  return $data;
	  
     }
     
     
     function upload_to_s3_server($fname, $path,$uid)
     {
	  $famazonname = AMAZONPATH_360.'isp_homeusers/kyc_docs/'.$uid."/".$fname;
	  $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
	  $this->s3->putObjectFile($path, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
     }
     public function add_kyc($jsondata)
     {
	  
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $user_uid = $jsondata->user_uid;
	  $field_boy_uid = $jsondata->field_boy_uid;
	  
	  $photo_id_doc = $jsondata->photo_id_doc;
	  $photo_id_num = $jsondata->photo_id_num;
	  
	  $address_id_doc = $jsondata->address_id_doc;
	  $address_id_num = $jsondata->address_id_num;
	  
	  $corporate_id_doc = $jsondata->corporate_id_doc;
	  $corporate_id_num = $jsondata->corporate_id_num;
	  
	  $photo_id_photo = '';
	  $address_photo = '';
	  $corportate_photo = '';
	  $user_profile_photo = '';
	  $signature_photo = '';
	  //get suscriber_id
	  $suscriber_id = 0;
	  $get_sub_id = $this->db->query("select id from sht_users where uid = '$user_uid'");
	  if($get_sub_id->num_rows() > 0)
	  {
	       $sub_id_row = $get_sub_id->row_array();
	       $suscriber_id = $sub_id_row['id'];
	  }
	  
	  
	  $result_code = 1;
	  $result_msg = "Success";
	  
	  if($suscriber_id != '0')
	  {
	       if(isset($_FILES['image']['name']) && $_FILES['image']['name']!='')
	       {
		    $result_code = 1;
		    $base_url = 'assets/homewifi_user_kyc/';
		    if($base_url != ''){
			 $output = exec("sudo chmod -R 0777 \"$base_url\"");
		    }
		    if (!file_exists($base_url . $user_uid.'/')) {
			 mkdir($base_url . $user_uid.'/', 0777, TRUE);
			 $base_url = $base_url.$user_uid."/";
			 $output = exec("sudo chmod -R 0777 \"$base_url\"");
		    }
		    else
		    {
		       $base_url = $base_url.$user_uid."/";  
		    }
		    $filename=$_FILES['image']['name'];
		    $fileupload= move_uploaded_file($_FILES['image']['tmp_name'], $base_url.$filename);
		    $zip = new ZipArchive;
		    if ($zip->open($base_url."KYC_PHOTO_CAPTURE.zip") === TRUE)
		    {
			 if($base_url != ''){
			      $output = exec("sudo chmod -R 0777 \"$base_url\"");
				
			 }
			 $zip->extractTo($base_url);
			 if($base_url != ''){
			      $output = exec("sudo chmod -R 0777 \"$base_url\"");
				
			 }
			 $utput = exec("sudo chmod -R 0777 \"$base_url\"");
			 for($i = 0; $i < $zip->numFiles; $i++)
			 {
			      $file = $zip->getNameIndex($i);
			      $file_array =  explode('.',$zip->getNameIndex($i));
			      $file_name = '';$file_ext = '';
			      $file_name= $file_array[0];
			      $file_ext = $file_array[1];
			      if($file_name == 'photo_id')
			      {
				   $photo_id_photo = date('Ymd').time().rand().".".$file_ext;
				   rename($base_url.$file,$base_url.$photo_id_photo);
				   $this->upload_to_s3_server($photo_id_photo,$base_url.$photo_id_photo,$user_uid);
				   
			      }
			      else if($file_name == 'address_photo')
			      {
				   $address_photo = date('Ymd').time().rand().".".$file_ext;
				   rename($base_url.$file,$base_url.$address_photo);
				   $this->upload_to_s3_server($address_photo,$base_url.$address_photo,$user_uid);
			      }
			      else if($file_name == 'corporate_photo')
			      {
				   $corportate_photo = date('Ymd').time().rand().".".$file_ext;
				   rename($base_url.$file,$base_url.$corportate_photo);
				   $this->upload_to_s3_server($corportate_photo,$base_url.$corportate_photo,$user_uid);
			      }
			      else if($file_name == 'user_photo')
			      {
				   $user_profile_photo = date('Ymd').time().rand().".".$file_ext;
				   rename($base_url.$file,$base_url.$user_profile_photo);
				   $this->upload_to_s3_server($user_profile_photo,$base_url.$user_profile_photo,$user_uid);
			      }
			      else if($file_name == 'signature_photo')
			      {
				   $signature_photo = date('Ymd').time().rand().".".$file_ext;
				   rename($base_url.$file,$base_url.$signature_photo);
				   $this->upload_to_s3_server($signature_photo,$base_url.$signature_photo,$user_uid);
			      }
			 }
			 $zip->close();
			 $this->load->helper("file");
                         delete_files($base_url, true);
		    }
		    // add logs
		    $this->add_logs($user_uid, 'Add KYC Details', $isp_uid, $field_boy_uid);
	       }
	       else{
		    $result_code = 0;
		    $result_msg = "no image found";
	       }
	       
	       
	  
	       if($photo_id_photo != '')
	       {
		    $insert_photo_id = $this->db->query("insert into sht_subscriber_documents (isp_uid, subscriber_id, subscriber_uuid, doctype, doc_prooftype, document_number, filename, is_deleted, added_on) values('$isp_uid', '$suscriber_id', '$user_uid', '$photo_id_doc', 'PhotoID', '$photo_id_num', '$photo_id_photo', '0', now())");
	       }
	       if($address_photo != '')
	       {
		    $insert_photo_id = $this->db->query("insert into sht_subscriber_documents (isp_uid, subscriber_id, subscriber_uuid, doctype, doc_prooftype, document_number, filename, is_deleted, added_on) values('$isp_uid', '$suscriber_id', '$user_uid', '$address_id_doc', 'KYC', '$address_id_num', '$address_photo', '0', now())");
	       }
	       if($corportate_photo != '')
	       {
		    $insert_photo_id = $this->db->query("insert into sht_subscriber_documents (isp_uid, subscriber_id, subscriber_uuid, doctype, doc_prooftype, document_number, filename, is_deleted, added_on) values('$isp_uid', '$suscriber_id', '$user_uid', '$corporate_id_doc', 'Corporate', '$corporate_id_num', '$corportate_photo', '0', now())");
	       }
	       if($user_profile_photo != '')
	       {
		    $update = $this->db->query("update sht_users set user_profile_image = '$user_profile_photo' where uid = '$user_uid'");
	       }
	       if($signature_photo != '')
	       {
		    $update = $this->db->query("update sht_users set user_signature_image = '$signature_photo' where uid = '$user_uid'");
	       }
	  }
	  else
	  {
	       $result_code = 0;
	       $result_msg = "User Not found";
	  }
	  
	  $data['resultCode'] = $result_code;
	  $data['resultMsg'] = $result_msg;
	  return $data;
     }
     
     public function add_logs($user_uid, $action, $isp_uid, $field_boy_uid){
	  $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$field_boy_uid."'");
          if($teamQ->num_rows() > 0)
	  {
               $team_rowdata = $teamQ->row();
               $added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
          }
	  else{
               $added_by = 'TeamMember';
          }
	  $added_on = date('Y-m-d H:i:s');
	  $logactivity = array(
            'isp_uid' => $isp_uid,
            'uid' => $user_uid,
            'action' => "$action",
            'added_by' => $added_by,
            'added_on' => $added_on
	  );
	  $this->db->insert('sht_activity_logs', $logactivity);
     }
     
     public function add_user_payment($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $user_uid = $jsondata->user_uid;
	  $suscriber_id = 0;
	  $get_sub_id = $this->db->query("select id from sht_users where uid = '$user_uid'");
	  if($get_sub_id->num_rows() > 0)
	  {
	       $sub_id_row = $get_sub_id->row_array();
	       $suscriber_id = $sub_id_row['id'];
	  }
	  $field_boy_uid = $jsondata->field_boy_uid;
	  $date_time = $jsondata->date_time;
	  $type = $jsondata->type;
	  $amount = $jsondata->amount;
	  $bank_name = $jsondata->bank_name;
	  $bank_address = $jsondata->bank_address;
	  $bank_IFSE = $jsondata->bank_IFSE;
	  $account_num = $jsondata->account_num;
	  $cheque_num = $jsondata->cheque_num;
	  $dd_num = $jsondata->dd_num;
	  $issued_date = $jsondata->issued_date;
	  $slipimage = '';;
	  if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){
		  $base_url = 'assets/homewifi_user_kyc/';
		  if($base_url != ''){
		       $output = exec("sudo chmod -R 0777 \"$base_url\"");
		  }
		  if (!file_exists($base_url . $user_uid.'/')) {
			  mkdir($base_url . $user_uid.'/', 0777, TRUE);
			  $base_url = $base_url.$user_uid."/";
			  $output = exec("sudo chmod -R 0777 \"$base_url\"");
		  }
		  else
		  {
		     $base_url = $base_url.$user_uid."/";  
		  }
		  $filename=$_FILES['image']['name'];
		  $fileupload= move_uploaded_file($_FILES['image']['tmp_name'], $base_url.$filename);
		  $zip = new ZipArchive;
		  if ($zip->open($base_url."BILLING_PHOTO_ZIP.zip") === TRUE)
		  {
		    if($base_url != ''){
			      $output = exec("sudo chmod -R 0777 \"$base_url\"");
				
			 }
		       $zip->extractTo($base_url);
		       if($base_url != ''){
			      $output = exec("sudo chmod -R 0777 \"$base_url\"");
				
			 }
		       $output = exec("sudo chmod -R 0777 \"$base_url\"");
			  for($i = 0; $i < $zip->numFiles; $i++)
			  {
			    $file = $zip->getNameIndex($i);
			    $file_array =  explode('.',$zip->getNameIndex($i));
			    $file_name = '';$file_ext = '';
			    $file_name= $file_array[0];
			    $file_ext = $file_array[1];
			    if($file_name == 'gate_photo')
			    {
				 $slipimage = date('Ymd').time().rand().".".$file_ext;
				 rename($base_url.$file,$base_url.$slipimage);
				 $this->upload_to_s3_server($slipimage,$base_url.$slipimage,$user_uid);
				 
			    }
			    else if($file_name == 'cheque_photo')
			    {
				 $slipimage = date('Ymd').time().rand().".".$file_ext;
				 rename($base_url.$file,$base_url.$slipimage);
				 $this->upload_to_s3_server($slipimage,$base_url.$slipimage,$user_uid);
			    }
			    else if($file_name == 'dd_photo')
			    {
				 $slipimage = date('Ymd').time().rand().".".$file_ext;
				 rename($base_url.$file,$base_url.$slipimage);
				 $this->upload_to_s3_server($slipimage,$base_url.$slipimage,$user_uid);
			    }
			    
		       }
		       $zip->close();
		       $this->load->helper("file");
		       delete_files($base_url, true);
		  }
	  }
	  
	  $receipt_number = mt_rand(1000000, 9999999);
	  
	  if($type == 'not_available')
	  {
	       // add logs
	       $this->add_logs($user_uid, 'User Not Available At Home', $isp_uid, $field_boy_uid);
	       $this->user_not_available_alert($isp_uid, $user_uid);
	       $resultCode = 1;
	       $resultMsg = "Success";
	  }
	  else if($type == 'cash')
	  {
	       $this->addtowallet($isp_uid,$field_boy_uid,$suscriber_id,$user_uid, $amount,$receipt_number,'cash',$date_time,'','','','','','');
	       $resultCode = 1;
	       $resultMsg = "Success";
	  }
	  else if($type == 'cheque')
	  {
	       $this->addtowallet($isp_uid,$field_boy_uid,$suscriber_id,$user_uid, $amount,$receipt_number,'cheque',$date_time,$account_num,$bank_name,$bank_address,$issued_date,$cheque_num,$slipimage);
	       $resultCode = 1;
	       $resultMsg = "Success";
	  }
	  else if($type == 'dd')
	  {
	       $this->addtowallet($isp_uid,$field_boy_uid,$suscriber_id,$user_uid, $amount,$receipt_number,'dd',$date_time,$account_num,$bank_name,$bank_address,$issued_date,$dd_num,$slipimage);
	       $resultCode = 1;
	       $resultMsg = "Success";
	  }
	  else
	  {
	       $resultCode = 0;
	       $resultMsg = "Invalid Type";
	  }
	  
	  $data['resultCode'] = $resultCode;
	  $data['resultMsg'] = $resultMsg;
	  return $data;
     }
     public function addtowallet($isp_uid,$field_boy_uid,$suscriber_id,$user_uid,$amount,$receipt_number,$payment_mode,$date_time, $account_num, $bank_name, $bank_address, $issued_date, $dd_num, $slipimage){
	  $uuid = $user_uid;
	  $wallet_amount = $amount;
	  $walletbillid_foredit = '';
	  $walletid_foredit = '';
	  if($receipt_number != '')
	  {
	       $receipt_received = '1';
	       $this->add_logs($uuid, "Amount added to Wallet",$isp_uid, $field_boy_uid);
	       $bill_paid_on = date('Y-m-d H:i:s');
	  }
	  else
	  {
	       $receipt_received = '0';
	       $this->add_logs($uuid, "Wallet Amount Request Added",$isp_uid, $field_boy_uid);
	       $bill_paid_on = '0000-00-00 00:00:00';
	  }
	  $receiptArr = array();
	  /*if(isset($_FILES) && (count($_FILES) > 0)){
		  $slipimage = $_FILES['bill_slipimage']['name'];
	  }*/
	  $update_payment_mode = $payment_mode;
	  if(($update_payment_mode == 'cheque') || ($update_payment_mode == 'dd'))
	  {
	       $receiptArr['account_number'] = $account_num;
	       $receiptArr['branch_name'] = $bank_name;
	       $receiptArr['branch_address'] = $bank_address;
	       $receiptArr['bankslip_issued_date'] = $issued_date;
	       $receiptArr['bankslip_image'] = $slipimage;
	       $receiptArr['cheque_dd_paytm_number'] = $dd_num;
	  }
	  $wallet_amount_received = '0';
	  $walletArr = array(
		  'isp_uid' => $isp_uid,
		  'subscriber_uuid' => $uuid,
		  'wallet_amount' => $wallet_amount,
		  'added_on'	=> date('Y-m-d H:i:s')
	  );
	  if($walletid_foredit != '')
	  {
	       $this->db->update('sht_subscriber_wallet', $walletArr, array('id' => $walletid_foredit));
	       $wlastid = $walletid_foredit;
	  }
	  else
	  {
	       $this->db->insert('sht_subscriber_wallet', $walletArr);
	       $wlastid = $this->db->insert_id();
	  }
	  $wallet_billarr = array(
	       'isp_uid' => $isp_uid,
	       'subscriber_id' => $suscriber_id,
	       'subscriber_uuid' => $user_uid,
	       'wallet_id' => $wlastid,
	       'bill_added_on' => $date_time,
	       'receipt_number' => $receipt_number,
	       'bill_type' => 'addtowallet',
	       'payment_mode' => $update_payment_mode,
	       'actual_amount' => $amount,
	       'total_amount' => $amount,
	       'receipt_received' => $receipt_received,
	       'alert_user' => '1',
	       'bill_generate_by' => $field_boy_uid,
	       'bill_paid_on' => $bill_paid_on,
	       'wallet_amount_received' => $wallet_amount_received
	  );
	  if($walletbillid_foredit != '')
	  {
	       $this->db->update('sht_subscriber_billing', $wallet_billarr, array('id' => $walletbillid_foredit));
	       $wlast_billid = $walletbillid_foredit;
	  }
	  else
	  {
	       $this->db->insert('sht_subscriber_billing', $wallet_billarr);
	       $wlast_billid = $this->db->insert_id();
	  }
	  if(count($receiptArr) > 0){
	       $receiptArr['isp_uid'] = $isp_uid;
	       $receiptArr['uid'] = $uuid;
	       $receiptArr['bill_id'] = $wlast_billid;
	       $receiptArr['receipt_number'] = $receipt_number;
	       $receiptArr['receipt_amount'] = $amount;
	       $receiptArr['added_on'] = date('Y-m-d H:i:s');
	       $checkrcpthistoryQ = $this->db->query("SELECT id FROM sht_subscriber_receipt_history WHERE bill_id='".$wlast_billid."'");
	       if($checkrcpthistoryQ->num_rows() > 0)
	       {
		    $this->db->update('sht_subscriber_receipt_history', $receiptArr, array('bill_id' => $wlast_billid));
	       }
	       else
	       {
		    $this->db->insert('sht_subscriber_receipt_history', $receiptArr);
	       }
	  }
	  else
	  {
	       $this->db->delete('sht_subscriber_receipt_history', array('bill_id' => $wlast_billid));
	  }
	  $userassoc_plandata = $this->userassoc_plandata($uuid,$isp_uid);
	  $plan_cost_perday = $userassoc_plandata['plan_cost_perday'];
	  $account_activated_on = $userassoc_plandata['account_activated_on'];
	  $user_credit_limit = $userassoc_plandata['user_credit_limit'];
	  $total_advpay = $userassoc_plandata['total_advpay'];
	  $user_wallet_balance = $this->userbalance_amount($uuid);
	  if($account_activated_on != '0000-00-00')
	  {
	       $actiondate = $this->affected_date_on_wallet($uuid);
	       $expiration_acctdays = round(($user_credit_limit + $user_wallet_balance + $total_advpay) / $plan_cost_perday);
	       $expiration_date = date('Y-m-d', strtotime($actiondate . " +".$expiration_acctdays." days"));
	       $expiration_date = $expiration_date.' 00:00:00';
	       $this->db->update('sht_users', array('expiration' => $expiration_date), array('uid' => $uuid));
	  }
	  if($wallet_amount_received == 0)
	  {
	       $useralerts_status = $this->useralerts_status($uuid);
	       $sms_alert = $useralerts_status['sms_alert'];
	       $email_alert = $useralerts_status['email_alert'];
	       if($sms_alert == 1)
	       {
		    $this->payment_received_alert($isp_uid, $uuid, $wallet_amount);
	       }
	  }
	  return 1;
     }
     public function userassoc_plandata($subscriber_uuid,$isp_uid){
	  $data = array();
	  $user_currplanQ = $this->db->query("SELECT tb2.datacalc, tb2.datalimit, tb2.plantype, tb2.plan_duration, tb1.baseplanid, tb1.user_plan_type, tb1.user_credit_limit, DATE(tb1.account_activated_on) as account_activated_on, tb1.paidtill_date, DATE(tb1.expiration) as expiration_date, tb3.net_total, tb3.gross_amt FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) INNER JOIN sht_plan_pricing as tb3 ON(tb2.srvid=tb3.srvid) WHERE tb1.uid='".$subscriber_uuid."'");
	  $current_planrow = $user_currplanQ->row();
	  $tax = $this->current_taxapplicable($isp_uid);
	  $plan_duration = $current_planrow->plan_duration;
	  $srvid = $current_planrow->baseplanid;
	  $custom_planpriceArr = $this->getcustom_planprice($subscriber_uuid, $srvid,$isp_uid);
	  $custom_planprice = $custom_planpriceArr['gross_amt'];
	  if($custom_planprice != '')
	  {
	       $gross_amt = round($custom_planprice);
	       $plan_price = round($custom_planprice);
	  }
	  else
	  {
	       $gross_amt = ($current_planrow->gross_amt * $plan_duration);
	       $plan_price = round($gross_amt + (($gross_amt * $tax)/100));
	  }
	  $data['plan_duration'] = $plan_duration;
	  $data['datacalc'] = $current_planrow->datacalc;
	  $data['datalimit'] = $current_planrow->datalimit;
	  $data['planid'] = $current_planrow->baseplanid;
	  $data['plan_price'] = $plan_price;
	  $data['plantype'] = $current_planrow->plantype;
	  $data['user_credit_limit'] = $current_planrow->user_credit_limit;
	  $data['account_activated_on'] = $current_planrow->account_activated_on;
	  $plan_tilldays = ($plan_duration * 30);
	  $data['plan_tilldays'] = $plan_tilldays;
	  $data['plan_cost_perday'] = round($plan_price/$plan_tilldays);
	  $data['expiration_date'] = $current_planrow->expiration_date;
	  $data['user_plan_type'] = $current_planrow->user_plan_type;
	  $data['total_advpay'] = $this->user_advpayments($subscriber_uuid);
	  return $data;
     }
     public function user_advpayments($uid){
	  $query = $this->db->query("SELECT COALESCE(SUM(balance_left),0) as balance_left FROM sht_advance_payments WHERE uid='".$uid."'");
	  $advobj = $query->row();
	  $total_advpay = $advobj->balance_left;
	  return $total_advpay;
	}

     public function current_taxapplicable($isp_uid){
	  $query = $this->db->query("SELECT tax FROM sht_tax WHERE isp_uid='".$isp_uid."'");
	  if($query->num_rows() > 0){
	       $rowdata = $query->row();
	       return $rowdata->tax;
	  }
     }
     
     
     
     public function getcustom_planprice($uuid, $srvid,$isp_uid){
	  $data = array();
	  $customplanQ = $this->db->query("SELECT baseplanid,gross_amt FROM sht_custom_plan_pricing WHERE isp_uid='".$isp_uid."' AND uid='".$uuid."' AND baseplanid='".$srvid."' ORDER BY id DESC LIMIT 1");
	  $num_rows = $customplanQ->num_rows();
	  if($num_rows > 0)
	  {
	       $rowdata = $customplanQ->row();
	       $data['baseplanid'] = $rowdata->baseplanid;
	       $data['gross_amt'] = $rowdata->gross_amt;
	  }
	  else
	  {
	       $data['baseplanid'] = '';
	       $data['gross_amt'] = '';
	  }
	  return $data;
     }
     public function userbalance_amount($uuid){	
	  $wallet_amt = 0;
	  $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
	  if($walletQ->num_rows() > 0)
	  {
	       $wallet_amt = $walletQ->row()->wallet_amt;
	  }
	  $passbook_amt = 0;
	  $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
	  if($passbookQ->num_rows() > 0)
	  {
	       $passbook_amt = $passbookQ->row()->plan_cost;
	  }
	  $balanceamt = ($wallet_amt - $passbook_amt);
	  return $balanceamt;
     }
     public function affected_date_on_wallet($uid){
	  $action_date = '';
	  $wallet_date = ''; $passbook_date = '';
	  $lastwallet_addedQ = $this->db->query("SELECT DATE(added_on) as wallet_date FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uid."' ORDER BY id DESC LIMIT 1");
	  $lnumrows = $lastwallet_addedQ->num_rows();
	  if($lnumrows > 0)
	  {
	       $wallet_date = $lastwallet_addedQ->row()->wallet_date;
	  }
	  $lastpassbook_addedQ = $this->db->query("SELECT DATE(added_on) as passbook_date FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uid."' ORDER BY id DESC LIMIT 1");
	  $pnumrows = $lastpassbook_addedQ->num_rows();
	  if($pnumrows > 0)
	  {
	       $passbook_date = $lastpassbook_addedQ->row()->passbook_date;
	  }
	  if(($wallet_date != '') && ($passbook_date != ''))
	  {
	       if(strtotime($wallet_date) >= strtotime($passbook_date))
	       {
		    $action_date = $wallet_date;
	       }
	       else
	       {
		    $action_date = $passbook_date;
	       }
	  }
	  elseif(($wallet_date == '') && ($passbook_date != ''))
	  {
	       $action_date = $passbook_date;
	  }
	  elseif(($wallet_date != '') && ($passbook_date == ''))
	  {
	       $action_date = $wallet_date;
	  }
	  elseif(($wallet_date == '') && ($passbook_date == ''))
	  {
	       $subsAcctQ = $this->db->query("SELECT plan_activated_date FROM sht_users WHERE uid='".$uid."'");
	       if($subsAcctQ->num_rows() > 0)
	       {
		    $rowdata = $subsAcctQ->row();
		    $action_date = $rowdata->plan_activated_date;
	       }
			
	  }
		
	  return $action_date;
     }
     public function useralerts_status($uuid){
	  $data = array();
	  $notifyQ = $this->db->query("SELECT sms_alert, email_alert FROM sht_users WHERE uid='".$uuid."'");
	  if($notifyQ->num_rows() > 0)
	  {
	       $notifydata = $notifyQ->row();
	       $data['sms_alert'] = $notifydata->sms_alert;
	       $data['email_alert'] = $notifydata->email_alert;
	  }
	  return $data;
     }
     public function payment_received_alert($isp_uid, $uuid, $amount_received){
	  
	  $username = $this->getcustomer_fullname($uuid);
	  $mobileno = $this->getcustomer_mobileno($uuid);
	  $ispdetArr = $this->getispdetails($isp_uid);
	  $company_name = $ispdetArr['company_name'];
	  $isp_name = $ispdetArr['isp_name'];
	  $help_number = $ispdetArr['help_number'];
	  
	  $message = urlencode('Dear '.$username.', we have received a payment of Rs. '.$amount_received.' towards your account with User ID: '.$uuid).'%0a'. urlencode(' Thank you for your payment!').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
	  $sms_tosend = 'Dear '.$username.', we have received a payment of Rs. '.$amount_received.' towards your account with User ID: '.$uuid.' <br/> Thank you for your payment! <br/> For any queries please call us @ '.$help_number.' <br/>Team '.$isp_name;
	  
	  $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
	  if($txtsmsQ->num_rows() > 0){
	       foreach($txtsmsQ->result() as $txtsmsobj){
		    $gupshup_user = $txtsmsobj->gupshup_user;
		    $gupshup_pwd = $txtsmsobj->gupshup_pwd;
		    $msg91authkey = $txtsmsobj->msg91authkey;
		    $bulksmsuser = $txtsmsobj->bulksmsuser;
		    $bulksms_pwd = $txtsmsobj->bulksms_pwd;
		    $rightsms_user = $txtsmsobj->rightsms_user;
		    $rightsms_pwd = $txtsmsobj->rightsms_pwd;
		    $rightsms_sender = $txtsmsobj->rightsms_sender;
		    $cloudplace_user = $txtsmsobj->cloudplace_user;
		    $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
		    $cloudplace_sender = $txtsmsobj->scope;
		    $infini_apikey = $txtsmsobj->infini_apikey;
		    $infini_senderid = $txtsmsobj->infini_senderid;
		    
		    $sender = $txtsmsobj->scope;
		    
		    if(($gupshup_user != '') && ($gupshup_pwd != '')){
			$this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif($msg91authkey != ''){
			$this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
			$this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
			$this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
			$this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($infini_apikey != '') && ($infini_senderid != '')){
                         $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $mobileno, $message);
			 $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    }else{
			return 1;
		    }
	       }
	  }
     }
     
     public function user_not_available_alert($isp_uid, $uuid){
	  
	  $username = $this->getcustomer_fullname($uuid);
	  $mobileno = $this->getcustomer_mobileno($uuid);
	  $ispdetArr = $this->getispdetails($isp_uid);
	  $company_name = $ispdetArr['company_name'];
	  $isp_name = $ispdetArr['isp_name'];
	  $help_number = $ispdetArr['help_number'];
	  
	  $message = urlencode('Dear '.$username.'('.$uuid.'), we have visited at your place but you are currently unavailable').'%0a'. urlencode('Please contact to customer care for next schedule !').'%0a'. urlencode('For any queries please call us @ '.$help_number).'%0a'.urlencode('Team '.$isp_name);
	  $sms_tosend = 'Dear '.$username.'('.$uuid.'), we have visited at your place but you are currently unavailable. <br/> Please contact to customer care for next schedule ! <br/> For any queries please call us @ '.$help_number.' <br/>Team '.$isp_name;
	  
	  $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
	  if($txtsmsQ->num_rows() > 0){
	       foreach($txtsmsQ->result() as $txtsmsobj){
		    $gupshup_user = $txtsmsobj->gupshup_user;
		    $gupshup_pwd = $txtsmsobj->gupshup_pwd;
		    $msg91authkey = $txtsmsobj->msg91authkey;
		    $bulksmsuser = $txtsmsobj->bulksmsuser;
		    $bulksms_pwd = $txtsmsobj->bulksms_pwd;
		    $rightsms_user = $txtsmsobj->rightsms_user;
		    $rightsms_pwd = $txtsmsobj->rightsms_pwd;
		    $rightsms_sender = $txtsmsobj->rightsms_sender;
		    $cloudplace_user = $txtsmsobj->cloudplace_user;
		    $cloudplace_pwd = $txtsmsobj->cloudplace_pwd;
		    $cloudplace_sender = $txtsmsobj->scope;
		    $infini_apikey = $txtsmsobj->infini_apikey;
		    $infini_senderid = $txtsmsobj->infini_senderid;
		    
		    $sender = $txtsmsobj->scope;
		    
		    if(($gupshup_user != '') && ($gupshup_pwd != '')){
			$this->gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $mobileno, $message);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif($msg91authkey != ''){
			$this->msg91_txtsmsgateway($msg91authkey, $mobileno, $message, $sender);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($bulksmsuser != '') && ($bulksms_pwd != '')){
			$this->bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $mobileno, $message);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($rightsms_user != '') && ($rightsms_pwd != '') && ($rightsms_sender != '')){
			$this->rightchoice_txtsmsgateway($rightsms_user, $rightsms_pwd, $mobileno, $message, $rightsms_sender);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($cloudplace_user != '') && ($cloudplace_pwd != '') && ($cloudplace_sender != '')){
			$this->cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $mobileno, $message, $cloudplace_sender);
			$this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
			return 1;
		    }elseif(($infini_apikey != '') && ($infini_senderid != '')){
                         $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $mobileno, $user_message);
			 $this->addmessage_todb($isp_uid, $uuid, $sms_tosend);
                    }else{
			return 1;
		    }
	       }
	  }
     }

     /**********************************
      * SMS GATEWAY STARTS
      *********************************/
     
     public function getispdetails($isp_uid){
	  $data = array();
	  $ispdetQ = $this->db->query("SELECT company_name,isp_name,help_number1 FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
	  $rdata = $ispdetQ->row();
	  $data['company_name'] = $rdata->company_name;
	  $data['isp_name'] = $rdata->isp_name;
	  $data['help_number'] = $rdata->help_number1;
	  
	  return $data;
      }
      
      public function getcustomer_fullname($uuid){
	  $mobileQ = $this->db->query("SELECT firstname,lastname FROM sht_users WHERE uid='".$uuid."'");
	  $rdata = $mobileQ->row();
	  return $rdata->firstname .' '. $rdata->lastname;
      }
      
      public function getcustomer_mobileno($uuid){
	  $mobileQ = $this->db->query("SELECT mobile FROM sht_users WHERE uid='".$uuid."'");
	  return $mobileQ->row()->mobile;
      }
      
      public function addmessage_todb($isp_uid, $uuid, $message){
	  $this->db->insert("sht_users_notifications", array('isp_uid' => $isp_uid, 'uid' => $uuid, 'notify_type' => 'SMS', 'message' => $message, 'added_on' => date('Y-m-d H:i:s')));
	  return 1;
      }
      
      public function gupshup_txtsmsgateway($gupshup_user, $gupshup_pwd, $user_mobileno, $user_message){
	  $param = array();
	  $request =""; //initialise the request variable
	  $param['method']= "sendMessage";
	  $param['send_to'] = "91"+$user_mobileno;
	  $param['msg'] = $user_message;
	  $param['userid'] = $gupshup_user;
	  $param['password'] = $gupshup_pwd;
	  $param['v'] = "1.1";
	  $param['msg_type'] = "TEXT"; //Can be "FLASH�/"UNICODE_TEXT"/�BINARY�
	  $param['auth_scheme'] = "PLAIN";
	  
	  //Have to URL encode the values
	  foreach($param as $key=>$val) {
	      $request.= $key."=".urlencode($val);
	      $request.= "&";
	  }
	  
	  $request = substr($request, 0, strlen($request)-1);
	  //remove final (&) sign from the request
	  $url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
	  $ch = curl_init($url);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	  $curl_scraped_page = curl_exec($ch);
	  curl_close($ch);
	  //echo $curl_scraped_page;
	  
	  return 1;
      }
      
      public function msg91_txtsmsgateway($msg91authkey, $phone, $msg, $sender) {
	  $postData = array(
	     'authkey' => $msg91authkey, //'106103ADQeqKxOvbT856d19deb',
	     'mobiles' => $phone,
	     'message' => $msg,
	     'sender' => $sender, //'SHOUUT',
	     'route' => '4'
	  );
  
	  $url="https://control.msg91.com/api/sendhttp.php";
	  $ch = curl_init();
	  curl_setopt_array($ch, array(
	     CURLOPT_URL => $url,
	     CURLOPT_RETURNTRANSFER => true,
	     CURLOPT_POST => true,
	     CURLOPT_POSTFIELDS => $postData
	     //,CURLOPT_FOLLOWLOCATION => true
	  ));
  
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	  $output = curl_exec($ch);
	  curl_close($ch);
	  
	  return 1;
      }
  
      public function bulksms_txtsmsgateway($bulksmsuser, $bulksms_pwd, $phone, $msg){
	  $bulksmsurl = "http://bulksmsindia.mobi/sendurlcomma.aspx?";
	  $postvalues = "user=".$bulksmsuser."&pwd=".$bulksms_pwd."&senderid=ABC&mobileno=".$phone."&msgtext=".$msg."&smstype=0/4/3";
	  //print_r($bulksmsurl); die;
	  
	  $ch = curl_init($bulksmsurl);
	  curl_setopt($ch, CURLOPT_POST, 1);
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	  $data = curl_exec($ch);
	  curl_close($ch);
	  
	  return 1;
      }
      
      public function decibel_txtsmsgateway($mobile, $msg){
	  $postData = array(
	     'authkey' => '106103ADQeqKxOvbT856d19deb',
	     'mobiles' => $mobile,
	     'message' => $msg,
	     'sender' => 'SHOUUT',
	     'route' => '4'
	  );
   
	  $url="https://control.msg91.com/api/sendhttp.php";
	  $ch = curl_init();
	  curl_setopt_array($ch, array(
	      CURLOPT_URL => $url,
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_POST => true,
	      CURLOPT_POSTFIELDS => $postData
	      //,CURLOPT_FOLLOWLOCATION => true
	  ));
   
	  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	  $output = curl_exec($ch);
	  var_dump($output); die;
	  
	  curl_close($ch);
	  return 1;
      }
      public function rightchoice_txtsmsgateway($rcuser, $rc_pwd, $phone, $msg, $sender){
	  $rcsmsurl = "http://www.sms.rightchoicesms.com/sendsms/groupsms.php?";
	  $postvalues = "username=".$rcuser."&password=".$rc_pwd."&type=TEXT&mobile=".$phone."&sender=".$sender."&message=".$msg;
	  //print_r($bulksmsurl); die;
	  
	  $ch = curl_init($rcsmsurl);
	  curl_setopt($ch, CURLOPT_POST, 1);
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	  $data = curl_exec($ch);
	  curl_close($ch);
	  
	  return 1;
      }
  
      public function cloudplace_txtsmsgateway($cloudplace_user, $cloudplace_pwd, $user_mobileno, $user_message, $cloudplace_sender){
	  $bulksmsurl = "http://203.212.70.200/smpp/sendsms?";
	  $postvalues = "username=".$cloudplace_user."&password=".$cloudplace_pwd."&to=".$user_mobileno."&from=".$cloudplace_sender."&udh=&text=".$user_message."&dlr-mask=19";
	  //print_r($bulksmsurl); die;
	  
	  $ch = curl_init($bulksmsurl);
	  curl_setopt($ch, CURLOPT_POST, 1);
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $postvalues);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	  $data = curl_exec($ch);
	  curl_close($ch);
	  
	  return 1;
      }
      public function infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message){
	  $baseUrl = "https://alerts.solutionsinfini.com/api/v4/?api_key=".$infini_apikey;
	  $user_message=urlencode($user_message); 
  
	  $ch = curl_init();
	  curl_setopt($ch, CURLOPT_URL, $baseUrl."&method=sms&message=$user_message&to=$user_mobileno&sender=$infini_senderid");
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  $result = curl_exec($ch);
  
	  $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
	  if($statusCode==200){
	      return 1;
	  }
	  else{
	      return 0;
	  }
	  curl_close($ch);
      }


     /**********************************
      * SMS GATEWAY ENDS
      *********************************/

     public function getstatename($stateid){
	  $name = '';
	  $stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
	  $num_rows = $stateQ->num_rows();
	  if($num_rows > 0){
	       $data = $stateQ->row();
	       $name = ucwords($data->state);
	  }
	  return $name;
     }
     public function getcityname($stateid, $cityid){
	  $name = '';
	  $cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
	  $num_rows = $cityQ->num_rows();
	  if($num_rows > 0){
	       $data = $cityQ->row();
	       $name = ucwords($data->city_name);
	  }
	  return $name;
     }
     public function getzonename($zoneid){
	  $name = '';
	  $zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
	  $num_rows = $zoneQ->num_rows();
	  if($num_rows > 0){
	       $data = $zoneQ->row();
	       $name = ucwords($data->zone_name);
	  }
	  return $name;
     }

     public function getactive_userslist($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $field_boy_uid = $jsondata->field_boy_uid;
	  $data = array();
	  $permicond = ''; $cwhere = '1';
	  $todaydate = date('Y-m-d');
	  $checkdepartmentQ = $this->db->query("SELECT dept_id FROM sht_isp_users WHERE isp_uid='".$isp_uid."' AND id='".$field_boy_uid."' AND status='1' AND is_deleted='0'");
	  if($checkdepartmentQ->num_rows() > 0){
	       $dep_id = $checkdepartmentQ->row()->dep_id;
	       $regionquery = $this->db->get_where('sht_department', array('id' => $dept_id));
	       if ($regionquery->num_rows() > 0) {
		    $rowarr = $regionquery->row_array();
		    $region_type = $rowarr['region_type'];
		    if ($regiontype == "region") {
			$dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='".$dept_id."' AND status='1' AND is_deleted='0'");
			$total_deptregion = $dept_regionQ->num_rows();
			if($dept_regionQ->num_rows() > 0){
			      $c = 1;
			      foreach($dept_regionQ->result() as $deptobj){
				   $stateid = $deptobj->state_id;
				   $cityid = $deptobj->city_id;
				   $zoneid = $deptobj->zone_id;
				   if($cityid == 'all'){
					$permicond .= " (state='".$stateid."') ";
				   }elseif($zoneid == 'all'){
					$permicond .= " (state='".$stateid."' AND city='".$cityid."') ";
				   }else{
					$permicond .= " (state='".$stateid."' AND city='".$cityid."' AND zone='".$zoneid."') ";
				   }
				   if($c != $total_deptregion){
					$permicond .= ' OR ';
				   }
				   $c++;
			      }
			}
			$cwhere .= ' AND ('.$permicond.')';
		    }
	       }
	       
	       $cquery = $this->db->query("SELECT uid, firstname, enableuser, inactivate_type, lastname, username, mobile, alt_mobile, expiration FROM sht_users WHERE $cwhere AND isp_uid='".$isp_uid."'");
	       $total_csearch = $cquery->num_rows();
	       if($total_csearch > 0){
		    $data['resultCode'] = '1';
		    $data['resultMsg'] = 'Success';
		    //$data = $cquery->result_array();
		    $i = 0;
		    foreach($cquery->result() as $cobj){
			 $uuid = $cobj->uid;
			 $data['userarray'][$i]['uid'] = $cobj->uid;
			 $data['userarray'][$i]['username'] = $cobj->username;
			 $data['userarray'][$i]['firstname'] = $cobj->firstname;
			 $data['userarray'][$i]['lastname'] = $cobj->lastname;
			 $user_wallet_balance = $this->userbalance_amount($uuid);
			 $expiration = date('Y-m-d', strtotime($cobj->expiration));
			 
			 $data['userarray'][$i]['balance'] = $user_wallet_balance;
			 if($cobj->inactivate_type == 'suspended'){
			      $status = 'Suspended';
			      $onoff_style = "#2D9EE0";
			 }elseif($cobj->inactivate_type == 'terminate'){
			      $status = 'Terminated';
			      $onoff_style = "#D28C32";
			 }elseif(strtotime($todaydate) > strtotime($expiration)){
			      $status = 'expired';
			      $onoff_style = "#FF0000";
			 }else{
			      $status = 'Active';
			      $onoff_style = "#439B46";
			 }
			 
			 $data['userarray'][$i]['account_status'] = $status;
			 $data['userarray'][$i]['status_colorcode'] = $onoff_style;
			 $data['userarray'][$i]['mobile'] = $cobj->mobile;
			 $data['userarray'][$i]['alt_mobile'] = $cobj->alt_mobile;
			 $i++;
		    }
	       }
	       
	       
	  }else{
	       $data['resultCode'] = '0';
	       $data['resultMsg'] = 'Invalid Field Boy Id.';
	  }
	  
	  return $data;
     }
     
     
     public function get_fieldboy_task($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $field_boy_uid = $jsondata->field_boy_uid;
	  $data = array();
	  $checkdepartmentQ = $this->db->query("SELECT dept_id FROM sht_isp_users WHERE isp_uid='".$isp_uid."' AND id='".$field_boy_uid."' AND status='1' AND is_deleted='0'");
	  $this->db->select('*')->from('sht_team_task');
	  $this->db->where(array('isp_uid' => $isp_uid, 'task_assignto' => $field_boy_uid));
	  $get_task = $this->db->get();
	  $user_tasks = array();
	  if($get_task->num_rows() > 0){
	       $data['result_code'] = 1;
	       $data['resultMsg'] = 'success';
	       $i = 0;
	       foreach($get_task->result() as $row){
		    $user_tasks[$i]['task_name'] = $row->task_name;
		    $user_tasks[$i]['task_priority'] = $row->task_priority;
		    $user_tasks[$i]['task_assign_datetime'] = $row->task_assign_datetime;
		    $user_tasks[$i]['geoaddress_searchtype'] = $row->geoaddress_searchtype;
		    $user_tasks[$i]['geoaddr'] = $row->geoaddr;
		    $user_tasks[$i]['placeid'] = $row->placeid;
		    $user_tasks[$i]['latitude'] = $row->latitude;
		    $user_tasks[$i]['longitude'] = $row->longitude;
		    $user_tasks[$i]['task_status'] = $row->task_status;
		    $i++;
	       }
	  }
	  else{
	       $data['result_code'] = 0;
	       $data['resultMsg'] = 'No Tast Found';
	  }
	  $data['user_tasks'] = $user_tasks;
	  return $data;
     }
     public function get_fieldboy_ticket($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $field_boy_uid = $jsondata->field_boy_uid;
	  $data = array();
	  $checkdepartmentQ = $this->db->query("SELECT  sst.subscriber_uuid,sst.id, sst.ticket_id, sst.ticket_type, sst.ticket_description, sst.ticket_comment, sst.ticket_priority,sst.status, sst.added_on, su.firstname, su.lastname from sht_subscriber_tickets as sst inner join sht_users as su on(sst.subscriber_uuid = su.uid)  where sst.ticket_assign_to = '$field_boy_uid' AND sst.status != '1'");
	 
	  $user_tasks = array();
	  if($checkdepartmentQ->num_rows() > 0){
	       $data['result_code'] = 1;
	       $data['resultMsg'] = 'success';
	       $i = 0;
	       foreach($checkdepartmentQ->result() as $row){
		    $user_tasks[$i]['id'] = $row->id;
		    $user_tasks[$i]['ticket_id'] = $row->ticket_id;
		    $user_tasks[$i]['ticket_type'] = $row->ticket_type;
		    $user_tasks[$i]['ticket_priority'] = $row->ticket_priority;
		    $user_tasks[$i]['ticket_status'] = $row->status;
		    $user_tasks[$i]['ticket_description'] = $row->ticket_description;
		    $user_tasks[$i]['ticket_comment'] = $row->ticket_comment;
		    $user_tasks[$i]['added_on'] = $row->added_on;
		    $user_tasks[$i]['subscriber_name'] = $row->firstname." ".$row->lastname;
		    $user_tasks[$i]['subscriber_id'] = $row->subscriber_uuid;
		    //$user_tasks[$i]['ticket_assign_date'] = $row->ticket_assign_date;
		    $i++;
	       }
	  }
	  else{
	       $data['result_code'] = 0;
	       $data['resultMsg'] = 'No Tast Found';
	  }
	  $data['user_ticket'] = $user_tasks;
	  return $data;
     }
     
     public function getticketnumber($tktid){
	  $data = array();
	  $tktQuery = $this->db->query("SELECT ticket_type, ticket_id, subscriber_uuid FROM sht_subscriber_tickets WHERE id = '".$tktid."'");
	  $num_rows = $tktQuery->num_rows();
	  if($num_rows > 0){
	       $rowdata = $tktQuery->row();
	       $data['ticket_number'] = $rowdata->ticket_id;
	       $data['uuid'] = $rowdata->subscriber_uuid;
	       $data['ticket_type'] = $rowdata->ticket_type;
	  }
	  return $data;
     }

     
     
     public function fieldboy_ticket_status_update($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $field_boy_uid = $jsondata->field_boy_uid;
	  $chngtktstatus = $jsondata->status;
	  $request_id = $jsondata->request_id;
	  $chngtktstatus_desc = $jsondata->ticket_message;
	  $chngtktid = $jsondata->ticket_id;
	  $subscriber_id = $jsondata->subscriber_id;
	  $ticket_type = $jsondata->ticket_type;
	  $data = array();
	  $data['result_code'] = 1;
	  $data['resultMsg'] = 'success';
	  
	  if($chngtktstatus != '1'){
	       $this->db->update('sht_subscriber_tickets', array('status' => $chngtktstatus), array('id' => $request_id));
	  }else{
	       $this->db->update('sht_subscriber_tickets', array('closed_on' => date('Y-m-d H:i:s'), 'status' => '1'), array('id' => $request_id));
	  }
	  if(isset($chngtktstatus_desc) && $chngtktstatus_desc != ''){
	       $ticketRequestData = array(
		    'ticket_id' => $request_id,
		    'ticket_edit_by' => $field_boy_uid,
		    'comment' => $chngtktstatus_desc,
		    'added_on' => date('Y-m-d H:i:s')
	       );
	       $this->db->insert('sht_subscriber_tickets_comments', $ticketRequestData);
	  }
	  
	  if($chngtktstatus == '0'){ $status = 'Open'; }
	  elseif($chngtktstatus == '1'){ $status = 'Closed'; }
	  elseif($chngtktstatus == '2'){ $status = 'Pending'; }
	  
	  
	  $ticketnumber = $chngtktid;
	  $uuid = $subscriber_id;
	  $tickettype = $ticket_type;
	  $this->add_user_activitylogs($field_boy_uid, $isp_uid, $uuid, "Ticket $ticketnumber $status");
	     
	  return $data;
     }
     
     public function add_user_activitylogs($field_boy_uid, $isp_uid, $uuid, $action, $table=''){
	  $session_data = $this->session->userdata('isp_session');
	  $super_admin = 0;
	  $userid = $field_boy_uid;
	  if($super_admin == 1){
	      $added_by = 'SuperAdmin';
	  }else{
	      $teamQ = $this->db->query("SELECT name, lastname FROM sht_isp_users WHERE id='".$userid."'");
	      if($teamQ->num_rows() > 0){
		  $team_rowdata = $teamQ->row();
		  $added_by = $team_rowdata->name. " " .$team_rowdata->lastname; 
	      }else{
		  $added_by = 'TeamMember';
	      }
	  }
	  
	  if(is_array($action)){
	      $arraykeys = array_keys($action);
	      $action = implode(',',$arraykeys);
	  }
	  
	  $added_on = date('Y-m-d H:i:s');
	  
	  $logactivity = array(
	      'isp_uid' => $isp_uid,
	      'uid' => $uuid,
	      'action' => "$action",
	      'added_by' => $added_by,
	      'added_on' => $added_on
	  );
	  
	  $this->db->insert('sht_activity_logs', $logactivity);
	  return 1;
     }
     

}



?>
