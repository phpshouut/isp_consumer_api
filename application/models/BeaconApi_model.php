<?php

class BeaconApi_model extends CI_Model {

	  public function __construct() {
		    parent::__construct();
		    $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
		    $this->load->helper('date');
	  }
	

	
	/*public function onehop_traffic_analytics($jsondata){
		$data=array();
		//echo "<pre>"; print_R($jsondata);// die;
		if($jsondata->Service=="ClientDetails")
		{
			
			//$dataval= $jsondata->Data[0];
			//echo "<pre>"; print_R($jsondata->Data[0]);die;
			foreach($jsondata->Data as $row){
			$timestam = date("Y-m-d H:i:s",strtotime($row->Timestamp));
			$tabledata=array("APMAC"=>$row->APMAC,"ClientMAC"=>$row->ClientMAC,"Timestamp"=>$timestam,
			"Band"=>$row->Band,"Channel"=>$row->Channel,"Signal"=>$row->Signal,"Connected"=>$row->Connected,
			"added_on"=>date("Y-m-d H:i:s"));
			$this->DB2->insert('onehop_clientdetail',$tabledata);
			}
			 if ($this->DB2->affected_rows() >= 0)
			 {
				 $data['resultCode']=1;
				 $data['resultMsg']="Success";
			 }
			 else{
				 $data['resultCode']=0;
				 $data['resultMsg']="Fail";
			 }
			
		}
		else if($jsondata->Service=="TrafficLogs")
		{
		    foreach($jsondata->Data as $row){
			      $timestam = date("Y-m-d H:i:s",strtotime($row->Timestamp));
				$tabledata=array("APMAC"=>$row->APMAC,"APPubicIP"=>$row->APPubicIP,"APLANIP"=>$row->APLANIP,
			"ClientMAC"=>$row->ClientMAC,"SourceIP"=>$row->SourceIP,"SourcePort"=>$row->SourcePort,"DestIP"=>$row->DestIP,"DestPort"=>$row->DestPort,"NATSourceIP"=>$row->NATSourceIP,"NATSourcePort"=>$row->NATSourcePort,"Protocol"=>$row->Protocol,"Timestamp"=>$timestam,
			"added_on"=>date("Y-m-d H:i:s"));
			$this->DB2->insert('onehop_trafficlog',$tabledata);
		    }
			 if ($this->DB2->affected_rows() >= 0)
			 {
				 $data['resultCode']=1;
				 $data['resultMsg']="Success";
			 }
			 else{
				 $data['resultCode']=0;
				 $data['resultMsg']="Faissl";
			 }
			
		}
		else{
			$data['resultCode']=0;
				 $data['resultMsg']="Fail";
		}
		return $data;
		
	}*/
	
	  public function onehop_traffic_analytics($jsondata){
		    $data=array();
		    //echo "<pre>"; print_R($jsondata);// die;
		    if($jsondata['Service'] == "ClientDetails"){
			      $jsondata['addedon'] = date('Y-m-d H:i:s');
			      $this->mongo_db->insert('onehop_demologs', $jsondata);
			      $data['resultCode'] = 1;
			      $data['resultMsg'] = "Success";
		    }
		    else{
			      $data['resultCode'] = 0;
			      $data['resultMsg'] = "Fail";
		    }
		    return $data;
		
	  }
	
	  
	  public function getlocation_uid($apmac){
	      $location_uid = '';
	      $locmacidsQuery = $this->db->query("SELECT location_uid FROM wifi_location_access_point WHERE macid='".$apmac."'");
	      if($locmacidsQuery->num_rows() > 0){
		  $location_uid = $locmacidsQuery->row()->location_uid;
	      }
	      return $location_uid;
	  }
	  public function encrypt_data($clientMAC){
      
	      $max_msg_size = 1000;
	      $clientMAC = substr($clientMAC, 0, $max_msg_size);
	      $password = '0neh0p';
	      
	      // Initialization Vector, randomly generated and saved each time
	      //$salt = sha1(mt_rand());
	      //$iv = substr(sha1(mt_rand()), 0, 16);
	      
	      $salt = sha1($password);
	      $iv = substr(sha1($clientMAC), 0, 16);
	      
	      //echo "\n Password: $password \n Message: $clientMAC \n Salt: $salt \n IV: $iv\n";
	      
	      $encrypted = openssl_encrypt(
		"$clientMAC", 'aes-256-cbc', "$salt:$password", null, $iv
	      );
	      
	      $msg_bundle = "$salt:$iv:$encrypted";
	      return $msg_bundle;
	  }
	  public function decrypt_data($msg_bundle){
	      $password = '0neh0p';
	      // Parse iv and encrypted string segments
	      $components = explode( ':', $msg_bundle );
	      
	      //var_dump($components);
	      
	      $salt          = $components[0];
	      $iv            = $components[1];
	      $encrypted_msg = $components[2];
	      
	      $decrypted_msg = openssl_decrypt(
		"$encrypted_msg", 'aes-256-cbc', "$salt:$password", null, $iv
	      );
	      
	      if ( $decrypted_msg === false ) {
		die("Unable to decrypt message! (check password) \n");
	      }
	      
	      //$msg = substr( $decrypted_msg, 41 );
	      return $decrypted_msg;
	  }
	  public function check_dateformat(){
	      $timestamp = '2018-04-28 19:17:53';
	      $dbtimestamp = $this->mongo_db->date(strtotime("$timestamp"));
	      var_dump($dbtimestamp);
	      $lasttimestamp = date(DATE_ISO8601, $dbtimestamp->sec);
	      echo $lasttimestamp;
      
	      //$lastid = $this->mongo_db->insert('test_date', array('Timestamp' => $dbtimestamp));
	      $getdata = $this->mongo_db->get_where('onehop_logsdata', array('_id' => new MongoId('5af6c83083b391c42b012eff')));
	      echo '<pre>'; print_r($getdata);
	      
	      echo date('Y-m-d H:i:s', $getdata[0]['Timestamp']->sec);
	  }
	  public function extract_datalogs(){
	      ini_set('max_execution_time', 0);
	      //Add Index
	      //$this->mongo_db->add_index('onehop_logsdata', array('Timestamp' => 1, 'ClientMAC' => 1));
	      //$this->mongo_db->add_index('onehop_logsdata', array('Timestamp' => 1, 'APMAC' => 1)); die;
	      
	      //$this->find_timebetween_timestamps(); die;
	      //$this->find_duplicates(); die;
	      //$this->find_distance_ofmacids(); die;
	      
	      //$yesterday_date = date('Y-m-d', strtotime('-1 days'));
	      $yesterday_date = '2018-08-07';
	      $logdate_regex = new MongoRegex("/$yesterday_date/i");
	      $start_date = $this->mongo_db->date(strtotime($yesterday_date.' 00:00:00'));
	      $end_date = $this->mongo_db->date(strtotime($yesterday_date.' 23:59:59'));
	      
	      $logsQuery = $this->mongo_db->order_by(array('_id' => 'ASC'))->get_where('onehop_demologs', array('Data.Timestamp' => $logdate_regex));
	      if(count($logsQuery) > 0){
		  foreach($logsQuery as $logsdocument){
		      $logdata_array = $logsdocument['Data'];
		      //echo '<pre>'; print_r($logdata_array); die;
		      foreach($logdata_array as $logdata){
			  $apmac = $logdata['APMAC'];
			  $user_brandmac = explode(":" , $logdata['ClientMAC']);
			  $user_brandmac = $user_brandmac[0].$user_brandmac[1].$user_brandmac[2];
			  
			  $check_brandmacQ = $this->mongo_db->get_where('mobile_brand', array('macid' => $user_brandmac));
			  if(count($check_brandmacQ) != 0){
			      
			      $client_mac = $this->encrypt_data($logdata['ClientMAC']);
			      $timestamp = $logdata['Timestamp'];
			      
			      $timearr = explode(' ', $timestamp);
			      if(strtotime($timearr[0]) == strtotime($yesterday_date)){
				  
				  $location_uid = $this->getlocation_uid($apmac);
				  
				  $logdata['APMAC'] = $this->encrypt_data($logdata['APMAC']);
				  $logdata['ClientMAC'] = $client_mac;
				  $band_freq = $logdata['Band'];
				  $band_freq = substr($band_freq, 0 , strpos($band_freq, 'GHz', 0));
				  $frequency = ($band_freq * 1000);
				  $signal = $logdata['Signal'];
				  $freqval = ((27.55 - (20 * log10($frequency))+ abs($signal))/20);
				  $apdistance = pow(10,$freqval);
				  $apdistance = round($apdistance, 1);
				  
				  $logdata['location_uid'] = $location_uid;
				  $logdata['distance'] = $apdistance;
				  $logdata['datetime'] = "$timestamp";
				  
				  $checkmacid_indatabase = $this->mongo_db->order_by(array('_id' => 'ASC'))->get_where('onehop_logsdata', array('$and' => array( array('ClientMAC' => $client_mac), array('location_uid' => $location_uid), array('Timestamp' => array('$gte' => $start_date, '$lte' => $end_date) ) )));
				  
				  //echo json_encode($checkmacid_indatabase); die;
				  $datacount = count($checkmacid_indatabase);
				  if($datacount == 0){
				      //$logdata['Timestamp'] = new \MongoDate(strtotime("$timestamp"));
				      //$mongodate = date(DATE_ISO8601, $logdata['Timestamp']->sec);
				      $logdata['Timestamp'] = $this->mongo_db->date(strtotime("$timestamp"));
				      $logdata['avg_time'] = 0;
				      $logdata['slot'] = 1;
				      $this->mongo_db->insert('onehop_logsdata', $logdata);
				      //echo 'FIRST => '.$client_mac .' => '.$timestamp. '<br/>';
				  }else{
				      
				      /*if(($datacount % 2) != 0){
					  $logdata['Timestamp'] = $this->mongo_db->date(strtotime("$timestamp"));
					  $this->mongo_db->insert('test_logsdata', $logdata);
					  //echo 'SECOND => '.$client_mac .' => '.$timestamp. '=>' .'<br/>';
				      }else{*/
					  $mongo_index = ($datacount - 1);
					  $lastdocid = $checkmacid_indatabase[$mongo_index]['_id']->{'$id'};
					  $lasttimestamp = $checkmacid_indatabase[$mongo_index]['Timestamp'];
					  $lasttimestamp = date('Y-m-d H:i:s', $lasttimestamp->sec);
					  $lastslot = $checkmacid_indatabase[$mongo_index]['slot'];
					  
					  $ts1 = strtotime($timestamp);
					  $ts2 = strtotime($lasttimestamp);     
					  $seconds_diff = ($ts1 - $ts2);                            
					  $timediff = round(($seconds_diff/60) , 2);
					  
					  if($timediff > 15){
					      $logdata['Timestamp'] = $this->mongo_db->date(strtotime("$timestamp"));
					      $logdata['avg_time'] = 0;
					      $logdata['slot'] = ($lastslot + 1);
					      $this->mongo_db->insert('onehop_logsdata', $logdata);
					  }else{
      
					      if(($datacount % 2) != 0){
						  $logdata['Timestamp'] = $this->mongo_db->date(strtotime("$timestamp"));
						  $logdata['avg_time'] = $timediff;
						  $logdata['slot'] = $lastslot;
						  $this->mongo_db->insert('onehop_logsdata', $logdata);
						  //echo 'SECOND => '.$client_mac .' => '.$timestamp. '=>' .'<br/>';
					      }else{
						  $checkfirst_dataslot = $this->mongo_db->limit('1')->order_by(array('_id' => 'ASC'))->get_where('onehop_logsdata', array('$and' => array( array('ClientMAC' => $client_mac), array('location_uid' => $location_uid), array('datetime' => $logdate_regex), array('slot' => $lastslot) )));
						  if(count($checkfirst_dataslot) > 0){
						      $firsttimestamp = $checkfirst_dataslot[0]['Timestamp'];
						      $firsttimestamp = date('Y-m-d H:i:s', $firsttimestamp->sec);
						  }
						  
						  $ts1 = strtotime($timestamp);
						  $ts2 = strtotime($firsttimestamp);     
						  $seconds_diff = ($ts1 - $ts2);                            
						  $timediff = round(($seconds_diff/60) , 2);
						  
						  $logdata['Timestamp'] = $this->mongo_db->date(strtotime("$timestamp"));
						  $logdata['avg_time'] = $timediff;
						  $this->mongo_db->where(array('_id' => new MongoId($lastdocid)))->set($logdata)->update('onehop_logsdata');
					      }
					  }
				      //}
				  }
			      }
			  }
		      }
		  }
	      }
	  }
    
	  public function get_and_save_mac_to_device_info(){
		    //$filter_date = '2018-05-11';
		    
		    $filter_date = date('Y-m-d');
		    $start_date = $this->mongo_db->date(strtotime($filter_date.' 00:00:00'));
		    $end_date = $this->mongo_db->date(strtotime($filter_date.' 23:59:59'));
		    
		    // get today new mac id
		    $user_new_macid = $this->mongo_db->where(array('$and' => array(array('Timestamp' => array('$gte' => $start_date, '$lte' => $end_date)) )))->distinct('onehop_logsdata', 'ClientMAC');
		    
		    // get brad detail 
		    $get_brand_id = $this->mongo_db->get('mobile_brand');
		    $brand_mac_detail = array();
		    foreach($get_brand_id as $get_brand_id1){
			$brand_mac_detail[strtolower($get_brand_id1['macid'])] = strtolower($get_brand_id1['brand']);
		    }
		    //echo "<pre>"; print_r($brand_mac_detail);
		    //echo "<pre>"; print_r($user_new_macid); die;
		    
		    foreach($user_new_macid as $user_new_macid1){
			$decrypt_macid = $this->decrypt_data($user_new_macid1);
			$user_new_macid_three = explode(":" , $decrypt_macid);
			$user_new_macid_three = $user_new_macid_three[0].$user_new_macid_three[1].$user_new_macid_three[2];
			if(array_key_exists($user_new_macid_three, $brand_mac_detail))
			{
			    $brand_name = $brand_mac_detail[$user_new_macid_three];
			    $logdata = array();
			    $logdata['brand'] = $brand_name;
			    $logdata['model'] = '';
			    $logdata['price'] = '0';
			    $logdata['os_info'] = '';
			    $this->mongo_db->where(array('usermacid' => $user_new_macid1))->set($logdata)->update('mobile_mac_detail', array('upsert' => 1));
			    echo $brand_name."<br />";
			}
			else
			{
			    //echo "not exist;
			    $logdata = array();
			    $logdata['brand'] = $brand_name;
			    $logdata['model'] = '';
			    $logdata['price'] = '0';
			    $logdata['os_info'] = '';
			    $this->mongo_db->where(array('usermacid' => $user_new_macid1))->set($logdata)->update('mobile_mac_detail', array('upsert' => 1));
			}
		    }
		    
		    
		    
	  }

	
	
	
	
	


}

?>
