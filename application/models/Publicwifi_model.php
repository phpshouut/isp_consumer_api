<?php
class Publicwifi_model extends CI_Model{
     private $DB2;
     
     public function __construct(){
	 $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
     }
     // CI query changes
     public function location_count($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  //$isp_uid = '1111';
	  $data = array();
	  $public_active = 0;
	  $public_inactive = 0;
	  $hotel_active = 0;
	  $hotel_inactive = 0;
	  $hospital_active = 0;
	  $hospital_inactive = 0;
	  $institutional_active = 0;
	  $institutional_inactive = 0;
	  $enterprise_active = 0;
	  $enterprise_inactive = 0;
	  $caffe_active = 0;
	  $caffe_inactive = 0;
	  $retail_active = 0;
	  $retail_inactive = 0;
	  $custom_active = 0;
	  $custom_inactive = 0;
	  $purple_active = 0;
	  $purple_inactive = 0;
	  $offline_active = 0;
	  $offline_inactive = 0;
	  $this->DB2->select('status, location_type')->from('wifi_location');
	  $this->DB2->where(array('is_deleted' => '0', 'isp_uid' => $isp_uid));
	  $query = $this->DB2->get();
	  foreach($query->result() as $row){
		  if($row->location_type == '1'){
			  //public location
			  if($row->status == '1'){
				  $public_active = $public_active+1;
			  }else{
				  $public_inactive = $public_inactive+1;
			  }
		  }
		  elseif($row->location_type == '2'){
			  //sme location
			  if($row->status == '1'){
				  $hotel_active = $hotel_active+1;
			  }else{
				  $hotel_inactive = $hotel_inactive+1;
			  }
		  }
		  elseif($row->location_type == '3'){
			  //sme location
			  if($row->status == '1'){
				  $hospital_active = $hospital_active+1;
			  }else{
				  $hospital_inactive = $hospital_inactive+1;
			  }
		  }
		  elseif($row->location_type == '4'){
			  //sme location
			  if($row->status == '1'){
				  $institutional_active = $institutional_active+1;
			  }else{
				  $institutional_inactive = $institutional_inactive+1;
			  }
		  }
		  elseif($row->location_type == '5'){
			  //sme location
			  if($row->status == '1'){
				  $enterprise_active = $enterprise_active+1;
			  }else{
				  $enterprise_inactive = $enterprise_inactive+1;
			  }
		  }elseif($row->location_type == '6'){
			  //sme location
			  if($row->status == '1'){
				  $caffe_active = $caffe_active+1;
			  }else{
				  $caffe_inactive = $caffe_inactive+1;
			  }
		  }
		  elseif($row->location_type == '7'){
			  //sme location
			  if($row->status == '1'){
				  $retail_active = $retail_active+1;
			  }else{
				  $retail_inactive = $retail_inactive+1;
			  }
		  }
		  elseif($row->location_type == '8'){
			  //sme location
			  if($row->status == '1'){
				  $custom_active = $custom_active+1;
			  }else{
				  $custom_inactive = $custom_inactive+1;
			  }
		  }
		  elseif($row->location_type == '9'){
			  //sme location
			  if($row->status == '1'){
				  $purple_active = $purple_active+1;
			  }else{
				  $purple_inactive = $purple_inactive+1;
			  }
		  }
		   elseif($row->location_type == '10'){
			  //sme location
			  if($row->status == '1'){
				  $offline_active = $offline_active+1;
			  }else{
				  $offline_inactive = $offline_inactive+1;
			  }
		   }
	  }
		$data['public_active'] = $public_active;
		$data['public_inactive'] = $public_inactive;
		$data['hotel_active'] = $hotel_active;
		$data['hotel_inactive'] = $hotel_inactive;
		$data['hospital_active'] = $hospital_active;
		$data['hospital_inactive'] = $hospital_inactive;
		$data['institutional_active'] = $institutional_active;
		$data['institutional_inactive'] = $institutional_inactive;
		$data['enterprise_active'] = $enterprise_active;
		$data['enterprise_inactive'] = $enterprise_inactive;
		$data['caffe_active'] = $caffe_active;
		$data['caffe_inactive'] = $caffe_inactive;
		$data['retail_active'] = $retail_active;
		$data['retail_inactive'] = $retail_inactive;
		$data['custom_active'] = $custom_active;
		$data['custom_inactive'] = $custom_inactive;
		$data['purple_active'] = $purple_active;
		$data['purple_inactive'] = $purple_inactive;
		$data['offline_active'] = $offline_active;
		$data['offline_inactive'] = $offline_inactive;
		$data['total_location'] = $public_active+$public_inactive+$hotel_active+$hotel_inactive+$hospital_active+$hospital_inactive+$institutional_active+$institutional_inactive+$enterprise_active+$enterprise_inactive+$caffe_active+$caffe_inactive+$retail_active+$retail_inactive+$custom_active+$custom_inactive+$purple_active+$purple_inactive+$offline_active+$offline_inactive;
		//echo "<pre>";print_r($data);die;
		return $data;
     }

     // Now not use same function written in publicwif
     public function location_list($jsondata){
	  $data = array();
	  //{"isp_uid" : "300", "location_type" : "public", "state" : "3", "city": "5", "search_pattern" : "" }
	  $isp_uid = $jsondata->isp_uid;
	  $location_type = $jsondata->location_type;
	  $state = $jsondata->state;
	  $city = $jsondata->city;
	  $serach_pattern = $jsondata->search_pattern;
	  $locations = array();
	  $location_type_where = '';
	  if($location_type != ''){
	      $location_type_where = "AND location_type=".$location_type."";
	  }
	  $state_where = '';
	  if($state != ''){
	      $state_where = "AND state_id=".$state."";
	  }
	  $city_where = '';
	  if($city != ''){
	      $city_where = "AND city_id=".$city."";
	  }
	  $serach_pattern_where = '';
	  if($serach_pattern!= ''){
	      $serach_pattern_where = " AND (location_name LIKE '%$serach_pattern%' OR location_uid LIKE '%$serach_pattern%' OR contact_person_name LIKE '%$serach_pattern%' OR mobile_number LIKE '%$serach_pattern%')";
	  }
        $query = $this->DB2->query("select * from wifi_location where is_deleted = '0' and isp_uid = '$isp_uid' $location_type_where $state_where $city_where $serach_pattern_where order by location_name");
        if($query->num_rows() > 0){
            $data['resultCode'] = 1;
            $data['resultMsg'] = "Success";
            $i = 0;
	    $location_ids = array();
	    foreach($query->result() as $locrow){
	       $location_ids[] = $locrow->location_uid;
	    }
	       $location_ids = '"'.implode('", "', $location_ids).'"';
	       // get devide model
	       $device_model = array();
	       
	       $get_device = $this->DB2->query("select location_uid, device_model_name, router_type from wifi_location_access_point where location_uid IN ($location_ids)");
	       if($get_device->num_rows() > 0){
		    foreach($get_device->result() as $de_row){
			 if(array_key_exists($de_row->location_uid, $device_model)){
			      $device_model[$de_row->location_uid]['device_model_name'] = $device_model[$de_row->location_uid]['device_model_name'].', '.$de_row->device_model_name;
			 }else{
			      $device_model[$de_row->location_uid]['router_type'] = $de_row->router_type;
			      $device_model[$de_row->location_uid]['device_model_name'] = $de_row->device_model_name;
			 }
			 
		    }
	       }
	       //echo "<pre>";print_r($device_model);die;
            foreach($query->result() as $row){
                $locations[$i]['id'] = $row->id;
		$locations[$i]['location_type'] = $row->location_type;
                $locations[$i]['locationid'] = $row->location_uid;
                $locations[$i]['location_name'] = $row->location_name;
                $locations[$i]['state'] = $row->state;
                $locations[$i]['city'] = $row->city;
                $locations[$i]['contact_person_name'] = $row->contact_person_name;
                $locations[$i]['mobile_number'] = $row->mobile_number;
                $locations[$i]['nasip'] = $row->nasipaddress;
                $locations[$i]['status'] = $row->status;
		$device_name = '';
		$location_uid = $row->location_uid;
		if(isset($device_model[$location_uid])){
		    if($device_model[$location_uid]['router_type'] == 6){
			 $device_name = $device_model[$location_uid]['device_model_name'];
		    }
		    else if($device_model[$location_uid]['router_type'] == 7){
			 $device_name = 'RASBERRY PI';
		    }
		}
		$locations[$i]['device_name'] = $device_name;
                $i++;
            }
            $data['location_list'] = $locations;
        }else{
            $data['resultCode'] = 0;
            $data['resultMsg'] = "No result Found";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

     
     // CI query changes
     public function location_list_data_usages($jsondata){
	 $data = array();
	 //{"isp_uid" : "300", "location_type" : "public", "state" : "3", "city": "5", "search_pattern" : "" }
	 $isp_uid = $jsondata->isp_uid;
	 $locations = array();
	  $this->DB2->select('*')->from('wifi_location');
	  $this->DB2->where(array('is_deleted' => '0', 'isp_uid' => $isp_uid, 'status' => '1', 'is_deleted' => '0'));
	  $this->DB2->order_by('location_name');
	  $query = $this->DB2->get();
	 if($query->num_rows() > 0){
	     $data['resultCode'] = 1;
	     $data['resultMsg'] = "Success";
	     $i = 0;
	     foreach($query->result() as $row){
		 $locations[$i]['id'] = $row->id;
		 $locations[$i]['locationid'] = $row->location_uid;
		 $locations[$i]['location_name'] = $row->location_name;
		 $i++;
	     }
	     $data['location_list'] = $locations;
	 }else{
	     $data['resultCode'] = 0;
	     $data['resultMsg'] = "No result Found";
	 }
	 //echo "<pre>";print_r($data);die;
	 return $data;
     }
    
     // CI query changes
     public function state_list($jsondata){
	 $data = array();
	 $state = array();
	 $isp_uid = $jsondata->isp_uid;
	 //get state assign to isp
	  $this->db->select('state')->from('sht_isp_admin_region');
	  $this->db->where(array('isp_uid'=>$isp_uid, 'status' => '1'));
	  $get_state_query = $this->db->get();
	 $state_ids = array();
	 foreach($get_state_query->result() as $row_state){
	     $state_ids[] = $row_state->state;
	 }
	  //$state_ids = '"'.implode('", "', $state_ids).'"';
	  $this->db->select('*')->from('sht_states');
	  $this->db->where_in('id', $state_ids);
	  $stateQ = $this->db->get();
	 if($stateQ->num_rows() > 0){
	     $data['resultCode'] = '1';
	     $i = 0;
	     foreach($stateQ->result() as $row){
		 $state[$i]['state_id'] = $row->id;
		 $state[$i]['state_name'] = $row->state;
		 $i++;
	     }
	     $data['state'] = $state;
	 }else{
	     $data['resultCode'] = '0';
	 }
	 
	 return $data;
     }
     // CI query changes
     public function city_list($jsondata){
	 $state_id = $jsondata->state_id;
	 $data = array();
	  $this->db->select('city_id, city_name')->from('sht_cities');
	  $this->db->where('city_state_id', $state_id);
	  $query = $this->db->get();
	 $i = 0;
	 foreach($query->result() as $row){
	     $data[$i]['city_id'] = $row->city_id;
	     $data[$i]['city_name'] = $row->city_name;
	     $i++;
	 }
	 return $data;
     }
     // CI query changes
     public function zone_list($jsondata){
	  $city_id = $jsondata->city_id;
	  $isp_uid = $jsondata->isp_uid;
	  $data = array();
	  $this->db->select('id, zone_name')->from('sht_zones');
	  $this->db->where(array('city_id' => $city_id, 'isp_uid' => $isp_uid));
	  $query = $this->db->get();
	  $i = 0;
	  foreach($query->result() as $row){
	      $data[$i]['zone_id'] = $row->id;
	      $data[$i]['zone_name'] = $row->zone_name;
	      $i++;
	  }
	  return $data;
     }
     // CI query changes
     public function plan_list($jsondata){
	  $plan_type = $jsondata->plan_type;
	  $isp_uid = $jsondata->isp_uid;
	  $data = array();
	  if($plan_type !='')
	  {
	       if($plan_type == '6')
	       {
		    $this->DB2->select('srvid, srvname,plantype')->from('sht_services');
		    $this->DB2->group_start();
		    $this->DB2->or_where('plantype','2');
		    $this->DB2->or_where('plantype','5');
		    $this->DB2->group_end();
		    $this->DB2->where(array('isp_uid'=>$isp_uid, 'enableplan'=>'1', 'is_deleted' => '0'));
		    $query = $this->DB2->get();
	       }
	       else
	       {
		    $this->DB2->select('srvid, srvname,plantype')->from('sht_services');
		    $this->DB2->where(array('plantype' => $plan_type,'isp_uid'=>$isp_uid, 'enableplan'=>'1', 'is_deleted' => '0'));
		    $query = $this->DB2->get();
	       }
	  }
	  else{
	       $this->DB2->select('srvid, srvname,plantype')->from('sht_services');
	       $this->DB2->group_start();
	       $this->DB2->or_where('plantype','2');
	       $this->DB2->or_where('plantype','4');
	       $this->DB2->or_where('plantype','5');
	       $this->DB2->group_end();
	       $this->DB2->where(array('isp_uid'=>$isp_uid, 'enableplan'=>'1', 'is_deleted' => '0'));
	       $query = $this->DB2->get();
	  }
        
	  $i = 0;
	  foreach($query->result() as $row){
	      $data[$i]['srvid'] = $row->srvid;
	      $data[$i]['srvname'] = $row->srvname;
	      $data[$i]['plan_type'] = $row->plantype;
	      $i++;
	  }
	  return $data;
     }
     // CI query changes
     public function location_autogenerate_id($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  //get last locationid
	  $id = 0;
	  $this->DB2->select('id')->from('wifi_location')->where('isp_uid', $isp_uid)->order_by('id', 'DESC')->limit('1');
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	      $row = $query->row_array();
	      $id = $row['id'];
	  }
	  $id = $id+1;
	  // id will be next id
	  // check last next id assign from temp table
	  $next_id_assigned = 0;
	  $this->DB2->select('location_id')->from('wifi_locationid_temp')->where('isp_uid', $isp_uid)->order_by('id', 'DESC')->limit('1');
	  $query1 = $this->DB2->get();
	  if($query1->num_rows() > 0){
	      $row1 = $query1->row_array();
	      $next_id_assigned = $row1['location_id'];
	  }
	  // which will be next id
	  $next_id = '';
	  if($next_id_assigned >= $id ){
	      $next_id = $next_id_assigned+1;
	  }else{
	      $next_id = $id;
	  }
        // insert in temp table
	  $insert_data = array(
	       'location_id' => $next_id,
	       'isp_uid' => $isp_uid,
	       'created_on' => date('Y-m-d H:i:s')
	  );
	  $this->DB2->insert('wifi_locationid_temp', $insert_data);
	  $data['location_id'] = $isp_uid.$next_id;
	  $country_code = $this->get_country_code($isp_uid);
	  $data['country_code'] = $country_code;
	  return $data;
     }
     // CI query changes
     public function add_location($jsondata){
	  $location_display_name = '';
	  if(isset($jsondata->location_display_name)){
	       $location_display_name = $jsondata->location_display_name;
	  }
	  $hashtags = '';
	  if(isset($jsondata->hashtags)){
	       $hashtags = $jsondata->hashtags;
	  }
	  $enable_channel = '1';
	  if(isset($jsondata->enable_channel)){
	       $enable_channel = $jsondata->enable_channel;
	  }
	  $enable_nas_down_notification = '0';
	  if(isset($jsondata->enable_channel)){
	       $enable_nas_down_notification = $jsondata->enable_nas_down_notification;
	  }
	  $created_location_id = $jsondata->created_location_id;
	  $isp_uid = $jsondata->isp_uid;
	  $location_type = $jsondata->location_type;
	  $location_name = $jsondata->location_name;
	  $geo_address= $jsondata->geo_address;
	  $placeid= $jsondata->placeid;
	  $lat= $jsondata->lat;
	  $long= $jsondata->long;
	  $address1= $jsondata->address1;
	  $address2= $jsondata->address2;
	  $pin= $jsondata->pin;
	  $state= $jsondata->state;
	  $city= $jsondata->city;
	  $zone= $jsondata->zone;
	  $location_id= $jsondata->location_id;
	  $contact_person_name= $jsondata->contact_person_name;
	  $email_id= $jsondata->email_id;
	  $mobile_no= $jsondata->mobile_no;
	  $menu_display_option = isset($jsondata->menu_display_option) ? $jsondata->menu_display_option : '2';
	  $enable_payrolldebit = isset($jsondata->enable_payrolldebit) ? $jsondata->enable_payrolldebit : '0';
	  $return_policy_number = isset($jsondata->return_policy_number) ? $jsondata->return_policy_number : '0';
	  $location_price_surge = isset($jsondata->location_price_surge) ? $jsondata->location_price_surge : '0';
	  //get city_name and state_name
	  $city_name = '';
	  $state_name = '';
	  $zone_name = '';
	  $country_name = '';
	  $this->db->select('sc.city_name, ss.state, ss.zone_name, scon.name')->from('sht_cities as sc');
	  $this->db->where('sc.city_id', $city);
	  $this->db->join('sht_states as ss', 'sc.city_state_id = ss.id', 'INNER');
	  $this->db->join('sht_countries as scon', 'scon.id = ss.country_id', 'LEFT');
	  $get_name = $this->db->get();
	  if($get_name->num_rows() > 0){
	      $row = $get_name->row_array();
	      $city_name = $row['city_name'];
	      $state_name = $row['state'];
	      $zone_name = $row['zone_name'];
	      $country_name = strtolower($row['name']);
	      $country_name = trim(str_replace(' ', '', $country_name));
	  }
	  if($created_location_id == ''){
	       $location_brand = 'sh';
	       if($isp_uid == '176' || $isp_uid == '178' || $isp_uid == '179' || $isp_uid == '180' || $isp_uid == '121'){
		    $location_brand = 'ck';
	       }
	  //check balance is available
	    //$check_balance = $this->isp_license_data($isp_uid);
	    //if($check_balance == '1'){
	       $location_insert_data = array(
		    'enable_nas_down_noti' => $enable_nas_down_notification,
		    'enable_channel' => $enable_channel,
		    'location_uid' => $location_id,
		    'location_type' => $location_type,
		    'location_name' => $location_name,
		    'geo_address' => $geo_address,
		    'placeid' => $placeid,
		    'latitude' => $lat,
		    'longitude' => $long,
		    'region_id' => $zone,
		    'address_1' => $address1,
		    'address_2' => $address2,
		    'state' => $state_name,
		    'state_id' => $state,
		    'city' => $city_name,
		    'city_id' => $city,
		    'pin' => $pin,
		    'contact_person_name' => $contact_person_name,
		    'mobile_number' => $mobile_no,
		    'status' => '1',
		    'is_deleted' => '0',
		    'created_on' => date('Y-m-d H:i:s'),
		    'isp_uid' => $isp_uid,
		    'user_email' => $email_id,
		    'location_brand' => $location_brand,
		    'provider_id' => $isp_uid,
		    'hashtags' => $hashtags,
		    'zone_name' => $zone_name,
		    'country_name' => $country_name,
		    'foodmenu_options_list' => $menu_display_option,
		    'enable_payrolldebit' => $enable_payrolldebit,
		    'return_policy_number' => $return_policy_number,
		    'location_display_name' => $location_display_name,
		    'location_price_surge' => $location_price_surge
	       );
	       $insert = $this->DB2->insert('wifi_location', $location_insert_data);
		   
	       $created_location_id_return = $this->DB2->insert_id();
	       // for audit logs
	       if($location_type == 13)
	       {
		    $this->retail_audit_log_record('STORE_CREATE', $created_location_id_return);
	       }
	       // insert location deduction price
	       //get location amount
	      /* $location_price = $this->countrydetails($isp_uid);
	       $location_cost_deduct = '';
	       if(isset($location_price['cost_per_location'])){
		 $location_cost_deduct = $location_price['cost_per_location'];
		 $this->db->query("insert into sht_isp_passbook (isp_uid, total_active_ap, cost, added_on) values('$isp_uid', '1', '$location_cost_deduct', now())");
	       }
	    }else{
	       $created_location_id_return = "balance not available";
	    }*/
            
	  }
	  else{
	       $location_update_data = array(
		    'enable_nas_down_noti' => $enable_nas_down_notification,
		    'enable_channel' => $enable_channel,
		    'location_type' => $location_type,
		    'location_name' => $location_name,
		    'geo_address' => $geo_address,
		    'placeid' => $placeid,
		    'latitude' => $lat,
		    'longitude' => $long,
		    'region_id' => $zone,
		    'address_1' => $address1,
		    'address_2' => $address2,
		    'state' => $state_name,
		    'state_id' => $state,
		    'city' => $city_name,
		    'city_id' => $city,
		    'pin' => $pin,
		    'contact_person_name' => $contact_person_name,
		    'mobile_number' => $mobile_no,
		    'user_email' => $email_id,
		    'modified_on' => date('Y-m-d H:i:s'),
		    'hashtags' => $hashtags,
		    'zone_name' => $zone_name,
		    'country_name' => $country_name,
		    'foodmenu_options_list' => $menu_display_option,
		    'enable_payrolldebit' => $enable_payrolldebit,
		    'return_policy_number' => $return_policy_number,
		    'location_display_name' => $location_display_name,
		    'location_price_surge' => $location_price_surge
	       );
	       $this->DB2->where('id', $created_location_id);
	       $this->DB2->update('wifi_location', $location_update_data);
	       $created_location_id_return = $created_location_id;
	       // update offline version
	       $this->update_offline_version($created_location_id);
	  }
	  $hashtags = preg_replace('/(\w+)([#])/U', '\\1 \\2', $hashtags);
	  $has_array = explode(" ",$hashtags);
	  if(count($has_array) > 0)
	  {
	       foreach($has_array as $has_array_new)
	       {
		    if($has_array_new != '' && preg_match("/#(\\w+)/", $has_array_new))
		    {
			 $this->DB2->select('id')->from('wifi_location_hashtags');
			 $this->DB2->where('hashtags', $has_array_new);
			 $get_tags = $this->DB2->get();
			 if($get_tags->num_rows() <= 0)
			 {
			      $insert_data = array(
				   'hashtags' => $has_array_new,
				   'location_id' => $created_location_id,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
			      $this->DB2->insert('wifi_location_hashtags', $insert_data);
			 }
			 
			 
		    }
	       }
	  }
	  $data = array();
	  $data['created_location_id'] = $created_location_id_return;
	  return $data;
     }
     // CI query changes
     public function countrydetails($isp_uid){
	  $data = array();
	  $location_cost = '0';
	  $this->db->select('country_id')->from('sht_isp_admin')->where('isp_uid', $isp_uid);
	  $ispcountryQ = $this->db->get();
	  if($ispcountryQ->num_rows() > 0){
	       $crowdata = $ispcountryQ->row();
	       $countryid = $crowdata->country_id;
	       $this->db->select('*')->from('sht_countries')->where('id', $countryid);
	       $countryQ = $this->db->get();
	       if($countryQ->num_rows() > 0){
		    $rowdata = $countryQ->row();
		    $currid = $rowdata->currency_id;
		    $currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
		    if($currencyQ->num_rows() > 0){
			 $currobj = $currencyQ->row();
			 $currsymbol = $currobj->currency_symbol;
			 $data['currency'] = $currsymbol;
		    }
		    $data['demo_cost'] = $rowdata->demo_cost;
		    $data['cost_per_user'] = $rowdata->cost_per_user;
		    $location_cost = $rowdata->cost_per_location; // get from coutnry table
		    // if isp pricing set then get and replace by coutry pricing
		    $this->db->select('billing_frequency,cost_per_location')->from('sht_isp_publicwifi_pricing')->where('isp_uid', $isp_uid);
		    $isp_location_cost = $this->db->get();
		    if($isp_location_cost->num_rows() > 0){
			 $isp_cost_row = $isp_location_cost->row_array();
			 if($isp_cost_row['billing_frequency'] == "Y"){
			      $location_cost = ($isp_cost_row['cost_per_location']*12);
			 }else{
			      $location_cost = $isp_cost_row['cost_per_location'];
			 }
			 
		    }
		    $data['cost_per_location'] = $location_cost;
	       }
	  }
	  return $data;
     }
     
     public function isp_license_data($isp_uid){
     
	  $wallet_amt = 0; $passbook_amt = 0;
	  $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_isp_wallet WHERE isp_uid='$isp_uid' AND is_paid='1'");
	  if($walletQ->num_rows() > 0){
	      $wallet_amt = $walletQ->row()->wallet_amt;
	  }
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(cost),0) as activeusers_cost FROM sht_isp_passbook WHERE isp_uid='$isp_uid'");
        if($passbookQ->num_rows() > 0){
            $passrowdata = $passbookQ->row();
            $passbook_amt = $passrowdata->activeusers_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;
	//get location amount
	$location_price = $this->countrydetails($isp_uid);
	$location_cost_deduct = '';
	if(isset($location_price['cost_per_location'])){
	  $location_cost_deduct = $location_price['cost_per_location'];
	}
	  if($location_cost_deduct != ''){
		 if($balanceamt < $location_cost_deduct){
		   return 0;
		 }else{
		     return 1;
		 }
	  }else{
	    return 0;
	  }
        
     }
    
   
    

   
    

     // CI query changes
     public function add_captive_portal($jsondata){
	  $is_pinenable = "0";
	  if(isset($jsondata->is_pinenable)){
	       $is_pinenable = $jsondata->is_pinenable;
	  }
	  $cp1_plan_type = "0";
	  if(isset($jsondata->cp1_plan_type)){
	       $cp1_plan_type = $jsondata->cp1_plan_type;
	  }
	  $slider_background_color = "";
	  if(isset($jsondata->slider_background_color)){
	       $slider_background_color = $jsondata->slider_background_color;
	  }
	  $original_slider1 = "";
	  if(isset($jsondata->original_slider1)){
	       $original_slider1 = $jsondata->original_slider1;
	  }
	  $original_slider2 = "";
	  if(isset($jsondata->original_slider2)){
	       $original_slider2 = $jsondata->original_slider2;
	  }
	  
	  $is_sociallogin = "0";
	  if(isset($jsondata->is_socialloginenable)){
	       $is_sociallogin = $jsondata->is_socialloginenable;
	  }
	  
	  $login_with_mobile_email = "0";
	  if(isset($jsondata->login_with_mobile_email)){
	       $login_with_mobile_email = $jsondata->login_with_mobile_email;
	  }
	  
	  $is_otpdisabled = '0';
	  if(isset($jsondata->is_otpdisabled)){
	       $is_otpdisabled = $jsondata->is_otpdisabled;
	  }
	  $main_ssid = '';
	  if(isset($jsondata->main_ssid)){
	       $main_ssid = $jsondata->main_ssid;
	  }
	  $icon_color = "";
	  if(isset($jsondata->icon_color)){
	       $icon_color = $jsondata->icon_color;
	  }
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  $locationid = $jsondata->locationid;
	  $cp_type = $jsondata->cp_type;
	  $cp1_plan = 0;
	  $cp1_no_of_daily_session = 0;
	  if($jsondata->cp_type=="cp1" || $jsondata->cp_type=="cp_truck_stop" || $jsondata->cp_type=="cp4"){
	    $cp1_no_of_daily_session = $jsondata->cp1_no_of_daily_session;
	    $cp1_plan = $jsondata->cp1_plan;
	  }
	  $cp2_plan= ($jsondata->cp_type=="cp2")?$jsondata->cp2_plan:"";
	  $cp2_signip_data= ($jsondata->cp_type=="cp2")?$jsondata->cp2_signip_data:"";
	  $cp2_daily_data= ($jsondata->cp_type=="cp2")?$jsondata->cp2_daily_data:"";
	  $cp3_hybrid_plan= ($jsondata->cp_type=="cp3")?$jsondata->cp3_hybrid_plan:"";
	  $cp3_data_plan= ($jsondata->cp_type=="cp3")?$jsondata->cp3_data_plan:"";
	  $cp3_signip_data= ($jsondata->cp_type=="cp3")?$jsondata->cp3_signip_data:"";
	  $cp3_daily_data= ($jsondata->cp_type=="cp3")?$jsondata->cp3_daily_data:"";
	  $original_image = $jsondata->image_original;
	  $small_image = $jsondata->image_small;
	  $logo_image = $jsondata->image_logo;
	  $retail_original_image = '';
	  if(isset($jsondata->retail_image_original)){
	    $retail_original_image = $jsondata->retail_image_original;
	  }
	  $retail_small_image = '';
	  if(isset($jsondata->retail_image_small)){
	    $retail_small_image = $jsondata->retail_image_small;
	  }
	  $retail_logo_image = '';
	  if(isset($jsondata->retail_image_logo)){
	    $retail_logo_image = $jsondata->retail_image_logo;
	  }
	  $created_cp_id = '';
	  $error_msg = '';
	  //check already created or not
	  $this->DB2->select('id,cp_type')->from('wifi_location_cptype');
	  $this->DB2->where('location_id', $locationid);
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0)
	  {
	       $row = $query->row_array();
	       if($row['cp_type'] == $cp_type)
	       {
		    $update_image_query = array();
		    if($original_image != '' || $small_image != '' || $logo_image != ''){
			$update_image_query['original_image'] = $original_image;
			$update_image_query['small_image'] = $small_image;
			$update_image_query['logo_image'] = $logo_image;
		    }
		    $update_retail_image_query = array();
		    if($retail_original_image != '' || $retail_small_image != '' || $retail_logo_image != ''){
			$update_retail_image_query['retail_original_image'] = $retail_original_image;
			$update_retail_image_query['retail_small_image'] = $retail_small_image;
			$update_retail_image_query['retail_logo_image'] = $retail_logo_image;
		    }
		    $update_slider1_image = array();
		    if($original_slider1 != ""){
			 $update_slider1_image['custom_slider_image1'] = $original_slider1;
		    }
		    $update_slider2_image = array();
		    if($original_slider2 != ""){
			 $update_slider2_image['custom_slider_image2'] = $original_slider2;
		    }
		    $cp_update_data = array(
			 'colour_code' => $icon_color,
			 'uid_location' => $location_uid,
			 'cp_type' => $cp_type,
			 'cp1_plan' => $cp1_plan,
			 'cp1_no_of_daily_session' => $cp1_no_of_daily_session,
			 'cp2_plan' => $cp2_plan,
			 'cp2_signup_data' => $cp2_signip_data,
			 'cp2_daily_data' => $cp2_daily_data,
			 'cp3_hybrid_plan' => $cp3_hybrid_plan,
			 'cp3_data_plan' => $cp3_data_plan,
			 'cp3_signup_data' => $cp3_signip_data,
			 'cp3_daily_data' => $cp3_daily_data,
			 'is_otpdisabled' => $is_otpdisabled,
			 'main_ssid' => $main_ssid,
			 'is_sociallogin' => $is_sociallogin,
			 'is_loginemail' => $login_with_mobile_email,
			 'slider_background_color' => $slider_background_color,
			 'plan_type_time_hybrid_data' => $cp1_plan_type,
			 'is_enablepin' => $is_pinenable,
			 'updated_on' => date('Y-m-d H:i:s')
		    );
		    $cp_update_data = array_merge($cp_update_data,$update_image_query);
		    $cp_update_data = array_merge($cp_update_data,$update_retail_image_query);
		    $cp_update_data = array_merge($cp_update_data,$update_slider1_image);
		    $cp_update_data = array_merge($cp_update_data,$update_slider2_image);
		    $this->DB2->where('location_id', $locationid);
		    $update = $this->DB2->update('wifi_location_cptype', $cp_update_data);
		    $created_cp_id = $row['id'];
		    $this->update_cp_ssid_onehop($location_uid, $main_ssid);
	       }
	       else{
		    $error_msg = 'already';
	       }
            
	  }
	  else
	  {
	       $insert_cp_date = array(
		    'uid_location' => $location_uid,
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'cp_type' => $cp_type,
		    'cp1_plan' => $cp1_plan,
		    'cp1_no_of_daily_session' => $cp1_no_of_daily_session,
		    'cp2_plan' => $cp2_plan,
		    'cp2_signup_data' => $cp2_signip_data,
		    'cp2_daily_data' => $cp2_daily_data,
		    'cp3_hybrid_plan' => $cp3_hybrid_plan,
		    'cp3_data_plan' => $cp3_data_plan,
		    'cp3_signup_data' => $cp3_signip_data,
		    'cp3_daily_data' => $cp3_daily_data,
		    'created_on' => date('Y-m-d H:i:s'),
		    'original_image' => $original_image,
		    'small_image' => $small_image,
		    'logo_image' => $logo_image,
		    'is_otpdisabled' => $is_otpdisabled,
		    'main_ssid' => $main_ssid,
		    'retail_original_image' => $retail_original_image,
		    'retail_small_image' => $retail_small_image,
		    'retail_logo_image' => $retail_logo_image,
		    'colour_code' => $icon_color,
		    'custom_slider_image1' => $original_slider1,
		    'custom_slider_image2' => $original_slider2,
		    'is_sociallogin' => $is_sociallogin,
		    'is_loginemail' => $login_with_mobile_email,
		    'slider_background_color' => $slider_background_color,
		    'plan_type_time_hybrid_data' => $cp1_plan_type,
		    'is_enablepin' => $is_pinenable,
	       );
	       $insert = $this->DB2->insert('wifi_location_cptype', $insert_cp_date);
	       $created_cp_id = $this->DB2->insert_id();
	  }
     // check nas and plan assoc
	  $this->DB2->select('nasid')->from('wifi_location');
	  $this->DB2->where('location_uid', $location_uid);
	  $check = $this->DB2->get();
	  if($check->num_rows() > 0){
	       $check_row = $check->row_array();
	       $nasid = $check_row['nasid'];
	       if($nasid > 0){
		    if($cp_type == 'cp1' || $cp_type == 'cp_truck_stop' || $cp_type == 'cp4'){
			// check already associated
			 $this->DB2->select('*')->from('sht_plannasassoc');
			 $this->DB2->where(array('srvid' => $cp1_plan, 'nasid' => $nasid));
			 $check = $this->DB2->get();
			 if($check->num_rows() <= '0'){
			      $data_new = array(
				   'srvid' => $cp1_plan,
				   'nasid' => $nasid,
			      );
			      $this->DB2->insert('sht_plannasassoc', $data_new);
			 }
		    }
		    elseif($cp_type == 'cp2'){
			// check already associated
			$this->DB2->select('*')->from('sht_plannasassoc');
			$this->DB2->where(array('srvid' => $cp2_plan, 'nasid' => $nasid));
			$check = $this->DB2->get();
			 if($check->num_rows() <= '0'){
			       $data_new = array(
				    'srvid' => $cp2_plan,
				    'nasid' => $nasid,
			       );
			       $this->DB2->insert('sht_plannasassoc', $data_new);
			 }
		    }
		    elseif($cp_type == 'cp3'){
			 $this->DB2->select('*')->from('sht_plannasassoc');
			 $this->DB2->where(array('srvid' => $cp3_hybrid_plan, 'nasid' => $nasid));
			 $check = $this->DB2->get();
			 if($check->num_rows() <= '0'){
			      $data_new = array(
				   'srvid' => $cp3_hybrid_plan,
				   'nasid' => $nasid,
			      );
			      $this->DB2->insert('sht_plannasassoc', $data_new);
			 }
			 $this->DB2->select('*')->from('sht_plannasassoc');
			 $this->DB2->where(array('srvid' => $cp3_data_plan, 'nasid' => $nasid));
			 $check = $this->DB2->get();
			 if($check->num_rows() <= '0'){
			      $data_new = array(
				   'srvid' => $cp3_data_plan,
				   'nasid' => $nasid,
			      );
			      $this->DB2->insert('sht_plannasassoc', $data_new);
			 }
		    }
	       }
	  }
        
	  $data = array();
	  $data['created_cp_id'] = $created_cp_id;
	  $data['error'] = $error_msg;
	  return $data;
     }
     // CI query changes
     public function change_location_status($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  //get current status
	  $this->DB2->select('status')->from('wifi_location');
	  $this->DB2->where('location_uid', $location_uid);
	  $get = $this->DB2->get();
	  if($get->num_rows() > 0){
	       $row = $get->row_array();
	       $current_status = $row['status'];
	       $status_change = '';
	       if($current_status == '1'){
		   $status_change = '0';
	       }else{
		   $status_change = '1';
	       }
	       $this->DB2->set('status', $status_change);
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->update('wifi_location');
	  }
	 $data = array();
	 $data['resultCode'] = '1';
	 return $data;
     }
     // CI query changes
     public function edit_location_data($jsondata){
	  $data = array();
	  $location_uid = $jsondata->locationid;
	  $location_id = 0;
	  $location_type = 0;
	  $this->DB2->select('wl.*, n.nastype')->from('wifi_location as wl');
	  $this->DB2->where('wl.location_uid', $location_uid);
	  $this->DB2->join('nas as n', 'n.id = wl.nasid', 'LEFT');
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $row = $query->row_array();
	       $location_id = $row['id'];
	       $data['id'] = $row['id'];
	       $data['location_name'] = $row['location_name'];
	       $data['location_display_name'] = $row['location_display_name'];
	       $data['user_email'] = $row['user_email'];
	       $data['mobile_number'] = $row['mobile_number'];
	       $data['geo_address'] = $row['geo_address'];
	       $data['placeid'] = $row['placeid'];
	       $data['latitude'] = $row['latitude'];
	       $data['longitude'] = $row['longitude'];
	       $data['address_1'] = $row['address_1'];
	       $data['address_2'] = $row['address_2'];
	       $data['pin'] = $row['pin'];
	       $data['state_id'] = $row['state_id'];
	       $data['city_id'] = $row['city_id'];
	       $data['zone_id'] = $row['region_id'];
	       $data['location_uid'] = $row['location_uid'];
	       $data['contact_person_name'] = $row['contact_person_name'];
	       $data['nasconfigure'] = $row['nasconfigure'];
	       $data['nasipaddress'] = $row['nasipaddress'];
	       $data['location_type'] = $row['location_type'];
	       $location_type = $row['location_type'];
	       $data['is_voucher_used'] = $row['is_voucher_used'];
	       $data['nastype'] = $row['nastype'];
	       $data['hotel_portal_password'] = $row['password'];
	       $data['enable_channel'] = $row['enable_channel'];
	       $data['enable_nas_down_noti'] = $row['enable_nas_down_noti'];
	       $data['hashtags'] = $row['hashtags'];
	       $data['is_wifi_enabled'] = $row['is_wifi_enabled'];
	       $data['is_passiveprob_enabled'] = $row['is_passiveprob_enabled'];
	       $data['foodmenu_options_list'] = $row['foodmenu_options_list'];
	       $data['enable_payrolldebit'] = $row['enable_payrolldebit'];
	       $data['return_policy_number'] = $row['return_policy_number'];
	       $data['location_price_surge'] = $row['location_price_surge'];
	       if($row['nasconfigure'] == '1'){
		    $isp_uid = '';
		    if(isset($jsondata->isp_uid)){
			 $isp_uid = $jsondata->isp_uid;
		    }
		    $requestData = array(
		     'location_id' => $location_id,
		     'isp_uid' => $isp_uid
		    );
		    $data_request = json_decode(json_encode($requestData));
		    $url = $this->get_isp_url($data_request);
		    if($url['url'] != ''){
			 if($row['location_type'] == '10'){
			      $url = $url['url']."?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;
			 }else{
			      //$url = $url['url']."/login.php?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;
			      $url = $url['url']."?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;
			 }
			 
		    }else{
			 $url = '';
		    }
		    
		    if($row['bitly_url'] != ''){
			$bitly_url = $row['bitly_url'];
		    }else{
			 $bitly_url = $this->get_bitly_short_url($url);
			 if($bitly_url == ''){
			      $bitly_url = $url;
			 }else{
			      $this->DB2->set('bitly_url', $bitly_url);
			      $this->DB2->where('id', $location_id);
			      $this->DB2->update('wifi_location');
			 }
		    }
		    
		    $data['cp_url'] = $bitly_url;
		    //$data['cp_url'] = $url['url']."/login.php?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;
	       }
	       else
	       {
		    if($row['location_type'] == '13'){
			 $bitly_url = $row['bitly_url'];
			 $data['cp_url'] = $bitly_url;
		    }
	       }
	  }
	  $cp_hotel_plan_type = '0';
	  // get cp information
	  $this->DB2->select('*')->from('wifi_location_cptype');
	  $this->DB2->where('uid_location', $location_uid);
	  $query_cp = $this->DB2->get();
	  if($query_cp->num_rows() > 0){
	       $row_cp = $query_cp->row_array();
	       $data['cp_cafe_plan_id'] = $row_cp['cafe_plan'];
	       $data['cp_type'] = $row_cp['cp_type'];
	       $data['cp1_plan'] = $row_cp['cp1_plan'];
	       $data['cp1_no_of_daily_session'] = $row_cp['cp1_no_of_daily_session'];
	       $data['cp2_plan'] = $row_cp['cp2_plan'];
	       $data['cp2_signup_data'] = $row_cp['cp2_signup_data'];
	       $data['cp2_daily_data'] = $row_cp['cp2_daily_data'];
	       $data['cp3_hybrid_plan'] = $row_cp['cp3_hybrid_plan'];
	       $data['cp3_data_plan'] = $row_cp['cp3_data_plan'];
	       $data['cp3_signup_data'] = $row_cp['cp3_signup_data'];
	       $data['cp3_daily_data'] = $row_cp['cp3_daily_data'];
	       $data['cp_hotel_plan_type'] = $row_cp['cp_hotel_plan_type'];
	       $data['cp_enterprise_user_type'] = $row_cp['entrprise_usertype'];
	       $data['main_ssid'] = $row_cp['main_ssid'];
	       $data['guest_ssid'] = $row_cp['guest_ssid'];
	       $data['is_otpdisabled'] = $row_cp['is_otpdisabled'];
	       $original_image_path = '';
	       if($row_cp['original_image'] != ''){
		  $original_image_path = CLOUD_IMAGEPATH.'isp_location_logo/original/'.$row_cp['original_image'];
	       }
	       $data['original_image'] = $original_image_path;
	       $logo_image_path = '';
	       if($row_cp['logo_image'] != ''){
		  $logo_image_path = CLOUD_IMAGEPATH.'isp_location_logo/logo/'.$row_cp['logo_image'];
	       }
	       $data['logo_image'] = $logo_image_path;
	       $data['is_payment_gateway_disable'] = $row_cp['is_pgdisabled'];
	       
	       $retail_original_image_path = '';
	       if($row_cp['retail_original_image'] != ''){
		  $retail_original_image_path = CLOUD_IMAGEPATH.'retail_background_image/original/'.$row_cp['retail_original_image'];
	       }
	       $data['retail_original_image'] = $retail_original_image_path;
	       
	       $slider1_image = '';
	       if($row_cp['custom_slider_image1'] != ''){
		  $slider1_image = CLOUD_IMAGEPATH.'retail_background_image/original/'.$row_cp['custom_slider_image1'];
	       }
	       $data['slider1_image'] = $slider1_image;
	       
	       $slider2_image = '';
	       if($row_cp['custom_slider_image2'] != ''){
		  $slider2_image = CLOUD_IMAGEPATH.'retail_background_image/original/'.$row_cp['custom_slider_image2'];
	       }
	       $data['slider2_image'] = $slider2_image;
	    
	       $data['is_wifidisabled'] = $row_cp['is_wifidisabled'];
	       $data['is_sendvcemail'] = $row_cp['is_sendvcemail'];
	       $data['is_loginemail'] = $row_cp['is_loginemail'];
	       $data['is_sociallogin'] = $row_cp['is_sociallogin'];
	       $data['is_enablepin'] = $row_cp['is_enablepin'];
	       $data['colour_code'] = $row_cp['colour_code'];
	    
	       // for custom location
	       $data['custom_url'] = $row_cp['custom_url'];
	       $data['custom_plan_type'] = $row_cp['custom_plan_type'];
	       $data['custom_planid'] = $row_cp['custom_planid'];
	       $data['custom_signup_data'] = $row_cp['custom_signup_data'];
	       $data['custom_daily_data'] = $row_cp['custom_daily_data'];
	       $data['custom_daily_no_of_session'] = $row_cp['custom_daily_no_of_session'];
	    
	       $data['slider_background_color'] = $row_cp['slider_background_color'];
	       
	       $data['offline_frequency_hour'] = $row_cp['frequency_hour'];
	       $data['offline_cp_content_type'] = $row_cp['offline_cp_content_type'];
	       $data['offline_cp_source_folder_path'] = $row_cp['offline_cp_source_folder_path'];
	       $data['offline_cp_landing_page_path'] = $row_cp['offline_cp_landing_page_path'];
	       $data['is_offline_registration'] = $row_cp['is_offline_registration'];
	       $data['location_plan_type'] = $row_cp['plan_type_time_hybrid_data'];
	       $data['offline_content_title'] = $row_cp['offline_content_title'];
	       $data['is_white_theme'] = $row_cp['is_white_theme'];
	       $data['is_preaudit_disable'] = $row_cp['is_preaudit_disable'];
	       $data['audit_ticket_email_send'] = $row_cp['audit_ticket_email_send'];
	       $data['tatdays'] = $row_cp['tatdays'];
	  }
	  else
	  {
	       $data['cp_cafe_plan_id'] = '';
	       if($location_type == 20){
		    $data['cp_type'] = 'cp_truck_stop';
	       }else{
		    $data['cp_type'] = '';
	       }
	       $data['cp1_plan'] = '';
	       $data['cp1_no_of_daily_session'] = '';
	       $data['cp2_plan'] = '';
	       $data['cp2_signup_data'] = '';
	       $data['cp2_daily_data'] = '';
	       $data['cp3_hybrid_plan'] = '';
	       $data['cp3_data_plan'] = '';
	       $data['cp3_signup_data'] = '';
	       $data['cp3_daily_data'] = '';
	       $data['original_image'] = '';
	       $data['logo_image'] = '';
	       $data['cp_hotel_plan_type'] = '0';
	       $data['cp_enterprise_user_type'] = '0';
	       $data['main_ssid'] = '';
	       $data['guest_ssid'] = '';
	       $data['is_otpdisabled'] = '0';
	       $data['is_payment_gateway_disable'] = '0';
	    
	       $data['retail_original_image'] = '';
	       $data['slider1_image'] = '';
	       $data['slider2_image'] = '';
	    
	       $data['is_wifidisabled'] = '0';
	       $data['is_sendvcemail'] = '0';
	       $data['is_loginemail'] = '0';
	       $data['is_sociallogin'] = '0';
	       $data['is_enablepin'] = '0';
	       $data['colour_code'] = '0';
	     
	       // for custom location
	      $data['custom_url'] = '';
	      $data['custom_plan_type'] = '';
	      $data['custom_planid'] = '';
	      $data['custom_signup_data'] = '';
	      $data['custom_daily_data'] = '';
	      $data['custom_daily_no_of_session'] = '';
	    
	       $data['slider_background_color'] = '0';
	    
	       $data['offline_frequency_hour'] = '0';
	       $data['offline_cp_content_type'] = '0';
	       $data['offline_cp_source_folder_path'] = '';
	       $data['offline_cp_landing_page_path'] = '';
	       $data['is_offline_registration'] = '0';
	       $data['location_plan_type'] = '0';
	       $data['offline_content_title'] = '';
	       $data['is_white_theme'] = '0';
	       $data['is_preaudit_disable'] = '0';
	       $data['audit_ticket_email_send'] = '';
	       $data['tatdays'] = '3';
	  }
	
	  $this->DB2->select('*')->from('offline_event_module_turn_reature');
	  $this->DB2->where('location_uid', $location_uid);
	  $query_turn_reature = $this->DB2->get();
	  if($query_turn_reature->num_rows() > 0)
	  {
	       $row_turn_feature = $query_turn_reature->row_array();
	       $data['is_live_chat_enable'] = $row_turn_feature['is_live_chat_enable'];
	       $data['is_discuss_enable'] = $row_turn_feature['is_discuss_enable'];
	       $data['is_information_enable'] = $row_turn_feature['is_information_enable'];
	       $data['is_poll_enable'] = $row_turn_feature['is_poll_enable'];
	       $data['is_download_enable'] = $row_turn_feature['is_download_enable'];
	  }
	  else
	  {
	      $data['is_live_chat_enable'] = 0;
	       $data['is_discuss_enable'] = 0;
	       $data['is_information_enable'] = 0;
	       $data['is_poll_enable'] = 0;
	       $data['is_download_enable'] = 0; 
	  }
	  $this->DB2->select('id, field_name, field_type, is_enable')->from('offline_event_module_signup_fields');
	  $this->DB2->where(array('location_uid' => $location_uid, 'is_deleted' => '0'));
	  $query_event_signup = $this->DB2->get();
	  $event_model_extra_signup_fields = array();
	  $ems = 0;
	  if($query_event_signup->num_rows() > 0)
	  {
	       foreach($query_event_signup->result() as $row_signup)
	       {
		    if($row_signup->field_type == '1'){
			$data['event_module_signup_user_name_require'] = $row_signup->is_enable; 
		    }
		    else if($row_signup->field_type == '2'){
			$data['event_model_signup_user_organisation_require'] = $row_signup->is_enable; 
		    }
		    else if($row_signup->field_type == '3'){
			$data['event_module_signup_nick_name'] = $row_signup->is_enable; 
		    }
		    else if($row_signup->field_type == '4'){
			$data['event_module_signup_email'] = $row_signup->is_enable; 
		    }
		    else if($row_signup->field_type == '5'){
			$data['event_module_signup_phone'] = $row_signup->is_enable; 
		    }
		    else
		    {
			 $event_model_extra_signup_fields[$ems]['id'] = $row_signup->id;
			 $event_model_extra_signup_fields[$ems]['field_name'] = $row_signup->field_name;
			 $ems++;
		    }
	       }
	  }
	  else
	  {
	       $data['event_module_signup_user_name_require'] = 0;
	       $data['event_model_signup_user_organisation_require'] = 0;
	       $data['event_module_signup_nick_name'] = 0;
	       $data['event_module_signup_email'] = 0;
	       $data['event_module_signup_phone'] = 0; 
	  }
	  $data['event_model_extra_signup_fields'] = $event_model_extra_signup_fields; 
	  // get offline category list
	  $offline_category = array();
	  $this->DB2->select('wolc.*, woc.category_name')->from('wifi_offline_location_category as wolc');
	  $this->DB2->where('wolc.location_uid', $location_uid);
	  $this->DB2->join('wifi_offer_category as woc', 'wolc.category_id = woc.id', 'INNER');
	  $query_offline_category = $this->DB2->get();
	  $oi = 0;
	  foreach($query_offline_category->result() as $offline_row){
	       $offline_category[$oi]['id'] = $offline_row->id;
	       $offline_category[$oi]['sub_category_name'] = $offline_row->sub_category_name;
	       $icon_path = '';
	       if($offline_row->icon != ''){
		  $icon_path  = CLOUD_IMAGEPATH.'offline_icon/original/'.$offline_row->icon;
	       }
	       
	       $offline_category[$oi]['icon_path'] = $icon_path;
	       $offline_category[$oi]['category_name'] = $offline_row->category_name;
	       $oi++;
	  }
	  $data['offline_category'] = $offline_category;
	  // hotel wifi plan ids
	  $hotel_plan_list = array();
	  $this->DB2->select('ss.srvname, ss.srvid')->from('ht_planassociation as htp');
	  $this->DB2->where('htp.location_uid', $location_uid);
	  $this->DB2->join('sht_services as ss', 'htp.plan_id = ss.srvid', 'INNER');
	  $get_hotel_plan = $this->DB2->get();
	  $i = 0;
	  if($get_hotel_plan->num_rows()> 0){
	    foreach($get_hotel_plan->result() as $get_hotel_plan1){
		 $hotel_plan_list[$i]['plan_id'] = $get_hotel_plan1->srvid;
		 $hotel_plan_list[$i]['plan_name'] = $get_hotel_plan1->srvname;
		 $i++;
	    }
	  }
	  $data['hotel_plan_list'] = $hotel_plan_list;
	
	// get enterprise location user type list
	  $enterprise_user_list = array();
	  $this->DB2->select('*')->from('location_usertype');
	  $this->DB2->where('location_id', $location_id);
	  $get_enterprise_user_list = $this->DB2->get();
	  $i = 0;
	  if($get_enterprise_user_list->num_rows()> 0){
	    foreach($get_enterprise_user_list->result() as $get_enterprise_user_list1){
		 $enterprise_user_list[$i]['id'] = $get_enterprise_user_list1->id;
		 $enterprise_user_list[$i]['user_type'] = $get_enterprise_user_list1->usertype;
		 $i++;
	    }
	  }
	  $data['hotel_plan_list'] = $hotel_plan_list;
	  $data['enterprise_user_list'] = $enterprise_user_list;
	  $isp_uid = '';
          if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
          }
	  $country_code = $this->get_country_code($isp_uid);
	  $data['country_code'] = $country_code;
	  
	  $this->DB2->select('name_require,email_mobile_require,age_require,gender_require');
	  $this->DB2->from('offline_contestifi_extra_feature')->where('location_id', $location_id);
	  $query_extra = $this->DB2->get();
	  if($query_extra->num_rows() > 0){
	       $row_extra = $query_extra->row_array();
	       $data['name_require'] = $row_extra['name_require'];
	       $data['email_mobile_require'] = $row_extra['email_mobile_require'];
	       $data['age_require'] = $row_extra['age_require'];
	       $data['gender_require'] = $row_extra['gender_require'];
	  }
	  else{
	       $data['name_require'] = '0';
	       $data['email_mobile_require'] = '0';
	       $data['age_require'] = '0';
	       $data['gender_require'] = '0';
	  }
        //echo "<pre>";print_r($data);die;
        return $data;
     }
    

     // CI query changes
     public function get_bitly_short_url($url) {
	  $login = 'shouut';
	  $appkey = 'R_495a520806484396941bd1a37d70228b';
	  $query = array(
	       "version" => "2.0.1",
	       "longUrl" => $url,
	       "login" => $login, // replace with your login
	       "apiKey" => $appkey // replace with your api key
	  );
	  $query = http_build_query($query);
	  $ch = curl_init();
	  curl_setopt($ch, CURLOPT_URL, "http://api.bit.ly/shorten?".$query);
	  curl_setopt($ch, CURLOPT_HEADER, 0);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  $response = curl_exec($ch);
	  curl_close($ch);
	  $response = json_decode($response);
	  if($response->errorCode == 0 && $response->statusCode == "OK") {
	       return $response->results->{$url}->shortUrl;
	  }else {
	       return '';
	  }
     }


     // CI query changes
     public function nas_list($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $router_type = $jsondata->router_type;
	  $data = array();
	  $return_array = array();
	  if($router_type == '6' || $router_type == '7'){
	       $this->DB2->select('id, nasname,shortname')->from('nas');
	       $this->DB2->where('nastype', $router_type);
	       $query = $this->DB2->get();
	  }else{
	       $this->DB2->select('id, nasname,shortname')->from('nas');
	       $this->DB2->where(array('isp_uid'=>$isp_uid, 'is_used' => '0', 'nastype' => $router_type));
	       $query = $this->DB2->get();
	  }
        
        $i = 0;
        foreach($query->result() as $row){
            $data[$i]['id'] = $row->id;
            $data[$i]['nasname'] = $row->nasname;
            $data[$i]['shortname'] = $row->shortname;
            $i++;
        }
        return $data;
     }
     // CI query changes
     public function get_isp_url($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $location_id = '';
	  if(isset($jsondata->location_id)){
	       $location_id = $jsondata->location_id;
	  }
	  $this->db->select('portal_url, subdomain_ip, isp_domain_name,domain_exists,isp_domain_protocol')->from('sht_isp_admin');
	  $this->db->where('isp_uid', $isp_uid);
	  $query = $this->db->get();
	  $path = '';
	  $cp_folder = '';
	  $subdomain_ip = '';
	  $main_ssid = '';
	  $guest_ssid = '';
	  $cp_type = '';
	  $custom_url = '';
	  $offline_cp_type = '';
	  $offline_static_langing_path = '';
	  $isp_domain_name = '';
	  $isp_domain_protocol = '';
	  $domain_exists = '0';
	  if($query->num_rows() > 0){
	       $row = $query->row_array();
	       $path =  $row['portal_url'];
	       $subdomain_ip =  $row['subdomain_ip'];
	       $isp_domain_name =  $row['isp_domain_name'];
	       $isp_domain_protocol =  $row['isp_domain_protocol'];
	       $domain_exists =  $row['domain_exists'];
	       //get cptype;
	       $this->DB2->select('offline_cp_content_type,offline_cp_landing_page_path,custom_url,cp_type, main_ssid, guest_ssid')->from('wifi_location_cptype');
	       $this->DB2->where(array('isp_uid'=>$isp_uid, 'location_id' => $location_id));
	       $get_cptype = $this->DB2->get();
	       if($get_cptype->num_rows() > 0){
		    $get_cptype_row = $get_cptype->row_array();
		    $cp_type =  $get_cptype_row['cp_type'] ;
		    $custom_url = $get_cptype_row['custom_url'] ;
		    if($get_cptype_row['cp_type'] == 'cp1'){
			$cp_folder = 'hotspot';
		    }
		    elseif($get_cptype_row['cp_type'] == 'cp_truck_stop'){
			$cp_folder = 'hotspot_truck';
		    }
		    elseif($get_cptype_row['cp_type'] == 'cp2'){
			$cp_folder = 'hotspot_v2';
		    }
		    elseif($get_cptype_row['cp_type'] == 'cp3'){
			 $cp_folder = 'hotspot_v3';
		    }elseif($get_cptype_row['cp_type'] == 'cp4'){
			 //$cp_folder = 'hotspot_v4';
			 $cp_folder = 'hotspot';
		    }elseif($get_cptype_row['cp_type'] == 'cp_hotel'){
			 $cp_folder = 'hotspot_htv2';
		    }elseif($get_cptype_row['cp_type'] == 'cp_hospital'){
			 $cp_folder = 'hotspot_hsptlv2';
		    }elseif($get_cptype_row['cp_type'] == 'cp_institutional'){
			 $cp_folder = 'hotspot_institutev2';
		    }elseif($get_cptype_row['cp_type'] == 'cp_enterprise'){
			 $cp_folder = 'hotspot_enterprisev2';
		    }elseif($get_cptype_row['cp_type'] == 'cp_club'){
			 $cp_folder = 'hotspot_club';
		    }elseif($get_cptype_row['cp_type'] == 'cp_cafe'){
			 $cp_folder = 'hotspot_cafe';
		    }elseif($get_cptype_row['cp_type'] == 'cp_retail'){
			 $cp_folder = 'hotspot_retail';
		    }elseif($get_cptype_row['cp_type'] == 'cp_purple'){
			 $cp_folder = 'hotspot_purplebox';
		    }
		    $main_ssid = $get_cptype_row['main_ssid'];
		    $guest_ssid = $get_cptype_row['guest_ssid'];
		    $offline_cp_type = $get_cptype_row['offline_cp_content_type'];
		    $offline_static_langing_path = $get_cptype_row['offline_cp_landing_page_path'];
	       }
	  }
	  $check_subdomain_created = $this->getisp_accounttype($isp_uid);
	  $router_type = 'mikrotik_kt';
	  //get router type
	  $this->DB2->select('ns.nastype')->from('wifi_location as wl');
	  $this->DB2->where('wl.id', $location_id);
	  $this->DB2->join('nas as ns', 'wl.nasid = ns.id', 'inner');
	  $get_router_type = $this->DB2->get();
	  if($get_router_type->num_rows() > 0){
	       $get_type_row = $get_router_type->row_array();
	       if($get_type_row['nastype'] == '1'){
		    $router_type = 'mikrotik_kt';
	       }elseif($get_type_row['nastype'] == '6'){
		    $router_type = 'onehop';
	       }
	  }
	  if(isset($jsondata->for_mikrotik) && $jsondata->for_mikrotik != ''){
	       // for mikrotik router new setup
	       $router_type = 'mikrotik_kt';
	  }
	  
	  if($cp_type == 'cp_custom'){
	       $data['url'] = $custom_url;
	  }elseif($cp_type == 'cp_offline'){
	       if($offline_cp_type == '1'){
		    $data['url'] = OFFLINE_CODE.'isp_hotspot/offline/offlinedynamic_bitly/index.php';//hardcoded dymanic path
	       }else{
		    $data['url'] = $offline_static_langing_path;
	       }
	  }
	  elseif($cp_type == 'cp_visitor'){
	       $data['url'] = OFFLINE_CODE.'isp_hotspot/offline/visitoroffline_bitly/index.php';
	       
	  }
	  elseif($cp_type == 'cp_eventguest'){
	       $data['url'] = OFFLINE_CODE.'isp_hotspot/offline/eventguest_bitly/index.php';
		   $data['urlguest'] = OFFLINE_CODE.'isp_hotspot/offline/eventguesttable_bitly/index.php';
	       
	  }
	  elseif($cp_type == 'cp_contestifi'){
	       //$data['url'] = OFFLINE_CODE.'isp_hotspot/offline/contestifi_bitly/index.php';
	       $data['url'] = OFFLINE_CODE.'isp_hotspot/offline/contestifi_quiz/index.php';
	       
	  }
	  elseif($cp_type == 'cp_retail_audit'){
	       $data['url'] = OFFLINE_CODE.'retail_audit/index.php';
	       
	  }
	  elseif($cp_type == 'cp_event_module'){
	       $data['url'] = OFFLINE_CODE.'isp_hotspot/offline/event_mgmt_bitly/index.php';
	       
	  }
	  elseif($cp_type == 'cp_foodle'){
	       $data['url'] = 'https://www.shouut.com/middlewarewani/login.php';
	       
	  }
	  else{
	       if($domain_exists == '1'){
		    $data['url'] = $isp_domain_protocol.$isp_domain_name."/".$router_type."/".$cp_folder."/login.php";
	       }
	       else{
		    if(strtolower($check_subdomain_created) == 'paid'){
			 if(IS_INDIA_COUNTRY == '1'){
			      //$data['url'] = "https://".$path.".shouut.com/".$router_type."/".$cp_folder;
			      $data['url'] = ISP_URL.$path.".shouut.com/".$router_type."/".$cp_folder."/login.php";
			 }else{
			      $data['url'] = ISP_URL.$path.".shouut.com/".$router_type."/".$cp_folder."/login.php";
			 }
			 
		    }else{
			 $data['url'] = ISP_URL.$path.".shouut.com/".$router_type."/".$cp_folder."/login.php";
		    }
	       }
	       
	  }
	  $data['subdomain_ip'] = $subdomain_ip;
	  $data['main_ssid'] = $main_ssid;
	  $data['guest_ssid'] = $guest_ssid;
	  return $data;
     }

     // CI query changes
     public function get_nas_detail($jsondata){
	  $data = array();
	  $nasid = $jsondata->nasid;
	  $this->DB2->select('nasname,shortname,secret,is_dynamic')->from('nas');
	  $this->DB2->where('id', $nasid);
	  $get_nasname = $this->DB2->get();
	  $username = '';
	  $nasaddress = '';
	  $secret = '';
	  $is_dynamic = 0;
	  if($get_nasname->num_rows() > 0){
	      $row = $get_nasname->row_array();
	      $username = $row['shortname'];
	      $nasaddress = $row['nasname'];
	      $secret = $row['secret'];
	      $is_dynamic = $row['is_dynamic'];
	  }
	  $data['username'] = $username;
	  $data['nasaddress'] = $nasaddress;
	  $data['secret'] = $secret;
	  $data['is_dynamic'] = $is_dynamic;
	  return $data;
     }
    
     // CI query changes
     public function total_data_used($jsondata){
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
	  $where_array = array();
	  if($state != ''){
	      $where_array['wl.state_id'] = $state;
	  }
	  $city_where = '';
	  if($city != ''){
	      $where_array['wl.city_id'] = $city;
	  }
	  $data = array();
	  $date_from = date('Y-m-d', strtotime("$jsondata->from"));
	  $date_to = date('Y-m-d', strtotime("$jsondata->to"));
	  $location_filter_query = '';
	  $live_location_nasip = array();
	  $onehop_location_uid = array();
	  $onehop_macid = array();
	  
	  $this->DB2->select('wl.location_uid,wl.nasipaddress,ns.nastype')->from('wifi_location as wl');
	  $this->DB2->where(array('wl.isp_uid' => $jsondata->isp_uid, 'wl.status' => '1', 'wl.is_deleted' => '0'));
	  if(count($jsondata->locations) > 0){
	       $this->DB2->where_in('wl.location_uid', $jsondata->locations);
	  }
	  $this->DB2->where($where_array);
	  $this->DB2->join('nas as ns', 'wl.nasid = ns.id', 'LEFT');
	  $query = $this->DB2->get();
	  foreach($query->result() as $row){
	       if($row->nastype == '6'){
		    $onehop_location_uid[] = $row->location_uid;
	       }else{
		    if($row->nasipaddress != '')
		    {
			 $live_location_nasip[] = $row->nasipaddress;
		    }
		 
	       }
	  }
	  //get macids
	  if(count($onehop_location_uid) <= 0)
	  {
	      $onehop_location_uid[] = '';
	  }
	  $this->DB2->select('macid')->from('wifi_location_access_point');
	  $this->DB2->where_in('location_uid', $onehop_location_uid);
	  $get_macid = $this->DB2->get();
	  foreach($get_macid->result() as $row_mac){
	       $onehop_macid[] = $row_mac->macid;
	  }
	  if(count($live_location_nasip) <= 0)
	  {
	      $live_location_nasip[] = '';
	  }
	  if(count($onehop_macid) <= 0)
	  {
	       $onehop_macid[] = '';
	  }
	  
	  $this->DB2->select('DATE(acctstarttime) AS viewed_on')->from('radacct');
	  $this->DB2->where(array('DATE(acctstarttime) >=' => $date_from, 'DATE(acctstarttime) <=' => $date_to));
	  $this->DB2->group_start();
	  $this->DB2->where_in('nasipaddress', $live_location_nasip);
	  $this->DB2->or_where_in('calledstationid', $onehop_macid);
	  
	  
	  $this->DB2->group_end();
	  $this->DB2->group_by('DATE(viewed_on)')->order_by('viewed_on');
	  $query_get_mg = $this->DB2->get();
	  $i = 0;
	  $total_mb = 0;
	  // time filter
	  
	  $user_detail = array();
	  $userids = array();
	  /*$total_mg_date_wise = $this->DB2->query("SELECT r.username,r.acctinputoctets as input , r.acctoutputoctets as output, date(r.acctstarttime) as sesstion_date
    FROM radacct as r  WHERE  DATE(acctstarttime) BETWEEN '$date_from' AND '$date_to' AND (r.nasipaddress IN
    (".$live_location_nasip.") OR r.calledstationid IN (".$onehop_macid."))  $time_filter ");*/
	  $this->DB2->select('r.username,r.acctinputoctets as input , r.acctoutputoctets as output, date(r.acctstarttime) as sesstion_date')->from('radacct as r');
	  $this->DB2->where(array('DATE(acctstarttime) >=' => $date_from, 'DATE(acctstarttime) <=' => $date_to));
	  $this->DB2->group_start();
	  $this->DB2->where_in('nasipaddress', $live_location_nasip);
	  $this->DB2->or_where_in('calledstationid', $onehop_macid);
	  $this->DB2->group_end();
	  if(count($jsondata->time_slot) > 0){
	       $this->DB2->group_start();
	       $t = 0;
	       foreach($jsondata->time_slot as $filt_time){
		    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
			 $this->DB2->or_where(array('TIME(acctstarttime) >=' => $time_from));
			 $this->DB2->where(array('TIME(acctstarttime) <=' => $time_to));
		    $t++;
	       }
	       $this->DB2->group_end();
	  }
	  $total_mg_date_wise = $this->DB2->get();
	  
	  
	  foreach($total_mg_date_wise->result() as $row_ids){
	       $userids[] = $row_ids->username;
	  }
	  if(count($userids) > 0)
	  {
	       $this->DB2->select('username,gender,age_group')->from('sht_users');
	       $this->DB2->where_in('username', $userids);
	       $get_user_detail = $this->DB2->get();
	       foreach($get_user_detail->result() as $row_detail){
		   $user_detail[$row_detail->username] = $row_detail;
	       }
	  }
	  
	  foreach($query_get_mg->result() as $row1){
	       $analysis_date = date_parse($row1->viewed_on);
	       $monthNum  = $analysis_date['month'];
	       $dateObj   = DateTime::createFromFormat('!m', $monthNum);
	       $monthName = $dateObj->format('F');
	       $data[$i]['viewed_on'] = $analysis_date['day'].' '.$monthName;
	       $total_mb_date = 0;
	       if($total_mg_date_wise->num_rows() > 0){
		    foreach($total_mg_date_wise->result() as $data_row){
			 if($row1->viewed_on == $data_row->sesstion_date){
			      $gender_where = $jsondata->gender;
			      $age_group_filter = $jsondata->age_group;
			      $gender = '';$age_group = '';
			      if(isset($user_detail[$data_row->username])){
				   $gender = strtolower(trim($user_detail[$data_row->username]->gender));
				   if($gender != ''){
					$gender =  $gender[0];
				   }
				   
				   $age_group = $user_detail[$data_row->username]->age_group;
			      }
			      $gender_verify = 1; $age_group_verify = 1;
			      if($gender_where != ''){
				   if($gender_where != $gender){
					$gender_verify = '0';
				   }
			      }
			      if(count($jsondata->age_group) > 0){
				   if(!in_array($age_group,$jsondata->age_group)){
					$gender_verify = '0';
				   }
			      }
			      if($gender_verify == '1' && $age_group_verify == '1'){
				   $total_mb_date = $total_mb_date + $data_row->input+$data_row->output;
			      }
			      
			 }
		    }
	       }
	       $total_mb_date = $total_mb_date/(1024*1024);
	       $data[$i]['total_mb'] = round(($total_mb_date/1024),2);
	       $total_mb = $total_mb+$total_mb_date;
	       $i++;
	  }
      $return_array["data"] = $data;
      $return_array["total_mb"] = $total_mb;
      //echo "<pre>";print_r($return_array);die;
      return $return_array;
 }


     // CI query changes (sone query pending)
     public function location_wise_total_data_used($jsondata){
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $where_condition = array();
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
	  if($state != ''){
	      $where_condition['wl.state_id'] = $state;
	  }
	  $city_where = '';
	  if($city != ''){
	      $where_condition['wl.city_id'] = $city;
	  }
	  $limit = '10';
	  if(isset($jsondata->limit)){
	       $limit = $jsondata->limit;
	  }
	  $offset = 0;
	  if(isset($jsondata->offset)){
	       $offset = $jsondata->offset;
	  }
	  $date_from = date('Y-m-d', strtotime("$jsondata->from"));
	  $date_to = date('Y-m-d', strtotime("$jsondata->to"));
	  $return_array = array();
	  $live_location_nasip = array();
	  $this->DB2->select('ns.nastype, wl.nasipaddress, wl.location_name,wl.location_uid')->from('wifi_location as wl');
	  $this->DB2->where(array('wl.isp_uid' => $jsondata->isp_uid, 'wl.status' => '1', 'wl.is_deleted' => '0'));
	  if(count($jsondata->locations) > 0){
	       $this->DB2->where_in('wl.location_uid',$jsondata->locations);
	  }
	  $this->DB2->where($where_condition);
	  $this->DB2->join('nas as ns', 'wl.nasid = ns.id', 'LEFT');
	  $this->DB2->limit($limit,$offset);
	  $query = $this->DB2->get();
	  $i = 0;
	  $user_detail = array();
	  $userids = array();
	  $this->DB2->select('r.username,r.acctinputoctets as input , r.acctoutputoctets as output, date(r.acctstarttime) as sesstion_date')->from('radacct as r');
	  $this->DB2->where(array('DATE(acctstarttime) >=' => $date_from, 'DATE(acctstarttime) <=' => $date_to));
	  $total_mg_date_wise = $this->DB2->get();
	  
	  
	  foreach($total_mg_date_wise->result() as $row_ids){
	       $userids[] = $row_ids->username;
	  }
	  if(count($userids) > 0)
	  {
	       $userids = array_unique($userids);
	       $this->DB2->select('username,gender,age_group')->from('sht_users');
	       $this->DB2->where_in('username', $userids);
	       $get_user_detail = $this->DB2->get();
	       foreach($get_user_detail->result() as $row_detail){
		   $user_detail[$row_detail->username] = $row_detail;
	       }
	  }
	  foreach($query->result() as $row){
	       $nas_where = '';
	       $nasip_address = '';
	       if($row->nastype == '6'){
		    $onehop_macid = array();
		    $location_uid = $row->location_uid;
		    $this->DB2->select('macid')->from('wifi_location_access_point');
		    $this->DB2->where('location_uid',$location_uid);
		    $get_macid = $this->DB2->get();
		    foreach($get_macid->result() as $row_mac){
			 $onehop_macid[] = $row_mac->macid;
		    }
	       if(count($onehop_macid) <= '0'){
		    $nas_where = $nas_where." r.calledstationid IN ('notFound') ";
	       }else{
		    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		    $nas_where = $nas_where." r.calledstationid IN ($onehop_macid) ";
	       }
	  }else{
	       $nasip_address  = $row->nasipaddress;
	       $nas_where = $nas_where." r.nasipaddress = '$nasip_address' ";
	  }
	  $time_filter = '';
	  if(count($jsondata->time_slot) > 0){
	       $time_or_query = '';
	       $t = 0;
	       foreach($jsondata->time_slot as $filt_time){
		   $time_sended = explode('-', $filt_time);
		   $time_from =  $time_sended[0];
		   $time_to =  $time_sended[1];
		   if($t == 0){
		       $time_or_query = " TIME(r.acctstarttime) BETWEEN '$time_from' AND '$time_to' ";
		   }
		   else{
		       $time_or_query = $time_or_query." OR TIME(r.acctstarttime) BETWEEN '$time_from' AND '$time_to' ";
		   }
		   $t++;
	       }
	       $time_filter = " AND ($time_or_query) ";
	  }
	  $total_mb = 0;
	  $return_array[$i]["nasipaddress"]=$row->nasipaddress;
	  $return_array[$i]["location_name"]=$row->location_name;
	  $total_mg_location_wise = $this->DB2->query("SELECT r.username,r.acctinputoctets as input , r.acctoutputoctets as output FROM radacct as r  WHERE $nas_where  AND  DATE(r.acctstarttime) BETWEEN '$date_from' AND '$date_to' $time_filter");
	  if($total_mg_location_wise->num_rows() > 0){
	       $gender_where = $jsondata->gender;
	       $age_group_filter = $jsondata->age_group;
	       foreach($total_mg_location_wise->result() as $row1){
		    $gender = '';$age_group = '';
		    if(isset($user_detail[$row1->username])){
			 $gender = strtolower(trim($user_detail[$row1->username]->gender));
			 if($gender != ''){
			      $gender =  $gender[0];
			 }
			 $age_group = $user_detail[$row1->username]->age_group;
		    }
		    $gender_verify = 1; $age_group_verify = 1;
		    if($gender_where != ''){
			 if($gender_where != $gender){
			      $gender_verify = '0';
			 }
		    }
		    if(count($jsondata->age_group) > 0){
			 if(!in_array($age_group,$jsondata->age_group)){
			      $gender_verify = '0';
			 }
		    }
		    if($gender_verify == '1' && $age_group_verify == '1'){
			 $total_mb = $total_mb+$row1->input+$row1->output;
		    }
	       }
	  }
	  $total_mb = $total_mb/(1024*1024);
	  if($total_mb > 1024){
	       $total_mb = round(($total_mb/1024),2)."GB";
	  }else{
	       $total_mb = round($total_mb,2)."MB";
	  }
	  $return_array[$i]["total_mb"]=$total_mb;
	  $i++;
     }
     //echo "<pre>";print_r($return_array);die;
     return $return_array;
}



     // CI query changes (some query pending)
     public function traffic_summary_total_user($jsondata){
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $where_array = array();
	  $state_where = '';
	    if($state != ''){
		$where_array['state_id'] = $state;
	    }
	    $city_where = '';
	    if($city != ''){
	       $where_array['city_id'] = $city;
	    }
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $location_filter_query = '';;
	  $total_unique_user_array=array();
	  $total_new_user=0;
	  $total_returning_user=array();
	  $live_location_id = array();
	  $this->DB2->select('id, location_name')->from('wifi_location');
	  $this->DB2->where(array('isp_uid'=>$jsondata->isp_uid, 'status' => '1', 'is_deleted' => '0'));
	  $this->DB2->where($where_array);
	  if(count($jsondata->locations) > 0){
	       $this->DB2->where_in('location_uid', $jsondata->locations);
	  }
	  $query = $this->DB2->get();
	  foreach($query->result() as $row){
	     $live_location_id[] = $row->id; 
	  }
	  $live_location_id='"'.implode('", "', $live_location_id).'"';
	  $previous_user = array();
	  $this->DB2->select('userid')->from('wifi_user_free_session');
	  $this->DB2->where('DATE(session_date) <', $from);
	  $this->DB2->group_by('userid');
	  $previous_session = $this->DB2->get();
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  
      //condition
      $cond = "and date(wufs.session_date) between '$from' and '$to'";
      // time filter
      $time_filter = '';
      if(count($jsondata->time_slot) > 0){
	   $time_or_query = '';
	   $t = 0;
	   foreach($jsondata->time_slot as $filt_time){
		$time_sended = explode('-', $filt_time);
		$time_from =  $time_sended[0];
		$time_to =  $time_sended[1];
		if($t == 0){
		     $time_or_query = " TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
		}
		else{
		     $time_or_query = $time_or_query." OR TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
		}
		$t++;
	   }
	   $time_filter = " AND ($time_or_query) ";
      }
      //gender filter
      $gender_where = '';
      if($jsondata->gender == 'm'){
	   $gender_where = " AND (wufs.gender = 'm' or wufs.gender = 'M' or wufs.gender = 'male' or wufs.gender = 'Male')";
      }elseif($jsondata->gender == 'f'){
	   $gender_where = " AND (wufs.gender = 'f' or wufs.gender = 'F' or wufs.gender = 'female' or wufs.gender = 'Female')";
      }
      // age group filter
      $age_group_filter = '';
      if(count($jsondata->age_group) > 0){
	   $age_group_or_query = '';
	   $t = 0;
	   foreach($jsondata->age_group as $age_group_value){
		       if($t == 0){
			   $age_group_or_query = " wufs.age_group = '$age_group_value' ";
		       }
		       else{
			   $age_group_or_query = $age_group_or_query." OR wufs.age_group = '$age_group_value' ";
		       }
		       $t++;
	   }
	   $age_group_filter = " AND ($age_group_or_query) ";
      }
      $chech_user = $this->DB2->query("select wufs.userid, date(session_date) as date_field  from wifi_user_free_session as wufs  WHERE wufs.location_id IN (".$live_location_id.")  $cond
    $time_filter $gender_where $age_group_filter  order by wufs.session_date");
     
      $date_wise = array();
      foreach($chech_user->result() as $chech_user_row){
	   $date_wise[$chech_user_row->date_field][]= $chech_user_row;
      }
       
      $fnlarr=array();
      $dataarr=array();
      $daywisearr=array();
     // echo "<pre>";print_r($date_wise_arr);die;
      $flipped_previous_user = array_flip($previous_user);
      foreach($date_wise as $keydate => $date_wise_row){
	   $date_wise_arr=array();
	   $repeatuser=0;
	   $newuser=0;
	   foreach($date_wise_row as $valnew){
		//if(!in_array($valnew->userid,$date_wise_arr)){
		if(!isSet($date_wise_arr[$valnew->userid])){
		     if(isSet($flipped_previous_user[$valnew->userid])){
			  $repeatuser=$repeatuser+1;
			  $total_returning_user[] = $valnew->userid;
		     }
		     else{
			 $newuser=$newuser+1;
			 $total_new_user = $total_new_user+1;
		     }
		     $total_unique_user_array[] = $valnew->userid;
		     //$date_wise_arr[]=$valnew->userid;
		     $date_wise_arr[$valnew->userid]=$valnew->userid;
		     //$previous_user[]=$valnew->userid;
		     $flipped_previous_user[$valnew->userid] = $valnew->userid;
		}
		$fnlarr[$keydate]['newuser']= $newuser;
		$fnlarr[$keydate]['repeatuser']= $repeatuser;
	   }
	  
      }
      
      $x=0;
      foreach($fnlarr as $key=>$valdate)
      {
	   $dataarr['data'][$x]['viewed_on']=date("d M",strtotime($key));
	   $dataarr['data'][$x]['newuser']=$valdate['newuser'];
	   $dataarr['data'][$x]['returninguser']=$valdate['repeatuser'];
	   $x++;
	   
      }
      $dataarr["total_newuser"] = $total_new_user;
      $dataarr["total_returninguser"] = count(array_unique($total_returning_user));
      $dataarr["total_user"] = count(array_unique($total_unique_user_array));
      //echo "<pre>";print_r($dataarr);die;
      
      return $dataarr;
    
 }


     // CI query changes(some query pending)
     public function traffic_summary_location_wise($jsondata){
	  $limit = '10';
	  if(isset($jsondata->limit)){
	       $limit = $jsondata->limit;
	  }
	  $offset = 0;
	  if(isset($jsondata->offset)){
	       $offset = $jsondata->offset;
	  }
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $previous_user = array();
	  $this->DB2->select('userid')->from('wifi_user_free_session');
	  $this->DB2->where('DATE(session_date) <', $from);
	  $previous_session = $this->DB2->get();
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  $return_array = array();
	  $re_count_id = 0;
	  $location_filter_query = '';
	  if(count($jsondata->locations) > 0){
	      $locations = '';
	      $locations='"'.implode('", "', $jsondata->locations).'"';
	      $location_filter_query = " AND location_uid IN($locations) ";
	  }
	  $live_location_id = array();
	  
	  $query = $this->DB2->query("select id, location_name,nasipaddress from wifi_location where isp_uid = '$jsondata->isp_uid' and status = '1' and is_deleted = '0' $location_filter_query $state_where $city_where limit $offset,$limit");
	  foreach($query->result() as $row){
	       $previous_user_id_collection = $previous_user;
	       $location_id = $row->id;
	       $return_array[$re_count_id]["nasipaddress"]=$row->nasipaddress;
	       $return_array[$re_count_id]["location_name"]=$row->location_name;
	       $totalunique = 0;
	       $totalrepeat = 0;
	       $totalrepeat_array = array();
	       
	       $totaluser = 0;
	       
	       $cond="";
	       $cond = "and date(wufs.session_date) between '$from' and '$to'";
	       // time filter
	       $time_filter = '';
	       if(count($jsondata->time_slot) > 0){
		    $time_or_query = '';
		    $t = 0;
		    foreach($jsondata->time_slot as $filt_time){
			 $time_sended = explode('-', $filt_time);
			 $time_from =  $time_sended[0];
			 $time_to =  $time_sended[1];
			 if($t == 0){
			      $time_or_query = " TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
			 }
			 else{
			      $time_or_query = $time_or_query." OR TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
			 }
			 $t++;
		    }
		    $time_filter = " AND ($time_or_query) ";
	       }
	       //gender filter
	       $gender_where = '';
	       if($jsondata->gender == 'm'){
		    $gender_where = " AND (wufs.gender = 'm' or wufs.gender = 'M' or wufs.gender = 'male' or wufs.gender = 'Male')";
	       }elseif($jsondata->gender == 'f'){
		    $gender_where = " AND (wufs.gender = 'f' or wufs.gender = 'F' or wufs.gender = 'female' or wufs.gender = 'Female')";
	       }
	       // age group filter
	       $age_group_filter = '';
	       if(count($jsondata->age_group) > 0){
		    $age_group_or_query = '';
		    $t = 0;
		    foreach($jsondata->age_group as $age_group_value){
				if($t == 0){
				    $age_group_or_query = " wufs.age_group = '$age_group_value' ";
				}
				else{
				    $age_group_or_query = $age_group_or_query." OR wufs.age_group = '$age_group_value' ";
				}
				$t++;
		    }
		    $age_group_filter = " AND ($age_group_or_query) ";
	       }
	       $chech_user = $this->DB2->query("select * from (select wufs.*  from wifi_user_free_session as wufs  WHERE wufs.location_id = '$location_id'  $cond
        $time_filter $gender_where $age_group_filter  order by wufs.session_date) as temp group by userid, date(session_date)");
	       
	       $location_user_already_checked = array();
	       foreach($chech_user->result() as $chech_user_row){
		    //if(!in_array($chech_user_row->userid,$location_user_already_checked)){
			 if(in_array($chech_user_row->userid,$previous_user_id_collection))
			 {
			   $totalrepeat=$totalrepeat+1;
			   $totalrepeat_array[] = $chech_user_row->userid;
			 }
			 else
			 {
			     $totalunique=$totalunique+1;
			 }
			 if(!in_array($chech_user_row->userid,$location_user_already_checked)){
			      $totaluser = $totaluser+1;
			 }
			 $location_user_already_checked[]=$chech_user_row->userid;
			 $previous_user_id_collection[]=$chech_user_row->userid;
		   // }else{
			 //$totalrepeat_array[] = $chech_user_row->userid;
		    //}
		    
	       }
	       
	       $return_array[$re_count_id]["totalunique"]=$totalunique;
	       $return_array[$re_count_id]["totalrepeat"]=count(array_unique($totalrepeat_array));
	       $return_array[$re_count_id]["totaluser"]=$totaluser;
	       $re_count_id++;
	       
	  }
	  //die;
	 // echo "<pre>";print_r($return_array);die;
	  return $return_array;
     }



    
     
     public function legal_invention_total_user($jsondata){
	  $mobile_where = '';
	  $nas_where = '';
	  if($jsondata->mobile != ''){
	      $mobile_where = "  su.mobile = '$jsondata->mobile' ";
	  }
	 if($jsondata->location_nasip != ''){
	   $location_uid = $jsondata->location_nasip;// now we pass location id in this variable
	   $get_type = $this->DB2->query("select wl.nasipaddress, ns.nastype from wifi_location as wl left join nas as ns on(wl.nasid = ns.id) where wl.location_uid = '$location_uid'");
	   if($get_type->num_rows() > 0){
		$row_get = $get_type->row_array();
		if($row_get['nastype'] == '6'){
		     $onehop_macid = array();
		     //get macids
		     $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid = '$location_uid'");
		     foreach($get_macid->result() as $row_mac){
			  $onehop_macid[] = $row_mac->macid;
		     }
		     $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		     if($jsondata->mobile != ''){
			  $nas_where = "  ra.calledstationid IN ($onehop_macid) AND ";
		     }else{
			  $nas_where = "  ra.calledstationid IN ($onehop_macid) ";
		     }
		}else{
		     $nasip = $row_get['nasipaddress'];
		     if($jsondata->mobile != ''){
			  $nas_where = "  ra.nasipaddress = '$nasip' AND ";
		     }else{
			  $nas_where = "  ra.nasipaddress = '$nasip' ";
		     }
		}
	   }
	   
	 }
	 if($nas_where == '' && $mobile_where ==''){
		$nas_where = "  ra.nasipaddress = '' ";
	 }
	 $from = date('Y-m-d H:i:s', strtotime("$jsondata->selecteddate"));
	 $to = date('Y-m-d H:i:s', strtotime("$jsondata->selecteddateto"));
	 $query = $this->DB2->query("select ra.radacctid
 from radacct as ra INNER JOIN sht_users as su on (ra.username = su.uid) WHERE  $nas_where $mobile_where AND (ra.acctstarttime BETWEEN '$from' AND '$to' OR
 ra.acctstoptime BETWEEN '$from' AND '$to') ORDER BY ra.radacctid DESC");
	 return $query->num_rows();
     }
    public function legal_invention_get_user($jsondata){
        $mobile_where = '';
	$nas_where = '';
        if($jsondata->mobile != ''){
            $mobile_where = "  su.mobile = '$jsondata->mobile' ";
        }
	if($jsondata->location_nasip != ''){
	  $location_uid = $jsondata->location_nasip;// now we pass location id in this variable
	  $get_type = $this->DB2->query("select wl.nasipaddress, ns.nastype from wifi_location as wl left join nas as ns on(wl.nasid = ns.id) where wl.location_uid = '$location_uid'");
	  if($get_type->num_rows() > 0){
	       $row_get = $get_type->row_array();
	       if($row_get['nastype'] == '6'){
		    $onehop_macid = array();
		    //get macids
		    $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid = '$location_uid'");
		    foreach($get_macid->result() as $row_mac){
			 $onehop_macid[] = $row_mac->macid;
		    }
		    $onehop_macid='"'.implode('", "', $onehop_macid).'"';
		    if($jsondata->mobile != ''){
			 $nas_where = "  ra.calledstationid IN ($onehop_macid) AND ";
		    }else{
			 $nas_where = "  ra.calledstationid IN ($onehop_macid) ";
		    }
	       }else{
		    $nasip = $row_get['nasipaddress'];
		    if($jsondata->mobile != ''){
			 $nas_where = "  ra.nasipaddress = '$nasip' AND ";
		    }else{
			 $nas_where = "  ra.nasipaddress = '$nasip' ";
		    }
	       }
	  }
	  
	}
	if($nas_where == '' && $mobile_where ==''){
	       $nas_where = "  ra.nasipaddress = '' ";
	}
        $from = date('Y-m-d H:i:s', strtotime("$jsondata->selecteddate"));
        $to = date('Y-m-d H:i:s', strtotime("$jsondata->selecteddateto"));
	$data=array();
	if($nas_where != '' || $mobile_where != ''){
	   $query = $this->DB2->query("select su.firstname, su.lastname,su.email,su.mobile,ra.acctinputoctets,ra.acctoutputoctets,ra.radacctid,ra.username, ra.nasipaddress,
ra.acctstarttime, ra.acctstoptime,ra.callingstationid, ra.nasportid,ra.nasporttype,ra.framedipaddress from radacct as ra
INNER JOIN sht_users as su ON (ra.username = su.uid) WHERE  $nas_where $mobile_where AND
(ra.acctstarttime BETWEEN '$from' AND '$to' OR ra.acctstoptime BETWEEN '$from' AND '$to') ORDER BY ra.radacctid DESC LIMIT $jsondata->start,
$jsondata->limit");
        
	  if ($query->num_rows() > 0) {
	      foreach ($query->result() as $row) {
		  //$row->user_email  = 'aaa';
		  $data[] = $row;
  
	      }
	  }
	}
       
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
     // CI query changes
     public function location_voucher_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_currency = $this->countrydetails($isp_uid);
	  $currency_sumbol = '';
	  if(isset($location_currency['currency'])){
	    $currency_sumbol = $location_currency['currency'];
	  }
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $vouchers = array();
	  $this->DB2->select('*')->from('wifi_location_voucher')->where('location_uid', $location_uid);
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       foreach($query->result() as $row){
		   $vouchers[$i]['id'] = $row->id;
		   $vouchers[$i]['voucher_data'] = $row->voucher_data;
		   $vouchers[$i]['voucher_data_type'] = $row->voucher_data_type;
		   $vouchers[$i]['voucher_cost'] = $row->voucher_cost;
		   $vouchers[$i]['voucher_selling_price'] = $row->voucher_selling_price;
		   $vouchers[$i]['currency_sumbol'] = $currency_sumbol;
		   $i++;
	       }
	       $data['vouchers']= $vouchers;
	  }
	  else
	  {
            $data['resultCode'] = '0';
	  }
	  return $data;
     }
     // CI query changes
     public function add_location_vouchers($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $voucher_id = $jsondata->voucher_id;
	  $voucher_data = $jsondata->voucher_data;
	  $voucher_data_type = $jsondata->voucher_data_type;
	  $voucher_cost = $jsondata->voucher_cost;
	  
	  $voucher_selling_price = $jsondata->voucher_selling_price;
	  $voucher_name = $voucher_data." ".$voucher_data_type;
	  $voucher_mb = 0;
	  if($voucher_data_type == 'GB' || $voucher_data_type == 'gb'){
	    $voucher_mb = $voucher_data*1024;
	  }else{
	    $voucher_mb = $voucher_data;
	  }
	  // check already 4 voucher created or not
	  $this->DB2->select('id')->from('wifi_location_voucher')->where('location_uid', $location_uid);
	  $check_voucher = $this->DB2->get();
	  if($check_voucher->num_rows() < 4)
	  {
	       
	       $data = array();
	       
	       if($voucher_id == ''){
		    $insert_data = array(
			 'location_uid' => $location_uid,
			 'voucher_data' => $voucher_data,
			 'voucher_data_type' => $voucher_data_type,
			 'voucher_cost' => $voucher_cost,
			 'created_on' => date('Y-m-d H:i:s'),
			 'voucher_name' => $voucher_name,
			 'voucher_mb' => $voucher_mb,
			 'voucher_selling_price' => $voucher_selling_price
		    );
		    $this->DB2->insert('wifi_location_voucher', $insert_data);
	       }
	       else{
		    $update_data = array(
			 'voucher_data' => $voucher_data,
			 'voucher_data_type' => $voucher_data_type,
			 'voucher_cost' => $voucher_cost,
			 'updated_on' => date('Y-m-d H:i:s'),
			 'voucher_name' => $voucher_name,
			 'voucher_mb' => $voucher_mb,
			 'voucher_selling_price' => $voucher_selling_price
		    );
		    $this->DB2->where('id', $voucher_id);
		    $query = $this->DB2->update('wifi_location_voucher', $update_data);
	       }
	        $data['resultCode'] = '1';
	  }
	  else{
	       if($voucher_id != ''){
		    $update_data = array(
			 'voucher_data' => $voucher_data,
			 'voucher_data_type' => $voucher_data_type,
			 'voucher_cost' => $voucher_cost,
			 'updated_on' => date('Y-m-d H:i:s'),
			 'voucher_name' => $voucher_name,
			 'voucher_mb' => $voucher_mb,
			 'voucher_selling_price' => $voucher_selling_price
		    );
		    $this->DB2->where('id', $voucher_id);
		    $query = $this->DB2->update('wifi_location_voucher', $update_data);
	       }
	        $data['resultCode'] = '2';
	  }
	  return $data;
     }


     // CI query changes
     public function delete_voucher($jsondata){
	  $voucher_id = $jsondata->voucher_id;
	  $data = array();
	  $query = $this->DB2->delete('wifi_location_voucher', array('id' => $voucher_id)); ;
	  $data['resultCode'] = '1';
	     
	  return $data;
     }
     // CI query changes
     public function update_location_voucher_status($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $voucher_need = $jsondata->voucher_need;
	  $data = array();
	  $this->DB2->set('is_voucher_used', $voucher_need);
	  $this->DB2->where('location_uid', $location_uid);
	  $this->DB2->update('wifi_location');
	  $payment_gateway = $jsondata->payment_gateway;
	  $this->DB2->set('is_pgdisabled', $payment_gateway);
	  $this->DB2->where('uid_location', $location_uid);
	  $this->DB2->update('wifi_location_cptype');
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     // CI query changes
     public function update_microtic_router_with_nas_location($jsondata){
	  $router_type = '1';
	  $location_id = $jsondata->location_id;
	  $nasaddress = $jsondata->nasaddress;
	  $nasid = $jsondata->nasid;
	  $isp_uid = $jsondata->isp_uid;
	  // balance deduct at first time mikrotic setup
	  $this->DB2->select('is_mikrotik_setup_paid')->from('wifi_location')->where('id',$location_id);
	  $get_balance_paid = $this->DB2->get();
	  if($get_balance_paid->num_rows() > 0){
	       $balance_row = $get_balance_paid->row_array();
	       $is_paid = $balance_row['is_mikrotik_setup_paid'];
	       if($is_paid == '' || $is_paid == '0'){
		    $location_price = $this->countrydetails($isp_uid);
		    $location_cost_deduct = '';
		    if(isset($location_price['cost_per_location'])){
			 $location_cost_deduct = $location_price['cost_per_location'];
			 $data_passbook = array(
			      'isp_uid' => $isp_uid,
			      'total_active_ap' => '1',
			      'cost' => $location_cost_deduct,
			      'added_on' => date('Y-m-d H:i:s')
			 );
			 $this->db->insert('sht_isp_passbook', $data_passbook);
		    }
		    $this->DB2->set('is_mikrotik_setup_paid', '1');
		    $this->DB2->where('id', $location_id);
		    $this->DB2->update('wifi_location');
	       }
	  }
        // update location table
		
	       $update_data = array(
			 'nasipaddress' => $nasaddress,
			 'nasid' => $nasid,
			 'nasconfigure' => '1',
			 'modified_on' => date('Y-m-d H:i:s'),
		 );
		 $this->DB2->where('id', $location_id);
		 $this->DB2->update('wifi_location', $update_data);
                // update nas table
	       $this->DB2->set('is_used', '1')->where('id', $nasid);
	       $this->DB2->update('nas');
		
		// for logs
		$get = $this->DB2->query("select * from wifi_location_nas_used_logs where location_id = '$location_id' AND nasid = '$nasid' AND nasaddress = '$nasaddress'");
		if($get->num_rows() <= '0'){
		   
		    $insert_data = array(
				   'location_id' => $location_id,
				   'nasid' => $nasid,
				   'nasaddress' => $nasaddress,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
			      $insert_option = $this->DB2->insert('wifi_location_nas_used_logs', $insert_data);
		}
		// associate plan with nas
		// get plan id of location
                $cp_type = '';
		$get_plan_id = $this->DB2->query("select custom_planid,cp_type, cp1_plan, cp2_plan, cp3_hybrid_plan, cp3_data_plan, cafe_plan from wifi_location_cptype where location_id = '$location_id'");
		
		if($get_plan_id->num_rows() > 0){
			$row = $get_plan_id->row_array();
			$cp_type = $row['cp_type'];
			$cp1_plan = $row['cp1_plan'];
			$cp2_plan = $row['cp2_plan'];
			$cp3_hybrid_plan = $row['cp3_hybrid_plan'];
			$cp3_data_plan = $row['cp3_data_plan'];
			if($cp_type == 'cp1' || $cp_type == 'cp_truck_stop' || $cp_type == 'cp4'){
				// check already associated
				$check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp1_plan' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					 $insert_data = array(
				   'srvid' => $cp1_plan,
				   'nasid' => $nasid,
			      );
			      $insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				}
				
			}elseif($cp_type == 'cp2'){
				// check already associated
				$check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp2_plan' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					$insert_data = array(
				   'srvid' => $cp2_plan,
				   'nasid' => $nasid,
			      );
			      $insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				}
				
			}elseif($cp_type == 'cp3'){
				$check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp3_hybrid_plan' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					$insert_data = array(
					     'srvid' => $cp3_hybrid_plan,
					     'nasid' => $nasid,
					);
					$insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				   }
					$check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp3_data_plan' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					$insert_data = array(
					     'srvid' => $cp3_data_plan,
					     'nasid' => $nasid,
					);
					$insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				}
			}elseif($cp_type == 'cp_cafe' || $cp_type == 'cp_retail' || $cp_type == 'cp_purple'){
				   $cp_plan = $row['cafe_plan'];
				// check already associated
				$check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp_plan' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					$insert_data = array(
					     'srvid' => $cp_plan,
					     'nasid' => $nasid,
					);
					$insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				}
				
			}elseif($cp_type == 'cp_custom'){
				   $cp_plan = $row['custom_planid'];
				// check already associated
				$check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp_plan' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					$insert_data = array(
					     'srvid' => $cp_plan,
					     'nasid' => $nasid,
					);
					$insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				}
				
			}elseif($cp_type == 'cp_hotel' || $cp_type == 'cp_hospital' || $cp_type == 'cp_institutional' || $cp_type == 'cp_enterprise'|| $cp_type == 'cp_club'){
			      //get cp hotel plans
			      $get = $this->DB2->query("select plan_id from ht_planassociation where location_id = '$location_id'");
			      foreach($get->result() as $row_plan){
				   $plan_id = $row_plan->plan_id;
				   $select = $this->DB2->query("select * from sht_plannasassoc where srvid = '$plan_id' and nasid = '$nasid'");
				   if($select->num_rows() <=0){
					
					$insert_data = array(
					     'srvid' => $plan_id,
					     'nasid' => $nasid,
					);
					$insert_option = $this->DB2->insert('sht_plannasassoc', $insert_data);
				   }
				   
				   
				   /*$insert_plan = $this->DB2->query("INSERT INTO sht_plannasassoc (srvid, nasid)
				SELECT * FROM (SELECT '$plan_id', '$nasid') AS tmp
				WHERE NOT EXISTS (
				    SELECT srvid FROM sht_plannasassoc WHERE srvid = '$plan_id' and nasid = '$nasid'
				) LIMIT 1;");*/
			      }
			}
			 if($cp_type != 'cp_foodle' && $cp_type != 'cp_custom' && $cp_type != 'cp_offline' && $cp_type != 'cp_visitor' && $cp_type != 'cp_contestifi' && $cp_type != 'cp_retail_audit' && $cp_type != 'cp_event_module'){
			      if(IS_INDIA_COUNTRY == '1'){
				   $this->createuserpanelfolder($cp_type,$isp_uid,$location_id,$router_type);
			      }
			 }
		}
                
                $data = array();
        $data['resultCode'] = '1';
           
        return $data;
    }



     // CI query changes
     public function check_cp_selected_for_location($location_uid){
	 $data = array();
	 $this->DB2->select('id')->from('wifi_location_cptype')->where('location_id', $location_uid);
	 $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	      $data['resultCode'] = '1';
	  }else{
	      $data['resultCode'] = '0';
	  }
	 return $data;
     }
     // CI query changes
     public function update_other_router_with_nas_location($jsondata){
        $location_id = $jsondata->location_uid;
        $nasid = $jsondata->nasid;
        $isp_uid = $jsondata->isp_uid;
        $router_type = $jsondata->router_type;
        // first check cp selected for this nas
        $check_cp_selected = $this->check_cp_selected_for_location($location_id);
	
	  $data = array();
	  if($check_cp_selected['resultCode'] == 1){
           
	       $this->DB2->select('nasname,shortname')->from('nas');
	       $this->DB2->where('id', $nasid);
	       $get_nasname = $this->DB2->get();
		$username = '';
		$nasaddress = '';
		if($get_nasname->num_rows() > 0){
			$row = $get_nasname->row_array();
			$username = $row['shortname'];
			$nasaddress = $row['nasname'];
		}
        
        // update location table
	       $data_loc_up = array(
		    'nasipaddress' => $nasaddress,
		    'nasid' => $nasid,
		    'nasconfigure' => '1',
		    'modified_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $location_id);
	       $this->DB2->update('wifi_location', $data_loc_up);
		// update nas table
	       $this->DB2->set('is_used', '1')->where('id', $nasid);
	       $this->DB2->update('nas');
		// for log
	       $this->DB2->select('*')->from('wifi_location_nas_used_logs');
	       $this->DB2->where(array('location_id'=>$location_id, 'nasid' => $nasid, 'nasaddress' => $nasaddress));
	       $get = $this->DB2->get();
		if($get->num_rows() <= '0'){
		    $data_nas_used = array(
			      'location_id' => $location_id,
			      'nasid' => $nasid,
			      'nasaddress' => $nasaddress,
			      'created_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->insert('wifi_location_nas_used_logs', $data_nas_used);
		}
		// associate plan with nas
		// get plan id of location
                $cp_type = '';
	       $this->DB2->select('custom_planid,cp_type, cp1_plan, cp2_plan, cp3_hybrid_plan, cp3_data_plan, cafe_plan')->from('wifi_location_cptype')->where('location_id',$location_id );
	       $get_plan_id = $this->DB2->get();
		if($get_plan_id->num_rows() > 0){
                   	$row = $get_plan_id->row_array();
			$cp_type = $row['cp_type'];
			$cp1_plan = $row['cp1_plan'];
			$cp2_plan = $row['cp2_plan'];
			$cp3_hybrid_plan = $row['cp3_hybrid_plan'];
			$cp3_data_plan = $row['cp3_data_plan'];
			if($cp_type == 'cp1' || $cp_type == 'cp_truck_stop' || $cp_type == 'cp4'){
				// check already associated
				
			      $this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $cp1_plan, 'nasid'=> $nasid));
			      $check = $this->DB2->get();
			      if($check->num_rows() <= '0'){
				      $data_plan = array(
					'srvid' => $cp1_plan,
					'nasid' => $nasid,
				   );
				   $this->DB2->insert('sht_plannasassoc', $data_plan);
			      }
				
			}elseif($cp_type == 'cp2'){
				// check already associated
				$this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $cp2_plan, 'nasid'=> $nasid));
			      $check = $this->DB2->get();
				if($check->num_rows() <= '0'){
				   $data_plan = array(
					'srvid' => $cp2_plan,
					'nasid' => $nasid,
				   );
				   $this->DB2->insert('sht_plannasassoc', $data_plan);
				}
				
			}elseif($cp_type == 'cp3'){
				$this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $cp3_hybrid_plan, 'nasid'=> $nasid));
			      $check = $this->DB2->get();
				if($check->num_rows() <= '0'){
				    $data_plan = array(
					'srvid' => $cp3_hybrid_plan,
					'nasid' => $nasid,
				   );
				   $this->DB2->insert('sht_plannasassoc', $data_plan);
			      }
					
					$this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $cp3_data_plan, 'nasid'=> $nasid));
			      $check = $this->DB2->get();
				if($check->num_rows() <= '0'){
					
					$data_plan = array(
					'srvid' => $cp3_data_plan,
					'nasid' => $nasid,
				   );
				   $this->DB2->insert('sht_plannasassoc', $data_plan);
				}
			}elseif($cp_type == 'cp_cafe' || $cp_type == 'cp_retail' || $cp_type == 'cp_purple'){
				   $cp_plan = $row['cafe_plan'];
				// check already associated
				
				$this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $cp_plan, 'nasid'=> $nasid));
			      $check = $this->DB2->get();
				if($check->num_rows() <= '0'){
				
				   $data_plan = array(
					'srvid' => $cp_plan,
					'nasid' => $nasid,
				   );
				   $this->DB2->insert('sht_plannasassoc', $data_plan);
				}
				
			}elseif($cp_type == 'cp_custom'){
				   $cp_plan = $row['custom_planid'];
				// check already associated
				
				$this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $cp_plan, 'nasid'=> $nasid));
			      $check = $this->DB2->get();
				if($check->num_rows() <= '0'){
					$data_plan = array(
					'srvid' => $cp_plan,
					'nasid' => $nasid,
				   );
				   $this->DB2->insert('sht_plannasassoc', $data_plan);
				}
				
			}elseif($cp_type == 'cp_hotel' || $cp_type == 'cp_hospital' || $cp_type == 'cp_institutional' || $cp_type == 'cp_enterprise' || $cp_type == 'cp_club'){
			      //get cp hotel plans
			      
			      $this->DB2->select('plan_id')->from('ht_planassociation')->where('location_id', $location_id);
			      $get = $this->DB2->get();
			      foreach($get->result() as $row_plan){
				    $plan_id = $row_plan->plan_id;
				  
				   $this->DB2->select('*')->from('sht_plannasassoc')->where(array('srvid' => $plan_id, 'nasid'=> $nasid));
			      $select = $this->DB2->get();
				   
				   if($select->num_rows() <=0){
				   $data_plan = array(
					'srvid' => $plan_id,
					'nasid' => $nasid,
				   );
				   $insert_plan = $this->DB2->insert('sht_plannasassoc', $data_plan);
				   }
			      }
			}
			//if($cp_type != 'cp_hotel'){
			//echo $cp_type."--".$isp_uid."--".$location_id.'--'.$router_type;die;
			 if($cp_type != 'cp_foodle' && $cp_type != 'cp_custom' && $cp_type != 'cp_offline' && $cp_type != 'cp_visitor' && $cp_type != 'cp_contestifi' && $cp_type != 'cp_retail_audit' && $cp_type != 'cp_event_module'){
			      if(IS_INDIA_COUNTRY == '1'){
				   $this->createuserpanelfolder($cp_type,$isp_uid,$location_id,$router_type);
			      }
			 }
			      
			//}
			
		}
                
                 $data['resultCode'] = '1';
        }else{
             $data['resultCode'] = '0';
        }
        
                
       
        return $data;
    }


     // check isp subdomain created or not
     // CI query changes
     public function getisp_accounttype($isp_uid){
	  $this->db->select('decibel_account')->from('sht_isp_admin');
	  $this->db->where(array('isp_uid' => $isp_uid, 'is_activated' => '1'));
	  $query = $this->db->get();
	 if($query->num_rows() > 0){
	     $rowdata = $query->row();
	     return $rowdata->decibel_account;
	 }
     }
     // CI query changes
     public function createuserpanelfolder($cptype,$isp_uid,$location_id,$router_type = 1){
     
	  $this->db->select('portal_url,isp_domain_name,domain_exists,isp_domain_protocol')->from('sht_isp_admin');
	  $this->db->where('isp_uid', $isp_uid);
	  $query = $this->db->get();
	  
	  if($query->num_rows() > 0){
	       $row = $query->row_array();
	       $portal_url = $row['portal_url'];
	       $portal_url = strtolower($portal_url);
	       $check_subdomain_created = $this->getisp_accounttype($isp_uid);
	       $isp_domain_name = $row['isp_domain_name'];
	       $isp_domain_protocol = $row['isp_domain_protocol'];
	       $domain_exists = $row['domain_exists'];
	       if($domain_exists == '1'){
		   if($router_type == '5'){//cambium
			     $ispurl = $isp_domain_protocol.$isp_domain_name.'/cambium/';//for live  
		       }else if($router_type == '6'){//onehop
			    $ispurl = $isp_domain_protocol.$isp_domain_name.'/onehop/';//for live  
		       }else{
			     $ispurl = $isp_domain_protocol.$isp_domain_name.'/mikrotik_kt/';//for live
		       }
	       }
	       else{
		    if(strtolower($check_subdomain_created) == 'paid'){
			 /*if($router_type == '5'){//cambium
			       $ispurl = 'https://'.$portal_url.'.shouut.com/cambium/';//for live  
			 }else if($router_type == '6'){//onehop
			      $ispurl = 'https://'.$portal_url.'.shouut.com/onehop/';//for live  
			 }else{
			       $ispurl = 'https://'.$portal_url.'.shouut.com/mikrotik_kt/';//for live
			 }*/
			 
			 if($router_type == '5'){//cambium
			       $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/cambium/';//for live
			 }else if($router_type == '6'){//onehop
			       $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/onehop/';//for live
			 }else{
			       $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/mikrotik_kt/';//for live
			 }
			 
		      }else{
			 if($router_type == '5'){//cambium
			       $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/cambium/';//for live
			 }else if($router_type == '6'){//onehop
			       $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/onehop/';//for live
			 }else{
			       $ispurl = 'https://www.shouut.com/ispadmins/'.$portal_url.'.shouut.com/mikrotik_kt/';//for live
			 }
			 
		      }
	       }
	    
	    
	    
            $hotspot_folder_type = '';
            if($cptype == 'cp1'){
                $hotspot_folder_type = 'hotspot';
            }elseif($cptype == 'cp_truck_stop'){
                $hotspot_folder_type = 'hotspot_truck';
            }elseif($cptype == 'cp2'){
                $hotspot_folder_type = 'hotspot_v2';
            }
            elseif($cptype == 'cp3'){
		$hotspot_folder_type = 'hotspot_v3';
            }elseif($cptype == 'cp4'){
		//$hotspot_folder_type = 'hotspot_v4';
		$hotspot_folder_type = 'hotspot';
            }elseif($cptype == 'cp_hotel'){
                    $hotspot_folder_type = 'hotspot_htv2';
                
            }elseif($cptype == 'cp_hospital'){
                    $hotspot_folder_type = 'hotspot_hsptlv2';
                
            }elseif($cptype == 'cp_institutional'){
                    $hotspot_folder_type = 'hotspot_institutev2';
                
            }elseif($cptype == 'cp_enterprise'){
                    $hotspot_folder_type = 'hotspot_enterprisev2';
                
            }elseif($cptype == 'cp_club'){
                    $hotspot_folder_type = 'hotspot_club';
                
            }elseif($cptype == 'cp_cafe'){
                    $hotspot_folder_type = 'hotspot_cafe';
                
            }elseif($cptype == 'cp_retail'){
                    $hotspot_folder_type = 'hotspot_retail';
                
            }
	    elseif($cptype == 'cp_purple'){
                    $hotspot_folder_type = 'hotspot_purplebox';
                
            }
	       // change permission of root file
	       $path = $_SERVER['DOCUMENT_ROOT'].'/isp_hotspot';
		if($path != ''){
			$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
			if( $fldrperm != 0777) {
				$output = exec("sudo chmod -R 0777 \"$path\"");
			}
		}
		// change isp folder permission
		if($domain_exists == '1'){
		     $path = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name;
		}else{
		     $path = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com";
		}
	      
	       if($path != ''){
		    $fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
		    if( $fldrperm != 0777) {
			 $output = exec("sudo chmod -R 0777 \"$path\"");
		    }
	       }
	       $src = '';
	       if($domain_exists == '1'){
		    if($router_type == '1'){
			 $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/mikrotik_kt";
			 if (!file_exists($dst_microtik)) {
			     mkdir($dst_microtik, 0777, true);
			 }
			 $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/mikrotik_kt/".$hotspot_folder_type;
			 if (!file_exists($dst)) {
			     mkdir($dst, 0777, true);
			 }
			 $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/mikrotik_kt/".$hotspot_folder_type;
			 $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/mikrotik_kt/".$hotspot_folder_type;
			
		    }else if($router_type == '5'){
			 $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/cambium";
			 if (!file_exists($dst_microtik)) {
			     mkdir($dst_microtik, 0777, true);
			 }
			 $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/cambium/".$hotspot_folder_type;
			 if (!file_exists($dst)) {
			     mkdir($dst, 0777, true);
			 }
			 $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/cambium/".$hotspot_folder_type;
			 $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/cambium/".$hotspot_folder_type;
		    }else if($router_type == '6'){
			 $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/onehop";
			 if (!file_exists($dst_microtik)) {
			     mkdir($dst_microtik, 0777, true);
			 }
			 $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/onehop/".$hotspot_folder_type;
			 if (!file_exists($dst)) {
			     mkdir($dst, 0777, true);
			 }
			 $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$isp_domain_name."/onehop/".$hotspot_folder_type;
			 $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/onehop/".$hotspot_folder_type;
		    }
	       }else{
		    if($router_type == '1'){
			 $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/mikrotik_kt";
			 if (!file_exists($dst_microtik)) {
			     mkdir($dst_microtik, 0777, true);
			 }
			 $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/mikrotik_kt/".$hotspot_folder_type;
			 if (!file_exists($dst)) {
			     mkdir($dst, 0777, true);
			 }
			 $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/mikrotik_kt/".$hotspot_folder_type;
			 $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/mikrotik_kt/".$hotspot_folder_type;
			
		    }else if($router_type == '5'){
			 $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/cambium";
			 if (!file_exists($dst_microtik)) {
			     mkdir($dst_microtik, 0777, true);
			 }
			 $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/cambium/".$hotspot_folder_type;
			 if (!file_exists($dst)) {
			     mkdir($dst, 0777, true);
			 }
			 $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/cambium/".$hotspot_folder_type;
			 $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/cambium/".$hotspot_folder_type;
		    }else if($router_type == '6'){
			 $dst_microtik = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/onehop";
			 if (!file_exists($dst_microtik)) {
			     mkdir($dst_microtik, 0777, true);
			 }
			 $dst = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/onehop/".$hotspot_folder_type;
			 if (!file_exists($dst)) {
			     mkdir($dst, 0777, true);
			 }
			 $editorpath = $_SERVER['DOCUMENT_ROOT']."/ispadmins/".$portal_url.".shouut.com/onehop/".$hotspot_folder_type;
			 $src = $_SERVER['DOCUMENT_ROOT']."/isp_hotspot/onehop/".$hotspot_folder_type;
		    }
	       }
	       
	       // change isp folder permission
	       $path = $dst_microtik;
	       if($path != ''){
		    $fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
		    if( $fldrperm != 0777) {
			 $output = exec("sudo chmod -R 0777 \"$path\"");
		    }
	       }
	           //echo $src.'--'.$dst;die;
		   // echo $portal_url."--".$ispurl."--".$editorpath."--".$isp_uid.'--'.$hotspot_folder_type.'--'.$location_id;die;
		    if($router_type != '2' && $router_type != '3' && $router_type != '4'){
			 $this->recurse_copy($src,$dst);
			 $this->edit_configuration($portal_url,$ispurl,$editorpath,$isp_uid,$hotspot_folder_type,$location_id);
			 
			 $path = $dst_microtik;
			 if($path != ''){
			      $fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
			      if( $fldrperm != 0777) {
				   $output = exec("sudo chmod -R 0777 \"$path\"");
			      }
			 }
		    }
		    /*if($router_type == '6' && $cptype == 'cp2'){// for onehop cp2 only
			 
			 $this->recurse_copy($src,$dst);
			 $this->edit_configuration($portal_url,$ispurl,$editorpath,$isp_uid,$hotspot_folder_type,$location_id);
		    }*/
		    
        }
     }

     // CI query changes
     public function recurse_copy($src,$dst) {
	 $dir = opendir($src); 
	 @mkdir($dst); 
	 while(false !== ( $file = readdir($dir)) ) { 
	     if (( $file != '.' ) && ( $file != '..' )) { 
		 if ( is_dir($src . '/' . $file) ) {
		     if(($file == 'source') || ($file == 'user_guide')){
			 continue;
		     }elseif($file == 'ispmedia'){
			 if (!file_exists($dst.'/'.$file)) {
			     @mkdir($dst.'/'.$file, 0777, true);
			 }
			 continue;
		     }elseif($file == 'documents'){
			 if (!file_exists($dst.'/'.$file)) {
			     @mkdir($dst.'/'.$file, 0777, true);
			 }
			 continue;
		     }elseif($file == 'marketing_promotion'){
			 if (!file_exists($dst.'/'.$file)) {
			     @mkdir($dst.'/'.$file, 0777, true);
			 }
			 continue;
		     }elseif($file == 'slider_promotion'){
			 if (!file_exists($dst.'/'.$file)) {
			     @mkdir($dst.'/'.$file, 0777, true);
			 }
			 continue;
		     }else{
			 $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
		     }
		 } 
		 else { 
		     copy($src . '/' . $file,$dst . '/' . $file); 
		 } 
	     } 
	 } 
	 closedir($dir);
	 return 1;
     }
    
     // CI query changes
     public function edit_configuration($portal,$ispurl='',$editorpath='',$isp_uid='',$type='', $location_id = ''){
         
	//echo $editorpath;die;
            $chreading = fopen($editorpath."/libraries/config.php", 'r');
            $chwriting = fopen($editorpath."/libraries/config.tmp", 'w');
            $chreplaced = false;
            while (!feof($chreading)) {
                $line = fgets($chreading);
                if(preg_match("/\b(BASE_URL)\b/", $line)){
                    $line = 'define("BASE_URL","'.$ispurl.$type.'/"); ';
                    $chreplaced = true;
                }
                if(preg_match("/\b(ASSETS_URL)\b/", $line)){
                    $line = 'define("ASSETS_URL","'.$ispurl.$type.'/"); ';
                    $chreplaced = true;
                }
                if(preg_match("/\b(ISP_UID)\b/", $line)){
                    $line = 'define("ISP_UID","'.$isp_uid.'"); ';
                    $chreplaced = true;
                }
                if(preg_match("/\b(LOCATIONID)\b/", $line)){
                    $line = 'define("LOCATIONID","'.$location_id.'"); ';
                    $chreplaced = true;
                }
                fputs($chwriting, $line);
            }
            fclose($chreading); fclose($chwriting);
            if ($chreplaced) {
                rename($editorpath."/libraries/config.tmp", $editorpath."/libraries/config.php");
            } else {
                unlink($editorpath."/libraries/config.tmp");
            }
       
	/*****************************************************************************************/

        return 1;
     }
    
     // CI query changes
     public function nas_setup_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $isp_uid = '';
	  if(isset($jsondata->isp_uid)){
	    $isp_uid = $jsondata->isp_uid;
	  }
	  $data = array();
	  $vouchers = array();
	  $this->DB2->select('wl.id as location_id,wl.nasipaddress, n.nastype,n.id,n.shortname,n.nasname')->from('wifi_location as wl');
	  $this->DB2->where(array('wl.location_uid'=>$location_uid, 'wl.nasconfigure' => '1'));
	  $this->DB2->join('nas as n', 'n.id = wl.nasid', 'INNER');
	  $query = $this->DB2->get();
	 //echo $query->num_rows();
	 if($query->num_rows() > 0){
	     $data['resultCode'] = '1';
	     $i = 0;
	     foreach($query->result() as $row){
		$data['shortname'] = $row->shortname;
		$data['nasname'] = $row->nasname;
		 $data['nasipaddress'] = $row->nasipaddress;
		 $data['nastype'] = $row->nastype;
		 $data['nasid'] = $row->id;
		 $requestData = array(
			 'location_id' => $row->location_id,
			 'isp_uid' => $isp_uid
		 );
		 $data_request = json_decode(json_encode($requestData));
		 $url = $this->get_isp_url($data_request);
		 $data['cp_url'] = $url['url'];
	     }
	     $data['vouchers']= $vouchers;
	     // check network created or not
	     $is_network_created = '0';
	     $network_name = '';
	       $this->DB2->select('id,network_name,parent_network')->from('onehop_networks');
	       $this->DB2->where('location_uid', $location_uid);
	       $check_network = $this->DB2->get();
	     if($check_network->num_rows() > 0){
		$is_network_created = 1;
		$network_row = $check_network->row_array();
		$parent_network = $network_row['parent_network'];
		if($parent_network > 0){
		     $check_network_parent = $this->DB2->query("select id,network_name from onehop_networks where id = '$parent_network'");
		    $this->DB2->select('id,network_name')->from('onehop_networks');
		    $this->DB2->where('id', $parent_network);
		    $check_network_parent = $this->DB2->get();
		     if($check_network_parent->num_rows() > 0){
			  $network_row_parent = $check_network_parent->row_array();
			  $network_name = $network_row_parent['network_name'];
		     }
		}else{
		     $network_name = $network_row['network_name'];
		}
		
	     }
	     $data['is_network_created']= $is_network_created;
	     $data['network_name']= $network_name;
	     // check network ssid created or not
	     $is_network_ssid_created = '0';
	     
	       $this->DB2->select('id')->from('onehop_ssid');
	       $this->DB2->where(array('location_uid'=> $location_uid, 'is_deleted'=>'0'));
	       $this->DB2->group_start();
	       $this->DB2->where('ssid_index', '0');
	       $this->DB2->or_where('ssid_index IS NULL', null, false);
	       $this->DB2->group_end();
	       $check_network_ssid = $this->DB2->get();
	     //echo $this->DB2->last_query();die;
	     if($check_network_ssid->num_rows() > 0){
		$is_network_ssid_created = 1;
	       
	     }
	     $data['is_network_ssid_created']= $is_network_ssid_created;
	     
	     $is_network_vip_ssid_created = '0';
	     
	     $this->DB2->select('id')->from('onehop_ssid');
	       $this->DB2->where(array('location_uid'=> $location_uid, 'is_deleted'=>'0', 'ssid_index'=>'1'));
	       $check_network_ssid = $this->DB2->get();
	     //echo $this->DB2->last_query();die;
	     if($check_network_ssid->num_rows() > 0){
		$is_network_vip_ssid_created = 1;
	       
	     }
	     $data['is_network_vip_ssid_created']= $is_network_vip_ssid_created;
	     
	     // get onehop macid list
	     $onehop_macids = array();
	     $this->DB2->select('macid,apmac_name')->from('onehop_macids');
	       $this->DB2->where('location_uid',$location_uid );
	       $get_macids = $this->DB2->get();
	     if($check_network->num_rows() > 0){
		$i = 0;
		foreach($get_macids->result() as $mac_row){
		     $onehop_macids[$i]['macid'] = $mac_row->macid;
		     $onehop_macids[$i]['apmac_name'] = $mac_row->apmac_name;
		     $i++;
		}
	     }
	     $data['onehop_macids']= $onehop_macids;
	 }else{
	     $data['resultCode'] = '0';
	 }
	 return $data;
     }

     // CI query changes
     public function add_location_access_point($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $access_point_ssid = $jsondata->access_point_ssid;
	  $access_point_hotspot_type = $jsondata->access_point_hotspot_type;
	  $access_point_macid = $jsondata->access_point_macid;
	  $access_point_id = $jsondata->access_point_id;
	  $access_point_ip = $jsondata->access_point_ip;
	  $ap_location_type = '';
	  if(isset($jsondata->ap_location_type)){
	    $ap_location_type = $jsondata->ap_location_type;
	  }
	  $nasid = '0';
	  $nas_type = '0';
	  $this->DB2->select('n.nastype,wl.nasid')->from('wifi_location as wl');
	  $this->DB2->where('wl.location_uid', $location_uid);
	  $this->DB2->join('nas as n', 'wl.nasid = n.id', 'LEFT');
	  $get_router_type = $this->DB2->get();
	  if($get_router_type->num_rows() > 0){
	       $row = $get_router_type->row_array();
	       $nasid = $row['nasid'];
	       if(isset($row['nastype'])){
		    $nastype = $row['nastype'];
	       }
	  }
	  $resultCode = '0'  ;
	  $resultMsg = ''  ;
	  $data = array();
	  if($nasid != '0' && $nasid != ''){
	       if($access_point_id == ''){
		    //get router_name
		    $router_type = '';
		    $router_name = '';
		    
		    $this->DB2->select('n.nastype')->from('wifi_location as wl');
		    $this->DB2->where('wl.location_uid', $location_uid);
		    $this->DB2->join('nas as n', 'n.id = wl.nasid', 'INNER');
		    $get_name = $this->DB2->get();
		    if($get_name->num_rows() > 0){
			 $get_name_row = $get_name->row_array();
			 $router_type = $get_name_row['nastype'];
			 if($router_type == '1'){
			     $router_name = 'Microtik';
			 }elseif($router_type == '2'){
			     $router_name = 'EnGenius';
			 }elseif($router_type == '3'){
			     $router_name = 'Cisco';
			 }elseif($router_type == '4'){
			     $router_name = 'Ubiquity';
			 }elseif($router_type == '5'){
			     $router_name = 'Cambium';
			 }elseif($router_type == '6'){
			     $router_name = 'One Hop';
			 }
			 elseif($router_type == '7'){
			     $router_name = 'RASBERRY PI';
			 }
		    }
		   
		    
		    $this->DB2->select('*')->from('wifi_location_access_point');
		    $this->DB2->where('macid', $access_point_macid);
		    $check = $this->DB2->get();
		    
		    if($check->num_rows() > 0){
		       $resultCode = '0'  ;
		       $resultMsg = 'MAC ID already created'  ;
		    }
		    else{
			 //check balance is available
			 
			 $check_balance = $this->isp_license_data($isp_uid);
			 if($check_balance == '1'){
			      if($nastype == '6'){
				   $data_request = array("location_uid" => $location_uid, "onehop_ap_mac" => $access_point_macid, 'onehop_ap_name' => '');
				   $data_request = json_decode(json_encode($data_request));
				   $add_ssid = json_decode($this->onehop_add_apmac($data_request));
				   if($add_ssid->code == '0'){
					$resultCode = '0'  ;
					$resultMsg = $add_ssid->msg ;
				   }else{
					$resultCode = '1'  ;
					$resultMsg = '';
					$data_access = array(
					     'location_uid' => $location_uid,
					     'router_type' => $router_type,
					     'router_name' => $router_name,
					     'macid' => $access_point_macid,
					     'hotspot_type' => $access_point_hotspot_type,
					     'ssid' => $access_point_ssid,
					     'created_on' => date('Y-m-d H:i:s'),
					     'ip' => $access_point_ip,
					     'ap_location_type' => $ap_location_type
					);
					$this->DB2->insert('wifi_location_access_point', $data_access);
					// insert location deduction price
					//get location amount
					$location_price = $this->countrydetails($isp_uid);
					$location_cost_deduct = '';
					if(isset($location_price['cost_per_location'])){
					     $location_cost_deduct = $location_price['cost_per_location'];
					     
					     $data_pass = array(
						  'isp_uid' => $isp_uid,
						  'total_active_ap' => '1',
						  'cost' => $location_cost_deduct,
						  'added_on' => date('Y-m-d H:i:s')
					     );
					     $this->db->insert('sht_isp_passbook', $data_pass);
					}
				   }
			      }else{
				   $resultCode = '1'  ;
					$resultMsg = '';
					
					     $data_access = array(
						  'location_uid' => $location_uid,
						  'router_type' => $router_type,
						  'router_name' => $router_name,
						  'macid' => $access_point_macid,
						  'hotspot_type' => $access_point_hotspot_type,
						  'ssid' => $access_point_ssid,
						  'created_on' => date('Y-m-d H:i:s'),
						  'ip' => $access_point_ip,
						  'ap_location_type' => $ap_location_type
					     );
					     $this->DB2->insert('wifi_location_access_point', $data_access);
					// insert location deduction price
					//get location amount
					$location_price = $this->countrydetails($isp_uid);
					$location_cost_deduct = '';
					if(isset($location_price['cost_per_location'])){
					     $location_cost_deduct = $location_price['cost_per_location'];
					     $data_pass = array(
						  'isp_uid' => $isp_uid,
						  'total_active_ap' => '1',
						  'cost' => $location_cost_deduct,
						  'added_on' => date('Y-m-d H:i:s')
					     );
					     $this->db->insert('sht_isp_passbook', $data_pass);
					}
				   // add in indian country for synk
				   if($nastype == 7){
					$this->add_app_in_country_for_synk($isp_uid, $access_point_macid);
					
				   }
			      }
			      
			 }else{
			      $resultCode = '0'  ;
			      $resultMsg = 'Balance not available' ;
			 }
			 
			 
		    }
	       }
	       else{
		    // before update check ssid already exist
		    $this->DB2->select('*')->from('wifi_location_access_point');
		    $this->DB2->where('id !=', $access_point_id);
		    $this->DB2->where('macid', $access_point_macid);
		    $check = $this->DB2->get();

		    if($check->num_rows() > 0){
		       $resultCode = '0'  ;
		       $resultMsg = 'MAC ID already created'  ;
		    }else{
			 $data_access = array(
			      'macid' => $access_point_macid,
			      'hotspot_type' => $access_point_hotspot_type,
			      'ssid' => $access_point_ssid,
			      'ip' => $access_point_ip,
			      'ap_location_type' => $ap_location_type,
			      'updated_on' => date('Y-m-d H:i:s')
			 );
			 $this->DB2->where('id', $access_point_id);
			 $this->DB2->update('wifi_location_access_point', $data_access);
			  // add in indian country for synk
			  
			 $this->add_app_in_country_for_synk($isp_uid, $access_point_macid);
		    }
	       }
	       if($resultCode != '0'){
		    // store macid for analytics
		    $this->DB2->select('id')->from('wifi_location_nas_used_logs');
		    $this->DB2->where(array('location_uid'=>$location_uid, 'macid' => $access_point_macid));
		    $query_mac = $this->DB2->get();
		    if($query_mac->num_rows() <= '0'){
			 $data_nas = array(
			      'location_uid' => $location_uid,
			      'macid' => $access_point_macid,
			      'created_on' => date('Y-m-d H:i:s')
			 );
			 $this->DB2->insert('wifi_location_nas_used_logs', $data_nas);
		    }
	       }
	  }else{
	       $resultCode = '0'  ;
	       $resultMsg = 'First setup NAS' ;
	  }
	  
	  if($nastype == 7)
	  {
	       // create zip
	       $requestData = array(
		    'location_uid' => $location_uid
	       );
	       $data_request = json_decode(json_encode($requestData));
	       $this->create_zip_for_offline_content($data_request);
	  }
        
	  $data['resultCode'] = $resultCode;
	  $data['resultMsg'] = $resultMsg;
           
	  return $data;
    }
    public function add_app_in_country_for_synk($isp_uid, $access_point_ssid){
	  $countryid = 0;
	  $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
	  if($ispcountryQ->num_rows() > 0){
	       $crowdata = $ispcountryQ->row();
	       $countryid = $crowdata->country_id;
	  }
	  $requestData = array(
			'macId' => $access_point_ssid,
			'country_id' => $countryid,
			'added_on' => date("y-m-d h:i:s")
	  );
	  $service_url = "https://www.shouut.com/isp_consumer_api/naswifi/add_apmac_country";
	  $curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
     }
    
public function raspberry_ping_sync_info($jsondata){
		$data['resultCode']=1;
		$pingq=$this->DB2->query("select opl.id,opl.status,opl.pinged_on,opl.loc_macid from offline_ping_logs opl inner join wifi_location wl on (opl.loc_id=wl.id) where opl.loc_macid = '$jsondata->macid' and wl.location_uid='$jsondata->location_uid' order by opl.id desc limit 20");
		
		if($pingq->num_rows()>0)
		{
			$i=0;
			foreach($pingq->result() as $val)
			{
				$data['pingdata'][$i]['status']=($val->status==1)?"Connected":"Not Connected";
				$data['pingdata'][$i]['pinged_on']=date("d-m-Y H:i:s",strtotime($val->pinged_on));
				$data['pingdata'][$i]['loc_macid']=$val->loc_macid;
				$i++;
			}
			
		}else{
			$data['pingdata']=array();
		}
		
		$syncq=$this->DB2->query("select version,last_communicated,payload_size,payload_receive,upload_status from offline_build_version obv inner join wifi_location wl on (obv.loc_id=wl.id) where obv.mac_id = '$jsondata->macid' and wl.location_uid='$jsondata->location_uid'  order by obv.id desc limit 20");
		if($syncq->num_rows()>0)
		{
			$i=0;
			foreach($syncq->result() as $val)
			{
				if($val->upload_status == '0'){
					 $payload_size = $val->payload_size;
					 $payload_receive = $val->payload_receive;
					  $payload_percent = 0;
					  $status="In Progress";
					 if($payload_size > 0){
					  $payload_percent = round(($payload_receive*100)/$payload_size);
					  }
				}else if($val->upload_status == '1'){
				  $payload_size = $val->payload_size;
					 $payload_receive = $val->payload_receive;
					  $payload_percent = 0;
					  $status="Complete";
					 if($payload_size > 0){
					  $payload_percent = round(($payload_receive*100)/$payload_size);
					  }	
				}else{
					 $payload_size = $val->payload_size;
					 $payload_receive = $val->payload_receive;
					  $payload_percent = 0;
					  $status="Failed";
					 if($payload_size > 0){
					  $payload_percent = round(($payload_receive*100)/$payload_size);
					  }
					
				}
					$data['syncdata'][$i]['version']=$val->version;
					$data['syncdata'][$i]['last_communicated']=date("d-m-Y H:i:s",strtotime($val->last_communicated));
				$data['syncdata'][$i]['payload_size']=$val->payload_size;
				$data['syncdata'][$i]['payload_receive']=$val->payload_receive;
				$data['syncdata'][$i]['percent']=$payload_percent;
				$data['syncdata'][$i]['status']=$status;
				$i++;
			}
			
		}else{
			$data['syncdata']=array();
		}
		
	return $data;
	}
    public function check_raspberry_ping($macid,$locuid)
	{
		 $query = $this->DB2->query("select opl.id,opl.status,opl.pinged_on from offline_ping_logs opl inner join wifi_location wl on (opl.loc_id=wl.id) where opl.loc_macid = '$macid' and wl.location_uid='$locuid' order by id desc limit 1");
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			
			$currenttime=strtotime(date("Y-m-d H:i:s"));
			$pinged_on=strtotime($rowarr['pinged_on']);
			$timediff=$currenttime-$pinged_on;
			if($timediff<600)
			{
				return $rowarr['status'];
			}
			else{
				return 0;
			}
			
		}
		else{
			return 0;
		}
	}
    public function location_access_point_list($jsondata){
        $location_uid = $jsondata->location_uid;
        $data = array();
        $vouchers = array();
        
	  $this->DB2->select('*')->from('wifi_location_access_point');
	  $this->DB2->where('location_uid', $location_uid);
	  $query = $this->DB2->get();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $i = 0;
            foreach($query->result() as $row){
                $vouchers[$i]['id'] = $row->id;
                $vouchers[$i]['router_name'] = $row->router_name;
		$vouchers[$i]['router_type'] = $row->router_type;
		$vouchers[$i]['location_uid'] = $row->location_uid;
                $vouchers[$i]['macid'] = $row->macid;
                $vouchers[$i]['hotspot_type'] = $row->hotspot_type;
                $vouchers[$i]['ssid'] = $row->ssid;
                $vouchers[$i]['ip'] = $row->ip;
		$vouchers[$i]['ap_location_type'] = $row->ap_location_type;
		if($row->router_type==7)
				{
					$vouchers[$i]['raspberry_status']=$this->check_raspberry_ping($row->macid, $row->location_uid);
				}else{
					$vouchers[$i]['raspberry_status']=0;
				}
                $i++;
            }
            $data['vouchers']= $vouchers;
        }else{
            $data['resultCode'] = '0';
        }
        return $data;
    }
    


    public function delete_access_point($jsondata){
        $access_point_id = $jsondata->access_point_id;
        
        $data = array();
	$get = $this->DB2->query("select macid from wifi_location_access_point where id = '$access_point_id'");
	if($get->num_rows() > 0){
	       $macid_row = $get->row_array();
	       $macid = $macid_row['macid'];
	       $this->DB2->query("delete from onehop_macids where macid = '$macid'");
	}
        $query = $this->DB2->query("delete from wifi_location_access_point where id = '$access_point_id'");
        $data['resultCode'] = '1';
           
        return $data;
    }
     // CI query changes
     public function add_zone($jsondata){
	 $city_id = $jsondata->city_id;
	 $zone_name = $jsondata->zone_name;
	 $isp_uid = $jsondata->isp_uid;
	 
	 $data = array();
	 
	  $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'zone_name' => $zone_name,
		    'city_id' => $city_id
	    );
	    $this->db->insert('sht_zones', $insert_data);
	   $data['resultCode'] = '1';
	    
	 return $data;
     }
     // CI query changes
     public function add_city($jsondata){
	 $state_id = $jsondata->state_id;
	 $city_name = $jsondata->city_name;
	 $isp_uid = $jsondata->isp_uid;
	 
	 $data = array();
	 $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'city_name' => $city_name,
		    'city_state_id' => $state_id
	    );
	    $this->db->insert('sht_cities', $insert_data);
	 $data['resultCode'] = '1';
	    
	 return $data;
     }
     // CI query changes
     public function location_dashboard_search($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $serach_pattern = $jsondata->search_val;
	  $data = array();$locations = array();
	  /*$query = $this->DB2->query("select location_name,location_uid,contact_person_name,mobile_number,city from wifi_location where isp_uid = '$isp_uid' AND is_deleted = '0' AND (location_name LIKE '%$serach_pattern%' OR location_uid LIKE '%$serach_pattern%' OR contact_person_name LIKE '%$serach_pattern%' OR mobile_number LIKE '%$serach_pattern%' OR pin LIKE '%$serach_pattern%')");*/
	  
	  $this->DB2->select('location_name, location_uid, contact_person_name, mobile_number, city');
	  $this->DB2->from('wifi_location');
	  $this->DB2->where(array('isp_uid' => $isp_uid, 'is_deleted' => '0'));
	  $this->DB2->group_start();
	  $this->DB2->like('location_name', $serach_pattern);
	  $this->DB2->or_like(array('location_uid'=>$serach_pattern, 'contact_person_name'=>$serach_pattern, 'mobile_number'=>$serach_pattern, 'pin'=>$serach_pattern));
	  $this->DB2->group_end();
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $i = 0;
	       foreach($query->result() as $row){
		   if($i ==4){
		       break;
		   }else{
		       $locations[$i]['location_name'] = $row->location_name;
		       $locations[$i]['location_uid'] = $row->location_uid;
		       $locations[$i]['contact_person_name'] = $row->contact_person_name;
		       $locations[$i]['mobile_number'] = "+91".$row->mobile_number;
		       $locations[$i]['city'] = $row->city;
		   }
		   
		   $i++;
	       }
	  }
	  $data['totalrecord'] = $query->num_rows();
	  $data['locations'] = $locations;
	  //echo "<pre>";print_r($data);die;
	  return $data;
     }
         public function wifi_session_report($jsondata){
	   $limit = '20';
	  if(isset($jsondata->limit)){
	       $limit = $jsondata->limit;
	  }
	  $offset = '0';
	  if(isset($jsondata->offset)){
	       $offset = $jsondata->offset;
	  }
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND wl.state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND wl.city_id=".$city."";
		}
        $isp_uid = $jsondata->isp_uid;
        $start_date = $jsondata->start_date;
        $end_date = $jsondata->end_date;
        $query = $this->DB2->query("select wl.address_1, wl.address_2, wl.contact_person_name, wl.mobile_number, wl.user_email,
wl.id,wl.location_uid,wl.location_name,
(SELECT COUNT(*) FROM wifi_user_free_session WHERE location_id = wl.id AND osinfo = 'Windows Phone' AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_session_windows_phone,
(SELECT COUNT(DISTINCT(userid)) FROM wifi_user_free_session WHERE location_id = wl.id AND osinfo = 'Windows Phone' AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_user_windows_phone,
 (SELECT COUNT(*) FROM wifi_user_free_session WHERE location_id = wl.id AND osinfo = 'Windows Desktop' AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_session_laptop,
(SELECT COUNT(DISTINCT(userid)) FROM wifi_user_free_session WHERE location_id = wl.id AND osinfo = 'Windows Desktop' AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_user_laptop,
(SELECT COUNT(*) FROM wifi_user_free_session WHERE location_id = wl.id AND (osinfo = 'Android Phone' OR osinfo = '') AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_session_android,
(SELECT COUNT(DISTINCT(userid)) FROM wifi_user_free_session WHERE location_id = wl.id AND (osinfo = 'Android Phone' OR osinfo = '') AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_user_android,
(SELECT COUNT(*) FROM wifi_user_free_session WHERE location_id = wl.id AND osinfo ='Apple Phone' AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date') as total_session_iphone,
(SELECT COUNT(DISTINCT(userid)) FROM wifi_user_free_session WHERE location_id = wl.id AND  osinfo ='Apple Phone' AND  DATE (session_date) BETWEEN '$start_date' AND '$end_date') as total_user_iphone
  from wifi_location as wl WHERE wl.is_deleted= '0' AND wl.status = '1' AND wl.provider_id = '$isp_uid'  $state_where $city_where  ORDER by wl.location_name LIMIT $offset,$limit");
       
        $data=array();
        if (count($query) > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $data[$i]['id'] = $row->id;
                $data[$i]['location_uid'] = $row->location_uid;
                $data[$i]['address'] = $row->address_1;
                $data[$i]['user_name'] = $row->contact_person_name;
                $data[$i]['mobile'] = $row->mobile_number;
                $data[$i]['user_email'] = $row->user_email;
                $data[$i]['location_name'] = $row->location_name;
                $data[$i]['total_session_windows_phone'] = $row->total_session_windows_phone;
                $data[$i]['total_user_windows_phone'] = $row->total_user_windows_phone;
                $data[$i]['total_session_laptop'] = $row->total_session_laptop;
                $data[$i]['total_user_laptop'] = $row->total_user_laptop;
                $data[$i]['total_session_android'] = $row->total_session_android;
                $data[$i]['total_user_android'] = $row->total_user_android;
                $data[$i]['total_session_iphone'] = $row->total_session_iphone;
                $data[$i]['total_user_iphone'] = $row->total_user_iphone;
                $total_cp_impression = 0;
		$unique_cp_impression = 0;
                $unique_user = 0;
                $total_impression_query = $this->DB2->query("select sum(total_impression) as total_impression,sum(unique_impression) as unique_impression from
                channel_captiveportal_impression where location_id = '$row->id' AND
           (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') ");
                if($total_impression_query->num_rows() > 0){
                    $total_cp_impression_row = $total_impression_query->row_array();
                    $total_cp_impression = $total_cp_impression+$total_cp_impression_row['total_impression'];
		    $unique_cp_impression = $unique_cp_impression+$total_cp_impression_row['unique_impression'];
                }

               /* $captive_portal_data = $this->db->query(" SELECT MAX(unique_user) as unique_user FROM
                `channel_wifioffer_captiveportal_analysis` where location_id = '$row->id' AND
                (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') GROUP BY DATE (visit_date)");*/
                $captive_portal_data = $this->DB2->query(" SELECT MAX(total_unique) as unique_user FROM
                `channel_unique_user` where location_id = '$row->id' AND
                (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') GROUP BY DATE (visit_date)");
                if($captive_portal_data->num_rows() > 0){
                    foreach($captive_portal_data->result() as $captive_portal_data1){
                        $unique_user = $unique_user+$captive_portal_data1->unique_user;
                    }

                }

                $captive_portal_temp_data = $this->DB2->query("SELECT count(DISTINCT(macid)) as unique_user
 FROM channel_unique_daily_user  WHERE location_id =
 '$row->id' AND (DATE(visit_date) BETWEEN '$start_date' AND '$end_date')");
                if($captive_portal_temp_data->num_rows() > 0){
                    $total_cp_click_temp_row = $captive_portal_temp_data->row_array();
                    $unique_user = $unique_user+$total_cp_click_temp_row['unique_user'];
                }
                $data[$i]['unique_user'] = $unique_user;
                $data[$i]['total_cp_impression'] = $total_cp_impression;
		$data[$i]['unique_cp_impression'] = $unique_cp_impression;
                $i++;
            }
          }
         //echo "<pre>";print_r($data);die;
        return $data;
    }
    

        public function wifi_first_free_session_user($jsondata){
        $locationid = $jsondata->locationid;
        /*$start_date = $jsondata->start_date;
        $end_date = $jsondata->end_date;*/
	$start_date = date("Y-m-d", strtotime($jsondata->start_date) );
        $end_date = date("Y-m-d", strtotime($jsondata->end_date) );
	$device_type = '';
	if(isset($jsondata->device_type)){
	  $device_type = $jsondata->device_type;
	}
        $data = array();$users_detail = array();
        if($start_date != '' && $end_date != ''){
            $query = $this->DB2->query("select userid from wifi_user_free_session WHERE DATE(session_date) BETWEEN
            '$start_date' AND '$end_date' AND location_id = '$locationid' AND osinfo = '$device_type'");
        }
        else{
            $query = $this->DB2->query("select userid from wifi_user_free_session WHERE location_id = '$locationid' AND osinfo = '$device_type'");
        }
        $userids = array();
        $userids_num =array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $userids_num[] = $row->userid;
                //$userids[] = new MongoId($row->userid);
		$userids[] = $row->userid;
            }
	    $userids ='"'.implode('", "', $userids).'"';
	    $get_user = $this->DB2->query("select username,uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid IN ($userids)");
            if($get_user->num_rows() > 0){
	       $data['resultCode'] = 1;
               $i = 0;
	       foreach($get_user->result() as $get_user1){
		    //$id = $get_user1->uid;
		    $id = $get_user1->username;
		    $friends_count = array_count_values($userids_num);
		    $no_of_session = $friends_count[$id];
		    $users_detail[$i]['screen_name'] = $get_user1->firstname." ".$get_user1->lastname;
                        if($get_user1->mobile != ''){
			 $users_detail[$i]['mobile'] = $get_user1->mobile;
			 }else{
			      $users_detail[$i]['mobile'] = $get_user1->username;
			 }
                        $users_detail[$i]['email'] = $get_user1->email;
                        $users_detail[$i]['no_of_session'] = $no_of_session;
                        $users_detail[$i]['gender'] = $get_user1->gender;
                        $users_detail[$i]['age_group'] = $get_user1->age_group;
                        $i++;
	       }
	        $data['users_detail'] = $users_detail;
	    }else{
                $data['resultCode'] = 0;
            }
        }else{
            $data['resultCode'] = 0;
        }
        return $data;
        
    }
    



    public function wifi_session_user($jsondata){
        $locationid = $jsondata->locationid;
        $start_date = $jsondata->start_date;
        $end_date = $jsondata->end_date;
        $via = $jsondata->via;
        $data = array();$users_detail = array();
        if($via == 'web'){

            if($start_date != '' && $end_date != ''){
                $query = $this->DB2->query("select userid from wifi_user_session_info WHERE location_id = '$locationid' AND
DATE(session_date) BETWEEN '$start_date' AND '$end_date' AND via = 'web'");
            }
            else{
                $query = $this->DB2->query("select userid from wifi_user_session_info WHERE location_id =
                '$locationid' AND via = 'web'");
            }
        }
        else{
            if($start_date != '' && $end_date != ''){
                $query = $this->DB2->query("select userid from wifi_user_session_info WHERE location_id = '$locationid' AND
DATE(session_date) BETWEEN '$start_date' AND '$end_date' AND via != 'web'");
            }
            else{
                $query = $this->DB2->query("select userid from wifi_user_session_info WHERE location_id =
                '$locationid' AND via != 'web'");
            }
        }
        $userids = array();
        $userids_num =array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $userids_num[] = $row->userid;
               $userids[] = $row->userid;
            }
	    $userids ='"'.implode('", "', $userids).'"';
            $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid IN ($userids)");
            if($get_user->num_rows() > 0){
	       $data['resultCode'] = 1;
               $i = 0;
	       foreach($get_user->result() as $get_user1){
		    $id = $get_user1->uid;
		    $friends_count = array_count_values($userids_num);
		    $no_of_session = $friends_count[$id];
		    $users_detail[$i]['screen_name'] = $get_user1->firstname." ".$get_user1->lastname;
                        $users_detail[$i]['mobile'] = $get_user1->mobile;
                        $users_detail[$i]['email'] = $get_user1->email;
                        $users_detail[$i]['no_of_session'] = $no_of_session;
                        $users_detail[$i]['gender'] = $get_user1->gender;
                        $users_detail[$i]['age_group'] = $get_user1->age_group;
                        $i++;
	       }
	        $data['users_detail'] = $users_detail;
	    }else{
                $data['resultCode'] = 0;
            }
        }else{
            $data['resultCode'] = 0;
        }
        return $data;
        
    }
         public function wifi_session_report_locationvise($jsondata){
	  //$executionStartTime = microtime(true);
	  $locationid = $jsondata->locationid;
	  $start_date = $jsondata->start_date;
	  $end_date = $jsondata->end_date;
	  $isp_uid = $jsondata->isp_uid;
	  $data = array();
	  $query = $this->DB2->query("SELECT DATE_FORMAT(`session_date`, '%Y-%m-%d') AS theDate
FROM wifi_user_free_session wsi WHERE  DATE(wsi.session_date) BETWEEN '$start_date' AND '$end_date'
GROUP BY theDate desc");
	  $i = 0;
	  foreach($query->result() as $row){
	       $session_date = $row->theDate;
	       $data[$i]['theDate'] = $row->theDate;
	       $query_info = $this->DB2->query("SELECT DATE_FORMAT(`session_date`, '%Y-%m-%d') AS theDate,
(SELECT COUNT(*) FROM  wifi_user_free_session where   location_id = '$locationid' AND osinfo = 'Windows Phone' AND DATE(session_date) = '$session_date' ) AS total_session_windows_phone,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where   location_id = '$locationid' AND osinfo ='Windows Phone' AND  DATE(session_date)='$session_date' ) AS total_user_windows_phone,
(SELECT COUNT(*) FROM  wifi_user_free_session where   location_id = '$locationid' AND osinfo = 'Windows Desktop' AND DATE(session_date) = '$session_date' ) AS total_session_laptop,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where   location_id = '$locationid' AND osinfo ='Windows Desktop' AND  DATE(session_date)='$session_date' ) AS total_user_laptop,
(SELECT COUNT(*) FROM  wifi_user_free_session where location_id = '$locationid' AND  osinfo = 'Android Phone' AND DATE(session_date)='$session_date'  ) AS total_session_android,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where  location_id = '$locationid' AND osinfo = 'Android Phone' AND  DATE(session_date)='$session_date') AS total_user_android,
(SELECT COUNT(*) FROM  wifi_user_free_session where  location_id = '$locationid' AND osinfo = 'Apple Phone' AND DATE(session_date)='$session_date'  ) AS total_session_iphone,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where  location_id = '$locationid' AND osinfo = 'Apple Phone' AND DATE(session_date)='$session_date' ) AS total_user_iphone
FROM wifi_user_free_session wsi WHERE  DATE(wsi.session_date) = '$session_date' group by theDate ");
	       $total_session_windows_phone = 0;$total_user_windows_phone = 0;
	       $total_session_laptop = 0; $total_user_laptop = 0;
	       $total_session_android = 0;$total_user_android = 0;
	       $total_session_iphone = 0; $total_user_iphone = 0;
	       foreach($query_info->result() as $row_info){
		    $total_session_windows_phone = $row_info->total_session_windows_phone;
		    $total_user_windows_phone = $row_info->total_user_windows_phone;
		    $total_session_laptop = $row_info->total_session_laptop;
		    $total_user_laptop = $row_info->total_user_laptop;
		    $total_session_android = $row_info->total_session_android;
		    $total_user_android = $row_info->total_user_android;
		    $total_session_iphone = $row_info->total_session_iphone;
		    $total_user_iphone = $row_info->total_user_iphone;
	       }
	       $data[$i]['total_session_windows_phone'] = $total_session_windows_phone;
	       $data[$i]['total_user_windows_phone'] = $total_user_windows_phone;
	       $data[$i]['total_session_laptop'] = $total_session_laptop;
	       $data[$i]['total_user_laptop'] = $total_user_laptop;
	       $data[$i]['total_session_android'] = $total_session_android;
	       $data[$i]['total_user_android'] = $total_user_android;
	       $data[$i]['total_session_iphone'] = $total_session_iphone;
	       $data[$i]['total_user_iphone'] = $total_user_iphone;
	       
	       // get date for impression
	       $total_impression_query = $this->DB2->query("SELECT sum(total_impression) as total_impression, sum(unique_impression) as unique_impression from channel_captiveportal_impression cci
		    WHERE DATE(cci.visit_date) = '$session_date' AND location_id = '$locationid' ");
	       $total_impression = 0;
	       $unique_impression = 0;
	       foreach($total_impression_query->result() as $row_total_imp){
		    $total_impression = $row_total_imp->total_impression;
		    $unique_impression = $row_total_imp->unique_impression;
	       }
	       $data[$i]['total_impression'] = $total_impression;
	       $data[$i]['unique_impression'] = $unique_impression;
	       $total_unique_impression = 0;
	       // get unique impression
	       $captive_portal_data = $this->DB2->query(" SELECT MAX(total_unique) as unique_user FROM
		    channel_unique_user where location_id = '$locationid' AND DATE(visit_date) = '$session_date'");
	       foreach($captive_portal_data->result() as $row_unique_imp){
		    $total_unique_impression = $total_unique_impression+$row_unique_imp->unique_user;
	       }
	       // get unique impression from temp table
	       $captive_portal_temp_data = $this->DB2->query("SELECT count(DISTINCT(macid)) as unique_user FROM
		    channel_unique_daily_user  WHERE location_id = '$locationid' AND DATE(visit_date) = '$session_date'");
	       foreach($captive_portal_temp_data->result() as $row_unique_temp_imp){
		    $total_unique_impression = $total_unique_impression+$row_unique_temp_imp->unique_user;
	       }
	        $data[$i]['total_unique_impression'] = $total_unique_impression;
	
	       $i++;
	  }
	  $data_new = array();
	  $location_ids = array($locationid);
	  $location_ids='"'.implode('", "', $location_ids).'"';
	  $total_online_user = $this->online_user($location_ids);
	  $data_new["total_online_user"] = $total_online_user;
	  $data_new["session_data"] = $data;
	  return $data_new;
	  /*echo "<pre>"; print_r($data);  
	  $executionEndTime = microtime(true);
	  $seconds = $executionEndTime - $executionStartTime;
	  echo "<br />This script took $seconds to execute.";*/
     }

        public function wifi_session_report_locationvise_old($jsondata){
        $locationid = $jsondata->locationid;
        $start_date = $jsondata->start_date;
        $end_date = $jsondata->end_date;
        $isp_uid = $jsondata->isp_uid;
        $query = $this->DB2->query("SELECT DATE_FORMAT(`session_date`, '%Y-%m-%d') AS theDate,
(SELECT COUNT(*) FROM  wifi_user_free_session where   location_id = '$locationid' AND DATE(session_date) = thedate AND osinfo = 'Windows Phone') AS total_session_windows_phone,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where   location_id = '$locationid' AND  DATE(session_date)=thedate AND osinfo ='Windows Phone') AS total_user_windows_phone,
(SELECT COUNT(*) FROM  wifi_user_free_session where   location_id = '$locationid' AND DATE(session_date) = thedate AND osinfo = 'Windows Desktop') AS total_session_laptop,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where   location_id = '$locationid' AND  DATE(session_date)=thedate AND osinfo ='Windows Desktop') AS total_user_laptop,
(SELECT COUNT(*) FROM  wifi_user_free_session where location_id = '$locationid' AND DATE(session_date)=thedate  AND  osinfo = 'Android Phone') AS total_session_android,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where  location_id = '$locationid' AND DATE(session_date)=thedate AND osinfo = 'Android Phone') AS total_user_android,
(SELECT COUNT(*) FROM  wifi_user_free_session where  location_id = '$locationid' AND DATE(session_date)=thedate  AND osinfo = 'Apple Phone') AS total_session_iphone,
(SELECT COUNT(DISTINCT(userid)) FROM  wifi_user_free_session where  location_id = '$locationid' AND DATE(session_date)=thedate AND osinfo = 'Apple Phone') AS total_user_iphone
FROM wifi_user_free_session wsi WHERE  DATE(wsi.session_date) BETWEEN '$start_date' AND '$end_date'
GROUP BY theDate ");
        // get date for impression
        $total_impression_query = $this->DB2->query("SELECT sum(total_impression) as total_impression, DATE_FORMAT
            (`visit_date`, '%Y-%m-%d') AS theDate from
channel_captiveportal_impression cci WHERE DATE(cci.visit_date) BETWEEN '$start_date' AND '$end_date' AND
location_id = '$locationid' GROUP BY
theDate ");
         // get unique impression
        $captive_portal_data = $this->DB2->query(" SELECT MAX(total_unique) as unique_user, DATE_FORMAT
            (`visit_date`, '%Y-%m-%d') AS theDate FROM
                `channel_unique_user` where location_id = '$locationid' AND
                (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') GROUP BY DATE (visit_date)");
// get unique impression from temp table
        $captive_portal_temp_data = $this->DB2->query("SELECT count(DISTINCT(macid)) as unique_user, DATE_FORMAT
            (`visit_date`, '%Y-%m-%d') AS theDate
 FROM channel_unique_daily_user  WHERE location_id =
 '$locationid' AND (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') GROUP BY DATE (visit_date)");
        $data=array();
        $totalimpression = array();
        $totalunique = array();
        $totalunique_temp = array();
        $locationvise = array();
        $daily_free = array();
        if (count($query) > 0) {
            $i = 0;
            foreach ($query->result() as $row) {
                $locationvise[$i]['theDate'] = $row->theDate;
                $locationvise[$i]['total_impression'] = '0';
                $locationvise[$i]['total_unique_impression'] = '0';
                $locationvise[$i]['total_session_windows_phone'] = $row->total_session_windows_phone;
                $locationvise[$i]['total_user_windows_phone'] = $row->total_user_windows_phone;
                $locationvise[$i]['total_session_laptop'] = $row->total_session_laptop;
                $locationvise[$i]['total_user_laptop'] = $row->total_user_laptop;
                $locationvise[$i]['total_session_android'] = $row->total_session_android;
                $locationvise[$i]['total_user_android'] = $row->total_user_android;
                $locationvise[$i]['total_session_iphone'] = $row->total_session_iphone;
                $locationvise[$i]['total_user_iphone'] = $row->total_user_iphone;
                $i++;
            }
        }
        
        if (count($total_impression_query) > 0) {
            $k = 0;
            foreach ($total_impression_query->result() as $row2) {
                $totalimpression[$k]['theDate'] = $row2->theDate;
                $totalimpression[$k]['total_impression'] = $row2->total_impression;
                $totalimpression[$k]['total_unique_impression'] = '0';
                $totalimpression[$k]['total_session_windows_phone'] = '0';
                $totalimpression[$k]['total_user_windows_phone'] = '0';
                $totalimpression[$k]['total_session_laptop'] = '0';
                $totalimpression[$k]['total_user_laptop'] = '0';
                $totalimpression[$k]['total_session_android'] = '0';
                $totalimpression[$k]['total_user_android'] = '0';
                $totalimpression[$k]['total_session_iphone'] = '0';
                $totalimpression[$k]['total_user_iphone'] = '0';
                $k++;
            }
        }
        if (count($captive_portal_data) > 0) {
            $l = 0;
            foreach ($captive_portal_data->result() as $row3) {
                $totalunique[$l]['theDate'] = $row3->theDate;
                $totalunique[$l]['total_impression'] = '0';
                $totalunique[$l]['total_unique_impression'] = $row3->unique_user;
                $totalimpression[$k]['total_session_windows_phone'] = '0';
                $totalimpression[$k]['total_user_windows_phone'] = '0';
                $totalunique[$l]['total_session_laptop'] = '0';
                $totalunique[$l]['total_user_laptop'] = '0';
                $totalunique[$l]['total_session_android'] = '0';
                $totalunique[$l]['total_user_android'] = '0';
                $totalunique[$l]['total_session_iphone'] = '0';
                $totalunique[$l]['total_user_iphone'] = '0';
                $l++;
            }
        }
        if (count($captive_portal_temp_data) > 0) {
            $m = 0;
            foreach ($captive_portal_temp_data->result() as $row4) {
                $totalunique_temp[$m]['theDate'] = $row4->theDate;
                $totalunique_temp[$m]['total_impression'] = '0';
                $totalunique_temp[$m]['total_unique_impression'] = $row4->unique_user;
                $totalunique_temp[$m]['total_session_windows_phone'] = '0';
                $totalunique_temp[$m]['total_user_windows_phone'] = '0';
                $totalunique_temp[$m]['total_session_laptop'] = '0';
                $totalunique_temp[$m]['total_user_laptop'] = '0';
                $totalunique_temp[$m]['total_session_android'] = '0';
                $totalunique_temp[$m]['total_user_android'] = '0';
                $totalunique_temp[$m]['total_session_iphone'] = '0';
                $totalunique_temp[$m]['total_user_iphone'] = '0';
                $m++;
            }
        }

        $mergearr=array_merge($daily_free,$locationvise, $totalimpression, $totalunique, $totalunique_temp);
        $newarr=array();
        foreach($mergearr as $val)
        {
            if(!empty($newarr))
            {
                if(array_key_exists($val['theDate'],$newarr))
                {

                    $newarr[$val['theDate']]=$newarr[$val['theDate']];
                    $newarr[$val['theDate']]['total_impression']=$newarr[$val['theDate']]['total_impression']+$val['total_impression'];
                    $newarr[$val['theDate']]['total_unique_impression']=$newarr[$val['theDate']]['total_unique_impression']+$val['total_unique_impression'];
                    $newarr[$val['theDate']]['total_session_windows_phone']=$newarr[$val['theDate']]['total_session_windows_phone']+$val['total_session_windows_phone'];
                    $newarr[$val['theDate']]['total_user_windows_phone']=$newarr[$val['theDate']]['total_user_windows_phone']+$val['total_user_windows_phone'];
                    $newarr[$val['theDate']]['total_session_laptop']=$newarr[$val['theDate']]['total_session_laptop']+$val['total_session_laptop'];
                    $newarr[$val['theDate']]['total_user_laptop']=$newarr[$val['theDate']]['total_user_laptop']+$val['total_user_laptop'];
                    $newarr[$val['theDate']]['total_session_android']=$newarr[$val['theDate']]['total_session_android']+$val['total_session_android'];
                    $newarr[$val['theDate']]['total_user_android']=$newarr[$val['theDate']]['total_user_android']+$val['total_user_android'];
                    $newarr[$val['theDate']]['total_session_iphone']=$newarr[$val['theDate']]['total_session_iphone']+$val['total_session_iphone'];
                    $newarr[$val['theDate']]['total_user_iphone']=$newarr[$val['theDate']]['total_user_iphone']+$val['total_user_iphone'];
                }
                else{
                    $newarr[$val['theDate']]=$val;
                }
            }
            else
            {
                $newarr[$val['theDate']]=$val;
            }

        }
        $data=array_values($newarr);
        function date_compare($a, $b)
        {
            $t1 = strtotime($a['theDate']);
            $t2 = strtotime($b['theDate']);
            return $t2 - $t1;
        }
        usort($data, 'date_compare');
        //echo "<pre>";print_r($data);die;
        return $data;

    }
        public function wifi_first_free_session_user_locationvise($jsondata){
        $locationid = $jsondata->locationid;
        $visit_date = $jsondata->visit_date;
	$device_type = '';
	if(isset($jsondata->device_type)){
	  $device_type = $jsondata->device_type;
	}
        $data = array();$users_detail = array();
        $query = $this->DB2->query("select userid from wifi_user_free_session WHERE location_id = '$locationid'
            AND DATE(session_date) = '$visit_date' AND osinfo = '$device_type'");

        $userids = array();
        $userids_num =array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $userids_num[] = $row->userid;
                $userids[] = $row->userid;
            }
	    $userids ='"'.implode('", "', $userids).'"';
            $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid IN ($userids)");
            if($get_user->num_rows() > 0){
	       $data['resultCode'] = 1;
               $i = 0;
	       foreach($get_user->result() as $get_user1){
		    $id = $get_user1->uid;
		    $friends_count = array_count_values($userids_num);
		    $no_of_session = $friends_count[$id];
		    $users_detail[$i]['screen_name'] = $get_user1->firstname." ".$get_user1->lastname;
                        $users_detail[$i]['mobile'] = $get_user1->mobile;
                        $users_detail[$i]['email'] = $get_user1->email;
                        $users_detail[$i]['no_of_session'] = $no_of_session;
                        $users_detail[$i]['gender'] = $get_user1->gender;
                        $users_detail[$i]['age_group'] = $get_user1->age_group;
                        $i++;
	       }
	        $data['users_detail'] = $users_detail;
	    }else{
                $data['resultCode'] = 0;
            }
        }else{
            $data['resultCode'] = 0;
        }
        return $data;
    }
    




        public function wifi_session_user_locationvise($jsondata){
        $locationid = $jsondata->locationid;
        $visit_date = $jsondata->visit_date;
        $via = $jsondata->via;
        $data = array();$users_detail = array();
        if($via == 'web'){

                $query = $this->DB2->query("select userid from wifi_user_session_info WHERE location_id =
                '$locationid' AND via = 'web' AND DATE(session_date) = '$visit_date'");

        }
        else{

                $query = $this->DB2->query("select userid from wifi_user_session_info WHERE location_id =
                '$locationid' AND via != 'web' AND DATE(session_date) = '$visit_date'");

        }

        $userids = array();
        $userids_num =array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $userids_num[] = $row->userid;
                $userids[] = $row->userid;
            }
            $userids ='"'.implode('", "', $userids).'"';
	    $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid IN ($userids)");
            if($get_user->num_rows() > 0){
	       $data['resultCode'] = 1;
               $i = 0;
	       foreach($get_user->result() as $get_user1){
		    $id = $get_user1->uid;
		    $friends_count = array_count_values($userids_num);
		    $no_of_session = $friends_count[$id];
		    $users_detail[$i]['screen_name'] = $get_user1->firstname." ".$get_user1->lastname;
                        $users_detail[$i]['mobile'] = $get_user1->mobile;
                        $users_detail[$i]['email'] = $get_user1->email;
                        $users_detail[$i]['no_of_session'] = $no_of_session;
                        $users_detail[$i]['gender'] = $get_user1->gender;
                        $users_detail[$i]['age_group'] = $get_user1->age_group;
                        $i++;
	       }
	        $data['users_detail'] = $users_detail;
	    }else{
                $data['resultCode'] = 0;
            }
        }else{
            $data['resultCode'] = 0;
        }
        return $data;
        
    }
    
         public function wifi_session_failure_report($jsondata){
	  $limit = '20';
	  if(isset($jsondata->limit)){
	       $limit = $jsondata->limit;
	  }
	  $offset = '0';
	  if(isset($jsondata->offset)){
	       $offset = $jsondata->offset;
	  }
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND wl.state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND wl.city_id=".$city."";
		}
        $isp_uid = $jsondata->isp_uid;
        $start_date = $jsondata->start_date;
        $end_date = $jsondata->end_date;
        $query = $this->DB2->query("select wl.id,wl.location_name from wifi_location as wl WHERE wl.is_deleted= '0'
        AND status = '1' AND provider_id = '$isp_uid' $state_where $city_where
        ORDER BY wl.location_name LIMIT $offset,$limit");
        $data=array();
       if(count($query) > 0){
           $i = 0;
           $location_ids = array();
           foreach ($query->result() as $row) {
               $location_ids[] = $row->id;
               $data[$i]['id'] = $row->id;
               $data[$i]['location_name'] = $row->location_name;
			   $data[$i]['total_cp_impression'] = 0;
			   $data[$i]['unique_user'] = 0;
			   $data[$i]['total_otp_send'] = 0;
			   $data[$i]['total_otp_verified'] = 0;
			   $data[$i]['total_new_user'] = 0;
			   $data[$i]['total_session'] = 0;
               $i++;
           }
           $location_ids='"'.implode('", "', $location_ids).'"';
           $data1=array();
           // to get total impression
           $total_impression_query = $this->DB2->query("select sum(total_impression) as total_impression, location_id
           from channel_captiveportal_impression where location_id IN ($location_ids) AND
           (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') GROUP BY location_id");
		   if($total_impression_query->num_rows()> 0){
			foreach($total_impression_query->result() as $total_impression_query1){
                foreach($data as $key=>$value) {
                    if($total_impression_query1->location_id == $value['id']){
                        $data[$key]['total_cp_impression'] = $data[$key]['total_cp_impression']+$total_impression_query1->total_impression;
						break;
                    }
                }
            }   
		   }
            
           // to get unique user from main table
         
           $captive_portal_data = $this->DB2->query(" SELECT MAX(total_unique) as unique_user, location_id FROM
                `channel_unique_user` where location_id in($location_ids)  AND
                (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') GROUP BY location_id");
				if($captive_portal_data->num_rows()> 0){
					foreach($captive_portal_data->result() as $captive_portal_data1){
						foreach($data as $key=>$value) {
							if($captive_portal_data1->location_id == $value['id']){
								$data[$key]['unique_user'] = $data[$key]['unique_user']+$captive_portal_data1->unique_user;
								break;
							}
						}
					}
				}
			
             // to get unique user from temp table
              
                $captive_portal_temp_data = $this->DB2->query("SELECT count(DISTINCT(macid)) as unique_user, location_id
 FROM channel_unique_daily_user  WHERE location_id in($location_ids) 
 AND (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') group by location_id");
 if($captive_portal_temp_data->num_rows()> 0){
 foreach($captive_portal_temp_data->result() as $captive_portal_temp_data1){
	 
                foreach($data as $key=>$value) {
                    if($captive_portal_temp_data1->location_id == $value['id']){
                        $data[$key]['unique_user'] = $data[$key]['unique_user']+$captive_portal_temp_data1->unique_user;
						break;
                    }
                }
            }
 }
			// get the total opt send
				$otp_send_query = $this->DB2->query("select count(*) as total_otp_send, location_id from channel_wifi_user_otp where DATE(added_on) 
				BETWEEN '$start_date' AND '$end_date' AND location_id in ($location_ids) group by location_id");
                               if($otp_send_query->num_rows()> 0){
				 foreach($otp_send_query->result() as $otp_send_query1){
					foreach($data as $key=>$value) {
						if($otp_send_query1->location_id == $value['id']){
							$data[$key]['total_otp_send'] = $data[$key]['total_otp_send']+$otp_send_query1->total_otp_send;
							break;
						}
					}
				}
				}
			// get total otp verified
			$otp_verified_query = $this->DB2->query("select count(*) as total_otp_verified, location_id from channel_wifi_user_otp where DATE(added_on) 
				BETWEEN '$start_date' AND '$end_date' AND location_id in ($location_ids) AND is_verified = '1' group by location_id");
				if($captive_portal_temp_data->num_rows()> 0){
				 foreach($otp_verified_query->result() as $otp_verified_query1){
					foreach($data as $key=>$value) {
						if($otp_verified_query1->location_id == $value['id']){
							$data[$key]['total_otp_verified'] = $data[$key]['total_otp_verified']+$otp_verified_query1->total_otp_verified;
							break;
						}
					}
				}
				}
			// get total new user register
			$new_register_user_query = $this->DB2->query("select count(*) as total_new_user, location_id from channel_wifi_user_otp where DATE(added_on) 
				BETWEEN '$start_date' AND '$end_date' AND location_id in ($location_ids) AND new_user = '1' group by location_id");
				if($captive_portal_temp_data->num_rows()> 0){
				 foreach($new_register_user_query->result() as $new_register_user_query1){
					foreach($data as $key=>$value) {
						if($new_register_user_query1->location_id == $value['id']){
							$data[$key]['total_new_user'] = $data[$key]['total_new_user']+$new_register_user_query1->total_new_user;
							break;
						}
					}
				}
				}
            // get total session from cp table
			    $get_session = $this->DB2->query("SELECT COUNT(*) as total_session, location_id FROM wifi_user_free_session WHERE location_id IN($location_ids) AND
                                            DATE(session_date) BETWEEN '$start_date' AND '$end_date' group by location_id");
											if($captive_portal_temp_data->num_rows()> 0){
                foreach($get_session->result() as $get_session1){
					foreach($data as $key=>$value) {
						if($get_session1->location_id == $value['id']){
							$data[$key]['total_session'] = $data[$key]['total_session']+$get_session1->total_session;
							break;
						}
					}
				}
			}
			//get total session from mobile table
			$get_session_limited = $this->DB2->query("SELECT COUNT(*) as total_session_limited, location_id FROM
                wifi_user_session_info WHERE location_id IN($location_ids) AND DATE(session_date) BETWEEN '$start_date'
                AND '$end_date' group by location_id");
				if($captive_portal_temp_data->num_rows()> 0){
            foreach($get_session_limited->result() as $get_session_limited1){
					foreach($data as $key=>$value) {
						if($get_session_limited1->location_id == $value['id']){
							$data[$key]['total_session'] = $data[$key]['total_session']+$get_session_limited1->total_session_limited;
							break;
						}
					}
				}
				}
              
       }
         //echo "<pre>";print_r($data);die;
        return $data;
    }
    

   


    public function configure_microtic_router($jsondata){
     $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  
	  $is_dynamic = 0;
	  if(isset($jsondata->is_dynamic)){
	       $is_dynamic = $jsondata->is_dynamic;
	  }
	  
	  $router_type = strtolower($jsondata->router_type);
	  
        $router_id = $jsondata->router_id;
        $router_user = $jsondata->router_user;
        $router_password = $jsondata->router_password;
	$router_port = '';
	if(isset($jsondata->router_port)){
	  $router_port = $jsondata->router_port;
	}
        $location_name = $jsondata->location_name;
        $location_id = $jsondata->location_id;
        $cp_path = $jsondata->cp_path;
	$subdomain_ip = '';
	if(isset($jsondata->subdomain_ip)){
	  $subdomain_ip = $jsondata->subdomain_ip;
	}
	$secret = '';
	if(isset($jsondata->secret)){
	  $secret = $jsondata->secret;
	}else{
	  $secret = 'testing123';
	}
        $isp_uid = $jsondata->isp_uid;
       $main_ssid = '';
	if(isset($jsondata->main_ssid) && $jsondata->main_ssid != ''){
	  $main_ssid = $jsondata->main_ssid;
	}else{
	  $main_ssid = "SHOUUT_FREE_WIFI";
	}
	$guest_ssid = '';
	if(isset($jsondata->guest_ssid) && $jsondata->guest_ssid != ''){
	  $guest_ssid = $jsondata->guest_ssid;
	}else{
	  $guest_ssid = 'SHOUUT_FREE_GUEST_WIFI';
	}
       
       
	  if($router_type == 'wap'){
	      $data = $this->step_execute_wap($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);       
	  }elseif($router_type == 'wapac'){
		 $data = $this->step_execute_wapac($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }elseif($router_type == 'rb850gx2'){
	       $data = $this->step_execute_Rb850Gx2($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == 'rb941-2nd'){
	       $data = $this->step_execute_Rb941_2nD($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }elseif($router_type == '2011il'){
	       $data = $this->step_execute_2011il($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == 'ccr1036-8g-2s'){
	       $data = $this->step_execute_CCR1036_8G_2S($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == '921gs-5hpacd'){
	       $data = $this->step_execute_921gs_5hpacd($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == '750r2-gr3'){
	       $data = $this->step_execute_750r2_gr3($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == '952-ui-5ac2nd'){
	       $data = $this->step_execute_952_ui_5ac2nd($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == '450g'){
	       $data = $this->step_execute_450g($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == 'ccr_1009_7g_1c_1s_pc'){
	       $data = $this->CCR_1009_7G_1C_1S_PC($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
	  elseif($router_type == 'rb_1100'){
	       $data = $this->RB_1100($is_dynamic,$router_id, $router_user, $router_password,$router_port, $location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid);
	  }
        return 1;
    }
     public function mikrotik_path_whitelist($subdomain_ip,$conn){
	  //whitelist subdomain
	       if($subdomain_ip != ''){
		    $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$subdomain_ip","comment"=>"$subdomain_ip"));
	       }
	       $white_ip1 = WHITELIST_IP1;
	       $white_ip2 = WHITELIST_IP2;
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$white_ip2","comment"=>"$white_ip2"));
	       $walled_garden_ip_shouutcom = $conn->add("/ip/hotspot/walled-garden/ip",array("action"=>"accept", "disabled"=>"no", "server"=>"shouut_hotspot","dst-address"=>"$white_ip1","comment"=>"shouut.com"));
		
		$walled_garden_ip_pguat = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"pguat.paytm.com","comment"=>"pguat.paytm.com"));
		
		$walled_garden_ip_paytm_accounts = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"accounts-uat.paytm.com","comment"=>"accounts-uat.paytm.com"));
	
		
		$walled_garden_ip_secure_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure.paytm.in","comment"=>"secure.paytm.in"));
		
		$walled_garden_ip_static4_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static4.paytm.in","comment"=>"static4.paytm.in"));
		
		$walled_garden_ip_static1_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static1.paytm.in","comment"=>"static1.paytm.in"));
		
		$walled_garden_ip_static3_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static3.paytm.in","comment"=>"static3.paytm.in"));
		
		$walled_garden_ip_static2_paytm_in = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"static2.paytm.in","comment"=>"static2.paytm.in"));
		
		$walled_garden_ip_3dsecure_payseal_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"3dsecure.payseal.com","comment"=>"3dsecure.payseal.com"));
		
		$walled_garden_ip_secure4_arcot_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"secure4.arcot.com","comment"=>"secure4.arcot.com"));
		
		$walled_garden_giantshouut = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no","server"=>"shouut_hotspot","dst-host"=>"s3-ap-southeast-1.amazonaws.com", "comment"=>"s3-ap-southeast-1.amazonaws.com"));
		
		$walled_garden_cdn101_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cdn101.shouut.com", "comment"=>"cdn101.shouut.com"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"st2.dineout-cdn.co.in", "comment"=>"st2.dineout-cdn.co.in"));
		
		
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"fbcdn.net", "comment"=>"fbcdn.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"facebook.net", "comment"=>"facebook.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"maps.googleapis.com", "comment"=>"maps.googleapis.com"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"akamaihd.net", "comment"=>"akamaihd.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"connect.facebook.net", "comment"=>"connect.facebook.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"fbstatic-a.akamaihd.net", "comment"=>"fbstatic-a.akamaihd.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"m.facebood.com", "comment"=>"m.facebood.com"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"d392o9g87c202y.cloudfront.net", "comment"=>"d392o9g87c202y.cloudfront.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"cloudfront.net", "comment"=>"cloudfront.net"));
		$walled_garden_dineout_shouut_com = $conn->add("/ip/hotspot/walled-garden",array("action"=>"allow", "disabled"=>"no", "server"=>"shouut_hotspot","dst-host"=>"ads360network.in", "comment"=>"ads360network.in"));
		
     }
     // CI query changes
     public function step_execute_wap($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
		$Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
		if($Ip_address_get->num_rows() > 0){
			$row = $Ip_address_get->row_array();
			$Ip = $row['ip'];
			$Ip =  explode('.',$Ip);
			if($Ip[2] == '255'){
				$Ip[1]++;
				$Ip[2] =0;
			}
			else{
				$Ip[2]++;
			}
			$new_ip = implode('.',$Ip);
			$Ip1 =  explode('.',$new_ip);
			$Ip1[3] = 0;
			$new_address = implode('.',$Ip1);

			$Ip2 =  explode('.',$new_ip);
			$Ip2[3] = 10;
			$from = implode('.',$Ip2);
			$Ip3 =  explode('.',$new_ip);
			$Ip3[3] = 254;
			$to = implode('.',$Ip3);
			$poll_range = $from.'-'.$to;
		}
		else{
			$poll_range = "192.168.36.10-192.168.36.254";
			$new_ip = "192.168.36.1";
			$new_address = "192.168.36.0";
		}
		
		
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  // check router reset properly or not start
	       
	       
	  // check router reset properly or not end
	  //B.  tell user to reset router properly
	  //C. Main configuration:
	       //1. Setting ports:
	       $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	       $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	       $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	       $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
		
		//2. Setting Wireless:
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1", "mode" => "ap-bridge", "band" => "2ghz-b/g/n", "frequency" => "2412", "channel-width" => "20mhz", "radio-name" => "SHOUUT", "default-forwarding" => "no", "ssid" =>"$main_ssid", "name" => "shouut", "disabled"=> "no", "wireless-protocol"=> "802.11"));
	       
	       $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	       $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	      
	       //3. Setting up DHCP and Hotspot:
	       $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	       $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	       $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	       $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11"));
	       $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	       $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	       
	       //4. Setting up RADIUS server:
	       $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	       if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	       
	       
	       //5. Preparing for logs:
	       if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	       
	       $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	       $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	      
	       //6. Preparing access for first time login for users:
	       $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	       
	       //7. Securing the router:
	       $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	       $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
		$this->create_login_file($location_id, "WAP", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
		$this->create_alogin_file($location_id, "WAP", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
		$this->create_status_file($location_id, "WAP", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	       $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));


    }





     // CI query changes
     public function step_execute_wapac($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	 $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
		$Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
		if($Ip_address_get->num_rows() > 0){
			$row = $Ip_address_get->row_array();
			$Ip = $row['ip'];
			$Ip =  explode('.',$Ip);
			if($Ip[2] == '255'){
				$Ip[1]++;
				$Ip[2] =0;
			}
			else{
				$Ip[2]++;
			}
			$new_ip = implode('.',$Ip);
			$Ip1 =  explode('.',$new_ip);
			$Ip1[3] = 0;
			$new_address = implode('.',$Ip1);

			$Ip2 =  explode('.',$new_ip);
			$Ip2[3] = 10;
			$from = implode('.',$Ip2);
			$Ip3 =  explode('.',$new_ip);
			$Ip3[3] = 254;
			$to = implode('.',$Ip3);
			$poll_range = $from.'-'.$to;
		}
		else{
			$poll_range = "192.168.36.10-192.168.36.254";
			$new_ip = "192.168.36.1";
			$new_address = "192.168.36.0";
		}
		
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  // check router reset properly or not start
	       
	       
	  // check router reset properly or not end
	  //B.  tell user to reset router properly
	  //C. Main configuration:
	       //1. Setting ports:
	       $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	       $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	       $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	       $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
		
		//2. Setting Wireless:
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1","mode"=>"ap-bridge","band"=>"2ghz-b/g/n","frequency"=>"2412","channel-width"=>"20mhz","radio-name"=> "SHOUUT","default-forwarding" => "no",   "ssid" => "$main_ssid", "disabled"=>"no", "wireless-protocol"=> "802.11"));
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan2", "mode"=>"ap-bridge","band"=>"5ghz-a/n/ac","frequency"=>"2412","channel-width"=>"20/40mhz-Ce", "radio-name"=> "SHOUUT","default-forwarding"=> "no","ssid"=>"SHOUUT_FREE_HISPEED", "disabled"=>"no", "wireless-protocol"=> "802.11"));
	       
	       $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut", "disabled"=>"no"));
	       $interface_bridge_port = $conn->add("/interface/bridge/port",array("interface"=>"wlan1", "bridge"=>"shouut"));
	       $interface_bridge_port1 = $conn->add("/interface/bridge/port",array("interface"=>"wlan2", "bridge"=>"shouut"));
	       
	       $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	       $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	      
	       //3. Setting up DHCP and Hotspot:
	       $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	       $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	       $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	       $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	       $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	       $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	       
	       //4. Setting up RADIUS server:
	       $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	       if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	       
	       //5. Preparing for logs:
	       if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	       $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	       $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	      
	       //6. Preparing access for first time login for users:
	       $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	       
	       //7. Securing the router:
	       $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	       $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	       $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
		$this->create_login_file($location_id, "WAP", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
		$this->create_alogin_file($location_id, "WAP", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
		$this->create_status_file($location_id, "WAP", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	       $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));


    }


     // CI query changes
     public function step_execute_Rb850Gx2($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	 $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether5 = $conn->set("/interface/ethernet",array(".id"=>"ether5","master-port"=>"ether2"));
	  $interface_set_ether2 = $conn->set("/interface/ethernet",array(".id"=>"ether2","name"=>"shouut"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "Rb850Gx2", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "Rb850Gx2", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "Rb850Gx2", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
     }
	

     // CI query changes
     public function step_execute_Rb941_2nD($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	 $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1","mode"=>"ap-bridge","band"=>"2ghz-b/g/n","frequency"=>"2412","channel-width"=>"20mhz","radio-name"=> "SHOUUT","default-forwarding" => "no",   "ssid" => "$main_ssid", "disabled"=>"no", "wireless-protocol"=> "802.11"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut","disabled"=>"no"));
	  $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut","disabled"=>"no"));
	  $interface_bridge_port_wan1 = $conn->add("/interface/bridge/port",array("interface"=>"wlan1", "bridge"=>"shouut","disabled"=>"no"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "Rb941_2nD", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "Rb941_2nD", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "Rb941_2nD", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	   
	  
     }


     // CI query changes
     public function step_execute_2011il($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	 $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_set_ether7 = $conn->set("/interface/ethernet",array(".id"=>"ether7","master-port"=>"ether6"));
	  $interface_set_ether8 = $conn->set("/interface/ethernet",array(".id"=>"ether8","master-port"=>"ether6"));
	  $interface_set_ether9 = $conn->set("/interface/ethernet",array(".id"=>"ether9","master-port"=>"ether6"));
	  $interface_set_ether10 = $conn->set("/interface/ethernet",array(".id"=>"ether10","master-port"=>"ether6"));
	  $interface_set_ether6 = $conn->set("/interface/ethernet",array(".id"=>"ether6","name"=>"shouut"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "2011iL", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "2011iL", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "2011iL", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
     }



     // CI query changes
     public function step_execute_CCR1036_8G_2S($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip, $secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether3", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether4 = $conn->add("/interface/bridge/port",array("interface"=>"ether4", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether5 = $conn->add("/interface/bridge/port",array("interface"=>"ether5", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether6 = $conn->add("/interface/bridge/port",array("interface"=>"ether6", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether7 = $conn->add("/interface/bridge/port",array("interface"=>"ether7", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether8 = $conn->add("/interface/bridge/port",array("interface"=>"ether8", "bridge"=>"shouut", "disabled"=>"no"));
	
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "CCR1036-8G-2S", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "CCR1036-8G-2S", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "CCR1036-8G-2S", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
     }




     // CI query changes
     public function step_execute_921gs_5hpacd($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
		$Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
		if($Ip_address_get->num_rows() > 0){
			$row = $Ip_address_get->row_array();
			$Ip = $row['ip'];
			$Ip =  explode('.',$Ip);
			if($Ip[2] == '255'){
				$Ip[1]++;
				$Ip[2] =0;
			}
			else{
				$Ip[2]++;
			}
			$new_ip = implode('.',$Ip);
			$Ip1 =  explode('.',$new_ip);
			$Ip1[3] = 0;
			$new_address = implode('.',$Ip1);

			$Ip2 =  explode('.',$new_ip);
			$Ip2[3] = 10;
			$from = implode('.',$Ip2);
			$Ip3 =  explode('.',$new_ip);
			$Ip3[3] = 254;
			$to = implode('.',$Ip3);
			$poll_range = $from.'-'.$to;
		}
		else{
			$poll_range = "192.168.36.10-192.168.36.254";
			$new_ip = "192.168.36.1";
			$new_address = "192.168.36.0";
		}
		
		
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  // check router reset properly or not start
	       
	       
	  // check router reset properly or not end
	  //B.  tell user to reset router properly
	  //C. Main configuration:
	       //1. Setting ports:
	       $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	       $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	       $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	       $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
		
		//2. Setting Wireless:
	       $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1", "mode" => "ap-bridge", "band" => "5ghz-a/n/ac", "frequency" => "2412", "channel-width" => "20mhz", "radio-name" => "SHOUUT", "default-forwarding" => "no", "ssid" =>"$main_ssid", "name" => "shouut", "disabled"=> "no", "wireless-protocol"=> "802.11"));
	       
	       $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	       $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	      
	       //3. Setting up DHCP and Hotspot:
	       $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	       $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	       $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	       $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	       $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	       $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	       
	       //4. Setting up RADIUS server:
	       $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	       if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	       
	       //5. Preparing for logs:
	       if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	       $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	       $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	      
	       //6. Preparing access for first time login for users:
	       $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	       
	       //7. Securing the router:
	       $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	       $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	       
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	       $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
		$this->create_login_file($location_id, "921gs_5hpacd", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
		$this->create_alogin_file($location_id, "921gs_5hpacd", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
		$this->create_status_file($location_id, "921gs_5hpacd", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	       $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	       $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));


    }







   
     // CI query changes
     public function step_execute_750r2_gr3($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  /*$interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether5 = $conn->set("/interface/ethernet",array(".id"=>"ether5","master-port"=>"ether2"));
	  $interface_set_ether2 = $conn->set("/interface/ethernet",array(".id"=>"ether2","name"=>"shouut"));*/
	   $interface_set_ether3 = $conn->add("/interface/bridge",array("name"=>"shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=> "shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether3", "bridge"=> "shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether4", "bridge"=> "shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether5", "bridge"=> "shouut","disabled"=>"no"));
	  
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array( "html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	   //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "disabled"=>"no", "chain"=>"input", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53","action"=>"discard"));
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
	  // create file
	  $this->create_login_file($location_id, "750r2-gr3", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "750r2-gr3", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "750r2-gr3", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
     }

















     // CI query changes
     public function step_execute_952_ui_5ac2nd($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	   $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting Wireless:
	  $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan1","mode"=>"ap-bridge","band"=>"2ghz-b/g/n","frequency"=>"2412","channel-width"=>"20mhz","radio-name"=> "SHOUUT2","default-forwarding" => "no",   "ssid" => "$main_ssid", "disabled"=>"no"));
	  $interface_wireless = $conn->set("/interface/wireless",array(".id"=>"wlan2","mode"=>"ap-bridge","band"=>"5ghz-a/n/ac","frequency"=>"5745","channel-width"=>"20mhz","radio-name"=> "SHOUUT5","default-forwarding" => "no",   "ssid" => "$main_ssid", "disabled"=>"no"));
	  $interface_set_ether5 = $conn->set("/interface/ethernet",array(".id"=>"ether5","master-port"=>"ether2"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut","disabled"=>"no"));
	   $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut","disabled"=>"no"));
	  $interface_bridge_port_wan1 = $conn->add("/interface/bridge/port",array("interface"=>"wlan1", "bridge"=>"shouut","disabled"=>"no"));
	  $interface_bridge_port_wan1 = $conn->add("/interface/bridge/port",array("interface"=>"wlan2", "bridge"=>"shouut","disabled"=>"no"));
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	   $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	   //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"discard", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "952-ui-5ac2nd", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "952-ui-5ac2nd", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "952-ui-5ac2nd", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
	  
     }



















  
     // CI query changes
     public function step_execute_450g($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip,$secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  
	  //2. Setting LAN:
	  /*$interface_set_ether3 = $conn->set("/interface/ethernet",array(".id"=>"ether3","master-port"=>"ether2"));
	  $interface_set_ether4 = $conn->set("/interface/ethernet",array(".id"=>"ether4","master-port"=>"ether2"));
	  $interface_set_ether5 = $conn->set("/interface/ethernet",array(".id"=>"ether5","master-port"=>"ether2"));
	  $interface_set_ether2 = $conn->set("/interface/ethernet",array(".id"=>"ether2","name"=>"shouut"));*/
	   $interface_set_ether3 = $conn->add("/interface/bridge",array("name"=>"shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=> "shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether3", "bridge"=> "shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether4", "bridge"=> "shouut","disabled"=>"no"));
	  $interface_set_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether5", "bridge"=> "shouut","disabled"=>"no"));
	  
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array( "html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	   //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "disabled"=>"no", "chain"=>"input", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53","action"=>"discard"));
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
	  // create file
	  $this->create_login_file($location_id, "450g", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "450g", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "450g", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
     }


















     
     public function CCR_1009_7G_1C_1S_PC($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip, $secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  $ip_service_winbox = $conn->set("/ip/service",array(".id"=>"winbox","port"=>"8295"));
	  //2. Setting LAN:
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether3", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether4 = $conn->add("/interface/bridge/port",array("interface"=>"ether4", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether5 = $conn->add("/interface/bridge/port",array("interface"=>"ether5", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether6 = $conn->add("/interface/bridge/port",array("interface"=>"ether6", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether7 = $conn->add("/interface/bridge/port",array("interface"=>"ether7", "bridge"=>"shouut", "disabled"=>"no"));
	
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "CCR_1009_7G_1C_1S_PC", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "CCR_1009_7G_1C_1S_PC", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "CCR_1009_7G_1C_1S_PC", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
     }




    
     
     
     public function RB_1100($is_dynamic, $router_id, $router_user, $router_password,$router_port,$location_name, $location_id, $cp_path, $isp_uid,$subdomain_ip, $secret,$main_ssid, $guest_ssid){
	  $radius_ip = AUTH_SERVER_IP;
	  $new_ip = '';
	  $Ip_address_get = $this->DB2->query("select ip from wifi_router_ip ORDER BY id DESC ");
	  if($Ip_address_get->num_rows() > 0){
	       $row = $Ip_address_get->row_array();
	       $Ip = $row['ip'];
	       $Ip =  explode('.',$Ip);
	       if($Ip[2] == '255'){
		       $Ip[1]++;
		       $Ip[2] =0;
	       }
	       else{
		       $Ip[2]++;
	       }
	       $new_ip = implode('.',$Ip);
	       $Ip1 =  explode('.',$new_ip);
	       $Ip1[3] = 0;
	       $new_address = implode('.',$Ip1);
     
	       $Ip2 =  explode('.',$new_ip);
	       $Ip2[3] = 10;
	       $from = implode('.',$Ip2);
	       $Ip3 =  explode('.',$new_ip);
	       $Ip3[3] = 254;
	       $to = implode('.',$Ip3);
	       $poll_range = $from.'-'.$to;
	  }
	  else{
	       $poll_range = "192.168.36.10-192.168.36.254";
	       $new_ip = "192.168.36.1";
	       $new_address = "192.168.36.0";
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  //1. Setting ports:
	  $ip_service_www = $conn->set("/ip/service",array(".id"=>"www","port"=>"8888"));
	  $ip_service_ssh = $conn->set("/ip/service",array(".id"=>"ssh","port"=>"2222"));
	  $ip_service_telnet = $conn->set("/ip/service",array(".id"=>"telnet","disabled"=>"yes"));
	  $ip_service_ftp = $conn->set("/ip/service",array(".id"=>"ftp","port"=>"2210"));
	  $ip_service_winbox = $conn->set("/ip/service",array(".id"=>"winbox","port"=>"8295"));
	  //2. Setting LAN:
	  $interface_bridge = $conn->add("/interface/bridge",array("name"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether2 = $conn->add("/interface/bridge/port",array("interface"=>"ether2", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether3 = $conn->add("/interface/bridge/port",array("interface"=>"ether3", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether4 = $conn->add("/interface/bridge/port",array("interface"=>"ether4", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether5 = $conn->add("/interface/bridge/port",array("interface"=>"ether5", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether6 = $conn->add("/interface/bridge/port",array("interface"=>"ether6", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether7 = $conn->add("/interface/bridge/port",array("interface"=>"ether7", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether8 = $conn->add("/interface/bridge/port",array("interface"=>"ether8", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether9 = $conn->add("/interface/bridge/port",array("interface"=>"ether9", "bridge"=>"shouut", "disabled"=>"no"));
	  $interface_bridge_port_ether10 = $conn->add("/interface/bridge/port",array("interface"=>"ether10", "bridge"=>"shouut", "disabled"=>"no"));
	
	  $ip_address = $conn->add("/ip/address",array("interface"=>"shouut","address"=>$new_ip."/24"));
	  $ip_dns_server = $conn->set("/ip/dns",array("allow-remote-requests" => "yes","servers"=>"8.8.8.8"));
	  
	  //3. Setting up DHCP and Hotspot:
	  $ip_pool = $conn->add("/ip/pool",array("name"=>"public","ranges"=>"$poll_range"));
	  $ip_dhcp_server_network = $conn->add("/ip/dhcp-server/network",array("address"=>$new_address."/24", "gateway"=>"$new_ip","dns-server"=>"$new_ip"));
	  $ip_dhcp_server = $conn->add("/ip/dhcp-server",array("name" => "shouut", "interface"=>"shouut", "address-pool"=>"public" , "lease-time"=>"15m", "disabled"=>"no"));
	  $ip_hotspot_profile = $conn->add("/ip/hotspot/profile",array("html-directory" =>"flash/hotspot","name"=>"shouut_hsprof", "hotspot-address"=>"$new_ip","login-by"=>"http-chap,http-pap,https,cookie","http-cookie-lifetime"=>"1d","use-radius"=>"yes","radius-accounting"=>"yes", "radius-interim-update"=>"1m", "nas-port-type"=>"wireless-802.11",));
	  $ip_hotspot = $conn->add("/ip/hotspot",array("name"=>"shouut_hotspot","interface"=>"shouut","address-pool"=>"public","disabled"=>"no",   "profile"=>"shouut_hsprof", "idle-timeout"=>"5m","keepalive-timeout"=>"1m", "addresses-per-mac"=>"5"));
	  $ip_hotspot_profile = $conn->set("/ip/hotspot/user/profile",array(".id" => "default", "shared-users" => "5"));
	  
	  //4. Setting up RADIUS server:
	  $radius_incoming_port = $conn->set("/radius/incoming",array("accept"=>"true","port"=>"3799"));
	  if($is_dynamic == '1'){
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"172.16.22.235","secret"=>"$secret","timeout"=>"2000ms"));    
	       }else{
		    $radius_service = $conn->add("/radius",array("service"=>"hotspot","disabled"=>"no","address"=>"$radius_ip","secret"=>"$secret","timeout"=>"2000ms"));
	       }
	  
	  //5. Preparing for logs:
	  if($is_dynamic == '1'){
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"172.16.22.235"));
	       }else{
		    $system_logging_action = $conn->add("/system/logging/action",array("name"=>"shouutServer", "target"=>"remote","remote"=>"$radius_ip", "remote-port"=>"514"));
	       }
	  $system_logging_topic = $conn->add("/system/logging",array("topics"=>"firewall", "action"=>"shouutServer"));
	  $system_logging_topic_set_0 = $conn->set("/system/logging",array(".id"=>"*1","topics"=>"info,!firewall"));
	  
	  //6. Preparing access for first time login for users:
	  $this->mikrotik_path_whitelist($subdomain_ip,$conn);
	  
	  //7. Securing the router:
	  $ip_firewall_addresslist_add = $conn->add("/ip/firewall/address-list", array("list"=>"shouut_ip", "address"=>$new_address."/24"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_tcp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"tcp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  $ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"shouut_log_udp", "disabled"=>"no", "chain"=>"forward", "src-address-list"=>"shouut_ip", "protocol"=>"udp", "connection-state"=>"new","action"=>"log", "log"=>"yes"));
	  //$ip_firewall_filter = $conn->add("/ip/firewall/filter",array("comment"=>"deny_dns_from_unknown", "src-address-list"=>"!shouut_ip", "protocol"=>"udp", "port"=>"53", "action"=>"drop", "chain"=>"input","disabled"=>"no"));	
	  $idle_time_out = $conn->add("/ip/firewall/nat",array("comment"=>"NAT_shouut_ip", "chain"=>"srcnat", "action"=>"masquerade", "disabled"=>"no", "src-address-list"=>"shouut_ip"));
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"shouutadmin", "password"=>"sH00ut","group"=>"full"));
	  $insert_data = array(
		    'ip' => $new_ip,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_ip = $this->DB2->insert('wifi_router_ip', $insert_data);
		// create file
	  $this->create_login_file($location_id, "RB_1100", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_alogin_file($location_id, "RB_1100", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
	  $this->create_status_file($location_id, "RB_1100", $cp_path, $isp_uid, $router_id, $router_user, $router_password);
                
	       //9. User Permission
	  $user_add = $conn->add("/user",array("disabled"=>"no", "name"=>"ispadmin", "password"=>"ispadmin","group"=>"full"));
	  $user_admin_disable = $conn->set("/user",array(".id"=>"admin","disabled"=>"yes"));
	  
     }




    
     
    
    // CI query changes
     public function create_login_file($locid, $router_type, $cp_path, $isp_uid, $router_id, $router_user, $router_password){
		// Get isp logo from path
		
                $isp_logo_path = '';
               $this->db->select('logo_image')->from('sht_isp_detail');
	       $this->db->where(array('status'=> '1', 'isp_uid'=>$isp_uid));
	       $query = $this->db->get();
                if($query->num_rows() > 0){
                        $rowdata = $query->row();
                        $isp_logo =  $rowdata->logo_image;
                        $server_isp_image_path = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_logo/';
                    if($isp_logo!=''){
                            $isp_logo_path =  $server_isp_image_path."logo/".$isp_logo;
                    }
                }
                if($isp_logo_path ==''){
                    $isp_logo_path = 'img/provider_logo.jpg';
                }
		// get cp_type
                $cp_type = 'cp1';
		if($isp_logo_path != ''){
		    $brand_logo = $isp_logo_path;
		}
		else{
		    $brand_logo = 'https://www.shouut.com/isp_consumer_api/routerfile/shouut_cp2_logo.png';
		}
		
	       $this->DB2->select('cp_type , original_image,logo_image')->from('wifi_location_cptype');
	       $this->DB2->where(array('location_id'=> $locid, 'isp_uid'=>$isp_uid));
	       $get_cp_type = $this->DB2->get();
                if($get_cp_type->num_rows() > 0){
                    $row = $get_cp_type->row_array();
                    $cp_type = $row['cp_type'];
		    if($row['logo_image'] != ''){
			$brand_logo = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_location_logo/logo/'.$row['logo_image'];
		    }elseif($row['original_image'] != ''){
			$brand_logo  = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_location_logo/original/'.$row['original_image'];
		    }
                }
		
                if($cp_type == 'cp2' || $cp_type == 'cp3'){
                    $content_login = '

                    <html>
                       <head>
                          <title>login</title>
                           <meta charset="utf-8">
                          <meta http-equiv="X-UA-Compatible" content="IE=edge">
                          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
                          <title>HOTSPOT </title>
                          
                          <link href="css/bootstrap.min.css" rel="stylesheet">
                          <link href="css/material-design.css" rel="stylesheet">
                          <link href="css/ripples.min.css" rel="stylesheet">
                          <link href="css/style.css" rel="stylesheet">
                          <link href="css/font-awesome.min.css" rel="stylesheet">
                       </head>
                       <body onload = "getMobileOperatingSystem()">
                          $(if chap-id)
                          <noscript>
                             <center><b>JavaScript required. Enable JavaScript to continue.</b></center>
                          </noscript>
                          $(endif)
                             <form name="redirect" action="'.$cp_path.'" method="post">
                                    <input type="hidden" name="mobileno" id="mobileno" value="">
                                    <input type="hidden" name="device" id = "device" value="">
                                    <input type="hidden" name="mac" value="$(mac)">
                                    <input type="hidden" name="ip" value="$(ip)">
                                    <input type="hidden" name="username" value="$(username)">
                                    <input type="hidden" name="link-login" value="$(link-login)">
                                    <input type="hidden" name="link-orig" value="$(link-orig)">
                                    <input type="hidden" name="error" value="$(error)">
                                    <input type="hidden" name="trial" value="$(trial)">
                                    <input type="hidden" name="chap-id" value="$(chap-id)">
                                    <input type="hidden" name="chap-challenge" value="$(chap-challenge)">
                                    <input type="hidden" name="link-login-only" value="$(link-login-only)">
                                    <input type="hidden" name="link-orig-esc" value="$(link-orig-esc)">
                                    <input type="hidden" name="mac-esc" value="$(mac-esc)">
                                    <input type="hidden" name="identity" value="$(identity)">
                                    <input type="hidden" name="bytes-in-nice" value="$(bytes-in-nice)">
                                    <input type="hidden" name="bytes-out-nice" value="$(bytes-out-nice)">
                                    <input type="hidden" name="session-time-left" value="$(session-time-left)">
                                    <input type="hidden" name="uptime" value="$(uptime)">
                                    <input type="hidden" name="refresh-timeout" value="$(refresh-timeout)">
                                    <input type="hidden" name="link-status" value="$(link-status)">
                                    <!-- Location for dynamic advertisement -->
                                    <input type="hidden" name="shouut-location" value="'.$locid.'">
                                    <input type="hidden" name="isp_uid" value="'.$isp_uid.'">
                                    <input type="hidden" id = "user" name="user" value="">
                                    <input type="hidden" id = "pwd" name="pwd" value="">
                                    <input type="submit" value="continue" style="display:none">
                                    <div class="container-fluid" id="mob-screen">
                                       <div class="row">
                                          <div class="container">
                                             <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-md-offset-3" >
                                                   <div class="row" >
                                                      <header class="header">
                                                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="logo">
                                                               <center><img src="'.$brand_logo.'" class="img-responsive" alt="logo" /></center>
                                                            </div>
                                                         </div>
                                                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <form class="form-horizontal">
                                                               <div class="form-group">
                                                                  <label>GET IN FOR FREE WIFI & MORE</label>
                                                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="input-group">
                                                                       <div class="input-group-btn search-panel" style="position: relative">
                                                                          <button type="button" class="btn btn-raised">
                                                                          <span>+91</span>
                                                                          </button>
                                                                       </div>
                                                                       <input type="tel" class="form-control" id="usermobileno" placeholder="10 digit mobile no." onBlur="this.placeholder=\'Enter 10 digit mobile no.\'" onFocus="this.placeholder=\'\'" autocomplete="off" required style="margin-left:-1px;">
                                                                    </div>
                                                                    <label id="mobile_err" style="color: #f00;font-weight:normal;float:left;"></label>
                                                                 </div>
                                                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                     <label class="labels">OTP will be sent over SMS for verification</label>
                                                                  </div>
                                                               </div>
                                                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <input type="submit" class="btn btn-danger btn-lg" onclick="return validate_mobile()" value="GET WI-FI" id="getwifibtn" />
                                                                    <center style="margin: 10px 1px;" id="getwifiloader" class="hide"><img src="http://d392o9g87c202y.cloudfront.net/loader/loader.gif" alt="" class="img-responsive" width="10%"></center>
                                                               </div>
                                                               <div class="form-group">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <h4>SERVICE PROVIDED BY</h4>
                                                                            <div class="col-sm-12">
                                                                            <div class="col-sm-3 col-xs-2"></div>
                                                                            <div class="col-sm-6 col-xs-8">
                                                                                    <div class="col-sm-12 col-xs-12"> 
                                                                                            <center><img src="'.$isp_logo_path.'" class="img-responsive"/></center>
                                                                                    </div>
                                                                            </div>
                                                                            <div class="col-sm-3 col-xs-2"></div></div>
                                                                    </div>
                                                               </div>
                                                            </form>
                                                         </div>
                                                      </header>
                                                      <footer id="footer">
                                                         <div class="sub_header">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                               <h1>EARN FREE WIFI DATA</h1>
                                                               <h4>To earn data on  Networks, sign up now<br/>and do what you love!</h4>
                                                               <h2>You can use the earned data at these</h2>
                                                               <h3>Wi-Fi Locations & ISP Partners</h3>
                                                            </div>
                                                         </div>
                                                      </footer>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                    
                    
                                    
                             </form>
                             <script type="text/javascript" src="js/jquery.min.js"></script>
                            <script type="text/javascript" src="js/bootstrap.min.js"></script>
                            <script type="text/javascript" src="js/material.min.js"></script>
                            <script type="text/javascript" src="js/ripples.min.js"></script>
                            <script type="text/javascript" src="js/login-validation.js"></script>
                            <script type="text/javascript">
                               $(document).ready(function() {
                                  var height = $(window).height();
                                  $(\'#main_div\').css(\'height\', height);
                                  $(\'#web_fluid\').css(\'height\', height);
                               });
                               
                               function getMobileOperatingSystem() {
                                  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                                  //alert(userAgent);
                                  if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ){
                                     document.getElementById(\'device\').value = \'Apple Phone\';
                                  }else if( userAgent.match( /Mac68K/i ) || userAgent.match( /MacPPC/i ) || userAgent.match( /MacIntel/i ) ){
                                     document.getElementById(\'device\').value = \'Apple Laptop\';
                                  }else if( userAgent.match( /Android/i ) ){
                                     document.getElementById(\'device\').value = \'Android Phone\';
                                  }else if(navigator.userAgent.match(/Windows Phone/i)){
                                     document.getElementById(\'device\').value = \'Windows Phone\';
                                  }else{
                                     document.getElementById(\'device\').value = \'Windows Desktop\';
                                  }
                               }
                            </script>
                       </body>
                    </html>'; 
                }
		else{
                                            $content_login = '<html>
                        <head><title>login</title>
                        <script type="text/javascript">
                        function getParameterByName(name, url) {
                                if (!url) url = window.location.href;
                                name = name.replace(/[\[\]]/g, "\$&");
                                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                                        results = regex.exec(url);
                                if (!results) return null;
                                if (!results[2]) return \'\';
                                return decodeURIComponent(results[2].replace(/\+/g, " "));
                        }
                        
                        function getMobileOperatingSystem() {
                        
                        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                        
                        //alert(userAgent);
                          if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
                          {
                          document.getElementById(\'device\').value = \'Apple Phone\';
                          }
                          else if( userAgent.match( /Mac68K/i ) || userAgent.match( /MacPPC/i ) || userAgent.match( /MacIntel/i ) )
                          {
                          document.getElementById(\'device\').value = \'Apple Laptop\';
                          }
                          else if( userAgent.match( /Android/i ) )
                          {
                          document.getElementById(\'device\').value = \'Android Phone\';
                          }
                          else if(navigator.userAgent.match(/Windows Phone/i)){
                                document.getElementById(\'device\').value = \'Windows Phone\';
                         }
                          else
                          {
                        
                          document.getElementById(\'device\').value = \'Windows Desktop\';
                          }
                        document.redirect.submit();
                            /*var count=6;
                            var counter = setInterval(function(){
                                                count=count-1;
                                                //document.getElementById(\'timer\').innerHTML = count;
                                                if (count <= 0){
                                count = 6;
                                                  document.redirect.submit();
                                                }else{
                                                 if (count == 3) {
                                                     document.getElementById(\'sesstimer\').innerHTML = \'Starting a session. Almost Done!\';
                                                 }
                                                }
                           }, 1000);*/
                        
                        }
                        
                        function submitform() {
                           document.redirect.submit();
                        }
                        
                        </script>
                        <style type="text/css">
                         body{font-family: \'Open Sans\', sans-serif;}
                         .container{ margin:0px auto;height:100%;border-collapse:collapse;display : table;}
                         .logo{width:100%; text-align:center;}
                         .session{width:100%; text-align:center; margin-top:50px;font-size:16px;color:#263646}
                         .sessionloader{width:100%; text-align:center; margin-top:50px;}
                         .footer{display : table-row;vertical-align : bottom;height : 1px;bottom:0px;font-size:16px;text-align:center;color:#808285}
                        </style>
                        
                        
                        </head>
                        <body onload = "getMobileOperatingSystem()">
                        
                        <div class="container">
                           <div class="logo"><img src="'.$brand_logo.'" alt="" width="50%" /></div>
                           <div class="session" id="sesstimer">Trying to get you online! This may take a moment.</div>
                           <div class="sessionloader"><img src="http://d392o9g87c202y.cloudfront.net/loader/loader.gif" alt="" width="10%"/></div>
                           <div id="timer"></div>
                           <div class="footer">
                              <div style="padding:20px 0px">In case you are not re-directed automatically in 10 seconds, <span style="color:#27aae1;cursor:pointer;text-decoration:underline;" onclick="submitform()">Click Here</span></div>
                           </div>
                        </div>
                        
                        $(if chap-id)
                        <noscript>
                        <center><b>JavaScript required. Enable JavaScript to continue.</b></center>
                        </noscript>
                        $(endif)
                        <center>
                        <form name="redirect" action="'.$cp_path.'" method="post">
                        
                        <input type="hidden" name="device" id = "device" value="">
                        <input type="hidden" name="mac" value="$(mac)">
                        <input type="hidden" name="ip" value="$(ip)">
                        <input type="hidden" name="username" value="$(username)">
                        <input type="hidden" name="link-login" value="$(link-login)">
                        <input type="hidden" name="link-orig" value="$(link-orig)">
                        <input type="hidden" name="error" value="$(error)">
                        <input type="hidden" name="trial" value="$(trial)">
                        <input type="hidden" name="chap-id" value="$(chap-id)">
                        <input type="hidden" name="chap-challenge" value="$(chap-challenge)">
                        <input type="hidden" name="link-login-only" value="$(link-login-only)">
                        <input type="hidden" name="link-orig-esc" value="$(link-orig-esc)">
                        <input type="hidden" name="mac-esc" value="$(mac-esc)">
                        <input type="hidden" name="identity" value="$(identity)">
                        <input type="hidden" name="bytes-in-nice" value="$(bytes-in-nice)">
                        <input type="hidden" name="bytes-out-nice" value="$(bytes-out-nice)">
                        <input type="hidden" name="session-time-left" value="$(session-time-left)">
                        <input type="hidden" name="uptime" value="$(uptime)">
                        <input type="hidden" name="refresh-timeout" value="$(refresh-timeout)">
                        <input type="hidden" name="link-status" value="$(link-status)">
                        
                        <!-- Location for dynamic advertisement -->
                        
                        <input type="hidden" name="shouut-location" value="'.$locid.'">
                        <input type="hidden" name="isp_uid" value="'.$isp_uid.'">
                        <input type="hidden" id = "user" name="user" value="">
                        <input type="hidden" id = "pwd" name="pwd" value="">
                        
                        
                        <input type="submit" value="continue" style="display:none">
                        </form>
                        <script language="JavaScript">
                        var user = getParameterByName(\'user\');
                        document.getElementById(\'user\').value = user;
                        var pwd = getParameterByName(\'pwd\');
                        document.getElementById(\'pwd\').value = pwd;
                        
                        //document.redirect.submit();
                        </script></center>
                        </body>
                        </html>';
                }
		

		$open = fopen(ROUTERFILEPATH."routerfile/login.html","w+");
		
		fwrite($open, $content_login);
		fclose($open);
		$source_file = ROUTERFILEPATH.'routerfile/login.html';
		//$source_file = './routerfile/login.html';
		
		/*if($router_type == "921gs_5hpacd" || $router_type == "Rb850Gx2" || $router_type == 'Rb941_2nD'|| $router_type == '2011iL' || $router_type == 'CCR1036-8G-2S'){
			$remote_file = 'hotspot/login.html';
			$dst_dir = '/hotspot/';
		}
		elseif($router_type == "WAP" || $router_type == "750r2-gr3" || $router_type == "952-ui-5ac2nd"){
		  
		    $remote_file = 'flash/hotspot/login.html';
		    $dst_dir = '/flash/hotspot/';
			
		}*/
		


		$ftp_server = $router_id;
		$ftp_user_name = $router_user;
		$ftp_user_pass = $router_password;
		//$conn_id = ftp_connect($ftp_server)or die("Unable to connect to server login.");
		$conn_id = ftp_connect($ftp_server, '2210')or die("Unable to connect to server login.");
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	       ftp_pasv($conn_id, true);
	       
	       
	       
	       // create folder
	       $dst_dir = '/flash/';
	       if (ftp_nlist($conn_id, $dst_dir) == false) {
		    ftp_mkdir($conn_id, $dst_dir);
	       }
	       $dst_dir = '/flash/hotspot/';
	       if (ftp_nlist($conn_id, $dst_dir) == false) {
		    ftp_mkdir($conn_id, $dst_dir);
	       }
	       $remote_file = 'flash/hotspot/login.html';
	       
	       
		ftp_put($conn_id, $remote_file, $source_file, FTP_ASCII);
	      
	       if($cp_type != 'cp1' && $cp_type != 'cp_truck_stop' &&  $cp_type != 'cp4'){
		    // for folter copy
		    $src_dir = ROUTERFILEPATH."routerfile/cp2html/";
		     $this->load->library('ftp');
		    $config['hostname'] = $router_id;
		    $config['username'] = $router_user;
		    $config['password'] = $router_password;
		    $config['port']     = 2210;
		    $config['debug']        = TRUE;
		    $this->ftp->connect($config);
		    $this->ftp->mirror($src_dir."css/", $dst_dir."css/");
		    $this->ftp->mirror($src_dir."img/", $dst_dir."img/");
		    $this->ftp->mirror($src_dir."js/", $dst_dir."js/");
		// for folder copy end
	       }
	       // copy extra files
	       $src_dir = ROUTERFILEPATH."routerfile/extrafiles/";
	       $this->load->library('ftp');
	       $config['hostname'] = $router_id;
	       $config['username'] = $router_user;
	       $config['password'] = $router_password;
	       $config['port']     = 2210;
	       $config['debug']        = TRUE;
	       $this->ftp->connect($config);
	       $this->ftp->mirror($src_dir, $dst_dir);
		
		ftp_close($conn_id);
		
     }
     // CI query changes
     public function create_alogin_file($locid, $router_type, $cp_path, $isp_uid, $router_id, $router_user, $router_password){
		$cp_path =  str_replace("login.php","alogin.php",$cp_path);
		$content_alogin = '<html>
			<head><title>...</title></head>
			<body>
			$(if chap-id)
			<noscript>
			<center><b>JavaScript required. Enable JavaScript to continue.</b></center>
			</noscript>
			$(endif)
			<center>If you are not redirected in a few seconds, click \'continue\' below<br>

			<form name="redirect" action="'.$cp_path.'" method="post">

			<input type="hidden" name="mac" value="$(mac)">
			<input type="hidden" name="ip" value="$(ip)">
			<input type="hidden" name="username" value="$(username)">
			<input type="hidden" name="link-login" value="$(link-login)">
			<input type="hidden" name="link-orig" value="$(link-orig)">
			<input type="hidden" name="error" value="$(error)">
			<input type="hidden" name="chap-id" value="$(chap-id)">
			<input type="hidden" name="chap-challenge" value="$(chap-challenge)">
			<input type="hidden" name="link-login-only" value="$(link-login-only)">
			<input type="hidden" name="link-orig-esc" value="$(link-orig-esc)">
			<input type="hidden" name="mac-esc" value="$(mac-esc)">
			<input type="hidden" name="identity" value="$(identity)">
			<input type="hidden" name="bytes-in-nice" value="$(bytes-in-nice)">
			<input type="hidden" name="bytes-out-nice" value="$(bytes-out-nice)">
			<input type="hidden" name="session-time-left" value="$(session-time-left)">
			<input type="hidden" name="uptime" value="$(uptime)">
			<input type="hidden" name="refresh-timeout" value="$(refresh-timeout)">
			<input type="hidden" name="link-status" value="$(link-status)">

			<!-- Location for dynamic advertisement -->

			<input type="hidden" name="shouut-location" value="'.$locid.'">
			<input type="hidden" name="isp_uid" value="'.$isp_uid.'">
			<input type="submit" value="continue">
			</form>
			<script language="JavaScript">
			   document.redirect.submit();
			</script>
			</center>
			</body>
			</html>';

		$open = fopen(ROUTERFILEPATH."routerfile/alogin.html","w+");
		
		fwrite($open, $content_alogin);
		fclose($open);
		$source_file = ROUTERFILEPATH.'routerfile/alogin.html';
		/*if($router_type == "921gs_5hpacd" || $router_type == "Rb850Gx2" || $router_type == 'Rb941_2nD' || $router_type == '2011iL' || $router_type == 'CCR1036-8G-2S'){
			$remote_file = 'hotspot/alogin.html';
		}
		elseif($router_type == "WAP" || $router_type == "750r2-gr3" || $router_type == "952-ui-5ac2nd"){
			$remote_file = 'flash/hotspot/alogin.html';
		}*/
		
		$remote_file = 'flash/hotspot/alogin.html';
		
		$ftp_server = $router_id;
		$ftp_user_name = $router_user;
		$ftp_user_pass = $router_password;
		// set up basic connection
		
		//$conn_id = ftp_connect($ftp_server)or die("Unable to connect to server login.");
		$conn_id = ftp_connect($ftp_server, '2210')or die("Unable to connect to server login.");
		
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

		ftp_pasv($conn_id, true);

		ftp_put($conn_id, $remote_file, $source_file, FTP_ASCII);

		ftp_close($conn_id);

	}
        
     // CI query changes
     public function create_status_file($locid, $router_type, $cp_path, $isp_uid, $router_id, $router_user, $router_password){
		$cp_path =  str_replace("login.php","status.php",$cp_path);
		$content_status = '<html>
				<head><title>status</title>
				<script type="text/javascript">
				function getMobileOperatingSystem() {

						var userAgent = navigator.userAgent || navigator.vendor || window.opera;

						//alert(userAgent);
						  if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
						  {
						  document.getElementById(\'device\').value = \'Apple Phone\';
						  }
						  else if( userAgent.match( /Mac68K/i ) || userAgent.match( /MacPPC/i ) || userAgent.match( /MacIntel/i ) )
						  {
						  document.getElementById(\'device\').value = \'Apple Laptop\';
						  }
						  else if( userAgent.match( /Android/i ) )
						  {
						  document.getElementById(\'device\').value = \'Android Phone\';
						  }
						  else if(navigator.userAgent.match(/Windows Phone/i)){
							document.getElementById(\'device\').value = \'Windows Phone\';
						 }
						  else
						  {

						  document.getElementById(\'device\').value = \'Windows Desktop\';
						  }
						  document.redirect.submit();
				}
</script>
				</head>
				<body onload = "getMobileOperatingSystem()">
				$(if chap-id)
				<noscript>
				<center><b>JavaScript required. Enable JavaScript to continue.</b></center>
				</noscript>
				$(endif)
				<center>If you are not redirected in a few seconds, click \'continue\' below<br>

				<form name="redirect" action="'.$cp_path.'" method="post">
                <input type="hidden" name="device" id = "device" value="">
				<input type="hidden" name="mac" value="$(mac)">
				<input type="hidden" name="ip" value="$(ip)">
				<input type="hidden" name="username" value="$(username)">
				<input type="hidden" name="link-login" value="$(link-login)">
				<input type="hidden" name="link-orig" value="$(link-orig)">
				<input type="hidden" name="error" value="$(error)">
				<input type="hidden" name="trial" value="$(trial)">
				<input type="hidden" name="loginby" value="$(loginby)">
				<input type="hidden" name="chap-id" value="$(chap-id)">
				<input type="hidden" name="chap-challenge" value="$(chap-challenge)">
				<input type="hidden" name="link-login-only" value="$(link-login-only)">
				<input type="hidden" name="link-orig-esc" value="$(link-orig-esc)">
				<input type="hidden" name="mac-esc" value="$(mac-esc)">

				<input type="hidden" name="identity" value="$(identity)">
				<input type="hidden" name="bytes-in-nice" value="$(bytes-in-nice)">
				<input type="hidden" name="bytes-out-nice" value="$(bytes-out-nice)">
				<input type="hidden" name="session-time-left" value="$(session-time-left)">
				<input type="hidden" name="uptime" value="$(uptime)">
				<input type="hidden" name="refresh-timeout" value="$(refresh-timeout)">
				<input type="hidden" name="link-status" value="$(link-status)">
				<!-- Location for dynamic advertisement -->

				<input type="hidden" name="shouut-location" value="'.$locid.'">
				<input type="hidden" name="isp_uid" value="'.$isp_uid.'">
				<input type="submit" value="continue">
				</form>
				<script language="JavaScript">
				   //document.redirect.submit();
				</script></center>
				</body>
				</html>';

		$open = fopen(ROUTERFILEPATH."routerfile/status.html","w+");
		
		fwrite($open, $content_status);
		fclose($open);
		$source_file = ROUTERFILEPATH.'routerfile/status.html';
		/*if($router_type == "921gs_5hpacd" || $router_type == "Rb850Gx2" || $router_type == 'Rb941_2nD'|| $router_type == '2011iL' || $router_type == 'CCR1036-8G-2S'){
			$remote_file = 'hotspot/status.html';
		}
		elseif($router_type == "WAP" || $router_type == "750r2-gr3" || $router_type == "952-ui-5ac2nd"){
			$remote_file = 'flash/hotspot/status.html';
		}*/
	       $remote_file = 'flash/hotspot/status.html';
		
		$ftp_server = $router_id;
		$ftp_user_name = $router_user;
		$ftp_user_pass = $router_password;
		// set up basic connection
	       //$conn_id = ftp_connect($ftp_server)or die("Unable to connect to server login.");
		$conn_id = ftp_connect($ftp_server, '2210')or die("Unable to connect to server login.");
		
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

		ftp_pasv($conn_id, true);

		ftp_put($conn_id, $remote_file, $source_file, FTP_ASCII);

		ftp_close($conn_id);

	}
	
	
	
     public function reset_router($jsondata){
	  $router_type = strtolower($jsondata->router_type);
	  $router_id = $jsondata->router_id;
	  $router_user = $jsondata->router_user;
	  $router_password = $jsondata->router_password;
	  $router_port = '';
	  if(isset($jsondata->router_port)){
	       $router_port = $jsondata->router_port;
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  $reset_router = $conn->set("/system/reset-configuration",array("no-defaults"=>"yes", "skip-backup"=>"yes"));
	  return 1;
     }
     public function check_router_selected_matched($jsondata){
	  
	  $router_type = strtolower($jsondata->router_type);
	  $router_id = $jsondata->router_id;
	  $router_user = $jsondata->router_user;
	  $router_password = $jsondata->router_password;
	  $router_port = '';
	  if(isset($jsondata->router_port)){
	       $router_port = $jsondata->router_port;
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  // check router type select and connected are match
	  $check_router_type = $conn->getall("/system/routerboard");
	  //print_r($check_router_type);
	  if(isset($check_router_type['model'])){
	       if($router_type == 'wap'){
		    if($check_router_type['model'] == 'RouterBOARD wAP 2nD' || $check_router_type['model'] == 'RouterBOARD wAP 2nD r2'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == 'wapac'){
		    if($check_router_type['model'] == 'RouterBOARD wAP G-5HacT2HnD'){
			 
		    }else{
			 return 3;
		    }
	       }elseif($router_type == 'rb850gx2'){
		    if($check_router_type['model'] == '850Gx2'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == 'rb941-2nd'){
		    if($check_router_type['model'] == '941-2nD' || $check_router_type['model'] == 'RouterBOARD 941-2nD'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == '2011il' ){
		    if($check_router_type['model']  == '2011L' || $check_router_type['model'] == '2011UiAS-2HnD'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == 'ccr1036-8g-2s'){
		    if($check_router_type['model'] == 'CCR1036-8G-2S+' || $check_router_type['model'] == 'CCR1036-12G-4S'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == '921gs-5hpacd'){
		    
		    if($check_router_type['model'] == '921GS-5HPacD r2'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == '750r2-gr3'){
		    
		    if( $check_router_type['model'] == 'RouterBOARD 750G r3' || $check_router_type['model'] == 'RouterBOARD 750 r2' || $check_router_type['model'] == 'RouterBOARD 750UP r2' || $check_router_type['model'] == '750' || $check_router_type['model'] == '750GL' || $check_router_type['model'] == 'RB750Gr3'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == '952-ui-5ac2nd'){
		    
		    if($check_router_type['model'] == 'RouterBOARD 952Ui-5ac2nD'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == '450g'){
		    
		    if($check_router_type['model'] == '450G'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == 'rb_1100'){
		    
		    if($check_router_type['model'] == 'RouterBOARD 1100Dx4' || $check_router_type['model'] == 'RouterBOARD 1100'){
			 
		    }else{
			 return 3;
		    }
	       }
	       elseif($router_type == 'ccr_1009_7g_1c_1s_pc'){
		    
		    if($check_router_type['model'] == 'CCR_1009_7G_1C_1S_PC' || $check_router_type['model'] == "CCR1009-7G-1C-1S+"){
			 
		    }else{
			 return 3;
		    }
	       }
	  }
	  
	  return 1;
     }
     public function check_wap_router_reset_porperly($jsondata){
	  $router_type = strtolower($jsondata->router_type);
	  $router_id = $jsondata->router_id;
	  $router_user = $jsondata->router_user;
	  $router_password = $jsondata->router_password;
	  $router_port = '';
	  if(isset($jsondata->router_port)){
	       $router_port = $jsondata->router_port;
	  }
	  $this->load->library('routerlib');
	  if($router_port != ''){
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port");
	  }else{
	       $conn = RouterOS::connect("$router_id", "$router_user", "$router_password")or die("couldn't connectto router");
	  }
	  
	  // check router reset properly or not
	  //1. see if there is an IP address 192.168.88.1 in /ip address
	  $check_ip = $conn->getall("/ip/address");
	  foreach($check_ip as $check_ip1){
	       if($check_ip1['address'] == '192.168.88.1'){
		    return 2;
	       }
	  }
	  //2. check if there is any firewall rule in /ip firewall filter
	  $check_firewall = $conn->getall("/ip/firewall/filter");
	  if(count($check_firewall) > 0){
	       return 2;
	  }
	  //3. check if there is any interface by the name of bridge1 in /interface
	  $check_interface = $conn->getall("/interface");
	  foreach($check_interface as $check_interface1){
	       if($check_interface1['name'] == 'bridge1'){
		    return 2;
	       }
	  }
	  
	  // in 850gx2 check ip set on either 1
	  if($router_type == 'rb850gx2' || $router_type == '2011il' || $router_type == 'ccr1036-8g-2s'){
	       $is_either1_set = '0';
	       foreach($check_ip as $check_ip1){
		    if($check_ip1['interface'] == 'ether1'){
			 if($check_ip1['address'] != ''){
			      $is_either1_set = '1';
			 }
		    }
	       }
	       if($is_either1_set == '0'){
		    return 4;
	       }
	  }
	  return 1;
     }
     
     public function event_module_add_location_cp_extra_feature($jsondata)
     {
	  $location_uid = trim($jsondata->location_uid);
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $is_live_chat_enable = $jsondata->is_live_chat_enable;
	  $is_discuss_enable = $jsondata->is_discuss_enable;
	  $is_information_enable = $jsondata->is_information_enable;
	  $is_poll_enable = $jsondata->is_poll_enable;
	  $is_download_enable = $jsondata->is_download_enable;
	  $icon_color = $jsondata->icon_color;
	  // check and update or insert terms
	  $check = $this->DB2->query("select id from offline_event_module_turn_reature where location_uid = '$location_uid'");
	  if($check->num_rows() > 0)
	  {
	       $update_data = array(
		    'is_live_chat_enable' => $is_live_chat_enable,
		    'is_discuss_enable' => $is_discuss_enable,
		    'is_information_enable' => $is_information_enable,
		    'is_poll_enable' => $is_poll_enable,
		    'is_download_enable' => $is_download_enable,
		    'theme_color' => $icon_color
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->update('offline_event_module_turn_reature', $update_data);
	  }
	  else
	  {
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' => $location_uid,
		    'location_id' => $locationid,
		    'is_live_chat_enable' => $is_live_chat_enable,
		    'is_discuss_enable' => $is_discuss_enable,
		    'is_information_enable' => $is_information_enable,
		    'is_poll_enable' => $is_poll_enable,
		    'is_download_enable' => $is_download_enable,
		    'theme_color' => $icon_color
	       );
	       $this->DB2->insert('offline_event_module_turn_reature', $insert_data);
	  }
	  // create category
	  if($is_information_enable == '1')
	  {
	       $check = $this->DB2->query("select id from offline_main_category where location_uid = '$location_uid' AND is_event_module_category = '1'");
	       if($check->num_rows() > 0)
	       {
		    $update_data = array(
			 'is_deleted' => '0',
			 'updated_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->where('location_uid', $location_uid);
		    $this->DB2->where('is_event_module_category', '1');
		    $this->DB2->update('offline_main_category', $update_data);
	       }
	       else
	       {
		    
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'category_name' => 'Information',
			 'created_on' => date('Y-m-d H:i:s'),
			 'is_event_module_category' => '1',
		    );
		    $this->DB2->insert('offline_main_category', $insert_data);
	       }
	       
	  }
	  else
	  {
	       $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('is_event_module_category', '1');
	       $this->DB2->update('offline_main_category', $update_data);
	  }
	  if($is_poll_enable == '1')
	  {
	       $check = $this->DB2->query("select id from offline_main_category where location_uid = '$location_uid' AND is_event_module_category = '2'");
	       if($check->num_rows() > 0)
	       {
		    $update_data = array(
			 'is_deleted' => '0',
			 'updated_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->where('location_uid', $location_uid);
		    $this->DB2->where('is_event_module_category', '2');
		    $this->DB2->update('offline_main_category', $update_data);
	       }
	       else
	       {
		    
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'category_name' => 'Polls',
			 'created_on' => date('Y-m-d H:i:s'),
			 'is_event_module_category' => '2',
		    );
		    $this->DB2->insert('offline_main_category', $insert_data);
	       }
	       
	  }
	  else
	  {
	       $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('is_event_module_category', '2');
	       $this->DB2->update('offline_main_category', $update_data);
	  }
	  if($is_download_enable == '1')
	  {
	       $check = $this->DB2->query("select id from offline_main_category where location_uid = '$location_uid' AND is_event_module_category = '3'");
	       if($check->num_rows() > 0)
	       {
		    $update_data = array(
			 'is_deleted' => '0',
			 'updated_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->where('location_uid', $location_uid);
		    $this->DB2->where('is_event_module_category', '3');
		    $this->DB2->update('offline_main_category', $update_data);
	       }
	       else
	       {
		   
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'category_name' => 'Downloads',
			 'created_on' => date('Y-m-d H:i:s'),
			 'is_event_module_category' => '3',
		    );
		    $this->DB2->insert('offline_main_category', $insert_data);
	       }
	       
	  }
	  else
	  {
	       $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('is_event_module_category', '3');
	       $this->DB2->update('offline_main_category', $update_data);
	  }
	  
	  if($is_discuss_enable == '1')
	  {
	       $check = $this->DB2->query("select id from offline_main_category where location_uid = '$location_uid' AND is_event_module_category = '4'");
	       if($check->num_rows() > 0)
	       {
		   
		    $update_data = array(
			 'is_deleted' => '0',
			 'updated_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->where('location_uid', $location_uid);
		    $this->DB2->where('is_event_module_category', '4');
		    $this->DB2->update('offline_main_category', $update_data);
	       }
	       else
	       {
		   
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'category_name' => 'Discuss',
			 'created_on' => date('Y-m-d H:i:s'),
			 'is_event_module_category' => '4',
		    );
		    $this->DB2->insert('offline_main_category', $insert_data);
	       }
	       
	  }
	  else
	  {
	       $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('is_event_module_category', '4');
	       $this->DB2->update('offline_main_category', $update_data);
	  }
	  
	  $event_module_signup_user_name_require = $jsondata->event_module_signup_user_name_require;
	  $event_model_signup_user_organisation_require = $jsondata->event_model_signup_user_organisation_require;
	  $event_module_signup_nick_name = $jsondata->event_module_signup_nick_name;
	  $event_module_signup_email = $jsondata->event_module_signup_email;
	  $event_module_signup_phone = $jsondata->event_module_signup_phone;
	  // check update and insert signup fields
	  $check_signup = $this->DB2->query("select id from offline_event_module_signup_fields where location_uid = '$location_uid'");
	  if($check_signup->num_rows() > 0)
	  {
	        //update
	       
	       $update_data = array(
		    'is_enable' => $event_module_signup_user_name_require,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('field_type', '1');
	       $this->DB2->update('offline_event_module_signup_fields', $update_data);
	       
	     
	       $update_data = array(
		    'is_enable' => $event_model_signup_user_organisation_require,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('field_type', '2');
	       $this->DB2->update('offline_event_module_signup_fields', $update_data);
	     
	       $update_data = array(
		    'is_enable' => $event_module_signup_nick_name,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('field_type', '3');
	       $this->DB2->update('offline_event_module_signup_fields', $update_data);
	      
	       $update_data = array(
		    'is_enable' => $event_module_signup_email,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('field_type', '4');
	       $this->DB2->update('offline_event_module_signup_fields', $update_data);
	      
	       $update_data = array(
		    'is_enable' => $event_module_signup_phone,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $this->DB2->where('field_type', '5');
	       $this->DB2->update('offline_event_module_signup_fields', $update_data);
	  }
	  else
	  {
	       //insert
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' => $location_uid,
		    'location_id' => $locationid,
		    'field_name' => 'Name',
		    'field_type' => '1',
		    'is_enable' => $event_module_signup_user_name_require,
		    'is_required' => '1',
		    'created_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('offline_event_module_signup_fields', $insert_data);
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' => $location_uid,
		    'location_id' => $locationid,
		    'field_name' => 'Organisation',
		    'field_type' => '2',
		    'is_enable' => $event_model_signup_user_organisation_require,
		    'is_required' => '1',
		    'created_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('offline_event_module_signup_fields', $insert_data);
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' => $location_uid,
		    'location_id' => $locationid,
		    'field_name' => 'Nick Name',
		    'field_type' => '3',
		    'is_enable' => $event_module_signup_nick_name,
		    'is_required' => '1',
		    'created_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('offline_event_module_signup_fields', $insert_data);
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' => $location_uid,
		    'location_id' => $locationid,
		    'field_name' => 'Email',
		    'field_type' => '4',
		    'is_enable' => $event_module_signup_email,
		    'is_required' => '1',
		    'created_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('offline_event_module_signup_fields', $insert_data);
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' => $location_uid,
		    'location_id' => $locationid,
		    'field_name' => 'Phone No.',
		    'field_type' => '5',
		    'is_enable' => $event_module_signup_phone,
		    'is_required' => '1',
		    'created_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('offline_event_module_signup_fields', $insert_data);
	  }
     }
     
     public function add_hotel_captive_portal($jsondata){
	  $tatdays = 3;
	  if(isset($jsondata->tatdays))
	  {
	       $tatdays = $jsondata->tatdays;
	  }
	  $is_preaudit_disable = '0';
	  if(isset($jsondata->is_preaudit_disable))
	  {
	       $is_preaudit_disable = $jsondata->is_preaudit_disable;
	  }
	  $audit_ticket_email_send = '';
	  if(isset($jsondata->audit_ticket_email_send))
	  {
	       $audit_ticket_email_send = $jsondata->audit_ticket_email_send;
	  }
	  $is_white_theme = '0';
	  if(isset($jsondata->is_white_theme))
	  {
	       $is_white_theme = $jsondata->is_white_theme;
	  }
	  $contestifi_contest_header = '';
	  if(isset($jsondata->contestifi_contest_header))
	  {
	       $contestifi_contest_header = $jsondata->contestifi_contest_header;
	  }
	  $cp_cafe_retail_plan_type = "0";
	  if(isset($jsondata->cp_cafe_retail_plan_type)){
	       $cp_cafe_retail_plan_type = $jsondata->cp_cafe_retail_plan_type;
	  }
	  $is_offline_registration = 0;
	  if(isset($jsondata->is_offline_registration)){
	       $is_offline_registration = $jsondata->is_offline_registration;
	  }
	  $cp_type = $jsondata->cp_type;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $locationid = $jsondata->locationid;
	  
	  $synk_frequency = "1";
	  if(isset($jsondata->synk_frequency)){
	       $synk_frequency = $jsondata->synk_frequency;
	  }
	  $offline_cp_source_folder_path = "";
	  if(isset($jsondata->offline_cp_source_folder_path)){
	       $offline_cp_source_folder_path = $jsondata->offline_cp_source_folder_path;
	  }
	  $offline_cp_landing_page_path = "";
	  if(isset($jsondata->offline_cp_landing_page_path)){
	       $offline_cp_landing_page_path = $jsondata->offline_cp_landing_page_path;
	  }
	  $icon_color = "";
	  if(isset($jsondata->icon_color)){
	       $icon_color = $jsondata->icon_color;
	  }
	  
	  $offline_cp_type = "0";
	  if($cp_type == 'cp_offline'){
	       if(isset($jsondata->offline_cp_type)){
		    $offline_cp_type = $jsondata->offline_cp_type;
		    $cp_url_bitly_set = '';
		    if($offline_cp_type == '2'){//static cp for offline
			 $cp_url_bitly_set = $offline_cp_landing_page_path;
		    }elseif($offline_cp_type == '1'){// dynamic cp for offline
			 $cp_url_bitly_set = OFFLINE_CODE.'isp_hotspot/offline/offlinedynamic_bitly/index.php';
		    }
		    
		    $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
		    $bitly_url = $this->get_bitly_short_url($url);
		    $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
		    $this->DB2->update('wifi_location');
		    
	       }
	  }
	  if($cp_type == 'cp_visitor'){
	       $offline_cp_type = $jsondata->offline_cp_type;
               $cp_url_bitly_set = OFFLINE_CODE.'isp_hotspot/offline/visitoroffline_bitly/index.php';
               $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
		$bitly_url = $this->get_bitly_short_url($url);
		    $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
		    $this->DB2->update('wifi_location');
	  }
	  if($cp_type == 'cp_eventguest'){
	       $offline_cp_type = $jsondata->offline_cp_type;
               $cp_url_bitly_set = OFFLINE_CODE.'isp_hotspot/offline/eventguest_bitly/index.php';
               $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
		$bitly_url = $this->get_bitly_short_url($url);
		    $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
		    $this->DB2->update('wifi_location');
	  }
	  
	  if($cp_type == 'cp_contestifi'){
	       $offline_cp_type = $jsondata->offline_cp_type;
               //$cp_url_bitly_set = OFFLINE_CODE.'isp_hotspot/offline/contestifi_bitly/index.php';
	       $cp_url_bitly_set = OFFLINE_CODE.'isp_hotspot/offline/contestifi_quiz/index.php';
               $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
	       $bitly_url = $this->get_bitly_short_url($url);
	       $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
	       $this->DB2->update('wifi_location');
	  }
	  if($cp_type == 'cp_retail_audit'){
               $cp_url_bitly_set = OFFLINE_CODE.'retail_audit/index.php';
               $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
	       $bitly_url = $this->get_bitly_short_url($url);
	       $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
	       $this->DB2->update('wifi_location');
	  }
	  if($cp_type == 'cp_event_module'){
	       $offline_cp_type = $jsondata->offline_cp_type;
               $cp_url_bitly_set = OFFLINE_CODE.'isp_hotspot/offline/event_mgmt_bitly/index.php';
               $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
	       $bitly_url = $this->get_bitly_short_url($url);
	       $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
	       $this->DB2->update('wifi_location');
	       $is_live_chat_enable = '0';
	       if(isset($jsondata->is_live_chat_enable))
	       {
		    $is_live_chat_enable = $jsondata->is_live_chat_enable;
	       }
	       $is_discuss_enable = '0';
	       if(isset($jsondata->is_discuss_enable))
	       {
		    $is_discuss_enable = $jsondata->is_discuss_enable;
	       }
	       $is_information_enable = '0';
	       if(isset($jsondata->is_information_enable))
	       {
		    $is_information_enable = $jsondata->is_information_enable;
	       }
	       $is_poll_enable = '0';
	       if(isset($jsondata->is_poll_enable))
	       {
		    $is_poll_enable = $jsondata->is_poll_enable;
	       }
	       $is_download_enable = '0';
	       if(isset($jsondata->is_download_enable))
	       {
		    $is_download_enable = $jsondata->is_download_enable;
	       }
	       $event_module_signup_user_name_require = '0';
	       if(isset($jsondata->event_module_signup_user_name_require))
	       {
		    $event_module_signup_user_name_require = $jsondata->event_module_signup_user_name_require;
	       }
	       $event_model_signup_user_organisation_require = '0';
	       if(isset($jsondata->event_model_signup_user_organisation_require))
	       {
		    $event_model_signup_user_organisation_require = $jsondata->event_model_signup_user_organisation_require;
	       }
	       $event_module_signup_nick_name = '0';
	       if(isset($jsondata->event_module_signup_nick_name))
	       {
		    $event_module_signup_nick_name = $jsondata->event_module_signup_nick_name;
	       }
	       $event_module_signup_email = '0';
	       if(isset($jsondata->event_module_signup_email))
	       {
		    $event_module_signup_email = $jsondata->event_module_signup_email;
	       }
	       $event_module_signup_phone = '0';
	       if(isset($jsondata->event_module_signup_phone))
	       {
		    $event_module_signup_phone = $jsondata->event_module_signup_phone;
	       }
	       $requestData = array(
		    'location_uid' => $location_uid,
		    'locationid' => $locationid,
		    'isp_uid' => $isp_uid,
		    'is_live_chat_enable' => $is_live_chat_enable,
		    'is_discuss_enable' => $is_discuss_enable,
		    'is_information_enable' => $is_information_enable,
		    'is_poll_enable' => $is_poll_enable,
		    'is_download_enable' => $is_download_enable,
		    'event_module_signup_user_name_require' => $event_module_signup_user_name_require,
		    'event_model_signup_user_organisation_require' => $event_model_signup_user_organisation_require,
		    'event_module_signup_nick_name' => $event_module_signup_nick_name,
		    'event_module_signup_email' => $event_module_signup_email,
		    'event_module_signup_phone' => $event_module_signup_phone,
		    'icon_color' => $icon_color
	       );
	       $data_request = json_decode(json_encode($requestData));
	       $this->event_module_add_location_cp_extra_feature($data_request);
	       
	  }
	  
	  
	  
	 
	  $original_slider1 = "";
	  if(isset($jsondata->original_slider1)){
	       $original_slider1 = $jsondata->original_slider1;
	  }
	  $original_slider2 = "";
	  if(isset($jsondata->original_slider2)){
	       $original_slider2 = $jsondata->original_slider2;
	  }
	  
	  $custom_captive_url = '';
	  if($cp_type == 'cp_custom'){
	       if(isset($jsondata->custom_captive_url)){
		    $custom_captive_url = $jsondata->custom_captive_url;
		    if($custom_captive_url != ''){
			 //$url = $custom_captive_url."/login.php?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
			 $url = $custom_captive_url."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
			 $bitly_url = $this->get_bitly_short_url($url);
			 $this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
			 $this->DB2->update('wifi_location');
		    }
		    
	       }
	  }
	  
	  $custom_location_plan = '0';
	  if(isset($jsondata->custom_location_plan)){
	       $custom_location_plan = $jsondata->custom_location_plan;
	  }
	  $custon_cp_signip_data = '0';
	  if(isset($jsondata->custon_cp_signip_data)){
	       $custon_cp_signip_data = $jsondata->custon_cp_signip_data;
	  }
	  $custon_cp_daily_data = '0';
	  if(isset($jsondata->custon_cp_daily_data)){
	       $custon_cp_daily_data = $jsondata->custon_cp_daily_data;
	  }
	  $custon_cp_no_of_daily_session = '0';
	  if(isset($jsondata->custon_cp_no_of_daily_session)){
	       $custon_cp_no_of_daily_session = $jsondata->custon_cp_no_of_daily_session;
	  }
	       $is_otpdisabled = '0';
	       if(isset($jsondata->is_otpdisabled)){
		    $is_otpdisabled = $jsondata->is_otpdisabled;
	       }
	       $cp_cafe_plan_id = '';
	       if(isset($jsondata->cp_cafe_plan_id)){
		    $cp_cafe_plan_id = $jsondata->cp_cafe_plan_id;
	       }
	       $num_of_session_per_day = 0;
	  if(isset($jsondata->num_of_session_per_day)){
	       $num_of_session_per_day = $jsondata->num_of_session_per_day;
	  }
	  
	  
	  $is_sociallogin = "0";
	  if(isset($jsondata->is_socialloginenable)){
	       $is_sociallogin = $jsondata->is_socialloginenable;
	  }
	  $is_pinenable = "0";
	  if(isset($jsondata->is_pinenable)){
	       $is_pinenable = $jsondata->is_pinenable;
	  }
	  $is_wifidisabled = "0";
	  if(isset($jsondata->is_wifidisabled)){
	       $is_wifidisabled = $jsondata->is_wifidisabled;
	  }
	  $login_with_mobile_email = "0";
	  if(isset($jsondata->login_with_mobile_email)){
	       $login_with_mobile_email = $jsondata->login_with_mobile_email;
	  }
	  $offer_redemption_mode = "0";
	  if(isset($jsondata->offer_redemption_mode)){
	       $offer_redemption_mode = $jsondata->offer_redemption_mode;
	  }
	       
	
        
	$cp_type = $jsondata->cp_type;
        $cp_hotel_plan_type = $jsondata->cp_hotel_plan_type;
	$cp_home_selected_plan_id = $jsondata->cp_home_selected_plan_id;
        $original_image = $jsondata->image_original;
        $small_image = $jsondata->image_small;
        $logo_image = $jsondata->image_logo;
	
	$retail_original_image = '';
	if(isset($jsondata->retail_image_original)){
	  $retail_original_image = $jsondata->retail_image_original;
	}
        $retail_small_image = '';
	if(isset($jsondata->retail_image_small)){
	  $retail_small_image = $jsondata->retail_image_small;
	}
        $retail_logo_image = '';
	if(isset($jsondata->retail_image_logo)){
	  $retail_logo_image = $jsondata->retail_image_logo;
	}
	
	$enterprise_user_type = 0;
	if(isset($jsondata->cp_enterprise_user_type)){
	  $enterprise_user_type = $jsondata->cp_enterprise_user_type;
	}
	$institutional_user_list = '';
	if(isset($jsondata->institutional_user_list)){
	  $institutional_user_list = $jsondata->institutional_user_list;
	}
	$main_ssid = '';
	if(isset($jsondata->main_ssid)){
	  //$main_ssid = $jsondata->main_ssid;
	  $main_ssid = $jsondata->main_ssid;
	}
	$guest_ssid = '';
	if(isset($jsondata->guest_ssid)){
	  $guest_ssid = $jsondata->guest_ssid;
	}
	$created_cp_id = '';
	// for new concept
	$cp_hotel_plan_type = '2';
	if($cp_cafe_plan_id != ''){
	  //get plan type
	  $get_plan_type = $this->DB2->query("select plantype from sht_services where srvid = '$cp_cafe_plan_id'");
	  if($get_plan_type->num_rows() > 0){
	       $plan_row = $get_plan_type->row_array();
	       if($plan_row['plantype'] == '4'){
		    $cp_hotel_plan_type == '1';
	       }
	  }
	}
	// for cafe
	  $cafe_plan_type = "time";
	  if($cp_hotel_plan_type == '1'){
	       $cafe_plan_type = "data";  
	  }
        //check already created or not
        $query = $this->DB2->query("select id,cp_type from wifi_location_cptype where location_id = '$locationid'");
        if($query->num_rows() > 0){
	  // update offline version
	  if($cp_type == 'cp_offline'){
	       // update offline version
	       $this->update_offline_version($locationid);
	  }
            $row = $query->row_array();
	       $update_image_query = array();
	       if($original_image != '' || $small_image != '' || $logo_image != ''){
		   $update_image_query['original_image'] = $original_image;
		   $update_image_query['small_image'] = $small_image;
		   $update_image_query['logo_image'] = $logo_image;
	       }
	       $update_retail_image_query = array();
	       if($retail_original_image != '' || $retail_small_image != '' || $retail_logo_image != ''){
		   $update_retail_image_query['retail_original_image'] = $retail_original_image;
		   $update_retail_image_query['retail_small_image'] = $retail_small_image;
		   $update_retail_image_query['retail_logo_image'] = $retail_logo_image;
	       }
	       $update_slider1_image = array();
	       if($original_slider1 != ""){
		    $update_slider1_image['custom_slider_image1'] = $original_slider1;
	       }
	       $update_slider2_image = array();
	       if($original_slider2 != ""){
		    $update_slider2_image['custom_slider_image2'] = $original_slider2;
	       }
	       $update_data = array(
		    'is_offline_registration' => $is_offline_registration,
		    'is_sendvcemail' => $offer_redemption_mode,
		    'is_loginemail' => $login_with_mobile_email,
		    'is_wifidisabled' => $is_wifidisabled,
		    'is_enablepin' => $is_pinenable,
		    'is_sociallogin' => $is_sociallogin,
		    'colour_code' => $icon_color,
		    'cp1_no_of_daily_session' => $num_of_session_per_day,
		    'uid_location' => $location_uid,
		    'cp_type' => $cp_type,
		    'cp_hotel_plan_type' => $cp_hotel_plan_type,
		    'entrprise_usertype' => $enterprise_user_type,
		    'main_ssid' => $main_ssid,
		    'guest_ssid' => $guest_ssid,
		    'cafe_plan' => $cp_cafe_plan_id,
		    'cafe_plantype' => $cafe_plan_type,
		    'is_otpdisabled' => $is_otpdisabled,
		    'custom_url' => $custom_captive_url,
		    'custom_planid' => $custom_location_plan,
		    'custom_signup_data' => $custon_cp_signip_data,
		    'custom_daily_data' => $custon_cp_daily_data,
		    'custom_daily_no_of_session' => $custon_cp_no_of_daily_session,
		    'frequency_hour' => $synk_frequency,
		    'offline_cp_content_type' => $offline_cp_type,
		    'offline_cp_source_folder_path' => $offline_cp_source_folder_path,
		    'offline_cp_landing_page_path' => $offline_cp_landing_page_path,
		    'plan_type_time_hybrid_data' => $cp_cafe_retail_plan_type,
		    'is_white_theme' => $is_white_theme,
		    'updated_on' => date('Y-m-d H:i:s'),
		    'is_preaudit_disable' => $is_preaudit_disable,
		    'audit_ticket_email_send' => $audit_ticket_email_send,
		    'tatdays' => $tatdays
	       );
	       $update_data = array_merge($update_data, $update_image_query);
	       $update_data = array_merge($update_data, $update_retail_image_query);
	       $update_data = array_merge($update_data, $update_slider1_image);
	       $update_data = array_merge($update_data, $update_slider2_image);
	       $this->DB2->where('location_id', $locationid);
	       $this->DB2->update('wifi_location_cptype', $update_data);
	       
	       
	       // update offline version
	       $this->update_offline_version($locationid);
		$created_cp_id = $locationid;
		$this->update_cp_ssid_onehop($location_uid, $main_ssid,$custom_captive_url);
	    
            
        }
        else{
	       $random_salt = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,2 ) .substr( md5( time() ), 1, 5);
	       $random_salt = md5($location_uid.$random_salt);
	       $api_key = time().md5($random_salt);
	       $offline_build_version = 1;
	       $insert_data = array(
		    'uid_location' => $location_uid,
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'cp_type' => $cp_type,
		    'cp_hotel_plan_type' => $cp_hotel_plan_type,
		    'entrprise_usertype' => $enterprise_user_type,
		    'created_on' => date('Y-m-d H:i:s'),
		    'original_image' => $original_image,
		    'small_image' => $small_image,
		    'logo_image' => $logo_image,
		    'main_ssid' => $main_ssid,
		    'guest_ssid' => $guest_ssid,
		    'cafe_plan' => $cp_cafe_plan_id,
		    'cafe_plantype' => $cafe_plan_type,
		    'is_otpdisabled' => $is_otpdisabled,
		    'retail_original_image' => $retail_original_image,
		    'retail_small_image' => $retail_small_image,
		    'retail_logo_image' => $retail_logo_image,
		    'cp1_no_of_daily_session' => $num_of_session_per_day,
		    'colour_code' => $icon_color,
		    'is_sociallogin' => $is_sociallogin,
		    'is_enablepin' => $is_pinenable,
		    'is_wifidisabled' => $is_wifidisabled,
		    'is_loginemail' => $login_with_mobile_email,
		    'is_sendvcemail' => $offer_redemption_mode,
		    'custom_url' => $custom_captive_url,
		    'custom_planid' => $custom_location_plan,
		    'custom_signup_data' => $custon_cp_signip_data,
		    'custom_daily_data' => $custon_cp_daily_data,
		    'custom_daily_no_of_session' => $custon_cp_no_of_daily_session,
		    'custom_slider_image1' => $original_slider1,
		    'custom_slider_image2' => $original_slider2,
		    'frequency_hour' => $synk_frequency,
		    'offline_cp_content_type' => $offline_cp_type,
		    'offline_cp_source_folder_path' => $offline_cp_source_folder_path,
		    'offline_cp_landing_page_path' => $offline_cp_landing_page_path,
		    'offline_version' => $offline_build_version,
		    'apikey' => $api_key,
		    'is_offline_registration' => $is_offline_registration,
		    'plan_type_time_hybrid_data' => $cp_cafe_retail_plan_type,
		    'offline_content_title' => $contestifi_contest_header,
		    'is_white_theme' => $is_white_theme,
		    'is_preaudit_disable' => $is_preaudit_disable,
		    'audit_ticket_email_send' => $audit_ticket_email_send,
		    'tatdays' => $tatdays
	       );
	       $insert = $this->DB2->insert('wifi_location_cptype', $insert_data);
	       $created_cp_id = $this->DB2->insert_id();
        }
	//insert into ht_planassociation table
	if($cp_type == 'cp_custom'){
	       $check = $this->DB2->query("select nasid from wifi_location where location_uid = '$location_uid'");
	       if($check->num_rows() > 0){
		    $check_row = $check->row_array();
		    $nasid = $check_row['nasid'];
		    $check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$custom_location_plan' and nasid = '$nasid'");
		    if($check->num_rows() <= '0'){
			
			 $insert_data = array(
			      'srvid' => $custom_location_plan,
			      'nasid' => $nasid,
			 );
			 $this->DB2->insert('sht_plannasassoc', $insert_data);
		    }
	       }
	}
	elseif($cp_type == 'cp_cafe' || $cp_type == 'cp_retail'){
	  // check already associated
	  $check = $this->DB2->query("select nasid from wifi_location where location_uid = '$location_uid'");
	  if($check->num_rows() > 0){
	       $check_row = $check->row_array();
	     $nasid = $check_row['nasid'];
	     $check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp_cafe_plan_id' and nasid = '$nasid'");
				if($check->num_rows() <= '0'){
					
					$insert_data = array(
					     'srvid' => $cp_cafe_plan_id,
					     'nasid' => $nasid,
					);
					$this->DB2->insert('sht_plannasassoc', $insert_data);
				}
	  }
				
	}else{
	       if($cp_home_selected_plan_id != ''){
	       $cp_home_selected_plan_id = explode(',',$cp_home_selected_plan_id);
	  
	       foreach($cp_home_selected_plan_id as $cp_home_selected_plan_id1){
		    $get = $this->DB2->query("select * from ht_planassociation where location_id = '$locationid' and plan_id = '$cp_home_selected_plan_id1'");
		    if($get->num_rows() <=0){
			 $insert_data = array(
			      'location_uid' => $location_uid,
			      'location_id' => $locationid,
			      'plan_id' => $cp_home_selected_plan_id1,
			 );
			 $insert = $this->DB2->insert('ht_planassociation', $insert_data);
		    }
	       }
	       // delete plan id who not selected and previous selected
	       $delete_plan = $this->DB2->query("delete from ht_planassociation where location_uid = '$location_uid' and location_id = '$locationid' AND plan_id NOT IN ( '" . implode($cp_home_selected_plan_id, "', '") . "' )");
	       // check nas and plan assoc
	       $check = $this->DB2->query("select nasid from wifi_location where location_uid = '$location_uid'");
	       if($check->num_rows() > 0){
		 
		   $check_row = $check->row_array();
		   $nasid = $check_row['nasid'];
		   if($nasid > 0){
		       foreach($cp_home_selected_plan_id as $cp_home_selected_plan_id1){
			 $check = $this->DB2->query("select * from sht_plannasassoc where srvid = '$cp_home_selected_plan_id1' and nasid = '$nasid'");
			 if($check->num_rows() <= 0){
			      
			      $insert_data = array(
				   'srvid' => $cp_home_selected_plan_id1,
				   'nasid' => $nasid,
			      );
			      $insert = $this->DB2->insert('sht_plannasassoc', $insert_data);
			 }
		      }
		   }
	       }
	     }
	}
	
        //insert into location_usertype table
	if($institutional_user_list != ''){
	       $institutional_user_list = explode(',',$institutional_user_list);
     
	       foreach($institutional_user_list as $institutional_user_list1){
		    $user_type = explode('~~~~',$institutional_user_list1);
		    $id = $user_type[0];
		    $type = $user_type[1];
		    if($id != ''){
			 $this->DB2->set('usertype', $type)->where('id', $id);
			 $update = $this->DB2->update('location_usertype');
		    }else{
			 //check already exist
			 $check = $this->DB2->query("select * from location_usertype where location_id = '$locationid' AND usertype = '$type'");
			 if($check->num_rows() <=0){
			     
			      $insert_data = array(
				   'usertype' => $type,
				   'location_id' => $locationid,
			      );
			      $insert = $this->DB2->insert('location_usertype', $insert_data);
			 }
			 
		    }
		
	       }
	  }
	  if($cp_type == 'cp_contestifi')
	  {
	       $user_name_required = 1;
	       if(isset($jsondata->user_name_required)){
		    $user_name_required = $jsondata->user_name_required;
	       }
	       $user_email_required = 1;
	       if(isset($jsondata->user_email_required)){
		    $user_email_required = $jsondata->user_email_required;
	       }
	       $user_age_required = 1;
	       if(isset($jsondata->user_age_required)){
		    $user_age_required = $jsondata->user_age_required;
	       }
	       $user_gender_required = 1;
	       if(isset($jsondata->user_gender_required)){
		    $user_gender_required = $jsondata->user_gender_required;
	       }
	       $country_code = $this->get_country_code($isp_uid);
	       $requestData = array(
		    'type' => "cp",
		    'locationid' => $created_cp_id,
		    'background_color' => $icon_color,
		    'is_otp_enable' => $is_otpdisabled,
		    'is_signup_enable' => $is_offline_registration,
		    'country_code' => $country_code,
		    'is_loginemail' => $login_with_mobile_email,
		    'user_name_required' => $user_name_required,
		    'user_email_required' => $user_email_required,
		    'user_age_required' => $user_age_required,
		    'user_gender_required' => $user_gender_required
	       );
	       $data_request = json_decode(json_encode($requestData));
	       $this->add_contestifi_extra_feature($data_request);
	  }
	  if($cp_type == 'cp_offline' || $cp_type == 'cp_visitor' || $cp_type == 'cp_contestifi')
	  {
	       // create zip
	       $requestData = array(
		    'location_uid' => $location_uid
	       );
	       $data_request = json_decode(json_encode($requestData));
	       $this->create_zip_for_offline_content($data_request);
	  }
        $data = array();
        $data['created_cp_id'] = $created_cp_id;
	
        return $data;
     }
     

     public function ap_location_list($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $data = array();
	  $this->DB2->select('*')->from('wifi_ap_location_type');
	  $query = $this->DB2->get();
        $i = 0;
        foreach($query->result() as $row){
            $data[$i]['ap_id'] = $row->ap_id;
            $data[$i]['ap_location_name'] = $row->ap_location_name;
            $i++;
        }
        return $data;
    }
    public function add_ap_location($jsondata){
        $ap_location_name = $jsondata->ap_location_name;
        $isp_uid = $jsondata->isp_uid;
        
        $data = array();
	$check = $this->DB2->query("select * from wifi_ap_location_type where ap_location_name = '$ap_location_name'");
	if($check->num_rows() > 0){
	  $data['resultCode'] = '0';
	}else{
	  $insert_data = array(
	       'ap_location_name' => $ap_location_name,
	       'created_on' => date('Y-m-d H:i:s'),
	  );
	  $insert = $this->DB2->insert('wifi_ap_location_type', $insert_data);
        
	  $data['resultCode'] = '1';
	}
        
           
        return $data;
    }

     public function is_location_proper_setup($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $nasid = '';
	  $location_type = '';
	  $is_nas_setup = 0;
	  $is_nas_plan_associated = 0;
	  //check nas is configure
	  $check = $this->DB2->query("select nasid, nasconfigure, location_type from wifi_location where location_uid = '$location_uid'");
	  if($check->num_rows() > 0){
	       $row = $check->row_array();
	       $nasid = $row['nasid'];
	       $is_nas_setup = $row['nasconfigure'];
	       $location_type = $row['location_type'];
	  }
	  //check nas and plan associated or not
	  if($location_type != ''){
	       if($location_type == '1' || $location_type == '6'){//public location
		    // get plan id
		    $plan_id = '';
		    $get_plan = $this->DB2->query("select cp_type,cp1_plan,cp2_plan,cp3_data_plan,cp3_hybrid_plan,cafe_plan from wifi_location_cptype where uid_location = '$location_uid'");
		    if($get_plan->num_rows() > 0){
			 $plan_row = $get_plan->row_array();
			 $cp_type = $plan_row['cp_type'];
			 if($cp_type == 'cp1' || $cp_type == 'cp_truck_stop' || $cp_type == 'cp4'){
			      $plan_id = $plan_row['cp1_plan'];
			 }else if($cp_type == 'cp2'){
			      $plan_id = $plan_row['cp2_plan'];
			 }else if($cp_type == 'cp3'){
			      $plan_id = $plan_row['cp3_data_plan'];
			 }else if($cp_type == 'cp_cafe'){
			      $plan_id = $plan_row['cafe_plan'];
			 }
			 if($plan_id != ''){
			      // check nas association table
			      $check_nas_assoc = $this->DB2->query("select * from sht_plannasassoc where srvid = '$plan_id' AND nasid = '$nasid'");
			      if($check_nas_assoc->num_rows()> 0){
				   $is_nas_plan_associated = '1';
				   // for cp3 hybrid plan association check
				   if($cp_type == 'cp3'){
					$plan_id = $plan_row['cp3_hybrid_plan'];
					$check_nas_assoc = $this->DB2->query("select * from sht_plannasassoc where srvid = '$plan_id' AND nasid = '$nasid'");
					if($check_nas_assoc->num_rows()> 0){
					     $is_nas_plan_associated = '1';
					}else{
					     $is_nas_plan_associated = '0';
					}
				   }
			      }
			 }
		    }
	       }else{// other location like hotel, hospital, etc
		    //get plan ids
		    $get_plan_id = $this->DB2->query("select plan_id from ht_planassociation where location_uid = '$location_uid'");
		    if($get_plan_id->num_rows() > 0){
			 foreach($get_plan_id->result() as $row_plan_id){
			      $plan_id = $row_plan_id->plan_id;
			      $check_nas_assoc = $this->DB2->query("select * from sht_plannasassoc where srvid = '$plan_id' AND nasid = '$nasid'");
			      if($check_nas_assoc->num_rows()> 0){
				   $is_nas_plan_associated = '1';
			      }else{
				   $is_nas_plan_associated = '0';
				   break;
			      }
			 }
		    }
	       }
	  }
	  $data['is_nas_setup'] = $is_nas_setup;
	  $data['is_nas_plan_associated'] = $is_nas_plan_associated;
        return json_encode($data);
    }
    public function location_logo_name($jsondata){
	 $location_uid = $jsondata->location_uid;
	   $data = array();
	   $original_image = '';
	   
	   $this->DB2->select('original_image')->from('wifi_location_cptype');
	  $this->DB2->where('uid_location', $location_uid);
	  $get_name = $this->DB2->get();
	   if($get_name->num_rows() > 0){
		$row = $get_name->row_array();
		$original_image = $row['original_image'];
	   }
	   $data['original_name'] = $original_image;
	 return $data;
     }
    

     public function traffic_summary_total_session_old($jsondata){
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $location_filter_query = '';;
	  $total_session =0 ;
	  $total_new_user=0;
	  $total_returning_session =0;
	  if(count($jsondata->locations) > 0){
	       $locations = '';
	       $locations='"'.implode('", "', $jsondata->locations).'"';
	       $location_filter_query = " AND location_uid IN($locations) ";
	  }
	  $live_location_id = array();
	  $query = $this->DB2->query("select id, location_name from wifi_location where isp_uid = '$jsondata->isp_uid' and status = '1' and is_deleted = '0' $location_filter_query $state_where $city_where");
	  foreach($query->result() as $row){
	     $live_location_id[] = $row->id; 
	  }
	  $live_location_id='"'.implode('", "', $live_location_id).'"';
	  $previous_user = array();
	  $previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$from'   group by userid");
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  
	  //condition
	  $cond = "and date(wufs.session_date) between '$from' and '$to'";
	  // time filter
	  $time_filter = '';
	  if(count($jsondata->time_slot) > 0){
	       $time_or_query = '';
	       $t = 0;
	       foreach($jsondata->time_slot as $filt_time){
		    $time_sended = explode('-', $filt_time);
		    $time_from =  $time_sended[0];
		    $time_to =  $time_sended[1];
		    if($t == 0){
			 $time_or_query = " TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
		    }
		    else{
			 $time_or_query = $time_or_query." OR TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
		    }
		    $t++;
	       }
	       $time_filter = " AND ($time_or_query) ";
	  }
	  //gender filter
	  $gender_where = '';
	  if($jsondata->gender == 'm'){
               $gender_where = " AND (wufs.gender = 'm' or wufs.gender = 'M' or wufs.gender = 'male' or wufs.gender = 'Male')";
          }elseif($jsondata->gender == 'f'){
               $gender_where = " AND (wufs.gender = 'f' or wufs.gender = 'F' or wufs.gender = 'female' or wufs.gender = 'Female')";
          }
	  // age group filter
	  $age_group_filter = '';
	  if(count($jsondata->age_group) > 0){
	       $age_group_or_query = '';
	       $t = 0;
	       foreach($jsondata->age_group as $age_group_value){
			   if($t == 0){
			       $age_group_or_query = " wufs.age_group = '$age_group_value' ";
			   }
			   else{
			       $age_group_or_query = $age_group_or_query." OR wufs.age_group = '$age_group_value' ";
			   }
			   $t++;
	       }
	       $age_group_filter = " AND ($age_group_or_query) ";
	  }
	  $chech_user = $this->DB2->query("select wufs.*, date(session_date) as date_field  from wifi_user_free_session as wufs  WHERE wufs.location_id IN (".$live_location_id.")  $cond
        $time_filter $gender_where $age_group_filter  order by wufs.session_date");
	  
	  $date_wise = array();
	  foreach($chech_user->result() as $chech_user_row){
	       $date_wise[$chech_user_row->date_field][]= $chech_user_row;
	  }
	  $fnlarr=array();
	  $dataarr=array();
	  $daywisearr=array();
	 // echo "<pre>";print_r($date_wise_arr);die;
	  foreach($date_wise as $keydate => $date_wise_row){
	       
	       $date_wise_arr=array();
	       $repeatuser=0;
	       $newuser=0;
	       foreach($date_wise_row as $valnew)
	       {
		    
		    
		    //if(!in_array($valnew->userid,$date_wise_arr)){
			 if(in_array($valnew->userid,$previous_user))
			 {
			   $repeatuser=$repeatuser+1;
			   $total_returning_session = $total_returning_session+1;;
			 }
			 else
			 {
			     $newuser=$newuser+1;
			     $total_new_user = $total_new_user+1;
			 }
			 $total_session = $total_session+1;
			 //$total_unique_user_array[] = $valnew->userid;
			 //$date_wise_arr[]=$valnew->userid;
			 $previous_user[]=$valnew->userid;
		    //}
		    $fnlarr[$keydate]['newuser']= $newuser;
		    $fnlarr[$keydate]['repeatuser']= $repeatuser;
	       }
	      
	  }
	  $x=0;
	  foreach($fnlarr as $key=>$valdate)
	  {
	       $dataarr['data'][$x]['viewed_on']=date("d M",strtotime($key));
	       $dataarr['data'][$x]['newuser']=$valdate['newuser'];
	       $dataarr['data'][$x]['returninguser']=$valdate['repeatuser'];
	       $x++;
	       
	  }
	  $dataarr["total_newuser"] = $total_new_user;
	  $dataarr["total_returninguser"] = $total_returning_session;
	  $dataarr["total_user"] = $total_session;
	  //echo "<pre>";print_r($dataarr);die;
	  return $dataarr;
        
     }
     
     
     
          public function traffic_summary_total_session($jsondata){
	  ini_set('memory_limit', '-1');
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $location_filter_query = '';;
	  $total_session =0 ;
	  $total_new_user=0;
	  $total_returning_session =0;
	  if(count($jsondata->locations) > 0){
	       $locations = '';
	       $locations='"'.implode('", "', $jsondata->locations).'"';
	       $location_filter_query = " AND location_uid IN($locations) ";
	  }
	  $live_location_id = array();
	  $query = $this->DB2->query("select id, location_name from wifi_location where isp_uid = '$jsondata->isp_uid' and status = '1' and is_deleted = '0' $location_filter_query $state_where $city_where");
	  foreach($query->result() as $row){
	     $live_location_id[] = $row->id; 
	  }
	  
	 
	  $live_location_id='"'.implode('", "', $live_location_id).'"';
	  $previous_user = array();
	  $previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$from'   group by userid");
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  //condition
	  $cond = "and date(wufs.session_date) between '$from' and '$to'";
	  // time filter
	  $time_filter = '';
	  if(count($jsondata->time_slot) > 0){
	       $time_or_query = '';
	       $t = 0;
	       foreach($jsondata->time_slot as $filt_time){
		    $time_sended = explode('-', $filt_time);
		    $time_from =  $time_sended[0];
		    $time_to =  $time_sended[1];
		    if($t == 0){
			 $time_or_query = " TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
		    }
		    else{
			 $time_or_query = $time_or_query." OR TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
		    }
		    $t++;
	       }
	       $time_filter = " AND ($time_or_query) ";
	  }
	  //gender filter
	  $gender_where = '';
	  if($jsondata->gender == 'm'){
               $gender_where = " AND (wufs.gender = 'm' or wufs.gender = 'M' or wufs.gender = 'male' or wufs.gender = 'Male')";
          }elseif($jsondata->gender == 'f'){
               $gender_where = " AND (wufs.gender = 'f' or wufs.gender = 'F' or wufs.gender = 'female' or wufs.gender = 'Female')";
          }
	  // age group filter
	  $age_group_filter = '';
	  if(count($jsondata->age_group) > 0){
	       $age_group_or_query = '';
	       $t = 0;
	       foreach($jsondata->age_group as $age_group_value){
			   if($t == 0){
			       $age_group_or_query = " wufs.age_group = '$age_group_value' ";
			   }
			   else{
			       $age_group_or_query = $age_group_or_query." OR wufs.age_group = '$age_group_value' ";
			   }
			   $t++;
	       }
	       $age_group_filter = " AND ($age_group_or_query) ";
	  }
	  $chech_user = $this->DB2->query("select wufs.userid, date(session_date) as date_field  from wifi_user_free_session as wufs  WHERE wufs.location_id IN (".$live_location_id.")  $cond
        $time_filter $gender_where $age_group_filter  order by wufs.session_date");
	  /*foreach($chech_user->result() as $chech_user_row){
	       if(in_array($chech_user_row->userid,$previous_user)){
		    $total_returning_session = $total_returning_session+1;;
	       }else{
		    $total_new_user = $total_new_user+1;
	       }
	       $total_session = $total_session+1;
	       //$previous_user[]=$chech_user_row->userid;
	  }
	  $time_end = microtime(true);
	  $time = $time_end - $time_start;
	  echo "Process Time: {$time}";
	  die;*/
	  
	  //die;
	  $date_wise = array();
	  foreach($chech_user->result() as $chech_user_row){
	       //echo "<pre>";print_r($chech_user_row);die;
	       $date_wise[$chech_user_row->date_field][]= $chech_user_row;
	  }
	 
	  $fnlarr=array();
	  $dataarr=array();
	  $daywisearr=array();
	 // echo "<pre>";print_r($date_wise_arr);die;
	 //echo "<pre>";print_r($previous_user);die;
	 $flipped_fruits = array_flip($previous_user);
	  foreach($date_wise as $keydate => $date_wise_row){
	       
	       $date_wise_arr=array();
	       $repeatuser=0;
	       $newuser=0;
	       foreach($date_wise_row as $valnew)
	       {
		    
		    
		    //if(!in_array($valnew->userid,$date_wise_arr)){
			 //if(in_array($valnew->userid,$previous_user))
			 
			 if (isSet($flipped_fruits[$valnew->userid])) 
			 {
			   $repeatuser=$repeatuser+1;
			   $total_returning_session = $total_returning_session+1;;
			 }
			 else
			 {
			     $newuser=$newuser+1;
			     $total_new_user = $total_new_user+1;
			 }
			 $total_session = $total_session+1;
			 //$previous_user[]=$valnew->userid;
			 $flipped_fruits[$valnew->userid]=$valnew->userid;
		    //}
		    $fnlarr[$keydate]['newuser']= $newuser;
		    $fnlarr[$keydate]['repeatuser']= $repeatuser;
	       }
	      
	  }
	  
	  $x=0;
	  foreach($fnlarr as $key=>$valdate)
	  {
	       $dataarr['data'][$x]['viewed_on']=date("d M",strtotime($key));
	       $dataarr['data'][$x]['newuser']=$valdate['newuser'];
	       $dataarr['data'][$x]['returninguser']=$valdate['repeatuser'];
	       $x++;
	       
	  }
	  $dataarr["total_newuser"] = $total_new_user;
	  $dataarr["total_returninguser"] = $total_returning_session;
	  $dataarr["total_user"] = $total_session;
	  //echo "<pre>";print_r($dataarr);die;
	  return $dataarr;
        
     }

 
	 
	 
	  public function traffic_session_summary_location_wise($jsondata){
	  $limit = '10';
	  if(isset($jsondata->limit)){
	       $limit = $jsondata->limit;
	  }
	  $offset = 0;
	  if(isset($jsondata->offset)){
	       $offset = $jsondata->offset;
	  }
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		
	  $from = date('Y-m-d', strtotime("$jsondata->from"));
	  $to = date('Y-m-d', strtotime("$jsondata->to"));
	  $previous_user = array();
	  $previous_session = $this->DB2->query("select userid  from wifi_user_free_session   WHERE  DATE(session_date) < '$from'   group by userid");
	  foreach($previous_session->result() as $row_previous){
	       $previous_user[] = $row_previous->userid;
	  }
	  $return_array = array();
	  $re_count_id = 0;
	  $location_filter_query = '';
	  if(count($jsondata->locations) > 0){
	      $locations = '';
	      $locations='"'.implode('", "', $jsondata->locations).'"';
	      $location_filter_query = " AND location_uid IN($locations) ";
	  }
	  $live_location_id = array();
	  
	  $query = $this->DB2->query("select id, location_name,nasipaddress from wifi_location where isp_uid = '$jsondata->isp_uid' and status = '1' and is_deleted = '0' $location_filter_query $state_where $city_where limit $offset,$limit");
	  foreach($query->result() as $row){
	       $previous_user_id_collection = $previous_user;
	       $location_id = $row->id;
	       $return_array[$re_count_id]["nasipaddress"]=$row->nasipaddress;
	       $return_array[$re_count_id]["location_name"]=$row->location_name;
	       $totalunique = 0;
	       $totalrepeat = 0;
	       $totalrepeat_session = 0;
	       
	       $totaluser = 0;
	       
	       $cond="";
	       $cond = "and date(wufs.session_date) between '$from' and '$to'";
	       // time filter
	       $time_filter = '';
	       if(count($jsondata->time_slot) > 0){
		    $time_or_query = '';
		    $t = 0;
		    foreach($jsondata->time_slot as $filt_time){
			 $time_sended = explode('-', $filt_time);
			 $time_from =  $time_sended[0];
			 $time_to =  $time_sended[1];
			 if($t == 0){
			      $time_or_query = " TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
			 }
			 else{
			      $time_or_query = $time_or_query." OR TIME(wufs.session_date) BETWEEN '$time_from' AND '$time_to' ";
			 }
			 $t++;
		    }
		    $time_filter = " AND ($time_or_query) ";
	       }
	       //gender filter
	       $gender_where = '';
	       if($jsondata->gender == 'm'){
		    $gender_where = " AND (wufs.gender = 'm' or wufs.gender = 'M' or wufs.gender = 'male' or wufs.gender = 'Male')";
	       }elseif($jsondata->gender == 'f'){
		    $gender_where = " AND (wufs.gender = 'f' or wufs.gender = 'F' or wufs.gender = 'female' or wufs.gender = 'Female')";
	       }
	       // age group filter
	       $age_group_filter = '';
	       if(count($jsondata->age_group) > 0){
		    $age_group_or_query = '';
		    $t = 0;
		    foreach($jsondata->age_group as $age_group_value){
				if($t == 0){
				    $age_group_or_query = " wufs.age_group = '$age_group_value' ";
				}
				else{
				    $age_group_or_query = $age_group_or_query." OR wufs.age_group = '$age_group_value' ";
				}
				$t++;
		    }
		    $age_group_filter = " AND ($age_group_or_query) ";
	       }
	       $chech_user = $this->DB2->query("select wufs.*  from wifi_user_free_session as wufs  WHERE wufs.location_id = '$location_id'  $cond
        $time_filter $gender_where $age_group_filter  order by wufs.session_date");
	       
	       $location_user_already_checked = array();
	       foreach($chech_user->result() as $chech_user_row){
		    //if(!in_array($chech_user_row->userid,$location_user_already_checked)){
			 if(in_array($chech_user_row->userid,$previous_user_id_collection))
			 {
			   $totalrepeat=$totalrepeat+1;
			 }
			 else
			 {
			     $totalunique=$totalunique+1;
			 }
			 $totaluser = $totaluser+1;
			 $previous_user_id_collection[]=$chech_user_row->userid;
		   // }else{
			 //$totalrepeat_array[] = $chech_user_row->userid;
		    //}
		    
	       }
	       
	       $return_array[$re_count_id]["totalunique"]=$totalunique;
	       $return_array[$re_count_id]["totalrepeat"]=$totalrepeat;
	       $return_array[$re_count_id]["totaluser"]=$totaluser;
	       $re_count_id++;
	       
	  }
	  //die;
	 // echo "<pre>";print_r($return_array);die;
	  return $return_array;
     }





     public function onehop_get_network_detail($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $this->DB2->select('*')->from('onehop_networks');
	  $this->DB2->where('location_uid', $location_uid);
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $row = $query->row_array();
	       $parent_network = $row['parent_network'];
	       if($parent_network > 0){
		    $this->DB2->select('*')->from('onehop_networks');
		    $this->DB2->where('id', $parent_network);
		    $query_parent = $this->DB2->get();
		    $network_name = '';$description = '';$address='';$latitude='';
		    $longitude = '';$max_ap = '';$coa_status = '';$coa_ip = '';
		    $coa_secret = '';$beacon_status = '';
		    if($query_parent->num_rows() > 0){
			 $row_parent = $query_parent->row_array();
			 $network_name = $row_parent['network_name'];
			 $description = $row_parent['description'];
			 $address = $row_parent['address'];
			 $latitude = $row_parent['latitude'];
			 $longitude = $row_parent['longitude'];
			 $max_ap = $row_parent['max_ap'];
			 $coa_status = $row_parent['coa_status'];
			 $coa_ip = $row_parent['coa_ip'];
			 $coa_secret = $row_parent['coa_secret'];
			 $beacon_status = $row_parent['beacon_status'];
		    }
		    $data['network_name'] = $network_name;
		    $data['description'] =  $description;
		    $data['address'] = $address;
		    $data['latitude'] = $latitude;
		    $data['longitude'] = $longitude;
		    $data['maxaps'] = $max_ap;
		    $data['coa_status'] = $coa_status;
		    $data['coa_ip'] = $coa_ip;
		    $data['coa_secret'] = $coa_secret;
		    $data['is_created'] = '1';
		    $data['beacon_status'] = $beacon_status;
		    $data['beacon_url'] = ONEHOP_BEACON_URL;
	       }else{
		    $data['network_name'] = $row['network_name'];
		    $data['description'] =  $row['description'];
		    $data['address'] = $row['address'];
		    $data['latitude'] = $row['latitude'];
		    $data['longitude'] = $row['longitude'];
		    $data['maxaps'] = $row['max_ap'];
		    $data['coa_status'] = $row['coa_status'];
		    $data['coa_ip'] = $row['coa_ip'];
		    $data['coa_secret'] = $row['coa_secret'];
		    $data['is_created'] = '1';
		    $data['beacon_status'] = $row['beacon_status'];
		    $data['beacon_url'] = ONEHOP_BEACON_URL;
	       }
	       
	  }else{
	       //get location name
	       $this->DB2->select('location_name,geo_address,latitude,longitude')->from('wifi_location');
	       $this->DB2->where('location_uid', $location_uid);
	       $get = $this->DB2->get();
	       $location_name = '';$geo_address = '';$latitude = ''; $longitude = '';
	       if($get->num_rows() > 0){
		    $row = $get->row_array();
		    $location_name = $row['location_name'];
		    $geo_address = $row['geo_address'];
		    $latitude = $row['latitude'];
		    $longitude = $row['longitude'];
	       }
	       $data['network_name'] = $location_name;
	       $data['description'] = $geo_address;
	       $data['address'] = $geo_address;
	       $data['latitude'] = $latitude;
	       $data['longitude'] = $longitude;
	       $data['maxaps'] = '16';
	       $data['coa_status'] = '1';
	       $data['coa_ip'] = AUTH_SERVER_IP;
	       $data['coa_secret'] = 'testing123';
	       $data['is_created'] = '0';
	       $data['beacon_status'] = '1';
	       $data['beacon_url'] = ONEHOP_BEACON_URL;
	  }
	  
        return json_encode($data);
    }
     public function onehop_get_network_create($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $network_name = $jsondata->network_name;
	  $description = $jsondata->description;
	  $maxaps = $jsondata->maxaps;
	  $coa_status = $jsondata->coa_status;
	  if($coa_status == ''){
	       $coa_status = '0';
	  }
	  $coa_ip = $jsondata->coa_ip;
	  $coa_secret = $jsondata->coa_secret;
	  $coa_ip = AUTH_SERVER_IP;
	  $coa_secret = 'testing123';
	  $address = $jsondata->address;
	  $latitude = $jsondata->latitude;
	  $longitude = $jsondata->longitude;
	  $service_name = $jsondata->service_name;
	  $beacon_status = 1;
	  if(isset($jsondata->beacon_status)){
	       $beacon_status = $jsondata->beacon_status;
	  }
	  $onehop_requestData = array(
		'Organization' => 'SHOUUT',
		'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
		'Service' => $service_name,
		'Name' => $network_name,
		'Description' => $description,
		'Address' => $address,
		'Longitude' => $latitude,
		'Latitude' => $longitude,
		'MaxAPs' => $maxaps,
		'CoA' => $coa_status,
		'CoAIP' => $coa_ip,
		'CoASecret' => $coa_secret,
		'ProbeTracking' => $beacon_status,
		'ProbeURL' => ONEHOP_BEACON_URL
	  );
	  //echo "<pre>";print_r($onehop_requestData);die;
	  $response = $this->onehop_wifiApi($onehop_requestData);
	  $response = json_decode($response);
	  if($response->ResponceCode == '200'){
	       if($service_name == 'NetworkCreate'){
		    $data['msg'] = 'Successfully created Network';
		    $data['code'] = '1';
	       }else{
		    $data['msg'] = 'Successfully Updated Network';
		    $data['code'] = '1';
	       }
	       
	       $this->DB2->select('location_uid,parent_network')->from('onehop_networks');
	       $this->DB2->where('location_uid', $location_uid);
	       $query = $this->DB2->get();
	       if($query->num_rows() > 0){
		    $row_parent = $query->row_array();
		    $parent_network = $row_parent['parent_network'];
		    if($parent_network > 0){
			 
			 $update_data = array(
			      'description' => $description,
			      'max_ap' => $maxaps,
			      'coa_status' => $coa_status,
			      'coa_ip' => $coa_ip,
			      'coa_secret' => $coa_secret,
			      'beacon_status' => $beacon_status,
			      'updated_on' => date('Y-m-d H:i:s'),
			 );
			 $this->DB2->where('id', $parent_network);
			 $this->DB2->update('onehop_networks', $update_data);
		    }else{
			 
			 $update_data = array(
			      'description' => $description,
			      'max_ap' => $maxaps,
			      'coa_status' => $coa_status,
			      'coa_ip' => $coa_ip,
			      'coa_secret' => $coa_secret,
			      'beacon_status' => $beacon_status,
			      'updated_on' => date('Y-m-d H:i:s'),
			 );
			 $this->DB2->where('location_uid', $location_uid);
			 $this->DB2->update('onehop_networks', $update_data);
		    }
		    
	       }else{
		    
		    $insert_data = array(
			 'location_uid' => $location_uid,
			 'organization' => 'SHOUUT',
			 'network_name' => $network_name,
			 'description' => $description,
			 'address' => $address,
			 'latitude' => $latitude,
			 'longitude' => $longitude,
			 'max_ap' => $maxaps,
			 'coa_status' => $coa_status,
			 'coa_ip' => $coa_ip,
			 'coa_secret' => $coa_secret, 
			 'created_on' => date('Y-m-d H:i:s'),
			 'beacon_status' => $beacon_status,
		    );
		    $insert = $this->DB2->insert('onehop_networks', $insert_data);
	       }
	  }else{
	       
	      if(isset($response->ReplyMessage) && $response->ReplyMessage != '')
	       {
		    $data['msg'] = $response->ReplyMessage;
	       }
	       else
	       {
		    $data['msg'] = 'Some error occure';
	       }
		   $data['code'] = '0';
	  }
	  return json_encode($data);
    }


    public function onehop_wifiApi($requestData){
	$service_url = "https://wlc.onehopnetworks.com/nbi/config";
	$curl = curl_init($service_url);
	$curl_post_data = json_encode($requestData);
	//print_r($curl_post_data);die;
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$curl_response = curl_exec($curl);
	//var_dump($curl_response);
	$tabledata=array("request"=>$curl_post_data,"response"=>$curl_response,"added_on"=>date("Y-m-d H:i:s"));
	$this->DB2->insert("onehop_response_log",$tabledata);
	if ($curl_response === false) {
	    $info = curl_getinfo($curl);
	    curl_close($curl);
	    die('error occured during curl exec. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
	return $curl_response;
    }
                public function onehop_add_apmac($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $onehop_ap_mac = $jsondata->onehop_ap_mac;
	  $onehop_ap_name = $jsondata->onehop_ap_name;
	  $data = array();
	  //check network is created or not
	  $check_network = $this->DB2->query("select id,network_name,parent_network from onehop_networks where location_uid = '$location_uid'");
	  if($check_network->num_rows() > 0){
	       $row_network = $check_network->row_array();
	       // check mac already exist or not
	       $check_mac = $this->DB2->query("select location_uid from onehop_macids where macid = '$onehop_ap_mac'");
	       if($check_mac->num_rows() > 0){
		    $row_mac = $check_mac->row_array();
		    if($location_uid == $row_mac['location_uid']){
			 $data['msg'] = 'Mac already added';
			 $data['code'] = '0';
		    }else{
			$data['msg'] = 'Mac already added with other location'; 
			 $data['code'] = '0';
		    }
	       }else{
		    $network_name = $row_network['network_name'];
		    $parent_network = $row_network['parent_network'];
		    if($parent_network > 0){
			 $check_network_new = $this->DB2->query("select id,network_name,parent_network from onehop_networks where id = '$parent_network'");
			 if($check_network_new->num_rows() > 0){
			      $row_network_new = $check_network_new->row_array();
			      $network_name = $row_network_new['network_name'];
			 }
		    }
		    $onehop_requestData = array(
			      'Organization' => 'SHOUUT',
			      'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
			      'Service' => 'APAdd',
			      'Network' => $network_name,
			      'APMAC' => $onehop_ap_mac,
			      'APName' => $onehop_ap_name,
			      
		    );
		    $response = $this->onehop_wifiApi($onehop_requestData);
		    $response = json_decode($response);
		    //print_r($response);die;
		    if($response->ResponceCode == '200'){
			 $insert_data = array(
			      'location_uid' => $location_uid,
			      'macid' => $onehop_ap_mac,
			      'apmac_name' => $onehop_ap_name,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert = $this->DB2->insert('onehop_macids', $insert_data);
			 $data['msg'] = 'Mac successfully added';
			 $data['code'] = '1';
		    }else{
			 if(isset($response->ReplyMessage)){
			      $data['msg'] = $response->ReplyMessage;
			 }else{
			      $data['msg'] = 'Sone error occure';
			 }
			 
			 $data['code'] = '0';
		    }
	       }
	  }else{
	       $data['code'] = '0'; 
	       $data['msg'] = 'Please first create network'; 
	  }
	  
	  return json_encode($data);
    }
    
    


     public function update_cp_ssid_onehop($location_uid, $onehop_ssid_name,$custom_captive_url = ''){
	  $data = array();
	  $onehop_ssid_index = '0';
	  $parent_network = 0;
	  $check_parent = $this->DB2->query("select parent_network from onehop_networks where location_uid = '$location_uid'");
	  if($check_parent->num_rows() > 0){
	       $check_parent_row = $check_parent->row_array();
	       $parent_network = $check_parent_row['parent_network'];
	  }
	  if($parent_network > 0){
	       $check_network = $this->DB2->query("select id,network_name from onehop_networks where id = '$parent_network'");
	  }else{
	       $check_network = $this->DB2->query("select id,network_name from onehop_networks where location_uid = '$location_uid'");     
	  }
	  // check main ssid or child ssid
	  $parent_ssid = 0;
	  $check_ssid_parent = $this->DB2->query("select parent_ssid from onehop_ssid where location_uid = '$location_uid'");
	  if($check_ssid_parent->num_rows() > 0){
	       $check_ssid_parent_row = $check_ssid_parent->row_array();
	       $parent_ssid = $check_ssid_parent_row['parent_ssid'];
	  }
	  if($check_network->num_rows() > 0){
	       $row_network = $check_network->row_array();
	       //check ssid is unique
	       if($parent_ssid > 0){
		    $check_ssid = $this->DB2->query("select ssid_name from onehop_ssid where ssid_name = '$onehop_ssid_name' AND id = '$parent_ssid' AND ssid_index != '$onehop_ssid_index' and is_deleted = '0'");
	       }else{
		    $check_ssid = $this->DB2->query("select ssid_name from onehop_ssid where ssid_name = '$onehop_ssid_name' AND location_uid = '$location_uid' AND ssid_index != '$onehop_ssid_index' and is_deleted = '0'");
	       }
	       
	       if($check_ssid->num_rows() > 0){
		    $data['msg'] = 'SSID Name already created';
		    $data['code'] = '0'; 
	       }else{
		    
		    $onehop_requestData = array(
			 'Organization' => 'SHOUUT',
			 'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
			 'Service' => 'SSIDAdd',
			 'Network' => trim($row_network['network_name']),
			 'SSIDIndex' => $onehop_ssid_index,
			 'SSIDName' => $onehop_ssid_name,
			 
			 
		    );
		    if($custom_captive_url != '')
		    {
			$arr1 = array("CPURL"=> $custom_captive_url);
			$onehop_requestData=array_merge($onehop_requestData,$arr1);
			
		    }
		    $response = $this->onehop_wifiApi($onehop_requestData);
		    $response = json_decode($response);
		    if($response->ResponceCode == '200'){
			 if($parent_ssid > 0){
			      $this->DB2->set('ssid_name', $onehop_ssid_name);
			      $this->DB2->where('id', $parent_ssid);
			      $this->DB2->where('ssid_index', $onehop_ssid_index);
			      $this->DB2->where('is_deleted', '0');
			      $this->DB2->update('onehop_ssid');
			      // get all child location
			      $locations_uid_array = array();
			      $get_lo = $this->DB2->query("select location_uid from onehop_ssid where (parent_ssid = '$parent_ssid' OR id = '$parent_ssid')");
			      foreach($get_lo->result() as $row_loc){
				   $locations_uid_array[] =  $row_loc->location_uid;
			      }
			      //$locations_uid_array = '"'.implode('", "', $locations_uid_array).'"';
			      if(count($locations_uid_array) > 0)
			      {
				   $this->DB2->set('main_ssid', $onehop_ssid_name);
				   $this->DB2->where_in('uid_location', $locations_uid_array);
				   $this->DB2->update('wifi_location_cptype');
			      }
			 }else{
			      $this->DB2->set('ssid_name', $onehop_ssid_name);
			      $this->DB2->where('location_uid', $location_uid);
			      $this->DB2->where('ssid_index', $onehop_ssid_index);
			      $this->DB2->where('is_deleted', '0');
			      $this->DB2->update('onehop_ssid');
			 }
			 
		    }
	       }
	  }
	  
	  return $data;
     }
     
     public function onehop_add_ssid($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $onehop_ssid_index = $jsondata->onehop_ssid_index;
	  $onehop_ssid_name = $jsondata->onehop_ssid_name;
	  $onehop_association= $jsondata->onehop_association;
	  $onehop_wap_mode = $jsondata->onehop_wap_mode;
	  $onehop_forwarding = $jsondata->onehop_forwarding;
	  $onehop_cp_mode = $jsondata->onehop_cp_mode;
	  $onehop_cp_url = $jsondata->onehop_cp_url;
	  $onehop_auth_server_ip = $jsondata->onehop_auth_server_ip;
	  $onehop_auth_server_port = $jsondata->onehop_auth_server_port;
	  $onehop_auth_server_secret = $jsondata->onehop_auth_server_secret;
	  $onehop_accounting = $jsondata->onehop_accounting;
	  $onehop_acc_server_ip = $jsondata->onehop_acc_server_ip;
	  $onehop_acc_server_port = $jsondata->onehop_acc_server_port;
	  $onehop_acc_server_secret = $jsondata->onehop_acc_server_secret;
	  $onehop_acc_interval = $jsondata->onehop_acc_interval;
	  $onehop_psk = $jsondata->onehop_psk;
	  $onehop_wallgarden_ip_list = $jsondata->onehop_wallgarden_ip_list;
	  $onehop_wallgarden_domain_list = $jsondata->onehop_wallgarden_domain_list;
	  // check main ssid or parent chind ssid
	  $check_parent = $this->DB2->query("select parent_ssid from onehop_ssid where location_uid = '$location_uid'");
	  if($check_parent->num_rows() > 0){
	       $check_parent_row = $check_parent->row_array();
	       $parent_ssid = $check_parent_row['parent_ssid'];
	       if($parent_ssid > 0){
		    // get parent location uid
		    $get = $this->DB2->query("select location_uid from onehop_ssid where id = '$parent_ssid'");
		    if($get->num_rows() > 0){
			 $row_get = $get->row_array();
			 $location_uid = $row_get['location_uid'];
		    }
	       }
	  }
	 $data = array();
	 // chek is parent network or child
	 $parent_network = 0;
	 $network_id = 0;
	  $check_parent_net = $this->DB2->query("select id,parent_network from onehop_networks where location_uid = '$location_uid'");
	  if($check_parent_net->num_rows() > 0){
	       $check_parent_net_row = $check_parent_net->row_array();
	       $parent_network = $check_parent_net_row['parent_network'];
	       $network_id = $check_parent_net_row['id'];
	  }
	  $group_loc_uid = array();
	  if($parent_network > 0){
	       $check_network = $this->DB2->query("select id,network_name from onehop_networks where id = '$parent_network'");
	       
	       // get all group location
	       $get_group = $this->DB2->query("select location_uid from onehop_networks where (id='$parent_network' OR parent_network = '$parent_network') ");
	       foreach($get_group->result() as $row_g){
		    if($location_uid != $row_g->location_uid){
			$group_loc_uid[] = $row_g->location_uid; 
		    }
		    
	       }
	       
	  }else{
	       $check_network = $this->DB2->query("select id,network_name from onehop_networks where location_uid = '$location_uid'");
	       // get all group location
	       if($network_id > 0){
		    $get_group = $this->DB2->query("select location_uid from onehop_networks where (id='$network_id' OR parent_network = '$network_id') ");
		    foreach($get_group->result() as $row_g){
			 if($location_uid != $row_g->location_uid){
			      $group_loc_uid[] = $row_g->location_uid; 
			 }
		    }    
	       }
	       
	  }
	  if($check_network->num_rows() > 0){
	       $row_network = $check_network->row_array();
	       //check ssid is unique
	       $check_ssid = $this->DB2->query("select ssid_name from onehop_ssid where ssid_name = '$onehop_ssid_name' AND location_uid = '$location_uid' AND ssid_index != '$onehop_ssid_index' and is_deleted = '0'");
	       if($check_ssid->num_rows() > 0){
		    $data['msg'] = 'SSID Name already created';
		    $data['code'] = '0'; 
	       }else{
		    $onehop_requestData = array(
			 'Organization' => 'SHOUUT',
			 'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
			 'Service' => 'SSIDAdd',
			 'Network' => trim($row_network['network_name']),
			 'SSIDIndex' => $onehop_ssid_index,
			 'SSIDName' => $onehop_ssid_name,
			 'Association' => $onehop_association,
			 'WPAMode' => $onehop_wap_mode,
			 'Forwarding' => $onehop_forwarding,
			 'CPMode' => $onehop_cp_mode,
			 'CPURL' => $onehop_cp_url,
			 'WGIPList' => implode(' ',explode(',',$onehop_wallgarden_ip_list)),
			 'WGDomainList' => implode(' ',explode(',',$onehop_wallgarden_domain_list)),
			 
			 'AuthServerIP' => $onehop_auth_server_ip,
			 'AuthServerPort' => $onehop_auth_server_port,
			 'AuthServerSecret' => $onehop_auth_server_secret,
			 'Accounting' => $onehop_accounting,
			 'AcctServerIP' => $onehop_acc_server_ip,
			 'AcctServerPort' => $onehop_acc_server_port,
			 'AcctServerSecret' => $onehop_acc_server_secret,
			 'AcctInterval' => $onehop_acc_interval,
			 'PSK' => $onehop_psk,
		    );
		   // echo "<pre>";print_r($onehop_requestData);
		    $response = $this->onehop_wifiApi($onehop_requestData);
		    $response = json_decode($response);
		    //print_r($response);die;
		    if($response->ResponceCode == '200'){
			 // insert or update
			 $check = $this->DB2->query("select ssid_name from onehop_ssid where  location_uid = '$location_uid' AND ssid_index = '$onehop_ssid_index' and is_deleted = '0'");
			 if($check->num_rows() > 0){
			      $update_data = array(
				   'ssid_name' => $onehop_ssid_name,
				   'association' => $onehop_association,
				   'wap_mode' => $onehop_wap_mode,
				   'forwarding' => $onehop_forwarding,
				   'cp_mode' => $onehop_cp_mode,
				   'cp_url' => $onehop_cp_url,
				   'auth_server_ip' => $onehop_auth_server_ip,
				   'auth_server_port' => $onehop_auth_server_port,
				   'auth_server_secret' => $onehop_auth_server_secret,
				   'accounting' => $onehop_accounting,
				   'acc_server_ip' => $onehop_acc_server_ip,
				   'acc_server_port' => $onehop_acc_server_port,
				   'acc_server_secret' => $onehop_acc_server_secret,
				   'acc_interval' => $onehop_acc_interval,
				   'psk' => $onehop_psk,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
			      $this->DB2->where('location_uid', $location_uid);
			      $this->DB2->where('ssid_index', $onehop_ssid_index);
			      $this->DB2->where('is_deleted', '0');
			      $this->DB2->update('onehop_ssid', $update_data);
			 }else{
			      $insert_data = array(
				   'location_uid' => $location_uid,
				   'ssid_index' => $onehop_ssid_index,
				   'ssid_name' => $onehop_ssid_name,
				   'association' => $onehop_association,
				   'wap_mode' => $onehop_wap_mode,
				   'forwarding' => $onehop_forwarding,
				   'cp_mode' => $onehop_cp_mode,
				   'cp_url' => $onehop_cp_url,
				   'auth_server_ip' => $onehop_auth_server_ip,
				   'auth_server_port' => $onehop_auth_server_port,
				   'auth_server_secret' => $onehop_auth_server_secret,
				   'accounting' => $onehop_accounting,
				   'acc_server_ip' => $onehop_acc_server_ip,
				   'acc_server_port' => $onehop_acc_server_port,
				   'acc_server_secret' => $onehop_acc_server_secret,
				   'acc_interval' => $onehop_acc_interval,
				   'psk' => $onehop_psk,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
			      $insert = $this->DB2->insert('onehop_ssid', $insert_data);
			      
			      $parent_ssid_create = $this->DB2->insert_id();
			      // create for all related child
			      foreach($group_loc_uid as $group_loc_uid1){
				   $insert_data = array(
					'location_uid' => $group_loc_uid1,
					'parent_ssid' => $parent_ssid_create,
					'created_on' => date('Y-m-d H:i:s'),
				   );
				   $insert = $this->DB2->insert('onehop_ssid', $insert_data);
			      }
			      //$locations_uid_array = '"'.implode('", "', $group_loc_uid).'"';
			      if(count($group_loc_uid) > 0)
			      {
				   $this->DB2->set('main_ssid', $onehop_ssid_name);
				   $this->DB2->where_in('uid_location', $group_loc_uid);
				   $update = $this->DB2->update('wifi_location_cptype');
			      }
			 }
			 //get previous
			 $previous_wall = array();
			 $get_previous = $this->DB2->query("select wall_ip from onehop_wall_list where location_uid = '$location_uid' and index_id = '$onehop_ssid_index' and is_deleted = '0' and is_domain = '0'");
			 foreach($get_previous->result() as $get_previous1){
			      $previous_wall[] = $get_previous1->wall_ip;
			 }
			 // insert into wall garden list
			 if($onehop_wallgarden_ip_list != ''){
			      $onehop_wallgarden_ip_list = explode(',',$onehop_wallgarden_ip_list);
			      foreach($onehop_wallgarden_ip_list as $onehop_wallgarden_ip_list1){
				   //echo $onehop_wallgarden_ip_list1;
				   if($onehop_wallgarden_ip_list1 != ''){
					if(in_array($onehop_wallgarden_ip_list1, $previous_wall)){
					    $key = array_search($onehop_wallgarden_ip_list1,$previous_wall);
					     unset($previous_wall[$key]);
					}else{
					     $insert_data = array(
						  'location_uid' => $location_uid,
						  'wall_ip' => $onehop_wallgarden_ip_list1,
						  'created_on' => date('Y-m-d H:i:s'),
						  'index_id' => $onehop_ssid_index,
					     );
					     $insert = $this->DB2->insert('onehop_wall_list', $insert_data);
					}
					
				   }
				   
	      
			      }
			      foreach($previous_wall as $previous_wall_new){
				   $this->DB2->set('is_deleted', '1');
				   $this->DB2->where('location_uid', $location_uid);
				   $this->DB2->where('index_id', $onehop_ssid_index);
				   $this->DB2->where('wall_ip', $previous_wall_new);
				   $update = $this->DB2->update('onehop_wall_list');
			      }
			 }
			 //get previous domain
			 $previous_wall_domain = array();
			 $get_previous_domain = $this->DB2->query("select wall_ip from onehop_wall_list where location_uid = '$location_uid' and index_id = '$onehop_ssid_index' and is_deleted = '0' and is_domain = '1'");
			 foreach($get_previous_domain->result() as $get_previous_domain1){
			      $previous_wall_domain[] = $get_previous_domain1->wall_ip;
			 }
			 // insert into wall garden list
			 if($onehop_wallgarden_domain_list != ''){
			      $onehop_wallgarden_domain_list = explode(',',$onehop_wallgarden_domain_list);
			      foreach($onehop_wallgarden_domain_list as $onehop_wallgarden_domain_list1){
				   //echo $onehop_wallgarden_ip_list1;
				   if($onehop_wallgarden_domain_list1 != ''){
					if(in_array($onehop_wallgarden_domain_list1, $previous_wall_domain)){
					    $key = array_search($onehop_wallgarden_domain_list1,$previous_wall_domain);
					     unset($previous_wall_domain[$key]);
					}else{
					    
					     $insert_data = array(
						  'location_uid' => $location_uid,
						  'wall_ip' => $onehop_wallgarden_domain_list1,
						  'created_on' => date('Y-m-d H:i:s'),
						  'index_id' => $onehop_ssid_index,
						  'is_domain' => '1',
					     );
					     $insert = $this->DB2->insert('onehop_wall_list', $insert_data);
					}
					
				   }
				   
	      
			      }
			      foreach($previous_wall_domain as $previous_wall_new_domain){
				   $this->DB2->set('is_deleted', '1');
				   $this->DB2->where('location_uid', $location_uid);
				   $this->DB2->where('index_id', $onehop_ssid_index);
				   $this->DB2->where('wall_ip', $previous_wall_new_domain);
				   $update = $this->DB2->update('onehop_wall_list');
			      }
			 }
			 $data['msg'] = 'Success';
			 $data['code'] = '1'; 
		    }else{
			 $data['msg'] = 'Some error occure';
			 $data['code'] = '0'; 
		    }
		    
		    
	       }
	       
	  }else{
	       $data['msg'] = 'Please first create network'; 
		   $data['code'] = '0'; 
	  }
	  
	  
	  
	  return json_encode($data);
    }
    


        
    


      public function onehop_get_ssid_detail($jsondata){
	  $location_uid = trim($jsondata->location_uid);
	  $index_number = $jsondata->index_number;
	  $data = array();
	  // check is main ssid or sub ssid
	  $parent_ssid = 0;
	  $this->DB2->select('parent_ssid')->from('onehop_ssid');
	  $this->DB2->where('location_uid', $location_uid);
	  $check_is_main = $this->DB2->get();
	  if($check_is_main->num_rows() > 0){
	       $check_is_main_row = $check_is_main->row_array();
	       $parent_ssid = $check_is_main_row['parent_ssid'];
	       
	  }
	  if($parent_ssid > 0){
	       $this->DB2->select('*')->from('onehop_ssid');
	       $this->DB2->where(array('id' => $parent_ssid, 'ssid_index'=>$index_number, 'is_deleted' => '0'));
	       $check = $this->DB2->get();
	  }else{
	       $this->DB2->select('*')->from('onehop_ssid');
	       $this->DB2->where(array('location_uid' => $location_uid, 'ssid_index'=>$index_number, 'is_deleted' => '0'));
	       $check = $this->DB2->get();
	  }
	  
	  if($check->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $row = $check->row_array();
	       $location_uid = $row['location_uid'];
	       $data['ssid_name'] = $row['ssid_name'];
	       $data['association'] = $row['association'];
	       $data['wap_mode'] = $row['wap_mode'];
	       $data['forwarding'] = $row['forwarding'];
	       $data['cp_mode'] = $row['cp_mode'];
	       $data['cp_url'] = $row['cp_url'];
	       $data['auth_server_ip'] = $row['auth_server_ip'];
	       $data['auth_server_port'] = $row['auth_server_port'];
	       $data['auth_server_secret'] = $row['auth_server_secret'];
	       $data['accounting'] = $row['accounting'];
	       $data['acc_server_ip'] = $row['acc_server_ip'];
	       $data['acc_server_port'] = $row['acc_server_port'];
	       $data['acc_server_secret'] = $row['acc_server_secret'];
	       $data['acc_interval'] = $row['acc_interval'];
	       $data['psk'] = $row['psk'];
	       $wall_list = array();
	       
	       $this->DB2->select('*')->from('onehop_wall_list');
	       $this->DB2->where(array('location_uid' => $location_uid, 'index_id'=>$index_number, 'is_deleted' => '0', 'is_domain' => '0'));
	       $get_wall_list = $this->DB2->get();
	       if($get_wall_list->num_rows() > 0){
		    $i = 0;
		    foreach($get_wall_list->result() as $get_wall_list1){
			 $wall_list[$i]['ip'] = $get_wall_list1->wall_ip;
			 $i++;
		    }
	       }
	       $data['wall_list'] = $wall_list;
	       
	       $wall_domain_list = array();
	       $this->DB2->select('*')->from('onehop_wall_list');
	       $this->DB2->where(array('location_uid' => $location_uid, 'index_id'=>$index_number, 'is_deleted' => '1', 'is_domain' => '1'));
	       $get_wall_domain_list = $this->DB2->get();
	       if($get_wall_domain_list->num_rows() > 0){
		    $i = 0;
		    foreach($get_wall_domain_list->result() as $get_wall_domain_list1){
			 $wall_domain_list[$i]['domain'] = $get_wall_domain_list1->wall_ip;
			 $i++;
		    }
	       }
	       $data['wall_domain_list'] = $wall_domain_list;
	  }
	  else{
	       $isp_uid = 0;
	       $this->DB2->select('main_ssid, isp_uid')->from('wifi_location_cptype');
	       $this->DB2->where('uid_location' ,$location_uid);
	       $get_ssid_name = $this->DB2->get();
	       $ssid_name = 'SHOUUT_FREE_WIFI';
	       if($get_ssid_name->num_rows() > 0){
		    $row = $get_ssid_name->row_array();
		    if($row['main_ssid'] != ''){
			 $ssid_name = $row['main_ssid'];
		    }
		    $isp_uid = $row['isp_uid'];
	       }
	       $data['ssid_name'] = $ssid_name;
	       
	       $data['auth_server_ip'] = AUTH_SERVER_IP;
               $data['acc_server_ip'] = AUTH_SERVER_IP;
               $wall_list = array();
                    $wall_list[0]['ip'] = WHITELIST_IP1;
                    $wall_list[1]['ip'] = WHITELIST_IP2;
		    // subdomain
		    $sub_domain_ip = '';
		    $isp_domain_name = '';
		    
		    $this->db->select('subdomain_ip,isp_domain_name')->from('sht_isp_admin');
		    $this->db->where('isp_uid' , $isp_uid);
		    $query_sub = $this->db->get();
		    if($query_sub->num_rows() > 0){
			 $sub_row = $query_sub->row_array();
			 $sub_domain_ip = $sub_row['subdomain_ip'];
			 $isp_domain_name = $sub_row['isp_domain_name'];
			 if($sub_domain_ip != ''){
			      $wall_list[2]['ip'] = $sub_domain_ip; 
			 }
		    }
               $data['wall_list'] = $wall_list;
               $wall_domain_list = array();
                    $wall_domain_list[0]['domain'] = 'cdn101.shouut.com';
                    $wall_domain_list[1]['domain'] = 's3-ap-southeast-1.amazonaws.com';
                    $wall_domain_list[2]['domain'] = 'secure4.arcot.com';
                    $wall_domain_list[3]['domain'] = '3dsecure.payseal.com';
                    $wall_domain_list[4]['domain'] = 'static2.paytm.in';
                    $wall_domain_list[5]['domain'] = 'static3.paytm.in';
                    $wall_domain_list[6]['domain'] = 'static1.paytm.in';
                    $wall_domain_list[7]['domain'] = 'static4.paytm.in';
                    $wall_domain_list[8]['domain'] = 'secure.paytm.in';
                    $wall_domain_list[9]['domain'] = 'accounts-uat.paytm.com';
                    $wall_domain_list[10]['domain'] = 'pguat.paytm.com';
                    $wall_domain_list[11]['domain'] = 'facebook.com';
                    $wall_domain_list[12]['domain'] = 'fbcdn.net';
                    $wall_domain_list[13]['domain'] = 'facebook.net';
                    $wall_domain_list[14]['domain'] = 'maps.googleapis.com';
                    $wall_domain_list[15]['domain'] = 'akamaihd.net';
                    $wall_domain_list[16]['domain'] = 'decibel.shouut.com';
                    $wall_domain_list[17]['domain'] = 'connect.facebook.net';
                    $wall_domain_list[18]['domain'] = 'fbstatic-a.akamaihd.net';
                    $wall_domain_list[19]['domain'] = 'm.facebood.com';
		    $wall_domain_list[20]['domain'] = 'd392o9g87c202y.cloudfront.net';
		    $wall_domain_list[21]['domain'] = 'cloudfront.net';
		    $wall_domain_list[22]['domain'] = $isp_domain_name;
               $data['wall_domain_list'] = $wall_domain_list;
	       
	       $data['resultCode'] = '0';
	  }
	  return json_encode($data);
    }



    public function onehop_delete_ssid($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $onehop_ssid_index = $jsondata->onehop_ssid_index;
	  $onehop_ssid_name = $jsondata->onehop_ssid_name;
	  $data = array();
	  $this->DB2->select('id,network_name')->from('onehop_networks');
	  $this->DB2->where('location_uid', $location_uid);
	  $check_network = $this->DB2->get();
	  if($check_network->num_rows() > 0){
	       $row_network = $check_network->row_array();
	     
		    $onehop_requestData = array(
			 'Organization' => 'SHOUUT',
			 'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
			 'Service' => 'SSIDDelete',
			 'Network' => trim($row_network['network_name']),
			 'SSIDIndex' => $onehop_ssid_index,
			 
		    );
		    //echo "<pre>";print_r($onehop_requestData);die;
		    $response = $this->onehop_wifiApi($onehop_requestData);
		    $response = json_decode($response);
		    //print_r($response);die;
		    if($response->ResponceCode == '200'){
			 $this->DB2->set('is_deleted', '1');
			 $this->DB2->where('location_uid', $location_uid);
			 $this->DB2->where('ssid_index', $onehop_ssid_index);
			 $update = $this->DB2->update('onehop_ssid');
			 
			
			 $this->DB2->set('is_deleted', '1');
			 $this->DB2->where('location_uid', $location_uid);
			 $this->DB2->where('index_id', $onehop_ssid_index);
			 $update1 = $this->DB2->update('onehop_wall_list');
			 $data['msg'] = 'Success'; 
		    }else{
			 $data['msg'] = 'Some error occure'; 
		    }
		    
		    
	       
	       
	  }else{
	       $data['msg'] = 'Please first create network'; 
	  }
	  
	  
	  
	  return json_encode($data);
    }
    
    public function plan_detail($jsondata){
	  $plan_id = $jsondata->plan_id;
	  $data = array();
	  $get = $this->DB2->query("select plantype, downrate, uprate, timelimit, datalimit from sht_services where srvid = '$plan_id'");
	  if($get->num_rows() > 0){
	       $row = $get->row_array();
	       $data['plan_type'] = $row['plantype'];
	       /*$data['download_rate'] = ceil($row['downrate']/1024);
	       $data['upload_rate'] = ceil($row['uprate']/1024);
	       $data['datalimit'] = ceil($row['datalimit']/(1024*1024));*/
	       $data['download_rate'] = ceil($row['downrate']/(1024*1024));
	       $data['upload_rate'] = ceil($row['uprate']/(1024*1024));
	       $data['datalimit'] = ceil($row['datalimit']/(1024*1024));
	       if($row['timelimit'] != ''){
		    $data['timelimit'] = $row['timelimit']/60;
	       }else{
		 $data['timelimit'] = '0';   
	       }
	       
	       $data['code'] = '1'; 
	  }else{
	      $data['code'] = '0';  
	  }
	  
	  
	  
	  return json_encode($data);
    }
    public function check_plan_already_attached($jsondata){
	  $plan_id = $jsondata->plan_id;
	  $data = array();
	  
	  $query = $this->DB2->query("select srvname, descr from sht_services where srvid = '$plan_id'");
	  if($query->num_rows() > 0){
	       $row = $query->row_array();
	       $data['plan_name'] = $row['srvname'];
	       $data['plan_desc'] =  $row['descr'];
	       $data['code'] = '1';
	  }else{
	       $data['code'] = '1';
	  }
	  $plan_associated_other_location = '0';
	  /*$check = $this->DB2->query("select cp1_plan, cp2_plan, cp3_hybrid_plan, cp3_data_plan, cafe_plan");
	  if($check->num_rows() > 0){
	       
	  }*/
	  
        return json_encode($data);
    }
     public function save_plan($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $plan_id = $jsondata->plan_id;
	  $data_limit = $jsondata->data_limit;
	  $time_limit = $jsondata->time_limit;
	  $plan_name = $jsondata->plan_name;
	  $plan_desc = $jsondata->plan_desc;
	  $downrate = $jsondata->downrate;
	  $uprate = $jsondata->uprate;
	  $plan_type = '';
	  if($jsondata->plan_type != ''){
	       $plan_type = $jsondata->plan_type;
	  }else{
	       if($data_limit == ''){
		    $plan_type = '2';// time plan
	       }elseif($time_limit == ''){
		    $plan_type = '4';
	       }else{
		    $plan_type = '5';
	       }
	  }
	  
	  $data = array();
	  //$downrate = $downrate*1024;
	  $downrate = $downrate*1024*1024;
	  //$uprate = $uprate*1024;
	  $uprate = $uprate*1024*1024;
	  $data_limit = $data_limit*(1024*1024);
	  $time_limit = $time_limit*60;
	  $created_plan_id = '0';
	  if($plan_id != ''){
	       $update_data = array(
		    'srvname' => $plan_name,
		    'descr' => $plan_desc,
		    'downrate' => $downrate,
		    'uprate' => $uprate,
		    'timelimit' => $time_limit,
		    'datalimit' => $data_limit,
	       );
	       $this->DB2->where('srvid', $plan_id);
	       $query = $this->DB2->update('sht_services', $update_data);
	       //echo $this->DB2->last_query();die;
	       $data['code'] = '1';
	  }else{
	       $data['code'] = '1';
	       $insert_data = array(
		    'srvname' => $plan_name,
		    'descr' => $plan_desc,
		    'plantype' => $plan_type,
		    'downrate' => $downrate,
		    'uprate' => $uprate,
		    'timelimit' => $time_limit,
		    'datalimit' => $data_limit,
		    'isp_uid' => $isp_uid,
		    'enableplan' => '1',
	       );
	       $query = $this->DB2->insert('sht_services', $insert_data);
	       $plan_id_created = $this->DB2->insert_id();
	       $created_plan_id = $plan_id_created;
	       $insert_data = array(
		    'plan_id' => $plan_id_created,
		    'plan_detail' => '1',
		    'plan_setting' => '1',
		    'plan_pricing' => '1',
		    'plan_region' => '1',
		    'forced_disable' => '0',
	       );
	       $query1 = $this->DB2->insert('sht_plan_activation_panel', $insert_data);
	  }
	  $plan_associated_other_location = '0';
	  /*$check = $this->DB2->query("select cp1_plan, cp2_plan, cp3_hybrid_plan, cp3_data_plan, cafe_plan");
	  if($check->num_rows() > 0){
	       
	  }*/
	  $data['created_plan_id'] = $created_plan_id;
        return json_encode($data);
    }

     public function get_country_code($isp_uid){
	  $country_code = '91';
	  $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$isp_uid."'");
	  if($ispcountryQ->num_rows() > 0){
	       $crowdata = $ispcountryQ->row();
	       $countryid = $crowdata->country_id;
	       $countryQ = $this->db->query("SELECT phonecode FROM sht_countries WHERE id='".$countryid."'");
	       if($countryQ->num_rows() > 0){
		    $rowdata = $countryQ->row();
		    $country_code = $rowdata->phonecode;
	       }
	       
	  }
	  return $country_code;
    }
    
    
         public function new_fun(){
	  $timestamp = strtotime(date("Y-m-d H:m:s"));
	 
	  $onehop_requestData = array(
		    "APMAC"  => "98:de:d0:fd:da:d8",
		    "ClientMAC" =>  "C0:EE:FB:70:0E:B3",
		    "Timestamp" =>  $timestamp,
		    "Band" => "5GHz",
		    "Channel" =>  '1',
		    "Signal" =>  "100",
		    "Connected" => 'True',
	  );
	  //echo "<pre>";print_r($onehop_requestData);die;
	  $response = $this->onehop_wifiApi_new($onehop_requestData);
	  $response = json_decode($response);
	  
    }

    public function onehop_wifiApi_new($requestData){
	$service_url = "https://wlc.onehopnetworks.com/nbi/config";
	$curl = curl_init($service_url);
	$curl_post_data = json_encode($requestData);
	//print_r($curl_post_data);die;
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	$curl_response = curl_exec($curl);
	print_r($curl_response);die;
	//var_dump($curl_response);
	if ($curl_response === false) {
	    $info = curl_getinfo($curl);
	    curl_close($curl);
	    die('error occured during curl exec. Additioanl info: ' . var_export($info));
	}
	curl_close($curl);
	return $curl_response;
    }
public function terms_of_use($requestData){
	$location_uid = $requestData->location_uid;
	$data = array();
	$query = $this->DB2->query("select terms_of_use from wifi_location_cptype where uid_location = '$location_uid'");
	if($query->num_rows() > 0){
	  $row = $query->row_array();
	  $data['content'] = $row['terms_of_use'];
	}else{
	  $data['content'] = '';
	}
	
	return $data;
    }
    public function update_terms_of_use($requestData){
	$location_uid = $requestData->location_uid;
	$text_value = $requestData->text_value;
	$data = array();
	$query = $this->DB2->query("select terms_of_use from wifi_location_cptype where uid_location = '$location_uid'");
	
	if($query->num_rows() > 0){
	  $tabledata=array("terms_of_use"=>$text_value);
          $this->DB2->update('wifi_location_cptype', $tabledata,array("uid_location"=>$location_uid));
	  $data['result_msg'] = 'successfully updated';
	}else{
	  $tabledata=array("terms_of_use"=>$text_value, "uid_location"=>$location_uid);
          $this->DB2->insert('wifi_location_cptype',$tabledata);
	  $data['result_msg'] = 'successfully inserted';
	}
	
	return $data;
    }
    
    public function current_image_path($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $query_cp = $this->DB2->query("select original_image,retail_original_image,custom_slider_image1,custom_slider_image2 from wifi_location_cptype where uid_location = '$location_uid'");
	  if($query_cp->num_rows() > 0){
	       $row_cp = $query_cp->row_array();
		    $original_image_path = '';
		    if($row_cp['original_image'] != ''){
			 $original_image_path = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_location_logo/original/'.$row_cp['original_image'];
		    }
		    $data['logo_image'] = $original_image_path;
			   $retail_original_image_path = '';
			   if($row_cp['retail_original_image'] != ''){
				    $retail_original_image_path = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$row_cp['retail_original_image'];
			   }
			   $data['offer_image'] = $retail_original_image_path;
			   
			  $slider1_image = '';
			   if($row_cp['custom_slider_image1'] != ''){
				    $slider1_image = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$row_cp['custom_slider_image1'];
			   }
			   $data['slider1_image'] = $slider1_image;
			   
			   $slider2_image = '';
			   if($row_cp['custom_slider_image2'] != ''){
				    $slider2_image = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$row_cp['custom_slider_image2'];
			   }
			   $data['slider2_image'] = $slider2_image;
			   
			   $data['code'] = 1;
		  }else{
			   $data['code'] = 0;
		  }
        return json_encode($data);
    }

     public function current_cp_path($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $query_cp = $this->DB2->query("select location_type, location_name,id,nasconfigure,isp_uid,bitly_url from wifi_location  where location_uid = '$location_uid'");
	   if($query_cp->num_rows() > 0){
	       $row_cp = $query_cp->row_array();
	       if($row_cp['nasconfigure'] == '1' || $row_cp['bitly_url'] != ''){
		    $isp_uid = $row_cp['isp_uid'];
		    $location_id = $row_cp['id'];
		    $requestData = array(
		     'location_id' => $location_id,
		     'isp_uid' => $isp_uid
		    );
		    $data_request = json_decode(json_encode($requestData));
		    $durl = $this->get_isp_url($data_request);
			//echo "<pre>"; print_R($url); die;
		    $bitly_url = '';
		    /*$url = $url['url']."/login.php?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;
		    if($row_cp['bitly_url'] != ''){
			 $bitly_url = $row_cp['bitly_url'];
		    }else{
			 $bitly_url = $this->get_bitly_short_url($url);
			 if($bitly_url == ''){
			      $bitly_url = $url;
			 }else{
			      $this->DB2->query("update wifi_location set bitly_url = '$bitly_url' where id = '$location_id'");
			 }
		    }*/
			$guestbitlyurl='';
                    if($row_cp['location_type'] == '10' || $row_cp['location_type'] == '11' || $row_cp['location_type'] == '12' || $row_cp['location_type'] == '13' || $row_cp['location_type'] == '14')
                    {
                        $url = $durl['url']."?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;  
						
                    }
		    else if($row_cp['location_type'] == '21')
                    {
                        $url = 'https://www.shouut.com/middlewarewani/login.php';  
						
                    }
                    else
                    {
                      //$url = $url['url']."/login.php?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;
				$url = $durl['url']."?bcklocid=".$location_id."&bckisp_uid=".$isp_uid;  
				
			  
                    }
					if($row_cp['location_type'] == '18'){
					
						$guesturl=$durl['urlguest']."?bcklocid=".$location_id."&bckisp_uid=".$isp_uid; 
						$guestbitlyurl=$this->get_bitly_short_url($guesturl);
					}
                    $bitly_url = $this->get_bitly_short_url($url);
					
		    if($bitly_url == '')
		    {
			$bitly_url =  $url;
		    }
		    $this->DB2->set('bitly_url', $bitly_url);
		    $this->DB2->where('id', $location_id);
		    $this->DB2->update('wifi_location');
		    //$bitly_url = 'www.google.com';
		    $data['cp_url'] = $bitly_url;
			$data['eventguest_url'] = $guestbitlyurl;
		    $data['qr_path'] = OFFLINE_CODE."isp_hotspot/location_qr/index.php?url=".$bitly_url."&location_name=".$row_cp['location_name'];
			$data['qr_pathguest'] = OFFLINE_CODE."isp_hotspot/location_qr/index.php?url=".$guestbitlyurl."&location_name=".$row_cp['location_name'];
		    $data['code'] = 1;
	       }
	  }else{
	       $data['code'] = 0;
	  }
	  return json_encode($data);
     }
     public function get_location_ap_move_where($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $router_type = 6;
	  if(isset($jsondata->router_type)){
	     $router_type = $jsondata->router_type;  
	  }
        $data = array();
        $query = $this->DB2->query("select wl.id, wl.location_uid, wl.location_name from wifi_location as wl left join nas as n on(wl.nasid = n.id) where wl.isp_uid = '$isp_uid' and wl.nasconfigure = '1' and wl.status = '1' and wl.is_deleted = '0' and n.nastype = '$router_type' order by wl.location_name");
        
        $i = 0;
        foreach($query->result() as $row){
            $data[$i]['id'] = $row->id;
            $data[$i]['location_uid'] = $row->location_uid;
            $data[$i]['location_name'] = $row->location_name;
            $i++;
        }
        return $data;
    }
    
             public function onehop_move_ap($jsondata){
	  $router_type = '6';
	  if(isset($jsondata->router_type)){
	       $router_type = $jsondata->router_type;
	  }
	  $onehop_move_macid = $jsondata->onehop_move_macid;
	  $onehop_move_location_uid = $jsondata->onehop_move_location_uid;
	  if($router_type == 6){
	       $SourceNetwork = '';
	       $current_loc_uid = '';
	       $get_cur_locid = $this->DB2->query("select location_uid from wifi_location_access_point where macid = '$onehop_move_macid'");
	       if($get_cur_locid->num_rows() > 0){
		   $row = $get_cur_locid->row_array();
		    $current_loc_uid = $row['location_uid']; 
	       }
	       $get_des_net = $this->DB2->query("select network_name from onehop_networks where location_uid = '$current_loc_uid'");
	       if($get_des_net->num_rows() > 0){
		    $row = $get_des_net->row_array();
		    $SourceNetwork = $row['network_name'];
	       }
	       $DestNetwork = '';
	       $get_des_net = $this->DB2->query("select network_name, parent_network from onehop_networks where location_uid = '$onehop_move_location_uid'");
	       if($get_des_net->num_rows() > 0){
		    $row = $get_des_net->row_array();
		    $parent_network = $row['parent_network'];
		    if($parent_network > 0){
			 $get_des_net_new = $this->DB2->query("select network_name, parent_network from onehop_networks where id = '$parent_network'");
			 if($get_des_net_new->num_rows() > 0){
			      $row_new = $get_des_net_new->row_array();
			      $DestNetwork = $row_new['network_name'];
			 }
		    }else{
			 $DestNetwork = $row['network_name'];
		    }
		    
	       }
	       $data = array();
	       if($DestNetwork == $SourceNetwork){
		    $this->DB2->set('location_uid', $onehop_move_location_uid)->where('macid', $onehop_move_macid);
		    $update = $this->DB2->update('wifi_location_access_point');
		    
		    $this->DB2->set('location_uid', $onehop_move_location_uid)->where('macid', $onehop_move_macid);
		    $update = $this->DB2->update('onehop_macids');
		    $data['code'] = '1';
		    $data['message'] = "success";
	       }else{
		    $onehop_requestData = array(
			 'Organization' => 'SHOUUT',
			 'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
			 'Service' => 'APMove',
			 'SourceNetwork' => $SourceNetwork,
			 'DestNetwork' => $DestNetwork,
			 'APMAC' => $onehop_move_macid,
		    );
		    //echo "<pre>";print_r($onehop_requestData);
		    $response = $this->onehop_wifiApi($onehop_requestData);
		    $response = json_decode($response);
		    //echo "<pre>";print_r($response);die;
		    if($response->ResponceCode == '200'){
			 $this->DB2->set('location_uid', $onehop_move_location_uid)->where('macid', $onehop_move_macid);
			 $update = $this->DB2->update('wifi_location_access_point');
			
			  $this->DB2->set('location_uid', $onehop_move_location_uid)->where('macid', $onehop_move_macid);
			 $update = $this->DB2->update('onehop_macids');
			 $data['code'] = '1';
			 $data['message'] = $response->ReplyMessage;
		    }else{
			 $data['code'] = '0';
			 $data['message'] = $response->ReplyMessage;
		    }
	       }
	       
	       
	  }else{
	       $this->DB2->set('location_uid', $onehop_move_location_uid)->where('macid', $onehop_move_macid);
	       $update = $this->DB2->update('wifi_location_access_point');
	       // create zip
	       $requestData = array(
		    'location_uid' => $onehop_move_location_uid
	       );
	       $data_request = json_decode(json_encode($requestData));
	       $this->create_zip_for_offline_content($data_request);
	       $data['code'] = '1';
	       $data['message'] = "success";
	  }
	  
	  
	  return json_encode($data);
     }


     public function ping_onehop_ap_location_wise($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $network =  '';
	  $get_des_net = $this->DB2->query("select network_name,parent_network from onehop_networks where location_uid = '$location_uid'");
	  if($get_des_net->num_rows() > 0){
	       $row = $get_des_net->row_array();
	       $parent_network = $row['parent_network'];
	       if($parent_network > 0){
			 $get_des_net_source = $this->DB2->query("select network_name, parent_network from onehop_networks where id = '$parent_network'");
			 if($get_des_net_source->num_rows() > 0){
			      $row_from = $get_des_net_source->row_array();
			      $network = $row_from['network_name'];
			 }
		    }else{
			 $network = $row['network_name'];
		    }
	       
	  }
	  $data = array();
	  $code = 0;
	  $get_mac_id = $this->DB2->query("select macid from wifi_location_access_point where location_uid = '$location_uid'");
	  if($get_mac_id->num_rows() > 0){
	       foreach($get_mac_id->result() as $row){
		    $mac = $row->macid;
		    $status = $this->ping_onehop_ap($network, $mac);
		    if($status != 'Offline'){
			 $code = '1';
			 break;
		    }
	       }
	  }
	  if($code == '1'){
	       $this->DB2->set('is_location_online', '1')->where('location_uid', $location_uid);
	       $update = $this->DB2->update('wifi_location');
	  }else{
	       $this->DB2->set('is_location_online', '0')->where('location_uid', $location_uid);
	       $update = $this->DB2->update('wifi_location');
	  }
	  $data['code'] = $code;
	  return json_encode($data);
     }
     public function ping_onehop_ap($network, $mac){
	  $onehop_requestData = array(
		'Organization' => 'SHOUUT',
		'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
		'Service' => 'APStatus',
		'Network' => $network,
		'APMAC' => $mac,
	  );
	  $response = $this->onehop_wifiApi($onehop_requestData);
	  $response = json_decode($response);
	  $status = 'Offline';
	  if(isset($response->Status)){
	       $status = $response->Status;
	       // update model
	       if(isset($response->APModel)){
		    $model = $response->APModel;
		   
		    $this->DB2->set('device_model_name', $model)->where('macid', $mac);
		    $update = $this->DB2->update('wifi_location_access_point');
		    
	       }
	  }
	  return $status;
     }
     
     public function ping_onehop_ap_wise($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $mac = $jsondata->macid;
	  $network =  '';
	  $get_des_net = $this->DB2->query("select network_name,parent_network from onehop_networks where location_uid = '$location_uid'");
	  if($get_des_net->num_rows() > 0){
	       $row = $get_des_net->row_array();
	       $parent_network = $row['parent_network'];
	       if($parent_network > 0){
			 $get_des_net_source = $this->DB2->query("select network_name, parent_network from onehop_networks where id = '$parent_network'");
			 if($get_des_net_source->num_rows() > 0){
			      $row_from = $get_des_net_source->row_array();
			      $network = $row_from['network_name'];
			 }
		    }else{
			 $network = $row['network_name'];
		    }
	       
	  }
	  $data = array();
	  $code = 0;
	  $status = $this->ping_onehop_ap($network, $mac);
	  if($status != 'Offline'){
	       $code = '1';
	  }
	  if($code == '1'){
	       $this->DB2->set('is_location_online', '1')->where('location_uid', $location_uid);
	       $update = $this->DB2->update('wifi_location');
	  }else{
	       
	       $this->DB2->set('is_location_online', '0')->where('location_uid', $location_uid);
	       $update = $this->DB2->update('wifi_location');
	  }
	  $data['code'] = $code;
	  return json_encode($data);
     }
     
     public function remove_slider_image($jsondata){
	  $slider_type = $jsondata->slider_type;
	  $location_uid = $jsondata->location_uid;
	  if($slider_type == 'slider1'){
	       
	       $this->DB2->set('custom_slider_image1', '')->where('uid_location', $location_uid);
	       $update = $this->DB2->update('wifi_location_cptype');
	  }elseif($slider_type == 'slider2'){
	      
	      $this->DB2->set('custom_slider_image2', '')->where('uid_location', $location_uid);
	       $update = $this->DB2->update('wifi_location_cptype');
	  }
	  $data['code'] = '1';
	  return json_encode($data);
     }
     
     public function delete_wifi_location($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
       
	  $this->DB2->set('is_deleted', '1')->where('location_uid', $location_uid);
	  $update = $this->DB2->update('wifi_location');
	  
	  $this->DB2->select('id')->from('wifi_location');
	  $this->DB2->where('location_uid', $location_uid);
	  $get_id = $this->DB2->get();
	  $location_id = '0';
	  if($get_id->num_rows() > 0)
	  {
	       $row = $get_id->row_array();
	       $location_id = $row['id'];
	  }
	  // delete location from group
	  $this->DB2->set('is_deleted', '1')->where('location_id', $location_id);
	  $update = $this->DB2->update('cp_offers_location');
	  // delete location from offers/program
	  $this->DB2->set('is_deleted', '1')->where('loc_id', $location_id);
	  $update = $this->DB2->update('wifi_loc_group_mapping');
	  
	  $data = array();
	  $data['resultCode'] = '1';
	  return $data;
     }
     
      public function wifi_offer_category($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $data = array();
	 
	  $query = $this->DB2->query("select * from wifi_offer_category");
	  $i = 0;
	  foreach($query->result() as $row){
	      $data[$i]['id'] = $row->id;
	      $data[$i]['category_name'] = $row->category_name;
	      $i++;
	  }
	  return $data;
     }
    public function add_offline_category($jsondata){
	  $location_uid = trim($jsondata->location_uid);
	  $isp_uid = trim($jsondata->isp_uid);
	  $category_name = trim($jsondata->category_name);
	  $offline_icon = trim($jsondata->offline_icon);
	  $offline_category_type = $jsondata->offline_category_type;
	  $insert_data = array(
	       'location_uid' => $location_uid,
	       'category_id' => $offline_category_type,
	       'sub_category_name' => $category_name,
	       'icon' => $offline_icon,
	       'created_on' => date('Y-m-d H:i:s'),
	  );
	  $insert = $this->db->insert('wifi_offline_location_category', $insert_data);
          $created_id = $this->DB2->insert_id();
	  $data = array();
	  $icon_path = '';
	  if($offline_icon != ''){
	       $icon_path  = CLOUD_IMAGEPATH.'offline_icon/original/'.$offline_icon;
	  }
	  $data['icon_path'] = $icon_path;
	  $data['created_id'] = $created_id;
	
        return $data;
     }
     public function delete_offline_category($jsondata){
	  $id = trim($jsondata->id);
	 
          $insert = $this->DB2->query("delete from wifi_offline_location_category where id='$id'");
          $data = array();
	  $data['deleted_id'] = $id;
	
        return $data;
     }
     
     public function content_builder_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $offline_content_title = '';
	  $offline_content_desc = '';
	  $compliance_category = array();
	  $compliance_category_query = $this->DB2->query("select id, category_name from retailaudit_compliance_category");
	  if($compliance_category_query->num_rows() > 0)
	  {
	       $cmp = 0;
	       foreach($compliance_category_query->result() as $comp_row)
	       {
		    $compliance_category[$cmp]['id'] = $comp_row->id;
		    $compliance_category[$cmp]['category_name'] = $comp_row->category_name;
		    $cmp++;
	       }
	  }
	  // get the title and desc of content builder
	  $query_cp = $this->DB2->query("select offline_content_title,offline_content_desc from wifi_location_cptype where uid_location = '$location_uid'");
	  if($query_cp->num_rows() > 0){
	       $row_cp = $query_cp->row_array();
	       $offline_content_title = $row_cp['offline_content_title'];
	       $offline_content_desc = $row_cp['offline_content_desc'];
	  }
	  $main_category = array();
	  // get main category list
	  $main_cat = $this->DB2->query("select omc.*, rpc.category_id as is_promoter from offline_main_category as omc left join retailaudit_is_promoter_category as rpc on (omc.id = rpc.category_id and  rpc.is_deleted = '0' and rpc.is_promoter = '1') where omc.location_uid = '$location_uid' and omc.is_deleted = '0' order by omc.id");
	  $i = 0;
	  foreach($main_cat->result() as $row){
	       
	       $sub_category = array();
	       $is_promoter = 0;
	       if(isset($row->is_promoter))
	       {
		    $is_promoter = 1;
	       }
	       $main_category_id = $row->id;
	       $main_category[$i]['id'] = $row->id;
	       $compliance_cat_id = 0;
	       $this->DB2->select('compliance_id')->from('retailaudit_is_promoter_category');
	       $this->DB2->where(array('category_id'=>$main_category_id, 'is_promoter'=>'0', 'is_deleted' => '0'));
	       $get_comp_id = $this->DB2->get();
	       if($get_comp_id->num_rows() > 0)
	       {
		    $cmp_row = $get_comp_id->row_array();
		    $compliance_cat_id = $cmp_row['compliance_id'];
	       }
	       $main_category[$i]['compliance_cat_id'] = $compliance_cat_id;
	       $main_category[$i]['is_promoter'] = $is_promoter;
	       $main_category[$i]['category_name'] = $row->category_name;
	       $main_category[$i]['category_desc'] = $row->category_desc;
	       $main_category[$i]['icon'] = $row->icon;
	       $main_category[$i]['is_event_module_category'] = $row->is_event_module_category;
	       //get sub category
	       $sub_query = $this->DB2->query("select id,sub_category_name, sub_category_desc,icon from offline_sub_category where mid = '$main_category_id' and is_deleted = '0'");
	       $j = 0;
	       foreach($sub_query->result() as $row_sub){
		    $sub_category_id = $row_sub->id;
		    $sub_category[$j]['id'] = $row_sub->id;
		    $sub_category[$j]['sub_category_name'] = $row_sub->sub_category_name;
		    $sub_category[$j]['sub_category_desc'] = $row_sub->sub_category_desc;
		    $sub_category[$j]['icon'] = $row_sub->icon;
		    $sub_category[$j]['is_this_is_content'] = '0';
		    $k = 0;
		    $content_array = array();
		    // get content which is attatched with subcategory
		    $content_query = $this->DB2->query("select upload_type,is_synced,id,content_title, content_desc,content_file,content_type,wikipedia_title_url from offline_content where fid = '$sub_category_id' and is_main_category = '0' and is_deleted = '0'");
		    foreach($content_query->result() as $row_content){
			 $content_array[$k]['id'] = $row_content->id;
			 $content_array[$k]['content_title'] = $row_content->content_title;
			 $content_array[$k]['content_desc'] = $row_content->content_desc;
			 $content_array[$k]['content_file'] = $row_content->content_file;
			 $content_array[$k]['content_type'] = $row_content->content_type;
			 $content_array[$k]['is_synced'] = $row_content->is_synced;
			 $content_array[$k]['upload_type'] = $row_content->upload_type;
			 $content_array[$k]['wikipedia_title_url'] = $row_content->wikipedia_title_url;
			 $k++;
		    }
		    $sub_category[$j]['content'] = $content_array;
		    $j++;
	       }
	       // get content which is directly attatched with  main category
	       $sub_content_query = $this->DB2->query("select upload_type,is_synced,id,content_title, content_desc,content_file,content_type,wikipedia_title_url from offline_content where fid = '$main_category_id' and is_main_category = '1' and is_deleted = '0'");
	       foreach($sub_content_query->result() as $row_content_sub){
		    $sub_category[$j]['id'] = $row_content_sub->id;
		    $sub_category[$j]['content_title'] = $row_content_sub->content_title;
		    $sub_category[$j]['content_desc'] = $row_content_sub->content_desc;
		    $sub_category[$j]['content_file'] = $row_content_sub->content_file;
		    $sub_category[$j]['content_type'] = $row_content_sub->content_type;
		    $sub_category[$j]['is_this_is_content'] = '1';
		    $sub_category[$j]['is_synced'] = $row_content_sub->is_synced;
		    $sub_category[$j]['upload_type'] = $row_content_sub->upload_type;
		    $sub_category[$j]['wikipedia_title_url'] = $row_content_sub->wikipedia_title_url;
		    $j++;
	       }
	       $main_category[$i]['sub_category'] = $sub_category;
	       $i++;
	  }
		    
	  $data['offline_content_title'] = $offline_content_title;
	  $data['offline_content_desc'] =  $offline_content_desc;
	  $data['main_category'] =  $main_category;
	  $data['compliance_category'] =  $compliance_category;
	  //echo "<pre>";print_r($data);die;
	  // create zip
	  $requestData = array(
	       'location_uid' => $location_uid
	  );
	  $data_request = json_decode(json_encode($requestData));
	  $this->create_zip_for_offline_content($data_request);
	  
	  return json_encode($data);
     }
     
     public function add_content_builder($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $offline_content_title = $jsondata->offline_content_title;
	  $offline_content_desc = $jsondata->offline_content_desc;
	  
	  $update_data = array(
	       'offline_content_title' => $offline_content_title,
	       'offline_content_desc' => $offline_content_desc,
	  );
	  $this->DB2->where('location_id', $locationid);
	  $update = $this->DB2->update('wifi_location_cptype', $update_data);
	  
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  // create zip
	  $requestData = array(
	       'location_uid' => $location_uid
	  );
	  $data_request = json_decode(json_encode($requestData));
	  $this->create_zip_for_offline_content($data_request);
	  
	  $data['code'] = 1;
	
        return $data;
     }
     public function add_main_category($jsondata){
	  $is_lms = 0;
	  if(isset($jsondata->is_lms))
	  {
	       $is_lms = $jsondata->is_lms;
	  }
	  $compliance_mandatory = 0;
	  if(isset($jsondata->compliance_mandatory))
	  {
	       $compliance_mandatory = $jsondata->compliance_mandatory;
	  }
	  $icon = '';
	  if(isset($jsondata->icon)){
	       $icon = $jsondata->icon;
	  }
	  $file_previous_name = '';
	  if(isset($jsondata->file_previous_name)){
	       $file_previous_name = $jsondata->file_previous_name;
	  }
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $main_category_name = $jsondata->main_category_name;
	  $main_category_desc = $jsondata->main_category_desc;
	  $is_promoter = 0;
	  if(isset($jsondata->is_promoter))
	  {
	       $is_promoter = $jsondata->is_promoter;
	  }
	  if($jsondata->main_category_id == ''){
	       // check if first program for audit if yes the insert into logs
	       $this->DB2->select('id')->from('offline_main_category');
	       $this->DB2->where('location_id', $locationid);
	       $check_log = $this->DB2->get();	
	       if($check_log->num_rows() <= 0)
	       {
		    $this->retail_audit_log_record('AUDIT_CREATE',$locationid);
	       }
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'category_name' => $main_category_name,
		    'category_desc' => $main_category_desc,
		    'created_on' => date('Y-m-d H:i:s'),
		    'icon' => $icon,
		    'is_lms' => $is_lms,
		    'compliance_mandatory' => $compliance_mandatory,
	       );
	       $create = $this->DB2->insert('offline_main_category', $insert_data);
	       $created_category_id = $this->DB2->insert_id();
	  }else{
	       $file_update = array();
	       if($icon != ''){
		    $file_update['icon'] = $icon;
	       }else{
		    if($file_previous_name == ''){
			 $file_update['icon'] = $file_previous_name;
		    }
		    
	       }
	       $update_data = array(
		    'category_name' => $main_category_name,
		    'category_desc' => $main_category_desc,
		    'compliance_mandatory' => $compliance_mandatory,
		    'updated_on' => date('Y-m-d H:i:s'),
		    
	       );
	       $update_data = array_merge($update_data, $file_update);
	       $this->DB2->where('id', $jsondata->main_category_id);
	       $update = $this->DB2->update('offline_main_category', $update_data);
	       $created_category_id = $jsondata->main_category_id;
	  }
	  $is_promoter_category = 0;
	  if(isset($jsondata->is_promoter_category))
	  {
	       $is_promoter_category = $jsondata->is_promoter_category;
	  }
	  $this->DB2->select('id')->from('retailaudit_is_promoter_category');
	  $this->DB2->where("category_id", $created_category_id);
	  $this->DB2->where(array("is_deleted"=>'0', "is_promoter"=>"1"));
	  $get_promoter = $this->DB2->get();
	  if($get_promoter->num_rows() > 0)
	  {
	       if($is_promoter_category == '0')
	       {
		    $update_data = array(
			 'updated_on' => date('Y-m-d H:i:s'),
			 'is_deleted' => '1',
		    );
		    $this->DB2->where('category_id', $created_category_id);
		    $this->DB2->where(array("is_deleted"=>'0', "is_promoter"=>"1"));
		    $this->DB2->update('retailaudit_is_promoter_category', $update_data);
	       }
	  }
	  else
	  {
	       if($is_promoter_category == '1')
	       {
		    $insert_data = array(
			 'category_id' => $created_category_id,
			 'created_on' => date('Y-m-d H:i:s'),
			 'is_deleted' => '0',
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'is_promoter' => '1'
		    );
		    $this->DB2->insert('retailaudit_is_promoter_category', $insert_data);
	       }
	  }
	  $compliance_cat = '';
	  if(isset($jsondata->compliance_cat))
	  {
	       $compliance_cat = $jsondata->compliance_cat;
	       
	       $this->DB2->select('id')->from('retailaudit_is_promoter_category');
	       $this->DB2->where("category_id", $created_category_id);
	       $this->DB2->where(array("is_deleted"=>'0', "is_promoter"=>"0"));
	       $get_compliance = $this->DB2->get();
	       if($get_compliance->num_rows() > 0)
	       {
		    if($compliance_cat == '' || $compliance_cat== '0')
		    {
			 $update_data = array(
			      'updated_on' => date('Y-m-d H:i:s'),
			      'is_deleted' => '1',
			 );
			 $this->DB2->where('category_id', $created_category_id);
			 $this->DB2->where(array("is_deleted"=>'0', "is_promoter"=>"0"));
			 $this->DB2->update('retailaudit_is_promoter_category', $update_data);
		    }
		    else
		    {
			 $update_data = array(
			      'updated_on' => date('Y-m-d H:i:s'),
			      'compliance_id' => $compliance_cat,
			 );
			 $this->DB2->where('category_id', $created_category_id);
			 $this->DB2->where(array("is_deleted"=>'0', "is_promoter"=>"0"));
			 $this->DB2->update('retailaudit_is_promoter_category', $update_data);
		    }
	       }
	       else
	       {
		    if($compliance_cat != '')
		    {
			 $insert_data = array(
			      'category_id' => $created_category_id,
			      'created_on' => date('Y-m-d H:i:s'),
			      'is_deleted' => '0',
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'is_promoter' => '0',
			      'compliance_id' => $compliance_cat
			 );
			 $this->DB2->insert('retailaudit_is_promoter_category', $insert_data);
		    }
	       }
	  }
	  
	  $offline_content_title = $jsondata->offline_content_title;
	  $offline_content_desc = $jsondata->offline_content_desc;
	  
	  $update_data = array(
	       'offline_content_title' => $offline_content_title,
	       'offline_content_desc' => $offline_content_desc,
	       
	  );
	  $this->DB2->where('location_id', $locationid);
	  $update = $this->DB2->update('wifi_location_cptype', $update_data);
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     public function add_sub_category($jsondata){
	  $icon = '';
	  if(isset($jsondata->icon)){
	       $icon = $jsondata->icon;
	  }
	  $file_previous_name = '';
	  if(isset($jsondata->file_previous_name)){
	       $file_previous_name = $jsondata->file_previous_name;
	  }
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $sub_category_name = $jsondata->sub_category_name;
	  $sub_category_desc = $jsondata->sub_category_desc;
	  $category_id = $jsondata->category_id;
	  if($jsondata->sub_category_id == ''){
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'mid' => $category_id,
		    'sub_category_name' => $sub_category_name,
		    'sub_category_desc' => $sub_category_desc,
		    'created_on' => date('Y-m-d H:i:s'),
		    'icon' => $icon,
	       );
	       $create = $this->DB2->insert('offline_sub_category', $insert_data);
	       $created_category_id = $this->DB2->insert_id();
	  }else{
	       $file_update = array();
	       if($icon != ''){
		    $file_update['icon'] = $icon;
	       }else{
		    if($file_previous_name == ''){
			 $file_update['icon'] = $file_previous_name;
		    }
		    
	       }
	       $update_data = array(
		    'sub_category_name' => $sub_category_name,
		    'sub_category_desc' => $sub_category_desc,
		    'updated_on' => date('Y-m-d H:i:s'),
		    'icon' => $icon,
	       );
	       $update_data = array_merge($update_data, $file_update);
	       $this->DB2->where('id', $jsondata->sub_category_id);
	       $create = $this->DB2->update('offline_sub_category', $update_data);
	       
	       $created_category_id = $jsondata->sub_category_id;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     public function add_content($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $category_id = $jsondata->category_id;
	  $is_main_category = $jsondata->is_main_category;
	  $content_title = $jsondata->content_title;
	  $content_desc = $jsondata->content_desc;
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $file_previous_name = $jsondata->file_previous_name;
	  if($jsondata->content_id == ''){
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'fid' => $category_id,
		    'is_main_category' => $is_main_category,
		    'content_title' => $content_title,
		    'content_desc' => $content_desc,
		    'content_file' => $content_file,
		    'created_on' => date('Y-m-d H:i:s'),
		    'content_type' => $content_type,
	       );
	       $create = $this->DB2->insert('offline_content', $insert_data);
	       $created_category_id = $this->DB2->insert_id();
	  }else{
	       $file_update = array();
	       if($content_file != ''){
		    $file_update['content_file'] = $content_file;
		    $file_update['content_type'] = $content_type;
	       }else{
		    if($file_previous_name == ''){
			 $file_update['content_file'] = $file_previous_name;
			 $file_update['content_type'] = $content_type;
		    }
		    
	       }
	       $update_data = array(
		    'content_title' => $content_title,
		    'content_desc' => $content_desc,
		    'updated_on' => date('Y-m-d H:i:s'),
	       );
	       $update_data = array_merge($update_data, $file_update);
	       $this->DB2->where('id', $jsondata->content_id);
	       $create = $this->DB2->update('offline_content', $update_data);;
	      
	       $created_category_id = $jsondata->content_id;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     public function update_offline_version($locationid){
	  // update offline version
	  $get_current_version = $this->DB2->query("select offline_version,offline_version_synced from wifi_location_cptype where location_id = '$locationid'");
	  if($get_current_version->num_rows() > 0){
	       $get_current_version_row = $get_current_version->row_array();
	       $offline_version = $get_current_version_row['offline_version'];
	       $offline_version_synced = $get_current_version_row['offline_version_synced'];
	       if($offline_version_synced >= $offline_version){
		    $offline_version = $offline_version+1;
		    $this->DB2->set('offline_version', $offline_version)->where('location_id', $locationid);
		    $update_current_version = $this->DB2->update('wifi_location_cptype');
	       }
	  }
     }
     public function delete_content($jsondata){
	  $content_id = $jsondata->content_id;
        
	  $data = array();
	
	  $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s'),
	    );
	    $this->DB2->where('id', $content_id);
	   $query =  $this->DB2->update('offline_content', $update_data);
	  // for update offline version
	  $get = $this->DB2->query("select location_id from offline_content where id = $content_id");
	  if($get->num_rows() > 0){
	       $row = $get->row_array();
	       $locationid = $row['location_id'];
	       $this->update_offline_version($locationid);
	  }
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     public function delete_sub_category($jsondata){
	  $sub_category_id = $jsondata->sub_category_id;
	  
	  $data = array();
	  
	  $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s'),
	    );
	    $this->DB2->where('id', $sub_category_id);
	   $query =  $this->DB2->update('offline_sub_category', $update_data);
	  // deleted content which is related to sub category
	  
	  $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s'),
	    );
	  $this->DB2->where('fid', $sub_category_id);
	  $this->DB2->where('is_main_category', '0');
	  $query =  $this->DB2->update('offline_content', $update_data);
	  
	  $data['resultCode'] = '1';
          
	  // for update offline version
	  $get = $this->DB2->query("select location_id from offline_sub_category where id = $sub_category_id");
	  if($get->num_rows() > 0){
	       $row = $get->row_array();
	       $locationid = $row['location_id'];
	       $this->update_offline_version($locationid);
	  }
        return $data;
     }
     public function delete_main_category($jsondata){
	  $main_category_id = $jsondata->main_category_id;
	  
	  $data = array();
	  
	  $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s'),
	    );
	  $this->DB2->where('id', $main_category_id);
	  $query =  $this->DB2->update('offline_main_category', $update_data);
	
	  // deleted content which is related to directly main category
	 
	  
	  $update_data = array(
		    'is_deleted' => '1',
		    'updated_on' => date('Y-m-d H:i:s'),
	    );
	  $this->DB2->where('fid', $main_category_id);
	  $this->DB2->where('is_main_category', '1');
	  $query =  $this->DB2->update('offline_content', $update_data);
	  
	  // delete sub category related to main category also delete content related to sub category
	  $get = $this->DB2->query("select id from offline_sub_category where mid = '$main_category_id' and is_deleted = '0'");
	  if($get->num_rows() > 0){
	       foreach($get->result() as $row){
		    $sub_category_id = $row->id;
		    // delete sub category
		    
		    $update_data = array(
			 'is_deleted' => '1',
			 'updated_on' => date('Y-m-d H:i:s'),
		    );	
		    $this->DB2->where('id', $sub_category_id);
		    $dete_sub =  $this->DB2->update('offline_sub_category', $update_data);
		    // deleted content which is related to sub category
		    
		    $update_data = array(
			 'is_deleted' => '1',
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('fid', $sub_category_id);
		    $this->DB2->where('is_main_category', '0');
		    $query =  $this->DB2->update('offline_content', $update_data);
	       }
	  }
	  
	  // for update offline version
	  $get = $this->DB2->query("select location_id from offline_main_category where id = $main_category_id");
	  if($get->num_rows() > 0){
	       $row = $get->row_array();
	       $locationid = $row['location_id'];
	       $this->update_offline_version($locationid);
	  }
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     public function add_content_poll($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $category_id = $jsondata->category_id;
	  $is_main_category = $jsondata->is_main_category;
	  $content_title = $jsondata->content_title;
	  $content_desc = '';
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'fid' => $category_id,
		    'is_main_category' => $is_main_category,
		    'content_title' => $content_title,
		    'content_desc' => $content_desc,
		    'content_file' => $content_file,
		    'created_on' => date('Y-m-d H:i:s'),
		    'content_type' => $content_type,
	       );
	       $create = $this->DB2->insert('offline_content', $insert_data);
	       $created_category_id = $this->DB2->insert_id();
	       // add poll question
	       $poll_querstion = $jsondata->poll_querstion;
	       $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four,$jsondata->option_five,$jsondata->option_six );
	      
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'content_id' => $created_category_id,
		    'question' => $poll_querstion,
		    'survey_type' => '0',
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_question = $this->DB2->insert('offline_poll_survey_question', $insert_data);
	       $question_id = $this->DB2->insert_id();
	       foreach($options as $options1){
		    if($options1 != ''){
			 $options1 = $options1;
			 
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_poll_survey_options', $insert_data);
		    }
	       }
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     
     public function get_content_poll_survey($jsondata){
	  $data = array();
	  $content_id = $jsondata->content_id;
	  $question = array();
	  $query = $this->DB2->query("select * from offline_poll_survey_question where content_id = '$content_id'");
	  $i = 0;
	  foreach($query->result() as $row){
	       $question_id = $row->id;
	       $question[$i]['question_id'] = $question_id;
	       $question[$i]['question'] = $row->question;
	       $question[$i]['survey_type'] = $row->survey_type;
	       $options = array();
	       $get_option = $this->DB2->query("select * from offline_poll_survey_options where question_id = '$question_id'");
	       $j = 0;
	       foreach($get_option->result() as $row_op){
		    $options[$j]['option_id'] = $row_op->id;
		    $options[$j]['option_text'] = $row_op->option_text;
		    $j++;
	       }
	       $question[$i]['options'] = $options;
	       $i++;
	  }
	  $data['question'] = $question;
	 // echo "<pre>";print_r($data);die;
	  return json_encode($data);
     }
     
     
     public function update_content_poll($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $content_title = $jsondata->content_title;
	  $content_desc = '';
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $content_id = $jsondata->content_id;
	  $file_previous_name = $jsondata->file_previous_name;
	  $file_update = '';
	  if($content_file != ''){
		    $file_update = ",content_file = '$content_file'";
	  }else{
	       if($file_previous_name == ''){
		    $file_update = ",content_file = '$file_previous_name'";
	       }
	  }
	  $update_data = array(
	       'content_title' => $content_title,
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $content_id);
	  $update_content = $this->DB2->update('offline_content', $update_data);
	  // update question
	  $poll_querstion = $jsondata->poll_querstion;
	
	  $update_data = array(
	       'question' => $poll_querstion,
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('content_id', $content_id);
	  $update_question = $this->DB2->update('offline_poll_survey_question', $update_data);
	  
	  $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four,$jsondata->option_five,$jsondata->option_six );
	  $options = array_values(array_filter($options));
	 // get question id
	  $get = $this->DB2->query("select id from offline_poll_survey_question where content_id = '$content_id' ");
	  if($get->num_rows() > 0){
	       $row = $get->row_array();
	       $question_id = $row['id'];
	       // get previous options
	       $get_options = $this->DB2->query("select id from offline_poll_survey_options where question_id = '$question_id'");
	       if($get_options->num_rows() > 0){
		    foreach($get_options->result() as $row_option){
			 $option_id = $row_option->id;
			 if(count($options) > 0){
			      $new_option_val = $options['0'];
			      
			      $update_data = array(
				   'option_text' => $new_option_val,
				   'updated_on' => date('Y-m-d H:i:s'),
			      );
			      $this->DB2->where('id', $option_id);
			      $update_option = $this->DB2->update('offline_poll_survey_options', $update_data);
			      unset($options[0]);
			      $options = array_values($options);
			 }else{
			      // delete extra already created option
			      
			     $delete =  $this->DB2->delete('offline_poll_survey_options', array('id' => $option_id));
			 }
			 
			 
		    }
	       }
	       // add new added option
	       foreach($options as $options1){
		    if($options1 != ''){
			 $options1 = $options1;
			
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_poll_survey_options', $insert_data);
		    }
	       }
	  }else{
	       
	       $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four,$jsondata->option_five,$jsondata->option_six );
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'content_id' => $content_id,
		    'question' => $poll_querstion,
		    'survey_type' => '0',
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_question = $this->DB2->insert('offline_poll_survey_question', $insert_data);
	       $question_id = $this->DB2->insert_id();
	       foreach($options as $options1){
		    if($options1 != ''){
			 $options1 = $options1;
			
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_poll_survey_options', $insert_data);
		    }
	       }
	  }
	  
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $content_id;
	
        return $data;
     }
     
     public function add_content_survey($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $category_id = $jsondata->category_id;
	  $is_main_category = $jsondata->is_main_category;
	  $content_title = $jsondata->content_title;
	  $content_desc = '';
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $question = $jsondata->questions;
	  $insert_data = array(
	       'isp_uid' => $isp_uid,
	       'location_id' => $locationid,
	       'location_uid' => $location_uid,
	       'fid' => $category_id,
	       'is_main_category' => $is_main_category,
	       'content_title' => $content_title,
	       'content_desc' => $content_desc,
	       'content_file' => $content_file,
	       'created_on' => date('Y-m-d H:i:s'),
	       'content_type' => $content_type,
	  );
	  $create = $this->DB2->insert('offline_content', $insert_data);
	  
	  $created_category_id = $this->DB2->insert_id();
	  foreach($question as $question1){
	       $question_type = $question1->question_type;
	       $question = $question1->question;
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'content_id' => $created_category_id,
		    'question' => $question,
		    'survey_type' => $question_type,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_question = $this->DB2->insert('offline_poll_survey_question', $insert_data);
	       
	       $question_id = $this->DB2->insert_id();
	       $options = $question1->options;
	       foreach($options as $options1){
		    $options1 = $options1;
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'question_id' => $question_id,
			 'option_text' => $options1,
			 'created_on' => date('Y-m-d H:i:s'),
		    );
		    $insert_option = $this->DB2->insert('offline_poll_survey_options', $insert_data);
		    
	       }
	       
	  }
	       
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     
     public function update_content_survey($jsondata){
	  
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $content_title = $jsondata->content_title;
	  $content_desc = '';
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $content_id = $jsondata->content_id;
	  $file_previous_name = $jsondata->file_previous_name;
	  $question = $jsondata->questions;
	  $file_update = '';
	  if($content_file != ''){
		    $file_update = ",content_file = '$content_file'";
	  }else{
	       if($file_previous_name == ''){
		    $file_update = ",content_file = '$file_previous_name'";
	       }
	  }
	  
	  $update_data = array(
		  'content_title' => $content_title,
		  'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $content_id);
	  $update_content = $this->DB2->update('offline_content', $update_data);
	 
	  $created_category_id = $content_id;
	  
	  
	  
	  
	  $question = $jsondata->questions;
	  $previous_question_id = array();
	  $get = $this->DB2->query("select id from offline_poll_survey_question where content_id = '$content_id' ");
	  foreach($get->result() as $row_old_ques){
	      $previous_question_id[] = $row_old_ques->id;
	  }
	  foreach($question as $question1){
	       $question_type = $question1->question_type;
	       $question_val = $question1->question;
	       if(count($previous_question_id) > 0){// update question
		    $question_id_to_update = $previous_question_id[0];
		   
		    $update_data = array(
			 'question' => $question_val,
			 'survey_type' => $question_type,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $question_id_to_update);
		    $update_question = $this->DB2->update('offline_poll_survey_question', $update_data);
		    // get new options
		    $options = $question1->options;
		    $options_new = array();
		    foreach($options as $options1){
			$options_new[] = $options1;
		    }
		    $options_new = array_values(array_filter($options_new));
		    // get previous options
		    $get_options = $this->DB2->query("select id from offline_poll_survey_options where question_id = '$question_id_to_update'");
		    if($get_options->num_rows() > 0){
			 foreach($get_options->result() as $row_option){
			      $option_id = $row_option->id;
			      if(count($options_new) > 0){
				   $new_option_val = $options_new['0'];
				   
				   $update_data = array(
					'option_text' => $new_option_val,
					'updated_on' => date('Y-m-d H:i:s'),
				   );
				   $this->DB2->where('id', $option_id);
				   $update_option = $this->DB2->update('offline_poll_survey_options', $update_data);
				   unset($options_new[0]);
				   $options_new = array_values($options_new);
			      }else{
				   // delete extra already created option
				   
				   $delete = $this->DB2->delete('offline_poll_survey_options', array('id' => $option_id));
			      }
			      
			      
			 }
		    }
		    // add new added option
		    foreach($options_new as $options_new1){
			 if($options1 != ''){
			      $options1 = $options1;
			      $options1;
			      $insert_data = array(
				   'isp_uid' => $isp_uid,
				   'location_id' => $locationid,
				   'location_uid' => $location_uid,
				   'question_id' => $question_id_to_update,
				   'option_text' => $options_new1,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
			      $insert_option = $this->DB2->insert('offline_poll_survey_options', $insert_data);
			 }
		    }
		    
		    // remove question from array
		    unset($previous_question_id[0]);
		    $previous_question_id = array_values($previous_question_id);
	       }else{// create new question
		    
		    $insert_data = array(
				   'isp_uid' => $isp_uid,
				   'location_id' => $locationid,
				   'location_uid' => $location_uid,
				   'content_id' => $created_category_id,
				   'question' => $question_val,
				   'survey_type' => $question_type,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
		    $insert_question = $this->DB2->insert('offline_poll_survey_question', $insert_data);
		    
		    $question_id = $this->DB2->insert_id();
		    $options = $question1->options;
		    foreach($options as $options1){
			 $options1 = $options1;
			 
			 $insert_data = array(
				   'isp_uid' => $isp_uid,
				   'location_id' => $locationid,
				   'location_uid' => $location_uid,
				   'question_id' => $question_id,
				   'option_text' => $options1,
				   'created_on' => date('Y-m-d H:i:s'),
			      );
			 $insert_option = $this->DB2->insert('offline_poll_survey_options', $insert_data);
			 
		    }
	       }
	  }
	  // delete extra question
	  if(count($previous_question_id) > 0){
	       foreach($previous_question_id as $previous_question_id1){
		 
		    $detele = $this->DB2->delete('offline_poll_survey_question', array('id' => $previous_question_id1));
		    
		    $detele = $this->DB2->delete('offline_poll_survey_options', array('question_id' => $previous_question_id1));
	       }
	  }
	  
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $content_id;
	
        return $data;
     }
     
     public function add_content_upload($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $category_id = $jsondata->category_id;
	  $is_main_category = $jsondata->is_main_category;
	  $content_title = $jsondata->content_title;
	  $content_desc = $jsondata->content_desc;
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $upload_type = $jsondata->upload_type;
	  $file_previous_name = $jsondata->file_previous_name;
	  if($jsondata->content_id == ''){
	        
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'fid' => $category_id,
		    'is_main_category' => $is_main_category,
		    'content_title' => $content_title,
		    'content_desc' => $content_desc,
		    'content_file' => $content_file,
		    'created_on' => date('Y-m-d H:i:s'),
		    'content_type' => $content_type,
		    'upload_type' => $upload_type,
	       );
	       $create = $this->DB2->insert('offline_content', $insert_data);
	       $created_category_id = $this->DB2->insert_id();
	  }else{
	       $file_update = array();
	       if($content_file != ''){
		    $file_update['content_file'] = $content_file;
		    $file_update['content_type'] = $content_type;
	       }else{
		    if($file_previous_name == ''){
			 $file_update['content_file'] = $file_previous_name;
			 $file_update['content_type'] = $content_type;
		    }
		    
	       }
	      
	       $update_data = array(
		    'content_title' => $content_title,
		    'content_desc' => $content_desc,
		    'updated_on' => date('Y-m-d H:i:s'),
	       );
	       $update_data = array_merge($update_data, $file_update);
	       $this->DB2->where('id', $jsondata->content_id);
	       $this->DB2->update('offline_content', $update_data);
	       $created_category_id = $jsondata->content_id;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     public function get_onehop_existing_network($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $location_type = '';
	  if(isset($jsondata->location_type))
	  {
	       $location_type = $jsondata->location_type;
	  }
	  $location_type_where = "";
	  if($location_type != '')
	  {
	       $location_type_where = "AND location_type = '$location_type'";
	  }
	  $loc_uid = array();
	  $get_locid = $this->DB2->query("select location_uid from wifi_location where isp_uid = '$isp_uid' $location_type_where");
	  if($get_locid->num_rows() > 0)
	  {
	       foreach($get_locid->result() as $row_id)
	       {
		    $loc_uid[] = $row_id->location_uid;
	       }
	  }
	  //echo "<pre>";print_r($loc_uid);die;
	  $loc_uid = '"'.implode('", "', $loc_uid).'"';
	  $query = $this->DB2->query("select id,location_uid, network_name from onehop_networks where location_uid IN ($loc_uid)");
	  $i = 0;
	  $data = array();
	  foreach($query->result() as $row)
	  {
	       if($row->network_name != '')
	       {
		    $data[$i]['id'] = $row->id;
		    $data[$i]['location_uid'] = $row->location_uid;
		    $data[$i]['network_name'] = $row->network_name;
		    $i++;
	       }
	      
	  }
	
        return $data;
    }
    
    
    
    public function use_existing_onehop_network($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $previous_network_id = $jsondata->previous_network_id;
	  $location_uid = $jsondata->location_uid;
	  
	  $insert_data = array(
	       'location_uid' => $location_uid,
	       'parent_network' => $previous_network_id,
	       'created_on' => date('Y-m-d H:i:s'),
	  );
	  $insert = $this->DB2->insert('onehop_networks', $insert_data);
	       $data['msg'] = 'Done';
	       $data['code'] = '1';
	  // check ssid create with this group then update
	  $location_id_group = array();
	  $get = $this->DB2->query("select location_uid from onehop_networks where (id = '$previous_network_id' OR parent_network = '$previous_network_id')");
	  
	  if($get->num_rows() > 0){
	       foreach($get->result() as $row){
		    $location_id_group[] = $row->location_uid;
	       }
	  }
	  
	  $locations_uid_array = '"'.implode('", "', $location_id_group).'"';
	  // check ssid created for this group
	  $check = $this->DB2->query("select id, ssid_name, parent_ssid from onehop_ssid where location_uid IN($locations_uid_array)");
	  if($check->num_rows() > 0){
	       foreach($check->result() as $row_check){
		    if($row_check->parent_ssid == 0){
			 $parent_ssid_create = $row_check->id;
			 $insert_data = array(
			      'location_uid' => $location_uid,
			      'created_on' => date('Y-m-d H:i:s'),
			      'parent_ssid' => $parent_ssid_create,
			 );
			 $insert = $this->DB2->insert('onehop_ssid', $insert_data);      
		    }
	       }
	  }
	  return json_encode($data);
    }
    
    
         public function nas_ping_status(){
	  $isp_array = array();
	  $get = $this->db->query("select isp_uid, isp_name, email from sht_isp_admin");
	  foreach($get->result() as $get_row){
	       $isp_array[$get_row->isp_uid]['isp_name'] = $get_row->isp_name;
	       $isp_array[$get_row->isp_uid]['email'] = $get_row->email;
	       
	  }
	  // get all location
	  $query = $this->DB2->query("select wl.last_nas_down_msg,wl.id, wl.location_uid, wl.nasipaddress, wl.nasid, wl.location_name, wl.isp_uid, wl.contact_person_name, wl.user_email,n.nasname,n.nastype from wifi_location as wl inner join nas as n on(wl.nasid = n.id) where wl.is_deleted = '0' and enable_nas_down_noti = '1'");
	  if($query->num_rows() > 0){
	       foreach($query->result() as $row){
		    $location_uid = $row->location_uid;
		    $location_id = $row->id;
		    $nasipaddress = $row->nasipaddress;
		    $nasid = $row->nasid;
		    $location_name = $row->location_name;
		    $isp_uid = $row->isp_uid;
		    $contace_person_name = $row->contact_person_name;
		    $user_email = $row->user_email;
		    $nas_type = $row->nastype;
		    $nasip = $row->nasname;
		    if($nas_type == '1'){// mikrotik
			 $mikritik_ping_status = $this->ping_mikrotik_ip_address($nasip);
			 //echo $nasip.'-----'.$location_uid.'----'.$nas_type.'---'.$mikritik_ping_status."<br />";die;
			 if($mikritik_ping_status == 0){
			      $nasip = "Location IP is: <strong>".$nasip."</strong>";
			      // for location user
			      $this->location_server_status_mail($user_email, $location_name, $nasip, $contace_person_name, "down",$isp_uid);
			      // for isp user
			      $this->location_server_status_mail($isp_array[$isp_uid]['email'], $location_name, $nasip, $isp_array[$isp_uid]['isp_name'], "down",$isp_uid);
			    
			      $this->DB2->set('last_nas_down_msg', '1')->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location');
			 }
			 else{
			      if($row->last_nas_down_msg == '1')
			      {
				   // for location user
				   $this->location_server_status_mail($user_email, $location_name, $nasip, $contace_person_name, "Up now",$isp_uid);
				   // for isp user
				   $this->location_server_status_mail($isp_array[$isp_uid]['email'], $location_name, $nasip, $isp_array[$isp_uid]['isp_name'], "Up now",$isp_uid);
			      }
			      
			      $this->DB2->set('last_nas_down_msg', '0')->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location');
			 }
		    }
		    else if($nas_type == '6'){// onehop
			 $network = '';
			 $get_des_net = $this->DB2->query("select network_name from onehop_networks where location_uid = '$location_uid'");
			 if($get_des_net->num_rows() > 0){
			      $row = $get_des_net->row_array();
			      $network = $row['network_name'];
			 }
			 $code = 0;
			 $get_mac_id = $this->DB2->query("select macid from wifi_location_access_point where location_uid = '$location_uid'");
			 if($get_mac_id->num_rows() > 0){
			      foreach($get_mac_id->result() as $row){
				   $mac = $row->macid;
				   $status = $this->ping_onehop_ap($network, $mac);
				   if($status != 'Offline'){
					$code = '1';
					break;
				   }
			      }
			 }else{
			      $code = '1';// means no device add with this location
			 }
			 $ping_onehop_status =  $code;
			 //echo $nasip.'-----'.$location_uid.'----'.$nas_type.'---'.$ping_onehop_status."<br />";die;
			 if($ping_onehop_status == 0){
			      
			      $nasip = "";
			      // for location user
			      $this->location_server_status_mail($user_email, $location_name, $nasip, $contace_person_name, 'down',$isp_uid);
			      // for isp user
			      $this->location_server_status_mail($isp_array[$isp_uid]['email'], $location_name, $nasip, $isp_array[$isp_uid]['isp_name'], 'down',$isp_uid);
			      
			      $this->DB2->set('last_nas_down_msg', '1')->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location');
			 }
			 else{
			      if($row->last_nas_down_msg == '1')
			      {
				   // for location user
				   $this->location_server_status_mail($user_email, $location_name, $nasip, $contace_person_name, 'Up now',$isp_uid);
				   // for isp user
				   $this->location_server_status_mail($isp_array[$isp_uid]['email'], $location_name, $nasip, $isp_array[$isp_uid]['isp_name'], 'Up now',$isp_uid);
				  
				   $this->DB2->set('last_nas_down_msg', '1')->where('location_uid', $location_uid);
				   $this->DB2->update('wifi_location');
			      }
			      $this->DB2->set('last_nas_down_msg', '0')->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location');
			 }
		    }
		    
	       }
	  }
	  echo "done";
     }
     
     

    
     public function ping($host){
		exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
		return $rval;
     }
     public function ping_mikrotik_ip_address($ip_address){
	 $up = $this->ping($ip_address);
	     if($up == '0'){//0 means server is up
		 return 1;
	     }else{
		 return 0;
	     }
     }
     
     public function location_server_status_mail($email_id, $location_name, $location_ip, $contact_person_name,$updown_msg, $isp_uid){
	  //echo "here".$email_id;
	  $support_id = 'talktous@shouut.com';
	  $email_password = 'G1@nt@sh0uut#';
	  $team = 'Team Shouut';
	  $from_name = "SHOUUT";
	  $smtp_host = 'ssl://smtp.gmail.com';
	  if($isp_uid == '193')
	  {
	       $support_id = 'support@ads360network.com';
	       $team = 'Team Ads360';
	       $from_name = "Ads360";
	       $email_password = 'support@3456';
	       $smtp_host = 'ssl://smtp.zoho.com';
	  }
        $subject = "LOCATION SERVER STATUS";
        $message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>LOCATION SERVER STATUS</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px"><strong>Hi '.$contact_person_name.',</strong></h5>
			<p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;">Your location is '.$updown_msg.'.
</p>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Location Name is:
                          <strong>'.$location_name.' </strong></p>
<p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">
                          '.$location_ip.' </p>
                         <p style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"> For any
                         query  : Please write to us <strong>'.$support_id.'</strong> 
</p>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px;">'.$team.'</h5>
                        
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>

                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';
                        $this->load->library('email');
                        $from = $support_id;
        $fromname = $from_name;
        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => $smtp_host,
        'smtp_port' => '465',
        'smtp_user' => $support_id,
        'smtp_pass' => $email_password,
        'mailtype'  => 'html',
        'charset'   => 'iso-8859-1',
        'wordwrap'  => FALSE,
        'crlf'      => "\r\n",
        'newline'   => "\r\n"
        );
           $this->email->initialize($config);
               $this->email->from($from, $from_name);
               $this->email->to($email_id);
               $this->email->subject($subject);
               $this->email->message($message);    
               $this->email->send();
        return 1;

    }

    

     
     public function add_content_wiki($jsondata){
	  $is_created = '0';
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $category_id = $jsondata->category_id;
	  $is_main_category = $jsondata->is_main_category;
	  $content_title = $jsondata->content_title;
	  $wikipedia_title_url = $jsondata->wikipedia_title_url;
	  $wikipedia_title = $jsondata->wikipedia_title;
	  $wikipedia_pageid = $jsondata->wikipedia_pageid;
	  //$wikipedia_content = json_decode($jsondata->wikipedia_content);
	  $wikipedia_content = $jsondata->wikipedia_content;
	  $content_type = $jsondata->file_type;
	  //$wifi_content = $this->find_publicwifi_content($wiki_title);
	  
	  if($jsondata->content_id == ''){
	       
	       
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'fid' => $category_id,
		    'is_main_category' => $is_main_category,
		    'content_title' => $content_title,
		    'created_on' => date('Y-m-d H:i:s'),
		    'content_type' => $content_type,
		    'wikipedia_title_url' => $wikipedia_title_url,
		    'wikipedia_title' => $wikipedia_title,
		    'wikipedia_pageid' => $wikipedia_pageid,
	       );
	       $create = $this->DB2->insert('offline_content', $insert_data);
	       
	       $created_category_id = $this->DB2->insert_id();
	       
	       $wikipedia_content = $this->find_images_from_wiki_content($wikipedia_content,$created_category_id,$location_uid,$locationid);
	       $wikipedia_content =  $wikipedia_content;
	       
	       $this->DB2->set('wikipedia_content', $wikipedia_content);
	       $this->DB2->where('id', $created_category_id);
	       $update = $this->DB2->update('offline_content');
	       $is_created = '1';
	  }
	  else{
	       // check content is update is same is previous or it is new
	       $get = $this->DB2->query("select wikipedia_title_url,wikipedia_content from offline_content where  id = '$jsondata->content_id'");
	       $previous_wikipedia_title_url = '';
	       //$wikipedia_content = '';
	       if($get->num_rows() > 0){
		    $row = $get->row_array();
		    $previous_wikipedia_title_url = $row['wikipedia_title_url'];
		    //$wikipedia_content = $row['wikipedia_content'];
	       }
	       //$wikipedia_content11 = $this->find_images_from_wiki_content($wikipedia_content,$jsondata->content_id,$location_uid,$locationid);
	       
	       $wiki_where = array();
	       if($previous_wikipedia_title_url != $wikipedia_title_url){
		    $wikipedia_content = $this->find_images_from_wiki_content($wikipedia_content,$jsondata->content_id,$location_uid,$locationid);
		    $wikipedia_content =  $wikipedia_content;
		    
		    $wiki_where['wikipedia_title'] = $wikipedia_title;
		    $wiki_where['wikipedia_pageid'] = $wikipedia_pageid;
		    $wiki_where['wikipedia_content'] = $wikipedia_content;
	       }
	       $update_data = array(
		    'content_title' => $content_title,
		    'updated_on' => date('Y-m-d H:i:s'),
		    'wikipedia_title_url' => $wikipedia_title_url
	       );
	       $update_data = array_merge($update_data, $wiki_where);
	       $this->DB2->where('id', $jsondata->content_id);
	       $create = $this->DB2->update('offline_content', $update_data);
	       
	       
	       $is_created = '1';
	      
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
	  $data['is_created'] = $is_created;
	
        return $data;
     }
     public function getExtension($str){
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
     }
     public function find_images_from_wiki_content($wikipedia_content, $content_id, $location_uid,$locationid){
	  //$wiki_image_dest_base_url = 'C:/wamp/www/isp_new/isp_publicwifi_new/assets/offline_content/wiki_content';
	  $wiki_image_dest_base_url = OFFLINE_DIR.'offline_video_image_content/wiki_content';
	  if (!file_exists($wiki_image_dest_base_url)) {
               mkdir($wiki_image_dest_base_url, 0777, true);
          }
	  preg_match_all('/<img[^>]+>/i',$wikipedia_content, $result);
	  //echo "<pre>";print_r($result);
	  $img = array();
	  foreach($result[0] as $img_tag){
	       preg_match_all('/(alt|title|src)=("[^"]*")/i',$img_tag, $img[$img_tag]);
	  }
	  $i = 1;
	  // delete previous content image from table
	  $delete = $this->DB2->query("delete from offline_wiki_images where content_id = '$content_id'");
	  $newpatharr = array();
	  foreach($img as $data){
	       $data = $data[0];
	       $alt = $data[0];
	       $img_path = $data[1];
	       //echo "<br />";
	       $download_path = preg_replace('#^src="#', 'https:', $img_path);
	       $download_path = trim(trim($download_path,"'"),'"');
	       $file_ext = $file_ext = $this->getExtension($download_path);
	       $newname = $i.date("Ymdhisv").rand().".".$file_ext;
	       
	       $wiki_image_dest = $wiki_image_dest_base_url."/".$newname;
	      
	       // download image
	       file_put_contents($wiki_image_dest, file_get_contents($download_path));
	       
	       // upload in s3
	       $fname = $wiki_image_dest;
	       $famazonname = AMAZONPATHWIKI.'offline_video_image_content/wiki_content/'.$newname;
	       $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
	       $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
	       //unlink($fname);
	       // rename file
	       $resource_path = 'resources/'.$newname;
	       $old_path = preg_replace('#^src="#', '', $img_path);
	       $old_path = trim(trim($old_path,"'"),'"');
	       
	       //This is to point to the string characters to search for the matching pair
	       $offset = NULL;
	       //This variable is used to store the length of the search string 
	       $searchLength = strlen($old_path);
	       $search = $old_path; $replace = $resource_path;
	       while ($stringPostion = strpos($wikipedia_content, $search, $offset)){
                    $offset = $stringPostion + $searchLength;
                    $wikipedia_content = substr_replace($wikipedia_content, $replace, $stringPostion, $searchLength);
               }
	       $insert_data = array(
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'content_id' => $content_id,
		    'image_name' => $newname,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert = $this->DB2->insert('offline_wiki_images', $insert_data);
	       //break;
	       $i++;
	  }
	  return $wikipedia_content;
     }
     
     
     public function update_device_online_offline_status(){
	  // get onehop status
	  // get onehop newtorks
	  $macids_status = array();
	  $location_macids = array();
	  // get location macid
	  $get_macs = $this->DB2->query("select location_uid, macid from wifi_location_access_point where router_type = '6'");
	  $i = 0;
	  foreach($get_macs->result() as $macs){
	       $location_macids[$macs->location_uid][$i]["macid"] = strtolower($macs->macid);
	       $i++;
	  }
	  //echo "<pre>";print_r($location_macids);die;
	  $newtworks = array();
	  $get_newtworkd = $this->DB2->query("select network_name from onehop_networks where network_name != ''");
	  foreach($get_newtworkd->result() as $new_rows){
	       if($new_rows->network_name != ''){
		    $newtworks[] = $new_rows->network_name;
	       }
	       
	  }
	  
	  foreach($newtworks as $net_name){
	       $onehop_requestData = array(
		    'Organization' => 'SHOUUT',
		    'APIKey' => '78a47ec2-163d-46ab-96fa-394889f71c46',
		    'Service' => 'APList',
		    'Network' => $net_name,
	       );
	       $response = $this->onehop_wifiApi($onehop_requestData);
	       
	       $response = json_decode($response);
	       
	       if($response->ResponceCode == 200){
		    foreach($response->APList as $ap){
			 $macids_status[$ap->MAC]['macid'] = strtolower($ap->MAC);
			 $macids_status[$ap->MAC]['status'] = $ap->Status;
			 $macids_status[$ap->MAC]['LastSeen'] = $ap->LastSeen;
		    }
	       }
	       //echo "<pre>";print_r($response);die;
	  }
	  // get all location
	  $query = $this->DB2->query("select wl.id, wl.location_uid, wl.nasipaddress, wl.nasid, wl.location_name, wl.isp_uid, wl.contact_person_name, wl.user_email,n.nasname,n.nastype from wifi_location as wl inner join nas as n on(wl.nasid = n.id) where wl.is_deleted = '0'");
	  //echo "<pre>";print_r($query->result());die;
	  if($query->num_rows() > 0){
	       foreach($query->result() as $row){
		    $location_uid = $row->location_uid;
		    $location_id = $row->id;
		    $nasipaddress = $row->nasipaddress;
		    $nasid = $row->nasid;
		    $location_name = $row->location_name;
		    $isp_uid = $row->isp_uid;
		    $contace_person_name = $row->contact_person_name;
		    $user_email = $row->user_email;
		    $nas_type = $row->nastype;
		    $nasip = $row->nasname;
		    if($nas_type == '1'){// mikrotik
			 $mikritik_ping_status = $this->ping_mikrotik_ip_address($nasip);
			 if($mikritik_ping_status == '1'){
			      $last_sceen = date('M d H:i Y',strtotime(date('Y-m-d H:i:s')));
			     
			      $update_data = array(
				   'is_location_online' => '1',
				   'last_online_time' => $last_sceen,
			      );
			      $this->DB2->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location', $update_data);
			 }else{
			      
			      $update_data = array(
				   'is_location_online' => '0',
			      );
			      $this->DB2->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location', $update_data);
			 }
			 
		    }
		    else if($nas_type == '6'){// onehop
			 $status = 'offline';
			 $last_sceen = '';
			 if(array_key_exists($location_uid, $location_macids)){
			      foreach($location_macids[$location_uid] as $check){
				   foreach($check as $check1){
					if(array_key_exists($check1, $macids_status)){
					     if($macids_status[$check1]['status'] == 'Online'){
						  $status = 'online';
						  $last_sceen = $macids_status[$check1]['LastSeen'];
					     }else{
						  $last_sceen = $macids_status[$check1]['LastSeen'];
					     }
					}
				   }
			      }
			 }
			 if($status == 'online'){
			      $update_data = array(
				   'is_location_online' => '1',
				   'last_online_time' => $last_sceen,
			      );
			      $this->DB2->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location', $update_data);
			      
			 }else{
			      $update_data = array(
				   'is_location_online' => '0',
				   'last_online_time' => $last_sceen,
			      );
			      $this->DB2->where('location_uid', $location_uid);
			      $this->DB2->update('wifi_location', $update_data);
			 }
		    }
		    
	       }
	  }
	  echo "done";
     }
     
     public function state_list_for_filter($jsondata){
		$data = array();
		$state = array();
		$isp_uid = $jsondata->isp_uid;
		//get state 
		$get_state_query = $this->DB2->query("select state_id from wifi_location where isp_uid = '$isp_uid' AND is_deleted = '0'");
		$state_ids = array();
		foreach($get_state_query->result() as $row_state){
		    $state_ids[] = $row_state->state_id;
		}
		$state_ids = '"'.implode('", "', $state_ids).'"';
		$stateQ = $this->db->query("select * from sht_states where id IN ($state_ids) order by state");
		if($stateQ->num_rows() > 0){
		    $data['resultCode'] = '1';
		    $i = 0;
		    foreach($stateQ->result() as $row){
			$state[$i]['state_id'] = $row->id;
			$state[$i]['state_name'] = $row->state;
			$i++;
		    }
		    $data['state'] = $state;
		}else{
		    $data['resultCode'] = '0';
		}
		
		return $data;
	}
     // CI query changes
     public function city_list_for_filter($jsondata){
	  $state_id = $jsondata->state_id;
	  $isp_uid = $jsondata->isp_uid;
	  $data = array();
	  $this->DB2->select('city_id')->from('wifi_location');
	  $this->DB2->where(array('isp_uid' => $isp_uid, 'is_deleted' => '0'));
	  $get_state_query = $this->DB2->get();
	  $city_ids = array();
	  foreach($get_state_query->result() as $row_state){
	       $city_ids[] = $row_state->city_id;
	  }
	  //$city_ids = '"'.implode('", "', $city_ids).'"';
	  $this->db->select('city_id, city_name')->from('sht_cities');
	  $this->db->where('city_state_id', $state_id);
	  $this->db->where_in('city_id', $city_ids);
	  $query = $this->db->get();
	  $i = 0;
	  foreach($query->result() as $row){
	      $data[$i]['city_id'] = $row->city_id;
	      $data[$i]['city_name'] = $row->city_name;
	      $i++;
	  }
	  return $data;
     }
    
    public function wifi_total_session($jsondata){
	  $total_session = 0;
	  $state = '';
	  if(isset($jsondata->state_id)){
	       $state = $jsondata->state_id;
	  }
	  $city = '';
	  if(isset($jsondata->city_id)){
	       $city = $jsondata->city_id;
	  }
	  $state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
	  $isp_uid = $jsondata->isp_uid;
	  $start_date = $jsondata->start_date;
	  $end_date = $jsondata->end_date;
	  
	  $location_ids = array();
	  $query = $this->DB2->query("select id,location_uid from wifi_location where isp_uid = '$jsondata->isp_uid' and status = '1' and is_deleted = '0'  $state_where $city_where");
	  if($query->num_rows() > 0){
	       foreach($query->result() as $row){
		    $location_ids[] = $row->id;
	       }
	  }
	  $location_ids='"'.implode('", "', $location_ids).'"';
	  $get_session = $this->DB2->query("SELECT id FROM wifi_user_free_session WHERE location_id IN ($location_ids)  AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date'");
	  $total_session = $get_session->num_rows();
	  $dataarr = array();
	  $dataarr["total_session"] = $total_session;
	  
	  $total_cp_impression = 0;
	  $unique_cp_impression = 0;
	  $total_impression_query = $this->DB2->query("select sum(total_impression) as total_impression,sum(unique_impression) as unique_impression from
                channel_captiveportal_impression where location_id IN ($location_ids) AND
           (DATE(visit_date) BETWEEN '$start_date' AND '$end_date') ");
          if($total_impression_query->num_rows() > 0){
	       $total_cp_impression_row = $total_impression_query->row_array();
               $total_cp_impression = $total_cp_impression+$total_cp_impression_row['total_impression'];
	       $unique_cp_impression = $unique_cp_impression+$total_cp_impression_row['unique_impression'];
          }
	  $total_unique_user = 0;
	  $total_android_user = 0;
	  $total_iso_user = 0;
	  $total_windwos_phone_user = 0;
	  $total_laptop_user = 0;
	  $users_array = array();
	  $get_users = $this->DB2->query("select osinfo from wifi_user_free_session where location_id IN ($location_ids) AND  DATE(session_date) BETWEEN '$start_date' AND '$end_date' group by userid");
	  foreach($get_users->result() as $user_row)
	  {
	       
		    $total_unique_user++;
		    if($user_row->osinfo == 'Windows Phone')
		    {
			 $total_windwos_phone_user++;
		    }
		    else if($user_row->osinfo == 'Windows Desktop')
		    {
			 $total_laptop_user++;
		    }
		    else if($user_row->osinfo == 'Apple Phone')
		    {
			 $total_iso_user++;
		    }
		    else{
			$total_android_user++; 
		    }
		    
	       
	       
	  }
	  $dataarr["total_cp_impression"] = $total_cp_impression;
	  $dataarr["unique_cp_impression"] = $unique_cp_impression;
	  $dataarr["total_session"] = $total_session;
	  $dataarr["total_unique_user"] = $total_unique_user;
	  $dataarr["total_android_user"] = $total_android_user;
	  $dataarr["total_iso_user"] = $total_iso_user;
	  $dataarr["total_windwos_phone_user"] = $total_windwos_phone_user;
	  $dataarr["total_laptop_user"] = $total_laptop_user;
	  $total_online_user = $this->online_user($location_ids);
	  
	  $dataarr["total_online_user"] = $total_online_user;
	  //echo "<pre>";print_r($dataarr);die;
	  return $dataarr;
        
     }
     
     public function online_user($location_ids){
	  $online_user = 0;
	  $today_date = date("Y-m-d");
	  $loc_uids = $location_ids;
	  $get_nas = $this->DB2->query("select nasipaddress, location_uid from wifi_location where id IN($loc_uids)");
	  $onehop_macid = array();
	  $mikrotik_nasip = array();
	  $location_uid_array = array();
	  if($get_nas->num_rows() > 0)
	  {
	       foreach($get_nas->result() as $row)
	       {
		    $nasip = $row->nasipaddress;
		    $location_uid = $row->location_uid;
		    if($nasip == "104.155.209.8")
		    {
			 $location_uid_array[] = $location_uid;
		    }
		    else
		    {
			 $mikrotik_nasip[] = $nasip;
		    }
	       }
	  }
	  if(count($location_uid_array) > 0)
	  {
	       $location_uid_array='"'.implode('", "', $location_uid_array).'"';
	       $get_macid = $this->DB2->query("select macid from wifi_location_access_point where location_uid IN ($location_uid_array)");
	       foreach($get_macid->result() as $row_mac){
		    $onehop_macid[] = $row_mac->macid;
	       }
	  }
	  if(count($onehop_macid) > 0)
	  {
	       $onehop_macid='"'.implode('", "', $onehop_macid).'"';
	       $count_user = $this->DB2->query("select count(*) as count from radacct WHERE calledstationid IN($onehop_macid) and date(acctstarttime) = '$today_date' and acctstoptime is null");
	       $countarr=$count_user->row_array(); 
	       $online_user= $online_user+$countarr['count'];
	  }
	  if(count($mikrotik_nasip) > 0)
	  {
	       $mikrotik_nasip='"'.implode('", "', $mikrotik_nasip).'"';
	       $count_user = $this->DB2->query("select count(*) as count from radacct WHERE nasipaddress IN($mikrotik_nasip) and date(acctstarttime) = '$today_date' and acctstoptime is null");
	       $countarr=$count_user->row_array();
	       $online_user= $online_user+$countarr['count'];
	  }
	  return $online_user;
     }
     
     
     public function get_location_group($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $location_type = $jsondata->location_type;
	  $data = array();
	  $get_group = $this->DB2->query("select id,group_name, username, password from wifi_loc_group where isp_uid = '$isp_uid' and group_type = '$location_type' AND is_deleted = '0'");
	  if($get_group->num_rows() > 0){
	       $i = 0;
	       foreach($get_group->result() as $row){
		    $group_id = $row->id;
		    $data[$i]['group_id'] = $group_id;
		    $data[$i]['group_name'] = $row->group_name;
		    $data[$i]['username'] = $row->username;
		    $data[$i]['password'] = $row->password;
		    $get_location = $this->DB2->query("select id from wifi_loc_group_mapping where group_id = '$group_id'  AND is_deleted = '0'");
		    $data[$i]['locations'] = $get_location->num_rows();
		    $i++;
	       }
	  }
	  
	  return $data;
        
     }
     
     
     
     public function create_location_group($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $group_name = $jsondata->group_name;
	  $username = $jsondata->username;
	  $password = $jsondata->password;
	  $location_type = $jsondata->location_type;
	  $check_group = $this->DB2->query("select id from wifi_loc_group where group_name = '$group_name' AND is_deleted = '0'");
	  if($check_group->num_rows() > 0){
	       $data['resultCode'] = 0;
	       $data['resultMsg'] = "Group Name Already Exist";
	  }else{
	       $check_username = $this->DB2->query("select id from wifi_loc_group where username = '$username' AND is_deleted = '0'");
	       if($check_username->num_rows() > 0){
		    $data['resultCode'] = 0;
		    $data['resultMsg'] = "User Name Already Exist";
	       }else{
		    $data['resultCode'] = 1;
		    $data['resultMsg'] = "Success";
		    
		   
		    $insert_data = array(
			 'group_name' => $group_name,
			 'username' => $username,
			 'password' => $password,
			 'created_on' => date('Y-m-d H:i:s'),
			 'isp_uid' => $isp_uid,
			 'group_type' => $location_type,
		    );
		    $create_group = $this->DB2->insert('wifi_loc_group', $insert_data);
		    
		    $group_id = $this->DB2->insert_id();
		    if(count($jsondata->locations) > 0){
			 foreach($jsondata->locations as $locuid){
			      // check already exist
			      $check = $this->DB2->query("select id from wifi_loc_group_mapping where location_uid = '$locuid' and is_deleted = '0' AND group_id = '$group_id'");
			      if($check->num_rows() <= 0){
				   $loc_id = 0;
				   $get_loc_id = $this->DB2->query("select id from wifi_location where location_uid = '$locuid'");
				   if($get_loc_id->num_rows() > 0){
					$get_row = $get_loc_id->row_array();
					$loc_id = $get_row['id'];
				   }
				   $insert_data = array(
					'group_id' => $group_id,
					'loc_id' => $loc_id,
					'location_uid' => $locuid,
				   );
				   $map_loc = $this->DB2->insert('wifi_loc_group_mapping', $insert_data);
			      }
			      
			 }
		    }
	       }
	  }
	  
	  return json_encode($data);
     }

     public function add_location_to_group($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $group_id = $jsondata->group_id;
	
		    $data['resultCode'] = 1;
		    $data['resultMsg'] = "Success";
		   
		    if(count($jsondata->locations) > 0){
			 foreach($jsondata->locations as $locuid){
			      // check already exist
			      $check = $this->DB2->query("select id from wifi_loc_group_mapping where location_uid = '$locuid' and is_deleted = '0' AND group_id = '$group_id'");
			      if($check->num_rows() <= 0){
				   $loc_id = 0;
				   $get_loc_id = $this->DB2->query("select id from wifi_location where location_uid = '$locuid'");
				   if($get_loc_id->num_rows() > 0){
					$get_row = $get_loc_id->row_array();
					$loc_id = $get_row['id'];
				   }
				   $insert_data = array(
					'group_id' => $group_id,
					'loc_id' => $loc_id,
					'location_uid' => $locuid,
				   );
				   $map_loc = $this->DB2->insert('wifi_loc_group_mapping', $insert_data);
			      }
			      
			 }
		    }
	      
	  
	  return json_encode($data);
     }

     public function delete_loc_group($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $group_id = $jsondata->group_id;
	
	  
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $group_id);
	  $delete_group = $this->DB2->update('wifi_loc_group', $update_data);
	  
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('group_id', $group_id);
	  $delete_mapped_loc = $this->DB2->update('wifi_loc_group_mapping', $update_data);
	  $data['resultCode'] = 1;
		    $data['resultMsg'] = "Success";
	  return json_encode($data);
     }
     
     
     public function update_location_group($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $group_name = $jsondata->group_name;
	  $username = $jsondata->username;
	  $password = $jsondata->password;
	  $group_id = $jsondata->group_id;
	  $check_group = $this->DB2->query("select id from wifi_loc_group where group_name = '$group_name' AND is_deleted = '0' AND id != '$group_id'");
	  if($check_group->num_rows() > 0){
	       $data['resultCode'] = 0;
	       $data['resultMsg'] = "Group Name Already Exist";
	  }else{
	       $check_username = $this->DB2->query("select id from wifi_loc_group where username = '$username' AND is_deleted = '0' AND id != '$group_id'");
	       if($check_username->num_rows() > 0){
		    $data['resultCode'] = 0;
		    $data['resultMsg'] = "User Name Already Exist";
	       }else{
		    $data['resultCode'] = 1;
		    $data['resultMsg'] = "Success";
		    
		    $update_data = array(
			 'group_name' => $group_name,
			 'username' => $username,
			 'password' => $password,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $group_id);
		    $update = $this->DB2->update('wifi_loc_group', $update_data);
		    if(count($jsondata->locations) > 0){
			 foreach($jsondata->locations as $locuid){
			      // check already exist
			      
			      $update_data = array(
				   'is_deleted' => '1',
			      );
			      $this->DB2->where('id', $locuid);
			      $delete = $this->DB2->update('wifi_loc_group_mapping', $update_data);
			      //echo $this->DB2->last_query();
			 }
		    }
	       }
	  }
	  
	  return json_encode($data);
     }
     
     
     public function get_group_location_list($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $group_id = $jsondata->group_id;
	  $data = array();
	  $get_group = $this->DB2->query("select wlgm.id, wl.location_name, wl.location_uid from wifi_loc_group_mapping as wlgm inner join wifi_location as wl on (wlgm.location_uid = wl.location_uid) where wlgm.group_id = '$group_id' and wlgm.is_deleted = '0' order by wl.location_name asc");
	  if($get_group->num_rows() > 0){
	       $i = 0;
	       foreach($get_group->result() as $row){
		    $group_id = $row->id;
		    $data[$i]['id'] = $row->id;
		    $data[$i]['location_name'] = $row->location_name."(".$row->location_uid.")";
		     $i++;
	       }
	  }
	  
	  return $data;
        
     }
     
     
     public function add_otp_sms($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  $locartion_id = $jsondata->locartion_id;
	  $otp_sms = $jsondata->otp_sms;
	  $sms_type = $jsondata->sms_type;
	  // check otp enable
	  $check_otp = $this->DB2->query("select is_otpdisabled from wifi_location_cptype  where uid_location = '$location_uid'");
	  if($check_otp->num_rows() > 0){
	       $otp_row = $check_otp->row_array();
	       $is_otpdisabled = $otp_row['is_otpdisabled'];
	       if($is_otpdisabled == '1'){
		   $data['resultCode'] = '2';   
	       }
	       else{
		    $check = $this->db->query("select * from sht_sms_gateway where isp_uid = '$isp_uid'");
		    if($check->num_rows() > 0){
			 $data['resultCode'] = '1';
			
			 $insert_data = array(
			      'location_id' => $locartion_id,
			      'location_uid' => $location_uid,
			      'isp_uid' => $isp_uid,
			      'total_sms' => $otp_sms,
			      'used_sms' => '0',
			      'sms_type' => $sms_type,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $query = $this->DB2->insert('wifi_location_sms_assign', $insert_data);
		   
		    }
		    else{
			 $data['resultCode'] = '0';
		    }
	       }
	  }
	  else{
	     $data['resultCode'] = '2';  
	  }
	  
	  return $data;
    }
    
    
     public function sms_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $location_id = $jsondata->location_id;
	  $data = array();
	  $vouchers = array();
	  $query = $this->DB2->query("select * from wifi_location_sms_assign where location_uid = '$location_uid'");
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       $row_first = $query->row_array();
	       $starting_date = date('Y-m-d',strtotime($row_first['created_on']));
	       // get total otp send
	       $this->DB2->where('location_id', $location_id);
	       $this->DB2->where('date(added_on) >=' , $starting_date);
	       $this->DB2->from('channel_wifi_user_otp');
	       $total_otp_send = $this->DB2->count_all_results();
	       // get all promotional sms send
	       /*$this->DB2->where('loc_uid', $location_uid);
	       $this->DB2->where('date(added_on) >=' , $starting_date);
	       $this->DB2->from('mailchimp_push_group_offers_send');
	       $total_promotional_sms_send = $this->DB2->count_all_results();*/
	       $get_promo = $this->DB2->query("select mpgos.id from mailchimp_push_group_offers_send as mpgos inner join mailchimp_push_group as mpg on (mpgos.offer_id = mpg.id) where mpg.send_via = 'sms' AND mpgos.loc_uid = '$location_uid' AND date(mpgos.added_on) >= '$starting_date'");
	       $total_promotional_sms_send = $get_promo->num_rows();
	       foreach($query->result() as $row){
		   $vouchers[$i]['id'] = $row->id;
		   $sms_type = '';
		   $used = 0;
		   $balance = 0;
		    if($row->sms_type == '1'){
			 $sms_type = 'OTP';
			 if($total_otp_send > 0){
			      if($total_otp_send > $row->total_sms){
				   $used = $row->total_sms;
				   $balance = 0;
				   $total_otp_send = $total_otp_send - $row->total_sms;
			      }
			      else{
				   $used = $total_otp_send;
				   $balance = $row->total_sms - $total_otp_send;
				   $total_otp_send = 0;
			      }
			 }
			 else{
			      $used = 0;
			      $balance = $row->total_sms;
			 }
		    }else{
			 $sms_type = 'MARKETING';
			 if($total_promotional_sms_send > 0){
			      if($total_promotional_sms_send > $row->total_sms){
				   $used = $row->total_sms;
				   $balance = 0;
				   $total_promotional_sms_send = $total_promotional_sms_send - $row->total_sms;
			      }
			      else{
				   $used = $total_promotional_sms_send;
				   $balance = $row->total_sms - $total_promotional_sms_send;
				   $total_promotional_sms_send = 0;
			      }
			 }
			 else{
			      $used = 0;
			      $balance = $row->total_sms;
			 }
		    }
		   $vouchers[$i]['sms_type'] = $sms_type;
		   $vouchers[$i]['total_sms'] = $row->total_sms;
		   
		   $vouchers[$i]['used_sms'] = $used;
		   $vouchers[$i]['sms_balance'] = $balance;
		   $vouchers[$i]['created_on'] = date('d-m-Y',strtotime($row->created_on));
		   $i++;
	       }
	       $data['vouchers']= array_reverse($vouchers);
	  }else{
	      $data['resultCode'] = '0';
	  }
	  //echo "<pre>";print_r($data);die;
	  return $data;
     }
     
     public function offline_organisation_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $query = $this->DB2->query("select id, organisation_name from offline_vm_organisation where location_uid = '$location_uid' AND is_deleted = '0' order by organisation_name asc");
	  $i = 0;
	  if($query->num_rows() > 0){
	       $organisations = array();
	       foreach($query->result() as $row){
		    $organisations[$i]['organisation_id'] = $row->id;
		    $organisations[$i]['organisation_name'] = $row->organisation_name;
		    $i++;
	       }
	       $data['resultCode'] = 1;
	       $data['organisations'] = $organisations;
	  }
	  else{
	      $data['resultCode'] = 0; 
	  }
	  $requestData = array(
	       'location_uid' => $location_uid
	  );
	  $data_request = json_decode(json_encode($requestData));
	  $this->create_zip_for_offline_content($data_request);
	  return json_encode($data);
     }
     
     public function add_offline_vm_organisation($jsondata){
	  $org_id = '';
	  if(isset($jsondata->org_id))
	  {
	       $org_id = $jsondata->org_id;
	  }
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $offline_organisation_name= $jsondata->offline_organisation_name;
	  $resultCode = 0;
	  if($org_id == '')
	  {
	       //check
	       $check = $this->DB2->query("select organisation_name from offline_vm_organisation where location_uid = '$location_uid' AND organisation_name = '$offline_organisation_name' AND is_deleted = '0'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    
		    $insert_data = array(
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'isp_uid' => $isp_uid,
			 'organisation_name' => $offline_organisation_name,
			 'created_on' => date('Y-m-d H:i:s'),
		    );
		    $insert = $this->DB2->insert('offline_vm_organisation', $insert_data);
		    // update offline version
		    $this->update_offline_version($locationid);
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  else
	  {
	       //check
	       $check = $this->DB2->query("select organisation_name from offline_vm_organisation where location_uid = '$location_uid' AND organisation_name = '$offline_organisation_name' AND id != '$org_id' AND is_deleted = '0'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    
		    $update_data = array(
			 'organisation_name' => $offline_organisation_name,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $org_id);
		    $this->DB2->update('offline_vm_organisation', $update_data);
		    // update offline version
		    $this->update_offline_version($locationid);
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  
	  $data['resultCode'] = $resultCode;
	
        return $data;
     }
     
      public function vm_offline_department_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $cond="";
	  if(isset($jsondata->orgid) && $jsondata->orgid!='null'){
		  $cond="and ovd.org_id='".$jsondata->orgid."'";
	  }
	  $data = array();
	  $query = $this->DB2->query("select ovd.id as department_id, ovo.id as organisation_id, ovd.department_name, ovo.organisation_name from offline_vm_department as ovd inner join offline_vm_organisation as ovo on (ovd.org_id = ovo.id) where ovd.location_uid = '$location_uid' {$cond} AND ovd.is_deleted = '0' AND ovo.is_deleted = '0' order by ovd.department_name asc");
	  $i = 0;
	  if($query->num_rows() > 0){
	       $department = array();
	       foreach($query->result() as $row){
		    $department[$i]['organisation_id'] = $row->organisation_id;
		    $department[$i]['department_id'] = $row->department_id;
		    $department[$i]['department_name'] = $row->department_name;
		    $department[$i]['organisation_name'] = $row->organisation_name;
		    $i++;
	       }
	       $data['resultCode'] = 1;
	       $data['department'] = $department;
	  }
	  else{
	      $data['resultCode'] = 0; 
	  }
	  
	  return json_encode($data);
     }


    public function add_offline_vm_department($jsondata){
	  $dept_id = '';
	  if(isset($jsondata->dept_id))
	  {
	       $dept_id = $jsondata->dept_id;
	  }
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $org_id = trim($jsondata->org_id);
	  $offline_department_name = $jsondata->offline_department_name;
	  $resultCode = 0;
	  
	  if($dept_id == '')
	  {
	       //check
	       $check = $this->DB2->query("select department_name from offline_vm_department where location_uid = '$location_uid' AND department_name = '$offline_department_name' AND org_id = '$org_id' AND is_deleted = '0'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    $insert_data = array(
			 'org_id' => $org_id,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'isp_uid' => $isp_uid,
			 'department_name' => $offline_department_name,
			 'created_on' => date('Y-m-d H:i:s'),
		    );
		    $insert = $this->DB2->insert('offline_vm_department', $insert_data);
		    // update offline version
		    $this->update_offline_version($locationid);
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  else
	  {
	       //check
	       $check = $this->DB2->query("select department_name from offline_vm_department where location_uid = '$location_uid' AND department_name = '$offline_department_name' AND org_id = '$org_id' AND id != '$dept_id' AND is_deleted = '0'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    $update_data = array(
			 'department_name' => $offline_department_name,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $dept_id);
		    $this->DB2->update('offline_vm_department', $update_data);
		    // update offline version
		    $this->update_offline_version($locationid);
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  
	  $data['resultCode'] = $resultCode;
	
        return $data;
     }
     
     public function vm_offline_employee_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	   $cond1="";
	  if(isset($jsondata->orgid) && $jsondata->orgid!='null'){
		  $cond1="and ove.org_id='".$jsondata->orgid."'";
	  }
	    $cond2="";
	  if(isset($jsondata->deptid) && $jsondata->deptid!='null'){
		  $cond2="and ove.dept_id='".$jsondata->deptid."'";
	  }
	  $data = array();
	  $query = $this->DB2->query("select ove.employee_last_name,ove.org_id, ove.dept_id,ovo.organisation_name,ove.employee_name, ove.id as employee_id, ove.employee_email, ove.employee_mobile, ovd.department_name from offline_vm_employee as ove inner join offline_vm_department as ovd on (ove.dept_id = ovd.id) inner join offline_vm_organisation as ovo on (ove.org_id = ovo.id) where ove.location_uid = '$location_uid' {$cond1} {$cond2} AND ove.is_deleted = '0' AND ovd.is_deleted = '0' AND ovo.is_deleted = '0'");
	  $i = 0;
	  if($query->num_rows() > 0){
	       $employee = array();
	       foreach($query->result() as $row){
		    $employee[$i]['employee_last_name'] = $row->employee_last_name;
		    $employee[$i]['employee_id'] = $row->employee_id;
		    $employee[$i]['employee_name'] = $row->employee_name;
		    $employee[$i]['employee_email'] = $row->employee_email;
		    $employee[$i]['employee_mobile'] = $row->employee_mobile;
		    $employee[$i]['department_name'] = $row->department_name;
		    $employee[$i]['organisation_name'] = $row->organisation_name;
		    $employee[$i]['organisation_id'] = $row->org_id;
		    $employee[$i]['department_id'] = $row->dept_id;
		    $i++;
	       }
	       $data['resultCode'] = 1;
	       $data['employee'] = $employee;
	  }
	  else{
	      $data['resultCode'] = 0; 
	  }
	  
	  return json_encode($data);
     }
     
    
     
     public function add_vm_offline_employee($jsondata){
	  $employee_unique_id = '';
	  $employee_id = '';
	  if(isset($jsondata->employee_id)){
	       $employee_id = $jsondata->employee_id;
	  }
	  $employee_last_name = '';
	  if(isset($jsondata->employee_last_name))
	  {
	       $employee_last_name = $jsondata->employee_last_name;
	  }
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $org_id = trim($jsondata->org_id);
	  $dept_id = trim($jsondata->dept_id);
	  $employee_name = $jsondata->employee_name;
	  $employee_email = $jsondata->employee_email;
	  $employee_mobile = trim($jsondata->employee_mobile);
	  $resultCode = 0;
	  if($employee_id != '')
	  {
	       //check
	       $check = $this->DB2->query("select id from offline_vm_employee where location_uid = '$location_uid' AND (employee_email = '$employee_email' OR employee_mobile = '$employee_mobile') AND is_deleted = '0' AND id != '$employee_id'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    
		    $update_data = array(
			 'org_id' => $org_id,
			 'dept_id' => $dept_id,
			 'employee_name' => $employee_name,
			 'employee_email' => $employee_email,
			 'employee_mobile' => $employee_mobile,
			 'updated_on' => date('Y-m-d H:i:s'),
			 'employee_last_name' => $employee_last_name,
		    );
		    $this->DB2->where('id', $employee_id);
		    $insert = $this->DB2->update('offline_vm_employee', $update_data);
		    
		    $this->update_offline_version($locationid);
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  else
	  {
	       //check
	       $check = $this->DB2->query("select id from offline_vm_employee where location_uid = '$location_uid' AND (employee_email = '$employee_email' OR employee_mobile = '$employee_mobile') AND is_deleted = '0'");
	       
	       if($check->num_rows() <= 0)
	       {
		    $is_insert = 0;
		    if(isset($jsondata->emp_unique_id) && $jsondata->emp_unique_id != '')
		    {
			 $employee_unique_id = $jsondata->emp_unique_id;
			  // check employee unique id present at the time of lms employee
			 $check_lms_unique = $this->DB2->query("select id from offline_vm_employee where location_uid = '$location_uid' AND employee_id = '$jsondata->emp_unique_id' AND is_deleted = '0'");
			 
			 if($check_lms_unique->num_rows() > 0)
			 {
			      $is_insert = 0;
			      $resultCode = '2';
 
			 }
			 else{
			      $is_insert = 1;
			      $resultCode = '1';
			 }
		    }
		    else
		    {
			$is_insert = 1;
			$resultCode = '1';
		    }
		    
		    if($is_insert == '1')
		    {
			 
			 $insert_data = array(
			      'org_id' => $org_id,
			      'dept_id' => $dept_id,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'isp_uid' => $isp_uid,
			      'employee_name' => $employee_name,
			      'employee_email' => $employee_email,
			      'employee_mobile' => $employee_mobile,
			      'created_on' => date('Y-m-d H:i:s'),
			      'employee_last_name' => $employee_last_name,
			      'employee_id' => $employee_unique_id,
			 );
			 $insert = $this->DB2->insert('offline_vm_employee', $insert_data);
			 
			 $this->update_offline_version($locationid);
		    }
		    
	       }
	       else
	       {
		    $resultCode = 0; 
		    
	       }
	  }
	  
	  $data['resultCode'] = $resultCode;
	
        return $data;
     }
     
     public function delete_offline_vm_organisation($jsondata){
	  $organisation_id = $jsondata->organisation_id;
	  
	  $data = array();
	 
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $organisation_id);
	  $query = $this->DB2->update('offline_vm_organisation', $update_data);
	  
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('org_id', $organisation_id);
	  $query = $this->DB2->update('offline_vm_department', $update_data);
	   
	
	  
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('org_id', $organisation_id);
	  $query = $this->DB2->update('offline_vm_employee', $update_data);
	  
	$locationid = 0;
	  if(isset($jsondata->locationid)){
	       $locationid = $jsondata->locationid;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     
     public function delete_offline_vm_department($jsondata){
	  $department_id = $jsondata->department_id;
	  
	  $data = array();
	  
	  
	  
	    $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $department_id);
	  $query = $this->DB2->update('offline_vm_department', $update_data);
	  
	
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('dept_id', $department_id);
	  $query = $this->DB2->update('offline_vm_employee', $update_data);
	$locationid = 0;
	  if(isset($jsondata->locationid)){
	       $locationid = $jsondata->locationid;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     
     public function delete_offline_vm_employee($jsondata){
	  $employee_id = $jsondata->employee_id;
	  
	  $data = array();
	  
	    
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $employee_id);
	  $query = $this->DB2->update('offline_vm_employee', $update_data);
	  $locationid = 0;
	  if(isset($jsondata->locationid)){
	       $locationid = $jsondata->locationid;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     
     
     
     public function create_zip_for_offline_content($jsondata)
     {
	  $this->load->library('zip');
	  $location_uid = trim($jsondata->location_uid);
        // get macid of this location
	  $get_mac = $this->DB2->query("select macid from wifi_location_access_point where location_uid = '$location_uid'");
	  if($get_mac->num_rows() > 0)
	  {
	       foreach($get_mac->result() as $row_mac)
	       {
		    $macid = strtolower($row_mac->macid);
                  // get info of macid
		    $get_mac_info = $this->DB2->query("select wlct.offline_cp_content_type,wlct.offline_cp_source_folder_path,wlct.cp_type,wlap.location_uid, wl.id as location_id from wifi_location_access_point as wlap inner join wifi_location as wl on (wlap.location_uid = wl.location_uid) inner join wifi_location_cptype as wlct on (wlap.location_uid = wlct.uid_location) where LOWER(wlap.macid) = '$macid' ");
                  // if macid register with us
		    if($get_mac_info->num_rows() > 0)
		    {
			 $row = $get_mac_info->row_array();
			 $location_id = $row['location_id'];
			 $location_uid = $row['location_uid'];
			 $cp_type = $row['cp_type'];
			 $offline_cp_content_type = $row['offline_cp_content_type'];
			 $offline_cp_source_folder_path = $row['offline_cp_source_folder_path'];
			 // check macid already synced with this location
			 $check_previous_synced = $this->DB2->query("select * from offline_macid_synced_detail where location_id = '$location_id' and LOWER(macid) = '$macid'");
                       
			 if($check_previous_synced->num_rows() > 0)
			 {
			      $check_previous_synced_row = $check_previous_synced->row_array();
			      $synced_on = $check_previous_synced_row['syncedon'];
                            // already synced previous. Synced if any update
			      $tables=array("offline_content","offline_main_category","offline_sub_category","offline_poll_survey_question","offline_poll_survey_options","offline_vm_department","offline_vm_employee","offline_vm_organisation", "offline_contestifi_contest","offline_contestifi_quiz_questions","offline_contestifi_quiz_option","offline_contestifi_contest_rules","offline_contestifi_contest_prize","offline_contestifi_extra_feature", "offline_event_module_turn_reature","offline_event_module_signup_fields");
			      $loc_id = $location_id;
			      $tablecond=array("offline_content"=>array("location_id"=>$loc_id),"offline_main_category"=>array("location_id"=>$loc_id),"offline_sub_category"=>array("location_id"=>$loc_id),"offline_poll_survey_question"=>array("location_id"=>$loc_id),"offline_poll_survey_options"=>array("location_id"=>$loc_id),"offline_vm_department"=>array("location_id"=>$loc_id),"offline_vm_employee"=>array("location_id"=>$loc_id),"offline_vm_organisation"=>array("location_id"=>$loc_id),"offline_contestifi_contest"=>array("location_id"=>$loc_id),"offline_contestifi_quiz_questions"=>array("location_id"=>$loc_id),"offline_contestifi_quiz_option"=>array("location_id"=>$loc_id),"offline_contestifi_contest_rules"=>array("location_id"=>$loc_id),"offline_contestifi_contest_prize"=>array("location_id"=>$loc_id),"offline_contestifi_extra_feature"=>array("location_id"=>$loc_id),"offline_event_module_turn_reature"=>array("location_id"=>$loc_id),"offline_event_module_signup_fields"=>array("location_id"=>$loc_id));
			      $host=$this->DB2->hostname;
			      $user=$this->DB2->username;
			      $pass=$this->DB2->password;
			      $name=$this->DB2->database;
                            
			      if (!file_exists(OFFLINE_DIR."offline_zip")) {
				  mkdir(OFFLINE_DIR."offline_zip", 0777, true);
			      }
			      $mac_folder = str_replace(":","-",$macid);
			      if (!file_exists(OFFLINE_DIR."offline_zip/".$mac_folder)) {
				  mkdir(OFFLINE_DIR."offline_zip/".$mac_folder, 0777, true);
			      }
			      $data['dbpathdynamic']=OFFLINE_DIR."offline_zip/".$mac_folder."/"."radius.sql";
			      $this->update_offline_db_create($host,$user,$pass,$name,$tables,$tablecond,$data['dbpathdynamic'],$synced_on);
                            // create zip
                           
			      if($offline_cp_content_type == '2')
			      {
				   $zip_source_code_path = $offline_cp_source_folder_path;
			      }
			      else
			      {
				   if($cp_type=="cp_offline")
				   {
					$zip_source_code_path=OFFLINE_DIR."offlinedynamic";
				   }
				   else if($cp_type=="cp_contestifi")
				   {
					$zip_source_code_path=OFFLINE_DIR."contestifi";
				   }
				   else if($cp_type=="cp_event_module")
				   {
					$zip_source_code_path=OFFLINE_DIR."event_mgmt";
				   }
				   else
				   {
					$zip_source_code_path=OFFLINE_DIR."visitoroffline";
				   }
				   
			      }
			      if (file_exists($zip_source_code_path))
			      {
				   
				   $src = $zip_source_code_path;
				   $dest = OFFLINE_DIR."offline_zip/".$mac_folder."/offlinedynamic";
				   //first delete old folder if exist
				   $this->load->helper("file");
				   delete_files($dest, true);
				   // copy files which are changed
				   $this->rcopy_synk($src, $dest,$synced_on);
				   $this->zip->read_dir($dest."/",FALSE);
				   $db_file_path = OFFLINE_DIR."offline_zip/".$mac_folder."/"."radius.sql";
				   $this->zip->read_file($db_file_path, "offlinedynamic/radius.sql");
				   //$this->zip->read_dir($path_of_code,FALSE);
				   $this->zip->archive(OFFLINE_DIR."offline_zip/".$mac_folder."/offlinedynamic".'.zip');
				   $this->zip->clear_data();
			      }
                            
                            
			 }
			 else
			 {
                            // synced all tabel and data(offlineapitest_model line number 266 for refrence)
			      $tables=array("offline_content","offline_content_impression","offline_home_impression","offline_main_category","offline_maincategory_impression","offline_ping","offline_sub_category","offline_subcategory_impression","offline_views_data","offline_poll_survey_question","offline_poll_survey_options","offline_image_video_upload","offline_survey_text_answer","offline_poll_survey_answer","offline_vm_department","offline_vm_employee","offline_vm_organisation", "offline_contestifi_contest","offline_contestifi_quiz_questions","offline_contestifi_quiz_option","offline_contestifi_contest_rules","offline_contestifi_contest_prize","offline_contestifi_extra_feature","offline_contestifi_contest_earn_point","offline_contestifi_contest_attempt","offline_contestifi_otp", "offline_contestifi_contest_survey_option_fill","offline_event_module_turn_reature","offline_event_discuss_comment","offline_event_module_chat","offline_event_module_chat_detail","offline_event_module_signup_fields","offline_event_module_user_reg","offline_event_module_user_registration");
                            
			      $loc_id = $location_id;
			      $tablecond=array("offline_build_version"=>array("loc_id"=>$loc_id),"offline_content"=>array("location_id"=>$loc_id),"offline_content_impression"=>array("loc_id"=>$loc_id),"offline_home_impression"=>array("loc_id"=>$loc_id),"offline_main_category"=>array("location_id"=>$loc_id),"offline_maincategory_impression"=>array("loc_id"=>$loc_id),"offline_ping"=>array("loc_id"=>$loc_id),"offline_sub_category"=>array("location_id"=>$loc_id),"offline_subcategory_impression"=>array("loc_id"=>$loc_id),"offline_views_data"=>array("location_id"=>$loc_id),"offline_poll_survey_answer"=>array("loc_id"=>$loc_id),"offline_survey_text_answer"=>array("loc_id"=>$loc_id),"offline_image_video_upload"=>array("loc_id"=>$loc_id),"offline_poll_survey_question"=>array("location_id"=>$loc_id),"offline_poll_survey_options"=>array("location_id"=>$loc_id),"offline_vm_department"=>array("location_id"=>$loc_id),"offline_vm_employee"=>array("location_id"=>$loc_id),"offline_vm_organisation"=>array("location_id"=>$loc_id),"offline_contestifi_contest"=>array("location_id"=>$loc_id),"offline_contestifi_quiz_questions"=>array("location_id"=>$loc_id),"offline_contestifi_quiz_option"=>array("location_id"=>$loc_id),"offline_contestifi_contest_rules"=>array("location_id"=>$loc_id),"offline_contestifi_contest_prize"=>array("location_id"=>$loc_id),"offline_contestifi_extra_feature"=>array("location_id"=>$loc_id),"offline_event_module_turn_reature"=>array("location_id"=>$loc_id),"offline_event_module_signup_fields"=>array("location_id"=>$loc_id));
                            
			      $notcontentarr=  array("offline_build_version","offline_ping","offline_content_impression","offline_views_data","offline_maincategory_impression","offline_home_impression","offline_subcategory_impression","offline_poll_survey_answer","offline_survey_text_answer","offline_image_video_upload","offline_contestifi_contest_earn_point","offline_contestifi_contest_attempt","offline_contestifi_otp", "offline_contestifi_contest_survey_option_fill","offline_event_discuss_comment","offline_event_module_chat","offline_event_module_chat_detail","offline_event_module_user_reg","offline_event_module_user_registration");
                            
			      $primarykeyremove=array("offline_content_impression","offline_maincategory_impression","offline_home_impression","offline_subcategory_impression","offline_poll_survey_answer","offline_survey_text_answer","offline_image_video_upload");
			      $host=$this->DB2->hostname;
			      $user=$this->DB2->username;
			      $pass=$this->DB2->password;
			      $name=$this->DB2->database;
			      if (!file_exists(OFFLINE_DIR."offline_zip")) {
				  mkdir(OFFLINE_DIR."offline_zip", 0777, true);
			      }
			      $mac_folder = str_replace(":","-",$macid);
			      if (!file_exists(OFFLINE_DIR."offline_zip/".$mac_folder)) {
				  mkdir(OFFLINE_DIR."offline_zip/".$mac_folder, 0777, true);
			      }
			      $data['dbpathdynamic']=OFFLINE_DIR."offline_zip/".$mac_folder."/"."radius.sql";
			      $this->backup_tables($host,$user,$pass,$name,$tables,$tablecond,$primarykeyremove,$notcontentarr,$data['dbpathdynamic']);
			      // create zip
			      if($offline_cp_content_type == '2')
			      {
								  
				   $zip_source_code_path = $offline_cp_source_folder_path;
								  
			      }
			      else
			      {
				   if($cp_type=="cp_offline")
				   {
					$zip_source_code_path=OFFLINE_DIR."offlinedynamic";
				   }
				   else if($cp_type=="cp_contestifi")
				   {
					$zip_source_code_path=OFFLINE_DIR."contestifi";
				   }
				   else if($cp_type=="cp_event_module")
				   {
					$zip_source_code_path=OFFLINE_DIR."event_mgmt";
				   }
				   else
				   {
					$zip_source_code_path=OFFLINE_DIR."visitoroffline";
				   }
			      }
							//$zip_source_code_path="/var/www/html/isp_hotspot/offline/bigbazzar";
							
			      if (file_exists($zip_source_code_path))
			      {
								  
				   /*$db_file_path = OFFLINE_DIR."offline_zip/".$mac_folder."/"."radius.sql";
				   $this->zip->read_file($db_file_path, "offlinedynamic/radius.sql");
				   $path_of_code = $zip_source_code_path."/";
				   $this->zip->read_dir($path_of_code,FALSE);
				   $this->zip->archive(OFFLINE_DIR."offline_zip/".$mac_folder."/offlinedynamic".'.zip');
				   $this->zip->clear_data();*/
				   $src = $zip_source_code_path;
				   $dest = OFFLINE_DIR."offline_zip/".$mac_folder."/offlinedynamic";
				   //first delete old folder if exist
				   $this->load->helper("file");
				   delete_files($dest, true);
				   // copy files
				   $this->rcopy_synk($src, $dest);
				   $this->zip->read_dir($dest."/",FALSE);
				   
				   $db_file_path = OFFLINE_DIR."offline_zip/".$mac_folder."/"."radius.sql";
				   $this->zip->read_file($db_file_path, "offlinedynamic/radius.sql");
				   
				   $this->zip->archive(OFFLINE_DIR."offline_zip/".$mac_folder."/offlinedynamic".'.zip');
				   $this->zip->clear_data();
			      }
                            //echo $zip_source_code_path;die;
                            
			 }
		    }
	       }
	  }
        
        
     }
     
     
     function update_offline_db_create($host,$user,$pass,$name,$tables='*',$tablecond,$pathdb,$synced_on)
     {
        //echo "here";die;
	  $link = mysqli_connect($host,$user,$pass,$name);
	  if($tables == '*')
	  {
	       $tables = array();
	       $result = mysqli_query($link,'SHOW TABLES');
	       while($row = mysqli_fetch_row($result))
	       {
		    $tables[] = $row[0];
	       }
	  }
	  else
	  {
	       $tables = is_array($tables) ? $tables : explode(',',$tables);
	  }
        //echo "<pre>";print_r($tables);die;
	  $return='';
	  $return.='CREATE DATABASE /*!32312 IF NOT EXISTS*/`radius` /*!40100 DEFAULT CHARACTER SET latin1 */;
        USE `radius`;';
	  $return .= "\n";

        //cycle through
	  foreach($tables as $table)
	  {
	       $l=0;
	       $cond='';
	       if(!empty($tablecond[$table]))
	       {
		    foreach($tablecond[$table] as $key=>$vald)
		    {
			 if($l==0)
			 {
			     $cond.="where $key='".$vald."'";
			     $cond .= " AND (created_on > '".$synced_on."' OR updated_on > '".$synced_on."')";
			     
			 }
			 else
			 {
			      $cond.="and $key='".$vald."'";
			 }
			 $l++;
		    }
	       }
	       $result = mysqli_query($link,'SELECT * FROM '.$table." ".$cond);
	       $num_fields = mysqli_num_fields($result);
	       $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
                for ($i = 0; $i < $num_fields; $i++) 
                  {
                       while($row = mysqli_fetch_row($result))
                       {
                             $is_update = 0;
                            if($table == 'offline_main_category')
                            {
                                 if(strtotime($row[6]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_sub_category')
                            {
                                 if(strtotime($row[4]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_content')
                            {
                                 if(strtotime($row[6]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_poll_survey_question')
                            {
                                 if(strtotime($row[7]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_poll_survey_options')
                            {
                                 if(strtotime($row[6]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_vm_department')
                            {
                                 if(strtotime($row[6]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_vm_employee')
                            {
                                 if(strtotime($row[9]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            else if($table == 'offline_vm_organisation')
                            {
                                 if(strtotime($row[5]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_contestifi_contest')
                            {
                                 if(strtotime($row[22]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_contestifi_quiz_questions')
                            {
                                 if(strtotime($row[7]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_contestifi_quiz_option')
                            {
                                 if(strtotime($row[8]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_contestifi_contest_rules')
                            {
                                 if(strtotime($row[6]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_contestifi_contest_prize')
                            {
                                 if(strtotime($row[7]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_contestifi_extra_feature')
                            {
                                 if(strtotime($row[8]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_event_module_turn_reature')
                            {
                                 if(strtotime($row[10]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
			    else if($table == 'offline_event_module_signup_fields')
                            {
                                 if(strtotime($row[9]) < strtotime($synced_on))
                                 {
                                      $is_update = 1;
                                 }
                            }
                            if($is_update == 0)
                            {
                               $return.= 'INSERT INTO '.$table.' VALUES(';
                                 for($j=0; $j < $num_fields; $j++) 
                                 {
                                      $row[$j] = addslashes($row[$j]);
                                      $row[$j] = str_replace("\n","\\n",$row[$j]);
                                     
                                               if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                           
                                           if ($j < ($num_fields-1)) { $return.= ','; }
                                       
                                 }
                                 $return.= ");\n";
                            }
                            else
                            {
                                 if($table == 'offline_main_category')
                                 {
                                      $return.= 'UPDATE '.$table. " set category_name = '".addslashes($row['4'])."' , category_desc = '".addslashes($row['5'])."',is_deleted = '".$row['8']."', icon = '".$row['9']."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_sub_category')
                                 {
                                      $return.= 'UPDATE '.$table. " set sub_category_name = '".addslashes($row['2'])."' , sub_category_desc = '".addslashes($row['3'])."',is_deleted = '".$row['6']."', icon = '".addslashes($row['10'])."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_content')
                                 {
                                      $return.= 'UPDATE '.$table. " set content_title = '".addslashes($row['3'])."' , content_desc = '".addslashes($row['4'])."', content_file = '".addslashes($row['5'])."', content_type = '".addslashes($row['9'])."', wikipedia_title_url = '".addslashes($row['15'])."', wikipedia_title = '".addslashes($row['16'])."', wikipedia_pageid = '".addslashes($row['17'])."', wikipedia_content = '".addslashes($row['18'])."',is_deleted = '".$row['8']."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_poll_survey_question')
                                 {
                                      $return.= 'UPDATE '.$table. " set question = '".addslashes($row['5'])."' , survey_type = '".addslashes($row['6'])."',is_deleted = '".$row['9']."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_poll_survey_options')
                                 {
                                      $return.= 'UPDATE '.$table. " set option_text = '".addslashes($row['5'])."',is_deleted = '".$row['8']."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_vm_department')
                                 {
                                      $return.= 'UPDATE '.$table. " set department_name = '".addslashes($row['5'])."',is_deleted = '".$row['8']."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_vm_employee')
                                 {
                                      $return.= 'UPDATE '.$table. " set employee_name = '".addslashes($row['6'])."',employee_email = '".addslashes($row['7'])."',employee_mobile = '".$row['8']."',is_deleted = '".$row['11']."', employee_last_name = '".addslashes($row['12'])."' where id= '".$row['0']."';";
                                 }
                                 else if($table == 'offline_vm_organisation')
                                 {
                                      $return.= 'UPDATE '.$table. " set organisation_name = '".addslashes($row['4'])."',is_deleted = '".$row['7']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_contestifi_contest')
                                 {
                                      $return.= 'UPDATE '.$table. " set contest_type = '".addslashes($row['4'])."',logo = '".addslashes($row['5'])."',small_logo = '".addslashes($row['6'])."',original_logo = '".addslashes($row['7'])."',button_color = '".addslashes($row['10'])."',contest_name = '".addslashes($row['11'])."',contest_start_time = '".$row['12']."',contest_end_time = '".$row['13']."',contest_frequency = '".$row['14']."',contestifi_custom_frequecny = '".$row['15']."',no_of_attemption = '".$row['16']."',points_per_question = '".$row['17']."',negative_point = '".$row['18']."',point_deduct_every_second = '".$row['19']."',max_time_allocation = '".$row['20']."',is_deleted = '".$row['21']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_contestifi_quiz_questions')
                                 {
                                      $return.= 'UPDATE '.$table. " set question = '".addslashes($row['5'])."',is_deleted = '".$row['6']."', contest_file = '".$row['9']."', contest_file_type = '".$row['10']."', survey_type = '".$row['11']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_contestifi_quiz_option')
                                 {
                                      $return.= 'UPDATE '.$table. " set option_text = '".addslashes($row['5'])."',is_right_option = '".$row['6']."',is_deleted = '".$row['7']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_contestifi_contest_rules')
                                 {
                                      $return.= 'UPDATE '.$table. " set contest_rule = '".addslashes($row['5'])."' ,is_deleted = '".$row['8']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_contestifi_contest_prize')
                                 {
                                      $return.= 'UPDATE '.$table. " set contest_prize = '".addslashes($row['5'])."',is_deleted = '".$row['6']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_contestifi_extra_feature')
                                 {
                                      $return.= 'UPDATE '.$table. " set macid = '".addslashes($row['2'])."',backgroud_color = '".addslashes($row['3'])."',is_otp_enable = '".addslashes($row['4'])."',is_signup_enable = '".addslashes($row['5'])."',country_mobile_code = '".addslashes($row['6'])."',	term_condition = '".addslashes($row['7'])."',is_loginemail = '".$row['10']."',name_require = '".$row['11']."',email_mobile_require = '".$row['12']."',age_require = '".$row['13']."',gender_require = '".$row['14']."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_event_module_turn_reature')
                                 {
                                      $return.= 'UPDATE '.$table. " set is_live_chat_enable = '".addslashes($row['4'])."',is_discuss_enable = '".addslashes($row['5'])."',is_information_enable = '".addslashes($row['6'])."',is_poll_enable = '".addslashes($row['7'])."',is_download_enable = '".addslashes($row['8'])."',theme_color = '".addslashes($row['9'])."' where id= '".$row['0']."';";
                                 }
				 else if($table == 'offline_event_module_signup_fields')
                                 {
                                      $return.= 'UPDATE '.$table. " set field_name = '".addslashes($row['4'])."',field_type = '".addslashes($row['5'])."',is_enable = '".addslashes($row['6'])."',is_require = '".addslashes($row['7'])."',is_deleted = '".addslashes($row['10'])."' where id= '".$row['0']."';";
                                 }
                                 $return .= "\n";
                            }
                            
                            
                       }
                  }
             
             $return.="\n\n\n";
              
        }
      
      //save file
      $handle = fopen($pathdb,'w+');
      fwrite($handle,$return);
      fclose($handle);
      
   }
     
     function backup_tables($host,$user,$pass,$name,$tables='*',$tablecond,$primarykeyremove,$notcontentarr,$pathdb)
     {
	  $link = mysqli_connect($host,$user,$pass,$name);
	  if($tables == '*')
	  {
	       $tables = array();
	       $result = mysqli_query($link,'SHOW TABLES');
	       while($row = mysqli_fetch_row($result))
	       {
		    $tables[] = $row[0];
	       }
	  }
	  else
	  {
	       $tables = is_array($tables) ? $tables : explode(',',$tables);
	  }
      
	  $return='';
	  $return.='CREATE DATABASE /*!32312 IF NOT EXISTS*/`radius` /*!40100 DEFAULT CHARACTER SET latin1 */;
        USE `radius`;';
$return .= "\n";
        //cycle through
	  foreach($tables as $table)
	  {
	       $l=0;
	       $cond='';
	       if(!empty($tablecond[$table]))
	       {
		    foreach($tablecond[$table] as $key=>$vald)
		    {
			 if($l==0)
			 {
			     $cond.="where $key='".$vald."'";
			 }
			 else
			 {
			      $cond.="and $key='".$vald."'";
			 }
			 $l++;
		    }
	       }
	       $result = mysqli_query($link,'SELECT * FROM '.$table." ".$cond);
	       $num_fields = mysqli_num_fields($result);
	       $return.= 'DROP TABLE IF EXISTS '.$table.';';
	       $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
	       $return.= "\n\n".$row2[1].";\n\n";
	       if(!in_array($table,$notcontentarr))
	       {
		    for ($i = 0; $i < $num_fields; $i++) 
		    {
			 while($row = mysqli_fetch_row($result))
			 {
			      $return.= 'INSERT INTO '.$table.' VALUES(';
			      for($j=0; $j < $num_fields; $j++) 
			      {
				   $row[$j] = addslashes($row[$j]);
				   $row[$j] = str_replace("\n","\\n",$row[$j]);
				   if(in_array($table,$primarykeyremove))
				   {
					if (isset($row[$j]) && $j!='0') { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					}
					else{
					    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
					}
					if ($j < ($num_fields-1)) { $return.= ','; }
				    
			      }
			      $return.= ");\n";
			 }
		    }
	       }
	       $return.="\n\n\n";
		
	  }
      
	  //save file
	  $handle = fopen($pathdb,'w+');
	  fwrite($handle,$return);
	  fclose($handle);
      
     }
        
     public function rcopy_synk($src, $dest,$synced_on = '')
     {
	  // If source is not a directory stop processing
	  if(!is_dir($src)) return false;
	  // If the destination directory does not exist create it
	  if(!is_dir($dest)) { 
	      if(!mkdir($dest)) {
		  // If the destination directory could not be created stop processing
		  return false;
	      }    
	  }
	  // Open the source directory to read in files
	  $i = new DirectoryIterator($src);
	  foreach($i as $f) {
	       if($f->isFile()) {
		    if($synced_on == '')
		    {
			 copy($f->getRealPath(), "$dest/" . $f->getFilename());
		    }
		    else
		    {
			 $file_last_modi_time = date ("Y-m-d H:i:s", filemtime($f->getRealPath()));
			 if(strtotime($file_last_modi_time) > strtotime($synced_on))
			 {
			      copy($f->getRealPath(), "$dest/" . $f->getFilename());
			 }
		    }
		    
		    
	       } else if(!$f->isDot() && $f->isDir()) {
		   $this->rcopy_synk($f->getRealPath(), "$dest/$f",$synced_on);
	       }
	  }
     }

     public function create_zip_for_all_location_manually()
     {
	  // get all resberry pie location
	  $get = $this->DB2->query("select location_uid from wifi_location_access_point where router_type = '7' group by location_uid");
	  if($get->num_rows())
	  {
	       foreach($get->result() as $row)
	       {
		    $location_uid = $row->location_uid;
		    // create zip
		    $requestData = array(
			 'location_uid' => $location_uid
		    );
		    $data_request = json_decode(json_encode($requestData));
		    $this->create_zip_for_offline_content($data_request);
	       }
	  }
     }
     
     public function contestifi_content_builder_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  
	  $contest_array = array();
	  // get contest list
	  $contest = $this->DB2->query("select id, contest_name, contest_type from offline_contestifi_contest where location_uid = '$location_uid' and is_deleted = '0'");
	  $i = 0;
	  foreach($contest->result() as $row){
	       $quiz_array = array();
	       $contest_id = $row->id;
	       $contest_array[$i]['id'] = $row->id;
	       $contest_array[$i]['contest_name'] = $row->contest_name;
	       $contest_array[$i]['contest_type'] = $row->contest_type;
	       //get quiz list
	       $quiz_query = $this->DB2->query("select id,question from offline_contestifi_quiz_questions where contest_id = '$contest_id' and is_deleted = '0'");
	        $j = 0;
	       foreach($quiz_query->result() as $row_sub){
		    $question_id = $row_sub->id;
		    $quiz_array[$j]['id'] = $row_sub->id;
		    $quiz_array[$j]['question'] = $row_sub->question;
		    $j++;
	       }
	       $contest_array[$i]['quiz_questions'] = $quiz_array;
	       
	       
	       // get prize list
	       $prize_array = array();
	       $prize_query = $this->DB2->query("select id,contest_prize from offline_contestifi_contest_prize where contest_id = '$contest_id' and is_deleted = '0'");
	       $k = 0;
	       foreach($prize_query->result() as $row_prize){
		    $prize_array[$k]['prize_id'] = $row_prize->id;
		    $prize_array[$k]['contest_prize'] = $row_prize->contest_prize;
		    $k++;
	       }
	       $contest_array[$i]['contest_prize'] = $prize_array;
	       
	       $i++;
	  }
		
	  $data['contest'] =  $contest_array;
	  $requestData = array(
	       'location_uid' => $location_uid
	  );
	  $data_request = json_decode(json_encode($requestData));
	  $this->create_zip_for_offline_content($data_request);
	  //echo "<pre>";print_r($data);die;
	  return json_encode($data);
     }
     
     public function add_contistifi_quiz_question($jsondata){
	  $contest_file = $jsondata->contest_file;
	  $contest_file_type = $jsondata->contest_file_type;
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $contest_id = $jsondata->contest_id;
	  $right_option = $jsondata->right_ans;
	  // add quiz question
	  $question_id = 0;
	  $poll_querstion = $jsondata->poll_querstion;
	  $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four);
	  
	  
	  $insert_data = array(
	       'isp_uid' => $isp_uid,
	       'location_id' => $locationid,
	       'location_uid' => $location_uid,
	       'contest_id' => $contest_id,
	       'question' => $poll_querstion,
	       'created_on' => date('Y-m-d H:i:s'),
	       'contest_file' => $contest_file,
	       'contest_file_type' => $contest_file_type,
	  );
	  $insert_question = $this->DB2->insert('offline_contestifi_quiz_questions', $insert_data);
	  
	       $question_id = $this->DB2->insert_id();
	       $i =1;
	       foreach($options as $options1){
		    if($options1 != ''){
			 $is_right_ans = 0;
			 if($i == $right_option)
			 {
			      $is_right_ans = 1;
			 }
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'is_right_option' => $is_right_ans,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_contestifi_quiz_option', $insert_data);
			 
			 $i++;
		    }
	       }
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_question_id'] = $question_id;
	
        return $data;
     }
     public function delte_contestifi_quiz_question($jsondata){
	  $question_id = $jsondata->question_id;
	  $location_id = $jsondata->location_id;
	  $data = array();
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $question_id);
	  $query = $this->DB2->update('offline_contestifi_quiz_questions', $update_data);
	  
	  // for update offline version
	  $this->update_offline_version($location_id);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     public function get_contestifi_quiz_question($jsondata){
	  $data = array();
	  $question_id = $jsondata->question_id;
	  $question = array();
	  $query = $this->DB2->query("select * from offline_contestifi_quiz_questions where id = '$question_id'");
	  $i = 0;
	  foreach($query->result() as $row){
	       $question_id = $row->id;
	       $question[$i]['question_id'] = $question_id;
	       $question[$i]['question'] = $row->question;
	       $question[$i]['contest_file'] = $row->contest_file;
	       $question[$i]['survey_type'] = $row->survey_type;
	       $options = array();
	       $get_option = $this->DB2->query("select * from offline_contestifi_quiz_option where question_id = '$question_id' and is_deleted = '0'");
	       $j = 0;
	       foreach($get_option->result() as $row_op){
		    $options[$j]['option_id'] = $row_op->id;
		    $options[$j]['option_text'] = $row_op->option_text;
		    $options[$j]['is_right_option'] = $row_op->is_right_option;
		    $j++;
	       }
	       $question[$i]['options'] = $options;
	       $i++;
	  }
	  $data['question'] = $question;
	 // echo "<pre>";print_r($data);die;
	  return json_encode($data);
     }
     public function update_contistifi_quiz_question($jsondata){
	  $contest_file = $jsondata->contest_file;
	  $contest_file_type = $jsondata->contest_file_type;
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $quiz_question_id = $jsondata->quiz_question_id;
	  $right_option = $jsondata->right_ans;
	  // update question
	  $poll_querstion = $jsondata->poll_querstion;
	  
	  
	  $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four);
	  $options = array_values(array_filter($options));
	  if($quiz_question_id > 0){
	       $file_update_condition = array();
	       if($contest_file != '')
	       {
		    $file_update_condition['contest_file'] = $contest_file;
		    $file_update_condition['contest_file_type'] = $contest_file_type;
	       }
	       $update_data = array(
		    'question' => $poll_querstion,
		    'updated_on' => date('Y-m-d H:i:s'),
	       );
	       $update_data = array_merge($update_data,$file_update_condition );
	       $this->DB2->where('id', $quiz_question_id);
	       $update_question = $this->DB2->update('offline_contestifi_quiz_questions', $update_data);
	       
	       $question_id = $quiz_question_id;
	       // get previous options
	       $get_options = $this->DB2->query("select id from offline_contestifi_quiz_option where question_id = '$question_id' and is_deleted = '0'");
	       $i = 1;
	       if($get_options->num_rows() > 0){
		    foreach($get_options->result() as $row_option){
			 $option_id = $row_option->id;
			 if(count($options) > 0){
			      $is_right_ans = 0;
			      if($i == $right_option)
			      {
				   $is_right_ans = 1;
			      }
			      $new_option_val = $options['0'];
			      if($new_option_val != '')
			      {
				   $update_data = array(
					'option_text' => $new_option_val,
					'is_right_option' => $is_right_ans,
					'updated_on' => date('Y-m-d H:i:s'),
				   );
				   $this->DB2->where('id', $option_id);
				   $update_option = $this->DB2->update('offline_contestifi_quiz_option', $update_data);
				   $i++;
			      }
			      
			      unset($options[0]);
			      $options = array_values($options);
			 }else{
			      // delete extra already created option
			      
			      $delete = $this->DB2->delete('offline_contestifi_quiz_option', array('id' => $option_id));
			 }
		    }
	       }
	       // add new added option
	       foreach($options as $options1){
		    if($options1 != ''){
			 $is_right_ans = 0;
			  if($i == $right_option)
			 {
			      $is_right_ans = 1;
			 }
			 $options1 = $options1;
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'is_right_option' => $is_right_ans,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_contestifi_quiz_option', $insert_data);
			 $i++;
		    }
	       }
	  }
	  else{
	       $contest_id = $jsondata->contest_id;
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'contest_id' => $contest_id,
		    'question' => $poll_querstion,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_question = $this->DB2->insert('offline_contestifi_quiz_questions', $insert_data);
	       $question_id = $this->DB2->insert_id();
	       $quiz_question_id = $question_id;
	       $i = 1;
	       foreach($options as $options1){
		    if($options1 != ''){
			 $is_right_ans = 0;
			  if($i == $right_option)
			 {
			      $is_right_ans = 1;
			 }
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'is_right_option' => $is_right_ans,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_contestifi_quiz_option', $insert_data);
			 
			 $i++;
		    }
	       }
	  }
	  
	  
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['update_question_id'] = $quiz_question_id;
	
        return $data;
     }
     public function add_contestifi_contest($jsondata){
	  $is_lms = 0;
	  if(isset($jsondata->is_lms))
	  {
	       $is_lms = $jsondata->is_lms;
	  }
	  $compliance_mandatory = 0;
	  if(isset($jsondata->compliance_mandatory))
	  {
	       $compliance_mandatory = $jsondata->compliance_mandatory;
	  }
	  $start_date = date("Y-m-d H:i:s", strtotime($jsondata->start_date));
	  $end_date = date("Y-m-d", strtotime($jsondata->end_date)). " 23:59:59";
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $contest_name = $jsondata->contest_name;
	  $contest_type = $jsondata->contest_type;
	  $bg_color = $jsondata->bg_color;
	  $text_color = $jsondata->text_color;
	  $button_color = $jsondata->button_color;
	  $contest_frequency = $jsondata->contest_frequency;
	  $contestifi_custom_frequecny = $jsondata->contestifi_custom_frequecny;
	  $contestifi_no_of_attemption = $jsondata->contestifi_no_of_attemption;
	  
	  $contestifi_point_per_question = $jsondata->contestifi_point_per_question;
	  $contestifi_negative_point = $jsondata->contestifi_negative_point;
	  $contestifi_point_deduct_every_secod = $jsondata->contestifi_point_deduct_every_secod;
	  $contestifi_max_time_allocation = $jsondata->contestifi_max_time_allocation;
	  $contestifi_contest_logo_image_original = $jsondata->contestifi_contest_logo_image_original;
	  $contestifi_contest_logo_image_logo = $jsondata->contestifi_contest_logo_image_logo;
	  $contestifi_contest_logo_image_small = $jsondata->contestifi_contest_logo_image_small;
	  $insert_data = array(
	       'location_uid' => $location_uid,
	       'location_id' => $locationid,
	       'isp_uid' => $isp_uid,
	       'contest_type' => $contest_type,
	       'backgroud_color' => $bg_color,
	       'text_color' => $text_color,
	       'button_color' => $button_color,
	       'contest_name' => $contest_name,
	       'contest_frequency' => $contest_frequency,
	       'contestifi_custom_frequecny' => $contestifi_custom_frequecny,
	       'no_of_attemption' => $contestifi_no_of_attemption,
	       'points_per_question' => $contestifi_point_per_question,
	       'negative_point' => $contestifi_negative_point,
	       'point_deduct_every_second' => $contestifi_point_deduct_every_secod,
	       'max_time_allocation' => $contestifi_max_time_allocation,
	       'logo' => $contestifi_contest_logo_image_logo,
	       'small_logo' => $contestifi_contest_logo_image_small,
	       'original_logo' => $contestifi_contest_logo_image_original,
	       'created_on' => date('Y-m-d H:i:s'),
	       'contest_start_time' => $start_date,
	       'contest_end_time' => $end_date,
	       'is_lms' => $is_lms,
	       'compliance_mandatory' => $compliance_mandatory,
	  );
	  $insert = $this->DB2->insert('offline_contestifi_contest', $insert_data);
	  
	  $insert_id = $this->DB2->insert_id();
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  
	  // insert rule
	  if($contest_type == '1')
	  {
	       $contestifi_contest_rule = $jsondata->contestifi_contest_rule;
	       if($contestifi_contest_rule != ''){
		    $contestifi_contest_rule = explode(',',$contestifi_contest_rule);
		    foreach($contestifi_contest_rule as $contestifi_contest_rule1)
		    {
			 if($contestifi_contest_rule1 != '')
			 {
			      $insert_data = array(
				   'contest_id' => $insert_id,
				   'contest_rule' => $contestifi_contest_rule1,
				   'created_on' => date('Y-m-d H:i:s'),
				   'isp_uid' => $isp_uid,
				   'location_id' => $locationid,
				   'location_uid' => $location_uid,
			      );
			      $insert = $this->DB2->insert('offline_contestifi_contest_rules', $insert_data);
			 }
		    }
	       }
	  }
	  
	  $data['created_contest_id'] = $insert_id;
	
        return $data;
     }

     public function delte_contestifi_contest($jsondata){
	  $contest_id = $jsondata->contest_id;
	  $location_id = $jsondata->location_id;
	  $data = array();
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $contest_id);
	  $query = $this->DB2->update('offline_contestifi_contest', $update_data);
	  // for update offline version
	  $this->update_offline_version($location_id);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     public function get_contestifi_contest_for_edit($jsondata){
	  $data = array();
	  $contest_id = $jsondata->contest_id;
	  $get_contest = $this->DB2->query("select * from offline_contestifi_contest where id = '$contest_id'");
	  if($get_contest->num_rows() > 0)
	  {
	       $data['resultCode'] = '1';
	       $row = $get_contest->row_array();
	       $data['contest_type'] = $row['contest_type'];
	       $original_logo = '';
	       if($row['original_logo'] != '')
	       {
		    $original_logo = "https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$row['original_logo'];
	       }
	       $data['original_logo'] = $original_logo;
	       $data['background_color'] = $row['backgroud_color'];
	       $data['text_color'] = $row['text_color'];
	       $data['button_color'] = $row['button_color'];
	       $data['contest_name'] = $row['contest_name'];
	       $data['contest_frequency'] = $row['contest_frequency'];
	       $data['contestifi_custom_frequecny'] = $row['contestifi_custom_frequecny'];
	       $data['no_of_attemption'] = $row['no_of_attemption'];
	       $data['points_per_question'] = $row['points_per_question'];
	       $data['negative_point'] = $row['negative_point'];
	       $data['point_deduct_every_second'] = $row['point_deduct_every_second'];
	       $data['max_time_allocation'] = $row['max_time_allocation'];
	       $contest_rule = array();
	       $get_rules = $this->DB2->query("select * from offline_contestifi_contest_rules where contest_id = '$contest_id'");
	       $i = 0;
	       foreach($get_rules->result() as $row_rules)
	       {
		    $contest_rule[$i]['rule'] = $row_rules->contest_rule;
		    $i++;
	       }
	       $data['contest_rule'] = $contest_rule;
	       $data['start_date'] = date("d-m-Y", strtotime($row['contest_start_time']));
	       $data['end_date'] = date("d-m-Y", strtotime($row['contest_end_time']));
	       $data['compliance_mandatory'] = $row['compliance_mandatory'];
	  }
	  else
	  {
	       $data['resultCode'] = '0';
	  }
	  return json_encode($data);
     }
     public function update_contestifi_contest($jsondata){
	  $compliance_mandatory = 0;
	  if(isset($jsondata->compliance_mandatory))
	  {
	       $compliance_mandatory = $jsondata->compliance_mandatory;
	  }
	  $start_date = date("Y-m-d H:i:s", strtotime($jsondata->start_date));
	  $end_date = date("Y-m-d", strtotime($jsondata->end_date)). " 23:59:59";
	  $contest_id = $jsondata->contest_id;
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $contest_name = $jsondata->contest_name;
	  $contest_type = $jsondata->contest_type;
	  $bg_color = $jsondata->bg_color;
	  $text_color = $jsondata->text_color;
	  $button_color = $jsondata->button_color;
	  $contest_frequency = $jsondata->contest_frequency;
	  $contestifi_custom_frequecny = $jsondata->contestifi_custom_frequecny;
	  $contestifi_no_of_attemption = $jsondata->contestifi_no_of_attemption;
	  
	  $contestifi_point_per_question = $jsondata->contestifi_point_per_question;
	  $contestifi_negative_point = $jsondata->contestifi_negative_point;
	  $contestifi_point_deduct_every_secod = $jsondata->contestifi_point_deduct_every_secod;
	  $contestifi_max_time_allocation = $jsondata->contestifi_max_time_allocation;
	  $contestifi_contest_logo_image_original = $jsondata->contestifi_contest_logo_image_original;
	  $contestifi_contest_logo_image_logo = $jsondata->contestifi_contest_logo_image_logo;
	  $contestifi_contest_logo_image_small = $jsondata->contestifi_contest_logo_image_small;
	  $image_update = array();
	  if($contestifi_contest_logo_image_original != '')
	  {
	      $image_update['logo'] = $contestifi_contest_logo_image_logo;
	      $image_update['small_logo'] = $contestifi_contest_logo_image_small;
	      $image_update['original_logo'] = $contestifi_contest_logo_image_original;
	  }
	  $update_data = array(
	       'contest_type' => $contest_type,
	       'backgroud_color' => $bg_color,
	       'contest_name' => $contest_name,
	       'contest_frequency' => $contest_frequency,
	       'contestifi_custom_frequecny' => $contestifi_custom_frequecny,
	       'no_of_attemption' => $contestifi_no_of_attemption,
	       'points_per_question' => $contestifi_point_per_question,
	       'negative_point' => $contestifi_negative_point,
	       'point_deduct_every_second' => $contestifi_point_deduct_every_secod,
	       'max_time_allocation' => $contestifi_max_time_allocation,
	       'updated_on' => date('Y-m-d H:i:s'),
	       'text_color' => $text_color,
	       'button_color' => $button_color,
	       'contest_start_time' => $start_date,
	       'contest_end_time' => $end_date,
	       'compliance_mandatory' => $compliance_mandatory,
	  );
	  $update_data = array_merge($update_data, $image_update);
	  $this->DB2->where('id', $contest_id);
	  $update = $this->DB2->update('offline_contestifi_contest', $update_data);
	  
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  if($contest_type == '1'){
	       
	  
	       $contestifi_contest_rule = $jsondata->contestifi_contest_rule;
	       
	       if($contestifi_contest_rule != '')
	       {
		    $contestifi_contest_rule = explode(',',$contestifi_contest_rule);
		    //print_r($contestifi_contest_rule);die;
		    // get previous rules
		    $get_rules = $this->DB2->query("select id from offline_contestifi_contest_rules where contest_id = '$contest_id'");
		    if($get_rules->num_rows() > 0)
		    {
			 foreach($get_rules->result() as $row_rule)
			 {
			      $rule_id = $row_rule->id;
			      if(count($contestifi_contest_rule) > 0)
			      {
				   $new_rule_val = $contestifi_contest_rule['0'];
				   $update_data = array(
					'contest_rule' => $new_rule_val,
				   );
				   $this->DB2->where('id', $rule_id);
				   $update_option = $this->DB2->update('offline_contestifi_contest_rules', $update_data);
				   unset($contestifi_contest_rule[0]);
				   $contestifi_contest_rule = array_values($contestifi_contest_rule);
			      }
			      else
			      {
				   // delete extra already created rules
				   $delete = $this->DB2->query("delete from offline_contestifi_contest_rules where id = '$rule_id'");
				   $delete = $this->DB2->delete('offline_contestifi_contest_rules', array('id' => $rule_id));
			      }
			 }
		    }
		    // add new added rules
		    foreach($contestifi_contest_rule as $contestifi_contest_rule1)
		    {
			 if($contestifi_contest_rule1 != '')
			 {
			      $insert_data = array(
				   'contest_id' => $contest_id,
				   'contest_rule' => $contestifi_contest_rule1,
				   'created_on' => date('Y-m-d H:i:s'),
				   'isp_uid' => $isp_uid,
				   'location_id' => $locationid,
				   'location_uid' => $location_uid,
			      );
			      $insert = $this->DB2->insert('offline_contestifi_contest_rules', $insert_data);
			 }
		    }
	       }
	  }
	  $data['created_contest_id'] = $contest_id;
	
        return $data;
     }
     

     public function add_contistifi_contest_prize($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $contest_id = $jsondata->contest_id;
	  $prize_id = $jsondata->prize_id;
	  // add quiz question
	  $contestifi_contest_prize = $jsondata->contestifi_contest_prize;
	  
	  if($prize_id != '')
	  {
	       
	       $insert_data = array(
		    'contest_prize' => $contestifi_contest_prize,
		    'updated_on' => date('Y-m-d H:i:s'),
	       );
	       $this->DB2->where('id', $prize_id);
	       $update = $this->DB2->update('offline_contestifi_contest_prize', $update_data);
	  }
	  else
	  {
	      $insert_data = array(
		    'contest_id' => $contest_id,
		    'contest_prize' => $contestifi_contest_prize,
		    'created_on' => date('Y-m-d H:i:s'),
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
	       );
	       $insert_question = $this->DB2->insert('offline_contestifi_contest_prize', $insert_data);
	       $prize_id = $this->DB2->insert_id(); 
	  }
	  
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_prize_id'] = $prize_id;
	
        return $data;
     }
     
     public function delte_contestifi_contest_prize($jsondata){
	  $prize_id = $jsondata->prize_id;
	  $location_id = $jsondata->location_id;
	  $data = array();
	  $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $this->DB2->where('id', $prize_id);
	  $query = $this->DB2->update('offline_contestifi_contest_prize', $update_data);
	  // for update offline version
	  $this->update_offline_version($location_id);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     
     
     public function contestifi_add_content_builder($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  
	  $this->update_offline_version($locationid);
	   // create zip
	  $requestData = array(
	       'location_uid' => $location_uid
	  );
	  $data_request = json_decode(json_encode($requestData));
	  $this->create_zip_for_offline_content($data_request);
	  $data['code'] = 1;
	
        return $data;
     }
     
     public function add_contestifi_extra_feature($jsondata)
     {
	  $user_name_required = 1;
	  if(isset($jsondata->user_name_required)){
	       $user_name_required = $jsondata->user_name_required;
	  }
	  $user_email_required = 1;
	  if(isset($jsondata->user_email_required)){
	       $user_email_required = $jsondata->user_email_required;
	  }
	  $user_age_required = 1;
	  if(isset($jsondata->user_age_required)){
	       $user_age_required = $jsondata->user_age_required;
	  }
	  $user_gender_required = 1;
	  if(isset($jsondata->user_gender_required)){
	       $user_gender_required = $jsondata->user_gender_required;
	  }
	  $is_loginemail = 0;
	  if(isset($jsondata->is_loginemail))
	  {
	       $is_loginemail = $jsondata->is_loginemail;
	  }
	  $type = '';
	  if(isset($jsondata->type))
	  {
	       $type = $jsondata->type;
	  }
	  $locationid = '';
	  if(isset($jsondata->locationid))
	  {
	       $locationid = $jsondata->locationid;
	  }
	  $background_color = '';
	  if(isset($jsondata->background_color))
	  {
	       $background_color = $jsondata->background_color;
	  }
	  $is_otp_enable = '0';
	  if(isset($jsondata->is_otp_enable))
	  {
	       $is_otp_enable = $jsondata->is_otp_enable;
	       if($is_otp_enable == 1)
	       {
		    $is_otp_enable = '0';
	       }
	       else
	       {
		    $is_otp_enable = '1';
	       }
	  }
	  $is_signup_enable = '';
	  if(isset($jsondata->is_signup_enable))
	  {
	       $is_signup_enable = $jsondata->is_signup_enable;
	  }
	  $country_code = '91';
	  if(isset($jsondata->country_code))
	  {
	       $country_code = $jsondata->country_code;
	  }
	  // check previus added
	  $check = $this->DB2->query("select id from offline_contestifi_extra_feature where location_id = '$locationid'");
	  if($check->num_rows() > 0)
	  {
	       if($type = 'cp')
	       {
		    $update_data = array(
			 'backgroud_color' => $background_color,
			 'is_otp_enable' => $is_otp_enable,
			 'is_signup_enable' => $is_signup_enable,
			 'country_mobile_code' => $country_code,
			 'updated_on' => date('Y-m-d H:i:s'),
			 'is_loginemail' => $is_loginemail,
			 'name_require' => $user_name_required,
			 'email_mobile_require' => $user_email_required,
			 'age_require' => $user_age_required,
			 'gender_require' => $user_gender_required,
		    );
		    $this->DB2->where('location_id', $locationid);
		    $this->DB2->update('offline_contestifi_extra_feature', $update_data);
		     
	       }
	  }
	  else
	  {
	       if($type == 'cp')
	       {
		    $insert_data = array(
			 'location_id' => $locationid,
			 'backgroud_color' => $background_color,
			 'is_otp_enable' => $is_otp_enable,
			 'is_signup_enable' => $is_signup_enable,
			 'country_mobile_code' => $country_code,
			 'created_on' => date('Y-m-d H:i:s'),
			 'is_loginemail' => $is_loginemail,
			 'name_require' => $user_name_required,
			 'email_mobile_require' => $user_email_required,
			 'age_require' => $user_age_required,
			 'gender_require' => $user_gender_required,
		    );
		    $insert_question = $this->DB2->insert('offline_contestifi_extra_feature', $insert_data);
	       }
	  }
     }
     
     public function get_contestifi_signup_term_condition($jsondata){
	  $location_id = $jsondata->location_id;
	  $data = array();
	  $terms = '';
	  $query = $this->DB2->query("select term_condition from offline_contestifi_extra_feature where location_id = '$location_id'");
	  if($query->num_rows() > 0){
	    $row = $query->row_array();
	    $terms = $row['term_condition'];
	  }
	  $data['terms'] = $terms;
	  return json_encode($data);
     }
     public function add_contestifi_terms_conditon($jsondata){
	  $location_id = $jsondata->location_id;
	  $terms = $jsondata->terms;
	  $data = array();
	  $query = $this->DB2->query("select id from offline_contestifi_extra_feature where location_id = '$location_id'");
	  if($query->num_rows() > 0){
	       $update_data = array(
		    'term_condition' => $terms,
		    'updated_on' => date('Y-m-d H:i:s'),
	       );
	       $this->DB2->where('location_id', $location_id);
	       $this->DB2->update('offline_contestifi_extra_feature', $update_data);
	  }
	  else
	  {
	      
	       $insert_data = array(
		    'location_id' => $location_id,
		    'term_condition' => $terms,
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert_question = $this->DB2->insert('offline_contestifi_extra_feature', $insert_data);
	  }
	  $data['resuldCode'] = '1';
	  return json_encode($data);
     }
     
     
     public function contestifi_otp_send($jsondata){
	  $msg = $jsondata->msg;
	  $phone = $jsondata->phone;
	  $msg91authkey = '106103ADQeqKxOvbT856d19deb';
	  $sender = 'SHOUUT';
	  $postData = array(
	       'authkey' => $msg91authkey, //'106103ADQeqKxOvbT856d19deb',
	       'mobiles' => $phone,
	       'message' => $msg,
	       'sender' => $sender, //'SHOUUT',
	       'route' => '4'
	  );
		//echo "<pre>"; print_R($postData); die;

        $url="https://control.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
           CURLOPT_URL => $url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_POST => true,
           CURLOPT_POSTFIELDS => $postData
           //,CURLOPT_FOLLOWLOCATION => true
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
     }
     
     public function add_contistifi_survey_question($jsondata){
	  $contest_file = $jsondata->contest_file;
	  $contest_file_type = $jsondata->contest_file_type;
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $contest_id = $jsondata->contest_id;
	  $survey_type = $jsondata->survey_type;
	  // add quiz question
	  $question_id = 0;
	  $poll_querstion = $jsondata->poll_querstion;
	  $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four);
	  
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'contest_id' => $contest_id,
		    'question' => $poll_querstion,
		    'created_on' => date('Y-m-d H:i:s'),
		    'contest_file' => $contest_file,
		    'contest_file_type' => $contest_file_type,
		    'survey_type' => $survey_type,
	       );
	       $insert_question = $this->DB2->insert('offline_contestifi_quiz_questions', $insert_data);
	       
	       $question_id = $this->DB2->insert_id();
	       $i =1;
	       foreach($options as $options1){
		    if($options1 != ''){
			 $is_right_ans = 0;
			 
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'is_right_option' => $is_right_ans,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_contestifi_quiz_option', $insert_data);
			 $i++;
		    }
	       }
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_question_id'] = $question_id;
	
        return $data;
     }
     
     
     
     public function update_contistifi_survey_question($jsondata){
	  $contest_file = $jsondata->contest_file;
	  $contest_file_type = $jsondata->contest_file_type;
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $quiz_question_id = $jsondata->quiz_question_id;
	  $survey_type = $jsondata->survey_type;
	  // update question
	  $poll_querstion = $jsondata->poll_querstion;
	  
	  
	  $options = array($jsondata->option_one,$jsondata->option_two,$jsondata->option_three,$jsondata->option_four);
	  $options = array_values(array_filter($options));
	  if($quiz_question_id > 0){
	       
	       $update_data = array(
		    'question' => $poll_querstion,
		    'updated_on' => date('Y-m-d H:i:s'),
		    'survey_type' => $survey_type
	       );
	       $this->DB2->where('id', $quiz_question_id);
	       $update_question  = $this->DB2->update('offline_contestifi_quiz_questions', $update_data);
	       
	       $question_id = $quiz_question_id;
	       // get previous options
	       $get_options = $this->DB2->query("select id from offline_contestifi_quiz_option where question_id = '$question_id' and is_deleted = '0'");
	       $i = 1;
	       if($get_options->num_rows() > 0){
		    foreach($get_options->result() as $row_option){
			 $option_id = $row_option->id;
			 if(count($options) > 0){
			      $is_right_ans = 0;
			      
			      $new_option_val = $options['0'];
			      if($new_option_val != '')
			      {
				   
				   $update_data = array(
					'option_text' => $new_option_val,
					'is_right_option' => $is_right_ans,
					'updated_on' => date('Y-m-d H:i:s'),
					
				   );
				   $this->DB2->where('id', $option_id);
				   $update_option   = $this->DB2->update('offline_contestifi_quiz_option', $update_data);
				   $i++;
			      }
			      
			      unset($options[0]);
			      $options = array_values($options);
			 }else{
			      // delete extra already created option
			     
			      $delete = $this->DB2->delete('offline_contestifi_quiz_option', array('id' => $option_id));
			 }
		    }
	       }
	       // add new added option
	       foreach($options as $options1){
		    if($options1 != ''){
			 $is_right_ans = 0;
			  
			 $options1 = $options1;
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'is_right_option' => $is_right_ans,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_contestifi_quiz_option', $insert_data);
			 $i++;
		    }
	       }
	  }
	  else{
	       $contest_id = $jsondata->contest_id;
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'contest_id' => $contest_id,
		    'question' => $poll_querstion,
		    'created_on' => date('Y-m-d H:i:s'),
		    'survey_type' => $survey_type,
	       );
	       $insert_question = $this->DB2->insert('offline_contestifi_quiz_questions', $insert_data);
	       $question_id = $this->DB2->insert_id();
	       $quiz_question_id = $question_id;
	       $i = 1;
	       foreach($options as $options1){
		    if($options1 != ''){
			 $is_right_ans = 0;
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'question_id' => $question_id,
			      'option_text' => $options1,
			      'is_right_option' => $is_right_ans,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert_option = $this->DB2->insert('offline_contestifi_quiz_option', $insert_data);
			 $i++;
		    }
	       }
	  }
	  
	  
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['update_question_id'] = $quiz_question_id;
	
        return $data;
     }
     
     
     public function retail_audit_sku_pin_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $query = $this->DB2->query("select id, sku from retailaudit_sku where location_uid = '$location_uid' AND is_deleted = '0' order by id desc");
	  $i = 0;
	  if($query->num_rows() > 0){
	       $sku_list = array();
	       foreach($query->result() as $row){
		    $sku_list[$i]['sku_id'] = $row->id;
		    $sku_list[$i]['sku_name'] = $row->sku;
		    $i++;
	       }
	       $data['skuresultCode'] = 1;
	       $data['sku_list'] = $sku_list;
	  }
	  else{
	      $data['skuresultCode'] = 0; 
	  }
	  $pin_get = $this->DB2->query("select pin, promoter_pin from retailaudit_login_pin where location_uid = '$location_uid'");
	  if($pin_get->num_rows() > 0)
	  {
	       $data['pinresultCode'] = 1;
	       $row_pin = $pin_get->row_array();
	       $data['audit_pin'] = $row_pin['pin'];
	       $data['promoter_pin'] = $row_pin['promoter_pin'];
	  }
	  else
	  {
	       $data['pinresultCode'] = 0;
	  }
	  
	  
	  return json_encode($data);
     }
     
     public function add_retail_audit_sku($jsondata){
	  
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $sku_name= $jsondata->sku_name;
	  $sku_id = $jsondata->sku_id;
	  $resultCode = 0;
	  if($sku_id != '')
	  {
	       //check
	       $check = $this->DB2->query("select sku from retailaudit_sku where location_uid = '$location_uid' AND sku = '$sku_name' AND id != '$sku_id' AND is_deleted = '0'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    $update_data = array(
			 'sku' => $sku_name,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $sku_id);
		    $insert = $this->DB2->update('retailaudit_sku', $update_data);
		    
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  else
	  {
	       //check
	       $check = $this->DB2->query("select sku from retailaudit_sku where location_uid = '$location_uid' AND sku = '$sku_name' AND is_deleted = '0'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    $insert_data = array(
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'isp_uid' => $isp_uid,
			 'sku' => $sku_name,
			 'created_on' => date('Y-m-d H:i:s'),
		    );
		    $insert = $this->DB2->insert('retailaudit_sku', $insert_data);
	       }
	       else
	       {
		    $resultCode = 0;
	       }
	  }
	  
	  $data['resultCode'] = $resultCode;
	
        return $data;
     }
     public function add_retail_audit_pin($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $resultCode = 1;
	  if(isset($jsondata->login_pin))
	  {
	       $login_pin = $jsondata->login_pin;
	       // check if already exist in any program
	       $this->DB2->select("cp_offer_id")->from("cp_offers");
	       $this->DB2->where(array("program_code" => $login_pin));
	       $check = $this->DB2->get();
	       if($check->num_rows() > 0)
	       {
		   $resultCode = 0;
	       }
	       if($resultCode == 1)
	       {
		    $check = $this->DB2->query("select id,promoter_pin from retailaudit_login_pin where location_uid = '$location_uid'");
		    if($check->num_rows() <= 0)
		    {
			 $insert_data = array(
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'isp_uid' => $isp_uid,
			      'pin' => $login_pin,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert = $this->DB2->insert('retailaudit_login_pin', $insert_data);
		    }
		    else
		    {
			 $row = $check->row_array();
			 $pin_id = $row['id'];
			 $promotion_code_for_this_loc = $row['promoter_pin'];
			 if($promotion_code_for_this_loc == $login_pin)
			 {
			      $resultCode = 0;
			 }
			 else
			 {
			      $update_data = array(
				   'pin' => $login_pin,
				   'updated_on' => date('Y-m-d H:i:s'),
			      );
			      $this->DB2->where('id', $pin_id);
			      $update = $this->DB2->update('retailaudit_login_pin', $update_data);
			 }
			 
		    }
	       }
	  }
	  if(isset($jsondata->promoter_pin))
	  {
	       $promoter_pin = $jsondata->promoter_pin;
	       //check
	       
	       // check if already exist in any program
	       $this->DB2->select("cp_offer_id")->from("cp_offers");
	       $this->DB2->where(array("program_code" => $promoter_pin));
	       $check = $this->DB2->get();
	       if($check->num_rows() > 0)
	       {
		   $resultCode = 0;
	       }
	       if($resultCode == 1)
	       {
		   $check = $this->DB2->query("select id,pin from retailaudit_login_pin where location_uid = '$location_uid'");
		    if($check->num_rows() <= 0)
		    {
			 $insert_data = array(
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'isp_uid' => $isp_uid,
			      'promoter_pin' => $promoter_pin,
			      'created_on' => date('Y-m-d H:i:s'),
			 );
			 $insert = $this->DB2->insert('retailaudit_login_pin', $insert_data);
		    }
		    else
		    {
			 $row = $check->row_array();
			 $pin_id = $row['id'];
			 $store_code_for_this_loc = $row['pin'];
			 if($store_code_for_this_loc == $promoter_pin)
			 {
			      $resultCode = 0;
			 }
			 else
			 {
			      $update_data = array(
				   'promoter_pin' => $promoter_pin,
				   'updated_on' => date('Y-m-d H:i:s'),
			      );
			      $this->DB2->where('id', $pin_id);
			      $update = $this->DB2->update('retailaudit_login_pin', $update_data);
			 }
			 
		    }
	       }
	  }
	  $data['resultCode'] = $resultCode;
	
	  return $data;
	  
     }
     public function add_retail_audit_pin_old($jsondata){
	  
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  
	  
	  
	  $resultCode = 0;
	  if(isset($jsondata->login_pin))
	  {
	       $login_pin = $jsondata->login_pin;
	       //check
	       $check = $this->DB2->query("select id from retailaudit_login_pin where location_uid = '$location_uid'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    $insert_data = array(
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'isp_uid' => $isp_uid,
			 'pin' => $login_pin,
			 'created_on' => date('Y-m-d H:i:s'),
		    );
		    $insert = $this->DB2->insert('retailaudit_login_pin', $insert_data);
	       }
	       else
	       {
		    $resultCode = 1;
		    $row = $check->row_array();
		    $pin_id = $row['id'];
		    
		    $update_data = array(
			 'pin' => $login_pin,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $pin_id);
		    $update = $this->DB2->update('retailaudit_login_pin', $update_data);
	       }
	  }
	       
	  if(isset($jsondata->promoter_pin))
	  {
	       $promoter_pin = $jsondata->promoter_pin;
	       //check
	       $check = $this->DB2->query("select id from retailaudit_login_pin where location_uid = '$location_uid'");
	       if($check->num_rows() <= 0)
	       {
		    $resultCode = '1';
		    $insert_data = array(
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'isp_uid' => $isp_uid,
			 'promoter_pin' => $promoter_pin,
			 'created_on' => date('Y-m-d H:i:s'),
		    );
		    $insert = $this->DB2->insert('retailaudit_login_pin', $insert_data);
	       }
	       else
	       {
		    $resultCode = 1;
		    $row = $check->row_array();
		    $pin_id = $row['id'];
		    
		    $update_data = array(
			 'promoter_pin' => $promoter_pin,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $pin_id);
		    $update = $this->DB2->update('retailaudit_login_pin', $update_data);
	       }
	  }
	  $data['resultCode'] = $resultCode;
	
        return $data;
     }
     public function add_content_survey_retail_audit($jsondata){
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $category_id = $jsondata->category_id;
	  $is_main_category = $jsondata->is_main_category;
	  $content_title = $jsondata->content_title;
	  $content_desc = '';
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $question = $jsondata->questions;
	  
	  $insert_data = array(
	       'isp_uid' => $isp_uid,
	       'location_id' => $locationid,
	       'location_uid' => $location_uid,
	       'fid' => $category_id,
	       'is_main_category' => $is_main_category,
	       'content_title' => $content_title,
	       'content_desc' => $content_desc,
	       'content_file' => $content_file,
	       'created_on' => date('Y-m-d H:i:s'),
	       'content_type' => $content_type,
	  );
	  $create = $this->DB2->insert('offline_content', $insert_data);
	  
	  $created_category_id = $this->DB2->insert_id();
	  foreach($question as $question1){
	       $question_type = $question1->question_type;
	       $health_deduct= $question1->health_deduct;
	       $is_sku_use = $question1->is_sku_use;
	       $question = $question1->question;
	      
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'content_id' => $created_category_id,
		    'question' => $question,
		    'survey_type' => $question_type,
		    'created_on' => date('Y-m-d H:i:s'),
		    'health_deduction' => $health_deduct,
		    'is_use_sku' => $is_sku_use
	       );
	       $insert_question = $this->DB2->insert('retailaudit_survey_question', $insert_data);
	       $question_id = $this->DB2->insert_id();
	       /*$options = $question1->options;
	       foreach($options as $options1){
		    $insert_option = $this->DB2->query("insert into retailaudit_survey_option(isp_uid, location_id, location_uid,question_id, option_text, created_on) values('$isp_uid', '$locationid', '$location_uid','$question_id', '$options1', now())");
		    
	       }*/
	       
	  }
	       
		
	  $data['created_category_id'] = $created_category_id;
	
        return $data;
     }
     public function get_content_poll_survey_retail_audit($jsondata){
	  $data = array();
	  $content_id = $jsondata->content_id;
	  $question = array();
	  $query = $this->DB2->query("select * from retailaudit_survey_question where content_id = '$content_id' AND is_deleted = '0'");
	  $i = 0;
	  foreach($query->result() as $row){
	       $question_id = $row->id;
	       $question[$i]['question_id'] = $question_id;
	       $question[$i]['question'] = $row->question;
	       $question[$i]['survey_type'] = $row->survey_type;
	       $question[$i]['health_deduction'] = $row->health_deduction;
	       $question[$i]['is_use_sku'] = $row->is_use_sku;
	       $options = array();
	       /*$get_option = $this->DB2->query("select * from retailaudit_survey_option where question_id = '$question_id' AND is_deleted = '0'");
	       $j = 0;
	       foreach($get_option->result() as $row_op){
		    $options[$j]['option_id'] = $row_op->id;
		    $options[$j]['option_text'] = $row_op->option_text;
		    $options[$j]['is_flag'] = $row_op->is_flag;
		    $j++;
	       }*/
	       $question[$i]['options'] = $options;
	       $i++;
	  }
	  $data['question'] = $question;
	 // echo "<pre>";print_r($data);die;
	  return json_encode($data);
     }
     
     public function update_content_survey_retail_audit($jsondata){
	  
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $content_title = $jsondata->content_title;
	  $content_desc = '';
	  $content_file = $jsondata->content_file;
	  $content_type = $jsondata->file_type;
	  $content_id = $jsondata->content_id;
	  $file_previous_name = $jsondata->file_previous_name;
	  $question = $jsondata->questions;
	  $file_update = array();
	  if($content_file != ''){
		    $file_update['content_file'] = $content_file;
	  }else{
	       if($file_previous_name == ''){
		    $file_update['content_file'] = $file_previous_name;
	       }
	  }
	  
	  $update_data = array(
	       'content_title' => $content_title,
	       'updated_on' => date('Y-m-d H:i:s'),
	  );
	  $update_data = array_merge($update_data, $file_update);
	  $this->DB2->where('id', $content_id);
	  $update_content = $this->DB2->update('offline_content', $update_data);
	 
	  $created_category_id = $content_id;
	  
	  
	  
	  
	  $question = $jsondata->questions;
	  $previous_question_id = array();
	  $get = $this->DB2->query("select id from retailaudit_survey_question where content_id = '$content_id' AND is_deleted = '0'");
	  foreach($get->result() as $row_old_ques){
	      $previous_question_id[] = $row_old_ques->id;
	  }
	  foreach($question as $question1){
	       $question_type = $question1->question_type;
	       $health_deduct= $question1->health_deduct;
	       $is_sku_use = $question1->is_sku_use;
	       $question_val = $question1->question;
	       if(count($previous_question_id) > 0){// update question
		    $question_id_to_update = $previous_question_id[0];
		    
		    
		    $update_data = array(
			 'health_deduction' => $health_deduct,
			 'is_use_sku' => $is_sku_use,
			 'question' => $question_val,
			 'survey_type' => $question_type,
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $question_id_to_update);
		    $update_content = $this->DB2->update('retailaudit_survey_question', $update_data);
		    // get new options
		    $options = $question1->options;
		    $options_new = array();
		    foreach($options as $options1){
			$options_new[] = $options1;
		    }
		    $options_new = array_values(array_filter($options_new));
		    // get previous options
		   /* $get_options = $this->DB2->query("select id from retailaudit_survey_option where question_id = '$question_id_to_update' AND is_deleted = '0'");
		    if($get_options->num_rows() > 0){
			 foreach($get_options->result() as $row_option){
			      $option_id = $row_option->id;
			      if(count($options_new) > 0){
				   $new_option_val = $options_new['0'];
				   $update_option = $this->DB2->query("update retailaudit_survey_option set option_text = '$new_option_val', updated_on = now() where id = '$option_id'");
				   unset($options_new[0]);
				   $options_new = array_values($options_new);
			      }else{
				   // delete extra already created option
				   $delete = $this->DB2->query("update retailaudit_survey_option set is_deleted = '1', updated_on = now() where id = '$option_id'");
			      }
			      
			      
			 }
		    }
		    // add new added option
		    foreach($options_new as $options_new1){
			 if($options1 != ''){
			      $insert_option = $this->DB2->query("insert into retailaudit_survey_option(isp_uid, location_id, location_uid,question_id, option_text, created_on) values('$isp_uid', '$locationid', '$location_uid','$question_id_to_update', '$options_new1', now())");
			 }
		    }*/
		    
		    // remove question from array
		    unset($previous_question_id[0]);
		    $previous_question_id = array_values($previous_question_id);
	       }else{// create new question
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'content_id' => $created_category_id,
			 'question' => $question_val,
			 'survey_type' => $question_type,
			 'created_on' => date('Y-m-d H:i:s'),
			 'health_deduction' => $health_deduct,
			 'is_use_sku' => $is_sku_use
		    );
		    $insert_question = $this->DB2->insert('retailaudit_survey_question', $insert_data);
		    $question_id = $this->DB2->insert_id();
		    /*$options = $question1->options;
		    foreach($options as $options1){
			 $insert_option = $this->DB2->query("insert into retailaudit_survey_option(isp_uid, location_id, location_uid,question_id, option_text, created_on) values('$isp_uid', '$locationid', '$location_uid','$question_id', '$options1', now())");
			 
		    }*/
	       }
	  }
	  // delete extra question
	  if(count($previous_question_id) > 0){
	       foreach($previous_question_id as $previous_question_id1){
		    $update_data = array(
			 'is_deleted' => '1',
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $previous_question_id1);
		    $detele = $this->DB2->update('retailaudit_survey_question', $update_data);
		    //$delete = $this->DB2->query("update retailaudit_survey_question set is_deleted = '1', updated_on = now() where question_id = '$previous_question_id1'");
	       }
	  }
	  
	       
	  
	  // update offline version
	  $this->update_offline_version($locationid);
		
	  $data['created_category_id'] = $content_id;
	
        return $data;
     }
     
     public function delete_retail_audit_sku_name($jsondata){
	  $sku_id = $jsondata->sku_id;
	  
	  $data = array();
	  
	  $update_data = array(
			 'is_deleted' => '1',
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $sku_id);
		    $detele = $this->DB2->update('retailaudit_sku', $update_data);
	
	$locationid = 0;
	  if(isset($jsondata->locationid)){
	       $locationid = $jsondata->locationid;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     
     public function add_event_module_signup_extra_field($jsondata){
	  
	  $locationid = $jsondata->locationid;
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = trim($jsondata->location_uid);
	  $field_name = $jsondata->field_name;
	  $resultCode = 0;
	  
	       //check
	  $check = $this->DB2->query("select id from offline_event_module_signup_fields where location_uid = '$location_uid' AND field_name = '$field_name'  AND is_deleted = '0'");
	  if($check->num_rows() <= 0)
	  {
	       $insert_data = array(
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'isp_uid' => $isp_uid,
		    'field_name' => $field_name,
		    'field_type' => '0',
		    'is_enable' => '1',
		    'is_required' => '0',
		    'created_on' => date('Y-m-d H:i:s'),
	       );
	       $insert = $this->DB2->insert('offline_event_module_signup_fields', $insert_data);
	       
	       $resultCode = $this->DB2->insert_id();
	  }
	  else
	  {
	       $resultCode = 0;
	  }
	  
	  
	  $data['resultCode'] = $resultCode;
	
        return $data;
     }
     public function delete_event_module_signup_field($jsondata){
	  $field_id = $jsondata->field_id;
	  
	  $data = array();
	  
	
	$update_data = array(
			 'is_deleted' => '1',
			 'updated_on' => date('Y-m-d H:i:s'),
		    );
		    $this->DB2->where('id', $field_id);
		    $detele = $this->DB2->update('offline_event_module_signup_fields', $update_data);
	$locationid = 0;
	  if(isset($jsondata->locationid)){
	       $locationid = $jsondata->locationid;
	  }
	  // update offline version
	  $this->update_offline_version($locationid);
	  
	  $data['resultCode'] = '1';
           
	  return $data;
     }
     
     public function import_location(){
	  $this->load->library('Excel');
	  $isp_uid = '233';
	  $file = $_SERVER['DOCUMENT_ROOT'].'/isp_consumer_api/assets/Workbook4.xlsx';
	  //echo $file;die;
	  if(file_exists($file))
	  {
	       $objPHPExcel = PHPExcel_IOFactory::load($file);
	       //get only the Cell Collection
	       $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
	       foreach ($cell_collection as $cell)
	       {
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		    if ($row == 1)
		    {
			 $header[$row][$column] = $data_value;
		    }
		    else
		    {
			 $arr_data[$row][$column] = $data_value;
		    }
	       }
		
	       //send the data in an array format
	       $data['header'] = $header;
	       $data['values'] = $arr_data;
	       //echo '<pre>'; print_r($data); echo '</pre>'; die;
	       $this->inport_excel_create_location($arr_data);
	  }
	  else
	  {
	       echo "No such excel is associated with this user.";
	  }
     }
     public function inport_excel_create_location($arr_data)
     {
	  foreach($arr_data as $arr_data1)
	  {
	       $isp_uid = '233';//check
	       
	       $requestData = array(
			  'isp_uid' => $isp_uid
			 );
	       $data_request = json_decode(json_encode($requestData));
	       $autogenerate_uid = $this->location_autogenerate_id($data_request);
	       $location_id = $autogenerate_uid['location_id'];
	       $created_location_id = '';
	       $hashtags = "#".$this->DB2->escape_str($arr_data1['F']);
	       $enable_channel = '1';
	       $enable_nas_down_notification = '0';
	       $location_type = '13';
	       $location_name = $this->DB2->escape_str($arr_data1['B']);
	       $geo_address = $this->DB2->escape_str($arr_data1['C']);
	       $placeid= '';
	       $lat= '';
	       $long= '';
	       $address1= '';
	       $address2= '';
	       $pin= '';
	       $state= '';
	       $city= '';
	       $zone= '';
	       $contact_person_name= $this->DB2->escape_str($arr_data1['G']);
	       $email_id= $this->DB2->escape_str($arr_data1['H']);
	       $mobile_no= $this->DB2->escape_str($arr_data1['I']);
	       $store_unique_id = $this->DB2->escape_str($arr_data1['A']);
	       $city_name = '';
	       $state_name = '';
	       $zone_name = '';
	       $location_brand = 'sh';
	       
	       
	  
	       $location_insert_data = array(
		    'enable_nas_down_noti' => $enable_nas_down_notification,
		    'enable_channel' => $enable_channel,
		    'location_uid' => $location_id,
		    'location_type' => $location_type,
		    'location_name' => $location_name,
		    'geo_address' => $geo_address,
		    'placeid' => $placeid,
		    'latitude' => $lat,
		    'longitude' => $long,
		    'region_id' => $zone,
		    'address_1' => $address1,
		    'address_2' => $address2,
		    'state' => $state_name,
		    'state_id' => $state,
		    'city' => $city_name,
		    'city_id' => $city,
		    'pin' => $pin,
		    'contact_person_name' => $contact_person_name,
		    'mobile_number' => $mobile_no,
		    'status' => '1',
		    'is_deleted' => '0',
		    'created_on' => date('Y-m-d H:i:s'),
		    'isp_uid' => $isp_uid,
		    'user_email' => $email_id,
		    'location_brand' => $location_brand,
		    'provider_id' => $isp_uid,
		    'hashtags' => $hashtags,
		    'zone_name' => $zone_name,
		    'store_unique_id' => $store_unique_id
	       );
	       $insert = $this->DB2->insert('wifi_location', $location_insert_data);
	       $created_location_id_return = $this->DB2->insert_id();
	       $hashtags = preg_replace('/(\w+)([#])/U', '\\1 \\2', $hashtags);
	       $has_array = explode(" ",$hashtags);
	       if(count($has_array) > 0)
	       {
		    foreach($has_array as $has_array_new)
		    {
			 if($has_array_new != '' && preg_match("/#(\\w+)/", $has_array_new))
			 {
			      $this->DB2->select('id')->from('wifi_location_hashtags');
			      $this->DB2->where('hashtags', $has_array_new);
			      $get_tags = $this->DB2->get();
			      if($get_tags->num_rows() <= 0)
			      {
				   $insert_data = array(
					'hashtags' => $has_array_new,
					'location_id' => $created_location_id,
					'created_on' => date('Y-m-d H:i:s'),
				   );
				   $this->DB2->insert('wifi_location_hashtags', $insert_data);
			      }
			      
			      
			 }
		    }
	       }
	       // create cp
	       //$tatdays = $this->DB2->escape_str($arr_data1['K']);
	       $tatdays = 3;
	       $created_cp_id = $this->inport_excel_create_location_cp($isp_uid,$location_id,$created_location_id_return,$tatdays);
	       //upload image, sku, store_pin, promoter_pin
	       $reference_location_uid = $this->DB2->escape_str($arr_data1['J']);
	       if($reference_location_uid != '')
	       {
		    $this->import_excel_sku_pin_image_etc($created_cp_id,$reference_location_uid,$location_id,$created_location_id_return,$isp_uid);
	       
	       // impprt question
	       $this->import_excel_main_category_and_question($created_location_id_return,$isp_uid,$location_id,$reference_location_uid);
	       }
	       
	       //echo $hashtags;die;
	  }
	  echo "done";
     }
     public function inport_excel_create_location_cp($isp_uid,$location_id,$created_location_id_return,$tatdays)
     {
	 
	  $is_preaudit_disable = '0';$audit_ticket_email_send = '';$is_white_theme = '0';
	  $contestifi_contest_header = '';$cp_cafe_retail_plan_type = "0";$is_offline_registration = 0;
	  $cp_type = 'cp_retail_audit';$location_uid = trim($location_id);$locationid = $created_location_id_return;
	  $synk_frequency = "0";$offline_cp_source_folder_path = "";$offline_cp_landing_page_path = "";
	  $offline_cp_type = "0";
	  if($cp_type == 'cp_retail_audit'){
               $cp_url_bitly_set = OFFLINE_CODE.'retail_audit/index.php';
               $url = $cp_url_bitly_set."?bcklocid=".$locationid."&bckisp_uid=".$isp_uid;
		    
	       $bitly_url = $this->get_bitly_short_url($url);
	       //$this->DB2->set('bitly_url', $bitly_url)->where('id', $locationid);
	       //$this->DB2->update('wifi_location');
	  }
	  $original_slider1 = "";$original_slider2 = "";$custom_captive_url = '';
	  $custom_location_plan = '0';$custon_cp_signip_data = '0';$custon_cp_daily_data = '0';
	  $custon_cp_no_of_daily_session = '0';$is_otpdisabled = '0';$cp_cafe_plan_id = '';
	  $num_of_session_per_day = 0;$icon_color = "";$is_sociallogin = "0";
	  $is_pinenable = "0";$is_wifidisabled = "0";$login_with_mobile_email = "0";
	  $offer_redemption_mode = "0";$cp_hotel_plan_type = '';$cp_home_selected_plan_id = '';
	  $original_image = '';$small_image = '';$logo_image = '';
	  $retail_original_image = '';$retail_small_image = '';$retail_logo_image = '';
	  $enterprise_user_type = 0;$institutional_user_list = '';$main_ssid = 'Test';$guest_ssid = '';
	  $created_cp_id = '';$cp_hotel_plan_type = '2';$cafe_plan_type = "time";
	  $query = $this->DB2->query("select id,cp_type from wifi_location_cptype where location_id = '$locationid'");
	  if($query->num_rows() > 0)
	  {
	       
	  }
	  else
	  {
	       $random_salt = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,2 ) .substr( md5( time() ), 1, 5);
	       $random_salt = md5($location_uid.$random_salt);
	       $api_key = time().md5($random_salt);
	       $offline_build_version = 1;
	       $insert_data = array(
		    'uid_location' => $location_uid,
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'cp_type' => $cp_type,
		    'cp_hotel_plan_type' => $cp_hotel_plan_type,
		    'entrprise_usertype' => $enterprise_user_type,
		    'created_on' => date('Y-m-d H:i:s'),
		    'original_image' => $original_image,
		    'small_image' => $small_image,
		    'logo_image' => $logo_image,
		    'main_ssid' => $main_ssid,
		    'guest_ssid' => $guest_ssid,
		    'cafe_plan' => $cp_cafe_plan_id,
		    'cafe_plantype' => $cafe_plan_type,
		    'is_otpdisabled' => $is_otpdisabled,
		    'retail_original_image' => $retail_original_image,
		    'retail_small_image' => $retail_small_image,
		    'retail_logo_image' => $retail_logo_image,
		    'cp1_no_of_daily_session' => $num_of_session_per_day,
		    'colour_code' => $icon_color,
		    'is_sociallogin' => $is_sociallogin,
		    'is_enablepin' => $is_pinenable,
		    'is_wifidisabled' => $is_wifidisabled,
		    'is_loginemail' => $login_with_mobile_email,
		    'is_sendvcemail' => $offer_redemption_mode,
		    'custom_url' => $custom_captive_url,
		    'custom_planid' => $custom_location_plan,
		    'custom_signup_data' => $custon_cp_signip_data,
		    'custom_daily_data' => $custon_cp_daily_data,
		    'custom_daily_no_of_session' => $custon_cp_no_of_daily_session,
		    'custom_slider_image1' => $original_slider1,
		    'custom_slider_image2' => $original_slider2,
		    'frequency_hour' => $synk_frequency,
		    'offline_cp_content_type' => $offline_cp_type,
		    'offline_cp_source_folder_path' => $offline_cp_source_folder_path,
		    'offline_cp_landing_page_path' => $offline_cp_landing_page_path,
		    'offline_version' => $offline_build_version,
		    'apikey' => $api_key,
		    'is_offline_registration' => $is_offline_registration,
		    'plan_type_time_hybrid_data' => $cp_cafe_retail_plan_type,
		    'offline_content_title' => $contestifi_contest_header,
		    'is_white_theme' => $is_white_theme,
		    'is_preaudit_disable' => $is_preaudit_disable,
		    'audit_ticket_email_send' => $audit_ticket_email_send,
		    'tatdays' => $tatdays
	       );
	       $insert = $this->DB2->insert('wifi_location_cptype', $insert_data);
	       $created_cp_id = $this->DB2->insert_id();
	  }
	  return $created_cp_id;
     }
     
     
     


     

     
     
     




     public function import_excel_sku_pin_image_etc($created_cp_id,$reference_location_uid, $location_uid, $location_id, $isp_uid)
     {
	  // update image
	  $this->DB2->select('*')->from('wifi_location_cptype');
	  $this->DB2->where(array("uid_location" => $reference_location_uid));
	  $get = $this->DB2->get();
	  if($get->num_rows() > 0)
	  {
	       foreach($get->result() as $row)
	       {
		    $update_data = array(
			 'retail_original_image' => $row->retail_original_image,
			 'retail_small_image' => $row->retail_small_image,
			 'retail_logo_image' => $row->retail_logo_image,
			 'original_image' => $row->original_image,
			 'small_image' => $row->small_image,
			 'logo_image' => $row->logo_image,
			 
		    );
		    $this->DB2->where('id', $created_cp_id);
		    $query = $this->DB2->update('wifi_location_cptype', $update_data);
		    break;
	       }
	  }
	  // add sku
	  $get_sku = $this->DB2->query("select sku from retailaudit_sku where location_uid = '$reference_location_uid'  AND is_deleted = '0'");
	  foreach($get_sku->result() as $row1)
	  {
	       $created_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $location_id,
		    'location_uid' => $location_uid,
		    'sku' => $row1->sku,
		    'created_on' => date('Y-m-d H:i:s'),
			 
	       );
	       $insert = $this->DB2->insert('retailaudit_sku', $created_data);
	       $sku_id = $this->DB2->insert_id();
	  }
	  // add store pin and promoter pin
	  $get_pin = $this->DB2->query("select * from retailaudit_login_pin where location_uid = '$reference_location_uid'");
	  foreach($get_pin->result() as $row2)
	  {
	       $created_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $location_id,
		    'location_uid' => $location_uid,
		    'pin' => $row2->pin,
		    'promoter_pin' => $row2->promoter_pin,
		    'created_on' => date('Y-m-d H:i:s'),
			 
	       );
	       $insert = $this->DB2->insert('retailaudit_login_pin', $created_data);
	       $pin_id = $this->DB2->insert_id();
	  }
     }
     
     public function import_excel_main_category_and_question($locationid,$isp_uid,$location_uid,$reference_location_uid)
     {
	  $icon = '';$file_previous_name = '';
	  $this->DB2->select('*')->from('offline_main_category');
	  $this->DB2->where(array("location_uid" => $reference_location_uid, "is_deleted" => '0'));
	  $get_main_category = $this->DB2->get();
	  foreach($get_main_category->result() as $row)
	  {
	       // addd main category
	       $insert_data = array(
		    'isp_uid' => $isp_uid,
		    'location_id' => $locationid,
		    'location_uid' => $location_uid,
		    'category_name' => $row->category_name,
		    'category_desc' => $row->category_desc,
		    'created_on' => date('Y-m-d H:i:s'),
		    'icon' => $row->icon,
	       );
	       $create = $this->DB2->insert('offline_main_category', $insert_data);
	       $new_created_id = $this->DB2->insert_id();
	       
	       $this->DB2->select('*')->from('retailaudit_is_promoter_category');
	       $this->DB2->where(array("location_uid" => $reference_location_uid, "is_deleted" => '0', "category_id"=> $row->id));
	       $get_promoter = $this->DB2->get();
	       foreach($get_promoter->result() as $row1)
	       {
		    // add is promoter category
		    $insert_data = array(
			      'category_id' => $new_created_id,
			      'created_on' => date('Y-m-d H:i:s'),
			      'is_deleted' => '0',
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'is_promoter' => $row1->is_promoter,
			      'compliance_id' => $row1->compliance_id,
		    );
		    $this->DB2->insert('retailaudit_is_promoter_category', $insert_data);
	       }
	       // add content
	       
	       
	       $this->DB2->select('*')->from('offline_content');
	       $this->DB2->where(array("location_uid" => $reference_location_uid, "is_deleted" => '0', "fid"=> $row->id));
	       $get_content = $this->DB2->get();
	       foreach($get_content->result() as $row2)
	       {
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $locationid,
			 'location_uid' => $location_uid,
			 'fid' => $new_created_id,
			 'is_main_category' => $row2->is_main_category,
			 'content_title' => $row2->content_title,
			 'content_desc' => $row2->content_desc,
			 'content_file' => $row2->content_file,
			 'created_on' => date('Y-m-d H:i:s'),
			 'content_type' => $row2->content_type,
			 'is_synced' => $row2->is_synced,
			 'upload_type' => $row2->upload_type,
			 'wikipedia_title_url' => $row2->wikipedia_title_url,
			 'wikipedia_title' => $row2->wikipedia_title,
			 'wikipedia_pageid' => $row2->wikipedia_pageid,
			 'wikipedia_content' => $row2->wikipedia_content,
		    );
		    $create = $this->DB2->insert('offline_content', $insert_data);
		    $create_content_id = $this->DB2->insert_id();
		    
		    // add survey
		    
		    $this->DB2->select('*')->from('retailaudit_survey_question');
		    $this->DB2->where(array("location_uid" => $reference_location_uid, "is_deleted" => '0', "content_id"=> $row2->id));
		    $get_question = $this->DB2->get();
		    foreach($get_question->result() as $row3)
		    {
			 $insert_data = array(
			      'isp_uid' => $isp_uid,
			      'location_id' => $locationid,
			      'location_uid' => $location_uid,
			      'content_id' => $create_content_id,
			      'question' => $row3->question,
			      'survey_type' => $row3->survey_type,
			      'created_on' => date('Y-m-d H:i:s'),
			      'health_deduction' => $row3->health_deduction,
			      'is_use_sku' => $row3->is_use_sku,
			 );
			 $insert_question = $this->DB2->insert('retailaudit_survey_question', $insert_data);
			 $question_id = $this->DB2->insert_id();
		    }
	       }
	       
	  }
	  
	  $this->DB2->select('offline_content_title, offline_content_desc')->from('wifi_location_cptype');
	  $this->DB2->where(array("uid_location" => $reference_location_uid));
	  $get_detail = $this->DB2->get();
	  foreach($get_detail->result() as $row2)
	  {
	       $update_data = array(
		    'offline_content_title' => $row2->offline_content_title,
		    'offline_content_desc' => $row2->offline_content_desc,
	       
	       );
	       $this->DB2->where('location_id', $locationid);
	       $update = $this->DB2->update('wifi_location_cptype', $update_data);
	  }
	  
     }
     public function update_store_unique_id(){
	  $this->load->library('Excel');
	  $isp_uid = '103';
	  $file = $_SERVER['DOCUMENT_ROOT'].'/isp_consumer_api/assets/assets_location_list.xlsx';
	  //echo $file;die;
	  if(file_exists($file))
	  {
	       $objPHPExcel = PHPExcel_IOFactory::load($file);
	       //get only the Cell Collection
	       $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
	       foreach ($cell_collection as $cell)
	       {
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		    if ($row == 1)
		    {
			 $header[$row][$column] = $data_value;
		    }
		    else
		    {
			 $arr_data[$row][$column] = $data_value;
		    }
	       }
		
	       //send the data in an array format
	       $data['header'] = $header;
	       $data['values'] = $arr_data;
	       //echo '<pre>'; print_r($data); echo '</pre>'; die;
	       $this->update_store_unique_id_add($arr_data);
	  }
	  else
	  {
	       echo "No such excel is associated with this user.";
	  }
     }
     public function update_store_unique_id_add($arr_data)
     {
	  foreach($arr_data as $arr_data1)
	  {
	       $isp_uid = '103';//check
	       
	       
	      $location_uid = $this->DB2->escape_str($arr_data1['B']);
	      $unique_id = $this->DB2->escape_str($arr_data1['D']);
	       $update_data = array(
		    'store_unique_id' => $unique_id,
	       
	       );
	       $this->DB2->where('location_uid', $location_uid);
	       $update = $this->DB2->update('wifi_location', $update_data);
	  }
     }
      public function update_sku_through_excel(){
	  $this->load->library('Excel');
	  $isp_uid = '103';
	  $file = $_SERVER['DOCUMENT_ROOT'].'/isp_consumer_api/assets/sku.xlsx';
	  //echo $file;die;
	  if(file_exists($file))
	  {
	       $objPHPExcel = PHPExcel_IOFactory::load($file);
	       //get only the Cell Collection
	       $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
	       foreach ($cell_collection as $cell)
	       {
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		    if ($row == 1)
		    {
			 $header[$row][$column] = $data_value;
		    }
		    else
		    {
			 $arr_data[$row][$column] = $data_value;
		    }
	       }
		
	       //send the data in an array format
	       $data['header'] = $header;
	       $data['values'] = $arr_data;
	       //echo '<pre>'; print_r($data); echo '</pre>'; die;
	       $this->update_sku_through_excel_add($arr_data);
	  }
	  else
	  {
	       echo "No such excel is associated with this user.";
	  }
     }
     public function update_sku_through_excel_add($arr_data)
     {
	  //echo "<pre>";print_r($arr_data);die;
	  foreach($arr_data as $arr_data1)
	  {
	       $isp_uid = '103';//check
	       
	       
	       $location_uid = $this->DB2->escape_str($arr_data1['C']);
	       $sku = $this->DB2->escape_str($arr_data1['E']);
	       // delete old sku
	       if($sku != '')
	       {
		    /*$update_data = array(
			 'is_deleted' => '1',
			 'updated_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->where('location_uid', $location_uid);
		    $update = $this->DB2->update('retailaudit_sku', $update_data);
		    */
		    // insert new sku
		    $location_id = '0';
		    $get_loc_id = $this->DB2->query("select id from wifi_location where location_uid = '$location_uid'");
		    if($get_loc_id->num_rows() > 0)
		    {
			 $row = $get_loc_id->row_array();
			 $location_id = $row['id'];
		    }
		    $insert_data = array(
			 'isp_uid' => $isp_uid,
			 'location_id' => $location_id,
			 'location_uid' => $location_uid,
			 'sku' => $sku,
			 'created_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->insert('retailaudit_sku', $insert_data);
	       }
	       
	  }
     }
     public function retail_audit_log_record($slug, $loc_id)
     {
	  $insert_data = array(
	       'slug' => $slug,
	       'loc_id' => $loc_id,
	       'added_on' => date('Y-m-d H:i:s'),
	  );
	  $insert = $this->DB2->insert('retailaudit_log_info', $insert_data);
     }
     
     public function lms_vm_offline_department_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  
	  $data = array();
	  $query = $this->DB2->query("select ovd.id as department_id, ovd.department_name from offline_vm_department as ovd  where ovd.location_uid = '$location_uid'  AND ovd.is_deleted = '0'  order by ovd.department_name asc");
	  $i = 0;
	  if($query->num_rows() > 0){
	       $department = array();
	       foreach($query->result() as $row){
		    $department[$i]['department_id'] = $row->department_id;
		    $department[$i]['department_name'] = $row->department_name;
		    $i++;
	       }
	       $data['resultCode'] = 1;
	       $data['department'] = $department;
	  }
	  else{
	      $data['resultCode'] = 0; 
	  }
	  
	  return json_encode($data);
     }
     public function lms_vm_offline_employee_list($jsondata){
	  $location_uid = $jsondata->location_uid;
	  
	    $cond2="";
	  if(isset($jsondata->deptid) && $jsondata->deptid!='null'){
		  $cond2="and ove.dept_id='".$jsondata->deptid."'";
	  }
	  $data = array();
	  $query = $this->DB2->query("select ove.employee_id as employee_unique_id, ove.employee_last_name, ove.dept_id,ove.employee_name, ove.id as employee_id, ove.employee_email, ove.employee_mobile, ovd.department_name from offline_vm_employee as ove inner join offline_vm_department as ovd on (ove.dept_id = ovd.id)  where ove.location_uid = '$location_uid'  {$cond2} AND ove.is_deleted = '0' AND ovd.is_deleted = '0'");
	  $i = 0;
	  if($query->num_rows() > 0){
	       $employee = array();
	       foreach($query->result() as $row){
		    $employee[$i]['employee_unique_id'] = $row->employee_unique_id;
		    $employee[$i]['employee_last_name'] = $row->employee_last_name;
		    $employee[$i]['employee_id'] = $row->employee_id;
		    $employee[$i]['employee_name'] = $row->employee_name;
		    $employee[$i]['employee_email'] = $row->employee_email;
		    $employee[$i]['employee_mobile'] = $row->employee_mobile;
		    $employee[$i]['department_name'] = $row->department_name;
		    $employee[$i]['department_id'] = $row->dept_id;
		    $i++;
	       }
	       $data['resultCode'] = 1;
	       $data['employee'] = $employee;
	  }
	  else{
	      $data['resultCode'] = 0; 
	  }
	  
	  return json_encode($data);
     }
     
     
     public function lms_content_builder_list($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	
	  $main_category = array();
	  // get main category list
	  $main_cat = $this->DB2->query("select omc.* from offline_main_category as omc  where omc.isp_uid = '$isp_uid' AND omc.is_lms = '1' and omc.is_deleted = '0' order by omc.id");
	  $i = 0;
	  foreach($main_cat->result() as $row){
	       $sub_category = array();
	       
	       $main_category_id = $row->id;
	       $main_category[$i]['id'] = $row->id;
	       $main_category[$i]['category_name'] = $row->category_name;
	       $main_category[$i]['category_desc'] = $row->category_desc;
	       $main_category[$i]['icon'] = $row->icon;
	       $main_category[$i]['compliance_mandatory'] = $row->compliance_mandatory;
	       //get sub category
	       $sub_query = $this->DB2->query("select id,sub_category_name, sub_category_desc,icon from offline_sub_category where mid = '$main_category_id' and is_deleted = '0'");
	       $j = 0;
	       foreach($sub_query->result() as $row_sub){
		    $sub_category_id = $row_sub->id;
		    $sub_category[$j]['id'] = $row_sub->id;
		    $sub_category[$j]['sub_category_name'] = $row_sub->sub_category_name;
		    $sub_category[$j]['sub_category_desc'] = $row_sub->sub_category_desc;
		    $sub_category[$j]['icon'] = $row_sub->icon;
		    $sub_category[$j]['is_this_is_content'] = '0';
		    $k = 0;
		    $content_array = array();
		    // get content which is attatched with subcategory
		    $content_query = $this->DB2->query("select upload_type,is_synced,id,content_title, content_desc,content_file,content_type,wikipedia_title_url from offline_content where fid = '$sub_category_id' and is_main_category = '0' and is_deleted = '0'");
		    foreach($content_query->result() as $row_content){
			 $content_array[$k]['id'] = $row_content->id;
			 $content_array[$k]['content_title'] = $row_content->content_title;
			 $content_array[$k]['content_desc'] = $row_content->content_desc;
			 $content_array[$k]['content_file'] = $row_content->content_file;
			 $content_array[$k]['content_type'] = $row_content->content_type;
			 $content_array[$k]['is_synced'] = $row_content->is_synced;
			 $content_array[$k]['upload_type'] = $row_content->upload_type;
			 $content_array[$k]['wikipedia_title_url'] = $row_content->wikipedia_title_url;
			 $k++;
		    }
		    $sub_category[$j]['content'] = $content_array;
		    $j++;
	       }
	       // get content which is directly attatched with  main category
	       $sub_content_query = $this->DB2->query("select upload_type,is_synced,id,content_title, content_desc,content_file,content_type,wikipedia_title_url from offline_content where fid = '$main_category_id' and is_main_category = '1' and is_deleted = '0'");
	       foreach($sub_content_query->result() as $row_content_sub){
		    $sub_category[$j]['id'] = $row_content_sub->id;
		    $sub_category[$j]['content_title'] = $row_content_sub->content_title;
		    $sub_category[$j]['content_desc'] = $row_content_sub->content_desc;
		    $sub_category[$j]['content_file'] = $row_content_sub->content_file;
		    $sub_category[$j]['content_type'] = $row_content_sub->content_type;
		    $sub_category[$j]['is_this_is_content'] = '1';
		    $sub_category[$j]['is_synced'] = $row_content_sub->is_synced;
		    $sub_category[$j]['upload_type'] = $row_content_sub->upload_type;
		    $sub_category[$j]['wikipedia_title_url'] = $row_content_sub->wikipedia_title_url;
		    $j++;
	       }
	       $main_category[$i]['sub_category'] = $sub_category;
	       $i++;
	  }
	  $data['main_category'] =  $main_category;
	  //echo "<pre>";print_r($data);die;
	  
	  return json_encode($data);
     }
     public function lms_contestifi_content_builder_list($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  
	  $contest_array = array();
	  // get contest list
	  $contest = $this->DB2->query("select id, contest_name, contest_type from offline_contestifi_contest where isp_uid = '$isp_uid' and is_lms = '1' and is_deleted = '0'");
	  $i = 0;
	  foreach($contest->result() as $row){
	       $quiz_array = array();
	       $contest_id = $row->id;
	       $contest_array[$i]['id'] = $row->id;
	       $contest_array[$i]['contest_name'] = $row->contest_name;
	       $contest_array[$i]['contest_type'] = $row->contest_type;
	       //get quiz list
	       $quiz_query = $this->DB2->query("select id,question from offline_contestifi_quiz_questions where contest_id = '$contest_id' and is_deleted = '0'");
	        $j = 0;
	       foreach($quiz_query->result() as $row_sub){
		    $question_id = $row_sub->id;
		    $quiz_array[$j]['id'] = $row_sub->id;
		    $quiz_array[$j]['question'] = $row_sub->question;
		    $j++;
	       }
	       $contest_array[$i]['quiz_questions'] = $quiz_array;
	       
	       
	       // get prize list
	       $prize_array = array();
	       $prize_query = $this->DB2->query("select id,contest_prize from offline_contestifi_contest_prize where contest_id = '$contest_id' and is_deleted = '0'");
	       $k = 0;
	       foreach($prize_query->result() as $row_prize){
		    $prize_array[$k]['prize_id'] = $row_prize->id;
		    $prize_array[$k]['contest_prize'] = $row_prize->contest_prize;
		    $k++;
	       }
	       $contest_array[$i]['contest_prize'] = $prize_array;
	       
	       $i++;
	  }
		
	  $data['contest'] =  $contest_array;
	  return json_encode($data);
     }
     public function lms_get_content_location($jsondata){
	  $isp_uid = $jsondata->isp_uid;
	  $content_id = $jsondata->content_id;
	  $is_contest = $jsondata->is_contest;
	  // get exist location_id
	  $exist_location = array();
	  $get_loc = $this->DB2->query("select location_id from offline_lms_content_contest_location where is_deleted = '0' AND content_id = '$content_id' AND is_contest = '$is_contest'");
	  $j = 0;
	  foreach($get_loc->result() as $loc_row)
	  {
	       $exist_location[$j] = $loc_row->location_id;
	       $j++;
	  }
	  $data = array();
	  $get_group = $this->DB2->query("select id, location_name from wifi_location where isp_uid = '$isp_uid' and location_type = '16' and is_deleted = '0'");
	  if($get_group->num_rows() > 0){
	       $i = 0;
	       foreach($get_group->result() as $row){
		    $is_check = 0;
		    if(in_array($row->id,$exist_location)){
			 $is_check = 1;
		    }
		    $data[$i]['id'] = $row->id;
		    $data[$i]['is_check'] = $is_check;
		    $data[$i]['location_name'] = $row->location_name;
		     $i++;
	       }
	  }
	  return $data;
        
     }
     
     public function lms_add_content_location($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $content_id = $jsondata->content_id;
	  $is_contest = $jsondata->is_contest;
	  $previous_location_id = array();
	  $this->DB2->select("id, location_id")->from('offline_lms_content_contest_location');
	  $this->DB2->where(array('content_id' => $content_id, 'is_deleted' => '0'));
	  $get = $this->DB2->get();
	  foreach($get->result() as $row_old_ques)
	  {
	      $previous_location_id[] = $row_old_ques->location_id;
	  }
	  if(count($jsondata->locations) > 0){
	       foreach($jsondata->locations as $locuid){
		    if(in_array($locuid, $previous_location_id))
		    {// update old
			$pos = array_search($locuid, $previous_location_id);
			unset($previous_location_id[$pos]);
			$previous_location_id = array_values($previous_location_id);
		    }
		    else
		    {
			 // insert new
			 $insert_data = array(
			      'content_id' => $content_id,
			      'location_id' => $locuid,
			      'is_contest' => $is_contest,
			      'created_on' => date('Y-m-d H:i:s')
			 );
			 $this->DB2->insert('offline_lms_content_contest_location', $insert_data);
		    }
		    
	       }
	  }
	  // remove deleted store
	  foreach($previous_location_id as $previous_location_id1)
	  {
	       // update
	       $update_data = array(
		  'is_deleted' => '1',
		  'updated_on' => date('Y-m-d H:i:s'),
	       );
	       $this->DB2->where(array('content_id'=>$content_id, 'location_id' => $previous_location_id1, 'is_deleted' => 0));
	       $update = $this->DB2->update('offline_lms_content_contest_location', $update_data);
	  }
	  $data['resultCode'] = 1;
	  $data['resultMsg'] = "Success";
	  
	  
	  return json_encode($data);
     }
     public function treya_user_simultanes()
     {
	  $i = 1;
	  $get = $this->DB2->query("select mobile from club_user_detail");
	  foreach($get->result() as $row)
	  {
	       $user_mobile = $row->mobile;
	       echo $i."-----".$user_mobile."<br />";
	       $this->DB2->query("update radcheck set value = '5' where username = '$user_mobile' AND attribute = 'Simultaneous-Use'");
	       $i++;
	  }
     }
     
     
     public function foodle_loyalty_reward_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $loyalty_reward = array();
	  $this->DB2->select('*')->from('foodle_loyalty_reward')->where('is_deleted', 0);
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       foreach($query->result() as $row){
		   $loyalty_reward[$i]['id'] = $row->id;
		   $loyalty_reward[$i]['reward_name'] = $row->reward_name;
		   $i++;
	       }
	       $data['loyalty_reward']= $loyalty_reward;
	  }
	  else
	  {
            $data['resultCode'] = '0';
	  }
	  return $data;
     }
     
     public function foodle_location_loyalty_reward_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $loyalty_reward = array();
	  $this->DB2->select('fllr.*, flr.reward_image')->from('foodle_location_loyalty_reward as fllr');
	  $this->DB2->where(array("fllr.is_deleted" => "0", "fllr.loc_uid" => $location_uid));
	  $this->DB2->join('foodle_loyalty_reward as flr', 'fllr.reward_id = flr.id', 'inner');
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       foreach($query->result() as $row){
		   $loyalty_reward[$i]['id'] = $row->id;
		   $loyalty_reward[$i]['reward_name'] = $row->reward_name;
		   $loyalty_reward[$i]['reward_image'] = CDNPATH."foodle_rewards_icon/".$row->reward_image;
		   $i++;
	       }
	       $data['loyalty_reward']= $loyalty_reward;
	  }
	  else
	  {
            $data['resultCode'] = '0';
	  }
	  return $data;
     }
     
     public function foodle_add_loyalty_rewards($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  $locationid = $jsondata->locationid;
	  $loyalty_reward = $jsondata->loyalty_reward;
	  $loyalty_reward_name = $jsondata->loyalty_reward_name;
	  $location_loyalty_reward = $jsondata->location_loyalty_reward;
	  if($location_loyalty_reward > 0){
	       $data['resultCode'] = 1;
	       $data['resultMsg'] = 'Success';
	       $location_update_data = array(
		    'reward_name' => $loyalty_reward_name,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $location_loyalty_reward);
	       $this->DB2->update('foodle_location_loyalty_reward', $location_update_data);
	  }
	  else{
	       // check already uplade
	       $this->DB2->select('id')->from('foodle_location_loyalty_reward');
	       $this->DB2->where(array('is_deleted' => '0', 'reward_id' => $loyalty_reward, 'loc_uid' => $location_uid));
	       $check = $this->DB2->get();
	       //echo $this->DB2->last_query();die;
	       if($check->num_rows() > 0){
		    $data['resultCode'] = 0;
		    $data['resultMsg'] = 'Already added';
	       }
	       else{
		    $insert_data = array(
			 'reward_id' => $loyalty_reward,
			 'reward_name' => $loyalty_reward_name,
			 'loc_id' => $locationid,
			 'loc_uid' => $location_uid,
			 'is_deleted' => '0',
			 'added_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->insert('foodle_location_loyalty_reward', $insert_data);
		    $data['resultCode'] = 1;
		    $data['resultMsg'] = 'Success';
	       }
	  }
	  
	  return json_encode($data);
     }


    


     public function location_loyalty_reward_delete($jsondata){
	  $location_reward_id = $jsondata->location_reward_id;
	  $data = array();
	  $location_update_data = array(
		    'is_deleted' => 1,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $location_reward_id);
	       $this->DB2->update('foodle_location_loyalty_reward', $location_update_data);
	  $data['resultCode'] = '1';
	     
	  return $data;
     }
     public function foodle_add_loyalty_slab($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  $locationid = $jsondata->locationid;
	  $slab_loyalty_reward_amount = $jsondata->slab_loyalty_reward_amount;
	  $slab_location_loyalty_reward = $jsondata->slab_location_loyalty_reward;
	  $foodle_slab_id = $jsondata->foodle_slab_id;
	  if($foodle_slab_id > 0){
	       $data['resultCode'] = 1;
	       $data['resultMsg'] = 'Success';
	       $location_update_data = array(
		    'slab_amt' => $slab_loyalty_reward_amount,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $foodle_slab_id);
	       $this->DB2->update('foodle_location_loyalty_slab', $location_update_data);
	  }
	  else{
	       // check already uplade
	       $this->DB2->select('flls.id')->from('foodle_location_loyalty_slab flls');
	       $this->DB2->where(array('flls.is_deleted' => '0','flls.loc_uid' => $location_uid, 'flsi.is_deleted' => 0, 'flsi.foodle_rewardid' => $slab_location_loyalty_reward));
	       $this->DB2->join('foodle_location_slab_item as flsi', 'flls.id = flsi.slab_id ', 'inner');
	       $check = $this->DB2->get();
	       if($check->num_rows() > 0){
		    $data['resultCode'] = 0;
		    $data['resultMsg'] = 'Already added';
	       }
	       else{
		    $data['resultCode'] = 1;
		    $data['resultMsg'] = 'Success';
		    $insert_data = array(
			 'slab_amt' => $slab_loyalty_reward_amount,
			 'loc_id' => $locationid,
			 'loc_uid' => $location_uid,
			 'is_deleted' => '0',
			 'added_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->insert('foodle_location_loyalty_slab', $insert_data);
		    $slab_id = $this->DB2->insert_id();
		    
		    $insert_data = array(
			 'slab_id' => $slab_id,
			 'foodle_rewardid' => $slab_location_loyalty_reward,
			 'qty' => 1,
			 'is_deleted' => '0',
			 'added_on' => date('Y-m-d H:i:s')
		    );
		    $this->DB2->insert('foodle_location_slab_item', $insert_data);
	       }
	  }
	  
	  return json_encode($data);
     }
     public function foodle_location_loyalty_slab_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $loyalty_reward = array();
	  $this->DB2->select('flls.id, flls.slab_amt, fllr.reward_name')->from('foodle_location_loyalty_slab as flls');
	  $this->DB2->where(array("flls.is_deleted" => "0", "flls.loc_uid" => $location_uid, 'flsi.is_deleted' => 0, 'fllr.is_deleted' => 0));
	  $this->DB2->join('foodle_location_slab_item as flsi', 'flls.id = flsi.slab_id', 'inner');
	  $this->DB2->join('foodle_location_loyalty_reward as fllr', 'flsi.foodle_rewardid = fllr.id', 'inner');
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       foreach($query->result() as $row){
		   $loyalty_reward[$i]['slab_id'] = $row->id;
		   $loyalty_reward[$i]['slab_amount'] = $row->slab_amt;
		   $loyalty_reward[$i]['reward_name'] = $row->reward_name;
		   $i++;
	       }
	       $data['loyalty_reward']= $loyalty_reward;
	  }
	  else
	  {
            $data['resultCode'] = '0';
	  }
	  return $data;
     }
     public function location_loyalty_slab_delete($jsondata){
	  $location_reward_id = $jsondata->location_reward_id;
	  $data = array();
	  $location_update_data = array(
		    'is_deleted' => 1,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $location_reward_id);
	       $this->DB2->update('foodle_location_loyalty_slab', $location_update_data);
	  $data['resultCode'] = '1';
	     
	  return $data;
     }
     public function foodle_location_loyalty_reward_audience_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $loyalty_reward = array();
	  $this->DB2->select('*')->from('foodle_location_loyalty_reward_audience');
	  $this->DB2->where(array("is_deleted" => "0", 'loc_uid' => $location_uid));
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       foreach($query->result() as $row){
		    $loyalty_reward[$i]['id'] = $row->id;
		    $loyalty_reward[$i]['audience_name'] = $row->audience_name;
		    $loyalty_reward[$i]['audiance_duration'] = $row->audiance_duration;
		    $loyalty_reward[$i]['visit_from_condition'] = $row->visit_from_condition;
		    $loyalty_reward[$i]['visit_from_value'] = $row->visit_from_value;
		    $loyalty_reward[$i]['visit_to_conditon'] = $row->visit_to_conditon;
		    $loyalty_reward[$i]['visit_to_value'] = $row->visit_to_value;
		    $loyalty_reward[$i]['visit_spend_between_condition'] = $row->visit_spend_between_condition;
		    $loyalty_reward[$i]['spend_from_condition'] = $row->spend_from_condition;
		    $loyalty_reward[$i]['spend_from_value'] = $row->spend_from_value;
		    $loyalty_reward[$i]['spend_to_condition'] = $row->spend_to_condition;
		    $loyalty_reward[$i]['spend_to_value'] = $row->spend_to_value;
		    $rules = '';
		    $visit_rule = '';
		    $spend_rule = '';
		    if($row->visit_from_condition > 0){
			 $visit_rule .= "Visits";
			 if($row->visit_from_condition == '1'){
			      $visit_rule .= " >= ".$row->visit_from_value;
			 }
			 else if($row->visit_from_condition == '2'){
			      $visit_rule .= " <= ".$row->visit_from_value;
			 }
			 
			 if($row->visit_to_conditon > 0){
			      if($row->visit_to_conditon == '1'){
				   $visit_rule .= " & >= ".$row->visit_to_value;
			      }
			      else if($row->visit_to_conditon == '2'){
				   $visit_rule .= " & <= ".$row->visit_to_value;
			      }
			 }
		    }
		    if($row->spend_from_condition > 0){
			 $spend_rule .= "Spends";
			 if($row->spend_from_condition == '1'){
			      $spend_rule .= " >= ".$row->spend_from_value;
			 }
			 else if($row->spend_from_condition == '2'){
			      $spend_rule .= " <= ".$row->spend_from_value;
			 }
			 
			 if($row->spend_to_condition > 0){
			      if($row->visit_to_conditon == '1'){
				   $spend_rule .= " & >= ".$row->spend_to_value;
			      }
			      else if($row->spend_to_condition == '2'){
				   $spend_rule .= " & <= ".$row->spend_to_value;
			      }
			 }
		    }
		    $spend_visit_rule = '';
		    if($visit_rule != '' && $spend_rule != ''){
			 $between_conditon = '';
			 if($row->visit_spend_between_condition == 1){
			      $between_conditon = ' AND ';
			 }
			 else{
			      $between_conditon = ' OR ';
			 }
			 $spend_visit_rule = $visit_rule.$between_conditon.$spend_rule;
		    }
		    else if($visit_rule != ''){
			 $spend_visit_rule = $visit_rule;
		    }
		    else if($spend_rule != ''){
			 $spend_visit_rule = $spend_rule;
		    }
		    if($row->audiance_duration == 0){
			 $rules .= 'Lifetime '.$spend_visit_rule;
		    }
		    else if($row->audiance_duration == 1){
			 $rules .= $spend_visit_rule.'/ Month';
		    }
		    else{
			$rules .= $spend_visit_rule.'/ '.$row->audiance_duration.' Month'; 
		    }
		    
		    
		    
		    $loyalty_reward[$i]['rules'] = $rules;
		    $i++;
	       }
	       $data['loyalty_reward']= $loyalty_reward;
	  }
	  else
	  {
            $data['resultCode'] = '0';
	  }
	  return $data;
     }
     public function foodle_add_loyalty_audience($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  $locationid = $jsondata->locationid;
	  $foodle_audience_name = $jsondata->foodle_audience_name;
	  $foodle_audience_time_duration = $jsondata->foodle_audience_time_duration;
	  
	  $foodle_audience_visit_from_conditon = $jsondata->foodle_audience_visit_from_conditon;
	  $foodle_audience_visit_from_value = $jsondata->foodle_audience_visit_from_value;
	  $foodle_audience_visit_to_conditon = $jsondata->foodle_audience_visit_to_conditon;
	  $foodle_audience_visit_to_value = $jsondata->foodle_audience_visit_to_value;
	  $foodle_audience_spend_from_conditon = $jsondata->foodle_audience_spend_from_conditon;
	  $foodle_audience_spend_from_value = $jsondata->foodle_audience_spend_from_value;
	  $foodle_audience_spend_to_conditon = $jsondata->foodle_audience_spend_to_conditon;
	  $foodle_audience_spend_to_value = $jsondata->foodle_audience_spend_to_value;
	  $foodle_audience_visit_spend_conditon = $jsondata->foodle_audience_visit_spend_conditon;
	  
	  $audience_id = $jsondata->audience_id;
	  if($audience_id > 0){
	       $data['resultCode'] = 1;
	       $data['resultMsg'] = 'Success';
	       $location_update_data = array(
		    'audience_name' => $foodle_audience_name,
		    'audiance_duration' => $foodle_audience_time_duration,
		    'visit_from_condition' => $foodle_audience_visit_from_conditon,
		    'visit_from_value' => $foodle_audience_visit_from_value,
		    'visit_to_conditon' => $foodle_audience_visit_to_conditon,
		    'visit_to_value' => $foodle_audience_visit_to_value,
		    'visit_spend_between_condition' => $foodle_audience_visit_spend_conditon,
		    'spend_from_condition' => $foodle_audience_spend_from_conditon,
		    'spend_from_value' => $foodle_audience_spend_from_value,
		    'spend_to_condition' => $foodle_audience_spend_to_conditon,
		    'spend_to_value' => $foodle_audience_spend_to_value,
		    'loc_id' => $locationid,
		    'loc_uid' => $location_uid,
		    'is_deleted' => '0',
		    'added_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $audience_id);
	       $this->DB2->update('foodle_location_loyalty_reward_audience', $location_update_data);
	  }
	  else{
	       $data['resultCode'] = 1;
	       $data['resultMsg'] = 'Success';
	       $insert_data = array(
		    'audience_name' => $foodle_audience_name,
		    'audiance_duration' => $foodle_audience_time_duration,
		    'visit_from_condition' => $foodle_audience_visit_from_conditon,
		    'visit_from_value' => $foodle_audience_visit_from_value,
		    'visit_to_conditon' => $foodle_audience_visit_to_conditon,
		    'visit_to_value' => $foodle_audience_visit_to_value,
		    'visit_spend_between_condition' => $foodle_audience_visit_spend_conditon,
		    'spend_from_condition' => $foodle_audience_spend_from_conditon,
		    'spend_from_value' => $foodle_audience_spend_from_value,
		    'spend_to_condition' => $foodle_audience_spend_to_conditon,
		    'spend_to_value' => $foodle_audience_spend_to_value,
		    'loc_id' => $locationid,
		    'loc_uid' => $location_uid,
		    'is_deleted' => '0',
		    'added_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('foodle_location_loyalty_reward_audience', $insert_data);
	       $slab_id = $this->DB2->insert_id();
	  }
	  
	  return json_encode($data);
     }
     public function foodle_campaign_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->location_uid;
	  $data = array();
	  $loyalty_reward = array();
	  $this->DB2->select('fc.id, fc.campaign_name, fc.start_date, fc.end_date, fc.redemption_expiryday, fllr.reward_name, fllra.audience_name')->from('foodle_campaign as fc');
	  $this->DB2->where(array("fc.is_deleted" => "0", 'fc.loc_uid' => $location_uid, 'fllr.is_deleted' => 0, 'fllra.is_deleted' => 0));
	  $this->DB2->join('foodle_location_loyalty_reward as fllr', 'fc.reward_id = fllr.id', 'inner');
	  $this->DB2->join('foodle_location_loyalty_reward_audience as fllra', 'fc.audience_id = fllra.id', 'inner');
	  $this->DB2->order_by("fc.id", 'desc');
	  $query = $this->DB2->get();
	  if($query->num_rows() > 0){
	       $data['resultCode'] = '1';
	       $i = 0;
	       foreach($query->result() as $row){
		    $loyalty_reward[$i]['campaign_id'] = $row->id;
		    $loyalty_reward[$i]['campaign_name'] = $row->campaign_name;
		    $loyalty_reward[$i]['reward_name'] = $row->reward_name;
		    $loyalty_reward[$i]['audience_name'] = $row->audience_name;
		    $loyalty_reward[$i]['start_date'] = date('d-m-Y', strtotime($row->start_date));
		    $loyalty_reward[$i]['end_date'] = date('d-m-Y', strtotime($row->end_date));
		    $loyalty_reward[$i]['redemption_expiryday'] = $row->redemption_expiryday;
		    $status = 'Active';
		    
		    if(strtotime(date('Y-m-d')) < strtotime(date('Y-m-d', strtotime($row->start_date)))){
			 $status = 'Will Be Active';
		    }
		    else{
			 if(strtotime(date('Y-m-d')) < strtotime(date('Y-m-d', strtotime($row->end_date)))){
			      $status = 'Expired';
			 }
		    }
		    $loyalty_reward[$i]['status'] = $status;
		    $i++;
	       }
	       $data['loyalty_reward']= $loyalty_reward;
	  }
	  else
	  {
            $data['resultCode'] = '0';
	  }
	  return $data;
     }
     public function foodle_add_campaign($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  $location_uid = $jsondata->location_uid;
	  $locationid = $jsondata->locationid;
	  $foodle_campaign_name = $jsondata->foodle_campaign_name;
	  $foodle_campaign_location_reward = $jsondata->foodle_campaign_location_reward;
	  $foodle_campaign_location_audience = $jsondata->foodle_campaign_location_audience;
	  $foodle_campaign_start_date = date('Y-m-d', strtotime($jsondata->foodle_campaign_start_date));
	  $foodle_campaign_end_date = date('Y-m-d', strtotime($jsondata->foodle_campaign_end_date));
	  $foodle_campaign_expire_day = $jsondata->foodle_campaign_expire_day;
	  $foodle_campaign_id = $jsondata->foodle_campaign_id;
	  if($foodle_campaign_id > 0){
	       $data['resultCode'] = 1;
	       $data['resultMsg'] = 'Success';
	       //update campaign here
	  }
	  else{
	       $insert_data = array(
		    'loc_id' => $locationid,
		    'loc_uid' => $location_uid,
		    'reward_id' => $foodle_campaign_location_reward,
		    'campaign_name' => $foodle_campaign_name,
		    'audience_id' => $foodle_campaign_location_audience,
		    'start_date' => $foodle_campaign_start_date,
		    'end_date' => $foodle_campaign_end_date,
		    'redemption_expiryday' => $foodle_campaign_expire_day,
		    'is_deleted' => 0,
		    'added_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->insert('foodle_campaign', $insert_data);
	  }
	  
	  return json_encode($data);
     }
     
     public function foodle_campaign_delete($jsondata){
	  $foodle_campaign_id = $jsondata->foodle_campaign_id;
	  $data = array();
	  $location_update_data = array(
		    'is_deleted' => 1,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $foodle_campaign_id);
	       $this->DB2->update('foodle_campaign', $location_update_data);
	  $data['resultCode'] = '1';
	     
	  return $data;
     }
     public function location_loyalty_audience_delete($jsondata){
	  $audience_id = $jsondata->audience_id;
	  $data = array();
	  $location_update_data = array(
		    'is_deleted' => 1,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->DB2->where('id', $audience_id);
	       $this->DB2->update('foodle_location_loyalty_reward_audience', $location_update_data);
	  $data['resultCode'] = '1';
	     
	  return $data;
     }
    


}



?>

