<?php

class Planpublicwifi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
         $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
    }
 
    
    public function misc_data($misctype) {
        $condarr = array('misc_type' => $misctype);
        $query = $this->db->get_where(SHTMISC, $condarr);
        $rowarr = $query->row_array();
        $misc_id = $rowarr['id'];
        $condarr1 = array('misc_id' => $misc_id);
        $query1 = $this->db->select('id,misc_name')->get_where(SHTMISCDETAIL, $condarr1);
        return $query1->result();
        // echo "<pre>"; print_R($query1->result());die;
        //$this->db->query("select id from ")
    }

    public function listing_plan($jsondata) {
        
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        $cond = $this->dept_plan_condition($jsondata);
     //   echo $cond;die;
        $dataarr = array();
        $data=array();
        if ($cond != '') {

            $cond = ($cond == "allindia") ? "" : $cond;
           

            $query = $this->DB2->query("SELECT ss.*,spp.net_total,spp.region_type,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.is_deleted='0' and ss.plantype!='0' {$cond} {$ispcond} order by srvid desc");
         //    echo $this->db->last_query(); die;
            $dataarr = $query->result();
        } else {
            $dataarr == array();
        }
         $data['listing_plan']=$dataarr;
         $arr=$this->get_ispdetail_info($jsondata);
        // echo "<pre>"; print_R($arr); die;
         $data['ispdetail']=(isset($arr['isp_detail']))?$arr['isp_detail']:array();
        $data['plan_count']=$this->get_plan_usercount($jsondata);
        $data['state_list']= $this->state_list('',$jsondata) ;
       
       return $data;
    }
    
    
       public function active_inactive_userslist($jsondata) {
        // echo "===--->>".$searchtype;
        $data = array();
        $gen = '';
        $total_search = 0;
        $cwhere = 'enableuser=1';

       // $sessiondata = $this->session->userdata('isp_session');
        $searchtype=$jsondata->searchtype;
        $superadmin = $jsondata->super_admin;
        $dept_id = $jsondata->dept_id;
        $regiontype = 'region';
         $isp_uid = $jsondata->isp_uid;
        $permicond = '';
        $sczcond = '';
        if ($regiontype == "region") {
            $dept_regionQ = $this->db->query("SELECT state_id,city_id,zone_id FROM sht_dept_region WHERE dept_id='" . $dept_id . "' AND status='1' AND is_deleted='0'");
            $total_deptregion = $dept_regionQ->num_rows();
            if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
                foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (state='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "') ";
                    } else {
                        $permicond .= " (state='" . $stateid . "' AND city='" . $cityid . "' AND zone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
            }
            if($permicond!='')
            {
            $sczcond .= ' AND (' . $permicond . ')';
            }
        }
        $search_user = $jsondata->search_user;
        //$searchtype = $this->input->post('searchtype');
        $state = $jsondata->state;
        $city = $jsondata->city;
        $zone = $jsondata->zone;
        $locality = $jsondata->locality;
        if (isset($state) && $state != '') {
            if ($state != 'all') {
                $cwhere .= " AND state='" . $state . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($city) && $city != '') {
            if ($city != 'all') {
                $cwhere .= " AND city='" . $city . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($zone) && $zone != '') {
            if ($zone != 'all') {
                $cwhere .= " AND zone='" . $zone . "'";
            } else {
                $cwhere .= '';
            }
        }
        if (isset($locality) && $locality != '') {
            $cwhere .= " AND usuage_locality='" . $locality . "'";
        }

        if (isset($search_user)) {
            $cwhere .= " AND ((username LIKE '%" . $search_user . "%') OR (firstname LIKE '%" . $search_user . "%') OR (mobile LIKE '%" . $search_user . "%')) ";
        }
        if (isset($searchtype) && $searchtype!='') {
            $cwhere .= " AND baseplanid='" . $searchtype . "'  and expired=0";
            // echo "-------------000000".$searchtype;
        }
        $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
          $cwhere .= $ispcond;
       $cwhere .= $sczcond;

        if ($cwhere != '') {
            // echo $cwhere;
            $this->db->where($cwhere);
        }
        $this->db->select('*');
        $this->db->from('sht_users');
        //

        $cquery = $this->db->get();
        //   echo $this->db->last_query();die;
        $total_csearch = $cquery->num_rows();
        if ($total_csearch > 0) {
            $i = 1;
            foreach ($cquery->result() as $sobj) {
                $ticket_count = $this->customer_tickets_count($sobj->id);
                $state = $this->getstatename($sobj->state);
                $city = $this->getcityname($sobj->state, $sobj->city);
                $zone = $this->getzonename($sobj->zone);
                $status = ($sobj->enableuser == 1) ? 'active' : 'inactive';
                $activeplan = $this->userassoc_planname($sobj->id);
                $active_from = date('d-m-Y', strtotime($sobj->account_activated_on));
                $renewal_date = date('d-m-Y', strtotime($sobj->paidtill_date));
                $expiration = $sobj->expiration;
                if ($expiration == '0000-00-00 00:00:00') {
                    $expiration = '-';
                } else {
                    $expiration = date('d-m-Y', strtotime($sobj->expiration));
                }

                $gen .= "<tr><td>&nbsp;</td><td><div class='checkbox' style='margin-top:0px; margin-bottom:0px;'><label><input type='checkbox' class='collapse_checkbox' value='" . $sobj->id . "'></label></div></td><td><a href='" . base_url() . "user/edit/" . $sobj->id . "'>" . $sobj->uid . "</a></td><td>" . $sobj->firstname . " " . $sobj->lastname . "</td><td>" . $state . "</td><td>" . $city . "</td><td>" . $zone . "</td><td>" . $activeplan . "</td><td>" . $active_from . "</td><td>" . $renewal_date . "</td><td>" . $expiration . "</td><td>" . $status . "</td><td><span>&#8377;</span> 0</td><td class='mui--text-right'>" . $ticket_count . "</td></tr>";
                $i++;
            }
        }

        if ($total_csearch == 0) {
            $gen .= "<tr><td style='text-align:center' colspan='14'> No Result Found !!</td></tr>";
        }

        $data['total_results'] = $total_csearch;
        $data['search_results'] = $gen;
        $arr=$this->get_ispdetail_info($jsondata);
        // echo "<pre>"; print_R($arr); die;
         $data['ispdetail']=(isset($arr['isp_detail']))?$arr['isp_detail']:array();
        $data['plan_count']=$this->get_plan_usercount($jsondata);
        $data['state_list']= $this->state_list('',$jsondata) ;
         $data['usage_locality']= $this->usage_locality() ;
       //  echo "<pre>"; print_R($data); die;
        return $data;
    }
    
    public function usage_locality($locality=''){
		$gen = '';
		$query = $this->db->get_where('sht_usage_locality',array('status' => '1', 'is_deleted' => '0'));
		if($query->num_rows() > 0){
			foreach($query->result() as $qobj){
				$sel = '';
				if($locality == $qobj->id){
					$sel = 'selected="selected"';
				}
				$gen .= '<option value="'.$qobj->id.'" '.$sel.'>'.$qobj->locality_name.'</option>';
			}
		}
		return $gen;
	}
    
    
      public function get_ispdetail_info($jsondata)
         {
             $data=array();
            $isp_uid = $jsondata->isp_uid;
             $query=$this->db->query("select * from sht_isp_detail where status=1 and isp_uid='".$isp_uid."'");
             $datar=$query->result();
             $data['isp_detail']= ($query->num_rows()>0)?$datar[0]:array();
             $data['tax']= $this->get_tax($jsondata);
           
             return $data;
                   
         }
         
         public function get_ispbase_info($jsondata)
         {
             
             $data=array();
              
         $isp_uid = $jsondata->isp_uid;
             $query=$this->db->query("select * from sht_isp_admin where  isp_uid='".$isp_uid."'");
             $data['baseinfo']=$query->result();
              return $data;
         }
         
         
            public function add_ispdetail_data($jsondata){
                //echo "<pre>"; print_R($jsondata); die;
		 $postdata= $jsondata->postdata;
                 $imagedata=$jsondata->imagedata;
                $isp_uid = $jsondata->isp_uid;
               
                if(isset($imagedata->original) && $imagedata->original!='')
                {
                 
                   $tabledata=array("isp_name"=>$postdata->ispname,"company_name"=>$postdata->company_name,"tin_number"=>$postdata->tin_number,
                        "service_tax_no"=>$postdata->service_tax_number,"gst_no"=>$postdata->gstnumber,"address1"=>$postdata->address_1,"address2"=>$postdata->address_2,
                       "pincode"=>$postdata->pincode,"city"=>$postdata->city,"state"=>$postdata->state,
                       "help_number1"=>$postdata->help_number1,"help_number2"=>$postdata->help_number2,"help_number3"=>$postdata->help_number3,
                        "support_email"=>$postdata->supp_email,"sales_email"=>$postdata->sale_email,"website"=>$postdata->web,
                        "original_image"=>$imagedata->original,"small_image"=>$imagedata->small,"logo_image"=>$imagedata->logo,
                       "created_on"=>date("Y-m-d H:i:s"),"geoaddress"=>$postdata->geoaddr,"place_id"=>$postdata->placeid
                           ,"lat"=>$postdata->lat,"long"=>$postdata->long,"status"=>1,"isp_uid"=>$isp_uid);
                }
                else {
                 
                $tabledata=array("isp_name"=>$postdata->ispname,"company_name"=>$postdata->company_name,"tin_number"=>$postdata->tin_number,
                        "service_tax_no"=>$postdata->service_tax_number,"gst_no"=>$postdata->gstnumber,"address1"=>$postdata->address_1,"address2"=>$postdata->address_2,
                       "pincode"=>$postdata->pincode,"city"=>$postdata->city,"state"=>$postdata->state,
                       "help_number1"=>$postdata->help_number1,"help_number2"=>$postdata->help_number2,"help_number3"=>$postdata->help_number3,
                        "support_email"=>$postdata->supp_email,"sales_email"=>$postdata->sale_email,"website"=>$postdata->web,
                       "update_on"=>date("Y-m-d H:i:s"),"created_on"=>date("Y-m-d H:i:s"),"geoaddress"=>$postdata->geoaddr,"place_id"=>$postdata->placeid
                           ,"lat"=>$postdata->lat,"long"=>$postdata->long,"status"=>1,"isp_uid"=>$isp_uid);

                }
            $postdata=$this->input->post();
            $query=$this->db->query("select * from sht_isp_detail where status=1 and isp_uid='".$isp_uid."'");
                 
            if($query->num_rows()>0)
            {
          
                  $this->db->update('sht_isp_detail', $tabledata,array("isp_uid"=>$isp_uid));
            }
            else{
                
                 $this->db->insert('sht_isp_detail', $tabledata); 
            }
           
            return 1;
                
	}
         
         
          public function get_plan_usercount($jsondata) {

        
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        
        $cond = $this->dept_plan_condition($jsondata);
        $datarr = array();
        $planarr = array("unlimited" => 1, "Time" => 2, "FUP" => 3, "Data" => 4);
        $totalcount = 0;
        foreach ($planarr as $key => $val1) {

            if ($cond != '') {
                $cond1 = ($cond == "allindia") ? "" : $cond;

                $query = $this->DB2->query("select ss.srvid from sht_services ss where ss.plantype='" . $val1 . "' and ss.enableplan='1' and ss.is_deleted='0' {$cond1} {$ispcond}");
                //   echo "======>>>".$this->db->last_query()."<br/>";
                $plancount = $query->num_rows();
                 $query2 = $this->DB2->query('SELECT ss.*, COUNT(su.id) AS usercount        
                FROM sht_services ss
                LEFT JOIN sht_users su
                ON (ss.srvid = su.baseplanid) WHERE ss.plantype="' . $val1 . '" '.$cond1.' '.$ispcond.'
                GROUP BY
                ss.srvid ORDER BY usercount DESC LIMIT 1');
            $rowarr = $query2->row_array();

            $srvid = $rowarr['srvid'];
            $query1 = $this->DB2->query("select id from sht_users where baseplanid ='" . $srvid . "' and enableuser='1' and expired='0'");
            $datarr[$key]['usercount'] = $query1->num_rows();
               $datarr[$key]['plantyoe'] = $val1;
            $datarr[$key]['planname'] = $rowarr['srvname'];
            $datarr[$key]['planid'] = $rowarr['srvid'];
            $totalcount = $totalcount + $query1->num_rows();
            } else {
                $plancount = 0;
                 $datarr[$key]['usercount'] = 0;
               $datarr[$key]['plantyoe'] = $val1;
            $datarr[$key]['planname'] = "";
            $datarr[$key]['planid'] = "";
            $totalcount = 0;
            }



            $datarr[$key]['plancount'] = $plancount;

           
         
        }
        //  echo "<pre>"; print_R($datarr); die;
        $datarr['totalusercount'] = $totalcount;
        return $datarr;



        //$this->db->query('select * from ')
    }
    
    
        public function dept_plan_condition($jsondata) {
       
        $superadmin = $jsondata->super_admin;
        $dept_id = $jsondata->dept_id;
      
        $cond = '';
        if ( $superadmin == 1) {

            $cond = 'allindia';
        } else {
            $query = $this->db->query("select * from sht_dept_region where dept_id='" . $dept_id . "'");
            if ($query->num_rows() > 0) {
                $condarr = array();





                foreach ($query->result() as $val) {

                    if ($val->state_id != 'all' && $val->city_id != 'all' && $val->zone_id != 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "' and city_id='" . $val->city_id . "' and zone_id='" . $val->zone_id . "')";
                    } else if ($val->state_id != 'all' && $val->city_id == 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "')";
                    } else if ($val->state_id != 'all' && $val->city_id != 'all' && $val->zone_id == 'all') {
                        $condarr[] = "(state_id='" . $val->state_id . "' and city_id='" . $val->city_id . "')";
                    }
                }

             $cond1 = implode('OR', $condarr);
                $query2 = $this->DB2->query("select srvid from sht_plan_pricing where region_type='allindia'");
                $paramarr1 = array();
                if ($query2->num_rows() > 0) {
                    foreach ($query2->result() as $val2) {
                        $paramarr1[] = $val2->srvid;
                    }
                }


                $query1 = $this->DB2->query("select distinct(plan_id) from sht_plan_region where ({$cond1}) and status='1' and is_deleted='0'");
                //   echo $this->db->last_query(); die;

                $paramarr = array();
                if ($query1->num_rows() > 0) {
                    foreach ($query1->result() as $val1) {
                        $paramarr[] = $val1->plan_id;
                    }
                    $paramarr = array_merge($paramarr, $paramarr1);
                    $paramid = '"' . implode('", "', $paramarr) . '"';
                    $cond .= "AND ss.srvid IN ({$paramid})";
                } else {
                    $paramarr = array_merge($paramarr, $paramarr1);
                    if (!empty($paramarr)) {
                        $paramid = '"' . implode('", "', $paramarr) . '"';
                        $cond .= "AND ss.srvid IN ({$paramid})";
                    } else {
                        $cond .= "allindia";
                    }
                }
            }
        }
        // echo $cond; die;
        return $cond;
    }
    
    
     public function state_list($stateid = '',$jsondata) {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $jsondata->super_admin;
      //  echo "<pre>"; print_R($sessiondata); die;
        $dept_id = $jsondata->dept_id;
        $isp_uid = $jsondata->isp_uid;
        $regiontype = $this->dept_region_type();
        if ($superadmin == 1) {
             $query = $this->db->query("select state from sht_isp_admin_region where isp_uid='" . $isp_uid . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select state_id from sht_dept_region where dept_id='" . $dept_id . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state_id;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        }
        return $gen;
    }
    
      public function getstate_list($jsondata) {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $jsondata->super_admin;
      //  echo "<pre>"; print_R($sessiondata); die;
        $dept_id = $jsondata->dept_id;
        $isp_uid = $jsondata->isp_uid;
        $stateid = $jsondata->stateid;
        $data=array();
        $regiontype = $this->dept_region_type();
        if ($superadmin == 1) {
             $query = $this->db->query("select state from sht_isp_admin_region where isp_uid='" . $isp_uid . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        } else {
            $query = $this->db->query("select state_id from sht_dept_region where dept_id='" . $dept_id . "'");
            $statearr = array();
            foreach ($query->result() as $val) {
                $statearr[] = $val->state_id;
            }
            if (in_array('all', $statearr)) {
                $stateQ = $this->db->get('sht_states');
            } else {
                $sid = '"' . implode('", "', $statearr) . '"';
                $stateQ = $this->db->query("select * from sht_states where id IN ({$sid}) ");
            }
            $num_rows = $stateQ->num_rows();
            if ($num_rows > 0) {
                $gen = '';
                foreach ($stateQ->result() as $stobj) {
                    $sel = '';
                    if ($stateid == $stobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $stobj->id . '" ' . $sel . '>' . $stobj->state . '</option>';
                }
            }
        }
        $data['state_list']=$gen;
        return $data;
    }
    
      public function getcitylist($jsondata) {
      
        $superadmin = $jsondata->super_admin;
        $dept_id = $jsondata->dept_id;
        $isp_uid = $jsondata->isp_uid;
        $stateid= $jsondata->stateid;
         $cityid= $jsondata->cityid;
         $not_addable= ($jsondata->is_addable==1)?1:0;
       
       
        $allcity = '<option value="all">All Cities</option>';

        $gen = '';
        if ($superadmin == 1) {

          $cityQ = $this->db->query("SELECT * FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
            $num_rows = $cityQ->num_rows();
            $gen .= $allcity;
            if ($num_rows > 0) {

                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            }
            if ($not_addable == 0) {
                $gen .= '<option id="addcityfly" class="addcityfly" value="addc">--Add City--</option>';
            }
        } else {
            $query = $this->db->query("select city_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $cityarr = array();
            foreach ($query->result() as $val) {
                $cityarr[] = $val->city_id;
            }
            $isaddcity = 0;
            if (in_array('all', $cityarr)) {
                $allcity = '<option value="all">All Cities</option>';
                $cityQ = $this->db->query("SELECT * FROM sht_cities WHERE  (isp_uid='".$isp_uid."' AND city_state_id='".$stateid."') OR (isp_uid='0'  AND  city_state_id='".$stateid."')");
                $isaddcity = 1;
            } else {
                $allcity = '<option value="">Select City</option>';
                $cid = '"' . implode('", "', $cityarr) . '"';
                $cityQ = $this->db->query("select * from sht_cities where city_id IN ({$cid})");
                $isaddcity = 0;
            }
            $num_rows = $cityQ->num_rows();
            if ($num_rows > 0) {
                $gen .= $allcity;
                foreach ($cityQ->result() as $ctobj) {
                    $sel = '';
                    if ($cityid == $ctobj->city_id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $ctobj->city_id . '" ' . $sel . '>' . $ctobj->city_name . '</option>';
                }
            } else {
                $allcity = '<option value="all">All Cities</option>';
                $gen .= $allcity;
            }

            if ($isaddcity == 1) {
                if ($not_addable == 0) {
                    $gen .= '<option id="addcityfly" class="addcityfly" value="addc">--Add City--</option>';
                }
            }
        }
        return $gen;
    }

    public function getzonelist($jsondata) {

        $superadmin = $jsondata->super_admin;
        $dept_id = $jsondata->dept_id;
        $isp_uid = $jsondata->isp_uid;
        $stateid= $jsondata->stateid;
         $cityid= $jsondata->cityid;
         $zoneid=$jsondata->zoneid;
         $not_addable= ($jsondata->is_addable==1)?1:0;
       
        // $stateid=$this->input->post('stateid');
        $allzone = '<option value="all">All Zone</option>';
        $gen = '';
        if ($superadmin == 1) {

            $zoneQ = $this->db->query("SELECT * FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            $num_rows = $zoneQ->num_rows();
            $gen .= $allzone;
            if ($num_rows > 0) {

                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            }
            if ($not_addable == 0) {
                $gen .= '<option id="addzonefly" class="addzonefly" value="addz">--Add Zone--</option>';
            }
        } else {

            $query = $this->db->query("select zone_id from sht_dept_region where dept_id='" . $dept_id . "' and state_id='" . $stateid . "'");
            $zonearr = array();
            foreach ($query->result() as $val) {
                $zonearr[] = $val->zone_id;
            }
            $isaddzone = 0;
            if (in_array('all', $zonearr)) {
                //   echo "sssss"; die;
                $isaddzone = 1;
                $allzone = '<option value="all">All Zone</option>';
                $zoneQ = $this->db->query("SELECT * FROM `sht_zones` WHERE  (isp_uid='".$isp_uid."' AND city_id='".$cityid."') OR (isp_uid='0'  AND city_id='".$cityid."')");
            } else {
                $isaddzone = 0;
                $allzone = '<option value="">Select Zone</option>';
                $zid = '"' . implode('", "', $zonearr) . '"';
                $zoneQ = $this->db->query("select * from sht_zones where id IN ({$zid})");
            }
            $num_rows = $zoneQ->num_rows();

            if ($num_rows > 0) {
                $gen .= $allzone;
                foreach ($zoneQ->result() as $znobj) {
                    $sel = '';
                    if ($zoneid == $znobj->id) {
                        $sel = 'selected="selected"';
                    }
                    $gen .= '<option value="' . $znobj->id . '" ' . $sel . '>' . $znobj->zone_name . '</option>';
                }
            } else {
                $allzone = '<option value="all">All Zone</option>';
                $gen .= $allzone;
            }
            if ($isaddzone == 1) {
                if ($not_addable == 0) {
                    $gen .= '<option id="addzonefly" class="addzonefly" value="addz">--Add Zone--</option>';
                }
            }
        }
        return $gen;
    }
    
     public function viewplan_search_results($jsondata) {
        $data = array();
        $dataaind = array();
        $gen = '';
        $total_search = 0;


        $cond1 = "";
        $cond2 = "";
        $cond3 = "";
        $cond4 = "";
        /* Condition for search filter */
        $search_user = $jsondata->search_user;
        $user_state = $jsondata->state;
        $user_city = $jsondata->city;
        $user_zone = $jsondata->zone;
        
         
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";

        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "' and zone_id='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(state_id='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $cond1 = "and ($cond1)";
        }


        $query = $this->DB2->query("SELECT distinct(plan_id) FROM sht_plan_region WHERE status='1' $cond1  AND is_deleted='0'");

        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $pobj) {
                $data[$i] = $pobj->plan_id;
                $i++;
            }
        }

        $query1 = $this->DB2->query("SELECT ss.srvid FROM sht_services ss
INNER JOIN `sht_plan_pricing` spp ON(ss.srvid=spp.srvid) AND spp.region_type='allindia' {$ispcond} AND ss.`enableplan`=1");
        if ($query1->num_rows() > 0) {
            $i = 0;
            foreach ($query1->result() as $val) {
                $dataaind[$i] = $val->srvid;
                $i++;
            }
        }
        $pidarr = array_merge($data, $dataaind);
        $pid = '"' . implode('", "', $pidarr) . '"';
// echo $pid;
        $cond = "";
        if ($pid != '') {
            $cond .= "and ss.srvid IN ({$pid})";
        }

        if ($search_user != '') {
            $cond .= "and srvname LIKE '%" . $search_user . "%'";
        }
        //conditions for department filter
        $cond2 = $this->dept_plan_condition($jsondata);
        $cond2 = ($cond2 == "allindia") ? "" : $cond2;

        $query3 = $this->DB2->query("SELECT ss.*,spp.net_total,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1'"
                . " and ss.plantype!='0' {$cond} {$ispcond} {$cond2}");

            return $query3->result();    
        
    }
    
      public function viewplan_searchfilter($jsondata) {
        $data = array();
        $dataaind = array();
        $gen = '';
        $total_search = 0;
        $planarr = array("unlimited" => 1, "time" => 2, "fup" => 3, "data" => 4);

        $cond1 = "";
        $cond2 = "";
        $cond3 = "";
        $cond4 = "";
        
         
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";

        $search_user = $jsondata->search_user;
        $user_state = $jsondata->state;
        $user_city = $jsondata->city;
        $user_zone = $jsondata->zone;
        $filtertype = $jsondata->filtertype;
        $plantype = $planarr[$filtertype];
        $condarr = array();
        if ($user_state != 'all' && $user_city != 'all' && $user_zone != 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "' and zone_id='" . $user_zone . "')";
        } else if ($user_state != 'all' && $user_city == 'all') {
            $condarr[] = "(state_id='" . $user_state . "')";
        } else if ($user_state != 'all' && $user_city != 'all' && $user_zone == 'all') {
            $condarr[] = "(state_id='" . $user_state . "' and city_id='" . $user_city . "')";
        }
        $cond1 = implode('OR', $condarr);



        if ($cond1 != "") {
            $cond1 = "and ($cond1)";
        }


        $query = $this->DB2->query("SELECT distinct(plan_id) FROM sht_plan_region WHERE status='1' $cond1  AND is_deleted='0' ");
        //  echo $this->db->last_query();
//echo "<pre>"; print_R($query->result()); die;
        if ($query->num_rows() > 0) {
            $i = 0;
            foreach ($query->result() as $pobj) {
                $data[$i] = $pobj->plan_id;
                $i++;
            }
        }

        $query1 = $this->DB2->query("SELECT ss.srvid FROM sht_services ss
INNER JOIN `sht_plan_pricing` spp ON(ss.srvid=spp.srvid) AND spp.region_type='allindia' {$ispcond} AND ss.`enableplan`=1");
        if ($query1->num_rows() > 0) {
            $i = 0;
            foreach ($query1->result() as $val) {
                $dataaind[$i] = $val->srvid;
                $i++;
            }
        }
        $pidarr = array_merge($data, $dataaind);
        $pid = '"' . implode('", "', $pidarr) . '"';
// echo $pid;
        $cond = "";
        if ($pid != '') {
            $cond .= "and ss.srvid IN ({$pid})";
        }

        if ($search_user != '') {
            $cond .= "and srvname LIKE '%" . $search_user . "%'";
        }

        /*   $query3 = $this->db->query("SELECT ss.*,spp.net_total FROM `sht_services` ss
          LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1'"
          . " and ss.plantype!='0' {$cond} and plantype='".$plantype."'"); */

        $query3 = $this->DB2->query("SELECT ss.*,spp.net_total,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.enableplan='1'"
                . " and ss.plantype!='0' {$cond} {$ispcond} and plantype='" . $plantype . "'");

        //   echo $this->db->last_query(); die;
        $total_search = $query3->num_rows();
        $i = 1;
       // $is_editable = $this->plan_model->is_permission(PLANS, 'EDIT');
        //$is_deletable = $this->plan_model->is_permission(PLANS, 'DELETE');
        foreach ($query3->result() as $sobj) {

            $plandata = "";
            if ($sobj->plantype == 1) {
                $plandata = "Unlimited";
                $plantype="Unlimited";
            } else if ($sobj->plantype == 2) {
                $plandata = "Timeplan";
                $plantype="Timeplan";
            } else if ($sobj->plantype == 3) {
                $plandata = round($this->convertTodata($sobj->datalimit . "GB")) . " GB";
                 $plantype="FUP plan";
            } else {
                $plandata = round($this->convertTodata($sobj->datalimit . "GB")) . " GB";
                 $plantype="Data plan";
            }

            $status = ($sobj->enableplan == 1) ? 'active' : 'inactive';
            if ($sobj->enableplan == 1) {
                $class = 'class="inactive delete"';
                $status = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $stat1 = "Active";
                // $task="disable";
            } else {
                $class = 'class="active delete"';
                $status = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $stat1 = "Inactive";
                // $task="enable";
            }

            $gen .= '
				<tr>
					<td>' . $i . '.</td>
					
					<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">' . $sobj->srvname . '</a></td>
                                            <td>' . $plantype . '</td>
					<td>' . $this->convertTodata($sobj->downrate . "KB") . '</td>
					<td>' . $this->convertTodata($sobj->uprate . "KB") . '</td>
					<td>' . $plandata . '</td>
					<td>₹' . $sobj->net_total . '</td>
					<td><a href="' . base_url() . 'plan/user_stat/' . $sobj->srvid . '">' . $sobj->usercount . '</a></td>';

          
                $gen .= '<td><a ' . $class . '  onclick = "change_planstatus(\'' . $sobj->srvid . '\')" href="javascript:void(0)" id="' . $sobj->srvid . '"> ' . $status . ' </a></td>';
           
            $gen .= '<td><a href="' . base_url() . 'plan/edit_plan/' . $sobj->srvid . '">EDIT</a></td>';


            $delclass =  "deletplan" ;

            $gen .= ' <td><a href="javascript:void(0);" class="' . $delclass . '"  rel="' . $sobj->srvid . '">Delete</a></td>';

            $gen .= '</tr>
				';
            $i++;
        }


        $data['total_results'] = $total_search;
        $data['search_results'] = $gen;

        return $data;
    }
    
      public function plan_data($jsondata) {
            $id=$jsondata->id;
        //  $condarr=array('id'=>$id);
            $data=array();
        $query = $this->DB2->query("SELECT ss.*,spp.net_total,spp.gross_amt,spp.tax,spp.region_type FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE  ss.srvid='" . $id . "'");
        $data['plan_data']=$query->row_array();
        return  $data;
    }
    
    public function customer_tickets_count($userid){
		$query = $this->db->query("SELECT * FROM sht_subscriber_tickets WHERE subscriber_id='".$userid."'");
		$num_rows = $query->num_rows();
		return $num_rows;
	}
    
    public function getstatename($stateid){
		$name = '';
		$stateQ = $this->db->get_where('sht_states', array('id' => $stateid));
		$num_rows = $stateQ->num_rows();
		if($num_rows > 0){
			$data = $stateQ->row();
			$name = ucwords($data->state);
		}
		return $name;
	}
	public function getcityname($stateid, $cityid){
		$name = '';
		$cityQ = $this->db->get_where('sht_cities', array('city_state_id' => $stateid, 'city_id' => $cityid));
		$num_rows = $cityQ->num_rows();
		if($num_rows > 0){
			$data = $cityQ->row();
			$name = ucwords($data->city_name);
		}
		return $name;
	}
	public function getzonename($zoneid){
		$name = '';
		$zoneQ = $this->db->get_where('sht_zones', array('id' => $zoneid));
		$num_rows = $zoneQ->num_rows();
		if($num_rows > 0){
			$data = $zoneQ->row();
			$name = ucwords($data->zone_name);
		}
		return $name;
	}
        
        public function userassoc_planname($userid){
		$planname = '-';
		$query = $this->DB2->query("SELECT tb2.srvname FROM sht_users as tb1 INNER JOIN sht_services as tb2 ON(tb1.baseplanid=tb2.srvid) WHERE tb1.id='".$userid."'");
		if($query->num_rows() > 0){
			$planname = $query->row()->srvname;
		}
		return $planname;
	}
        
          public function plan_list_filter($jsondata) {
          $data=array();
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and ss.isp_uid='".$isp_uid."'";
        $cond = $this->dept_plan_condition($jsondata);
        $cond = ($cond == "allindia") ? "" : $cond;
        $plantype=$jsondata->plantype;
        $planarr = array("unlimited" => 1, "time" => 2, "fup" => 3, "data" => 4);
        //$condarr=array("enableplan"=>1);
        $query = $this->DB2->query("SELECT ss.*,spp.net_total,(SELECT COUNT(id) FROM sht_users su WHERE su.baseplanid=ss.srvid AND su.enableuser='1') as usercount FROM `sht_services` ss
                        LEFT JOIN `sht_plan_pricing` spp ON (ss.srvid=spp.srvid) WHERE ss.is_deleted='0' and ss.plantype!='0' and plantype='" . $planarr[$plantype] . "' {$cond} {$ispcond}");
        $arr=$this->get_ispdetail_info($jsondata);
        // echo "<pre>"; print_R($arr); die;
        $data['plan_listing']= $query->result();
         $data['ispdetail']=(isset($arr['isp_detail']))?$arr['isp_detail']:array();
        $data['plan_count']=$this->get_plan_usercount($jsondata);
        $data['state_list']= $this->state_list('',$jsondata) ;
                      //  return $query->result();
          return $data;              
    }
        
    
     public function add_plan_detail($jsondata) {
        $postdata = $jsondata->postdata;
        //$sessiondata = $jsondata->isp_session;
         $isp_uid = $jsondata->isp_uid;
     if ($postdata->plan_type_radio == "TP") {

            $timelimit = $postdata->timelimit*60;
            $dwnld_rate = $this->convertToBytes($postdata->dwnld_rate . "KB");
            $upld_rate = $this->convertToBytes($postdata->upld_rate . "KB");
            // `downrate``uprate``isspeedtopup``isdataunacnttopup``isunlimitedplan``istimeplan``isfupplan``isdataplan``timecalc``datacalc``fupuprate``fupdownrate``timelimit``datalimit`

            
            $tabledata = array("srvname" => $postdata->plan_name, "descr" => $postdata->plan_desc,
                "enableplan" => 0, "plantype" => 2, "datacalc" => 0,
                "fupuprate" => 0, "fupdownrate" => 0, "datalimit" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate, "timelimit" => $timelimit, "timecalc" => 0
            );
        } else if ($postdata->plan_type_radio == "HP") {
            $datalimit = $this->convertToBytes($postdata->data_limith . "MB");
            $dwnld_rate = $this->convertToBytes($postdata->dwnld_rate . "KB");
            $upld_rate = $this->convertToBytes($postdata->upld_rate . "KB");
             $timelimit = $postdata->timelimith*60;
           
            $datecal = 2 ;
            $tabledata = array("srvname" => $postdata->plan_name, "descr" => $postdata->plan_desc,
                "enableplan" => 0, "plantype" => 5, "downrate" => $dwnld_rate,
                "uprate" => $upld_rate, "datalimit" => $datalimit, "timecalc" => 0, "datacalc" => $datecal,"isp_uid"=>$isp_uid
                ,"timelimit" => $timelimit );
        } else {
            $datalimit = $this->convertToBytes($postdata->data_limit . "MB");
            $dwnld_rate = $this->convertToBytes($postdata->dwnld_rate . "KB");
            $upld_rate = $this->convertToBytes($postdata->upld_rate . "KB");
             $datecal =  2;
            $tabledata = array("srvname" => $postdata->plan_name, "descr" => $postdata->plan_desc,
                "enableplan" => 0, "plantype" => 4, "fupuprate" => 0, "fupdownrate" => 0, "downrate" => $dwnld_rate,"isp_uid"=>$isp_uid,
                "uprate" => $upld_rate, "datalimit" => $datalimit,"datacalc" => $datecal);
        }
        if (isset($postdata->plan_id) && $postdata->plan_id != '') {
            $this->DB2->update(SHTSERVICES, $tabledata, array('srvid' => $postdata->plan_id));
            $this->plan_enable_setting($postdata->plan_id, 'PLANDETAIL');
            return $postdata->plan_id;
        } else {
            $this->DB2->insert(SHTSERVICES, $tabledata);
            $id = $this->DB2->insert_id();
            $this->plan_enable_setting($id, 'PLANDETAIL');
            $this->add_system_serviceid($id);
            return $id;
        }
    }
    
    
       public function add_plan_setting($jsondata) {
        $postdata = $jsondata->postdata;
        // echo "<pre>"; print_R($postdata);die;
        $dlburstlimit = $this->convertToBytes($postdata->dwnld_burst . "KB");
        $dlburstthreshold = $this->convertToBytes($postdata->dwnld_threshold . "KB");
        $ulburstlimit = $this->convertToBytes($postdata->upld_burst . "KB");
        $ulburstthreshold = $this->convertToBytes($postdata->upld_threshold. "KB");
        $bursttime = (isset($postdata->burst_time) && $postdata->burst_time != '') ? $this->timeToSeconds($postdata->burst_time) : "";
        //
        $tabledata = array("dlburstlimit" => $dlburstlimit, "dlburstthreshold" => $dlburstthreshold, "ulburstlimit" => $ulburstlimit,
            "ulburstthreshold" => $ulburstthreshold, "bursttime" => $bursttime, "priority" => $postdata->burst_priority,
            "enableburst" => $postdata->is_burst_enable);
        if (isset($postdata->plan_id) && $postdata->plan_id != '') {
            $this->DB2->update(SHTSERVICES, $tabledata, array('srvid' => $postdata->plan_id));
            $this->plan_enable_setting($postdata->plan_id, 'PLANSETTING');
             $this->plan_enable_setting($postdata->plan_id, 'PLANPRICING');
             $this->plan_enable_setting($postdata->plan_id, 'PLANREGION');
            return $postdata->plan_id;
        }
    }
    
     public function add_pricing($jsondata) {
        $postdata =  $jsondata->postdata;
        $tabledata = array('srvid' => $postdata->plan_id, "gross_amt" => $postdata->gross_amt, "tax" => $postdata->tax, "net_total" => $postdata->net_amount, "created_on" => date("Y-m-d H:i:s")
        );
        if (isset($postdata->plan_id) && $postdata->plan_id != '') {
            $query = $this->DB2->query("select gross_amt from " . SHTPLANPRICING . " where srvid='" . $postdata->plan_id . "'");
            if ($query->num_rows() > 0) {
                $this->DB2->update(SHTPLANPRICING, $tabledata, array('srvid' => $postdata->plan_id));
                $this->plan_enable_setting($postdata->plan_id, 'PLANPRICING');
            } else {
                $this->DB2->insert(SHTPLANPRICING, $tabledata);
                $this->plan_enable_setting($postdata->plan_id, 'PLANPRICING');
            }

            return $postdata->plan_id;
        }
    }
        
    
     public function add_region($jsondata) {
        $postdata = $jsondata->postdata;
        $isp_uid = $jsondata->isp_uid;
        $tabledata = array("region_type" => $postdata->region);
        if (isset($postdata->plan_id) && $postdata->plan_id != '') {
            $query = $this->DB2->query("select region_type from sht_plan_pricing where srvid='" . $postdata->plan_id . "'");
            if ($query->num_rows() > 0) {
                $this->DB2->update(SHTPLANPRICING, $tabledata, array('srvid' => $postdata->plan_id));
                $this->plan_enable_setting($postdata->plan_id, 'PLANREGION');
            } else {
                $tabledata = array("region_type" => $postdata->region, 'srvid' => $postdata->plan_id);
                $this->DB2->insert(SHTPLANPRICING, $tabledata);
                $this->plan_enable_setting($postdata->plan_id, 'PLANREGION');
            }


            if ($postdata->region == "region") {

                $listarr = array_filter(explode(",", $postdata->regiondat));

                foreach ($listarr as $vald) {
                    $datarr = explode("::", $vald);
                    //  `plan_id`,`state_id`,`city_id`,`zone_id`,
                    $tabledata = array("plan_id" => $postdata->plan_id, "state_id" => $datarr[0],
                        "city_id" => $datarr['1'], "zone_id" => $datarr[2],
                        "created_on" => date("Y-m-d H:i:s"),"isp_uid"=>$isp_uid);

                    if (isset($datarr[3]) && $datarr[3] != '') {
                        $this->DB2->update(SHTPLANREGION, $tabledata, array('id' => $datarr[3]));
                    } else {
                        $this->DB2->insert(SHTPLANREGION, $tabledata);
                    }
                }
                $this->plan_nas_association($postdata->plan_id,$jsondata);
            } else {
                $this->DB2->delete(SHTPLANREGION, array("plan_id" => $postdata->plan_id));
                $this->plan_nas_association($postdata->plan_id,$jsondata);
            }

            return $postdata->plan_id;
        }
    }
    
    
      public function plan_region_data($jsondata) {
          $id=$jsondata->id;
        $condarr = array('plan_id' => $id, 'is_deleted' => 0);
        $query = $this->DB2->get_where(SHTPLANREGION, $condarr);
        
        $data['region_data']= $query->result();
        return $data;
        
    }
    
       public function check_plan_deletable($jsondata) {
        $planid =$jsondata->planid;
        $query = $this->DB2->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
     public function delete_plan($jsondata) {
        $postdata = $jsondata->postdata;
        $tabledata = array("is_deleted" => 1);
        $this->DB2->update(SHTSERVICES, $tabledata, array('srvid' => $postdata->planid));
      //  echo $this->DB2->last_query(); die;
        $this->disable_system_serviceid($postdata->planid);
        return $postdata->planid;
    }
    
      public function check_plan_disable($jsondata) {
          $postdata = $jsondata->postdata;
        $planid = $postdata->id;
        $query = $this->DB2->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }
    
      public function check_plan_enable($jsondata) {
        $postdata = $jsondata->postdata;
        $planid = $postdata->id;
        $conarr = array("plan_id" => $planid, "plan_detail" => 1, "plan_setting" => 1, "plan_pricing" => 1, "plan_region" => 1, "forced_disable" => 0);
        $query1 = $this->DB2->get_where(PLANACTIVATIONTAB, $conarr);

    if ($query1->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function status_approval($jsondata) {
        $id=$jsondata->id;
        $query = $this->DB2->query("select `enableplan` from sht_services where srvid='" . $id . "'");
        $record = $query->row_array();
        if (!empty($record)) {
            $stat = '';
            $statText = '';
            $status = $record['enableplan'];
            if ($status == 0) {
                $stat = '1';
                $statText = '<img src="' . base_url() . 'assets/images/on2.png" rel="disable">';
                $this->enable_system_serviceid($id);
            } else {
                $stat = '0';
                $statText = '<img src="' . base_url() . 'assets/images/off2.png" rel="enable">';
                $this->disable_system_serviceid($id);
            }
            $tabledata = array("enableplan" => $stat);
            $this->DB2->where('srvid', $id);
            $this->DB2->update('sht_services', $tabledata);
         
            
        }
        return $statText;
    }
    
       public function check_taxfilled($jsondata)
        {
           
           // $sessiondata = $this->session->userdata('isp_session');
             $isp_uid = $jsondata->isp_uid;
             $query=$this->db->query("select tax from sht_tax where isp_uid='".$isp_uid."'");
            // echo $this->db->last_query();die;
             if($query->num_rows()>0)
             {
                 return 1;
             }
             else
             {
                  return 0;
             }
        } 
        //plan ends for this

   

    public function convertToBytes($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number * 1024;
            case "MB":
                return $number * 1024 * 1024;
            case "GB":
                return $number * 1024 * 1024 * 1024;
            case "TB":
                return $number * 1024 * 1024 * 1024 * 1024;
            case "PB":
                return $number * 1024 * 1024 * 1024 * 1024 * 1024;
            default:
                return $from;
        }
    }

    public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number / pow(1024, 2);
            case "GB":
                return $number / pow(1024, 3);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }

    function timeToSeconds($time) {
        $timeExploded = explode(':', $time);
        if (isset($timeExploded[2])) {
            return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
        }
        return $timeExploded[0] * 3600 + $timeExploded[1] * 60;
    }

  

  

   

    public function dept_region_type() {
        $sessiondata = $this->session->userdata('isp_session');
        $superadmin = $sessiondata['super_admin'];
        $dept_id = $sessiondata['dept_id'];
        $region_type = '';
        if ($superadmin == 1) {
            $region_type = 'region';
        } else {
            $query = $this->db->get_where('sht_department', array('id' => $dept_id));
            if ($query->num_rows() > 0) {
                $rowarr = $query->row_array();
                $region_type = $rowarr['region_type'];
            }
        }
        

        return $region_type;
    }

  

   

    public function delete_region_plan() {
        $postdata = $this->input->post();
        $tabledata = array("is_deleted" => 1);
        $this->db->update(SHTPLANREGION, $tabledata, array('id' => $postdata['id']));
       
        return $postdata['id'];
    }



    function sec2hms($secs) {
        $secs = round($secs);
        $secs = abs($secs);
        $hours = floor($secs / 3600) . ':';
        if ($hours == '0:')
            $hours = '';
        $minutes = substr('00' . floor(($secs / 60) % 60), -2);
        //$seconds = substr('00' . $secs % 60, -2);
        return ltrim($hours . $minutes, '0');
    }

   


    public function plan_enable_setting($plan_id, $form) {
        $query = $this->DB2->query("select id from sht_plan_activation_panel where plan_id='" . $plan_id . "'");
        if ($query->num_rows() > 0) {
            if ($form == "PLANDETAIL") {
                $tabledata = array("plan_detail" => 1);
                $this->DB2->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else if ($form == "PLANSETTING") {
                $tabledata = array("plan_setting" => 1);
                $this->DB2->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else if ($form == "PLANPRICING") {
                $tabledata = array("plan_pricing" => 1);
                $this->DB2->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            } else {
                $tabledata = array("plan_region" => 1);
                $this->DB2->update(PLANACTIVATIONTAB, $tabledata, array('plan_id' => $plan_id));
            }
        } else {
            if ($form == "PLANDETAIL") {
                $tabledata = array("plan_detail" => 1, 'plan_id' => $plan_id);
                $this->DB2->insert(PLANACTIVATIONTAB, $tabledata);
            } else if ($form == "PLANSETTING") {
                $tabledata = array("plan_setting" => 1, 'plan_id' => $plan_id);
                $this->DB2->insert(PLANACTIVATIONTAB, $tabledata);
            } else if ($form == "PLANPRICING") {
                $tabledata = array("plan_pricing" => 1, 'plan_id' => $plan_id);
                $this->DB2->insert(PLANACTIVATIONTAB, $tabledata);
            } else {
                $tabledata = array("plan_region" => 1, 'plan_id' => $plan_id);
                $this->DB2->insert(PLANACTIVATIONTAB, $tabledata);
            }
        }
        $conarr = array("plan_id" => $plan_id, "plan_detail" => 1, "plan_setting" => 1, "plan_pricing" => 1, "plan_region" => 1, "forced_disable" => 0);
        $query1 = $this->DB2->get_where(PLANACTIVATIONTAB, $conarr);
        if ($query1->num_rows() > 0) {
            $tabledata = array("enableplan" => 1);
            $this->DB2->update(SHTSERVICES, $tabledata, array('srvid' => $plan_id));
        }

        return $plan_id;
    }

  

  
    
     public function check_plan_editable($id) {
        $planid = $id;
        $query = $this->db->query("select id from sht_users where baseplanid='" . $planid . "' and enableuser='1'");
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

 

    public function is_permission($slug, $permtype = "RO") {

        $userid = $this->session->userdata['isp_session']['userid'];
       $sessiondata = $this->session->userdata('isp_session');
      
       $query=$this->db->query("select dept_id from sht_isp_users where id='".$userid."'");
       $rowarr=$query->row_array();
         $isp_uid = $sessiondata['isp_uid'];
        if ($sessiondata['super_admin'] == 1) {
            return true;
        } else {
            $deptid = $rowarr['dept_id'];
            $query1 = $this->db->query("select id from sht_tab_menu where slug='" . $slug . "' ");
            $tabarr = $query1->row_array();
            $tabid = $tabarr['id'];
            //  echo $deptid."::".$tabid;

            $field = '';
            if ($permtype == "ADD") {
                $field = 'is_add';
            } else if ($permtype == "EDIT") {
                $field = 'is_edit';
            } else if ($permtype == "DELETE") {
                $field = 'is_delete';
            } else if ($permtype == "HIDE") {
                $field = 'is_hide';
            } else {
                $field = 'is_readonly';
            }

            $query2 = $this->db->query("select " . $field . " as perm from sht_isp_permission where isp_deptid='" . $deptid . "' and tabid='" . $tabid . "'");
            //  echo $this->db->last_query();
            //  die;
            if ($query2->num_rows() > 0) {
                $permarr = $query2->row_array();
                if ($permarr['perm'] == 1) {
                    if($permtype=="HIDE")
                    {
                         return false;
                    }
                    else
                    {
                         return true;
                    }
                   
                } else {
                     if($permtype=="HIDE")
                    {
                         return true;
                    }
                    else
                    {
                         return false;
                    }
                    //return false;
                }
            } else {
                return false;
            }
        }
    }

    public function leftnav_permission() {

        $userid = $this->session->userdata['isp_session']['userid'];
        $query = $this->db->query("select id,super_admin,dept_id from sht_isp_users where id='" . $userid . "'");
        $tabarr = array();
        $rowarr = $query->row_array();
        $sessiondata = $this->session->userdata('isp_session');
         $isp_uid = $sessiondata['isp_uid'];

        $deptid = $rowarr['dept_id'];


        if ($sessiondata['super_admin'] == 1) {
            $query = $this->db->query("select slug from sht_tab_menu where status='1'");
            foreach ($query->result() as $val) {
                $tabarr[$val->slug] = 0;
            }
        } else {
            $query1 = $this->db->query("SELECT sip.is_hide,stm.slug FROM `sht_isp_permission` sip
INNER JOIN `sht_tab_menu` stm ON (stm.id=sip.tabid) WHERE isp_deptid='" . $deptid . "' ");
            foreach ($query1->result() as $val) {
                $tabarr[$val->slug] = ($sessiondata['super_admin'] == 1) ? 0 : $val->is_hide;
            }
        }
        //  echo $deptid."::".$tabid;
      
       
        return $tabarr;
    }

    public function plan_nas_association($plan_id,$jsondata) {
        $condarr = array();
         
         $isp_uid = $jsondata->isp_uid;
         $dept_regionQ = $this->DB2->query("select * from sht_plan_region where plan_id='" . $plan_id . "'");
         $total_deptregion= $dept_regionQ->num_rows();
         $permicond='';
         $sczcond='';
          if ($dept_regionQ->num_rows() > 0) {
                $c = 1;
         foreach ($dept_regionQ->result() as $deptobj) {
                    $stateid = $deptobj->state_id;
                    $cityid = $deptobj->city_id;
                    $zoneid = $deptobj->zone_id;
                    if ($cityid == 'all') {
                        $permicond .= " (nasstate='" . $stateid . "') ";
                    } elseif ($zoneid == 'all') {
                        $permicond .= " (nasstate='" . $stateid . "' AND nascity='" . $cityid . "') ";
                    } else {
                        $permicond .= " (nasstate='" . $stateid . "' AND nascity='" . $cityid . "' AND naszone='" . $zoneid . "') ";
                    }
                    if ($c != $total_deptregion) {
                        $permicond .= ' OR ';
                    }
                    $c++;
                }
          }
           // echo 'ssss';
            if($permicond!='')
            {
            $sczcond .= '(' . $permicond . ')';
            }
              $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
            
             $query1 = $this->DB2->query("select distinct(id) from nas where {$sczcond} {$ispcond}");
       // echo $this->DB2->last_query(); die;

        foreach ($query1->result() as $val) {
            $query1 = $this->DB2->query("select srvid from sht_plannasassoc where srvid='" . $plan_id . "' and nasid='" . $val->id . "'");
            if ($query1->num_rows() == 0) {
                $tabledata = array('srvid' => $plan_id, 'nasid' => $val->id);
                $this->DB2->insert('sht_plannasassoc', $tabledata);
            }
        }
    }

  

  
    
    public function add_system_serviceid($srvid)
    {
       $tabledata=array("srvid"=>$srvid,"disabledservice"=>0,"expiredservice"=>0,"usermacauth"=>0);
        $this->DB2->insert("sht_system",$tabledata);
        
    }
    
    public function disable_system_serviceid($srvid)
    {
         $tabledata=array("disabledservice"=>1);
          $this->DB2->update("sht_system", $tabledata, array('srvid' => $srvid));
    }
    
     public function enable_system_serviceid($srvid)
    {
         $tabledata=array("disabledservice"=>0);
          $this->DB2->update("sht_system", $tabledata, array('srvid' => $srvid));
    }
    
      public function get_tax($jsondata)
        {
          
         $isp_uid = $jsondata->isp_uid;
          $ispcond='';
         $ispcond=" and isp_uid='".$isp_uid."'";
              $data=array();
           $query=$this->db->query("select * from sht_tax where status=1 {$ispcond}");
         //  echo $this->db->last_query(); die;
           if($query->num_rows()>0)
           {
               $rowarr=$query->row_array();
               $data['tax']=$rowarr['tax'];
           }
           else
           {
               $data['tax']=0;
           }
           return $data;
        }
        
        
             public function add_aboutus($jsondata)
         {
               //  echo "<pre>"; print_R($jsondata); die;
              $postdata= $jsondata->postdata;
              
         $isp_uid = $jsondata->isp_uid;
               $tabledata=array("about_us"=>$postdata->about_us);
                  $this->db->update('sht_isp_detail', $tabledata,array("isp_uid"=>$isp_uid));
                    return 1;
         }
         
          public function add_services($jsondata)
         {
               $postdata=$jsondata->postdata;
              
         $isp_uid = $jsondata->isp_uid;
               $tabledata=array("services"=>$postdata->services);
                  $this->db->update('sht_isp_detail', $tabledata,array("isp_uid"=>$isp_uid));
                    return 1;
         }
        
     
}

?>
