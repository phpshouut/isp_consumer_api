<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DineoutModel extends CI_Model {
    private $baseUrl = "https://inrestodemos.com/api/v1/";
    private $apiKey = "ebfc5588fc593790c66ab78cb689c68c";
    private $db2;
    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2_publicwifi', TRUE);
    }
    
    public function getmenu_cateid($rest_uid, $menu_category){
        $cate_uid = '';
        $categoryQ = $this->db2->query("SELECT menucategory_uid FROM offline_restaurants_menu_category WHERE rest_uid='".$rest_uid."' AND menucategory_name='".$menu_category."'");
        if($categoryQ->num_rows() > 0){
            $rawdata = $categoryQ->row();
            $cate_uid = $rawdata->menucategory_uid;
        }
        return $cate_uid;
    }
    
    public function get_dineout_menu_list(){
        //$resta_query = $this->db2->query("SELECT rest_uid FROM offline_restaurants_list");
        //if($resta_query->num_rows() > 0){
            //foreach($resta_query->result() as $restobj){
                //$rest_uid = $restobj->rest_uid;
                $rest_uid = '589c78ba36df285728a2bd61';
                
                $menu_url = $this->baseUrl."getMenuItems?restId=".$rest_uid."&version=1&apiKey=".$this->apiKey;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $menu_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                $result = curl_exec($ch);
                $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
        
                if($statusCode==200){
                    $json_menudata = json_decode($result);
                    if($json_menudata->status == '1'){
                        
                        $rest_uid = $json_menudata->Menu->_id;
                        $menucategoryArr = $json_menudata->Menu->menuCategories;
                        foreach($menucategoryArr as $menucateobj){
                            $menucateid = $menucateobj->_id;
                            $menucate_name = $menucateobj->name;
                            $caterating = $menucateobj->rating;
                            $cateversion = $menucateobj->version;
                            
                            $categoryQ = $this->db2->query("SELECT id FROM offline_restaurants_menu_category WHERE rest_uid='".$rest_uid."' AND menucategory_uid='".$menucateid."'");
                            if($categoryQ->num_rows() > 0){
                                $this->db2->query("UPDATE offline_restaurants_menu_category SET menucategory_name='".$menucate_name."', menucategory_rating='".$caterating."', menucategory_version='".$cateversion."', updated_on = '".date('Y-m-d H:i:s')."' WHERE rest_uid='".$rest_uid."' AND menucategory_uid='".$menucateid."'");
                            }else{
                                $this->db2->query("INSERT INTO offline_restaurants_menu_category SET menucategory_name='".$menucate_name."', menucategory_rating='".$caterating."', menucategory_version='".$cateversion."', rest_uid='".$rest_uid."', menucategory_uid='".$menucateid."', added_on = '".date('Y-m-d H:i:s')."'");
                            }
                        }
                        
                        
                        sleep(1);
                        $menulistArr = $json_menudata->Menu->menu;
                        foreach($menulistArr as $menulistobj){
                            $menuid = $menulistobj->_id;
                            $menu_category = $menulistobj->categoryName;
                            $menucategory_uid = $this->getmenu_cateid($rest_uid, $menu_category);
                            
                            $menu_title = $menulistobj->title;
                            $menu_cost = $menulistobj->cost;
                            if(isset($menulistobj->quantity)){
                                $menu_qty = ($menulistobj->quantity === null) ? 0 : $menulistobj->quantity;
                            }else{
                                $menu_qty = 0;
                            }
                            $menu_desc = $menulistobj->description;
                            $menu_image = $menulistobj->image;
                            $menu_food_type = $menulistobj->food_type;
                            $menu_restuid = $menulistobj->restId;
                            $menu_version = $menulistobj->version;
                            //$menu_recommendation = $menulistobj->recommendation;
                            $menu_avail = $menulistobj->is_avail;
                            $menu_customization = $menulistobj->customization;
                            $menu_addons = $menulistobj->addons;
                            
                            $menulistQ = $this->db2->query("SELECT id FROM offline_restaurants_menu_list WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."'");
                            if($menulistQ->num_rows() > 0){
                                $this->db2->query("UPDATE offline_restaurants_menu_list SET menu_category='".$menucategory_uid."', menu_title='".$menu_title."', menu_cost='".$menu_cost."', menu_quantity='".$menu_qty."', menu_description='".$menu_desc."', menu_image='".$menu_image."', food_type='".$menu_food_type."', version='".$menu_version."', is_avail='".$menu_avail."', updated_on = '".date('Y-m-d H:i:s')."' WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."'");
                            }else{
                                $this->db2->query("INSERT INTO offline_restaurants_menu_list SET rest_uid='".$rest_uid."', menu_id='".$menuid."', menu_category='".$menucategory_uid."', menu_title='".$menu_title."', menu_cost='".$menu_cost."', menu_quantity='".$menu_qty."', menu_description='".$menu_desc."', menu_image='".$menu_image."', food_type='".$menu_food_type."', version='".$menu_version."', is_avail='".$menu_avail."', addons='0', customization='0' added_on = '".date('Y-m-d H:i:s')."'");
                            }
                            
                            if(count($menu_addons) > 0){
                                foreach($menu_addons as $addonobj){
                                    $addonid = $addonobj->_id;
                                    $menuitem = $addonobj->menuitem;
                                    $quantity = $addonobj->quantity;
                                    $cost = $addonobj->cost;
                                    $categoryName = $addonobj->categoryName;
                                    $menucategory_uid = $this->getmenu_cateid($rest_uid, $categoryName);
                                    
                                    $addonsMax = $addonobj->addonsMax;
                                    $addonsMin = ($addonobj->addonsMin === null) ? 1 : $addonobj->addonsMin;
                                    
                                    $menuaddonQ = $this->db2->query("SELECT id FROM offline_restaurants_menu_addons WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."' AND addon_id='".$addonid."'");
                                    if($menuaddonQ->num_rows() > 0){
                                        $this->db2->query("UPDATE offline_restaurants_menu_addons SET addon_menuitem='".$menuitem."', addon_quantity='".$quantity."', addon_cost='".$cost."', addon_menu_category='".$menucategory_uid."', addonsMax='".$addonsMax."', addonsMin='".$addonsMin."' WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."' AND addon_id='".$addonid."'");
                                    }else{
                                        $this->db2->query("INSERT INTO offline_restaurants_menu_addons SET addon_menuitem='".$menuitem."', addon_quantity='".$quantity."', addon_cost='".$cost."', addon_menu_category='".$menucategory_uid."', addonsMax='".$addonsMax."', addonsMin='".$addonsMin."', rest_uid='".$rest_uid."', menu_id='".$menuid."', addon_id='".$addonid."'");
                                    }
                                }
                                
                                $this->db2->query("UPDATE offline_restaurants_menu_list SET addons='1' WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."'");
                            }
                            
                            if(count($menu_customization) > 0){
                                foreach($menu_customization as $custmzobj){
                                    $custmzid = $custmzobj->_id;
                                    $isMandatory = $custmzobj->isMandatory;
                                    $itemType = $custmzobj->itemType->name;
                                    $custItems = $custmzobj->custItems;
                                    
                                    $menuaddonQ = $this->db2->query("SELECT id FROM offline_restaurants_menu_customization WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."' AND custom_id='".$custmzid."'");
                                    if($menuaddonQ->num_rows() > 0){
                                        
                                        if(count($custItems) > 0){
                                            foreach($custItems as $custitemobj){
                                                $menuitem = $custitemobj->menuitem;
                                                $quantity = $custitemobj->quantity;
                                                $cost = $custitemobj->cost;
                                                //$stock = $custitemobj->stock;
                                                
                                                $this->db2->query("UPDATE offline_restaurants_menu_customization SET custom_menuitem='".$menuitem."', custom_quantity='".$quantity."', custom_cost='".$cost."', isMandatory='".$isMandatory."', custom_itemtype='".$itemType."' WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."' AND custom_id='".$custmzid."'");
                                            }
                                        }
                                        
                                    }else{
                                        if(count($custItems) > 0){
                                            foreach($custItems as $custitemobj){
                                                $menuitem = $custitemobj->menuitem;
                                                $quantity = $custitemobj->quantity;
                                                $cost = $custitemobj->cost;
                                                //$stock = $custitemobj->stock;
                                                
                                                $this->db2->query("INSERT INTO offline_restaurants_menu_customization SET custom_menuitem='".$menuitem."', custom_quantity='".$quantity."', custom_cost='".$cost."', isMandatory='".$isMandatory."', custom_itemtype='".$itemType."', rest_uid='".$rest_uid."', menu_id='".$menuid."', custom_id='".$custmzid."'");
                                            }
                                        }
                                    }
                                }
                                $this->db2->query("UPDATE offline_restaurants_menu_list SET customization='1' WHERE rest_uid='".$rest_uid."' AND menu_id='".$menuid."'");
                            }
                        }
                        
                    }
                }
           // }
        //}
    }
    
}


?>