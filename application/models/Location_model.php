<?php
class Location_model extends CI_Model{
	// get total location in dashboard active and inactive
	public function location_dashboard(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."location_count";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function location_dashboard_search($search_val){
		
		
		$requestData = array(
			'search_val' => $search_val,
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."location_dashboard_search";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		$content .= '<div class="row">';
		$content .= '<div class="search-dropdown">
                                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px;">
                                                            <h4>'.$responce_data->totalrecord.' <small>Results</small></h4>
                                                         </div>
                                                         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right: 0px;">
                                                            <h4><span>
							       <a href="#" onclick="view_all()">View All<a>
                                                               </span>
                                                            </h4>
                                                         </div>
                                                      </div>';
		if($responce_data->totalrecord > 0){
			foreach($responce_data->locations as $record){
				$content .= '<a href="'.base_url().'location/edit_location?id='.$record->location_uid.'"><div class="search-dropdown">
                                                         <h5><i class="fa fa-user" aria-hidden="true"></i> &nbsp;'.$record->location_name.' <small> ('.$record->location_uid.')</small></h5>
                                                         <samp>'.$record->contact_person_name.', '.$record->mobile_number.', '.$record->city.'</samp>
                                                      </div><a>';
			}
		}
		
		$content .= '</div>';
		return  $content;
	}
	
	// get location list based on location type
	public function location_list($type, $state_id, $city_id,$search_type_filter){
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		else{
			$type = '';
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_type' => $type,
			'state' => $state_id,
			'city' => $city_id,
			'search_pattern' => $search_type_filter
		);
		$service_url = $this->api_url."location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}


	// get state list
	public function state_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function state_list_add_city($state_id = ''){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		//echo "<pre>";print_r($responce_data);die;
		$content = '<option value = "">Select State</option>';
		foreach($responce_data->state as $responce_data1){
			$sel = '';
			if($responce_data1->state_id == $state_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->state_id.'">'.$responce_data1->state_name.'</option>';
		}
		return $content;
	}
	// get city list
	public function city_list($state_id, $city_id = ''){
		$requestData = array(
			'state_id' => $state_id
		);
		$service_url = $this->api_url."city_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select City</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->city_id == $city_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->city_id.'">'.$responce_data1->city_name.'</option>';
		}
		return $content;
	}
	// get zone list
	public function zone_list($city_id, $zone_id = ''){
		$requestData = array(
			'city_id' => $city_id,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."zone_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select zone</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->zone_id == $zone_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->zone_id.'">'.$responce_data1->zone_name.'</option>';
		}
		return $content;
	}
	//get the plan listing based on type
	public function plan_list($plan_type, $plan_id = ''){
		$requestData = array(
			'plan_type' => $plan_type,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."plan_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option data-plan_type = "" value = ""> < New Plan > </option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->srvid == $plan_id){
				$sel = "selected";
			}
			$content .= '<option data-plan_type="'.$responce_data1->plan_type.'" '.$sel.' value = "'.$responce_data1->srvid.'">'.$responce_data1->srvname.'</option>';
		}
		return $content;
	}
	// add location view open time get autogenerated location id
	public function add_location_view(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."location_autogenerate_id";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// add or update location
	public function add_location(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'created_location_id' => $this->input->post("created_location_id"),
			'location_type' => $this->input->post("location_type"),
			'location_name' => $this->input->post("location_name"),
			'geo_address' => $this->input->post("geo_address"),
			'placeid' => $this->input->post("placeid"),
			'lat' => $this->input->post("lat"),
			'long' => $this->input->post("long"),
			'address1' => $this->input->post("address1"),
			'address2' => $this->input->post("address2"),
			'pin' => $this->input->post("pin"),
			'state' => $this->input->post("state"),
			'city' => $this->input->post("city"),
			'zone' => $this->input->post("zone"),
			'location_id' => $this->input->post("location_id"),
			'contact_person_name' => $this->input->post("contact_person_name"),
			'email_id' => $this->input->post("email_id"),
			'mobile_no' => $this->input->post("mobile_no"),
			'enable_channel' => $this->input->post("enable_channel"),
			'enable_nas_down_notification' => $this->input->post("enable_nas_down_notification"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	// add or update captive portal
	public function add_captive_portal(){
		$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/cp_brand_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/cp_brand_image/';
		$original="";
                        $small="";
                        $logo="";
			$image_name = $this->input->post("image_name");
			$image_src = $this->input->post("image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$requestData = array(
						'location_uid' => $this->input->post("location_uid"),
					);
					$service_url = $this->api_url."location_logo_name";
					$curl = curl_init($service_url);
					$requestData = $requestData;
					$data_request = json_encode($requestData);
					//print_r($data_request);die;
					$curl_post_data = array("requestData" => $data_request);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$curl_response = curl_exec($curl);
					curl_close($curl);
					$image_responce = json_decode($curl_response);
					if(isset($image_responce->original_name) && $image_responce->original_name != ''){
						$newname = $image_responce->original_name;
					}else{
						$newname=date("Ymdhisv").rand().".".$file_ext; 
					}
					rename($base_url.$filename,$base_url.'original/'.$newname);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 $config2['source_image'] = $base_url . 'original/'.$newname;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					$original=$newname;
					 $small="600_".$newname;
					 $logo="300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'isp_location_logo/original/'.$newname;
			$famazonname1 = AMAZONPATH.'isp_location_logo/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'isp_location_logo/small/600_'. $newname;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
			    unlink($fname);
			unlink($fname1);
			unlink($fname2);
					 
					
				}	
			}
			$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/cp_brand_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/retail_background_image/';
		$original_retail="";
                        $small_retail="";
                        $logo_retail="";
			$image_name = $this->input->post("background_image_name");
			$image_src = $this->input->post("background_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					
					rename($base_url.$filename,$base_url.'original/'.$newname);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 $config2['source_image'] = $base_url . 'original/'.$newname;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					$original_retail = $newname;
					 $small_retail = "600_".$newname;
					 $logo_retail = "300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
			$famazonname1 = AMAZONPATH.'retail_background_image/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'retail_background_image/small/600_'. $newname;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
			    unlink($fname);
			unlink($fname1);
			unlink($fname2);
					 
					
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider1="";
			$image_name = $this->input->post("slider1_image_name");
			$image_src = $this->input->post("slider1_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider1 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider2="";
			$image_name = $this->input->post("slider2_image_name");
			$image_src = $this->input->post("slider2_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider2 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
	    
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'cp_type' => $this->input->post("cp_type"),
			'cp1_plan' => $this->input->post("cp1_plan"),
			'cp1_no_of_daily_session' => $this->input->post("cp1_no_of_daily_session"),
			'cp2_plan' => $this->input->post("cp2_plan"),
			'cp2_signip_data' => $this->input->post("cp2_signip_data"),
			'cp2_daily_data' => $this->input->post("cp2_daily_data"),
			'cp3_hybrid_plan' => $this->input->post("cp3_hybrid_plan"),
			'cp3_data_plan' => $this->input->post("cp3_data_plan"),
			'cp3_signip_data' => $this->input->post("cp3_signip_data"),
			'cp3_daily_data' => $this->input->post("cp3_daily_data"),
			'image_original' => $original,
			'image_small' => $small,
			'image_logo' => $logo,
			'retail_image_original' => $original_retail,
			'retail_image_small' => $small_retail,
			'retail_image_logo' => $logo_retail,
			'is_otpdisabled' => $this->input->post("is_otpdisabled"),
			'main_ssid' => $this->input->post("main_ssid"),
			'icon_color' => $this->input->post("icon_color"),
			'original_slider1' => $original_slider1,
			'original_slider2' => $original_slider2,
			'login_with_mobile_email' => $this->input->post("login_with_mobile_email"),
			'is_socialloginenable' => $this->input->post("is_socialloginenable"),
			'slider_background_color' => $this->input->post("slider_background_color"),
			//'cp1_plan_type' => $this->input->post("cp1_plan_type"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_captive_portal";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function change_location_status(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."change_location_status";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// get location date for edit view
	public function edit_location_data($locationid){
		$requestData = array(
			'locationid' => $locationid,
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."edit_location_data";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		//print_r(json_decode($curl_response));die;
		return json_decode($curl_response);
	}
	//get the nas list which are not attached with location
	public function nas_list($router_type){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'router_type' => $router_type
		);
		$service_url = $this->api_url."nas_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select NAS</option>';
		foreach($responce_data as $responce_data1){
			
			$content .= '<option data-nasid="'.$responce_data1->nasname.'"  value = "'.$responce_data1->id.'">'.$responce_data1->shortname.'</option>';
		}
		return $content;
	}
	
	// get the isp url
	public function get_isp_url($location_id = '', $for_mikrotik = ''){
		if($location_id == ''){
			if(isset($this->session->userdata['client_ap_configuration']['location_id'])){
				$location_id = $this->session->userdata['client_ap_configuration']['location_id'];
			}
		}
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_id' => $location_id,
			'for_mikrotik' => $for_mikrotik
		);
		$service_url = $this->api_url."get_isp_url";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// get nes shortname and nasaddress
	public function get_nas_detail($nasid){
		$requestData = array(
			'nasid' => $nasid,
		);
		$service_url = $this->api_url."get_nas_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
		public function location_voucher_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."location_voucher_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			$i = 1;
			foreach($responce_data->vouchers as $responce_data1){
				$content .= '<tr>';
				$content .= '<td>'.$i.'</td>';
				$content .= '<td>'.$responce_data1->voucher_data.'&nbsp'.$responce_data1->voucher_data_type.' </td>';
				$content .= '<td>Rs. '.$responce_data1->voucher_cost.' </td>';
				$content .= '<td>Rs. '.$responce_data1->voucher_selling_price.' </td>';
				$content .= ' <td class="mui--text-right">
						<a href="#" onclick="edit_voucher('.$responce_data1->id.', '.$responce_data1->voucher_data.', \''.$responce_data1->voucher_data_type.'\','.$responce_data1->voucher_cost.','.$responce_data1->voucher_selling_price.')">Edit</a> &nbsp;&nbsp;&nbsp;&nbsp;
						<a href="#" onclick="delete_voucher('.$responce_data1->id.')">Delete</a>
							 </td>';
				$content .= '</tr>';
				$i++;
			}
		}
		return $content;
		
	}
	public function add_location_vouchers(){
		$voucher_id = '';
		if($this->input->post('voucher_id') && $this->input->post('voucher_id') != ''){
			$voucher_id = $this->input->post('voucher_id');
		}
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'voucher_id' => $voucher_id,
			'voucher_data' => $this->input->post('voucher_data'),
			'voucher_data_type' => $this->input->post('voucher_data_type'),
			'voucher_cost' => $this->input->post('voucher_cost'),
			'voucher_selling_price' => $this->input->post('voucher_selling_price')
		);
		$service_url = $this->api_url."add_location_vouchers";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
		
	}

	public function delete_voucher($voucher_id){
		$requestData = array(
			'voucher_id' => $voucher_id
		);
		$service_url = $this->api_url."delete_voucher";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function update_location_voucher_status($locationid){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
			'voucher_need' => $this->input->post("voucher_need"),
			'payment_gateway' => $this->input->post("payment_gateway"),
		);
		$service_url = $this->api_url."update_location_voucher_status";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
	}
	public function update_other_router_with_nas_location(){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
			'nasid' => $this->input->post("nasid"),
			'isp_uid' => $this->isp_uid,
			'router_type' => $this->input->post("router_type"),
		);
		$service_url = $this->api_url."update_other_router_with_nas_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		return $responce_data->resultCode;
	}
	public function nas_setup_list(){
		$location_type = $this->input->post('location_type');
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."nas_setup_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			/*$EnGenius = ($responce_data->nastype == '2')?"selected":"";
			$Cisco = ($responce_data->nastype == '3')?"selected":"";
			$Ubiquity = ($responce_data->nastype == '4')?"selected":"";*/
			/*<option value="2" '.$EnGenius.'>EnGenius</option>
							 <option value="3" '.$Cisco.'>Cisco</option>
							 <option value="4" '.$Ubiquity.'>Ubiquity</option>*/
			$Microtik = ($responce_data->nastype == '1')?"selected":"";
			$Cambium = ($responce_data->nastype == '5')?"selected":"";
			$one_hop = ($responce_data->nastype == '6')?"selected":"";
			$RASBERRY_PI = ($responce_data->nastype == '7')?"selected":"";
			$is_mikrotik = 'style="display: none"';
			$is_onehop = 'style="display: none"';
			if($responce_data->nastype == '1'){
				$is_mikrotik = 'style="display: inline-block"';
			}else{
				$is_onehop = 'style="display: block"';
			}
			$options = '';
			if($location_type == '10' || $location_type == '11' || $location_type == '12'){
				$options = '<option value="">Select</option> <option value="7" '.$RASBERRY_PI.'>RASBERRY PI</option>';
			}
			else{
				$options = '<option value="">Select</option>
							 <option value="1" '.$Microtik.'>Microtik</option>
							 <option value="5" '.$Cambium.'>Cambium</option>
							 <option value="6" '.$one_hop.'>One Hop</option>';
			}
			$content .='<div class="row"><div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">NAS Brand <sup>*</sup></label>
						<select class="form-control" name="router_type" id="router_type" required>
							'.$options.'
						</select>
					</div>
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">Select NAS <sup>*</sup></label>
						<select class="form-control" name="nas" id="nas" required>
							<option data-nasid="'.$responce_data->nasname.'" value="'.$responce_data->nasid.'">'.$responce_data->shortname.'</option>
						</select>
					</div>
					<div class="col-sm-6 col-xs-6 col-pt-30">
						<p id="nas_select_erroe" style="color:red"></p>
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value="microtic" id="microtic_button" '.$is_mikrotik.'>Setup Router</button>
						
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value = "else"  id="else_router" '.$is_onehop.'>Setup Router</button>
					</div>
				</div></div>';
				if($responce_data->nastype == '1'){
					$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>1. &#60; Router IP '.$responce_data->nasipaddress.' &#62;</span> Configured
							</label>
						</div>
						
					</div>
				</div>';
					
				}if($responce_data->nastype == '7'){
					$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>1. &#60; Router IP '.$responce_data->nasipaddress.' &#62;</span> Configured
							</label>
						</div>
						
					</div>
				</div>';
					
				}elseif($responce_data->nastype == '6'){
					$create_network_msg = 'Configure';
					if($responce_data->is_network_ssid_created == '1'){
						$create_network_msg = 'Re-Configure';
					}
					$content .= '<div class="row router_setup_detail_info" style="margin-top:20px;">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-router">
								<span>1. &#60; Network Name: '.$responce_data->network_name.' &#62;</span> Created
							</label>
						</div>
						<div class="col-sm-4 col-xs-4">
							<button type="button" class="mui-btn mui-btn--outline-router btn-sm" onclick="add_onehop_ssid_view()">'.$create_network_msg.'<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
						</div>
						
					</div>
				</div>';
				}
			
			$content .= '<div class="col-sm-12 col-xs-12 router_setup_detail_info" style="margin-top: 20px; margin-bottom:15px">
					<h4>ADD ACCESS POINT TO NETWORK</h4>
				</div>	';
			$content .= '<div class="row router_setup_detail_info" style="margin-top: 20px">
					<div class="col-sm-12 col-xs-12">
						<div class="col-sm-4 col-xs-4">
							<label class="so-lable-network">MAC ID <sup>*</sup></label>
							<input type="text" class="form-control" name="access_point_macid" id="access_point_macid">
							
						</div>
						<div class="col-sm-6 col-xs-6">
							<div class="row">
								<div class="col-sm-7 col-xs-7">
									<label class="so-lable-network">Select AP Location <sup>*</sup></label>
									<select class="form-control" id="add_ap_location" name="add_ap_location">
										<option value="">Select AP location</option>
									</select>	
								</div>
								<div class="col-sm-5 col-xs-5">
									<h5 class="so-add_headings" onclick="add_ap_location_model()">
										Add New Location
									</h5>	
								</div>
							</div>
						</div>
						<div class="col-sm-2 col-xs-2 col-pt-30" >
						
							<button type="button" onclick="add_location_access_point()" class="mui-btn mui-btn--outline-network btn-sm">Add<span class="mui-btn__ripple-container"><span class="mui-ripple"></span></span></button>
						</div>
					</div>
					<div class="col-sm-12 col-xs-12">
					<p style="color:red" id="add_access_point_errro"></p>
					</div>
				</div>';
			$content .= '	<div class="row router_setup_detail_info">
						<div class="col-sm-12 col-xs-12">
							<div class="table-responsive">
								<div class="col-sm-12 col-xs-12">
									<table class="table table-striped">
										<thead>
											<tr class="active">
												<th>S.NO</th>
												<th>MAC ID</th>
												<th>BRAND</th>
												<th class="text-center">ACTIONS</th>
												<th class="text-center">STATUS</th>
											</tr>
										</thead>
										<tbody id="access_point_list">
										</tbody>
									</table>
								</div>
							</div>
						</div>	
					</div>';
				
		}
		else{
			/*<option value="2">EnGenius</option>
							 <option value="3">Cisco</option>
							 <option value="4">Ubiquity</option>*/
			$options = '';
			if($location_type == '10' || $location_type == '11' || $location_type == '12'){
				$options = ' <option value="">Select</option><option value="7">RASBERRY PI</option>';
			}
			else{
				$options = '<option value="">Select</option>
							 <option value="1" >Microtik</option>
							 <option value="5">Cambium</option>
							 <option value="6">One Hop</option>';
			}
			$content .='<div class="row "><div class="col-sm-12 col-xs-12">
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">NAS Brand <sup>*</sup></label>
						<select class="form-control" name="router_type" id="router_type" required>
							'.$options.'
						</select>
					</div>
					<div class="col-sm-3 col-xs-3">
						<label class="so-lable-network">Select NAS <sup>*</sup></label>
						<select class="form-control" name="nas" id="nas" required>
							<option value="">Select</option>
						</select>
					</div>
					<div class="col-sm-6 col-xs-6 col-pt-30">
						<p id="nas_select_erroe" style="color:red"></p>
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value="microtic" id="microtic_button" style="display: none">Setup Router</button>
						<button type="submit" class="mui-btn mui-btn--outline-network btn-sm" value = "else"  id="else_router" style="display: none">Setup Router</button>
					</div>
				</div></div>';
		}
		return $content;
	}
	public function add_location_access_point(){
		$access_point_id = '';
		if($this->input->post('access_point_id') && $this->input->post('access_point_id') != ''){
			$access_point_id = $this->input->post('access_point_id');
		}
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'access_point_id' => $access_point_id,
			'access_point_ssid' => $this->input->post('access_point_ssid'),
			'access_point_hotspot_type' => $this->input->post('access_point_hotspot_type'),
			'access_point_macid' => $this->input->post('access_point_macid'),
			'access_point_ip' => $this->input->post('access_point_ip'),
			'ap_location_type' => $this->input->post('ap_location_type'),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_location_access_point";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		return $responce_data->resultMsg;
		//return $responce_data->resultCode;
		
	}

	
	public function location_access_point_list($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."location_access_point_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		//echo "<pre>"; print_R($responce_data); die;
		if($responce_data->resultCode == 1){
			$i = 1;
			foreach($responce_data->vouchers as $responce_data1){
				$content .= '<tr>';
				$content .= '<td><span>'.$i.'</span></td>';
				$content .= '<td>'.$responce_data1->macid.'</td>';
				$content .= '<td>'.$responce_data1->router_name.' </td>';
				//$content .= '<td>'.$responce_data1->ip.' </td>';
				/*$content .= '<td>
                                 <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                                    <label class="onoffswitch-label" for="myonoffswitch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                    </label>
                                 </div>
                              </td>';*/
				$content .= ' <td class="text-center">
						<a href="#" onclick="edit_access_point('.$responce_data1->ap_location_type.','.$responce_data1->id.', \''.$responce_data1->macid.'\' , \''.$responce_data1->hotspot_type.'\', \''.$responce_data1->ssid.'\', \''.$responce_data1->ip.'\')">Edit</a> &nbsp;&nbsp;
						';
						if(isset($responce_data1->router_type)){
							if($responce_data1->router_type == '6'){
								$content .= '<a class="mui-url_btn--os" onclick="move_access_point('.$responce_data1->ap_location_type.','.$responce_data1->id.', \''.$responce_data1->macid.'\' , \''.$responce_data1->hotspot_type.'\', \''.$responce_data1->router_type.'\')">Move</a>';
							}elseif($responce_data1->router_type == '7'){
								$content .= '<a class="mui-url_btn--os" onclick="move_access_point('.$responce_data1->ap_location_type.','.$responce_data1->id.', \''.$responce_data1->macid.'\' , \''.$responce_data1->hotspot_type.'\', \''.$responce_data1->router_type.'\')">Move</a>';
							}	
						}
				
						$content .='
							 </td>';
				$nsimg='<img src="'.base_url().'assets/images/loader.svg" width="15%">';
				
				$router_type = '';
				if(isset($responce_data1->router_type)){
					$router_type = $responce_data1->router_type;
					if($responce_data1->router_type != '6'){
						$nsimg='<img src="'.base_url().'assets/images/green.png">';
					}	
				}
				if($responce_data1->router_type == '7'){
					$nsimg.='&nbsp; <span class="viewras" style="cursor:pointer;" data-macid="'.$responce_data1->macid.'" data-routertype="'.$router_type.'" data-locuid="'.$responce_data1->location_uid.'"><i class="fa fa-eye"></i></span>';
				}
				$ipadd = $responce_data1->location_uid.'_'.$i;
				$content .= '<td class = " text-center loc cla_'.$ipadd.'" data-ipadd="'.$ipadd.'" data-raspstat="'.$responce_data1->raspberry_status.'"  data-macid="'.$responce_data1->macid.'" data-routertype="'.$router_type.'" data-locuid="'.$responce_data1->location_uid.'">'.$nsimg.'</td>';
				$content .= '</tr>';
				$i++;
				//<a href="#" onclick="delete_access_point('.$responce_data1->id.')">Delete</a>
			}
		}
		return $content;
		
	}
	


	public function delete_access_point($access_point_id){
		$requestData = array(
			'access_point_id' => $access_point_id
		);
		$service_url = $this->api_url."delete_access_point";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function add_zone(){
		$requestData = array(
			'city_id' => $this->input->post("city_id"),
			'zone_name' => $this->input->post("zone_name"),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_zone";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function add_city(){
		$requestData = array(
			'state_id' => $this->input->post("state_id"),
			'city_name' => $this->input->post("city_name"),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_city";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
		public function check_cp_selected_for_location($location_uid){
		$requestData = array(
			'location_uid' => $location_uid,
		);
		$service_url = $this->api_url."check_cp_selected_for_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
	}
		public function done_configuration(){
		
		$router_type = '';
		if(isset($this->session->userdata['client_ap_configuration']['router_type_wap'])){
			$router_type = $this->session->userdata['client_ap_configuration']['router_type_wap'];
		}
		
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$location_name = $this->session->userdata['client_ap_configuration']['username'];
		$location_id = $this->session->userdata['client_ap_configuration']['location_id'];
		$cp_path = $this->session->userdata['client_ap_configuration']['cp_path'];
		$subdomain_ip = '';
		if(isset($this->session->userdata['client_ap_configuration']['subdomain_ip'])){
			$subdomain_ip = $this->session->userdata['client_ap_configuration']['subdomain_ip'];
		}
		$secret = '';
		if(isset($this->session->userdata['client_ap_configuration']['secret'])){
			$secret = $this->session->userdata['client_ap_configuration']['secret'];
		}
		$main_ssid= '';
		if(isset($this->session->userdata['client_ap_configuration']['main_ssid'])){
			$main_ssid = $this->session->userdata['client_ap_configuration']['main_ssid'];
		}
		$guest_ssid = '';
		if(isset($this->session->userdata['client_ap_configuration']['guest_ssid'])){
			$guest_ssid = $this->session->userdata['client_ap_configuration']['guest_ssid'];
		}
		$is_dynamic = '';
		if(isset($this->session->userdata['client_ap_configuration']['is_dynamic'])){
			$is_dynamic = $this->session->userdata['client_ap_configuration']['is_dynamic'];
		}
		$requestData = array(
			'secret' => $secret,
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
			'location_name' => $location_name,
			'location_id' => $location_id,
			'cp_path' => $cp_path,
			'subdomain_ip' => $subdomain_ip,
			'isp_uid' => $this->isp_uid,
			'main_ssid' => $main_ssid,
			'guest_ssid' => $guest_ssid,
			'is_dynamic' => $is_dynamic,
		);
		$service_url = $this->api_url."configure_microtic_router";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$curl_response = '1';
		if($curl_response == '1'){
			$this->update_configure();
			
		}
		
		return $curl_response;
	}

	public function update_configure(){
		$location_id = $this->session->userdata['client_ap_configuration']['location_id'];
		$nasaddress = $this->session->userdata['client_ap_configuration']['nasname'];
		$nasid = $this->session->userdata['client_ap_configuration']['nasid'];
		$requestData = array(
			'location_id' => $location_id,
			'nasaddress' => $nasaddress,
			'nasid' => $nasid,
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."update_microtic_router_with_nas_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		
	}
	
	public function reset_router($router_type){
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$requestData = array(
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		);
		$service_url = $this->api_url."reset_router";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}
	public function check_router_selected_matched($router_type){
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$requestData = array(
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		);
		$service_url = $this->api_url."check_router_selected_matched";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}
	public function check_wap_router_reset_porperly($router_type){
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$requestData = array(
			'router_type' => $router_type,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		);
		$service_url = $this->api_url."check_wap_router_reset_porperly";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return $curl_response;
	}
	public function getExtension($str){
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }

	public function add_hotel_captive_portal(){
		$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/cp_brand_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/cp_brand_image/';
		$original="";
                        $small="";
                        $logo="";
			$image_name = $this->input->post("image_name");
			$image_src = $this->input->post("image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$requestData = array(
						'location_uid' => $this->input->post("location_uid"),
					);
					$service_url = $this->api_url."location_logo_name";
					$curl = curl_init($service_url);
					$requestData = $requestData;
					$data_request = json_encode($requestData);
					//print_r($data_request);die;
					$curl_post_data = array("requestData" => $data_request);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$curl_response = curl_exec($curl);
					curl_close($curl);
					$image_responce = json_decode($curl_response);
					if(isset($image_responce->original_name) && $image_responce->original_name != ''){
						$newname = $image_responce->original_name;
					}else{
						$newname=date("Ymdhisv").rand().".".$file_ext; 
					}
					rename($base_url.$filename,$base_url.'original/'.$newname);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 $config2['source_image'] = $base_url . 'original/'.$newname;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					$original=$newname;
					 $small="600_".$newname;
					 $logo="300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'isp_location_logo/original/'.$newname;
			$famazonname1 = AMAZONPATH.'isp_location_logo/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'isp_location_logo/small/600_'. $newname;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				
				unlink($fname);
			unlink($fname1);
			unlink($fname2);	 
					
				}	
			}
			$path = FCPATH;
			if($path != ''){
				$path = $path.'assets/retail_background_image';
				// change permission 
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
				if( $fldrperm != 0777) {
					$output = exec("sudo chmod -R 0777 \"$path\"");
				}
			}
		$base_url = 'assets/retail_background_image/';
		$original_retail="";
                        $small_retail="";
                        $logo_retail="";
			$image_name = $this->input->post("background_image_name");
			$image_src = $this->input->post("background_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					
					rename($base_url.$filename,$base_url.'original/'.$newname);
					if (!file_exists($base_url . 'logo/')) {
						mkdir($base_url . 'logo/', 0777, TRUE);
					}
					if (!file_exists($base_url . 'small/')) {
					    mkdir($base_url . 'small/', 0777, TRUE);
					}
					$this->load->library('image_lib');
					$config1['image_library'] = 'gd2';
					$config1['source_image'] = $base_url . 'original/'.$newname;
					$config1['new_image'] = $base_url . 'logo/'."300_".$newname;
                        
					$config1['maintain_ratio'] = TRUE;
					$config1['width'] = 300;
					$config1['height'] = 100;
					$this->image_lib->initialize($config1);
					$this->image_lib->resize();
                            
					$this->image_lib->clear();
					 $config2['image_library'] = 'gd2';
					 $config2['source_image'] = $base_url . 'original/'.$newname;
					 $config2['new_image'] = $base_url . 'small/'."600_".$newname;
                          
					$config2['maintain_ratio'] = TRUE;
					$config2['width'] = 600;
					$config2['height'] = 200;
					  $this->image_lib->initialize($config2);
					  $this->image_lib->resize();
                              
					$original_retail = $newname;
					 $small_retail = "600_".$newname;
					 $logo_retail = "300_".$newname;
					 
					 $fname = $base_url.'original/'.$newname;
                        $fname1 = $base_url . 'logo/'."300_".$newname;
                        $fname2 = $base_url . 'small/'."600_".$newname;
			   
			$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
			$famazonname1 = AMAZONPATH.'retail_background_image/logo/300_'. $newname;
			$famazonname2 = AMAZONPATH.'retail_background_image/small/600_'. $newname;
	  
                          
                          $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
                            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
                            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				
				unlink($fname);
			unlink($fname1);
			unlink($fname2);	 
					
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider1="";
			$image_name = $this->input->post("slider1_image_name");
			$image_src = $this->input->post("slider1_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider1 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
			
			$base_url = 'assets/retail_background_image/';
			$original_slider2="";
			$image_name = $this->input->post("slider2_image_name");
			$image_src = $this->input->post("slider2_image_src");
			if($image_name != ''){
				if($image_src != ''){
					if (!file_exists($base_url . 'original/')) {
						mkdir($base_url . 'original/', 0777, TRUE);
					}
					$file_ext = $this->getExtension($image_name);
					$explode = explode(',',$image_src);
					$image_src = $explode['1'];
					$image_src = str_replace(' ', '+', $image_src);
					$data_img = base64_decode($image_src);
					$filename = uniqid() . '.'.$file_ext;
					$file = $base_url . $filename;
					$success = file_put_contents($file, $data_img);
					$newname=date("Ymdhisv").rand().".".$file_ext; 
					rename($base_url.$filename,$base_url.'original/'.$newname);
					$original_slider2 = $newname;
					$fname = $base_url.'original/'.$newname;
					
					$famazonname = AMAZONPATH.'retail_background_image/original/'.$newname;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($fname);
				}	
			}
	    
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'cp_type' => $this->input->post("cp_type"),
			'cp_hotel_plan_type' => $this->input->post("cp_hotel_plan_type"),
			'cp_home_selected_plan_id' => $this->input->post("cp_home_selected_plan_id"),
			'image_original' => $original,
			'image_small' => $small,
			'image_logo' => $logo,
			'cp_enterprise_user_type' => $this->input->post("cp_enterprise_user_type"),
			'institutional_user_list' => $this->input->post("institutional_user_list"),
			'main_ssid' => $this->input->post("main_ssid"),
			'guest_ssid' => $this->input->post("guest_ssid"),
			'cp_cafe_plan_id' => $this->input->post("cp_cafe_plan_id"),
			'is_otpdisabled' => $this->input->post("is_otpdisabled"),
			'retail_image_original' => $original_retail,
			'retail_image_small' => $small_retail,
			'retail_image_logo' => $logo_retail,
			'num_of_session_per_day' => $this->input->post("num_of_session_per_day"),
			'icon_color' => $this->input->post("icon_color"),
			'is_socialloginenable' => $this->input->post("is_socialloginenable"),
			'is_pinenable' => $this->input->post("is_pinenable"),
			'is_wifidisabled' => $this->input->post("is_wifidisabled"),
			'login_with_mobile_email' => $this->input->post("login_with_mobile_email"),
			'offer_redemption_mode' => $this->input->post("offer_redemption_mode"),
			
			'custom_captive_url' => $this->input->post("custom_captive_url"),
			'custom_location_plan' => $this->input->post("custom_location_plan"),
			'custon_cp_signip_data' => $this->input->post("custon_cp_signip_data"),
			'custon_cp_daily_data' => $this->input->post("custon_cp_daily_data"),
			'custon_cp_no_of_daily_session' => $this->input->post("custon_cp_no_of_daily_session"),
			
			'original_slider1' => $original_slider1,
			'original_slider2' => $original_slider2,
			
			'synk_frequency' => $this->input->post("synk_frequency"),
			'offline_cp_type' => $this->input->post("offline_cp_type"),
			'offline_cp_source_folder_path' => $this->input->post("offline_cp_source_folder_path"),
			'offline_cp_landing_page_path' => $this->input->post("offline_cp_landing_page_path"),
			'is_offline_registration' => $this->input->post("is_offline_registration"),
			'cp_cafe_retail_plan_type' => $this->input->post("cp_cafe_retail_plan_type"),
			'contestifi_contest_header' => $this->input->post("contestifi_contest_header"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_hotel_captive_portal";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function ap_location_list($ap_location_id = ''){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."ap_location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		$content = '<option value = "">Select AP Location</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->ap_id == $ap_location_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->ap_id.'">'.$responce_data1->ap_location_name.'</option>';
		}
		return $content;
	}
	
	public function add_ap_location(){
		$requestData = array(
			'ap_location_name' => $this->input->post("ap_location_name"),
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."add_ap_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function is_location_proper_setup($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."is_location_proper_setup";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_get_network_detail($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."onehop_get_network_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_get_network_create(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'network_name' => trim($this->input->post('network_name')),
			'description' => $this->input->post('description'),
			'maxaps' => $this->input->post('maxaps'),
			'coa_status' => $this->input->post('coa_status'),
			'coa_ip' => $this->input->post('coa_ip'),
			'coa_secret' => $this->input->post('coa_secret'),
			'address' => $this->input->post('address'),
			'latitude' => $this->input->post('latitude'),
			'longitude' => $this->input->post('longitude'),
			'service_name' => $this->input->post('service_name'),
			'beacon_status' => $this->input->post('beacon_status'),
		);
		$service_url = $this->api_url."onehop_get_network_create";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_add_apmac(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'onehop_ap_mac' => $this->input->post('onehop_ap_mac'),
			'onehop_ap_name' => $this->input->post('onehop_ap_name'),
			
		);
		
		$service_url = $this->api_url."onehop_add_apmac";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_add_ssid(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'onehop_ssid_index' => $this->input->post('onehop_ssid_index'),
			'onehop_ssid_name' => $this->input->post('onehop_ssid_name'),
			'onehop_association' => $this->input->post('onehop_association'),
			'onehop_wap_mode' => $this->input->post('onehop_wap_mode'),
			'onehop_forwarding' => $this->input->post('onehop_forwarding'),
			'onehop_cp_mode' => $this->input->post('onehop_cp_mode'),
			'onehop_cp_url' => $this->input->post('onehop_cp_url'),
			'onehop_auth_server_ip' => $this->input->post('onehop_auth_server_ip'),
			'onehop_auth_server_port' => $this->input->post('onehop_auth_server_port'),
			'onehop_auth_server_secret' => $this->input->post('onehop_auth_server_secret'),
			'onehop_accounting' => $this->input->post('onehop_accounting'),
			'onehop_acc_server_ip' => $this->input->post('onehop_acc_server_ip'),
			'onehop_acc_server_port' => $this->input->post('onehop_acc_server_port'),
			'onehop_acc_server_secret' => $this->input->post('onehop_acc_server_secret'),
			'onehop_acc_interval' => $this->input->post('onehop_acc_interval'),
			'onehop_psk' => $this->input->post('onehop_psk'),
			'onehop_wallgarden_ip_list' => $this->input->post('onehop_wallgarden_ip_list'),
			'onehop_wallgarden_domain_list' => $this->input->post('onehop_wallgarden_domain_list'),
		);
		
		$service_url = $this->api_url."onehop_add_ssid";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_get_ssid_detail(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'index_number' => $this->input->post('index_number'),
			
		);
		
		$service_url = $this->api_url."onehop_get_ssid_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function onehop_delete_ssid(){
		$requestData = array(
			'location_uid' => $this->input->post('location_uid'),
			'onehop_ssid_index' => $this->input->post('onehop_ssid_index'),
			'onehop_ssid_name' => $this->input->post('onehop_ssid_name'),
		);
		
		$service_url = $this->api_url."onehop_delete_ssid";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function plan_detail(){
		$requestData = array(
			'plan_id' => $this->input->post('plan_id'),
			
		);
		
		$service_url = $this->api_url."plan_detail";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function check_plan_already_attached($plan_id){
		$requestData = array(
			'plan_id' => $plan_id
		);
		$service_url = $this->api_url."check_plan_already_attached";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function save_plan(){
		$requestData = array(
			'plan_id' => $this->input->post("plan_id"),
			'data_limit' => $this->input->post("data_limit"),
			'time_limit' => $this->input->post("time_limit"),
			'plan_name' => $this->input->post("plan_name"),
			'plan_desc' => $this->input->post("plan_desc"),
			'downrate' => $this->input->post("downrate"),
			'uprate' => $this->input->post("uprate"),
			'plan_type' => $this->input->post("plan_type"),
			'isp_uid' => $this->isp_uid,
		);
		$service_url = $this->api_url."save_plan";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function terms_of_use($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."terms_of_use";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->content;
		
	}
	public function update_terms_of_use($location_uid,$text_value){
		$requestData = array(
			'location_uid' => $location_uid,
			'text_value' => $text_value
		);
		$service_url = $this->api_url."update_terms_of_use";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->result_msg;
		
	}
	public function current_image_path($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."current_image_path";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function current_cp_path($location_uid){
		$requestData = array(
			'location_uid' => $location_uid
		);
		$service_url = $this->api_url."current_cp_path";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function get_location_ap_move_where($router_type){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'router_type' => $this->input->post("router_type"),
		);
		$service_url = $this->api_url."get_location_ap_move_where";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select Location</option>';
		foreach($responce_data as $responce_data1){
			
			$content .= '<option  value = "'.$responce_data1->location_uid.'">'.$responce_data1->location_name.'</option>';
		}
		return $content;
	}
	public function onehop_move_ap($onehop_move_macid,$onehop_move_location_uid){
		$requestData = array(
			'onehop_move_macid' => $onehop_move_macid,
			'onehop_move_location_uid' => $onehop_move_location_uid,
			'router_type' => $this->input->post("router_type"),
		);
		$service_url = $this->api_url."onehop_move_ap";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function ping($host){
		exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host)), $res, $rval);
		return $rval;
	}
	public function ping_mikrotik_ip_address($ip_address){
            $up = $this->ping($ip_address);
	    $last_sceen = date('M d H:i Y',strtotime(date('Y-m-d H:i:s')));
                if($up == '0'){//0 means server is up
			$update = $this->db->query("update wifi_location set is_location_online = '1',last_online_time = '$last_sceen' where nasipaddress = '$ip_address'");
                    return 1;
                }else{
			$update = $this->db->query("update wifi_location set is_location_online = '0' where nasipaddress = '$ip_address'");
                    return 0;
                }
	}
	public function ping_onehop_ap($location_uid){
		$requestData = array(
			'location_uid' => $location_uid,
		);
		$service_url = $this->api_url."ping_onehop_ap";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function ping_onehop_ap_wise($location_uid,$macid){
		$requestData = array(
			'location_uid' => $location_uid,
			'macid' => $macid,
		);
		$service_url = $this->api_url."ping_onehop_ap_wise";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function remove_slider_image($location_uid,$slider_type){
		$requestData = array(
			'location_uid' => $location_uid,
			'slider_type' => $slider_type,
		);
		$service_url = $this->api_url."remove_slider_image";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_wifi_location(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."delete_wifi_location";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	
	
	
	public function content_builder_list(){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
		);
		$service_url = $this->api_url."content_builder_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_builder(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'offline_content_title' => $this->input->post("offline_content_title"),
			'offline_content_desc' => $this->input->post("offline_content_desc"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_builder";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_main_category(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$icon = '';
		
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$icon = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'main_category_name' => $this->input->post("main_category_name"),
			'main_category_desc' => $this->input->post("main_category_desc"),
			'main_category_id' => $this->input->post("main_category_id"),
			'offline_content_title' => $this->input->post("offline_content_title"),
			'offline_content_desc' => $this->input->post("offline_content_desc"),
			'icon' => $icon,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_main_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_sub_category(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$icon = '';
		
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$icon = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'sub_category_name' => $this->input->post("sub_category_name"),
			'sub_category_desc' => $this->input->post("sub_category_desc"),
			'category_id' => $this->input->post("category_id"),
			'sub_category_id' => $this->input->post("sub_category_id"),
			'icon' => $icon,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_sub_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'text';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			$mime = $_FILES['file']['type'];
			if(strstr($mime, "video/")){
				$file_type = "video";
			}else if(strstr($mime, "image/")){
				$file_type = "image";
			}else if(strstr($mime, "audio/")){
				$file_type = "audio";
			}
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_desc' => $this->input->post("content_desc",true),
			'content_file' => $content_file,
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function delete_content($content_id){
		$requestData = array(
			'content_id' => $content_id
		);
		$service_url = $this->api_url."delete_content";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_sub_category($sub_category_id){
		$requestData = array(
			'sub_category_id' => $sub_category_id
		);
		$service_url = $this->api_url."delete_sub_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_main_category($main_category_id){
		$requestData = array(
			'main_category_id' => $main_category_id
		);
		$service_url = $this->api_url."delete_main_category";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function get_content_poll_survey(){
		$requestData = array(
			'content_id' => $this->input->post("content_id"),
		);
		$service_url = $this->api_url."get_content_poll_survey";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_poll(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'poll';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'option_five' => $this->input->post("option_five"),
			'option_six' => $this->input->post("option_six"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_poll";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_content_poll(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'poll';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'content_id' => $this->input->post("content_id"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'option_five' => $this->input->post("option_five"),
			'option_six' => $this->input->post("option_six"),
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_content_poll";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_content_survey(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'survey';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'questions' => json_decode($this->input->post("questions")),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_survey";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_content_survey(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'survey';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'content_id' => $this->input->post("content_id"),
			'content_title' => $this->input->post("content_title"),
			'content_file' => $content_file,
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
			'questions' => json_decode($this->input->post("questions")),
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_content_survey";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_content_pdf(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = 'pdf';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
            
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_desc' => $this->input->post("content_desc",true),
			'content_file' => $content_file,
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_content_upload(){
		
		$file_type = 'upload';
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'content_desc' => $this->input->post("content_desc",true),
			'content_file' => '',
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			'file_previous_name' => $this->input->post("file_previous_name"),
			'upload_type' => $this->input->post("upload_type"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_upload";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function offline_upload_on_s3($base_url,$newname){
		$fname = $base_url.$newname;
		$famazonname = AMAZONPATH.'offline_video_image_content/'.$newname;
		$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
		$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
		//unlink($fname);
	}

	
	public function get_onehop_existing_network(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_type' => $this->input->post('location_type'),
		);
		$service_url = $this->api_url."get_onehop_existing_network";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select Network</option>';
		foreach($responce_data as $responce_data1){
			
			$content .= '<option  value = "'.$responce_data1->id.'">'.$responce_data1->network_name.'</option>';
		}
		return $content;
	}
	
	
	public function use_existing_onehop_network(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'previous_network_id' => $this->input->post('previous_network_id'),
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."use_existing_onehop_network";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_content_wiki(){
		
		$file_type = 'wiki';
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'category_id' => $this->input->post("category_id"),
			'is_main_category' => $this->input->post("is_main_category"),
			'content_title' => $this->input->post("content_title"),
			'wikipedia_title_url' => $this->input->post("wikipedia_title_url"),
			'wikipedia_title' => $this->input->post("wikipedia_title"),
			'wikipedia_pageid' => $this->input->post("wikipedia_pageid"),
			'wikipedia_content' => $this->input->post("wikipedia_content"),
			'content_id' => $this->input->post("content_id"),
			'file_type' => $file_type,
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_content_wiki";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function raspberry_ping_sync_info(){
		
		
		$requestData = array(
			'macid' => $this->input->post("locmacid"),
			'location_uid' => $this->input->post("locuid")
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."raspberry_ping_sync_info";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responsedata=json_decode($curl_response);
		//echo "<pre>"; print_R($responsedata); die;
		$data=array();
		$pinghtml=' <tr class="table_active">
                                 <th>S.No</th>
                                 <th>Location Macid</th>
                                 <th>Pinged On</th>
                                 <th>Status</th>
                              </tr>';
		if(count($responsedata->pingdata>0)){
			$i=1;
			
			foreach($responsedata->pingdata as $pingval){
				$pinghtml.='<tr><td>'.$i.'</td><td>'.$pingval->loc_macid.'</td><td>'.$pingval->pinged_on.'</td><td>'.$pingval->status.'</td></tr>';
				$i++;
			}
		}else{
			$pinghtml.="<tr><td>No data Found!</td></tr>";
		}
		$synchtml=' <tr class="table_active">
									 <th>S.No</th>
									 <th>Version</th>
									 <th>Synced On</th>
									 <th>Payload Size</th>
									 <th>Payload Recived</th>
									 <th>Percent Completed</th>
									 <th>Status</th>
								  </tr>';
			if(count($responsedata->syncdata>0)){
			$i=1;
			
			foreach($responsedata->syncdata as $syncval){
				$synchtml.='<tr><td>'.$i.'</td><td>'.$syncval->version.'</td><td>'.$syncval->last_communicated.'</td><td>'.$syncval->payload_size.'</td><td>'.$syncval->payload_receive.'</td><td>'.$syncval->percent.'</td><td>'.$syncval->status.'</td></tr>';
				$i++;
			}
		}else{
			$synchtml.="<tr><td>No data Found!</td></tr>";
		}
		
		$data['pinghtml']=$pinghtml;
		$data['synchtml']=$synchtml;
		
		return $data;
	}
	
	public function location_list_new($type, $state, $city,$serach_pattern,$limit,$offset,$online_filter){
		$data = array();
		$type_return = $type;
		$total_record = 0;
		$gen = '';
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		else{
			$type = '';
		}
		$isp_uid = $this->isp_uid;
		$location_type = $type;
		$locations = array();
		$online_offline_filter = '';
		if($online_filter != ''){
			$online_offline_filter = "AND is_location_online=".$online_filter."";
		}
		$location_type_where = '';
		if($location_type != ''){
			$location_type_where = "AND location_type=".$location_type."";
		}
		$state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		$serach_pattern_where = '';
		if($serach_pattern!= ''){
			$serach_pattern = strtolower($serach_pattern);
		    $serach_pattern_where = " AND (wl.state LIKE '%$serach_pattern%' OR wl.city LIKE '%$serach_pattern%' OR wl.location_name LIKE '%$serach_pattern%' OR wl.location_uid LIKE '%$serach_pattern%' OR wl.contact_person_name LIKE '%$serach_pattern%' OR wl.mobile_number LIKE '%$serach_pattern%' OR wl.pin LIKE '%$serach_pattern%'  OR wlap.macid LIKE '%$serach_pattern%')";
		}
		$query = $this->db->query("select wl.* from wifi_location as wl left join wifi_location_access_point as wlap on (wl.location_uid = wlap.location_uid) where wl.is_deleted = '0' and wl.isp_uid = '$isp_uid' $location_type_where $state_where $city_where $serach_pattern_where $online_offline_filter group by wl.location_uid order by wl.location_name LIMIT $offset,$limit");
		if($query->num_rows() > 0){
			$location_ids = array();
			foreach($query->result() as $locrow){
				$location_ids[] = $locrow->location_uid;
			}
			$location_ids = '"'.implode('", "', $location_ids).'"';
			// get devide model
			$device_model = array();
			$get_device = $this->db->query("select location_uid, device_model_name, router_type from wifi_location_access_point where location_uid IN ($location_ids)");
			if($get_device->num_rows() > 0){
			     foreach($get_device->result() as $de_row){
				  if(array_key_exists($de_row->location_uid, $device_model)){
				       $device_model[$de_row->location_uid]['device_model_name'] = $device_model[$de_row->location_uid]['device_model_name'].', '.$de_row->device_model_name;
				  }else{
				       $device_model[$de_row->location_uid]['router_type'] = $de_row->router_type;
				       $device_model[$de_row->location_uid]['device_model_name'] = $de_row->device_model_name;
				  }
				  
			     }
			}
			$i = $offset+1;
			
			foreach($query->result() as $row){
				$is_location_online = $row->is_location_online;
				$nsimg = '<img src = "'.base_url().'assets/images/red.png">';
				if($is_location_online == '1'){
					$nsimg = '<img src = "'.base_url().'assets/images/green.png">';
				}
				$device_name = '';
				$location_uid = $row->location_uid;
				if(isset($device_model[$location_uid])){
					if($device_model[$location_uid]['router_type'] == 6){
					     $device_name = $device_model[$location_uid]['device_model_name'];
					}
					else if($device_model[$location_uid]['router_type'] == 7){
					     $device_name = 'RASBERRY PI';
					}
				}
				$gen .= "<tr>";
				$gen .= "<td>".$i."</td>";
				$gen .= '<td><input type="checkbox" name="location_group[]" value="'.$row->location_uid.'"></td>';
				$gen .= '<td><a href="'.base_url().'location/edit_location?id='.$row->location_uid.'">'.$row->location_uid.'</a></td>';
				$gen .= '<td class="col-sm-2 col-xs-2">'.addslashes($row->location_name).'</td>';
				$gen .= '<td>'.addslashes($row->state).'</td>';
				$gen .= '<td>'.addslashes($row->city).'</td>';
				$gen .= '<td>'.addslashes($row->contact_person_name).'</td>';
				
				$gen .= '<td>'.$row->nasipaddress.'</td>';
				$gen .= '<td>'.$row->last_online_time.'</td>';
				$gen .= ' <td class="col-sm-1 col-xs-1">';
					$gen .= '<span class ="table_loader cla_'.$row->location_uid.'" >'.$nsimg.'</span>';
					$gen .= '<span style="cursor: pointer" class = "loc_click" data-location_type="'.$row->location_type.'" data-locuid="'.$row->location_uid.'" data-nasip="'.$row->nasipaddress.'"><i class="fa fa-refresh" aria-hidden="true"></i></span>';
				$gen .= '</td>';
				$gen .= '<td class="col-sm-2 col-xs-2">'.$device_name.'</td>';
				$gen .= '<td id="location_'.$row->location_uid.'" onclick="delete_wifi_location('.$row->location_uid.')">';
					$gen .= '<i class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer"></i>';
				$gen .='</td>';
				$gen .= "</tr>";
				$i++;
				$total_record++;
			}
			$loadmore = 1;
			
		}else{
			$gen .= '<tr><td colspan="11" style="text-align:center">No More Result Found !!</td></tr>';
			$loadmore = 0;
		}
		$data['search_results'] = $gen;
		$data['location_type'] = $type_return;
		$data['limit'] = $limit;
		$data['offset'] = $limit+$offset;
		$data['loadmore'] = $loadmore;
		$data['total_record'] = $total_record;
		return $data;
	}
	
	public function location_dashboard_new(){
		
		
		$isp_uid = $this->isp_uid;
		//$isp_uid = '1111';
		$data = array();
		$public_active = 0;
		$public_inactive = 0;
		$hotel_active = 0;
		$hotel_inactive = 0;
		$hospital_active = 0;
		$hospital_inactive = 0;
		$institutional_active = 0;
		$institutional_inactive = 0;
		$enterprise_active = 0;
		$enterprise_inactive = 0;
		$caffe_active = 0;
		$caffe_inactive = 0;
		$retail_active = 0;
		$retail_inactive = 0;
		$custom_active = 0;
		$custom_inactive = 0;
		$purple_active = 0;
		$purple_inactive = 0;
		$offline_active = 0;
		$offline_inactive = 0;
		$visitor_active = 0;
		$visitor_inactive = 0;
		$contestifi_active = 0;
		$contestifi_inactive = 0;
		//echo "<pre>";print_r($this->session->userdata('isp_session'));die;
		$query = $this->db->query("select is_location_online , location_type from wifi_location where is_deleted = '0' and isp_uid = '$isp_uid'");
		foreach($query->result() as $row){
			if($row->location_type == '1'){
				//public location
				if($row->is_location_online == '1'){
					$public_active = $public_active+1;
				}else{
					$public_inactive = $public_inactive+1;
				}
			}
			elseif($row->location_type == '2'){
				//sme location
				if($row->is_location_online == '1'){
					$hotel_active = $hotel_active+1;
				}else{
					$hotel_inactive = $hotel_inactive+1;
				}
			}
			elseif($row->location_type == '3'){
				//sme location
				if($row->is_location_online == '1'){
					$hospital_active = $hospital_active+1;
				}else{
					$hospital_inactive = $hospital_inactive+1;
				}
			}
			elseif($row->location_type == '4'){
				//sme location
				if($row->is_location_online == '1'){
					$institutional_active = $institutional_active+1;
				}else{
					$institutional_inactive = $institutional_inactive+1;
				}
			}
			elseif($row->location_type == '5'){
				//sme location
				if($row->is_location_online == '1'){
					$enterprise_active = $enterprise_active+1;
				}else{
					$enterprise_inactive = $enterprise_inactive+1;
				}
			}elseif($row->location_type == '6'){
				//sme location
				if($row->is_location_online == '1'){
					$caffe_active = $caffe_active+1;
				}else{
					$caffe_inactive = $caffe_inactive+1;
				}
			}
			elseif($row->location_type == '7'){
				//sme location
				if($row->is_location_online == '1'){
					$retail_active = $retail_active+1;
				}else{
					$retail_inactive = $retail_inactive+1;
				}
			}
			elseif($row->location_type == '8'){
				//sme location
				if($row->is_location_online == '1'){
					$custom_active = $custom_active+1;
				}else{
					$custom_inactive = $custom_inactive+1;
				}
			}
			elseif($row->location_type == '9'){
				//sme location
				if($row->is_location_online == '1'){
					$purple_active = $purple_active+1;
				}else{
					$purple_inactive = $purple_inactive+1;
				}
			}
			 elseif($row->location_type == '10'){
				//sme location
				if($row->is_location_online == '1'){
					$offline_active = $offline_active+1;
				}else{
					$offline_inactive = $offline_inactive+1;
				}
			 }
			elseif($row->location_type == '11'){
				//sme location
				if($row->is_location_online == '1'){
					$visitor_active = $visitor_active+1;
				}else{
					$visitor_inactive = $visitor_inactive+1;
				}
			}
			elseif($row->location_type == '12'){
				//sme location
				if($row->is_location_online == '1'){
					$contestifi_active = $contestifi_active+1;
				}else{
					$contestifi_inactive = $contestifi_inactive+1;
				}
			}
		}
		$data['public_active'] = $public_active;
		$data['public_inactive'] = $public_inactive;
		$data['hotel_active'] = $hotel_active;
		$data['hotel_inactive'] = $hotel_inactive;
		$data['hospital_active'] = $hospital_active;
		$data['hospital_inactive'] = $hospital_inactive;
		$data['institutional_active'] = $institutional_active;
		$data['institutional_inactive'] = $institutional_inactive;
		$data['enterprise_active'] = $enterprise_active;
		$data['enterprise_inactive'] = $enterprise_inactive;
		$data['caffe_active'] = $caffe_active;
		$data['caffe_inactive'] = $caffe_inactive;
		$data['retail_active'] = $retail_active;
		$data['retail_inactive'] = $retail_inactive;
		$data['custom_active'] = $custom_active;
		$data['custom_inactive'] = $custom_inactive;
		$data['purple_active'] = $purple_active;
		$data['purple_inactive'] = $purple_inactive;
		$data['offline_active'] = $offline_active;
		$data['offline_inactive'] = $offline_inactive;
		$data['visitor_active'] = $visitor_active;
		$data['visitor_inactive'] = $visitor_inactive;
		$data['contestifi_active'] = $contestifi_active;
		$data['contestifi_inactive'] = $contestifi_inactive;
		$data['total_location'] = $public_active+$public_inactive+$hotel_active+$hotel_inactive+$hospital_active+$hospital_inactive+$institutional_active+$institutional_inactive+$enterprise_active+$enterprise_inactive+$caffe_active+$caffe_inactive+$retail_active+$retail_inactive+$custom_active+$custom_inactive+$purple_active+$purple_inactive+$offline_active+$offline_inactive+$visitor_active+$visitor_inactive+$contestifi_active+$contestifi_inactive;
		//echo "<pre>";print_r($data);die;
		return json_encode($data);
	}
	public function location_dashboard_new_listview($type, $state, $city,$serach_pattern){
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		elseif($type == 'visitor'){
			$type = '11';
		}
		elseif($type == 'contestifi'){
			$type = '12';
		}
		else{
			$type = '';
		}
		$location_type = $type;
		$isp_uid = $this->isp_uid;
		//$isp_uid = '1111';
		$data = array();
		$active = 0;
		$inactive = 0;
		$location_type_where = '';
		if($location_type != ''){
			$location_type_where = "AND location_type=".$location_type."";
		}
		$state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		$serach_pattern_where = '';
		if($serach_pattern!= ''){
			$serach_pattern = strtolower($serach_pattern);
		    $serach_pattern_where = " AND (wl.state LIKE '%$serach_pattern%' OR wl.city LIKE '%$serach_pattern%' OR wl.location_name LIKE '%$serach_pattern%' OR wl.location_uid LIKE '%$serach_pattern%' OR wl.contact_person_name LIKE '%$serach_pattern%' OR wl.mobile_number LIKE '%$serach_pattern%' OR wl.pin LIKE '%$serach_pattern%' OR wlap.macid LIKE '%$serach_pattern%')";
		}
		//echo "<pre>";print_r($this->session->userdata('isp_session'));die;
		$query = $this->db->query("select wl.is_location_online , wl.location_type from wifi_location as wl left join wifi_location_access_point as wlap on (wl.location_uid = wlap.location_uid) where wl.is_deleted = '0' and wl.isp_uid = '$isp_uid' $location_type_where $state_where $city_where $serach_pattern_where");
		foreach($query->result() as $row){
				if($row->is_location_online == '1'){
					$active = $active+1;
				}else{
					$inactive = $inactive+1;
				}
			
			
		}
		$data['active'] = $active;
		$data['inactive'] = $inactive;
		
		$data['total_location'] = $active+$inactive;
		//echo "<pre>";print_r($data);die;
		return json_encode($data);
	}
	
	
	public function state_list_for_filter(){
		$requestData = array(
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."state_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	// get city list
	public function city_list_for_filter($state_id, $city_id = ''){
		$requestData = array(
			'state_id' => $state_id,
			'isp_uid' => $this->isp_uid
		);
		$service_url = $this->api_url."city_list_for_filter";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '<option value = "">Select City</option>';
		foreach($responce_data as $responce_data1){
			$sel = '';
			if($responce_data1->city_id == $city_id){
				$sel = "selected";
			}
			$content .= '<option '.$sel.' value = "'.$responce_data1->city_id.'">'.$responce_data1->city_name.'</option>';
		}
		return $content;
	}


	

	
	

	public function location_list_export_to_excel($type, $state, $city,$serach_pattern,$online_filter){
		$data = array();
		$type_return = $type;
		$total_record = 0;
		$gen = '';
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		else{
			$type = '';
		}
		$isp_uid = $this->isp_uid;
		$location_type = $type;
		$locations = array();
		$online_offline_filter = '';
		if($online_filter != ''){
			$online_offline_filter = "AND is_location_online=".$online_filter."";
		}
		$location_type_where = '';
		if($location_type != ''){
			$location_type_where = "AND location_type=".$location_type."";
		}
		$state_where = '';
		if($state != ''){
		    $state_where = "AND state_id=".$state."";
		}
		$city_where = '';
		if($city != ''){
		    $city_where = "AND city_id=".$city."";
		}
		$serach_pattern_where = '';
		if($serach_pattern!= ''){
		    $serach_pattern_where = " AND (state LIKE '%$serach_pattern%' OR city LIKE '%$serach_pattern%' OR location_name LIKE '%$serach_pattern%' OR location_uid LIKE '%$serach_pattern%' OR contact_person_name LIKE '%$serach_pattern%' OR mobile_number LIKE '%$serach_pattern%')";
		}
		
		$query = $this->db->query("select * from wifi_location where is_deleted = '0' and isp_uid = '$isp_uid' $location_type_where $state_where $city_where $serach_pattern_where $online_offline_filter order by location_name");
		$record_found = 0;
		if($query->num_rows() > 0){
			$record_found = 1;
			$location_ids = array();
			foreach($query->result() as $locrow){
				$location_ids[] = $locrow->location_uid;
			}
			$location_ids = '"'.implode('", "', $location_ids).'"';
			// get devide model
			$device_model = array();
			$get_device = $this->db->query("select location_uid, device_model_name, router_type from wifi_location_access_point where location_uid IN ($location_ids)");
			if($get_device->num_rows() > 0){
			     foreach($get_device->result() as $de_row){
				  if(array_key_exists($de_row->location_uid, $device_model)){
				       $device_model[$de_row->location_uid]['device_model_name'] = $device_model[$de_row->location_uid]['device_model_name'].', '.$de_row->device_model_name;
				  }else{
				       $device_model[$de_row->location_uid]['router_type'] = $de_row->router_type;
				       $device_model[$de_row->location_uid]['device_model_name'] = $de_row->device_model_name;
				  }
				  
			     }
			}
			
			$this->load->library('excel');
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex(0);
				$styleArray = array(
					'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'name'  => 'Tahoma',
					'size' => 14
				));
				//Set sheet style
				$styleArray1 = array(
					'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'name'  => 'Tahoma',
					'size' => 12
				));
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
				
				$objPHPExcel->getActiveSheet()->setTitle("Locations");
				
				//Set sheet columns Heading
				$objPHPExcel->getActiveSheet()->setCellValue('A1','S.No');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Location ID');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Location');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('D1','State');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('E1','City');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Contect Person Name');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('G1','Nas IP');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('H1','Last Active');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('I1','Status');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('J1','Device Model');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray);
				$i = 1;
				$j = 2;// excel row number
			
			
			foreach($query->result() as $row){
				$is_location_online = $row->is_location_online;
				$nsimg = 'Offline';
				if($is_location_online == '1'){
					$nsimg = 'Online';
				}
				$device_name = '';
				$location_uid = $row->location_uid;
				if(isset($device_model[$location_uid])){
					if($device_model[$location_uid]['router_type'] == 6){
					     $device_name = $device_model[$location_uid]['device_model_name'];
					}
					else if($device_model[$location_uid]['router_type'] == 7){
					     $device_name = 'RASBERRY PI';
					}
				}
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$i);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_uid);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->location_name);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->state);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$row->city);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$row->contact_person_name);
                $objPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$row->nasipaddress);
                $objPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$row->last_online_time);
                $objPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$nsimg);
                $objPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$device_name);
                $objPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($styleArray1);
				$i++;
				$j++;
			}
			
			
			 $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('assets/location_list.xlsx');
			
		}
		return $record_found;
	}
	
	
	public function get_location_group($type){
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		else{
			$type = '0';
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
                        'location_type' => $type
		);
		$service_url = $this->api_url."get_location_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$data= array();
		
		$res = '';
		if(count($result) > 0){
		    $i = 1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= '<td><input type="radio" name="group_select_radio" value="'.$row->group_id.'"></td>';
			$res .= "<td>".$row->group_name."</td>";
			$res .= "<td>".$row->locations." Locations</td>";
			$res .= "<td>".$row->username."</td>";
			$res .= "<td>".$row->password."</td>";
			$res .= '<td >';
			$res .= "<i onclick='edit_loc_group(&apos;".$row->group_id."&apos;, &apos;".$row->group_name."&apos;, &apos;".$row->username."&apos;, &apos;".$row->password."&apos;)' class='fa fa-edit fa-lg' aria-hidden='true' style='cursor:pointer'></i>"; 
					$res .= '&nbsp;&nbsp;&nbsp;<i onclick="delete_loc_group('.$row->group_id.')" class="fa fa-trash fa-lg" aria-hidden="true" style="cursor:pointer"></i>';
				$res .='</td>';
			$res .= "</tr>";
			$i++;
		    }
		}
		else{
		    $res .= '<tr><td colspan="7" style="text-align:center">No Group Found !!</td></tr>';
		}
		$data['search_results'] = $res;
		return $data;
                //return json_decode($curl_response);
		//print_r(json_decode($curl_response));die;
                
	}
	
	
	public function create_location_group(){
		$type = $this->input->post("location_type");
		if($type == 'public'){
			$type = '1';
		}elseif($type == 'hotel'){
			$type = '2';
		}elseif($type == 'hospital'){
			$type = '3';
		}elseif($type == 'institutional'){
			$type = '4';
		}elseif($type == 'enterprise'){
			$type = '5';
		}elseif($type == 'caffe'){
			$type = '6';
		}
		elseif($type == 'retail'){
			$type = '7';
		}
		elseif($type == 'custom'){
			$type = '8';
		}
		elseif($type == 'purple'){
			$type = '9';
		}
		elseif($type == 'offline'){
			$type = '10';
		}
		else{
			$type = '0';
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_name' => $this->input->post("group_name"),
			'username' => $this->input->post("username"),
			'password' => $this->input->post("password"),
			'location_type' => $type,
			'locations' => $this->input->post("locations"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."create_location_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	
	public function add_location_to_group(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_id' => $this->input->post("group_id"),
			'locations' => $this->input->post("locations"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_location_to_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_loc_group(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_id' => $this->input->post("group_id")
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."delete_loc_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function update_location_group(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'group_name' => $this->input->post("group_name"),
			'username' => $this->input->post("username"),
			'password' => $this->input->post("password"),
			'group_id' => $this->input->post("group_id"),
			'locations' => $this->input->post("locations"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_location_group";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function get_group_location_list(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
                        'group_id' => $this->input->post("group_id"),
		);
		$service_url = $this->api_url."get_group_location_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
                //print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($curl_response);
		$data= array();
		
		$res = '';
		if(count($result) > 0){
		    $i = 1;
		    foreach($result as $row){
			$res .= "<tr>";
			$res .= "<td>".$i."</td>";
			$res .= '<td><input type="checkbox" name="group_location_list[]" value="'.$row->id.'"></td>';
			$res .= "<td>".$row->location_name."</td>";
			
			$res .= "</tr>";
			$i++;
		    }
		}
		else{
		    $res .= '<tr><td colspan="3" style="text-align:center">No Location Added !!</td></tr>';
		}
		$data['search_results'] = $res;
		return $data;
                //return json_decode($curl_response);
		//print_r(json_decode($curl_response));die;
                
	}
	
	
	public function add_otp_sms(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
			'locartion_id' => $this->input->post('locartion_id'),
			'otp_sms' => $this->input->post('otp_sms'),
			'sms_type' => $this->input->post('sms_type')
		);
		$service_url = $this->api_url."add_otp_sms";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		
		return $responce_data->resultCode;
		
	}
	
	
	public function sms_list($location_uid){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $location_uid,
			'location_id' => $this->input->post('location_id'),
		);
		$service_url = $this->api_url."sms_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		$content = '';
		if($responce_data->resultCode == 1){
			$i = 1;
			foreach($responce_data->vouchers as $responce_data1){
				$content .= '<tr>';
				$content .= '<td>'.$i.'</td>';
				$content .= '<td>'.$responce_data1->sms_type.' </td>';
				$content .= '<td>'.$responce_data1->created_on.' </td>';
				$content .= '<td>'.$responce_data1->total_sms.' </td>';
				$content .= '<td>'.$responce_data1->used_sms.' </td>';
				$content .= '<td>'.$responce_data1->sms_balance.' </td>';
				$content .= '</tr>';
				$i++;
			}
		}
		return $content;
		
	}
	
	public function offline_organisation_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."offline_organisation_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	
	
	public function add_offline_vm_organisation(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'offline_organisation_name' => $this->input->post("offline_organisation_name"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_offline_vm_organisation";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function vm_offline_department_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."vm_offline_department_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_offline_vm_department(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'offline_department_name' => $this->input->post("offline_department_name"),
			'org_id' => $this->input->post("org_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_offline_vm_department";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function vm_offline_employee_list(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post('location_uid'),
		);
		$service_url = $this->api_url."vm_offline_employee_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_vm_offline_employee(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'employee_name' => $this->input->post("employee_name"),
			'employee_email' => $this->input->post("employee_email"),
			'employee_mobile' => $this->input->post("employee_mobile"),
			'org_id' => $this->input->post("org_id"),
			'dept_id' => $this->input->post("dept_id"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_vm_offline_employee";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delete_offline_vm_organisation($organisation_id){
		$requestData = array(
			'organisation_id' => $organisation_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_offline_vm_organisation";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_offline_vm_department($department_id){
		$requestData = array(
			'department_id' => $department_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_offline_vm_department";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function delete_offline_vm_employee($employee_id){
		$requestData = array(
			'employee_id' => $employee_id,
			'locationid' => $this->input->post("location_id"),
		);
		$service_url = $this->api_url."delete_offline_vm_employee";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}

	public function contestifi_content_builder_list(){
		$requestData = array(
			'location_uid' => $this->input->post("location_uid"),
		);
		$service_url = $this->api_url."contestifi_content_builder_list";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_contistifi_quiz_question(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = '';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			$mime = $_FILES['file']['type'];
			if(strstr($mime, "video/")){
				$file_type = "video";
			}else if(strstr($mime, "image/")){
				$file_type = "image";
			}else if(strstr($mime, "audio/")){
				$file_type = "audio";
			}
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_id' => $this->input->post("contest_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'right_ans' => $this->input->post("right_ans"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contistifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}

	public function delte_contestifi_quiz_question($question_id,$location_id){
		$requestData = array(
			'question_id' => $question_id,
			"location_id" => $location_id
		);
		$service_url = $this->api_url."delte_contestifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	public function get_contestifi_quiz_question(){
		$requestData = array(
			'question_id' => $this->input->post("question_id"),
		);
		$service_url = $this->api_url."get_contestifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_contistifi_quiz_question(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$path = $base_url;
		if($path != ''){
		    $output = exec("sudo chmod -R 0777 \"$path\"");
		}
		$content_file = '';
		$file_type = '';
		if(isset($_FILES['file']['name']) && $_FILES['file']['name']!=''){
			$mime = $_FILES['file']['type'];
			if(strstr($mime, "video/")){
				$file_type = "video";
			}else if(strstr($mime, "image/")){
				$file_type = "image";
			}else if(strstr($mime, "audio/")){
				$file_type = "audio";
			}
			$filename=$_FILES['file']['name'];
			$fileupload= move_uploaded_file($_FILES['file']['tmp_name'], $base_url.$filename);
			$i = strrpos($filename,".");
			if (!$i) { return ""; }
			$l = strlen($filename) - $i;
			$ext = substr($filename,$i+1,$l);
			$file_ext =  $ext;
			//$file_ext = end(explode('.',$filename));
			$newname=date("Ymdhisv").rand().".".$file_ext;    
                        rename($base_url.$filename,$base_url.$newname);
			$content_file = $newname;
			$this->offline_upload_on_s3($base_url,$newname);
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'quiz_question_id' => $this->input->post("quiz_question_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'contest_id' => $this->input->post("contest_id"),
			'right_ans' => $this->input->post("right_ans"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_contistifi_quiz_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}

	public function add_contestifi_contest(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$contestifi_contest_logo_image_original ="";
		$contestifi_contest_logo_image_logo ="";
		$contestifi_contest_logo_image_small ="";
		$image_name = $this->input->post("contestifi_contest_logo_image_name");
		$image_src = $this->input->post("contestifi_contest_logo_image_src");
		if($image_name != ''){
			if($image_src != ''){
				
				$file_ext = $this->getExtension($image_name);
				$explode = explode(',',$image_src);
				$image_src = $explode['1'];
				$image_src = str_replace(' ', '+', $image_src);
				$data_img = base64_decode($image_src);
				$filename = uniqid() . '.'.$file_ext;
				$file = $base_url . $filename;
				
				$success = file_put_contents($file, $data_img);
				$newname=date("Ymdhisv").rand().".".$file_ext; 
				rename($base_url.$filename,$base_url.$newname);
				
				$this->load->library('image_lib');
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $base_url .$newname;
				$config1['new_image'] = $base_url."300_".$newname;
		
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 300;
				$config1['height'] = 100;
				$this->image_lib->initialize($config1);
				$this->image_lib->resize();
		    
				$this->image_lib->clear();
				 $config2['image_library'] = 'gd2';
				 $config2['source_image'] = $base_url .$newname;
				 $config2['new_image'] = $base_url ."600_".$newname;
		  
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 600;
				$config2['height'] = 200;
				  $this->image_lib->initialize($config2);
				  $this->image_lib->resize();
		      
				$contestifi_contest_logo_image_original=$newname;
				$contestifi_contest_logo_image_logo="600_".$newname;
				$contestifi_contest_logo_image_small ="300_".$newname;
				 
				$fname = $base_url.$newname;
				$fname1 = $base_url ."300_".$newname;
				$fname2 = $base_url ."600_".$newname;
			   
				$famazonname = AMAZONPATH.'offline_video_image_content/'.$newname;
				$famazonname1 = AMAZONPATH.'offline_video_image_content/300_'. $newname;
				$famazonname2 = AMAZONPATH.'offline_video_image_content/600_'. $newname;
				$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				//unlink($fname);
				//unlink($fname1);
				//unlink($fname2);
					 
					
			}	
		}
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_name' => $this->input->post("contest_name"),
			'contest_type' => $this->input->post("contest_type"),
			'bg_color' => $this->input->post("bg_color"),
			'contest_frequency' => $this->input->post("contest_frequency"),
			'contestifi_custom_frequecny' => $this->input->post("contestifi_custom_frequecny"),
			'contestifi_no_of_attemption' => $this->input->post("contestifi_no_of_attemption"),
			
			'contestifi_point_per_question' => $this->input->post("contestifi_point_per_question"),
			'contestifi_negative_point' => $this->input->post("contestifi_negative_point"),
			'contestifi_point_deduct_every_secod' => $this->input->post("contestifi_point_deduct_every_secod"),
			'contestifi_max_time_allocation' => $this->input->post("contestifi_max_time_allocation"),
			'contestifi_contest_logo_image_original' => $contestifi_contest_logo_image_original,
			'contestifi_contest_logo_image_logo' => $contestifi_contest_logo_image_logo,
			'contestifi_contest_logo_image_small' => $contestifi_contest_logo_image_small,
			'contestifi_contest_rule' => $this->input->post("contestifi_contest_rule"),
			'text_color' => $this->input->post("text_color"),
			'button_color' => $this->input->post("button_color"),
			'start_date' => $this->input->post("start_date"),
			'end_date' => $this->input->post("end_date"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contestifi_contest";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function delte_contestifi_contest($contest_id,$location_id){
		$requestData = array(
			'contest_id' => $contest_id,
			"location_id" => $location_id
		);
		$service_url = $this->api_url."delte_contestifi_contest";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	
	public function get_contestifi_contest_for_edit(){
		$requestData = array(
			'contest_id' => $this->input->post("contest_id"),
		);
		$service_url = $this->api_url."get_contestifi_contest_for_edit";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function update_contestifi_contest(){
		$base_url = 'assets/offline_content/';
		$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$contestifi_contest_logo_image_original ="";
		$contestifi_contest_logo_image_logo ="";
		$contestifi_contest_logo_image_small ="";
		$image_name = $this->input->post("contestifi_contest_logo_image_name");
		$image_src = $this->input->post("contestifi_contest_logo_image_src");
		if($image_name != ''){
			if($image_src != ''){
				
				$file_ext = $this->getExtension($image_name);
				$explode = explode(',',$image_src);
				$image_src = $explode['1'];
				$image_src = str_replace(' ', '+', $image_src);
				$data_img = base64_decode($image_src);
				$filename = uniqid() . '.'.$file_ext;
				$file = $base_url . $filename;
				
				$success = file_put_contents($file, $data_img);
				$newname=date("Ymdhisv").rand().".".$file_ext; 
				rename($base_url.$filename,$base_url.$newname);
				
				$this->load->library('image_lib');
				$config1['image_library'] = 'gd2';
				$config1['source_image'] = $base_url .$newname;
				$config1['new_image'] = $base_url ."300_".$newname;
		
				$config1['maintain_ratio'] = TRUE;
				$config1['width'] = 300;
				$config1['height'] = 100;
				$this->image_lib->initialize($config1);
				$this->image_lib->resize();
		    
				$this->image_lib->clear();
				 $config2['image_library'] = 'gd2';
				 $config2['source_image'] = $base_url .$newname;
				 $config2['new_image'] = $base_url."600_".$newname;
		  
				$config2['maintain_ratio'] = TRUE;
				$config2['width'] = 600;
				$config2['height'] = 200;
				  $this->image_lib->initialize($config2);
				  $this->image_lib->resize();
		      
				$contestifi_contest_logo_image_original=$newname;
				$contestifi_contest_logo_image_logo="600_".$newname;
				$contestifi_contest_logo_image_small ="300_".$newname;
				 
				$fname = $base_url.$newname;
				$fname1 = $base_url ."300_".$newname;
				$fname2 = $base_url."600_".$newname;
			   
				$famazonname = AMAZONPATH.'offline_video_image_content/'.$newname;
				$famazonname1 = AMAZONPATH.'offline_video_image_content/300_'. $newname;
				$famazonname2 = AMAZONPATH.'offline_video_image_content/600_'. $newname;
				$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
				$this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
				$this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
				//unlink($fname);
				//unlink($fname1);
				//unlink($fname2);
					 
					
			}	
		}
		//$base_url = '/var/www/html/isp_hotspot/offline/offline_video_image_content/';
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_name' => $this->input->post("contest_name"),
			'contest_type' => $this->input->post("contest_type"),
			'bg_color' => $this->input->post("bg_color"),
			'contest_frequency' => $this->input->post("contest_frequency"),
			'contestifi_custom_frequecny' => $this->input->post("contestifi_custom_frequecny"),
			'contestifi_no_of_attemption' => $this->input->post("contestifi_no_of_attemption"),
			
			'contestifi_point_per_question' => $this->input->post("contestifi_point_per_question"),
			'contestifi_negative_point' => $this->input->post("contestifi_negative_point"),
			'contestifi_point_deduct_every_secod' => $this->input->post("contestifi_point_deduct_every_secod"),
			'contestifi_max_time_allocation' => $this->input->post("contestifi_max_time_allocation"),
			'contestifi_contest_logo_image_original' => $contestifi_contest_logo_image_original,
			'contestifi_contest_logo_image_logo' => $contestifi_contest_logo_image_logo,
			'contestifi_contest_logo_image_small' => $contestifi_contest_logo_image_small,
			'contestifi_contest_rule' => $this->input->post("contestifi_contest_rule"),
			'contest_id' => $this->input->post("contest_id"),
			'text_color' => $this->input->post("text_color"),
			'button_color' => $this->input->post("button_color"),
			'start_date' => $this->input->post("start_date"),
			'end_date' => $this->input->post("end_date"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_contestifi_contest";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function add_contistifi_contest_prize(){
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_id' => $this->input->post("contest_id"),
			'contestifi_contest_prize' => $this->input->post("contestifi_contest_prize"),
			'prize_id' => $this->input->post("prize_id"),
			
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contistifi_contest_prize";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function delte_contestifi_contest_prize($prize_id,$location_id){
		$requestData = array(
			'prize_id' => $prize_id,
			"location_id" => $location_id
		);
		$service_url = $this->api_url."delte_contestifi_contest_prize";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		$responce_data = json_decode($curl_response);
		 return $responce_data->resultCode;
		
	}
	
	public function contestifi_add_content_builder(){
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
		);
		//return $requestData;die;
		$service_url = $this->api_url."contestifi_add_content_builder";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function get_contestifi_signup_term_condition($location_id){
		$requestData = array(
			'location_id' => $location_id
		);
		$service_url = $this->api_url."get_contestifi_signup_term_condition";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_contestifi_terms_conditon(){
		$requestData = array(
			'location_id' => $this->input->post('location_id'),
			'terms' => $this->input->post('terms'),
		);
		$service_url = $this->api_url."add_contestifi_terms_conditon";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//echo "<pre>";print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	public function add_contistifi_survey_question(){
		
		$content_file = '';
		$file_type = '';
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'contest_id' => $this->input->post("contest_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'survey_type' => $this->input->post("survey_type"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."add_contistifi_survey_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}
	
	public function update_contistifi_survey_question(){
		
		$content_file = '';
		$file_type = '';
		
		$requestData = array(
			'isp_uid' => $this->isp_uid,
			'location_uid' => $this->input->post("location_uid"),
			'locationid' => $this->input->post("locationid"),
			'quiz_question_id' => $this->input->post("quiz_question_id"),
			'poll_querstion' => $this->input->post("poll_querstion"),
			'option_one' => $this->input->post("option_one"),
			'option_two' => $this->input->post("option_two"),
			'option_three' => $this->input->post("option_three"),
			'option_four' => $this->input->post("option_four"),
			'contest_id' => $this->input->post("contest_id"),
			'survey_type' => $this->input->post("survey_type"),
			'contest_file' => $content_file,
			'contest_file_type' => $file_type,
		);
		//return $requestData;die;
		$service_url = $this->api_url."update_contistifi_survey_question";
		$curl = curl_init($service_url);
		$requestData = $requestData;
		$data_request = json_encode($requestData);
		//print_r($data_request);die;
		$curl_post_data = array("requestData" => $data_request);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response);
	}


}


?>