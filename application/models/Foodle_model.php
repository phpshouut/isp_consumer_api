<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class Foodle_model extends CI_Model {
    private $db2; private $currsymbol = '';
    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2_publicwifi', TRUE);
    }
	
    public function log($function, $request, $responce) {
        $log_data = array(
            'function' => $function,
            'request' => $request,
            'response' => $responce,
            'added_on' => date("Y-m-d H:i:s"),
        );
		$this->db2->insert("foodle_logs",$log_data);
      //  $this->mongo_db->insert('log_data', $log_data);
    }
    
    
    public function mobile_login($jsondata){
        
       
       $data=array();
       $query=$this->db2->query("SELECT id FROM `foodle_user` WHERE `user`='".$jsondata->mobile."'");
       if($query->num_rows()>0){
            $data['resultcode']="200";
            $data['resultmsg']="Success";
			$otparr=$this->send_otp($jsondata);
			$data['otp']=$otparr['otp'];
       }else{
            $tabledata=array("user"=>$jsondata->mobile,"added_on"=>date("Y-m-d H:i:s"));
			if($jsondata->mobile!=''){
            $this->db2->insert("foodle_user",$tabledata);
            $data['resultcode']="200";
            $data['resultmsg']="Success";
			$otparr=$this->send_otp($jsondata);
			$data['otp']=$otparr['otp'];
			}else{
				$data['resultcode']="401";
				$data['resultmsg']="Something Wrong Happened";
			}			
       }
       
       return $data;
    }
    
    public function send_otp($jsondata){
        $data=array();
        $infini_apikey = 'A921b98066097485eddeb9388665a8946';
		$infini_senderid = 'FOODLE';
        $otpno = mt_rand(1000,9999);
        $user_message="Your OTP is ".$otpno;
        $user_mobileno=$jsondata->mobile;
        $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
	$data['otp']=$otpno;
        $query=$this->db2->query("select id from foodle_otp where user='".$user_mobileno."'");
        if($query->num_rows()>0){
            $rowdata=$query->row_array();
            $id=$rowdata['id'];
            $tabledata=array("user"=>$user_mobileno,"otp"=>$otpno,"added_on"=>date("Y-m-d H:i:s"));
            $this->db2->update("foodle_otp",$tabledata,array("id"=>$id));
        }else{
            $tabledata=array("user"=>$user_mobileno,"otp"=>$otpno,"added_on"=>date("Y-m-d H:i:s"));
            $this->db2->insert("foodle_otp",$tabledata);
        }
        $data['resultcode']=200;
        $data['resultmsg']="Success";
		$data['otp']=$otpno;
        return $data;
    }
    
    public function verify_otp($jsondata){
        $data=array();
        $query=$this->db2->query("select id from foodle_otp where user='".$jsondata->mobile."' and otp='".$jsondata->otp."'");
        if($query->num_rows()>0){
            $data['resultcode']=200;
            $data['resultmsg']="OTP Verified";
			$userq=$this->db2->query("select id,password,is_registered from foodle_user where user='".$jsondata->mobile."'");
			if($userq->num_rows()>0){
				$userd=$userq->row_array();
				if($userd['password']==""){
					$data['is_registered']=1;
					$data['resultmsg']="User already registered,set new password";
				}else{
					$data['is_registered']=2;
					$data['resultmsg']="User already registered";
				}
				
			}else{
				$data['is_registered']=0;
				$data['resultmsg']="User not registered";
			}
			
        }else{
            $data['resultcode']=401;
            $data['resultmsg']="Otp doesn't match";
        }
        return $data;
    }
    
    
    
    
    public function infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message)
    {
	$baseUrl = "https://alerts.solutionsinfini.com/api/v4/?api_key=".$infini_apikey;
	$user_message=urlencode($user_message); 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl."&method=sms&message=$user_message&to=$user_mobileno&sender=$infini_senderid");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
	$result = curl_exec($ch);
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
        if($statusCode==200){
	    return 1;
	}else{
	    return 0;
	}
	curl_close($ch);
    }
    
    public function foodle_offer($jsondata)
    {
	
	if($jsondata->lat!='' && $jsondata->long!=''){
	$earths_radius = 6371;
        $user_latitude = $jsondata->lat;
        $user_longitude = $jsondata->long;
	    $getlocationQuery = $this->db2->query("SELECT id, location_uid, location_name, geo_address, placeid, latitude, longitude, (acos(sin(RADIANS($user_latitude)) * sin(RADIANS(latitude)) + cos(RADIANS($user_latitude)) * cos(RADIANS(latitude)) * cos(RADIANS(longitude) - RADIANS(($user_longitude)))) * $earths_radius) as distance FROM wifi_location WHERE  status='1' AND is_deleted='0' AND location_type='21' and is_not_foodle='0' ORDER BY distance ASC");	  
	}else{
	  $getlocationQuery = $this->db2->query("SELECT id, location_uid, location_name FROM wifi_location WHERE  status='1' AND is_deleted='0' AND location_type='21'  and is_not_foodle='0'");	  
	}
	
	/*$rewardq=$this->db2->query("SELECT fc.`reward_id`,fc.`start_date`,fc.end_date,flr.reward_name,fc.loc_id,wl.location_name,fc.audience_id,
GROUP_CONCAT(CONCAT(flr.reward_name,' ::: ',fl.reward_image,' ::: ',flsi.qty) SEPARATOR ' @@@ ') AS offerdata,fls.slab_amt FROM `foodle_campaign` fc 
INNER JOIN `foodle_location_loyalty_reward` flr ON (flr.id=fc.reward_id AND flr.is_deleted='0')
INNER JOIN `foodle_loyalty_reward` fl ON (fl.id=flr.reward_id AND fl.is_deleted='0')
INNER JOIN `foodle_location_slab_item` flsi ON (flsi.foodle_rewardid=fc.reward_id AND flsi.is_deleted='0')
INNER JOIN wifi_location wl ON (wl.id=fc.loc_id)
INNER JOIN `foodle_location_loyalty_slab` fls ON (fls.id=flsi.`slab_id` AND fls.is_deleted='0') WHERE fc.is_deleted='0'
 AND NOW() BETWEEN fc.start_date AND fc.end_date  GROUP BY flsi.slab_id");*/
	$rewardarr=array();
	$campaignq=$this->db2->query("SELECT fc.refer_friend,fc.end_date,fcs.is_referred,fc.id as campaignid,fllr.id AS reward_id,fllr.reward_name,fllr.loc_id,fc.campaign_type,fc.slab_amt,wl.location_name,fllr.reward_name,fl.reward_image
         FROM `foodle_campaign_sent` fcs INNER JOIN 
        `foodle_campaign` fc ON (fc.id=fcs.`campaign_id`)
        INNER JOIN foodle_location_loyalty_reward  fllr ON (fc.reward_id=fllr.id AND fllr.is_deleted='0')
        INNER JOIN wifi_location wl ON (wl.id=fllr.loc_id and wl.is_deleted='0')
		INNER JOIN `foodle_loyalty_reward` fl ON (fl.id=fllr.reward_id AND fl.is_deleted='0') 
		WHERE fcs.user='".$jsondata->mobile."' and now() between fc.start_date and fc.end_date GROUP BY fcs.`campaign_id`
        ");
		
		if($campaignq->num_rows()>0){
			foreach($campaignq->result() as $val){
				$rewardarr[$val->loc_id][$val->reward_id]=$val;
				$rewardarr[$val->loc_id][$val->reward_id]->is_campaign=1;
				$rewardarr[$val->loc_id][$val->reward_id]->campaign_type=(int)$val->campaign_type;
				$rewardarr[$val->loc_id][$val->reward_id]->campaign_id=(int)$val->campaignid;
				$rewardarr[$val->loc_id][$val->reward_id]->is_referred=(int)$val->is_referred;
				$rewardarr[$val->loc_id][$val->reward_id]->expiry_days="Expires ".$this->get_time_difference_php($val->end_date)." later";
				$rewardarr[$val->loc_id][$val->reward_id]->refer_friend_count=(int)$val->refer_friend;
				
			}  
		}
		
	$generalrq=$this->db2->query("select fllr.id as reward_id,fllr.reward_name,fllr.loc_id,fllr.slab_amt,wl.location_name,fllr.reward_name,fl.reward_image
	from foodle_location_loyalty_reward  fllr 	inner join wifi_location wl ON (wl.id=fllr.loc_id)
	INNER JOIN `foodle_loyalty_reward` fl ON (fl.id=fllr.reward_id AND fl.is_deleted='0')
	where fllr.is_campaign='0' and fllr.is_deleted=0 order by fllr.slab_amt asc");
	
	if($generalrq->num_rows()>0){
	  foreach($generalrq->result() as $val){
		$rewardarr[$val->loc_id][$val->reward_id]=$val;
		$rewardarr[$val->loc_id][$val->reward_id]->is_campaign=0;
		$rewardarr[$val->loc_id][$val->reward_id]->campaign_type=0;
		$rewardarr[$val->loc_id][$val->reward_id]->campaign_id=0;
		$rewardarr[$val->loc_id][$val->reward_id]->is_referred=0;
		$rewardarr[$val->loc_id][$val->reward_id]->expiry_days='';
		$rewardarr[$val->loc_id][$val->reward_id]->refer_friend_count=0;
		}  
	}
	
	//echo "<pre>"; print_R($rewardarr); die;
	
	$useredeemq=$this->db2->query("select reward_id,loc_id from foodle_unlocked_offer where is_redeemed='1' and user='".$jsondata->mobile."'");
	$userredeemarr=array();
	if($useredeemq->num_rows()>0){
	    foreach($useredeemq->result() as $val){
		$userredeemarr[$val->loc_id][$val->reward_id]=$val->reward_id;
	    }
	}
		
		
	
	$userq=$this->db2->query("select amount,loc_id from foodle_user_wallet where user='".$jsondata->mobile."'");
	$useramtarr=array();
	if($userq->num_rows()>0){
	    foreach($userq->result() as $val){
		$useramtarr[$val->loc_id]=$val->amount;
	    }
	}
	//echo "<pre>"; print_R($useramtarr);die;
	//echo "<pre>"; print_R($rewardarr);die;
	
	$rewardimageq=$this->db2->query("select reward_id,image from foodle_campaign_image");
	$rewardimagearr=array();
	if($rewardimageq->num_rows()>0){
		foreach($rewardimageq->result() as $val){
			$rewardimagearr[$val->reward_id]=$val->image;
		}
	}
	
	
	$fnldata=array();
	 $x=0;
	foreach($getlocationQuery->result() as $val){
	     $i=0;
		
	   if(isset($rewardarr[$val->id])){
	   
		foreach($rewardarr[$val->id] as $val1){
		    //check user available in audinece or not  pending task
		 // $validcampaign= $this->campaign_valid($jsondata->mobile,$val->id,$val1->audience_id);
		 //$validcampaign=true;
		 // if($vaidcampaign){
			 
			if(!isset($userredeemarr[$val1->loc_id][$val1->reward_id])){
		    if(isset($useramtarr[$val->id])){
			
			$islocked=($useramtarr[$val->id]>=$val1->slab_amt)?0:1;
			if($islocked==1){
			    $fund_to_unlock=$val1->slab_amt-$useramtarr[$val->id];
			}else{
			    $fund_to_unlock=0;
			}
		    }else{
			$islocked=1;
			$fund_to_unlock=$val1->slab_amt;
		    }
			$userspend=(isset($useramtarr[$val->id]))?$useramtarr[$val->id]:0;
			$giftarr=array("gift1.png","gift2.png","gift3.png","gift4.png");
		    if(count($rewardarr[$val->id])>0){
			$giftkey=array_rand($giftarr);
			$giftval=$giftarr[$giftkey];
			if($val1->is_campaign==1){
				if(!isset($rewardimagearr[$val1->reward_id])){
					$tabledata=array("reward_id"=>$val1->reward_id,"image"=>$giftval,"added_on"=>date("Y-m-d H:i:s"));
					$this->db2->insert("foodle_campaign_image",$tabledata);
				}else{
					$giftval=$rewardimagearr[$val1->reward_id];
				}
			}
			
		    $fnldata[$x]['offercount']=count($rewardarr[$val->id]);
		    $fnldata[$x]['location_name']=$val1->location_name;
		    $fnldata[$x]['spends']=(isset($useramtarr[$val->id]))?number_format($useramtarr[$val->id]):0;
		    $fnldata[$x]['offerdata'][$i]['reward_id']=$val1->reward_id;
		    $fnldata[$x]['offerdata'][$i]['reward_name']=$val1->reward_name;
		    $fnldata[$x]['offerdata'][$i]['location_name']=$val1->location_name;
		    $fnldata[$x]['offerdata'][$i]['loc_id']=$val1->loc_id;
		    $fnldata[$x]['offerdata'][$i]['image']=($val1->is_campaign==1)?FOODLE_IMAGEPATH.trim($giftval):FOODLE_IMAGEPATH.trim($val1->reward_image);
		    $fnldata[$x]['offerdata'][$i]['original_image']=FOODLE_IMAGEPATH.trim($val1->reward_image);
			$fnldata[$x]['offerdata'][$i]['slab_amt']=number_format($val1->slab_amt);
		    $fnldata[$x]['offerdata'][$i]['is_locked']=$islocked;
		    $fnldata[$x]['offerdata'][$i]['fund_to_unlock']=number_format($fund_to_unlock);
			$fnldata[$x]['offerdata'][$i]['is_campaign']=$val1->is_campaign;
			$fnldata[$x]['offerdata'][$i]['campaign_type']=$val1->campaign_type;
			$fnldata[$x]['offerdata'][$i]['campaign_expiry']=$val1->expiry_days;
			$fnldata[$x]['offerdata'][$i]['refer_friend_count']=$val1->refer_friend_count;
			$fnldata[$x]['offerdata'][$i]['campaign_id']=$val1->campaign_id;
			$fnldata[$x]['offerdata'][$i]['is_referred']=$val1->is_referred;
			if($val1->campaign_id!=0)
			{
				$locakarr=$this->check_campaign_locked($val1->campaign_id,$jsondata->mobile,$userspend);
				$fnldata[$x]['offerdata'][$i]['currentstatus_locked']=$locakarr['currentlockstatus'];
				$fnldata[$x]['offerdata'][$i]['is_unlocakable']=$locakarr['is_unlocakable'];
			}else{
				 $fnldata[$x]['offerdata'][$i]['currentstatus_locked']=1;
				 $fnldata[$x]['offerdata'][$i]['is_unlocakable']=0;
			}
		    
		    $i++;
			
			}
		 }
		 // }
		}
		$x++;
		
		
	   }else{
		
		/*$fnldata[$x]['offercount']=0;
		$fnldata[$x]['location_name']=$val->location_name;
		$fnldata[$x]['spends']=(isset($useramtarr[$val->id]))?$useramtarr[$val->id]:0;
		$fnldata[$x]['offerdata']=array();*/
		
	   }
	   
	   
	}
	$data['resultcode']=200;
	$data['resultmsg']="Success";
	$data['storedata']=$fnldata;
	echo json_encode($data);die;
	return $data;
    }
	
	
	public function get_time_difference_php($created_time)
	{
	
	$str = strtotime($created_time);
	$today = strtotime(date('Y-m-d H:i:s')); // It returns the time difference in Seconds...
	$time_differnce =   $str-$today; // To Calculate the time difference in Years...
	$years = 60 * 60 * 24 * 365; // To Calculate the time difference in Months...
	$months = 60 * 60 * 24 * 30; // To Calculate the time difference in Days...
	$days = 60 * 60 * 24; // To Calculate the time difference in Hours...
	$hours = 60 * 60; // To Calculate the time difference in Minutes...
	$minutes = 60;
	if (intval($time_differnce / $years) > 1)
		{
		return intval($time_differnce / $years) . " years ";
		}
	  else
	if (intval($time_differnce / $years) > 0)
		{
		return intval($time_differnce / $years) . " year ";
		}
	  else
	if (intval($time_differnce / $months) > 1)
		{
		return intval($time_differnce / $months) . " months ";
		}
	  else
	if (intval(($time_differnce / $months)) > 0)
		{
		return intval(($time_differnce / $months)) . " month ";
		}
	  else
	if (intval(($time_differnce / $days)) > 1)
		{
		return intval(($time_differnce / $days)) . " days ";
		}
	  else
	if (intval(($time_differnce / $days)) > 0)
		{
		return intval(($time_differnce / $days)) . " day ";
		}
	  else
	if (intval(($time_differnce / $hours)) > 1)
		{
		return intval(($time_differnce / $hours)) . " hours ";
		}
	  else
	if (intval(($time_differnce / $hours)) > 0)
		{
		return intval(($time_differnce / $hours)) . " hour ";
		}
	  else
	if (intval(($time_differnce / $minutes)) > 1)
		{
		return intval(($time_differnce / $minutes)) . " minutes ";
		}
	  else
	if (intval(($time_differnce / $minutes)) > 0)
		{
		return intval(($time_differnce / $minutes)) . " minute ";
		}
	  else
	if (intval(($time_differnce)) > 1)
		{
		return intval(($time_differnce)) . " seconds ";
		}
	  else
		{
		   
		    return "few seconds ";
		}
	}
	
	public function check_campaign_locked($campaignid,$user,$userspend){
		$campaignq=$this->db2->query("select reward_id,campaign_type,slab_amt,refer_friend from foodle_campaign where id='".$campaignid."'");
		
		$campiagndata=$campaignq->row_array();
		$reward_id=$campiagndata['reward_id'];
		$campaigntype=$campiagndata['campaign_type'];
		$slab_amt=$campiagndata['slab_amt'];
		$referfriend=$campiagndata['refer_friend'];
		//echo "===>>>".$reward_id."::::".$campaigntype.":::".$slab_amt."::::".$referfriend.":::".$userspend."<br/>";
		if($campaigntype==0){
			$campq=$this->db2->query("select id from foodle_unlocked_offer where
			user='".$user."' and reward_id='".$reward_id."' and campaign_id='".$campaignid."' and is_referred='0'");
			if($campq->num_rows()>0){
				$campaignlocked=0;
				$is_unlocakable=0;
			}else{
				$campaignlocked=1;
				$is_unlocakable=1;
			}
			$is_unlocakable=1;
		}else if($campaigntype==1){
			$campq=$this->db2->query("select id from foodle_unlocked_offer where
			user='".$user."' and reward_id='".$reward_id."' and campaign_id='".$campaignid."' and is_referred='0'");
			if($campq->num_rows()>0){
				$campaignlocked=0;
				$is_unlocakable=0;
			}else{
				//echo "====>>>".$userspend.":::".$slab_amt; die;
				if($userspend<$slab_amt){
					$campaignlocked=1;
					$is_unlocakable=0;
				}else{
					$campaignlocked=1;
					$is_unlocakable=1;
				}
				
			}
			
			
		}else if($campaigntype==2){
			$campq=$this->db2->query("select id from foodle_campaign_sent where
			referred_by='".$user."' and campaign_id='".$campaignid."' and is_referred='1'");
			if($campq->num_rows()>0){
				$campq=$this->db2->query("select id from foodle_unlocked_offer where
			user='".$user."' and reward_id='".$reward_id."' and campaign_id='".$campaignid."' and is_referred='1'");
				if($campq->num_rows()==$referfriend){
					$campaignlocked=0;
					$is_unlocakable=0;
				}else{
					$campaignlocked=1;
					$is_unlocakable=0;
				}
			}else{
				$campaignlocked=1;
				$is_unlocakable=1;
			}
		}
		$data['currentlockstatus']=$campaignlocked;
		$data['is_unlocakable']=$is_unlocakable;
		return $data;
		
	}
	
	public function campaign_valid($user,$locid,$audienceid){
		$query=$this->db2->query("select amount,visits from foodle_user_wallet where user='".$user."' and loc_id='".$locid."'");
		//echo $this->db2->last_query(); die;
		if($query->num_rows()>0){
			$rowdata=$query->row_array();
			$visits=$rowdata['visits'];
			$amount=$rowdata['amount'];
			$audienceq=$this->db2->query("select visit_from_condition,visit_from_value,visit_to_conditon,visit_to_value,
			visit_spend_between_condition,spend_from_condition,spend_from_value,spend_to_condition,spend_to_value from foodle_location_loyalty_reward_audience where id='".$audienceid."' and is_deleted='0'");
			$carray=array(1=>">=",2=>"<=");
			$bwarry=array(1=>"&&",2=>"||");
			
			if($audienceq->num_rows()>0){
				$audinced=$audienceq->row_array();
				// both condition Spend && Visit
				$querycond='';
				$condtrue=0;
				if($audinced['visit_from_condition']!=0 && $audinced['spend_from_condition']!=0){
					//querycond  visit
					//$querycond.="(";
					if($audinced['visit_to_conditon']!=0){
							
							//check if satisfy from to both
							if(($visits.$carray[$audinced['visit_from_condition']].$audinced['visit_from_value']) && ($visits.$carray[$audinced['visit_to_conditon']].$audinced['visit_to_value'])){
								$condtrue=1;
							}else{
								return false;
							}
						}else{
							//check if statisfy only from 
							if($visits.$carray[$audinced['visit_from_condition']].$audinced['visit_from_value']){
								$condtrue=1;
							}else{
								return false;
							}
							
						}
						
						if($audinced['spend_to_condition']!=0){
							//check if satisfy from to both
							
							if(($amount.$carray[$audinced['spend_from_condition']].$audinced['spend_from_value']) && ($amount.$carray[$audinced['spend_to_condition']].$audinced['spend_to_value'])){
								$condtrue=1;
							}else{
								return false;
							}
						}else{
							// Check if satisfy only from 
							$querycond.="(".$amount.$carray[$audinced['spend_from_condition']].$audinced['spend_from_value'].")";
							if($amount.$carray[$audinced['spend_from_condition']].$audinced['spend_from_value']){
								$condtrue=1;
							}else{
								return false;
							}
						}
						
						if($condtrue==1){
							return true;
						}
					//$querycond.=")";
					//echo "==>>".$querycond;
					
					//$querycond="(".$querycond.")";
					
					
				}else{
					//individual condition
					if($audinced['visit_from_condition']!=0){
						//visit Check
						if($audinced['visit_to_conditon']!=0){
							
							//check if satisfy from to both
							echo "(".$visits.$carray[$audinced['visit_from_condition']].$audinced['visit_from_value'].") && (".$visits.$carray[$audinced['visit_to_conditon']].$audinced['visit_to_value'].")<br/>";
							if(($visits.$carray[$audinced['visit_from_condition']].$audinced['visit_from_value']) && ($visits.$carray[$audinced['visit_to_conditon']].$audinced['visit_to_value'])){
								echo "true<br/>";
								return true;
							}else{
								echo "false<br/>";
								return false;
							}
						}else{
							//check if statisfy only from 
							
							echo $visits.$carray[$audinced['visit_from_condition']].$audinced['visit_from_value']."<br/>";
							if($visits.$carray[$audinced['visit_from_condition']].$audinced['visit_from_value']){
								echo "true<br/>";
								return true;
							}else{
								echo "false<br/>";
								return false;
							}
							
						}
						
						
					}else{
						//Spend Check
						if($audinced['spend_to_condition']!=0){
							//check if satisfy from to both
							echo "(".$amount.$carray[$audinced['spend_from_condition']].$audinced['spend_from_value'].") && (".$amount.$carray[$audinced['spend_to_condition']].$audinced['spend_to_value'].")<br/>";
							if(($amount.$carray[$audinced['spend_from_condition']].$audinced['spend_from_value']) && ($amount.$carray[$audinced['spend_to_condition']].$audinced['spend_to_value'])){
								echo "true<br/>";
								return true;
							}else{
								echo "false<br/>";
								return false;
							}
						}else{
							// Check if satisfy only from 
							echo $amount.$carray[$audinced['spend_from_condition']].$audinced['spend_from_value']."<br/>";
							//$cond=eval($carray[$audinced['spend_from_condition']]);
							$str=$carray[$audinced['spend_from_condition']];
						//	echo "===".$str;
							$cond=eval("\$str");
							//echo "===>>".$cond."<br/>"; 
							if($amount.$cond.$audinced['spend_from_value']){
								echo "xxxxxtrue<br/>";
								return true;
							}else{
								echo "false<br/>";
								return false;
							}
						}
						
					}
				}
				
				
				
				
			}else{
				//no audience present
				return false;
			}
			
			
		}else{
			//no user data available
			return false;
		}
	}
    
    
    public function unlock_offer($jsondata){
	$data=array();
	/*$rewardq=$this->db2->query("SELECT fc.`reward_id`,fc.`start_date`,fc.end_date,flr.reward_name,fc.loc_id,wl.location_name,fc.audience_id,
GROUP_CONCAT(CONCAT(flr.reward_name,' ::: ',fl.reward_image,' ::: ',flsi.qty) SEPARATOR ' @@@ ') AS offerdata,fls.slab_amt FROM `foodle_campaign` fc 
INNER JOIN `foodle_location_loyalty_reward` flr ON (flr.id=fc.reward_id AND flr.is_deleted='0')
INNER JOIN `foodle_loyalty_reward` fl ON (fl.id=flr.reward_id AND fl.is_deleted='0')
INNER JOIN `foodle_location_slab_item` flsi ON (flsi.foodle_rewardid=fc.reward_id AND flsi.is_deleted='0')
INNER JOIN wifi_location wl ON (wl.id=fc.loc_id)
INNER JOIN `foodle_location_loyalty_slab` fls ON (fls.id=flsi.`slab_id` AND fls.is_deleted='0') WHERE fc.is_deleted='0'
 AND NOW() BETWEEN fc.start_date AND fc.end_date and fc.reward_id='".$jsondata->reward_id."'  GROUP BY flsi.slab_id");*/
 $campaignid='';
 if($jsondata->is_campaign==1)
 {
	$rewardq=$this->db2->query("SELECT fc.id as campaignid,fllr.id AS reward_id,fllr.reward_name,fllr.loc_id,fc.campaign_type,fc.slab_amt,wl.location_name,fllr.reward_name,fl.reward_image
         FROM `foodle_campaign_sent` fcs INNER JOIN 
        `foodle_campaign` fc ON (fc.id=fcs.`campaign_id`)
        INNER JOIN foodle_location_loyalty_reward  fllr ON (fc.reward_id=fllr.id AND fllr.is_deleted='0')
        INNER JOIN wifi_location wl ON (wl.id=fllr.loc_id)
		INNER JOIN `foodle_loyalty_reward` fl ON (fl.id=fllr.reward_id AND fl.is_deleted='0') WHERE fcs.user='".$jsondata->mobile."' and fc.reward_id='".$jsondata->reward_id."' GROUP BY fcs.`campaign_id`
        "); 
		$rewarddatar=$rewardq->row_array();
		$campaignid=$rewarddatar['campaignid'];
		
 }else{
	 	$rewardq=$this->db2->query("select fllr.id as reward_id,fllr.reward_name,fllr.loc_id,fllr.slab_amt,wl.location_name,fllr.reward_name,fl.reward_image
	
	from foodle_location_loyalty_reward  fllr 	inner join wifi_location wl ON (wl.id=fllr.loc_id)
	INNER JOIN `foodle_loyalty_reward` fl ON (fl.id=fllr.reward_id AND fl.is_deleted='0')
	where fllr.is_campaign='0' and fllr.is_deleted=0 and fllr.id='".$jsondata->reward_id."'");
 }
 
 //echo $this->db2->last_query(); die;
	
	$userq=$this->db2->query("select amount,loc_id from foodle_user_wallet where user='".$jsondata->mobile."'");
	$useramtarr=array();
	if($userq->num_rows()>0){
	    foreach($userq->result() as $val){
		$useramtarr[$val->loc_id]=$val->amount;
		
	    }
	}
	
	if($rewardq->num_rows()>0){
	    $data['resultcode']=200;
	    $data['resultmsg']="Success";
		//echo "<pre>"; print_R($rewardq->result()); die;
	    foreach($rewardq->result() as $valx){
		
		     if(isset($useramtarr[$valx->loc_id])){
				// echo "====>>".$useramtarr[$valx->loc_id].":::".$valx->slab_amt;
			$islocked=($useramtarr[$valx->loc_id]>=$valx->slab_amt)?0:1;
			if($islocked==1){
			    $fund_to_unlock=$valx->slab_amt-$useramtarr[$valx->loc_id];
			}else{
			    $fund_to_unlock=0;
			}
		    }else{
			$islocked=1;
			$fund_to_unlock=$valx->slab_amt;
		    }
			
			if(isset($valx->campaign_type)){
				if($valx->campaign_type==0){
					$islocked=0;
					$valx->slab_amt=0;
				}
				
				//$val1->slab_amt=0;
			}
			
			
			$fnldata['is_locked']=$islocked;
			$fnldata['location_name']=$valx->location_name;
			$fnldata['spends']=$useramtarr[$valx->loc_id];
			$fnldata['reward_id']=$valx->reward_id;
			$fnldata['reward_name']=$valx->reward_name;
			$fnldata['location_name']=$valx->location_name;
			$fnldata['loc_id']=$valx->loc_id;
			$fnldata['image']=FOODLE_IMAGEPATH.trim($valx->reward_image);
			$fnldata['original_image']=FOODLE_IMAGEPATH.trim($valx->reward_image);
			$fnldata['slab_amt']=number_format($valx->slab_amt);
			$fnldata['fund_to_unlock']=number_format($fund_to_unlock);
			
			$qrcode="";
			
			if($jsondata->is_campaign==1){
				$getredeemq=$this->db2->query("select id,qr_code from foodle_unlocked_offer where reward_id='".$valx->reward_id."' and loc_id='".$valx->loc_id."' and user='".$jsondata->mobile."'");
			    if($getredeemq->num_rows()>0){
				$rowdata=$getredeemq->row_array();
				$qrcode=$rowdata['qr_code'];
			    }else{
				$qrcode=strtoupper(random_string('alnum',8));
				$tabledata=array("reward_id"=>$valx->reward_id,"user"=>$jsondata->mobile,"campaign_id"=>$campaignid,"is_campaign"=>1,"loc_id"=>$valx->loc_id,"reward_amt"=>$valx->slab_amt,"opened_on"=>date("Y-m-d H:i:s"),"is_redeemed"=>"0","qr_code"=>$qrcode);
				$this->db2->insert("foodle_unlocked_offer",$tabledata);
			    }
			}else{
				if($islocked==0){
			    $getredeemq=$this->db2->query("select id,qr_code from foodle_unlocked_offer where reward_id='".$valx->reward_id."' and loc_id='".$valx->loc_id."' and user='".$jsondata->mobile."'");
			    if($getredeemq->num_rows()>0){
				$rowdata=$getredeemq->row_array();
				$qrcode=$rowdata['qr_code'];
			    }else{
				$qrcode=strtoupper(random_string('alnum',8));
				$tabledata=array("reward_id"=>$valx->reward_id,"user"=>$jsondata->mobile,"loc_id"=>$valx->loc_id,"reward_amt"=>$valx->slab_amt,"opened_on"=>date("Y-m-d H:i:s"),"is_redeemed"=>"0","qr_code"=>$qrcode);
				$this->db2->insert("foodle_unlocked_offer",$tabledata);
			    }
				}
			}
			
			
			$fnldata['qrcode']=$qrcode;
			$data['offerdata']=$fnldata;
			
		    
	    }
	}else{
	    $data['resultcode']=401;
	    $data['resultmsg']="Offer Not Available";
	}
	return $data;
    }
	
	
    
    public function add_paytmplan_ap(){
		$planarr=array(8,9,10,11,13);
		$locquery=$this->db->query("select location_uid from wifi_location where status='1' and is_deleted='0'");
		$locarr=array();
		foreach($locquery->result() as $val){
			$locarr[]=$val->location_uid;
		}
		//echo "<pre>"; print_R($locarr); die;
		
		foreach($locarr as $valx){
			foreach($planarr as $planid){
				$query=$this->db->query("select id from middleware_location_plan where location_uid='".$valx."' and plan_id='".$planid."'");
				if($query->num_rows()==0){
					$tabledata=array("location_uid"=>$valx,"plan_id"=>$planid,"created_on"=>date("Y-m-d H:i:s"));
					$this->db->insert("middleware_location_plan",$tabledata);
					echo $this->db->last_query()."<br/>"; 
				}
			}
		}
		
		//acces point 
		$netID=157; // CCD LIVE
		$apq=$this->db->query("select macid from wifi_location_access_point where router_type='6'");
		$aparr=array();
		foreach($apq->result() as $apval){
		    if($apval->macid!=''){
			$apval->macid=strtolower(str_replace("-",":",$apval->macid));
			$apq=$this->db->query("select id from paytm_ap_combination where ap_wifi='".$apval->macid."'");
			if($apq->num_rows()==0){
				$tabledata=array("ap_ether"=>$apval->macid,"ap_wifi"=>$apval->macid,"onehop_netID"=>$netID,"added_on"=>date("Y-m-d H:i:s"));
				$this->db->insert("paytm_ap_combination",$tabledata);
				echo $this->db->last_query()."<br/>"; 
			}
		    }
			
		}
		
		
		
	}
	
	public function user_registration($jsondata){
		$query=$this->db2->query("select id,password from foodle_user where user='".$jsondata->mobile."'");
		if($query->num_rows()>0){
			$data['resultcode']=401;
			$data['resultmsg']="User Already Registered";
			$userd=$query->row_array();
			if($userd['password']!=''){
				$password= $this->encrypt->encode($jsondata->password);
				$tabledata=array("user"=>$jsondata->mobile,"firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,
				"email"=>$jsondata->email,"password"=>$password,"age_group"=>$jsondata->age_group,"gender"=>$jsondata->gender);
				$this->db2->update("foodle_user",$tabledata,array("id"=>$userd['id']));
			}
			$data['mobile']=$jsondata->mobile;
			$data['firstname']=$jsondata->firstname;
				$data['lastname']=$jsondata->lastname;
				$data['email']=$jsondata->email;
				$data['age_group']=$jsondata->age_group;
				$data['gender']=$jsondata->gender;
			
		}else{
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			$password= $this->encrypt->encode($jsondata->password);
			$tabledata=array("user"=>$jsondata->mobile,"firstname"=>$jsondata->firstname,"lastname"=>$jsondata->lastname,
			"email"=>$jsondata->email,"password"=>$password,"added_on"=>date("Y-m-d H:i:s")
			,"age_group"=>$jsondata->age_group,"gender"=>$jsondata->gender);
			$this->db2->insert("foodle_user",$tabledata);
			$data['firstname']=$jsondata->firstname;
			$data['mobile']=$jsondata->mobile;
				$data['lastname']=$jsondata->lastname;
				$data['email']=$jsondata->email;
				$data['age_group']=$jsondata->age_group;
				$data['gender']=$jsondata->gender;
				$this->set_user_notification($jsondata,0);
		}
		return $data;
	}
	
	public function user_login($jsondata){
		$query=$this->db2->query("select id,password,firstname,lastname,email,age_group,gender from foodle_user where user='".$jsondata->mobile."'");
		if($query->num_rows()>0){
			
			$userd=$query->row_array();
			$decodepwd = $this->encrypt->decode($userd['password']);
			if($decodepwd!=$jsondata->password){
				$data['resultcode']=401;
				$data['resultmsg']="Invalid User";
				$data['decodepwd']=$decodepwd.":::".$jsondata->password;
			}else{
				$data['resultcode']=200;
				$data['resultmsg']="Login Successfull";
				$data['mobile']=$jsondata->mobile;
				$data['firstname']=$userd['firstname'];
				$data['lastname']=$userd['lastname'];
				$data['email']=$userd['email'];;
				$data['age_group']=$userd['age_group'];
				$data['gender']=$userd['gender'];
				$this->set_user_notification($jsondata,0);
				
			}
			
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="Invalid User";
			
		}
		return $data;
	}
	
	public function forget_password($jsondata){
		$password= $this->encrypt->encode($jsondata->password);
		$tabledata=array("password"=>$password,"is_registered"=>1);
		$query=$this->db2->query("select id,password,firstname,lastname,email,age_group,gender from foodle_user where user='".$jsondata->mobile."'");
		if($query->num_rows()>0){
			$userd=$query->row_array();
				$this->db2->update("foodle_user",$tabledata,array("user"=>$jsondata->mobile));
				$data['resultcode']=200;
				$data['resultmsg']="Password updated Succesfully";
				$data['mobile']=$jsondata->mobile;
				$data['firstname']=$userd['firstname'];
				$data['lastname']=$userd['lastname'];
				$data['email']=$userd['email'];;
				$data['age_group']=$userd['age_group'];
				$data['gender']=$userd['gender'];
				
				$this->set_user_notification($jsondata,0);
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="Invalid User";
		}
		return $data;
	}
	
	public function set_user_notification($jsondata,$islogout){
		$mobile=(isset($jsondata->mobile) && $jsondata->mobile!='')?$jsondata->mobile:"";
		$device_id=(isset($jsondata->device_token) && $jsondata->device_token!='')?$jsondata->device_token:"";
		if($mobile!='' && $device_id!=''){
			$query=$this->db2->query("select id from foodle_notification_user where user='".$jsondata->mobile."' and device_id='".$jsondata->device_token."'");
			if($query->num_rows()>0){
				$rowarr=$query->row_array();
				$tabledata=array("is_logout"=>$islogout);
				$this->db2->update("foodle_notification_user",$tabledata,array("id"=>$rowarr['id']));
			}else{
				$tabledata=array("user"=>$mobile,"device_id"=>$device_id,"is_enable"=>1,"is_logout"=>$islogout);
				$this->db2->insert("foodle_notification_user",$tabledata);
				
			}
			
		}
			return true;
	}
	
	public function logout($jsondata){
		$data['resultcode']=200;
		$data['resultmsg']="Logout Succesfully";
		$this->set_user_notification($jsondata,1);
		return $data;
	}
	
	public function redeem_check($jsondata){
		$data=array();
		$query=$this->db2->query("select id from foodle_unlocked_offer where user='".$jsondata->mobile."' and reward_id='".$jsondata->reward_id."' and is_redeemed='1'");
		if($query->num_rows()>0){
			$data['resultcode']=200;
			$data['resultmsg']="Succesfully Redeemed";
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="Redeem Pending";
		}
		return $data;
	}
	
	public function redeem_offer($jsondata){
		$data=array();
		$redeemed_by=(isset($jsondata->redeemed_by))?$jsondata->redeemed_by:0;
		$tabledata=array("is_redeemed"=>1,"redeemed_on"=>date("Y-m-d H:i:s"),"redeemed_by"=>$redeemed_by);
		$query=$this->db2->query("select id,is_redeemed,`loc_id`,`reward_amt`,`reward_id`,`is_campaign`,`campaign_id`,opened_on from foodle_unlocked_offer where user='".$jsondata->mobile."' and reward_id='".$jsondata->reward_id."'");
		if($query->num_rows()>0){
			$rowdata=$query->row_array();
			if($rowdata['is_redeemed']==1){
				$data['resultcode']=401;
				$data['resultmsg']="Offer Already Redeemed";
			}else{
				
				if($rowdata['is_campaign']==1){
					$campaignq=$this->db2->query("select redemption_expiryday from foodle_campaign where id='".$rowdata['campaign_id']."'");
					if($campaignq->num_rows()>0){
						$campaignd=$campaignq->row_array();
						$expirydays=$campaignd['redemption_expiryday']*24*60*60;
						$curtime=strtotime(date("Y-m-d H:i:s"));
						$opened_on=strtotime($rowdata['opened_on']);
						$cutofftime=$curtime-$opened_on;
						//echo "===>>".$expirydays."::::".$cutofftime;die;
						if($cutofftime<=$expirydays){
							$this->db2->update('foodle_unlocked_offer',$tabledata,array("user"=>$jsondata->mobile,"reward_id"=>$jsondata->reward_id));
							$data['resultcode']=200;
							$data['resultmsg']="Redeemed Succesfully";
							$loc_id=$rowdata['loc_id'];
							$reward_amt=$rowdata['reward_amt'];
							$reward_id=$rowdata['reward_id'];
							$tabledata=array("loc_id"=>$loc_id,"reward_id"=>$reward_id
							,"spent_amt"=>$reward_amt,"added_on"=>date("Y-m-d H:i:s"),"user"=>$jsondata->mobile);
							$this->db2->insert('foodle_user_spends',$tabledata);
							
							$this->db2->query("update foodle_user_wallet set amount=amount-{$reward_amt} where loc_id='".$loc_id."' and user='".$jsondata->mobile."'");
							
						}else{
							$data['resultcode']=401;
							$data['resultmsg']="Offer Redemption Period Expired";
						}
						
					}else{
						$this->db2->update('foodle_unlocked_offer',$tabledata,array("user"=>$jsondata->mobile,"reward_id"=>$jsondata->reward_id));
					$data['resultcode']=200;
					$data['resultmsg']="Redeemed Succesfully";
					$loc_id=$rowdata['loc_id'];
					$reward_amt=$rowdata['reward_amt'];
					$reward_id=$rowdata['reward_id'];
					$tabledata=array("loc_id"=>$loc_id,"reward_id"=>$reward_id
					,"spent_amt"=>$reward_amt,"added_on"=>date("Y-m-d H:i:s"),"user"=>$jsondata->mobile);
					$this->db2->insert('foodle_user_spends',$tabledata);
					$this->db2->query("update foodle_user_wallet set amount=amount-{$reward_amt} where loc_id='".$loc_id."' and user='".$jsondata->mobile."'");
					}
				}else{
					$this->db2->update('foodle_unlocked_offer',$tabledata,array("user"=>$jsondata->mobile,"reward_id"=>$jsondata->reward_id));
					$data['resultcode']=200;
					$data['resultmsg']="Redeemed Succesfully";
					$loc_id=$rowdata['loc_id'];
					$reward_amt=$rowdata['reward_amt'];
					$reward_id=$rowdata['reward_id'];
					$tabledata=array("loc_id"=>$loc_id,"reward_id"=>$reward_id
					,"spent_amt"=>$reward_amt,"added_on"=>date("Y-m-d H:i:s"),"user"=>$jsondata->mobile);
					$this->db2->insert('foodle_user_spends',$tabledata);
					$this->db2->query("update foodle_user_wallet set amount=amount-{$reward_amt} where loc_id='".$loc_id."' and user='".$jsondata->mobile."'");
				}
				
			}
			
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="Offer Not Unlocked with User";
		}
		return $data;
		
	}
	
	public function refer_campaign($jsondata){
		
		foreach($jsondata->mobile as $val){
			$tabledata=array("user"=>$val,"campaign_id"=>$jsondata->campaign_id,"sent_on"=>date("Y-m-d H:i:s"),"loc_id"=>$jsondata->loc_id,
			"is_referred"=>1,"referred_by"=>$jsondata->referred_by);
			$this->db2->insert('foodle_campaign_sent',$tabledata);
		}
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		return $data;
	}
	
	public function team_login($jsondata){
		
		$query=$this->db2->query("select flt.id as teamid,flt.permission_type,flt.`firstname`,flt.`lastname`,flt.`mobile`,flt.`loc_id`,flt.password,wl.location_name 
		from foodle_location_team flt
		inner join wifi_location wl on (wl.id=flt.loc_id)
		 where flt.mobile='".$jsondata->mobile."'" );
		if($query->num_rows()>0){
			//$rowd=$query->row_array();
			foreach($query->result() as $val){
				$decodepwd = $this->encrypt->decode($val->password);
				if($decodepwd!=$jsondata->password){
					$data['resultcode']=401;
					$data['resultmsg']="User not Valid";
				}else{
					$data['resultcode']=200;
					$data['resultmsg']="Login Successfull";
					$data['firstname']=$val->firstname;
					$data['lastname']=$val->lastname;
					$data['mobile']=$val->mobile;
					$data['loc_id']=$val->loc_id;
					$data['location_name']=$val->location_name;
					$data['teamid']=$val->teamid;
					$data['permission_type']=$val->permission_type;
					return $data;
				}
			}
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="User not Valid";
		}
		return $data;
	}
	
	public function scan_barcode($jsondata){
		$query=$this->db2->query("select wl.location_name,fuo.is_redeemed,fuo.user,fuo.reward_id,fllr.reward_name,fuo.reward_amt,flr.reward_image from foodle_unlocked_offer fuo
		inner join foodle_location_loyalty_reward fllr on (fllr.id=fuo.reward_id) 
		inner join foodle_loyalty_reward flr on (fllr.reward_id=flr.id)	
		inner join wifi_location wl on (fllr.loc_id=wl.id) where fuo.qr_code='".$jsondata->qr_code."' and fuo.loc_id='".$jsondata->loc_id."'");
		//echo $this->db2->last_query(); die;
		if($query->num_rows()>0){
			$rowd=$query->row_array();
			if($rowd['is_redeemed']==1){
				$data['resultcode']=401;
				$data['resultmsg']="Already Redeemed";
			}else{
				$data['resultcode']=200;
				$data['resultmsg']="Success";
				$data['qrcode']=$jsondata->qr_code;
				$data['reward_name']=$rowd['reward_name'];
				$data['reward_amt']=number_format($rowd['reward_amt']);
				$data['reward_id']=$rowd['reward_id'];
				$data['user']=$rowd['user'];
				$data['location_name']=$rowd['location_name'];
				$data['reward_image']=FOODLE_IMAGEPATH.trim($rowd['reward_image']);
				
			}
			
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="Item not present";
		}
		return $data;
	}
	
	
	
	public function version_check($jsondata){
		$data=array();
		$query=$this->db2->query("select version,force_update from foodle_app_version 
		where platform='".$jsondata->platform."' and app_type='".$jsondata->app."' order by id desc limit 1");
		if($query->num_rows()>0){
			$rowd=$query->row_array();
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			$data['version']=$rowd['version'];
			$data['force_update']=$rowd['force_update'];
			
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="No Version Available";
		}
		return $data;
		
	}
	
	public function refer_campaign_msg($jsondata){
		$query=$this->db2->query("SELECT wl.location_name,fllr.reward_name FROM `foodle_campaign` fc
		INNER JOIN `foodle_location_loyalty_reward` fllr ON (fc.reward_id=fllr.id AND fllr.is_deleted='0')
		INNER JOIN `foodle_loyalty_reward` flr ON (fllr.reward_id=flr.id)
		INNER JOIN wifi_location wl ON (wl.id=fc.loc_id) where fc.id='".$jsondata->campaign_id."'");
		if($query->num_rows()>0){
			$rowd=$query->row_array();
			$msg="Your Friend Has referred a ".$rowd['reward_name']." at ".$rowd['location_name'].".Redeem now www.foodle.me";
			foreach($jsondata->mobile as $val){
				$this->send_promotional_msg();
			}
			
		}
	}
	
	
	
	public function send_promotional_msg(){
		
	}
	
	public function invoice_list($jsondata){
		
		$data=array();
		$invoced=array();
		$query=$this->db2->query("select id as invoice_id,`user`,`amount`,`invoice_number`,uploaded_on from foodle_location_userinvoice where loc_id='".$jsondata->loc_id."' and uploaded_by='".$jsondata->team_id."' order by id desc");
		if($query->num_rows()>0){
			$i=0;
			foreach($query->result() as $val){
				$invoced[$i]['invoice_id']=$val->invoice_id;
				$invoced[$i]['user']=$val->user;
				$invoced[$i]['amount']=(int)$val->amount;
				$invoced[$i]['invoice_number']=$val->invoice_number;
				$invoced[$i]['uploaded_on']=date("jS M H:i",strtotime($val->uploaded_on));
				$i++;
			}
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			$data['invoicedata']=$invoced;
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="No Invoice uploaded";
		}
		//echo "<pre>"; print_R($data); die;
		return $data;
	}
	
	public function invoice_upload($jsondata){
		
		
		if($jsondata->invoice_id!=''){
			$tabledata=array("user"=>$jsondata->user,"loc_id"=>$jsondata->loc_id,"amount"=>$jsondata->invoice_amt,"invoice_number"=>$jsondata->invoice_no,
		"uploaded_by"=>$jsondata->team_id,"updated_on"=>date("Y-m-d H:i:s"));
		$previousamtq=$this->db2->query("select amount from foodle_location_userinvoice where id='".$jsondata->invoice_id."'");
		$prevoiusd=$previousamtq->row_array();
		$prevamt=$prevoiusd['amount'];
		$fnlamt=$jsondata->invoice_amt-$prevamt;
		$this->db2->query("Update foodle_user_spend_visit_earn set spend_amt=spend_amt+{$fnlamt} where
			user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."' and is_visit=0 order by id desc limit 1");
		
		$this->db2->update("foodle_location_userinvoice",$tabledata,array("id"=>$jsondata->invoice_id));
		$useracct=$this->db2->query("select id from foodle_user_wallet where user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."'");
		if($useracct->num_rows()>0){
			$this->db2->query("Update foodle_user_wallet set amount=amount+{$fnlamt} where
			user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."' order by id desc limit 1");
		}else{
			$tabledataw=array("loc_id"=>$jsondata->loc_id,"user"=>$jsondata->user,"amount"=>$fnlamt,"visits"=>1,"added_on"=>date("Y-m-d H:i:s"));
			$this->db2->insert("foodle_user_wallet",$tabledataw);	
		}
		
		
		}else{
			$tabledata=array("user"=>$jsondata->user,"loc_id"=>$jsondata->loc_id,"amount"=>$jsondata->invoice_amt,"invoice_number"=>$jsondata->invoice_no,
		"uploaded_by"=>$jsondata->team_id,"uploaded_on"=>date("Y-m-d H:i:s"));
		$this->db2->insert("foodle_location_userinvoice",$tabledata);
		$query=$this->db2->query("select id from foodle_user_spend_visit_earn where user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."' and added_on=curdate()");
		if($query->num_rows()>0){
			$this->db2->query("Update foodle_user_spend_visit_earn set spend_amt=spend_amt+{$jsondata->invoice_amt} where
			user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."' and is_visit=0");
			
		}else{
			$tabledata=array("user"=>$jsondata->user,"spend_amt"=>$jsondata->invoice_amt,"loc_id"=>$jsondata->loc_id,"added_on"=>date("Y-m-d H:i:s"));
			$this->db2->insert("foodle_user_spend_visit_earn",$tabledata);
			
			$tabledata1=array("user"=>$jsondata->user,"visit"=>1,"loc_id"=>$jsondata->loc_id,"added_on"=>date("Y-m-d H:i:s"),"is_visit"=>1);
			$this->db2->insert("foodle_user_spend_visit_earn",$tabledata1);
		}
		
		$useracct=$this->db2->query("select id from foodle_user_wallet where user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."'");
		if($useracct->num_rows()>0){
			$this->db2->query("Update foodle_user_wallet set amount=amount+{$jsondata->invoice_amt},visits=visits+1 where
			user='".$jsondata->user."' and loc_id='".$jsondata->loc_id."' order by id desc limit 1");
		}else{
			$tabledataw=array("loc_id"=>$jsondata->loc_id,"user"=>$jsondata->user,"amount"=>$jsondata->invoice_amt,"visits"=>1,"added_on"=>date("Y-m-d H:i:s"));
			$this->db2->insert("foodle_user_wallet",$tabledataw);	
		}
			
		}
		
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		return $data;
		
	}
	
	public function foodle_terms_privacy($jsondata){
		$data=array();
		
		$query=$this->db2->query("select tc,privacy from foodle_terms_policy");
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		if($query->num_rows()>0){
			$rowdata=$query->row_array();
			if($jsondata->type=="terms"){
				$data['content']=$rowdata['tc'];
			}else{
				
				$data['content']=$rowdata['privacy'];
			}
		}else{
			$data['content']="";
		}
		
		
		return $data;
	}
	
	public function user_spends($jsondata){
		
		$data=array();
		$query=$this->db2->query(" SELECT COUNT(fus.id) AS redeem,SUM(fus.`spent_amt`) AS spend,DATE(fus.`added_on`) AS added_on,wl.location_name FROM `foodle_user_spends` fus 
  INNER JOIN wifi_location wl ON (fus.loc_id=wl.id)
   WHERE `user`='".$jsondata->user."' GROUP BY DATE(fus.`added_on`),fus.`loc_id` order by fus.`added_on` desc");
		if($query->num_rows()>0){
			$spendarr=array();
			foreach($query->result() as $val){
				$spendarr[$val->added_on][]=$val;
			}
			$fnldata=array();
			$i=0;
			foreach($spendarr as $keyd => $vald){
				$fnldata[$i]['date']=date("d/m/Y",strtotime($keyd));
				$x=0;
				foreach($vald as $valx){
					
					$fnldata[$i]['details'][$x]['cafe_name']=$valx->location_name;
					$fnldata[$i]['details'][$x]['spends']=number_format($valx->spend);
					$fnldata[$i]['details'][$x]['redeems']=$valx->redeem;
					$x++;
				}
				$i++;
			}
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			$data['spend_data']=$fnldata;
			
			
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="No Spends yet";
		}
		return $data;
		
	}
	
	public function user_profile($jsondata){
		$data=array();
		$query=$this->db2->query("SELECT `firstname`,`lastname`,`email`,`age_group`,`gender` FROM `foodle_user` WHERE `user`='".$jsondata->user."'");
		if($query->num_rows()>0){
			$data['resultcode']=200;
			$data['resultmsg']="Success";
			$rowdata=$query->row_array();
			$data['full_name']=$rowdata['firstname']." ".$rowdata['lastname'];
			$data['phone_number']=$jsondata->user;
			$data['email']=$rowdata['email'];
			$data['gender']=($rowdata['gender']=="M")?"Male":"Female";
			$data['dob']="";
			$data['profile_image']='';
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="No User";
		}
		return $data;
	}
	
	public function edit_profile($jsondata){
		$data=array();
		if($jsondata->phone_number!=''){
			$query=$this->db2->query("SELECT id from foodle_user WHERE `user`='".$jsondata->phone_number."'");
			if($query->num_rows()>0){
				$namearr=explode(" ",$jsondata->full_name);
				$firstname=$namearr[0];
				$lastname=(isset($namearr[0]))?$namearr[0]:"";
				$gender=(isset($jsondata->gender) && $jsondata->gender!='')?strtoupper(substr($jsondata->gender,0,1)):"";
				$dob=(isset($jsondata->dob) && $jsondata->dob!='')?date("Y-m-d",strtotime($jsondata->dob)):"";
				$base_url='assets/images';
				$userimage='';
				if($jsondata->image!=''){
					$image_parts = explode(";base64,", $invoice_get->invoice_image);
					$image_type_aux = explode("image/", $image_parts[0]);
					$image_type = $image_type_aux[1];
					$image_base64 = base64_decode($image_parts[1]);
					$file_name = uniqid() . '.jpg';
					$logo_name = $file_name;
					$file = $base_url . $file_name;
					file_put_contents($file, $image_base64);
					$file_path_server = $base_url . '/'.$logo_name;
					$famazonname = AMAZONPATH.'/foodleuser/'.$logo_name;
					$this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
					$this->s3->putObjectFile($file_path_server, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
					unlink($file_path_server);
				}
				$tabledataparam=array();
				
				$tabledata=array("firstname"=>$firstname,"lastname"=>$lastname,"gender"=>$gender);
				if($userimage!=''){
					$tabledata=array_merge($tabledata,array("profile_img"=>$logo_name));
				}
				if($jsondata->email!=''){
					$tabledata=array_merge($tabledata,array("email"=>$jsondata->email));
				}
				if($dob!=''){
					$tabledata=array_merge($tabledata,array("dob"=>$dob));
				}
				
				$this->db2->update("foodle_user",$tabledata,array("user"=>$jsondata->phone_number));
				$data['resultcode']=200;
				$data['resultmsg']="Profile Updated";
			}else{
				$data['resultcode']=401;
				$data['resultmsg']="No User";
			}
			
		}else{
			$data['resultcode']=401;
			$data['resultmsg']="No User";
		}
		return $data;
	}
    
    
    
} 