<?php
class Offlineapitest_model extends CI_Model{
	
	

	  public function __construct() {
        parent::__construct();
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
		$path = OFFLINE_DIR;

        if($path != ''){

            $path = $path;
				$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
			if( $fldrperm != 0777) {

                $output = exec("sudo chmod -R 0777 \"$path\"");
				}
		}
	
    }
	
	  public function log($request, $responce) {
        $log_data = array(
            'request' => $request,
            'response' => $responce,
            'added_on' =>date("Y-m-d H:i:s")
            
        );
        $this->DB2->insert('offline_sync_logs', $log_data);
    }
	
	public function sync_datacontent($jsondata)
	{
		$data=array();
	//	echo "<pre>"; print_R($jsondata); die;
		if($jsondata->macId=='')
		{
			$data['resultcode']=0;
			$data['resultmsg']="Please send MacID";
			return $data;
		}
		else{
			
			foreach($jsondata->macId as $val)
			{
				$macarr[]=strtolower($val);
			}
			$macarr=array_filter($macarr);
			if(empty($macarr))
			{
				$data['resultcode']=0;
			$data['resultmsg']="Please send MacID";
			return $data;
			}
			 $macids='"'.implode('", "', $macarr).'"';
			 
			
			$query=$this->DB2->query("select wl.id,wl.isp_uid,wls.macid from wifi_location_access_point  wls
		inner join wifi_location wl on (wls.location_uid=wl.location_uid) where lower(wls.macid) in (".$macids.") and  wls.macid is not NULL");
	
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['resultMessage']="Success";
			$isp_uid=$rowarr['isp_uid'];
			$locid=$rowarr['id'];
			$macidsync=$rowarr['macid'];
			$macidfolder=strtolower(str_replace(":","-",$macidsync));
			
			$query1= $this->DB2->query("select wl.id,wl.location_uid,wl.isp_uid,wl.location_name,wl.geo_address,wlc.offline_cp_source_folder_path,wlc.cp_type,wlc.location_id,wlc.`offline_content_title`,wlc.`offline_content_desc`,wlc.uid_location,wlc.offline_cp_content_type,wlc.main_ssid,wlc.retail_original_image,wlc.offline_cp_landing_page_path,wlc.apikey,wlc.offline_version,wlc.frequency_hour,wlc.is_offline_registration,wlc.custom_slider_image1 from wifi_location  wl
			inner join wifi_location_cptype wlc on (wlc.location_id=wl.id) where wl.id='".$locid."' and (wlc.cp_type='cp_offline' or wlc.cp_type='cp_visitor' or wlc.cp_type='cp_contestifi')");
			if($query1->num_rows()>0)
			{
				
				
				$brandlogoarr=$this->brand_logo($locid);
				
				$this->load->library('zip');
				$rowarr=$query1->row_array();
				if($rowarr['cp_type']=="cp_offline")
				{
					$dynamicpath=OFFLINE_DIR."offlinedynamic";
					
					$zippath=OFFLINE_DIR."offline_zip/".$macidfolder."/offlinedynamic.zip";
				}else{
					$dynamicpath=OFFLINE_DIR."visitoroffline";
					$zippath=OFFLINE_DIR."offline_zip/".$macidfolder."/offlinedynamic.zip";
				}
				
					$rowarr['offline_cp_source_folder_path']=$zippath;
					$rowarr['offline_cp_landing_page_path']=$dynamicpath."/index.php";
					$locuid=$rowarr['location_uid'];
				
				
				//echo "sssss";die;
				//echo "sssss";die;
				if($jsondata->buildid!='')
				{
					$cond=false;
					if($jsondata->apikey==$rowarr['apikey']){
						if(isset($jsondata->syncincomplete))
						{
							if($jsondata->syncincomplete==1)
							{
								$cond=($jsondata->buildid<=$rowarr['offline_version'])?true:false;
							}else{
								$cond=($jsondata->buildid<$rowarr['offline_version'])?true:false;
							}
						}
						else{
							$cond=($jsondata->buildid<$rowarr['offline_version'])?true:false;
						}
						
					
						if($cond){
							//echo $rowarr['offline_cp_source_folder_path']; die;
							
							if (file_exists($rowarr['offline_cp_source_folder_path'])) {
								//$this->zip->add_dir($rowarr['offline_cp_source_folder_path']);
								$path = $rowarr['offline_cp_source_folder_path']."/";
								//$this->zip->read_dir($path,FALSE);
								//$this->zip->archive($rowarr['offline_cp_source_folder_path'].'.zip');
								$data['resultCode']=1;
								$data['resultMessage']="Success";
								$data['cp_sourcepath']=$rowarr['offline_cp_source_folder_path'];
								$folderarr=explode("/",$rowarr['offline_cp_source_folder_path']);
								$data['foldername']=str_replace(".zip","",end($folderarr));
								$data['zip_path']=$rowarr['offline_cp_source_folder_path'];
								$data['zipsize']=filesize($rowarr['offline_cp_source_folder_path']);
								$filearr=explode("/",$rowarr['offline_cp_source_folder_path']);
								$data['filename']= end($filearr);
								$data['cp_landingpath']=$rowarr['offline_cp_landing_page_path'];
								$data['location_uid']=$rowarr['uid_location'];
								$data['synctimingkey']=$rowarr['frequency_hour'];
								$data['location_id']=$rowarr['location_id'];
								$data['offline_content_title']=$rowarr['offline_content_title'];
								$data['cptype']=$rowarr['offline_cp_content_type'];
								$data['offline_content_desc']=$rowarr['offline_content_desc'];
								$data['registration_enable']=$rowarr['is_offline_registration'];
								$data['brand_imagepath']=$brandlogoarr['brand_imagepath'];
								$data['brand_image']=$this->image_copy($brandlogoarr['brand_image']);
								$data['banner_image_path']=($rowarr['retail_original_image']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr['retail_original_image']:'';
								$data['banner_image']=($rowarr['retail_original_image']!='')?$this->image_copy($data['banner_image_path']):"";
								$data['logo2_image_path']=($rowarr['custom_slider_image1']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr['custom_slider_image1']:'';
								$data['logo2_image']=($rowarr['custom_slider_image1']!='')?$this->image_copy($data['logo2_image_path']):"";
								
								$data['dbpathdynamic']=OFFLINE_DIR."raspberryDB/radius.sql";
								$data['dbname']="radius.sql";
								$data['location_name']=$rowarr['location_name'];
								$data['geo_address']=$rowarr['geo_address'];
								$loc_id=$rowarr['location_id'];
								
								
								if($rowarr['offline_cp_content_type']==1)
								{
									//sql generate
									
									
								 $data['dynamic_data']=$this->dynamic_data($loc_id,$macidsync);
								
								 $data['dynamic_data'][]=($data['brand_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['brand_image']:'';
								 $data['dynamic_data'][]=($data['banner_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['banner_image']:'';
								 $data['dynamic_data'][]=($data['logo2_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['logo2_image']:'';
								$data['dynamic_data']=array_unique($data['dynamic_data']);	
								$data['dynamic_data']=$this->filter_dynamic_data($data['dynamic_data'],$macidsync);		
								$data['resourcescount']	=count($data['dynamic_data']);
								 $resourcesize=0;
								 $resourcesize=$resourcesize+filesize($data['dbpathdynamic']);
								 $filearr=array();
								 foreach($data['dynamic_data'] as $valf)
								 {
									$resourcesize=$resourcesize+filesize($valf);
								}
								 $data['resourcessize']=$resourcesize;
								 $data['totalpackage_size']=$resourcesize+$data['zipsize'];
								}
								else{
									 $data['totalpackage_size']=$data['zipsize'];
								}
								
								
								
								
								$data['ssid']=$rowarr['main_ssid'];
								$data['buildid']=$rowarr['offline_version'];
								$data['apikey']=$rowarr['apikey'];
								
								//update db data
								
								$this->DB2->update("wifi_location_cptype",array("offline_version_synced"=>$rowarr['offline_version']),array("location_id"=>$loc_id));
								$this->DB2->update("offline_content",array("is_synced"=>1),array("location_id"=>$loc_id));
								
								} else {
									$data['resultCode']=0;
										$data['resultMessage']="Folder Not Present";
										$data['cp_sourcepath']="";
										$data['cp_landingpath']="";
								}
							
						}else{
							$data['resultCode']=0;
							$data['resultMessage']="No update Available";
						}
						
					}else{
						$data['resultCode']=0;
						$data['resultMessage']="Api key Invalid";
					}
					
				}else{
					
						$array = array('macid' => $macidsync);
								$this->DB2->where($array); 
								$this->DB2->delete('offline_macid_synced_detail'); 
								
								//create zip and db first time
								$_POST['filedata'] =array("location_uid"=>$locuid);
								$decodeData = json_decode(json_encode($_POST['filedata']));
								 $this->publicwifi_model->create_zip_for_offline_content($decodeData);
								
					
				if (file_exists($rowarr['offline_cp_source_folder_path'])) {
					//echo "sss";die;
				
							$path = $rowarr['offline_cp_source_folder_path']."/";
							//	$this->zip->read_dir($path,FALSE);
								//$this->zip->archive($rowarr['offline_cp_source_folder_path'].'.zip');
								$data['resultCode']=1;
								$data['resultMessage']="Success";
								$data['cp_sourcepath']=$rowarr['offline_cp_source_folder_path'];
								$folderarr=explode("/",$rowarr['offline_cp_source_folder_path']);
								$data['foldername']=str_replace(".zip","",end($folderarr));
								$data['zip_path']=$rowarr['offline_cp_source_folder_path'];
								$data['zipsize']=filesize($rowarr['offline_cp_source_folder_path']);
								$filearr=explode("/",$rowarr['offline_cp_source_folder_path']);
								$data['filename']= end($filearr);
								$data['cp_landingpath']=$rowarr['offline_cp_landing_page_path'];
								$data['synctimingkey']=$rowarr['frequency_hour'];
								$data['brand_imagepath']=$brandlogoarr['brand_imagepath'];
								$data['brand_image']=$this->image_copy($brandlogoarr['brand_image']);
								$data['banner_image_path']=($rowarr['retail_original_image']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr['retail_original_image']:'';
								$data['banner_image']=($rowarr['retail_original_image']!='')?$this->image_copy($data['banner_image_path']):"";
								$data['logo2_image_path']=($rowarr['custom_slider_image1']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr['custom_slider_image1']:'';
								$data['logo2_image']=($rowarr['custom_slider_image1']!='')?$this->image_copy($data['logo2_image_path']):"";
								$data['buildid']=$rowarr['offline_version'];
								$data['ssid']=$rowarr['main_ssid'];
								$data['registration_enable']=$rowarr['is_offline_registration'];
								$data['location_id']=$rowarr['location_id'];
								$data['offline_content_title']=$rowarr['offline_content_title'];
								$data['offline_content_desc']=$rowarr['offline_content_desc'];
								$data['location_uid']=$rowarr['uid_location'];
								$data['cptype']=$rowarr['offline_cp_content_type'];
								$data['apikey']=$rowarr['apikey'];
								$data['dbpathdynamic']=OFFLINE_DIR."raspberryDB/radius.sql";
								$data['dbname']="radius.sql";
								$data['location_name']=$rowarr['location_name'];
								$data['geo_address']=$rowarr['geo_address'];
								$loc_id=$rowarr['location_id'];
								
								
								
								if($rowarr['offline_cp_content_type']==1)
								{
									//sql generate
								$data['dynamic_data']=$this->dynamic_data($loc_id,$macidsync);
								   
								 $data['dynamic_data'][]=($data['brand_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['brand_image']:'';
								 $data['dynamic_data'][]=($data['banner_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['banner_image']:'';
								 $data['dynamic_data'][]=($data['logo2_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['logo2_image']:'';
								 
								 $data['dynamic_data']=array_unique($data['dynamic_data']);
								 $data['dynamic_data']=$this->filter_dynamic_data($data['dynamic_data'],$macidsync);
								$data['resourcescount']	=count($data['dynamic_data']);								 
								 $resourcesize=0;
								 $resourcesize=$resourcesize+filesize($data['dbpathdynamic']);
								 $filearr=array();
								 foreach($data['dynamic_data'] as $valf)
								 {
									$resourcesize=$resourcesize+filesize($valf);
								}
								 $data['resourcessize']=$resourcesize;
								 $data['totalpackage_size']=$resourcesize+$data['zipsize'];

								}else{
									 $data['totalpackage_size']=$data['zipsize'];
								}
									//update db data
								
								
								$this->DB2->update("wifi_location_cptype",array("offline_version_synced"=>$rowarr['offline_version']),array("location_id"=>$loc_id));
								$this->DB2->update("offline_content",array("is_synced"=>1),array("location_id"=>$loc_id));
								
							
								
							
				} else {
					$data['resultCode']=0;
						$data['resultMessage']="Folder Not Present";
						$data['cp_sourcepath']="";
						$data['cp_landingpath']="";
				}
				}
				
			}
		
			
		}else{
				$data['resultCode']=0;
			$data['resultMessage']="No Location Found";
		}
		}
		//echo "<pre>"; print_R($data); die;
		return $data;
			
		
	}
	
	
	public function sync_db($jsondata)
	{
		$data=array();
		//echo "<pre>"; print_R($jsondata); die;
		if($jsondata->macId=='')
		{
			$data['resultcode']=0;
			$data['resultmsg']="Please send MacID";
		}
		else{
			
			foreach($jsondata->macId as $val)
			{
				$macarr[]=strtolower($val);
			}
			 $macids='"'.implode('", "', $macarr).'"';
			
			$query=$this->DB2->query("select wl.id,wl.isp_uid from wifi_location_access_point  wls
		inner join wifi_location wl on (wls.location_uid=wl.location_uid) where lower(wls.macid) in (".$macids.") and  wls.macid is not NULL");
	//	echo $this->DB2->last_query(); die;
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['resultMessage']="Success";
			$isp_uid=$rowarr['isp_uid'];
			$locid=$rowarr['id'];
			
			$query1= $this->DB2->query("select wl.id,wl.isp_uid,wl.location_name,wl.geo_address,wlc.offline_cp_source_folder_path,wlc.location_id,wlc.uid_location,wlc.`offline_content_title`,wlc.`offline_content_desc`,wlc.offline_cp_content_type,wlc.main_ssid,wlc.retail_original_image,wlc.offline_cp_landing_page_path,wlc.apikey,wlc.offline_version,wlc.frequency_hour,wlc.cp_type,wlc.custom_slider_image1 from wifi_location  wl
			inner join wifi_location_cptype wlc on (wlc.location_id=wl.id) where wl.id='".$locid."' and (wlc.cp_type='cp_offline' or wlc.cp_type='cp_visitor' or wlc.cp_type='cp_contestifi')");
			if($query1->num_rows()>0)
			{
				
				$brandlogoarr=$this->brand_logo($locid);
				//echo "<pre>"; print_R($brandlogoarr);die;
				$this->load->library('zip');
				$rowarr=$query1->row_array();
				if($rowarr['cp_type']=="cp_offline")
				{
					$dynamicpath=OFFLINE_DIR."offlinedynamic";
				}else{
					$dynamicpath=OFFLINE_DIR."visitoroffline";
				}
				
				if($rowarr['offline_cp_content_type']==1)
				{
					$rowarr['offline_cp_source_folder_path']=$dynamicpath;
					$rowarr['offline_cp_landing_page_path']=$dynamicpath."/index.php";
				}
				//echo "sssss";die;
				//echo "sssss";die;
				if($jsondata->buildid!='')
				{
					if($jsondata->apikey==$rowarr['apikey']){
					
							if (file_exists($rowarr['offline_cp_source_folder_path'])) {
								//$this->zip->add_dir($rowarr['offline_cp_source_folder_path']);
								$path = $rowarr['offline_cp_source_folder_path']."/";
								
								$data['resultCode']=1;
								$data['resultMessage']="Success";
								$data['location_uid']=$rowarr['uid_location'];
								$data['cptype']=$rowarr['offline_cp_content_type'];
								$data['buildid']=$rowarr['offline_version'];
								$data['ssid']=$rowarr['main_ssid'];
								
								$data['location_id']=$rowarr['location_id'];
								$data['synctimingkey']=$rowarr['frequency_hour'];
								$data['offline_content_title']=$rowarr['offline_content_title'];
								$data['offline_content_desc']=$rowarr['offline_content_desc'];
								$data['apikey']=$rowarr['apikey'];
								$data['dbpathdynamic']=OFFLINE_DIR."raspberryDB/".$data['location_uid']."_download_radius.sql";
								$data['dbname']=$data['location_uid']."_download_radius.sql";
									$data['brand_image']=$this->image_copy($brandlogoarr['brand_image']);
								$data['banner_image_path']=($rowarr['retail_original_image']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr['retail_original_image']:'';
								$data['banner_image']=($rowarr['retail_original_image']!='')?$this->image_copy($data['banner_image_path']):"";
								$data['logo2_image_path']=($rowarr['custom_slider_image1']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr['custom_slider_image1']:'';
								$data['logo2_image']=($rowarr['custom_slider_image1']!='')?$this->image_copy($data['logo2_image_path']):"";
								$data['location_name']=$rowarr['location_name'];
								$data['geo_address']=$rowarr['geo_address'];
								if($rowarr['offline_cp_content_type']==1)
								{
									//sql generate
									
									$loc_id=$rowarr['location_id'];
									$tables=array("offline_content" ,"offline_main_category","offline_sub_category");

								$tablecond=array("offline_build_version"=>array("loc_id"=>$loc_id),"offline_content"=>array("location_id"=>$loc_id),
												 "offline_content_impression"=>array("loc_id"=>$loc_id),"offline_home_impression"=>array("loc_id"=>$loc_id),
												 "offline_main_category"=>array("location_id"=>$loc_id),
												 "offline_maincategory_impression"=>array("loc_id"=>$loc_id),"offline_ping"=>array("loc_id"=>$loc_id),
												 "offline_sub_category"=>array("location_id"=>$loc_id)
												 ,"offline_subcategory_impression"=>array("loc_id"=>$loc_id),"offline_views_data"=>array("location_id"=>$loc_id));

								$notcontentarr=  array("offline_build_version","offline_ping","offline_content_impression",
								"offline_views_data","offline_maincategory_impression","offline_home_impression","offline_subcategory_impression");              

								$primarykeyremove=array("offline_content_impression","offline_maincategory_impression","offline_home_impression","offline_subcategory_impression");
								
								$host=$this->DB2->hostname;
								$user=$this->DB2->username;
								$pass=$this->DB2->password;
								$name=$this->DB2->database;
								 $this->backup_tables($host,$user,$pass,$name,$tables,$tablecond,$primarykeyremove,$notcontentarr,$data['dbpathdynamic']);
								  $data['dynamic_data']=$this->dynamic_data($loc_id);
								 $data['dynamic_data'][]=($data['brand_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['brand_image']:'';
								 $data['dynamic_data'][]=($data['banner_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['banner_image']:'';
								  $data['dynamic_data']=array_unique($data['dynamic_data']);
								  $resourcesize=0;
								 $resourcesize=$resourcesize+filesize($data['dbpathdynamic']);
								 $filearr=array();
								 foreach($data['dynamic_data'] as $valf)
								 {
									$resourcesize=$resourcesize+filesize($valf);
								}
								 $data['resourcessize']=$resourcesize;
								 $data['totalpackage_size']=$resourcesize;
								 
								}
								
								
								
								
								
								
								} else {
									$data['resultCode']=0;
										$data['resultMessage']="Folder Not Present";
										$data['cp_sourcepath']="";
										$data['cp_landingpath']="";
								}
							
						
						
					}else{
						$data['resultCode']=0;
						$data['resultMessage']="Api key Invalid";
					}
					
				}
				
			}
		
			
		}else{
				$data['resultCode']=0;
			$data['resultMessage']="No Location Found";
		}
		}
		
		return $data;
			
		
	}
	
	public function new_ssid_get($jsondata)
	{
		if($jsondata->macId=='')
		{
			$data['resultcode']=0;
			$data['resultmsg']="Please send MacID";
		}
		foreach($jsondata->macId as $val)
			{
				$macarr[]=strtolower($val);
			}
			
			 $macids='"'.implode('", "', $macarr).'"';
			
			$query=$this->DB2->query("select wl.id,wl.isp_uid,wls.macid from wifi_location_access_point  wls
		inner join wifi_location wl on (wls.location_uid=wl.location_uid) where lower(wls.macid) in (".$macids.") and  wls.macid is not NULL");
	//	echo $this->DB2->last_query(); die;
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['resultMessage']="Success";
			$isp_uid=$rowarr['isp_uid'];
			$locid=$rowarr['id'];
			$macidsync=$rowarr['macid'];
			$macidfolder=strtolower(str_replace(":","-",$macidsync));
			
			$query1= $this->DB2->query("select wl.id,wl.isp_uid,wl.location_name,wl.geo_address,wlc.offline_cp_source_folder_path,wlc.cp_type,wlc.location_id,wlc.`offline_content_title`,wlc.`offline_content_desc`,wlc.uid_location,wlc.offline_cp_content_type,wlc.main_ssid,wlc.retail_original_image,wlc.offline_cp_landing_page_path,wlc.apikey,wlc.offline_version,wlc.frequency_hour,wlc.is_offline_registration,wlc.custom_slider_image1 from wifi_location  wl
			inner join wifi_location_cptype wlc on (wlc.location_id=wl.id) where wl.id='".$locid."' and (wlc.cp_type='cp_offline' or wlc.cp_type='cp_visitor' or wlc.cp_type='cp_contestifi')");
			if($query1->num_rows()>0)
			{
				
				
				$brandlogoarr=$this->brand_logo($locid);
			
				
				$rowarr1=$query1->row_array();
				if($rowarr1['cp_type']=="cp_offline")
				{
					$dynamicpath=OFFLINE_DIR."offlinedynamic";
					
					$zippath=OFFLINE_DIR."offline_zip/".$macidfolder."/offlinedynamic.zip";
				}else{
					$dynamicpath=OFFLINE_DIR."visitoroffline";
					$zippath=OFFLINE_DIR."offline_zip/".$macidfolder."/offlinedynamic.zip";
				}
				
				$rowarr1['offline_cp_source_folder_path']=$zippath;
				$rowarr1['offline_cp_landing_page_path']=$dynamicpath."/index.php";
				$filearr=explode("/",$rowarr1['offline_cp_source_folder_path'].".zip");
				$data['filename']= end($filearr);
				$data['brand_imagepath']=$brandlogoarr['brand_imagepath'];
				$data['brand_image']=$this->image_copy($brandlogoarr['brand_image']);
				$data['banner_image_path']=($rowarr1['retail_original_image']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr1['retail_original_image']:'';
				$data['banner_image']=($rowarr1['retail_original_image']!='')?$this->image_copy($data['banner_image_path']):"";
				$data['logo2_image_path']=($rowarr1['custom_slider_image1']!='')?'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/retail_background_image/original/'.$rowarr1['custom_slider_image1']:'';
				$data['logo2_image']=($rowarr1['custom_slider_image1']!='')?$this->image_copy($data['logo2_image_path']):"";
								
				$data['zipname']=array("offlinedynamic.zip");
				$data['new_ssid']=array("offlinedynamic.zip");
				$data['dynamic_data']=$this->dynamic_data($locid,$macidsync);
					
				 $data['dynamic_data'][]=($data['brand_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['brand_image']:'';
				$data['dynamic_data'][]=($data['banner_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['banner_image']:'';
				$data['dynamic_data'][]=($data['logo2_image']!='')?OFFLINE_DIR."offline_video_image_content/".$data['logo2_image']:'';
			
				 $data['dynamic_data']=array_unique($data['dynamic_data']);
				 $data['dynamic_data']=$this->filter_dynamic_data($data['dynamic_data'],$macidsync);
				 $data['dynamic_data']=array_merge($data['zipname'],$data['dynamic_data']);
				 $data['resources']=str_replace(OFFLINE_DIR."offline_video_image_content/","",implode(",",$data['dynamic_data']));
				
				
				
				
				//echo "<pre>"; print_R($brandlogoarr);die;
				$this->load->library('zip');
				$rowarr=$query1->row_array();
				//echo "<pre>"; print_R($rowarr); die;
				$data['ssid']=$rowarr['main_ssid'];
				$data['location_id']=$rowarr['location_id'];
				$data['servertime']=date("Y-m-d H:i:s");
				$data['build_version']=$rowarr['offline_version'];
				
			}else{
				$data['resultCode']=0;
				$data['resultMessage']="No location found";
			}
		}
		else{
			$data['resultCode']=0;
			$data['resultMessage']="No location found";
		}
		
		return $data;
		
	}
	
	public function get_resouces_data($jsondata)
	{
		if($jsondata->macId=='')
		{
			$data['resultcode']=0;
			$data['resultmsg']="Please send MacID";
		}
		foreach($jsondata->macId as $val)
			{
				$macarr[]=strtolower($val);
			}
			 $macids='"'.implode('", "', $macarr).'"';
			
			$query=$this->DB2->query("select wl.id,wl.isp_uid from wifi_location_access_point  wls
		inner join wifi_location wl on (wls.location_uid=wl.location_uid) where lower(wls.macid) in (".$macids.") and  wls.macid is not NULL");
	//	echo $this->DB2->last_query(); die;
	
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$data['resultCode']=1;
			$data['resultMessage']="Success";
			$isp_uid=$rowarr['isp_uid'];
			$locid=$rowarr['id'];
			
			
			$query1= $this->DB2->query("select wl.id,wl.isp_uid,wlc.offline_cp_source_folder_path,wlc.location_id,wlc.uid_location,wlc.`offline_content_title`,wlc.`offline_content_desc`,wlc.offline_cp_content_type,wlc.main_ssid,wlc.retail_original_image,wlc.custom_slider_image1,wlc.offline_cp_landing_page_path,wlc.apikey,wlc.offline_version,wlc.frequency_hour,wlc.cp_type from wifi_location  wl
			inner join wifi_location_cptype wlc on (wlc.location_id=wl.id) where wl.id='".$locid."' and (wlc.cp_type='cp_offline' or wlc.cp_type='cp_visitor' or wlc.cp_type='cp_contestifi')");
			if($query1->num_rows()>0)
			{
				$brandlogoarr=$this->brand_logo($locid);
				//echo "<pre>"; print_R($brandlogoarr);die;
				$this->load->library('zip');
				$rowarr=$query1->row_array();
					if($rowarr['cp_type']=="cp_offline")
				{
					$dynamicpath=OFFLINE_DIR."offlinedynamic";
				}else{
					$dynamicpath=OFFLINE_DIR."visitoroffline";
				}
				if($rowarr['offline_cp_content_type']==1)
				{
					$rowarr['offline_cp_source_folder_path']=$dynamicpath;
					$rowarr['offline_cp_landing_page_path']=$dynamicpath."/index.php";
				}
				//echo "<pre>"; print_R($rowarr); die;
				$data['ssid']=$rowarr['main_ssid'];
				$data['location_id']=$rowarr['location_id'];
				
				$data['dynamic_data']=$this->dynamic_data($rowarr['location_id']);
				$data['dynamic_data'][]=($rowarr['retail_original_image']!='')?$rowarr['retail_original_image']:"";
				$data['dynamic_data'][]=($rowarr['custom_slider_image1']!='')?$rowarr['custom_slider_image1']:"";
				$brandlogoarr=$this->brand_logo($rowarr['location_id']);
				$data['dynamic_data'][]=$brandlogoarr['brand_image'];
				$data['zip_path']=$rowarr['offline_cp_source_folder_path'].".zip";
				$data['zipsize']=filesize($rowarr['offline_cp_source_folder_path'].".zip");
				$filearr=explode("/",$rowarr['offline_cp_source_folder_path'].".zip");
				$data['filename']= end($filearr);
				
			}
		}
		else{
			$data['resultCode']=0;
			$data['resultMessage']="No location found";
		}
		
		return $data;
		
	}
	
	public function sync_upload_db($jsondata)
	{
		if(file_exists(OFFLINE_DIR."raspberryDB/".$jsondata->sqlfile))
		{
			$this->import_dynamicdb(OFFLINE_DIR."raspberryDB/".$jsondata->sqlfile);
			//unlink("/var/www/html/isp_hotspot/offline/raspberryDB/".$jsondata->sqlfile);
		}
		//sync version table
		/*$query=$this->DB2->query("select * from offline_build_version_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				$query1=$this->DB2->query("select id from offline_build_version where last_communicated='".$val->last_communicated."' and loc_id='".$val->loc_id."' and mac_id='".$val->mac_id."'");
				
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_build_version_temp'); 
												
				}
				else{
					$tabledata=array("version"=>$val->version,"apikey"=>$val->apikey,"sync_frequency"=>$val->sync_frequency,"last_communicated"=>$val->last_communicated,"loc_id"=>$val->loc_id,
					"mac_id"=>$val->mac_id,"title"=>$val->title,"description"=>$val->description,"ssid"=>$val->ssid,"brand_logo"=>$val->brand_logo,"banner_image"=>$val->banner_image,
					"url_redirect"=>$val->url_redirect,"location_name"=>$val->location_name,"location_address"=>$val->location_address,"upload"=>$val->upload,"download"=>$val->download,
					"payload_size"=>$val->payload_size,"payload_receive"=>$val->payload_receive,"upload_status"=>$val->upload_status);
					$this->DB2->insert('offline_build_version',$tabledata);
				
					//delete
					$this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_build_version_temp'); 
					
							
					
					
				}
			}
		}*/
		
		//offline ping sync
		//sync version table
		$query=$this->DB2->query("select * from offline_ping_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				$query1=$this->DB2->query("select id from offline_ping where mac_id='".$val->mac_id."' and loc_id='".$val->loc_id."' and ping_on='".$val->ping_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_ping_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					$tabledata=array("mac_id"=>$val->mac_id,"loc_id"=>$val->loc_id,"status"=>$val->status,"ping_on"=>$val->ping_on);
					$this->DB2->insert('offline_ping',$tabledata);
					
					//delete
					$this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_ping_temp'); 
							
					
					
				}
			}
		}
		//offline views data temp
		$query=$this->DB2->query("select * from offline_views_data_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				$query1=$this->DB2->query("select id from offline_views_data where ap_macid='".$val->ap_macid."' and location_id='".$val->location_id."' and added_on='".$val->added_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_views_data_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					$tabledata=array("user_macid"=>$val->user_macid,"ap_macid"=>$val->ap_macid,"event_name"=>$val->event_name,"category"=>$val->category
					,"type"=>$val->type,"location_id"=>$val->location_id,"added_on"=>$val->added_on);
					$this->DB2->insert('offline_views_data',$tabledata);
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_views_data_temp'); 
							
					
					
				}
			}
		}
		
			//offline poll Survey answer views data temp
		$query=$this->DB2->query("select * from offline_poll_survey_answer_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				$query1=$this->DB2->query("select id from offline_poll_survey_answer where loc_macid='".$val->loc_macid."' and loc_id='".$val->loc_id."' and added_on='".$val->added_on."' and user_macid='".$val->user_macid."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_poll_survey_answer_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					$tabledata=array("user_macid"=>$val->user_macid,"cid"=>$val->cid,"ques_id"=>$val->ques_id,"option_id"=>$val->option_id
					,"added_on"=>$val->added_on,"loc_id"=>$val->loc_id,"loc_macid"=>$val->loc_macid,"survey_type"=>$val->survey_type);
					$this->DB2->insert('offline_poll_survey_answer',$tabledata);
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_poll_survey_answer_temp'); 
							
					
					
				}
			}
		}
		
			//offline poll Survey text answer views data temp
		$query=$this->DB2->query("select * from offline_survey_text_answer_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				$answer=$this->DB2->escape_str($val->answer);
				$query1=$this->DB2->query("select id from offline_survey_text_answer where user_macid='".$val->user_macid."' and loc_id='".$val->loc_id."' and cid='".$val->cid."' and added_on='".$val->added_on."' and 	loc_macid='".$val->loc_macid."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_survey_text_answer_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					$tabledata=array("user_macid"=>$val->user_macid,"answer"=>$answer,"added_on"=>$val->added_on,"loc_id"=>$val->loc_id
					,"loc_macid"=>$val->loc_macid,"cid"=>$val->cid,"ques_id"=>$val->ques_id);
					$this->DB2->insert('offline_survey_text_answer',$tabledata);
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_survey_text_answer_temp'); 
							
					
					
				}
			}
		}
		
			//offline video image temp
		$query=$this->DB2->query("select * from offline_image_video_upload_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				$filename=$this->DB2->escape_str($val->filename);
				$query1=$this->DB2->query("select id from offline_image_video_upload where  uploaded_on='".$val->uploaded_on	."' and loc_id='".$val->loc_id."' and user_macid='".$val->user_macid."' and loc_macid='".$val->loc_macid."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_image_video_upload_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					$tabledata=array("filename"=>$filename,"uploaded_on"=>$val->uploaded_on,"loc_id"=>$val->loc_id,"user_macid"=>$val->user_macid
					,"loc_macid"=>$val->loc_macid,"cid"=>$val->cid);
					$this->DB2->insert('offline_image_video_upload',$tabledata);
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_image_video_upload_temp'); 
							
					
					
				}
			}
		}
		
		
			//offline user registration temp
		$query=$this->DB2->query("select * from offline_user_registration_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				
				$query1=$this->DB2->query("select id from offline_user_registration where  user_macid='".$val->user_macid	."' and loc_id='".$val->loc_id."'");
				
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_user_registration_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					$tabledata=array("user_macid"=>$val->user_macid,"loc_id"=>$val->loc_id,"mac_id"=>$val->mac_id,"mobile_no"=>$val->mobile_no
					,"email"=>$val->email,"age_group"=>$val->age_group,"uname"=>$val->uname,"gender"=>$val->gender,"registered_on"=>$val->registered_on);
					$this->DB2->insert('offline_user_registration',$tabledata);
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_user_registration_temp'); 
							
					
					
				}
			}
		}
		
		
		//offline visitor registration temp
		$query=$this->DB2->query("select * from offline_visitor_registration_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				
				$query1=$this->DB2->query("select id from offline_visitor_registration where  user_macid='".$val->user_macid."' and loc_id='".$val->loc_id."' and person_meeting='".$val->person_meeting."' and registered_on='".$val->registered_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_visitor_registration_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					
					$tabledata=array("user_macid"=>$val->user_macid,"loc_id"=>$val->loc_id,"visitor_name"=>$val->visitor_name,"visitor_email"=>$val->visitor_email
					,"visitor_organization"=>$val->visitor_organization,"company_visiting"=>$val->company_visiting,
					"person_meeting"=>$val->person_meeting,"purpsose_meeting"=>$val->purpsose_meeting,"is_completed"=>$val->is_completed,"registered_on"=>$val->registered_on);
					$this->DB2->insert('offline_visitor_registration',$tabledata);
					
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_visitor_registration_temp'); 
							
					
					
				}
			}
		}
		
		
			//offline contest earn point
		$query=$this->DB2->query("select * from offline_contestifi_contest_earn_point_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				
				$query1=$this->DB2->query("select id from offline_contestifi_contest_earn_point where  	userid='".$val->userid."' and contest_id='".$val->contest_id."' and question_id='".$val->question_id."' and submitted_on='".$val->submitted_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_contestifi_contest_earn_point_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					
					$tabledata=array("userid"=>$val->userid,"contest_id"=>$val->contest_id,"question_id"=>$val->question_id,"is_skip"=>$val->is_skip
					,"option_id"=>$val->option_id,"is_right"=>$val->is_right,
					"second_taken"=>$val->second_taken,"question_point"=>$val->question_point,"point_earn"=>$val->point_earn,"submitted_on"=>$val->submitted_on);
					$this->DB2->insert('offline_contestifi_contest_earn_point',$tabledata);
					
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_contestifi_contest_earn_point_temp'); 
							
					
					
				}
			}
		}
		
		//offline_contestifi_contest_attempt
		
		$query=$this->DB2->query("select * from offline_contestifi_contest_attempt_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				
				$query1=$this->DB2->query("select id from offline_contestifi_contest_attempt where  userid='".$val->userid."' and user_register_id='".$val->user_register_id."' and submitted_on='".$val->submitted_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_contestifi_contest_attempt_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					
					$tabledata=array("userid"=>$val->userid,"user_register_id"=>$val->user_register_id,"content_id"=>$val->content_id,"second_taken"=>$val->second_taken
					,"total_point"=>$val->total_point,"earn_point"=>$val->earn_point,"submitted_on"=>$val->submitted_on,"location_id"=>$val->location_id);
					$this->DB2->insert('offline_contestifi_contest_attempt',$tabledata);
					
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_contestifi_contest_attempt_temp'); 
							
				}
			}
		}
		
		//offline otp time 
		
		$query=$this->DB2->query("select * from offline_contestifi_otp_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				
				$query1=$this->DB2->query("select id from offline_contestifi_otp where  usermac='".$val->usermac."' and user_mobile='".$val->user_mobile."' and created_on='".$val->created_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_contestifi_otp_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					
					$tabledata=array("usermac"=>$val->usermac,"user_mobile"=>$val->user_mobile,"otp"=>$val->otp,"is_verified"=>$val->is_verified
					,"created_on"=>$val->created_on,"used_on"=>$val->used_on);
					$this->DB2->insert('offline_contestifi_otp',$tabledata);
					
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_contestifi_otp_temp'); 
							
				}
			}
		}
		
		//offline offline_contestifi_contest_survey_option_fill_temp
		
		$query=$this->DB2->query("select * from offline_contestifi_contest_survey_option_fill_temp");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				
				$query1=$this->DB2->query("select id from offline_contestifi_contest_survey_option_fill where  contest_id='".$val->contest_id."' and attempt_id='".$val->attempt_id."' and question_id='".$val->question_id."' and option_id='".$val->option_id."'  and submitted_on='".$val->submitted_on."'");
				//echo $this->DB2->last_query()."<br/>"; 
				if($query1->num_rows()>0)
				{
						 $this->DB2->where('id', $val->id);
							$this->DB2->delete('offline_contestifi_contest_survey_option_fill_temp'); 
							
						//	echo $this->DB2->last_query()."<br/>";     
				}
				else{
					
					$tabledata=array("contest_id"=>$val->contest_id,"attempt_id"=>$val->attempt_id,"question_id"=>$val->question_id,"option_id"=>$val->option_id
					,"submitted_on"=>$val->submitted_on);
					$this->DB2->insert('offline_contestifi_contest_survey_option_fill',$tabledata);
					
					
					//delete
					$this->DB2->where('id', $val->id);
					$this->DB2->delete('offline_contestifi_contest_survey_option_fill_temp'); 
							
				}
			}
		}
		
		
		
		
		$data['resultCode']=1;
		return $data;
		
		
		
	}
	
	
	public function import_dynamicdb($filename)
{
	$connection = $this->mysqlConnect();
		$filename = $filename;
		// Temporary variable, used to store current query
		$templine = '';

		
			$fp = fopen($filename, 'r');

				// Loop through each line
				while (($line = fgets($fp)) !== false) {
					// Skip it if it's a comment
					if (substr($line, 0, 2) == '--' || $line == '')
						continue;

					// Add this line to the current segment
					$templine .= $line;

					// If it has a semicolon at the end, it's the end of the query
					if (substr(trim($line), -1, 1) == ';') {
						// Perform the query
						if(!mysqli_query($connection, $templine)){
							print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
						}
						// Reset temp variable to empty
						$templine = '';
					}
				}

				mysqli_close($connection);
				fclose($fp);

				echo "Database imported successfully";
		   
		
		// Read in entire file
}

public function get_server_info($jsondata)
{
	$data['resultcode']=1;
	$query=$this->DB2->query("select country_id,timezone from offline_apmac_country where macid='".$jsondata->macId."' and is_deleted='0' order by id desc limit 1");
	if($query->num_rows()>0)
	{
		$rowarr=$query->row_array();
		if($rowarr['country_id']!="101")
		{
			$queryc=$this->db->query("select offline_server_host,offline_sshpassword,offline_server_directory,offline_api_url,offline_pemfile,offline_port from sht_country_credential where country_id='".$rowarr['country_id']."'");
			if($queryc->num_rows()>0)
			{
				$detailarr=$queryc->row_array();
				if($detailarr['offline_server_host']!='' &&  $detailarr['offline_server_directory']!='' &&   $detailarr['offline_api_url']!='')
				{
					
					$data['resultmsg']="Success";
					$data['offline_server_host']=$detailarr['offline_server_host'];
					$data['offline_sshpassword']= $detailarr['offline_sshpassword'];
					$data['offline_server_directory']=$detailarr['offline_server_directory'];
					$data['offline_api_url']=$detailarr['offline_api_url'];
					$data['offline_pemfile']=$detailarr['offline_pemfile'];	
					$data['offline_port']=$detailarr['offline_port'];;	
					$data['timezone']=$rowarr['timezone'];						
					
				}else{
						$data['resultcode']=0;
						$data['resultmsg']="Info data not Complete";
				}
				
			}
			else{
				$data['resultcode']=0;
				$data['resultmsg']="Info data not found";
			}
			
			
		}else{
			$data['resultmsg']="Success";
			$data['offline_server_host']='103.20.213.149';
		$data['offline_sshpassword']='ECDX2zD9K8R9fSvh';
		$data['offline_server_directory']=OFFLINE_DIR;
		$data['offline_api_url']='http://103.20.213.149/isp_consumer_api/offlineapi/';
		$data['offline_pemfile']='';
		$data['offline_port']='-P 6875';	
		$data['timezone']='Asia/Kolkata';				
		}
		
	}else{
		$data['resultmsg']="Success";
		$data['offline_server_host']='103.20.213.149';
		$data['offline_sshpassword']='ECDX2zD9K8R9fSvh';
		$data['offline_server_directory']=OFFLINE_DIR;
		$data['offline_api_url']='http://103.20.213.149/isp_consumer_api/offlineapi/';
		$data['offline_pemfile']='';
		$data['offline_port']='-P 6875';
		$data['timezone']='Asia/Kolkata';						
		
	}
	$this->log(json_encode($jsondata), json_encode($data)) ;
	return $data;
	
	
	
	
}


	public function mysqlConnect(){
		
		$mysql_host = $this->DB2->hostname;
		$mysql_username = $this->DB2->username;
		$mysql_password = $this->DB2->password;
		$mysql_database = $this->DB2->database;
		
		// Connect to MySQL server
		$connection = mysqli_connect($mysql_host, $mysql_username, $mysql_password, $mysql_database);

		if (mysqli_connect_errno())
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		return $connection;
}
	
	
	public function image_copy($imageurl)
	{
		$imagename='';
		if($imageurl!='')
		{
		$url = $imageurl;
		$imgarr=explode("/",$imageurl);
		
		$imagename=end($imgarr);
		$img = OFFLINE_DIR.'offline_video_image_content/'.$imagename;
		file_put_contents($img, file_get_contents($url));
		$path = OFFLINE_DIR.'offline_video_image_content/';


		if($path != ''){
		$output = exec("sudo chmod -R 0777 \"$path\"");
		}
				}
		return $imagename;
		
	}


	public function copy_s3_state($jsondata)
	{
		
			$mquery=$this->DB2->query("select wl.id,wl.isp_uid from wifi_location_access_point  wls
		inner join wifi_location wl on (wls.location_uid=wl.location_uid) where lower(wls.macid) in ('".$jsondata->macId."') and  wls.macid is not NULL");
		if($mquery->num_rows()>0)
		{
			$rowarrloc=$mquery->row_array();
			$data['resultcode']=1;
			if($jsondata->syncmanully==1)
			{
					$data['resultcode']=1;
					$query=$this->DB2->query("select id,status,started_on from offline_amazonresources_copystate where loc_id='".$rowarrloc['id']."' order by id desc limit 1");
				if($query->num_rows()>0)
				{
					$rowarr=$query->row_array();
					if($rowarr['status']==1)
					{
						$lastsync=strtotime($rowarr['started_on']);
						$currenttime=strtotime(date("Y-m-d H:i:s"));
						$syndiff=$currenttime-$lastsync;
						
						
						if($syndiff>300)
						{
							$data['resultcode']=1;
							 $this->DB2->where('loc_id', $rowarrloc['id']);
						$this->DB2->delete('offline_amazonresources_copystate'); 
						$this->copy_s3_toserver($rowarrloc['id']);
						
						}else{
							$data['resultcode']=0;
						$data['resultmsg']="file copying in process";
						}
						
						
					}else{
						$lastsync=strtotime($rowarr['started_on']);
						$currenttime=strtotime(date("Y-m-d H:i:s"));
						$syndiff=$currenttime-$lastsync;
						
						
						if($syndiff>300)
						{
							$data['resultcode']=1;
							 $this->DB2->where('loc_id', $rowarrloc['id']);
						$this->DB2->delete('offline_amazonresources_copystate'); 
						$this->copy_s3_toserver($rowarrloc['id']);
						
						}else{
							$data['resultcode']=0;
						$data['resultmsg']="file copying in process";
						}
						
					}
				}else{
					$data['resultcode']=1;
					 $this->DB2->where('loc_id', $rowarrloc['id']);
						$this->DB2->delete('offline_amazonresources_copystate'); 
						
						$this->copy_s3_toserver($rowarrloc['id']);
				}
					
					
				
			}
			else
			{
				$query=$this->DB2->query("select id,status,started_on from offline_amazonresources_copystate where loc_id='".$rowarrloc['id']."'  order by id desc limit 1");
				if($query->num_rows()>0)
				{
					$rowarr=$query->row_array();
					if($rowarr['status']==1)
					{
						$lastsync=strtotime($rowarr['started_on']);
						$currenttime=strtotime(date("Y-m-d H:i:s"));
						$syndiff=$currenttime-$lastsync;
						
						
						if($syndiff>300)
						{
							$data['resultcode']=1;
							 $this->DB2->where('loc_id', $rowarrloc['id']);
						$this->DB2->delete('offline_amazonresources_copystate'); 
						$this->copy_s3_toserver($rowarrloc['id']);
						
						}else{
							$data['resultcode']=0;
						$data['resultmsg']="file copying in process";
						}
					}else{
						$data['resultcode']=1;
						//$this->copy_s3_toserver($rowarrloc['id']);
					
					}
				}else{
					$data['resultcode']=1;
					$this->copy_s3_toserver($rowarrloc['id']);
				}
			}
			
		}
		else
		{
			$data['resultcode']=0;
			$data['resultmsg']="file copying in process";
		}
		return $data;
		
		
		
	}

	public function copy_s3_toserver($loc_id)
	{
		$dataarr=array();
		$tabledata=array("loc_id"=>$loc_id,"status"=>"1","started_on"=>date("Y-m-d H:i:s"));
		$this->DB2->insert("offline_amazonresources_copystate",$tabledata);
		$lastinsertd=$this->DB2->insert_id();
		$query=$this->DB2->query("select content_file from offline_content where location_id='".$loc_id."' and (content_type='video' or content_type='audio' or content_type='image' or content_type='pdf') and is_deleted='0'");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				//http://d392o9g87c202y.cloudfront.net/shouutofflinefirmware/ShouutOffline_Version2.3_Lite.img.zip'
				//$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$val->content_file);
				$this->image_copy("http://d392o9g87c202y.cloudfront.net/isp/offline_video_image_content/".$val->content_file);
				
			}
		}
		
		$query1=$this->DB2->query("select icon from offline_main_category where location_id='".$loc_id."'");
		if($query1->num_rows()>0)
		{
			foreach($query1->result() as $val1)
			{
				if($val1->icon!='')
				{
					//$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$val1->icon);
					$this->image_copy("http://d392o9g87c202y.cloudfront.net/isp/offline_video_image_content/".$val1->icon);
				
				}
				
			}
		}
		
		$query2=$this->DB2->query("select icon from offline_sub_category where location_id='".$loc_id."'");
		if($query2->num_rows()>0)
		{
			foreach($query2->result() as $val2)
			{
				if($val2->icon!='')
				{
					//$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$val2->icon);
					$this->image_copy("http://d392o9g87c202y.cloudfront.net/isp/offline_video_image_content/".$val2->icon);
					
				}
				
			}
		}
		
		
		$query3=$this->DB2->query("select image_name from offline_wiki_images where location_id='".$loc_id."'");
		if($query3->num_rows()>0)
		{
			foreach($query3->result() as $val3)
			{
				if($val3->image_name!='')
				{
				//	$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/wiki_content/".$val3->image_name);
					$this->image_copy("http://d392o9g87c202y.cloudfront.net/isp/offline_video_image_content/wiki_content/".$val3->image_name);
					
				}
				
			}
		}
		
		$tabledata2=array("status"=>"2");
		$this->DB2->update("offline_amazonresources_copystate",$tabledata2,array("id"=>$lastinsertd));
		return true;
	}
	
	
	public function dynamic_data($loc_id,$macid)
	{
		$dataarr=array();
		$query=$this->DB2->query("select content_file from offline_content where location_id='".$loc_id."' and (content_type='video' or content_type='audio' or content_type='image' or content_type='pdf') and is_deleted='0'");
		if($query->num_rows()>0)
		{
			foreach($query->result() as $val)
			{
				//$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$val->content_file);
				$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val->content_file;
			}
		}
		
		$query1=$this->DB2->query("select icon from offline_main_category where location_id='".$loc_id."'");
		if($query1->num_rows()>0)
		{
			foreach($query1->result() as $val1)
			{
				if($val1->icon!='')
				{
					//$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$val1->icon);
				$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val1->icon;	
				}
				
			}
		}
		
		$query2=$this->DB2->query("select icon from offline_sub_category where location_id='".$loc_id."'");
		if($query2->num_rows()>0)
		{
			foreach($query2->result() as $val2)
			{
				if($val2->icon!='')
				{
					//$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/".$val2->icon);
				$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val2->icon;	
				}
				
			}
		}
		
		$query3=$this->DB2->query("select image_name from offline_wiki_images where location_id='".$loc_id."'");
		
		if($query3->num_rows()>0)
		{
			foreach($query3->result() as $val3)
			{
				if($val3->image_name!='')
				{
				//	$this->image_copy("https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/offline_video_image_content/wikipedia/".$val3->image_name);
					$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val3->image_name;	
					
				}
				
			}
		}
		
		$query4=$this->DB2->query("select logo,small_logo,original_logo from offline_contestifi_contest where location_id='".$loc_id."'");
		
		if($query4->num_rows()>0)
		{
			foreach($query4->result() as $val4)
			{
				if($val4->logo!='')
				{
					$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val4->logo;	
				}
				if($val4->small_logo!='')
				{
					$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val4->small_logo;	
				}
				if($val4->original_logo!='')
				{
					$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val4->original_logo;	
				}
				
			}
		}
		
		$query5=$this->DB2->query("select contest_file from offline_contestifi_quiz_questions where location_id='".$loc_id."'");
		if($query5->num_rows()>0)
		{
			foreach($query5->result() as $val5)
			{
				if($val5->contest_file!='')
				{
					$dataarr[]="/var/www/html/isp_hotspot/offline/offline_video_image_content/".$val5->contest_file;	
				}
			}
		}
		
		
		
		return $dataarr;
	}
	
	public function filter_dynamic_data($dataarr,$macid)
	{
		
		$fileq=$this->DB2->query("select file_structure from offline_resources_filestructure where macid='".$macid."' order by id desc limit 1");
		if($fileq->num_rows()>0)
		{
			$newdatarr=array();
			$rowarr=$fileq->row_array();
			
			if($rowarr['file_structure']!='')
			{
				$existingfile=json_decode($rowarr['file_structure']);
				
				foreach($existingfile as $val)
				{
					$filenamarr=explode("/",$val->filename);
					$filename="/var/www/html/isp_hotspot/offline/offline_video_image_content/".end($filenamarr);
					if(in_array($filename,$dataarr))
					{
						$localfilezize=$val->filesize;
						$remotefilesize=filesize($filename);
						
						if($localfilezize==$remotefilesize)
						{
							$newdatarr[]=$filename;
						}
					}
					
				}
			
				
				$fnlarr=array_diff($dataarr,$newdatarr);
				$dataarr=$fnlarr;
				
			}
			
		}
		
		$dataarr=array_filter($dataarr);
		return $dataarr;
		
	}
	
	
function backup_tables($host,$user,$pass,$name,$tables='*',$tablecond,$primarykeyremove,$notcontentarr,$pathdb)
{
	
	$link = mysqli_connect($host,$user,$pass,$name);
	//mysqli_select_db($name,$link);
	
	//get all of the tables
	
	
	if($tables == '*')
	{
		$tables = array();
		$result = mysqli_query($link,'SHOW TABLES');
		while($row = mysqli_fetch_row($result))
		{
			$tables[] = $row[0];
		}
	}
	else
	{
		$tables = is_array($tables) ? $tables : explode(',',$tables);
	}
	
	$return='';
	$return.='CREATE DATABASE /*!32312 IF NOT EXISTS*/`radius` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `radius`;

';

	//cycle through
	foreach($tables as $table)
	{
           
            $l=0;
            $cond='';
			if(!empty($tablecond[$table]))
			{
            foreach($tablecond[$table] as $key=>$vald)
            {
                if($l==0)
                {
                    $cond.="where $key='".$vald."'";
                }
                else{
                     $cond.="and $key='".$vald."'";
                }
                $l++;
            }
			}
         
            
		$result = mysqli_query($link,'SELECT * FROM '.$table." ".$cond);
		$num_fields = mysqli_num_fields($result);
		
		$return.= 'DROP TABLE IF EXISTS '.$table.';';
		$row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
		$return.= "\n\n".$row2[1].";\n\n";
		
                if(!in_array($table,$notcontentarr))
                {
		for ($i = 0; $i < $num_fields; $i++) 
		{
			while($row = mysqli_fetch_row($result))
			{
				$return.= 'INSERT INTO '.$table.' VALUES(';
				for($j=0; $j < $num_fields; $j++) 
				{
                                  // echo "=====>>>".$j."dddddddddd".$row[$j]."<br/>";
					$row[$j] = addslashes($row[$j]);
					$row[$j] = str_replace("\n","\\n",$row[$j]);
                                        if(in_array($table,$primarykeyremove))
                                        {
                                            if (isset($row[$j]) && $j!='0') { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                        }else{
                                            if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                        }
					if ($j < ($num_fields-1)) { $return.= ','; }
                                    
				}
				$return.= ");\n";
			}
		}
                }
		$return.="\n\n\n";
		
	}
	
	//save file
	$handle = fopen($pathdb,'w+');
	fwrite($handle,$return);
	fclose($handle);
	
}

	
	public function brand_logo($locid)
	{
		$dataarr=array();
		$query1=$this->DB2->query("SELECT wl.location_name,wl.isp_uid,wlc.original_image,wlc.logo_image  FROM wifi_location wl
INNER JOIN `wifi_location_cptype` wlc ON (wl.id=wlc.location_id) where wl.id='".$locid."'");
$dataarr['brand_imagepath']='';
			$dataarr['brand_image']='';
//echo $this->DB2->last_query(); die;
	$brand_logo='';
	$locname='';
	
	if($query1->num_rows()>0)
	{
		$rowarr=$query1->row_array();
		$isp_uid=$rowarr['isp_uid'];
		 if($rowarr['logo_image'] != ''){
            $brand_logo = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_location_logo/logo/'.$rowarr['logo_image'];
			$dataarr['brand_imagepath']=$rowarr['logo_image'];
			$dataarr['brand_image']=$brand_logo;
            }elseif($rowarr['original_image'] != ''){
            $brand_logo  = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_location_logo/logo/'.$rowarr['original_image'];
			$dataarr['brand_imagepath']=$rowarr['original_image'];
			$dataarr['brand_image']=$brand_logo;
            }
			
			
			if($brand_logo=='')
			{
				
				$query = $this->db->query("SELECT `logo_image` FROM `sht_isp_detail` WHERE isp_uid = '$isp_uid'");
				if($query->num_rows() > 0){
				
					$row = $query->row_array();
					$brand_logo = ISP_IMAGEPATH."logo/".$row['logo_image'];
				}
				$dataarr['brand_imagepath']=$row['logo_image'];
			$dataarr['brand_image']=$brand_logo;
			
			}
			$this->image_copy(ISP_IMAGEPATH."logo/".$dataarr['brand_imagepath']);
			$this->image_copy($brand_logo);
			//$locname=$rowarr['location_name'] ;
			
	}
	return $dataarr;
	
	
	}
	
	
	public function ping_insert_data($jsondata)
	{
		$data=array();
		$datetime=date("Y-m-d H:i:s");
		$tabledata=array("loc_id"=>$jsondata->loc_id,"loc_macid"=>$jsondata->macid,"pinged_on"=>$datetime,"status"=>$jsondata->status);
		$this->DB2->insert("offline_ping_logs",$tabledata);
		
		$tabledata1=array("macid"=>$jsondata->macid,"added_on"=>date("Y-m-d H:i:s"),"file_structure"=>json_encode($jsondata->filestructure));
		$this->DB2->insert("offline_resources_filestructure",$tabledata1);
		if($jsondata->status==0){
				$query=$this->DB2->query("select id from offline_build_version where loc_id='".$jsondata->loc_id."' and status=0");
				if($query->num_rows()>0)
				{
					foreach($query->result() as $val)
					{
						$tabledata1=array("upload_status"=>2);
						$this->DB2->update("offline_build_version",$tabledata,array("id"=>$jsondata->lastid));
						
					}
					
				}
		}
	
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		return $data;
	}
	
		public function build_version_start($jsondata)
	{
		$data=array();
		$datetime=date("Y-m-d H:i:s");
		 $tabledata =	 array("version"=>$jsondata->version,"apikey"=>$jsondata->apikey,"sync_frequency"=>$jsondata->sync_frequency,"last_communicated"=>$datetime,
		 "loc_id"=>$jsondata->loc_id,"mac_id"=>$jsondata->mac_id,"ssid"=>$jsondata->ssid,"brand_logo"=>$jsondata->brand_logo,
		 "banner_image"=>$jsondata->banner_image,"title"=>$jsondata->title,"description"=>$jsondata->description,"location_name"=>$jsondata->location_name,"location_address"=>$jsondata->location_address,
		 "payload_size"=>$jsondata->payload_size);
		$this->DB2->insert("offline_build_version",$tabledata);
		$lastinserted_id=$this->DB2->insert_id();
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		$data['lastid']=$lastinserted_id;
		return $data;
	}
	
	public function build_version_update($jsondata)
	{
		
		$data=array();
		 $tabledata =array("payload_receive"=>$jsondata->payload_receive,"upload_status"=>$jsondata->upload_status);
		$this->DB2->update("offline_build_version",$tabledata,array("id"=>$jsondata->lastid));
		if($jsondata->upload_status==2)
		{
			$this->DB2->where('loc_id', $jsondata->locid);
				$this->DB2->delete('offline_amazonresources_copystate'); 
		}
		
	
		
		if(isset($jsondata->filename) && $jsondata->filename!='')
		{
			//unlink(OFFLINE_DIR."offline_video_image_content/".$jsondata->filename);
			 
			
		}
		if(isset($jsondata->sync_comp) && $jsondata->sync_comp=='1')
		{
				$this->DB2->where('loc_id', $jsondata->locid);
				$this->DB2->delete('offline_amazonresources_copystate'); 
		}
		
		
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		
		return $data;
	}
	
	public function send_visitor_email($jsondata)
	{
		$data['resultcode']=1;
		$data['resultmsg']="Success";
		if($jsondata->visitingemail=="")
		{
			$data['resultcode']=1;
		    $data['resultmsg']="Please enter valid email";
			return $data;
		}
		
		$msg = "Hi ".$jsondata->person_tomett.",<br/><br/>".$jsondata->visitor_name." from ".$jsondata->visitor_org."  is here to meet you and is waiting at the reception. Please provide them with a visitor badge before bringing them into the space.<br/><br/>Regards<br><br/>IBM Innovation Space - Markham Convergence Centre";
		$sub=$jsondata->visitor_name." is here to meet you";
		$sendmail=$this->send_via_vcemail($jsondata->visitingemail,$msg,$sub);
		if($sendmail)
		{
			$data['resultcode']=1;
		    $data['resultmsg']="Success";
			return $data;
		}
	}
	
	
	public function send_via_vcemail($email,$message,$sub)
	{
		$this->load->library('email');
		 $from = SMTP_USER;
	    $fromname = 'SHOUUT';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => 'venturelab@shouut.com',
	    'smtp_pass' => 'shouut@giant$#@',
	  // 'smtp_user' => SMTP_USER,
	  // 'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
		   $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
				  $this->email->bcc("welcome@venturelab.ca");
                $this->email->subject($sub);
                $this->email->message($message);	
                $this->email->send();
               return true;
	}
	
	public function cancel_sync($jsondata)
	{
		$data['resultcode']=1;
		if($jsondata->loc_id!='')
		{
			
				$this->DB2->where('loc_id', $jsondata->loc_id);
				$this->DB2->delete('offline_amazonresources_copystate'); 
				
				$array = array('loc_id' => $jsondata->loc_id, 'upload_status' => 0);
				$this->DB2->where($array); 
				$this->DB2->delete('offline_build_version'); 
				
		}
		return $data;
		
	}
	
	
	public function sync_data_completed($jsondata)
	{
		$data['resultcode']=1;
		$query=$this->DB2->query("select id from offline_macid_synced_detail where macid='".$jsondata->macid."'");
		//$mysqltime = 'now()';
		if($query->num_rows()>0)
		{
			$rowarr=$query->row_array();
			$id=$rowarr['id'];
			
			$tabledata =array("macid"=>$jsondata->macid,"location_id"=>$jsondata->location_id);
			$this->DB2->set('syncedon', 'NOW()', FALSE);
			//$query="UPDATE `offline_macid_synced_detail` SET `macid` = '".$jsondata->macid."', `syncedon` = now(), `location_id` = '".$jsondata->location_id."' WHERE `id` = '".$id."'";
			$this->DB2->update("offline_macid_synced_detail",$tabledata,array("id"=>$id));
			
		}else{
			$tabledata =	 array("macid"=>$jsondata->macid,"location_id"=>$jsondata->location_id);
			$this->DB2->set('syncedon', 'NOW()', FALSE);
			$this->DB2->insert("offline_macid_synced_detail",$tabledata);
		}
		echo $this->DB2->last_query(); die;
		return $data;
		 
	}
	
	public function sync_update_statuses($jsondata)
	{
	$dataarr=$jsondata->dataarr;
		if($jsondata->buildid!='')
		{		
			$query=$this->DB2->query("select id,recieved_size,file_recieved from offline_build_status where build_id='".$jsondata->buildid."'");
			$curtime=date("Y-m-d H:i:s");
			if($query->num_rows()>0)
			{
				
				$rowarr=$query->row_array();
				$id=$rowarr['id'];
				if($jsondata->progress_status=="filecopy"){
					$query="update offline_build_status set file_copied='1',mac_id='".$jsondata->macid."',server_connected='1',sync_initialized='1',sync_startedon='".$curtime."' where build_id='".$jsondata->buildid."'";
				}else if($jsondata->progress_status=="syncstarted"){
					$query="update offline_build_status set total_size='".$dataarr->total_size."' ,total_file='".$dataarr->total_file."', zip_size='".$dataarr->zip_size."' , sync_initialized='2',sync_startedon='".$curtime."' where build_id='".$jsondata->buildid."'";
				}else if($jsondata->progress_status=="dbuploaded"){
					$query="update offline_build_status set db_uploaded='1',sync_startedon='".$curtime."' where id='".$id."'";
				}else if($jsondata->progress_status=="zipdownloaded"){
					$query="update offline_build_status set zip_downloaded='1',recieved_size='".$dataarr->zip_size."',zip_size='".$dataarr->zip_size."',file_recieved='1',last_file_recieved='".$dataarr->last_file."',sync_startedon='".$curtime."'  where build_id='".$jsondata->buildid."'";
				}else if($jsondata->progress_status=="downloadprogress"){
					$fileno=$rowarr['file_recieved']+1;
					$recievesize=$dataarr['download_size'];
					$query="update offline_build_status set zip_downloaded='1',recieved_size='".$recievesize."',file_recieved='".$fileno."',last_file_recieved='".$dataarr->last_file."',sync_startedon='".$curtime."'  where build_id='".$jsondata->buildid."'";
				}else if($jsondata->progress_status=="resourcesdownload"){
					$query="update offline_build_status set resource_downloaded='1',sync_startedon='".$curtime."'  where build_id='".$jsondata->buildid."'";
				}else if($jsondata->progress_status=="syncfinalstatus")		{
				$query="update offline_build_status set is_complete='".$dataarr->status."',resource_downloaded='1',sync_startedon='".$curtime."'  where build_id='".$jsondata->buildid."'";
				}
				echo $query;
				$query=$this->DB2->query($query);
				
				
				
			}else{
				if($jsondata->progress_status=="filecopy"){
					$query="insert into offline_build_status set file_copied='1',mac_id='".$jsondata->macid."',server_connected='1',sync_initialized='1',build_id='".$jsondata->buildid."',sync_startedon='".$curtime."'";
				}
				$query=$this->DB2->query($query);
			}
			
		}
			return true;		
	}
	
	public function visitor_email_action($jsondata){
		$query=$this->DB2->query("SELECT ovr.id,ovr.purpsose_meeting,ovr.`visitor_name`,ovr.`visitor_lastname`,ovr.`visitor_email`,ovr.`visitor_organization`,ovr.`registered_on`,ovo.organisation_name,ove.employee_name,ove.`employee_email` FROM offline_visitor_registration ovr
		INNER JOIN offline_vm_organisation ovo ON (ovr.company_visiting=ovo.id) INNER JOIN offline_vm_employee ove ON (ovr.person_meeting=ove.id)WHERE ovr.id  = '$jsondata->registerid'");
		if($jsondata->type=="accept"){
			$sub="Visitor Confirmation";
		}else{
			$sub="Visitor Rejection";
		}
		$rowarr=$query->row_array();
		$msg="Visitor full name : ".$rowarr['visitor_name']." ".$rowarr['visitor_lastname']."<br/>"."Visitor email: ".$rowarr['visitor_email']."<br/>".
		"Visitor organization : ".$rowarr['visitor_organization']."<br/>"."Time checked in:".date("d-m-Y H:i:s",strtotime($rowarr['registered_on']))."<br/>".
		"To meet : ".$rowarr['employee_name']."<br/>"."Company name : ".$rowarr['organisation_name']."<br/>"."Purpose of meeting: ".$rowarr['purpsose_meeting'];
		$email="sandeep@shouut.com";
		$sendmail=$this->send_via_vcemail($email,$msg,$sub);
		if($sendmail)
		{
			$data['resultcode']=1;
		    $data['resultmsg']="Success";
			return $data;
		}
	}
	
	
	
	
	
	
   
    
}



?>
