<?php
class Mailchimpapi_model extends CI_Model{
	
	
	  public function __construct() {
		    parent::__construct();
		    $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
	  }
	
	public function cafe_mailchimpsync()
	{
		$query=$this->DB2->query("SELECT lmd.apikey,lmd.locationid,lmd.listid,lmd.location_uid FROM `location_mailchimp_detail` lmd
INNER JOIN `wifi_location_cptype` wlc ON (wlc.`location_id`=lmd.locationid) WHERE wlc.cp_type='cp_cafe' AND lmd.is_enable='1'");
		foreach($query->result() as $val)
		{
			$query1=$this->DB2->query("SELECT cep.uid,su.lastname,su.firstname,su.email FROM `cafe_earned_point` cep
			INNER JOIN sht_users su ON (su.uid=cep.uid) WHERE cep.loc_uid='".$val->location_uid."'");
			foreach($query1->result() as $vald)
			{
				$data=array();
				if($vald->email!='')
				{
					$data = array('email'=> $vald->email, 'status' => 'subscribed','firstname' => $vald->firstname,'lastname'  => $vald->lastname);
					if($val->apikey!='' && $val->listid!='')
					{
						
						$this->syncMailchimp($data,$val->apikey,$val->listid);
					}
					
				}
			}
		}
		
		echo "cafe email synced";

	}
	
	public function retail_mailchimpsyc()
	{
		$query=$this->DB2->query("SELECT lmd.apikey,lmd.locationid,lmd.listid,lmd.location_uid FROM `location_mailchimp_detail` lmd
INNER JOIN `wifi_location_cptype` wlc ON (wlc.`location_id`=lmd.locationid) WHERE wlc.cp_type='cp_retail' AND lmd.is_enable='1'");
		foreach($query->result() as $val)
		{
			$query1=$this->DB2->query("SELECT cep.customer_id,su.lastname,su.firstname,su.email FROM `retail_offer_reddem` cep
			INNER JOIN sht_users su ON (su.uid=cep.customer_id) WHERE cep.location_id='".$val->locationid."'");
			foreach($query1->result() as $vald)
			{
				$data=array();
				if($vald->email!='')
				{
					$data = array('email'=> $vald->email, 'status' => 'subscribed','firstname' => $vald->firstname,'lastname'  => $vald->lastname);
					if($val->apikey!='' && $val->listid!='')
					{
						
						$this->syncMailchimp($data,$val->apikey,$val->listid);
					}
					
				}
			}
		}
		
		echo "retail email synced";
	}
	
	
public function hotel_mailchimpsync()
	{
		$query=$this->DB2->query("SELECT lmd.apikey,lmd.locationid,lmd.listid,lmd.location_uid FROM `location_mailchimp_detail` lmd
INNER JOIN `wifi_location_cptype` wlc ON (wlc.`location_id`=lmd.locationid) WHERE wlc.cp_type='cp_hotel' AND lmd.is_enable='1'");
		foreach($query->result() as $val)
		{
			$query1=$this->DB2->query("SELECT cep.mobile,su.lastname,su.firstname,su.email FROM `ht_user_detail` cep
			INNER JOIN sht_users su ON (su.uid=cep.mobile) WHERE cep.location_id='".$val->locationid."'");
			foreach($query1->result() as $vald)
			{
				$data=array();
				if($vald->email!='')
				{
					$data = array('email'=> $vald->email, 'status' => 'subscribed','firstname' => $vald->firstname,'lastname'  => $vald->lastname);
					if($val->apikey!='' && $val->listid!='')
					{
						
						$this->syncMailchimp($data,$val->apikey,$val->listid);
					}
					
				}
			}
		}
		
		echo "retail email synced";
	}
	
	



	  public function syncMailchimp($data,$apikey,$listid) {
		  
		  $query=$this->DB2->query("select id from mailchimp_synced_email where email='".$data['email']."' and list_id='".$listid."' and apikey='".$apikey."'");
		  if($query->num_rows()==0)
		  {
			  $tabledata=array("email"=>$data['email'],"list_id"=>$listid,"apikey"=>$apikey);
			  $this->DB2->insert("mailchimp_synced_email",$tabledata);

	  $apiKey = $apikey;
       
	  $listId = $listid;
	       $memberId = md5(strtolower($data['email']));
	       $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
	       $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
	       $json = json_encode(array(
	       'email_address' => $data['email'],
	       'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
	       'merge_fields'  => array(
		       'FNAME'     => $data['firstname'],
		       'LNAME'     => $data['lastname']
		       )
	       ));
       
	       $ch = curl_init($url);
	       curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
	       curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
	       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	       curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	       curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                                                                
	       $result = curl_exec($ch);
	       $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	       curl_close($ch);
	       return $httpCode;
		  }

	  }
	  
	  public function cafe_group_mailchimpsyc(){
		    $get_group = $this->DB2->query("select mug.*, wl.location_type from mailchimp_user_group as mug inner join wifi_location as wl on(mug.location_uid = wl.location_uid) where mug.api_key != '' AND mug.list_id!= ''");
		    if($get_group->num_rows() > 0){
			      foreach($get_group->result() as $row){
					$user = array();
					$location_type = $row->location_type;
					$api_key = $row->api_key;
					$list_id = $row->list_id;
					$loc_uid = $row->location_uid;
					$visits_from = $row->visits_from;
					$visits_to = $row->visits_to;
					$start_date = $row->start_date;
					$end_date = $row->end_date;
					$gender = $row->gender;
					$osinfo = $row->osinfo;
					$redeemed = $row->redeemed;
					if($location_type == '6'){
						$user = $this->get_mailchimp_gropu_total_user_excel_export_cafe($loc_uid,$visits_from,$visits_to,$start_date,$end_date,$gender,$osinfo,$redeemed);  
					}else{
						  $user = $this->get_mailchimp_gropu_total_user_excel_export_other($loc_uid,$visits_from,$visits_to,$start_date,$end_date,$gender,$osinfo,$redeemed);
					}
					
					$user='"'.implode('", "', $user).'"';
					$query_user = $this->DB2->query("select uid,firstname,lastname,email,gender,age_group from sht_users where uid IN ($user)");
					foreach($query_user->result() as $row_user){
						  $data = array('email'=> $row_user->email, 'status' => 'subscribed','firstname' => $row_user->firstname,'lastname'  => $row_user->lastname);
						  if($api_key!='' && $list_id!=''){
							    $this->syncMailchimp($data,$api_key,$list_id);
						  }
					}
			      }
		    }
		    echo "done";
	  }
	  public function get_mailchimp_gropu_total_user_excel_export_cafe($loc_uid,$visits_from,$visits_to,$start_date,$end_date,$gender,$osinfo,$redeemed){
		    $user = array();
		    $gender_where = '';
		    if($gender == 'm'){
			$gender_where = "And (LOWER(gender) = 'm' Or LOWER(gender) = 'male')";
		    }elseif($gender == 'f'){
			$gender_where = "And (LOWER(gender) = 'f' Or LOWER(gender) = 'female')";
		    }
		    $osinfo_where = '';
		    if($osinfo == 'android'){
			$osinfo_where = "And LOWER(osinfo) = 'android phone'";
		    }elseif($osinfo == 'ios'){
			$osinfo_where = "And LOWER(osinfo) = 'apple phone'";
		    }elseif($osinfo == 'laptop'){
			$osinfo_where = "And LOWER(osinfo) = 'windows desktop'";
		    }
		    $redeemed_user_array = array();
		    $redeemed_query = $this->DB2->query("select uid from cafe_redeemed_point where loc_uid = '$loc_uid' and is_redeem = '1' and date(redeemed_on) between '$start_date' and '$end_date' group by uid");
		    foreach($redeemed_query->result() as $row_redeem){
			$redeemed_user_array[] = $row_redeem->uid;
		    }
		    $query = $this->DB2->query("select uid, count(*) as total_use from cafe_earned_point where loc_uid = '$loc_uid' AND date(added_on) between '$start_date' and '$end_date' $gender_where $osinfo_where group by uid");
		    //echo $this->db->last_query();
		    foreach($query->result() as $row){
			$total_use = $row->total_use;
			if($visits_to != '0'){
			    if($total_use >= $visits_from && $total_use <= $visits_to){
				if($redeemed == 'y'){
				    if(in_array($row->uid, $redeemed_user_array)){
					$user[] = $row->uid;
				    }
				}elseif($redeemed == 'n'){
				    if(!in_array($row->uid, $redeemed_user_array)){
					$user[] = $row->uid;
				    }
				}
			    }
			}else{
			    if($total_use == $visits_from){
				if($redeemed == 'y'){
				    if(in_array($row->uid, $redeemed_user_array)){
					$user[] = $row->uid;
				    }
				}elseif($redeemed == 'n'){
				    if(!in_array($row->uid, $redeemed_user_array)){
					$user[] = $row->uid;
				    }
				}
				
			    }
			}
		    }
		    return $user;
	  }
	  public function get_mailchimp_gropu_total_user_excel_export_other($loc_uid,$visits_from,$visits_to,$start_date,$end_date,$gender,$osinfo,$redeemed){
		    $user = array();
		    $gender_where = '';
		    if($gender == 'm'){
			$gender_where = "And (LOWER(gender) = 'm' Or LOWER(gender) = 'male')";
		    }elseif($gender == 'f'){
			$gender_where = "And (LOWER(gender) = 'f' Or LOWER(gender) = 'female')";
		    }
		    $osinfo_where = '';
		    if($osinfo == 'android'){
			$osinfo_where = "And LOWER(osinfo) = 'android phone'";
		    }elseif($osinfo == 'ios'){
			$osinfo_where = "And LOWER(osinfo) = 'apple phone'";
		    }elseif($osinfo == 'laptop'){
			$osinfo_where = "And LOWER(osinfo) = 'windows desktop'";
		    }
		   
		    $query = $this->DB2->query("select uid, count(*) as total_use from cp_location_user_daily_visit where loc_uid = '$loc_uid' AND date(added_on) between '$start_date' and '$end_date' $gender_where $osinfo_where group by uid");
		    //echo $this->db->last_query();
		    foreach($query->result() as $row){
			$total_use = $row->total_use;
			if($visits_to != '0'){
			    if($total_use >= $visits_from && $total_use <= $visits_to){
				$user[] = $row->uid;
			    }
			}else{
			    if($total_use == $visits_from){
				$user[] = $row->uid;
				
			    }
			}
		    }
		    return $user;
	  }
	  
	  public function location_sms_left($location_uid){
		    $data = array();
		    $is_otp_assign = 0;
		    $otp_balance = 0;
		    
			    $query = $this->DB2->query("select * from wifi_location_sms_assign where location_uid = '$location_uid'");
			    if($query->num_rows() > 0){
				
				$total_otp_assign = 0;
				$starting_date = date('Y-m-d');
				$i = 0;
				foreach($query->result() as $row){
				    if($row->sms_type != '1'){
				       $is_otp_assign = 1;
					if($i == 0){
					    $starting_date = date('Y-m-d',strtotime($row->created_on));
					}
					$i++;
					$total_otp_assign = $total_otp_assign+$row->total_sms;
				    }
				}
				// get total sms send
				$get_promo = $this->DB2->query("select mpgos.id from mailchimp_push_group_offers_send as mpgos inner join mailchimp_push_group as mpg on (mpgos.offer_id = mpg.id) where mpg.send_via = 'sms' AND mpgos.loc_uid = '$location_uid' AND date(mpgos.added_on) >= '$starting_date'");
				$total_promotional_sms_send = $get_promo->num_rows();
				$otp_balance = $total_otp_assign - $total_promotional_sms_send;
			    }
			
		    
		    $data['otp_balance'] = $otp_balance;
		    $data['is_otp_assign'] = $is_otp_assign;
		    return $data;
	  }
	  
	  public function cafe_push_msg(){
		    $today = date('Y-m-d');
		    //get all  group listing in which msg not send
		    $query = $this->DB2->query("select mpg.*, wl.location_type, wl.id as location_id, wl.isp_uid, wl.location_name from mailchimp_push_group as mpg inner join wifi_location as wl on (mpg.location_uid = wl.location_uid) where mpg.send_status = '0' and send_on = '$today'");
		    //echo "<pre>";print_r($query->result());die;
		    if($query->num_rows() > 0){
			      foreach($query->result() as $row){
					$location_logo = '';
					$group_id = $row->group_id;
					$push_group_id = $row->id;
					$send_via = $row->send_via;
					$location_type = $row->location_type;
					$location_id = $row->location_id;
					$isp_uid = $row->isp_uid;
					$location_name = $row->location_name;
					$location_logo = $this->get_isplogo($isp_uid, $location_id);
					$offer_name = $row->offer_name;
					$offer_desc = $row->offer_desc;
					$offer_type = $row->offer_type;
					$sms_gateway = array();
					$get_payment_gateway = $this->db->query("select * from sht_promotional_sms_gateway where isp_uid = '$isp_uid'");
					if($get_payment_gateway->num_rows() > 0)
					{
						$sms_gateway[0] = $get_payment_gateway->row_array();  
					}
					$push_status = '1';
					$check_sms = $this->location_sms_left($row->location_uid);
					$is_otp_assign = $check_sms['is_otp_assign'];
					$sms_balance = $check_sms['otp_balance'];
					// get group detail
					$group_detail = $this->DB2->query("select * from mailchimp_user_group where id = '$group_id'");
					if($group_detail->num_rows() > 0){
						  $group_detail_row = $group_detail->row_array();
						  $visits_from = $group_detail_row['visits_from'];
						  $visits_to = $group_detail_row['visits_to'];
						  $start_date = $group_detail_row['start_date'];
						  $end_date = $group_detail_row['end_date'];
						  $gender = $group_detail_row['gender'];
						  $osinfo = $group_detail_row['osinfo'];
						  $redeemed = $group_detail_row['redeemed'];
						  $loc_uid = $group_detail_row['location_uid'];
						  // get user list who exist in group
						  if($location_type == '6'){
							$total_user = $this->get_mailchimp_gropu_total_user_excel_export_cafe($loc_uid,$visits_from,$visits_to,$start_date,$end_date,$gender,$osinfo,$redeemed);    
						  }else{
							$total_user = $this->get_mailchimp_gropu_total_user_excel_export_other($loc_uid,$visits_from,$visits_to,$start_date,$end_date,$gender,$osinfo,$redeemed);       
						  }
						  if($is_otp_assign != '0' && $sms_balance <= count($total_user))
						  {
							 $push_status = '2';
							 //echo "fail";die;
						  }
						  else
						  {
							   // call function to generate coupon and send
							    $this->generate_push_coupon($offer_type,$sms_gateway,$isp_uid,$total_user,$send_via,$loc_uid,$push_group_id,$location_type,$location_logo,$location_name, $offer_name,$offer_desc);
						  }
						  
					}
					// update group that msg send
					$update_group = $this->DB2->query("update mailchimp_push_group set send_status = '$push_status' where id = '$push_group_id'");
			      }
		    }
		    echo "done";
	  }
	  public function get_isplogo($isp_uid, $location_id){
		$image_path = '';
		$get_location_photo = $this->DB2->query("select logo_image from wifi_location_cptype where location_id = '$location_id'");
		if($get_location_photo->num_rows() > 0){
		    $row = $get_location_photo->row_array();
		    $logo_name = $row['logo_image'];
		    if($logo_name != ''){
			$image_path = "http://cdn101.shouut.com/shouutmedia/isp/isp_location_logo/logo/".$logo_name;
		    }
		}
		if($image_path == ''){
		    $query = $this->db->query("select small_image,logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
		
		    if($query->num_rows() > 0){
			    $rowdata = $query->row();
			    $image_path =  IMAGEPATHISP.'logo/'.$rowdata->logo_image;
		    }
		}
		if($image_path != ''){
		    return $image_path;
		}else{
		    return $image_path;
		}
		
	  }
	  public function randon_voucher_cafe(){
		    $this->load->helper('string');
		    $random_number = strtoupper(random_string('alnum', 8));
		    $query = $this->DB2->query("select voucher_id from mailchimp_push_group_offers_send WHERE voucher_id = '$random_number'");
		    if($query->num_rows() > 0){
			    $this->randon_voucher_cafe();
		    }else{
			    return $random_number;
		    }
	  }
	  public function get_bitly_short_url($url) {
		    $login = 'shouut';
		    $appkey = 'R_495a520806484396941bd1a37d70228b';
		    $query = array(
			 "version" => "2.0.1",
			 "longUrl" => $url,
			 "login" => $login, // replace with your login
			 "apiKey" => $appkey // replace with your api key
		    );
		    $query = http_build_query($query);
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, "http://api.bit.ly/shorten?".$query);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    $response = curl_exec($ch);
		    curl_close($ch);
		    $response = json_decode($response);
		    if($response->errorCode == 0 && $response->statusCode == "OK") {
			 return $response->results->{$url}->shortUrl;
		    }else {
			 return '';
		    }
	  }
	  public function generate_push_coupon($offer_type,$sms_gateway,$isp_uid,$total_user,$send_via,$loc_uid,$push_group_id,$location_type,$location_logo,$location_name,$offer_name,$offer_desc){
		    $user='"'.implode('", "', $total_user).'"';
		    $query_user = $this->DB2->query("select uid,firstname,lastname,email,gender,age_group from sht_users where uid IN ($user)");
		    foreach($query_user->result() as $row_user){
			      $vcode = '';
			      if($offer_type != '1')
			      {
					$vcode = $this->randon_voucher_cafe();
			      }
			      
			      $uid = $row_user->uid;
			      $email = $row_user->email;
			      $username = $row_user->firstname." ".$row_user->lastname;
			      //$uid = '9650896116';
			      //$email = "deepak@shouut.com";
			      $insert_voucer = $this->DB2->query("insert into mailchimp_push_group_offers_send (uid, loc_uid, offer_id, voucher_id, added_on) values('$uid', '$loc_uid', '$push_group_id', '$vcode', now())");
			      if($isp_uid == '193')
			      {
					$msgurl = "https://ads360network.in/voucherRedeem/push_redeem.php?voucher_code=".$vcode."&loctype=".$location_type;
			      }
			      else
			      {
					$msgurl=VOUCHERPATH."voucherRedeem/push_redeem.php?voucher_code=".$vcode."&loctype=".$location_type;
			      }
			      $bitlurl = '';
			      if($offer_type != '1')
			      {
					$bitlurl=$this->get_bitly_short_url($msgurl);
			      }
			      
			      
			      if($send_via == 'sms')
			      {
					if(count($sms_gateway) > 0)
					{
						  if($sms_gateway[0]['gateway_name'] == 'infini')
						  {
							    $api_key = $sms_gateway[0]['api_key'];
							    $sender = $sms_gateway[0]['sender'];
							    $base_url = $sms_gateway[0]['base_url'];
							$this->push_voucher_code_via_sms_infini($offer_desc,$offer_type,$isp_uid,$vcode,$uid,$bitlurl,$api_key,$sender,$base_url);    
						  }
						  else
						  {
							$this->push_voucher_code_via_sms($offer_desc,$offer_type,$isp_uid,$vcode,$uid,$bitlurl);    
						  }
					}
					else
					{
						$this->push_voucher_code_via_sms($offer_desc,$offer_type,$isp_uid,$vcode,$uid,$bitlurl);	  
					}
					
			      }
			      else
			      {
					$this->push_voucher_code_via_email($offer_desc,$offer_type,$vcode, $location_type,$uid,$location_logo,$username,$location_name,$offer_name,$offer_desc,$bitlurl,$email);	
			      }
		    }   
	  }
	  public function push_voucher_code_via_sms_infini($offer_desc,$offer_type,$isp_uid,$vcode,$uid,$bitlurl,$api_key,$sender,$base_URL){
		    if($offer_type == 1)
		    {
			      $encoded_msg = urlencode($offer_desc);
		    }
		    else
		    {
			  $encoded_msg = $vcode.'%0a'.urlencode("Use the voucher code above to redeem the offer").'%0a'.$bitlurl;    
		    }
		
		$URL = $base_URL.'?method=sms'.'&api_key='.$api_key.'&sender='.$sender.'&to='.$uid.'&message='.$encoded_msg;
		    $curl = curl_init();
		    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		    curl_setopt($curl, CURLOPT_URL, $URL);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		    $response = curl_exec($curl);
		    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
		    $err      = curl_error($curl);
		    curl_close($curl);
		    return $response;
	  }
	  public function push_voucher_code_via_sms($offer_desc,$offer_type,$isp_uid,$vcode,$uid,$bitlurl){
		    if($offer_type == '1')
		    {
			$msg = urlencode($offer_desc);      
		    }
		    else
		    {
			   $msg = $vcode.'%0a'.urlencode("Use the voucher code above to redeem the offer").'%0a'.$bitlurl;   
		    }
		    
		    $postData = array(
			      'authkey' => '106103ADQeqKxOvbT856d19deb',
			      'mobiles' => $uid,
			      'message' => $msg,
			      'sender' => 'SHOUUT',
			      'route' => '4'
		    );
		    //print_r($postData);die;
		    $url="https://control.msg91.com/api/sendhttp.php";
		    $ch = curl_init();
		    curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $postData
			//,CURLOPT_FOLLOWLOCATION => true
		    ));
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    $output = curl_exec($ch);
		    curl_close($ch);
	  }
	  public function push_voucher_code_via_email($offer_desc,$offer_type,$vcode, $location_type,$uid,$location_logo,$username,$location_name,$offer_name,$offer_desc,$bitlurl,$email){
		    $msgmail='<!doctype html>
			      <html>
			      <head>
			      <meta charset="utf-8">
			      <meta name="viewport" content="width=device-width, initial-scale=1.0">
			      <title>Voucher Redeem</title>
			      <style type="text/css">
				  html,  body {
				      margin: 0 !important;
				      padding: 0 !important;
				      height: 100% !important;
				      width: 100% !important;
				 }
				      body{
				       background-color: #ffffff;
				      }
				      /* What it does: Stops email clients resizing small text. */
				      * {
					      -ms-text-size-adjust: 100%;
					      -webkit-text-size-adjust: 100%;
				      }
			      </style>
			      </head>
			      
			      <body>
				      <div style="width:700px; height: auto; margin: 0px auto; padding: 15px">
					<table width="700" border="0" cellspacing="0" cellpadding="0">
					<tbody>
					      <tr bgcolor="#f1f1f2">
						      <td style="text-align: center; color: #808184; font-size: 11px;padding:5px; font-family: \'Open Sans\', sans-serif; font-weight:600; font-style: italic">Unable to view the email? <a href="#" style="color:#25a9e0; text-decoration: none">Click here</a></td>
					      </tr>
					      <tr>
						<td>
							<div style="width:300px; height:100px;margin: 15px auto; background-color:#f2f2f2;">
							  <img src="'.$location_logo.'" width="100%"/>
							</div>
						</td>
					      </tr>
					      <tr>
						<td>&nbsp;</td>
					      </tr>
					      <tr>
						      <td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Hi '.$username.'&#44;</p></td>
					      </tr>';
					      if($offer_type == 1)
					      {
						  $msgmail .= '<tr>
							      <td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;"><strong>'.$offer_desc.'</strong>.</p></td>
							    </tr>';
					      }
					      else
					      {
						  
					      
					      $msgmail .= '<tr>
					      <td>
						<p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Thank you for requesting a voucher at<strong> '.$location_name.'</strong>.</p></td>
					      </tr>
					      <tr>
						<td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Voucher Details: <strong>'.$offer_desc.'</strong>.</p></td>
					      </tr>
						<tr>
						<td>&nbsp;</td>
					      </tr>
					      <tr>
						      <td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Please click below to view your voucher and redeem it.</p></td>
					      </tr>
					      <tr>
						<td>
							<a href="'.$bitlurl.'" target="_blank" style="background: #f00f64; padding:10px 20px;color: #ffffff; font-family: \'Open Sans\', sans-serif; font-size: 15px; line-height:25px; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: 400; height:25px; width: 180px; margin: 15px 0px 5px 0px"> 
					    VIEW VOUCHER
					</a>
						</td>
					      </tr>
					      <tr>
						      <td><span style="color:#929497; font-size: 13px; font-style:italic;font-family: \'Open Sans\', sans-serif;font-weight:400;">Please note that the voucher can be redeemed only once.</span></td>
					      </tr>
					      <tr>
						<td>&nbsp;</td>
					      </tr>';
					      }
					      $msgmail .= '
					      <tr>
						      <td><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 5px 0px;">Thanks,</p></td>
					      </tr>
					      <tr>
						<td style="border-bottom: 1px solid #e6e7e8"><p style="font-size: 13px; color:#231f20;font-family: \'Open Sans\', sans-serif; font-weight:600; margin: 10px 0px;">'.$location_name.'</p></td>
					      </tr>
						<tr>
						<td style="text-align: center; color: #808184; font-size: 13px;padding:5px; font-family: \'Open Sans\', sans-serif; font-weight:600; font-style: italic">Any issue with he vouchr? reach us at <a href="#" style="color:#25a9e0; text-decoration: none">help@shouut.com</a></td>
					      </tr>
					</tbody>
				      </table>
			      
				      </div>
			      </body>
			      </html>';
			      $sub=$location_name." ".$offer_name;
			      $this->load->library('email');
			      $from = SMTP_USER;
			      $fromname = 'SHOUUT';
			      $config = Array(
			      'protocol' => 'smtp',
			      'smtp_host' => SMTP_HOST,
			      'smtp_port' => SMTP_PORT,
			      'smtp_user' => SMTP_USER,
			      'smtp_pass' => SMTP_PASS,
			      'mailtype'  => 'html',
			      'charset'   => 'iso-8859-1',
			      'wordwrap'  => FALSE,
			      'crlf'      => "\r\n",
			      'newline'   => "\r\n"
			      );
			      $this->email->initialize($config);
			      $this->email->from($from, $fromname);
			      $this->email->to($email);
			      $this->email->subject($sub);
			      $this->email->message($msgmail);	
			      $this->email->send();
              
			   return true;
	  }
    

   
    

    

   
    

    
	  /**************************************************	
	   *	DLF DATA MAILCHIMP
	   *************************************************/
	  public function promenade_previousdata_mailchimpsync(){
		    $listid = '0ad6bc5c59';
		    $apikey = 'c5e7e8ef8c55c0e4c8a7cb3c571e298a-us18';
		    $userquery = $this->DB2->query('SELECT t2.firstname, t2.lastname, t2.email FROM `wifi_user_free_session` as t1 INNER JOIN `sht_users` as t2 ON(t1.userid=t2.uid) WHERE t1.`location_id` = 71 GROUP BY userid');
		    if($userquery->num_rows() > 0){
			      foreach($userquery->result() as $uobj){
					$firstname = $uobj->firstname;
					$lastname = $uobj->lastname;
					$email = $uobj->email;
					
					if($email != ''){
						  $data = array('email'=> $email, 'status' => 'subscribed','firstname' => $firstname,'lastname'  => $lastname);
						  if($apikey != '' && $listid != '')
						  {
						
						  	$this->syncMailchimp($data, $apikey, $listid);
						  }
					
					}
			      }
		    }
	  }
}



?>
