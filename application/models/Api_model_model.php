<?php
class Api_model extends CI_Model{
    public function login($jsondata){
        $data = array();
        $username = $jsondata->username;
        $password = $jsondata->password;
        $isp_uid = $jsondata->isp_uid;
        $password_new = md5($password);
        $query = $this->db->query("select uid from sht_users WHERE isp_uid = '$isp_uid' AND expired = '0' and enableuser = '1' and password = '$password_new' AND (uid = '$username' OR username = '$username' OR mobile = '$username' OR email = '$username')");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['resultCode'] = '1';
            $data['user_uid'] = $row['uid'];
            $data['resultMsg'] = 'Success';
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid username or password';
        }
        return $data;
    }
    
    public function isp_detail($jsondata){
        $data = array();
        $isp_id = $jsondata->isp_uid;
        $query = $this->db->query("select isp_name,logo_image, help_number1,help_number2,help_number3,support_email from sht_isp_detail where  isp_uid = '$isp_id'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $data['isp_name'] = $row['isp_name'];
            $data['logo_image'] = IMAGEPATHISP."logo/".$row['logo_image'];
            $data['help_number1'] = $row['help_number1'];
            $data['help_number2'] = $row['help_number2'];
            $data['help_number3'] = $row['help_number3'];
            $data['support_email'] = $row['support_email'];
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid ISP ID';
        }
        return $data;
    }
    
    public function slider_data($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select schsp.*, ss.srvname,ss.plantype from sht_consumer_home_slider_promotion as schsp inner join sht_services as ss on (schsp.plan_id = ss.srvid) where ss.isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $i = 0;
            $slider_data = array();
            foreach($query->result() as $row){
                $slider_data[$i]['service_id'] = $row->plan_id;
                $slider_data[$i]['service_name'] = $row->srvname;
                $slider_data[$i]['description'] = $row->description;
                $slider_data[$i]['image_path'] = IMAGEPATH.'promotion_image/slider_promotion/'.$row->image_path;
                if($row->plantype == '0'){
                    // topup
                    $plan_id = $row->plan_id;
                    $price_query = $this->db->query("select net_total from sht_topup_pricing where srvid = '$plan_id'");
                    $row_price = $price_query->row_array();
                    $slider_data[$i]['service_price'] = "@ &#8377;".$row_price['net_total'];
                }else{
                    // plan
                    $plan_id = $row->plan_id;
                    $price_query = $this->db->query("select net_total from sht_plan_pricing where srvid = '$plan_id'");
                    $row_price = $price_query->row_array();
                    $slider_data[$i]['service_price'] = "@ &#8377;".$row_price['net_total']." / month";
                }
                $i++;
            }
            
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $data['slider_data'] = $slider_data;
           
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid ISP ID OR No Data found';
        }
        return $data;
    }

    public function live_usage($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        $live_usage =0;
        $total_data = 0;
        $topupdata = 0;
        $monthly_data = 0;
        $speed = '';
        $due_amount = 0;
        $due_amout_expiration = '';
        // get topup limit
        $get_topup = $this->db->query("select ss.plantype, ss.datalimit from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$useruid' AND su.status='1' AND su.terminate='0' AND su.topuptype='1'");
        if($get_topup->num_rows() > 0){
            foreach($get_topup->result() as $tobj){
                $topupdata += $tobj->datalimit;
            }
        }
         // get user plan limit
        $get_plan = $this->db->query("select su.expiration, ss.plantype, ss.datalimit, ss.datacalc,ss.downrate, ss.fupdownrate from sht_users as su inner join sht_services as ss on(su.baseplanid = ss.srvid) where su.uid = '$useruid'");
        $data_allocated = 0;
        $plan_type = '';
        if($get_plan->num_rows() > 0){
            $get_plan_row = $get_plan->row_array();
            $plan_type = $get_plan_row['plantype'];
            $due_amout_expiration = date("d-m-Y", strtotime($get_plan_row['expiration']));
            //get speed
            $download_speed = $this->convertTodata($get_plan_row['downrate']."MB");
            if($download_speed < 1){
                $download_speed = $this->convertTodata($get_plan_row['downrate']."KB");
                $speed = round($download_speed,0)." kbps"; 
            
            }else{
               $speed = round($download_speed,0)." mbps"; 
            }
            if($get_plan_row['plantype'] == '1'){
                $plan_msg = "Unlimited plan";
                $total_data = $plan_msg;
                $monthly_data = "";
                $topupdata = "";
            }elseif($get_plan_row['plantype'] == '3'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
                //$total_data = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB";
                $total_data = $data_allocated;
                $monthly_data = "Monthly Data: ".$monthly_data;
                $topupdata = "Topup Data: ".$topupdata;
                
                // for after fup speed
                 $unlimited_speed = $this->convertTodata($get_plan_row['fupdownrate']."MB");
                if($unlimited_speed < 1){
                    $unlimited_speed = $this->convertTodata($get_plan_row['fupdownrate']."KB");
                    $speed = $speed."/".round($unlimited_speed,0)." kbps (Unlimited)"; 
                
                }else{
                   $speed = $speed."/".round($unlimited_speed,0)." mbps (Unlimited)"; 
                }
                
            }elseif($get_plan_row['plantype'] == '4'){
                $data_allocated = $get_plan_row['datalimit']+$topupdata;
                $monthly_data = round(($get_plan_row['datalimit']/(1024*1024*1024)),2)."GB";
                $topupdata = round(($topupdata/(1024*1024*1024)),2)."GB";
               // $total_data = "Total Data: ".round(($data_allocated/(1024*1024*1024)),2)."GB";
                $total_data = $data_allocated;
                $monthly_data = "Monthly Data: ".$monthly_data;
                $topupdata = "Topup Data: ".$topupdata;
            }
        }
        // get billing date
        $get_billing_date = $this->db->query("select billing_cycle from sht_billing_cycle where isp_uid = '$isp_uid'");
        $start_date = ''; $end_date = '';
        if($get_billing_date->num_rows() > 0){
            $row_billing_date = $get_billing_date->row_array();
            if(date("j") >= $row_billing_date['billing_cycle']){
                //current month to next month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                $start_date =  $date->format('Y-m-d');
                $end_date = date('Y-m-d', strtotime('+1 month -1 day', strtotime($start_date)));
            }else{
                //previous month to current month
                $date = date('Y-m-d');
                $date = new DateTime($date);
                $date->setDate($date->format('Y'), $date->format('m'), $row_billing_date['billing_cycle']);
                $end_date =  date('Y-m-d', strtotime('-1 day', strtotime($date->format('Y-m-d'))));
                $start_date = date('Y-m-d', strtotime('-1 month +1 day', strtotime($end_date)));
            }
        }
        // get monthly usage
        $query = $this->db->query("select acctinputoctets, acctoutputoctets from radacct where username = '$useruid' AND DATE(acctstarttime) between '$start_date' and '$end_date'");
       
        foreach($query->result() as $row){
            $datacalc = $get_plan_row['datacalc'];
           if($datacalc == 2){
            $live_usage = $live_usage + $row->acctinputoctets + $row->acctoutputoctets;
           }else{
            $live_usage = $live_usage + $row->acctoutputoctets;
           }
        }
        /*$live_usage_value = round($live_usage/(1024*1024*1024),2);
        if($live_usage_value < 1){
            //get in mb
            $live_usage_value = round($live_usage/(1024*1024),2);
            if($live_usage_value < 1){
                //get in kb
                $live_usage_value = round($live_usage/(1024),2)."KB";
            }else{
               $live_usage_value = $live_usage_value."MB" ;
            }
        }else{
            $live_usage_value = $live_usage_value."GB";
        }*/
        $live_usage_value = $live_usage;
        $data['live_usage'] = $live_usage_value;
        $data['total_data'] = $total_data;
        $data['monthly_data'] = $monthly_data;
        $data['topup_data'] = $topupdata;
        $data['current_speed'] = $speed;
    
        $get_due_amount =  $this->userbalance_amount($useruid);
        if($get_due_amount < 0){
            $due_amount = abs($get_due_amount);
        }else{
            $due_amout_expiration = '';
        }
        $data['due_amount'] = $due_amount;
        $data['due_amount_expiration'] = $due_amout_expiration;
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    public function userbalance_amount($uuid){
        $wallet_amt = 0;
        $walletQ = $this->db->query("SELECT COALESCE(SUM(wallet_amount),0) as wallet_amt FROM sht_subscriber_wallet WHERE subscriber_uuid='".$uuid."'");
        if($walletQ->num_rows() > 0){
            $wallet_amt = $walletQ->row()->wallet_amt;
        }
        
        $passbook_amt = 0;
        $passbookQ = $this->db->query("SELECT COALESCE(SUM(plan_cost),0) as plan_cost FROM sht_subscriber_passbook WHERE subscriber_uuid='".$uuid."'");
        if($passbookQ->num_rows() > 0){
            $passbook_amt = $passbookQ->row()->plan_cost;
        }
        $balanceamt = $wallet_amt - $passbook_amt;

        return $balanceamt;
    }
    public function convertTodata($from) {
        $number = substr($from, 0, -2);
        switch (strtoupper(substr($from, -2))) {
            case "KB":
                return $number / 1024;
            case "MB":
                return $number /(1024*1024);
            case "GB":
                return $number / (1024*1024*1024);
            case "TB":
                return $number / pow(1024, 4);
            case "PB":
                return $number / pow(1024, 5);
            default:
                return $from;
        }
    }

    public function promotions($jsondata){
        $data = array();
        $promotions = array();
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select * from sht_consumer_marketing_promotion where start_date <= now() AND end_date >= now() and is_deleted = '0' and status = '1' AND isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $i = 0;
            foreach($query->result() as $row){
                $promotions[$i]['image_path'] = IMAGEPATH.'promotion_image/marketing_promotion/'.$row->image_path;
                $promotions[$i]['title'] = $row->title;
                $promotions[$i]['description'] = $row->description;
                $promotions[$i]['promotion_offer'] = $row->promotion_offer;
                $promotions[$i]['redirect_link'] = $row->redirect_link;
               $i++; 
            }
            
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'No promotion live';
        }
        $data['promotions'] = $promotions;
        //echo "<pre>";print_r($data);die;
        return $data;
     }

    
    public function account_detail($jsondata){
        $data = array();
        $userid = $jsondata->user_uid;
        $query = $this->db->query("select su.*, sc.city_name, sz.zone_name from sht_users as su left join sht_cities as sc on (su.city = sc.city_id)
                                  left join sht_zones as sz on (su.zone = sz.id) where su.uid = '$userid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['name'] = $row['firstname']." ".$row['middlename'].' '.$row['lastname'];
            $data['uid'] = $row['uid'];
            $data['username'] = $row['username'];
            $data['email'] = $row['email'];
            $data['mobile'] = $row['mobile'];
            $data['address'] = $row['flat_number'].', '. $row['address'];
            $data['city'] = $row['city_name'];
            $data['zone'] = $row['zone_name'];
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    private  $baseUrl = "http://sendotp.msg91.com/api";
    public function update_mobile_send_otp($jsondata){
        $data_responce = array();
        $phone = $jsondata->old_number;
        $data = array("countryCode" => "91", "mobileNumber" => "$phone","getGeneratedOTP" => true);
        $data_string = json_encode($data);
        $ch = curl_init($this->baseUrl.'/generateOTP');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json',
     'Content-Length: ' . strlen($data_string),
     'application-Key: gGlhmtbDPqDEBNqIBINjXZfsLyVy5jOOszvb1Jy9SEHFN-HlARjLn-cGEsv2hZc9VBlyWSm2A9TaQDLzj2gejukAG0ZQrKsFynxW4s2NewHaPXbcU41WUEwiN7BJSjesbbjF4GdSvJ57rOo5yj1XcA=='
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        $otp_gets = '';
        $response = json_decode($result,true);
        if($response["status"] == "error"){
           
        }else{
            $otp_gets =  $response["response"]["oneTimePassword"];
        }
        
        $useruid = $jsondata->user_uid;
        $this->db->query("insert into sht_otp_verification (uid, otp, is_used) values('$useruid', '$otp_gets', '0')");
        $data_responce['otp'] = $otp_gets;
        return $data_responce;
    }
    
    public function update_mobile_verify_otp($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $otp = $jsondata->otp;
        $mobile = $jsondata->new_number;
        $otp_veri = $this->db->query("select * from sht_otp_verification where uid = '$useruid' and otp = '$otp' and is_used = '0' order by id desc limit 1");
        if($otp_veri->num_rows() > 0){
            $row = $otp_veri->row_array();
            $id = $row['id'];
            $this->db->query("update sht_otp_verification set is_used = '1' where id = '$id'");
            // update mobile
            $this->db->query("update sht_users set mobile = '$mobile' where uid = '$useruid'");
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'SUccessfully Updated phone number';
        }else{
           $data['resultCode'] = '0';
           $data['resultMsg'] = 'Invalid OTP';
        }
        return $data;
    }

    public function usage_logs($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $month = explode(" ", $jsondata->month);
        $current_month = $month[0] ;
        $current_year = $month[1];
        $logs = array();
        $query = $this->db->query("select * from sht_user_daily_data_usage where uid = '$useruid' AND MONTH(start_time) = '$current_month' AND YEAR(start_time) = '$current_year' order by id desc");
    
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
            $i = 0;
            foreach($query->result() as $row){
                $logs[$i]['start_time'] = $row->start_time;
                $logs[$i]['end_time'] = $row->end_time;
                $logs[$i]['upload'] = round($row->upload/(1024*1024),2);
                $logs[$i]['download'] = round($row->download/(1024*1024),2);
                $i++;
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "No logs found";
        }
        $data['usage_logs'] = $logs;
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    public function my_plan($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $query = $this->db->query("select ss.plantype,su.uid,ss.srvname,ss.plantype,ss.downrate, ss.fupdownrate,ss.datalimit, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "success";
            
            $row = $query->row_array();
            $data['planname'] = $row['srvname'];
            $data['rental'] = $row['net_total'];
            $speed = '';
            $download_speed = $this->convertTodata($row['downrate']."MB");
            if($download_speed < 1){
                $download_speed = $this->convertTodata($row['downrate']."KB");
                $speed = round($download_speed,0)." kbps"; 
            
            }else{
               $speed = round($download_speed,0)." mbps"; 
            }
            if($row['plantype'] == 3){
               $unlimited_speed = $this->convertTodata($row['fupdownrate']."MB");
                if($unlimited_speed < 1){
                    $unlimited_speed = $this->convertTodata($row['fupdownrate']."KB");
                    $speed = $speed."(FUP) /".round($unlimited_speed,0)." kbps(unlimited)"; 
                
                }else{
                   $speed = $speed."(FUP) / ".round($unlimited_speed,0)." mbps(Unlimited)"; 
                }
                
                $fup_limit = $this->convertTodata($row['datalimit']."GB");
                $data['fup_limit'] = round($fup_limit,2)." GB". " / month";
            }else{
                $data['fup_limit'] = 'None';
            }
            $data['speed'] = $speed;
             //get num of topup assign to user
            $uid = $row['uid'];
            $get_otp_query = $this->db->query("select * from sht_usertopupassoc where uid = '$uid' and terminate = '0' and topuptype != '1' and topup_enddate >= CURDATE()");
            $data['topup'] = $get_otp_query->num_rows();
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

     public function recommended_plans($jsondata){
        $data = array();
        $plans = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select su.zone, su.city, su.state,su.baseplanid, spp.net_total from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid) left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_active = $row['baseplanid'];
            $user_current_plan_price = $row['net_total'];
            //get all live plan
            $get_plan = $this->db->query("select ss.*, spp.region_type, spp.net_total from sht_services as ss  left join sht_plan_pricing as spp on (ss.srvid = spp.srvid) where ss.plantype != '0' and ss.plantype != '2' and ss.srvid != '$user_current_plan_active' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' AND ss.isp_uid = '$isp_uid'");
            $i = 0;
            if($get_plan->num_rows() > 0){
                
                foreach($get_plan->result() as $get_plan1){
                    // first check plan price is equal or grater
                    if($get_plan1->net_total >= $user_current_plan_price){
                        // if region_type  = allindia in sht_plan_pricing then show this plan dont check in plan region table
                        if($get_plan1->region_type == 'allindia'){
                            $plans[$i]['service_id'] = $get_plan1->srvid;
                            $plans[$i]['plan_name'] = $get_plan1->srvname;
                            if($get_plan1->plantype == 1){
                                $plan_type = "Unlimited Plan";
                                $plans[$i]['total_data'] = "&nbsp;";
                            }elseif($get_plan1->plantype == 2){
                                $plan_type = 'Time Plan';
                                $plans[$i]['total_data'] = " ";
                            }
                            elseif($get_plan1->plantype == 3){
                                $plan_type = 'FUP Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $plans[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                            }
                            elseif($get_plan1->plantype == 4){
                                $plan_type = 'Data Plan';
                                $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                $plans[$i]['total_data'] = round($data_limit,2)." GB";
                            }
                            $plans[$i]['plan_type'] = $plan_type;
                            $plans[$i]['plan_cost'] = ceil($get_plan1->net_total);
                            $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                            if($download_speed < 1){
                                $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                $plans[$i]['speed'] = ceil($download_speed)." kbps";
                            }else{
                                $plans[$i]['speed'] = ceil($download_speed)." mbps"; 
                            }
                            $i++;
                        }
                        else{
                            // get region is same as user from sht_plan_region table
                            $service_id = $get_plan1->srvid;
                            //$zone_ids = array();
                            $is_valid_offer = 0;
                            $get_plan_region = $this->db->query("select zone_id, city_id,state_id from sht_plan_region where plan_id = '$service_id'");
                            foreach($get_plan_region->result() as $get_plan_region1){
                                //$zone_ids[] = $get_plan_region1->zone_id;
                                //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_offer = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_offer = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_offer = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            $is_fup_plan = '';
                             // now check region is same as user region or all
                             if($is_valid_offer == 1){
                                $plans[$i]['service_id'] = $get_plan1->srvid;
                                $plans[$i]['plan_name'] = $get_plan1->srvname;
                                $plan_type = '';
                                if($get_plan1->plantype == 1){
                                    $plan_type = "Unlimited Plan";
                                    $plans[$i]['total_data'] = "&nbsp;";
                                }elseif($get_plan1->plantype == 2){
                                    $plan_type = 'Time Plan';
                                    
                                    $plans[$i]['total_data'] = "&nbsp;";
                                }
                                elseif($get_plan1->plantype == 3){
                                    $plan_type = 'FUP Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                    $plans[$i]['total_data'] = round($data_limit,2)." GB (FUP)";
                                }
                                elseif($get_plan1->plantype == 4){
                                    $plan_type = 'Data Plan';
                                    $data_limit = $this->convertTodata($get_plan1->datalimit."GB");
                                   $plans[$i]['total_data'] = round($data_limit,2)." GB";
                                }
                                $plans[$i]['plan_type'] = $plan_type;
                                $plans[$i]['plan_cost'] = ceil($get_plan1->net_total);
                                $download_speed = $this->convertTodata($get_plan1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_plan1->downrate."KB");
                                    $plans[$i]['speed'] = ceil($download_speed)." <small>kbps</small>";
                                }else{
                                    $plans[$i]['speed'] = ceil($download_speed)." <small>mbps</small>"; 
                                }
                           
                                
                                    $i++;
                    
                            }
                        }
                    }
                }
                if(count($plans) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['recommended_plans'] = $plans;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Suggested Plan";
                }
                
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Suggested Plan";
            }
            
            
           
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    public function active_topups($jsondata){
        $data = array();
        $topup = array();
        $user_uid = $jsondata->user_uid;
        $get_otp_query = $this->db->query("select su.*,ss.srvname, ss.topuptype from sht_usertopupassoc as su inner join sht_services as ss on(su.topup_id = ss.srvid) where su.uid = '$user_uid' and su.terminate = '0' and ss.enableplan = '1' and ss.is_deleted = '0' and ss.active = '0' and su.topuptype != '1'  and su.topup_enddate >= CURDATE()");
        if($get_otp_query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $i = 0;
            foreach($get_otp_query->result() as $row){
                $topup[$i]['topupname'] = $row->srvname;
                if($row->topuptype == '2'){
                    $topuptype = "Dataunacnttopup";
                    $topup[$i]['topupstartdate'] = date("d-m-Y",strtotime($row->topup_startdate));
                    $topup[$i]['topupenddate'] = date("d-m-Y",strtotime($row->topup_enddate));
                }else{
                    $topuptype = "SpeedTopup";
                    $topup[$i]['topupstartdate'] = date("d-m-Y",strtotime($row->topup_startdate));
                    $topup[$i]['topupenddate'] = date("d-m-Y",strtotime($row->topup_enddate));
                }
            
                $topup[$i]['topuptype'] = $topuptype;
                $i++;
            }
            $data['active_topup'] = $topup;
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'No topup apply';
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }

    public function recommended_speed_topup($jsondata){
        $data = array();
        $speed_topup = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.topuptype ='3' and stp.payment_type = 'Paid' AND ss.isp_uid = '$isp_uid'");
            if($get_topup->num_rows() > 0){
                $k = 0;
                foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                        $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                        $speed_topup[$k]['topup_id'] = $get_topup1->srvid;
                        $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                        if($download_speed < 1){
                            $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                            $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                        }else{
                            $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                        }
                        //pricing
                        $speed_topup_price = 0;
                        $start_time = '';
                        $end_time = '';
                        $days = array();
                        $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                        foreach($get_speed_pricing->result() as $get_speed_pricing1){
                            $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                            $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                            $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                            $days[] = $get_speed_pricing1->days;
                        }
                        $speed_topup[$k]['topup_price'] = $speed_topup_price;
                        if(count($days) == '7'){
                            $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                        }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                            $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                        }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                            $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                        }else{
                            $days_new = array();
                            foreach($days as $days1){
                                $days_new[] = substr(trim($days1), 0, 3);
                            }
                            $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                        }
                           $k++;
                    }
                    else{
                         $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        if($is_valid_topup == 1){
                            $speed_topup[$k]['topup_name'] = $get_topup1->srvname;
                                 $speed_topup[$k]['topup_id'] = $get_topup1->srvid;
                                $download_speed = $this->convertTodata($get_topup1->downrate."MB");
                                if($download_speed < 1){
                                    $download_speed = $this->convertTodata($get_topup1->downrate."KB");
                                    $speed_topup[$k]['download_speed'] = round($download_speed,2)." kbps"; 
                                
                                }else{
                                   $speed_topup[$k]['download_speed'] = round($download_speed,2)." mbps"; 
                                }
                                //pricing
                                $speed_topup_price = 0;
                            
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_speed_pricing = $this->db->query("select * from sht_speedtopup where srvid = '$topupid'");
                                foreach($get_speed_pricing->result() as $get_speed_pricing1){
                                    $speed_topup_price = $speed_topup_price + $get_topup1->net_total;
                                    $start_time = date("h:i a",strtotime($get_speed_pricing1->starttime));
                                    $end_time = date("h:i a",strtotime($get_speed_pricing1->stoptime));
                                    $days[] = $get_speed_pricing1->days;
                                }
                                $speed_topup[$k]['topup_price'] = $speed_topup_price;
                                if(count($days) == '7'){
                                   $speed_topup[$k]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                    $speed_topup[$k]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                    $speed_topup[$k]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $speed_topup[$k]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                            
                                $k++;
                        }
                        
                    }
                }
                if(count($speed_topup) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['speed_topup'] = $speed_topup;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Recommended Speed Topup";
                }
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Recommended Speed Topup";
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>"; print_r($data);die;
        return $data;
    }

    
    public function recommended_unaccoundancy_topup($jsondata){
        $data = array();
        $data_unacc_topup = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.topuptype ='2' and stp.payment_type = 'Paid' AND ss.isp_uid = '$isp_uid'");
            if($get_topup->num_rows() > 0){
                $j = 0;
                 foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                        $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                            $data_unacc_topup[$j]['topup_id'] = $get_topup1->srvid;
                            //pricing
                            $unaccount_topup_price = 0;
                            $unaccount_topup_percent = 0;
                            $start_time = '';
                            $end_time = '';
                            $days = array();
                            $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                            foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                $days[] = $get_unaccount_pricing->days;
                            }
                            $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                            $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                            if(count($days) == '7'){
                               $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                            }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                            }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                            }else{
                                 $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                            }
                            $j++;
                    }
                    else{
                         $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        if($is_valid_topup == 1){
                            $data_unacc_topup[$j]['topup_name'] = $get_topup1->srvname;
                               $data_unacc_topup[$j]['topup_id'] = $get_topup1->srvid;
                                //pricing
                                $unaccount_topup_price = 0;
                                $unaccount_topup_percent = 0;
                                $start_time = '';
                                $end_time = '';
                                $days = array();
                                $get_unaccount_pricing = $this->db->query("select * from sht_dataunaccountancy where srvid = '$topupid'");
                                foreach($get_unaccount_pricing->result() as $get_unaccount_pricing){
                                    $unaccount_topup_price = $unaccount_topup_price + $get_topup1->net_total;
                                    $unaccount_topup_percent = $get_unaccount_pricing->datacalcpercent;
                                    $start_time = date("h:i a",strtotime($get_unaccount_pricing->starttime));
                                    $end_time = date("h:i a",strtotime($get_unaccount_pricing->stoptime));
                                    $days[] = $get_unaccount_pricing->days;
                                }
                                $data_unacc_topup[$j]['topup_price'] = $unaccount_topup_price;
                                $data_unacc_topup[$j]['topup_percent'] = $unaccount_topup_percent;
                                if(count($days) == '7'){
                                   $data_unacc_topup[$j]['topup_days'] = 'Daily '.$start_time.'-'.$end_time;  
                                }elseif(count($days) == '2' && in_array('Sat',$days) && in_array("Sun", $days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'All Day Weekends ';
                                }elseif(count($days) == '5' && in_array('Mon',$days) && in_array('Tue',$days)&& in_array('Wed',$days)&& in_array('Thu',$days)&& in_array('Fri',$days)){
                                    $data_unacc_topup[$j]['topup_days'] = 'Weekends '.$start_time.'-'.$end_time;
                                }else{
                                    $days_new = array();
                                    foreach($days as $days1){
                                         $days_new[] = substr(trim($days1), 0, 3);
                                    }
                                    $data_unacc_topup[$j]['topup_days'] = implode(',',$days_new).' '.$start_time.'-'.$end_time;
                                }
                                $j++;
                        }
                        
                    }
                }
                if(count($data_unacc_topup) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['unaccoundancy_topup'] = $data_unacc_topup;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Recommended Unaccoundancy Topup";
                }
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Recommended Unaccoundancy Topup";
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>"; print_r($data);die;
        return $data;
    }

    public function recommended_data_topup($jsondata){
        $data = array();
        $data_topup = array();
        $useruid = $jsondata->user_uid;
        $isp_uid = $jsondata->isp_uid;
        //get user zone
        $query = $this->db->query("select su.zone, su.city, su.state, ss.plantype from sht_users as su left join sht_services as ss on (su.baseplanid = ss.srvid)  where su.uid = '$useruid'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $user_zone = $row['zone'];
            $user_city = $row['city'];
            $user_state = $row['state'];
            $user_current_plan_type = $row['plantype'];
            //get all topup list
            $get_topup = $this->db->query("select ss.*, stp.payment_type, stp.net_total, stp.region_type from sht_services as ss inner join sht_topup_pricing as stp on (ss.srvid = stp.srvid) where ss.enableplan and ss.is_deleted = '0' and ss.active = '0' and ss.topuptype ='1' and stp.payment_type = 'Paid' AND isp_uid = '$isp_uid'");
            if($get_topup->num_rows() > 0){
                $i = 0;
                 foreach($get_topup->result() as $get_topup1){
                    $topupid = $get_topup1->srvid;
                    $topup_type = $get_topup1->topuptype;
                    if($get_topup1->region_type == 'allindia'){
                         $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                            $data_topup[$i]['topup_id'] = $get_topup1->srvid;
                            $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                            $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                            $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                            $i++;
                    }
                    else{
                         $is_valid_topup = 0;
                        $get_plan_region = $this->db->query("select zone_id,city_id,state_id from sht_topup_region where topup_id = '$topupid'");
                        foreach($get_plan_region->result() as $get_plan_region1){
                            //$zone_ids[] = $get_plan_region1->zone_id;
                            //first check state
                                if($user_state == $get_plan_region1->state_id || $get_plan_region1->state_id == 'all'){
                                    //if state is all then valid no need to check city or zone
                                    if($get_plan_region1->state_id == 'all'){
                                        $is_valid_topup = 1;
                                        break;
                                    }else{
                                        //check city is same or all
                                        if($user_city == $get_plan_region1->city_id || $get_plan_region1->city_id == 'all'){
                                            // if city is all then valid no need to check zone 
                                            if($get_plan_region1->city_id == 'all'){
                                                $is_valid_topup = 1;
                                                break;
                                            }else{
                                                if($user_zone == $get_plan_region1->zone_id || $get_plan_region1->zone_id == 'all'){
                                                    $is_valid_topup = 1;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        if($is_valid_topup == 1){
                            $data_topup[$i]['topup_name'] = $get_topup1->srvname;
                                $data_topup[$i]['topup_id'] = $get_topup1->srvid;
                                $data_limit = $this->convertTodata($get_topup1->datalimit."GB");
                                $data_topup[$i]['total_data'] = round($data_limit,2)." GB ";
                                $data_topup[$i]['topup_price'] = $get_topup1->net_total;
                                $i++;
                        }
                        
                    }
                }
                if(count($data_topup) > 0){
                    $data['resultCode'] = '1';
                    $data['resultMsg'] = "Success";
                    $data['data_topup'] = $data_topup;
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMsg'] = "No Recommended Data Topup";
                }
            }else{
                $data['resultCode'] = '0';
                $data['resultMsg'] = "No Recommended Data Topup";
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "Something went wrong";
        }
        //echo "<pre>"; print_r($data);die;
        return $data;
    }

    public function pending_bills($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $bills = array();
        $query = $this->db->query("select * from sht_subscriber_billing where status = '1' and is_deleted = '0' and receipt_received = '0' and subscriber_uuid = '$useruid' and bill_type != 'Advprepay' order by id desc");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
            $i = 0;
             foreach($query->result() as $row){
                $bills[$i]['bill_id'] = $row->id;
               
                if($row->bill_type == 'montly_pay'){
                    $bill_generated_on = $row->bill_added_on;
                    $month  = date('M', strtotime("-1 Month",strtotime($bill_generated_on)));
                    $bills[$i]['bill_type'] = "Monthly Pay (".$month.")"; 

                }else{
                   $bills[$i]['bill_type'] = ucwords($row->bill_type); 
                }
                
                $bills[$i]['bill_amount'] = $row->total_amount;
                $i++;
            }
            $data['bills'] = $bills;
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "No pending bill";
            
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    public function previous_bills($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $bills = array();
        $query = $this->db->query("select * from sht_subscriber_billing where subscriber_uuid = '$useruid' and bill_type != 'Advprepay'  order by id desc");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
            $i = 0;
            foreach($query->result() as $row){
                $bills[$i]['generated_on'] = date("d.m.Y",strtotime($row->bill_added_on));
                $bills[$i]['payment_type'] = $row->bill_type;
                $bills[$i]['bill_number'] = ($row->bill_number != '')?$row->bill_number:"-";
                $bills[$i]['receipt_number'] = ($row->receipt_number != '')?$row->receipt_number:"-";
                $bills[$i]['transaction_id'] = ($row->transection_id!=0)?$row->transection_id:"-";
                $bills[$i]['payment_amount'] = $row->total_amount;
                $bills[$i]['payment_mode'] = $row->payment_mode;
                $bills[$i]['bill_status'] = ($row->receipt_received == '1')?'Paid':"Pending";
                $bills[$i]['bill_payed'] = ($row->receipt_received == '1')?'1':"0";
                $i++;
              
            }
            $data['bills'] = $bills;
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "No previous bill";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    public function current_service_request($jsondata){
        $data = array();
        $useruid = $jsondata->user_uid;
        $current_request = array();
        $query = $this->db->query("select * from sht_subscriber_tickets where subscriber_uuid = '$useruid' and status = '0' order by id desc");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = "Success";
             $i = 0;
            foreach($query->result() as $row){
                $current_request[$i]['reqiested_on'] = date("d.m.Y",strtotime($row->added_on));
                $current_request[$i]['ticket_type'] = ucfirst(str_replace('_', ' ', $row->ticket_type));
                $current_request[$i]['ticket_desc'] = $row->ticket_description;
                if($row->ticket_assign_to != '0'){
                    $current_request[$i]['status'] = "In Progress ";
                }else{
                  $current_request[$i]['status'] = "Open";  
                }
                $current_request[$i]['ticket_id'] = $row->ticket_id;
                
                $i++;
            }
            $data['current_request'] = $current_request;
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = "No prending request";
        }
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    public function generate_service_request($jsondata){
        $data = array();
        $subscriber_uuid = $jsondata->user_uid;
        $ticket_type = $jsondata->ticket_type;
        $ticket_priority = $jsondata->ticket_priority;
        $ticket_description = $jsondata->ticket_description;
        $ticketid = date("j").date('n').date("y").date('His');
        if($ticket_type != '' && $ticket_priority != '' && $ticket_description != ''){
            $data['resuldCode'] = '1';
            $data['resultMsg'] = "Success";
            //get sescriberid
            $subscriber_id = '';
            $get_id = $this->db->query("select id from sht_users where uid = '$subscriber_uuid'");
            if($get_id->num_rows() > 0){
                $row = $get_id->row_array();
                $subscriber_id = $row['id'];
            }
            $insert = $this->db->query("insert into sht_subscriber_tickets(ticket_id, subscriber_id, subscriber_uuid, ticket_type, ticket_priority,ticket_raise_by,ticket_description,status,added_on) values('$ticketid', '$subscriber_id', '$subscriber_uuid', '$ticket_type', '$ticket_priority', '0', '$ticket_description','0', now())");
        }else{
            $data['resuldCode'] = '0';
            $data['resultMsg'] = "Please Fill all data";
        }
        
        return $data;
    }
    
    
    public function update_password($jsondata){
        $data = array();
        $user_uid = $jsondata->user_uid;
        $current_password = $jsondata->current_password;
        $current_password = md5($current_password);
        $query = $this->db->query("select uid from sht_users where uid = '$user_uid' and password = '$current_password'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Successfully updated';
            $new_password = $jsondata->new_password;
            $new_password_mdb = md5($new_password);
            $update = $this->db->query("update sht_users set password = '$new_password_mdb', orig_pwd = '$new_password' where uid = '$user_uid'");
        
        }else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'Invalid Current Password';
        }
        
       
        return $data;
    }
    
    public function contact_us($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $query = $this->db->query("select * from sht_isp_detail where isp_uid = '$isp_uid'");
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMsg'] = 'Success';
            $row = $query->row_array();
            $data['company_name'] = $row['company_name'];
            $data['address'] = $row['address1'];
            $data['city'] = $row['city'];
            $data['pin'] = $row['pincode'];
            $data['help_number1'] = $row['help_number1'];
            $data['help_number2'] = $row['help_number2'];
            $data['help_number3'] = $row['help_number3'];
            $data['support_email'] = $row['support_email'];
            $data['lat'] = $row['lat'];
            $data['long'] = $row['long'];
            $data['place_id'] = $row['place_id'];
        }
        else{
            $data['resultCode'] = '0';
            $data['resultMsg'] = 'No data found';
        }
        //echo "<pre>";print_r($data);
        return $data;
    }
    
}



?>
