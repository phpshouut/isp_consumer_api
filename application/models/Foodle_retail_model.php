<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class Foodle_retail_model extends CI_Model {
    private $db2; private $currsymbol = '';
    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2_publicwifi', TRUE);
    }
	
    public function log($function, $request, $responce) {
        $log_data = array(
            'function' => $function,
            'request' => $request,
            'response' => $responce,
            'added_on' => date("Y-m-d H:i:s"),
        );
		$this->db2->insert("foodle_logs",$log_data);
      //  $this->mongo_db->insert('log_data', $log_data);
    }
    
    public function item_list(){
	$data=array();
	$post=$this->input->post();
	$query=$this->db2->query("select id,reward_name,reward_image from foodle_loyalty_reward where is_deleted='0' order by priority asc");
	if($query->num_rows()>0)
	{
	    $data['resultcode']=200;
	    $data['resultmsg']="Success";
	    $itemarr=array();
	    $i=0;
	    foreach($query->result() as $val){
		$itemarr[$i]['reward_id']=$val->id;
		$itemarr[$i]['reward_name']=$val->reward_name;
		$itemarr[$i]['reward_image']=FOODLE_IMAGEPATH.trim($val->reward_image);
		$i++;
	    }
	    $data['items']=$itemarr;
	    
	}else{
	    $data['resultcode']=404;
	    $data['resultmsg']="No Item Added";
	}
	return $data;
    }
    
    public function create_reward_offer($jsondata){
	$post=$this->input->post();
	if($jsondata->editruleid!=''){
	    $tabledata=array("slab_amt"=>$jsondata->slab_amt,"updated_on"=>date("Y-m-d H:i:s"));
	    $this->db2->update("foodle_location_loyalty_reward",$tabledata,array("id"=>$jsondata->editruleid));
	    $data['resultcode']=200;
	    $data['resultmsg']="Succes";
	}else{
	$query=$this->db2->query("select id from foodle_location_loyalty_reward where loc_id='".$jsondata->loc_id."' and reward_id='".$jsondata->reward_id."' and is_campaign='0'");
	
	if($query->num_rows()==0){
	    $data['resultcode']=200;
	    $data['resultmsg']="Succes";
	    $tabledata=array("reward_id"=>$jsondata->reward_id,"reward_name"=>$jsondata->reward_name,"loc_id"=>$jsondata->loc_id
	    ,"loc_uid"=>$jsondata->loc_uid,"is_deleted"=>0,"added_on"=>date("Y-m-d H:i:s"),"slab_amt"=>$jsondata->slab_amt);
	    $this->db2->insert("foodle_location_loyalty_reward",$tabledata);
	    $lastid=$this->db2->insert_id();
	    
	    
	    $data['loc_reward_id']=$lastid;
	}else{
	    $data['resultcode']=404;
	    $data['resultmsg']="Rule Already Added";
	   
	}
	}
	return $data;
    }
    
    public function reward_rules_list($jsondata){
	$cond='';
	$data=array();
	if($jsondata->type=="active"){
	    $cond.="and fllr.is_deleted='0'";
	}
	$dayscond='';
	if($jsondata->days!=''){
	    $days=$jsondata->days;
	    $enddate=date("Y-m-d");
	    $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	   // $dayscond.="and date(fllr.added_on) between '".$startdate."' and '".$enddate."'";
	}
	
	
	$query=$this->db2->query("select flr.id as reward_id,fllr.id,fllr.reward_name,(select count(id) from foodle_user ) as totaluser,
				 (select  COUNT(DISTINCT(USER)) from foodle_user_wallet where amount>fllr.slab_amt and loc_id='".$jsondata->loc_id."') as unlocked,
				 (select  COUNT(DISTINCT(USER)) from foodle_unlocked_offer where reward_id=fllr.id and is_redeemed='1' ) as redeemed,
				 flr.reward_image,fllr.slab_amt from  foodle_location_loyalty_reward fllr
				inner join foodle_loyalty_reward flr on (fllr.reward_id=flr.id and fllr.is_deleted='0')
				where fllr.loc_id='".$jsondata->loc_id."' {$cond} and fllr.is_campaign='0' {$dayscond} order by fllr.slab_amt asc");
	//echo $this->db2->last_query(); die;
	
	
	
	if($query->num_rows()>0){
	    $data['resultcode']=200;
	    $data['resultmsg']="Success";
	    $ruleasarr=array();
	    $i=0;
	    foreach($query->result() as $val){
		$ruleasarr[$i]['loc_reward_id']=$val->id;
		$ruleasarr[$i]['reward_name']=$val->reward_name;
		$ruleasarr[$i]['slab_amt']=number_format($val->slab_amt);
		$ruleasarr[$i]['locked_user']=$val->totaluser-$val->unlocked;
		$ruleasarr[$i]['unlocked_user']=$val->unlocked;
		$ruleasarr[$i]['redeemed_user']=$val->redeemed;
		$ruleasarr[$i]['reward_id']=$val->reward_id;
		$ruleasarr[$i]['origslab_amt']=(int)$val->slab_amt;
		$ruleasarr[$i]['reward_image']=FOODLE_IMAGEPATH.trim($val->reward_image);
		$i++;
	    }
	    $data['rules_list']=$ruleasarr;
	    
	}else{
	    $data['resultcode']=404;
	    $data['resultmsg']="No Rules Available";
	    
	}
	return $data;
    }
    
    public function edit_rules($jsondata){
	
	$data=array();
	$tabledata1=array();
	if(isset($jsondata->reward_name) && $jsondata->reward_name!=''){
	   $tabledata1=array("reward_name"=>$jsondata->reward_name) ;
	}
	$tabledata=array("updated_on"=>date("Y-m-d H:i:s"),"slab_amt"=>$jsondata->slab_amt);
	$fnldata=array_merge($tabledata,$tabledata1);
	$this->db2->update("foodle_location_loyalty_reward",$fnldata,array("id"=>$jsondata->loc_reward_id));
	
	$data['resultcode']=200;
	$data['resultmsg']="Success";
	return $data;    
    }
    
    public function activate_deactivate_rule($jsondata){
	$data=array();
	if($jsondata->is_deactivate==1){
	    $action=1;
	}else{
	    $action=0;
	}
	$tabledata=array("is_deleted"=>$action);
	$this->db2->update("foodle_location_loyalty_reward",$tabledata,array("id"=>$jsondata->loc_reward_id));
	$data['resultcode']=200;
	$data['resultmsg']="Success";
	return $data;  
    }
    
    public function campaign_create($jsondata){
	//echo "<pre>"; print_R($jsondata); die;
	$data=array();
	$tabledata=array("reward_id"=>$jsondata->reward_id,"reward_name"=>$jsondata->reward_name,"loc_id"=>$jsondata->loc_id,"loc_uid"=>$jsondata->loc_uid,
			    "is_campaign"=>1,"is_deleted"=>0,"added_on"=>date("Y-m-d H:i:s"));
	$this->db2->insert("foodle_location_loyalty_reward",$tabledata);
	    
	$loc_reward_id=$this->db2->insert_id();
	
	//campaignquery
	$campquery=$this->db2->query("select id,slab_amt,campaign_type from foodle_campaign where is_deleted='0'
				     and reward_id='".$loc_reward_id."' and loc_id='".$jsondata->loc_id."' and audience_id='".$jsondata->audience_id."' and campaign_type='".$jsondata->campaign_type."'
				     order by id desc");
	
	
	$userq=$this->db2->query("select wallet_amt from foodle_loc_wallet where loc_id='".$jsondata->loc_id."'");
	    $userdata=$userq->row_array();
	    $currentwalletamt=(int)$userdata['wallet_amt'];
	    $userarr=array();
	    $audience_user=array();
	if($jsondata->notaudience==1){
	    $audience_user=explode(",",$jsondata->userlist);
	    $userarr=$audience_user;
	}else{
	    $audience_user=$this->get_useraudience_list($jsondata->loc_id,$jsondata->audience_id);
	    $userarr=$audience_user['userarr'];
	}
	
	$audamount=count($userarr)*2;
	if($audamount>$currentwalletamt)
	{
	    $data['resulcode']=401;
	    $data['resultmsg']="Insuffecient Balance";
	    $data['audamount']=$audamount;
	    $data['currwallet']=$currentwalletamt;
	    $data['userarr']=$userarr;
	    return $data;
	}
	
	
	
	
	//echo $this->db2->last_query(); die;
	if($campquery->num_rows()>0){
	    $camparr=$campquery->row_array();
	    
	    if($jsondata->slab_amt!=$camparr['slab_amt']){
		
		//$audience_user=explode(",",$jsondata->userlist);
		$tabledata=array("loc_id"=>$jsondata->loc_id,"loc_uid"=>$jsondata->loc_uid,"reward_id"=>$loc_reward_id,"campaign_name"=>$jsondata->campaign_name,"audience_id"=>$jsondata->audience_id,
			     "start_date"=>$jsondata->start_date,"end_date"=>$jsondata->end_date,"redemption_expiryday"=>$jsondata->redemption_expiryday,"slab_amt"=>$jsondata->slab_amt,
			     "added_on"=>date("Y-m-d H:i:s"),"campaign_type"=>$jsondata->campaign_type,"refer_friend"=>$jsondata->refer_friend,"not_audience"=>$jsondata->notaudience,
			     "trend_reward_audience"=>$jsondata->audience_name);
		$this->db2->insert("foodle_campaign",$tabledata);
		 $campaignid=$this->db2->insert_id();
		if($jsondata->notaudience==1){
		
		foreach($audience_user as $val){
			$tabledata=array("user"=>$val,"campaign_id"=>$campaignid,"sent_on"=>date("Y-m-d H:i:s"),"loc_id"=>$jsondata->loc_id);
			$this->db2->insert("foodle_campaign_sent",$tabledata);
		    }
		}else{
		    
		   // $audience_user=$this->get_useraudience_list($jsondata->loc_id,$jsondata->audience_id);
		    if(count($audience_user['userarr'])>0){
			foreach($audience_user['userarr'] as $val){
			    $tabledata=array("user"=>$val,"campaign_id"=>$campaignid,"sent_on"=>date("Y-m-d H:i:s"),"loc_id"=>$jsondata->loc_id);
			    $this->db2->insert("foodle_campaign_sent",$tabledata);
			}
		    }
		    
		}
		$amount_deduct=$audamount;
	    $this->db2->query("update foodle_loc_wallet set wallet_amt=wallet_amt-{$amount_deduct} where loc_id='".$jsondata->loc_id."'");
	    
		$userq=$this->db2->query("select wallet_amt from foodle_loc_wallet where loc_id='".$jsondata->loc_id."'");
		$userdata=$userq->row_array();
		$currentwalletamt=(int)$userdata['wallet_amt'];
		
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		$data['walletamt']=(int)$userdata['wallet_amt'];
		}else{
		    $data['resultcode']=404;
		    $data['resultmsg']="Campaign Already Created";
		    
		}
	    
	}else{
	    if($jsondata->campaign_type==1 && $jsondata->slab_amt==''){
		$data['resultcode']=404;
		$data['resultmsg']="Please Enter Slab Amount";
	    }else{
		$tabledata=array("loc_id"=>$jsondata->loc_id,"loc_uid"=>$jsondata->loc_uid,"reward_id"=>$loc_reward_id,"campaign_name"=>$jsondata->campaign_name,"audience_id"=>$jsondata->audience_id,
			     "start_date"=>$jsondata->start_date,"end_date"=>$jsondata->end_date,"redemption_expiryday"=>$jsondata->redemption_expiryday,"slab_amt"=>$jsondata->slab_amt,
			     "added_on"=>date("Y-m-d H:i:s"),"campaign_type"=>$jsondata->campaign_type,"refer_friend"=>$jsondata->refer_friend,"not_audience"=>$jsondata->notaudience,
			     "trend_reward_audience"=>$jsondata->audience_name);
	    $this->db2->insert("foodle_campaign",$tabledata);
	    $campaignid=$this->db2->insert_id();
	    if($jsondata->notaudience==1){
		//$audience_user=explode(",",$jsondata->userlist);
		foreach($audience_user as $val){
			$tabledata=array("user"=>$val,"campaign_id"=>$campaignid,"sent_on"=>date("Y-m-d H:i:s"),"loc_id"=>$jsondata->loc_id);
			$this->db2->insert("foodle_campaign_sent",$tabledata);
		    }
	    }else{
		
		//$audience_user=$this->get_useraudience_list($jsondata->loc_id,$jsondata->audience_id);
		if(count($audience_user['userarr'])>0){
		    foreach($audience_user['userarr'] as $val){
			$tabledata=array("user"=>$val,"campaign_id"=>$campaignid,"sent_on"=>date("Y-m-d H:i:s"),"loc_id"=>$jsondata->loc_id);
			$this->db2->insert("foodle_campaign_sent",$tabledata);
		    }
		}
		
	    }
	    
	    $amount_deduct=$audamount;
	    $this->db2->query("update foodle_loc_wallet set wallet_amt=wallet_amt-{$amount_deduct} where loc_id='".$jsondata->loc_id."'");
	    
	    $userq=$this->db2->query("select wallet_amt from foodle_loc_wallet where loc_id='".$jsondata->loc_id."'");
	    $userdata=$userq->row_array();
	    $currentwalletamt=(int)$userdata['wallet_amt'];
	    
	    // Push Notification
	    if(count($audience_user['userarr'])>0){
		$currentstamp=strtotime(date("Y-m-d H:i:s"));
		$camapignstart_stamp=strtotime($jsondata->start_date);
		if($currentstamp>=$camapignstart_stamp){
		    $this->send_campaign_msg($audience_user['userarr'],$campaignid);
		}
	    }
	    $data['resultcode']=200;
	    $data['resultmsg']="Success";
	    $data['walletamt']=(int)$userdata['wallet_amt'];
	    
	    }
	    
	    
	}
	return $data;
	// Campaign Sent USER Entry to be done
	
    }
    
    public function campaign_list($jsondata){
	$cond='';
	$data=array();
	if($jsondata->filter_type=="active"){
	   // $cond.='and now() between fc.start_date and fc.end_date and fc.is_deleted=0';
	}else if($jsondata->filter_type=="expired"){
	     $cond.='and (now()>fc.end_date or fc.is_deleted=1)';
	}
	$dayscond='';
	if($jsondata->days!=''){
	    $days=$jsondata->days;
	    $enddate=date("Y-m-d");
	    $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	    $dayscond.="and date(fc.added_on) between '".$startdate."' and '".$enddate."'";
	}
	
	$query=$this->db2->query("SELECT fc.id AS campaignid,fc.`campaign_name`,fla.`audience_name`,fc.`end_date`,fc.start_date,(SELECT COUNT(`user`) FROM `foodle_campaign_sent` fcs WHERE fcs.`campaign_id`=fc.id and fcs.is_referred='0') AS sentcount,
(SELECT COUNT(`id`) FROM `foodle_unlocked_offer` fuo WHERE fuo.`campaign_id`=fc.id AND fuo.is_redeemed='1') AS redeemed FROM `foodle_campaign`  fc
 INNER JOIN `foodle_location_loyalty_reward_audience` fla ON (fc.audience_id=fla.id) where fc.loc_id='".$jsondata->loc_id."' {$cond} {$dayscond} order by fc.id desc");
	//echo $this->db2->last_query(); die;
	if($query->num_rows()>0){
	    $campdata=array();
	    $i=0;
	    foreach($query->result() as $val){
		
		$campdata[$i]['campaignid']=$val->campaignid;
		$campdata[$i]['campaign_name']=$val->campaign_name;
		$campdata[$i]['audience_name']=$val->audience_name;
		$campdata[$i]['sentcount']=$val->sentcount;
		$campdata[$i]['redeemed']=$val->redeemed;
		$campdata[$i]['perc']=($val->redeemed/$val->sentcount)*100;
		$curstamp=strtotime(date("Y-m-d H:i:s"));
		$endstamp=strtotime($val->end_date);
		$startstamp=strtotime($val->start_date);
		if($curstamp>$startstamp && $curstamp<$endstamp)
		{
		    $campdata[$i]['status']="live";
		    $campdata[$i]['is_live']="1"; 
		}else{
		    if($curstamp>$endstamp){
			$campdata[$i]['status']="Expired ".$this->get_time_difference_php($val->end_date);
			$campdata[$i]['is_live']="0";
		    }
		    
		    if($curstamp<$startstamp){
			
			$campdata[$i]['status']="Campaign Starts In ".$this->get_time_difference_php($val->start_date);
			$campdata[$i]['is_live']="2";
		    }
		}
		$i++;
	    }
	    //arsort($campdata);
	    
	    
	    
	}
	
	$query1=$this->db2->query("SELECT fc.id AS campaignid,fc.`campaign_name`,fc.`trend_reward_audience` as audience_name,fc.`end_date`,fc.start_date,(SELECT COUNT(`user`) FROM `foodle_campaign_sent` fcs WHERE fcs.`campaign_id`=fc.id and fcs.is_referred='0') AS sentcount,
(SELECT COUNT(`id`) FROM `foodle_unlocked_offer` fuo WHERE fuo.`campaign_id`=fc.id AND fuo.is_redeemed='1') AS redeemed FROM `foodle_campaign`  fc
  where fc.loc_id='".$jsondata->loc_id."' and not_audience='1' {$cond} {$dayscond} order by fc.id desc");
	if($query1->num_rows()>0){
	   
	    foreach($query1->result() as $val){
		
		$campdata[$i]['campaignid']=$val->campaignid;
		$campdata[$i]['campaign_name']=$val->campaign_name;
		$campdata[$i]['audience_name']=$val->audience_name;
		$campdata[$i]['sentcount']=$val->sentcount;
		$campdata[$i]['redeemed']=$val->redeemed;
		$campdata[$i]['perc']=($val->redeemed/$val->sentcount)*100;
		$curstamp=strtotime(date("Y-m-d H:i:s"));
		$endstamp=strtotime($val->end_date);
		$startstamp=strtotime($val->start_date);
		if($curstamp>$startstamp && $curstamp<$endstamp)
		{
		    $campdata[$i]['status']="live";
		    $campdata[$i]['is_live']="1"; 
		}else{
		    if($curstamp>$endstamp){
			$campdata[$i]['status']=$this->get_time_difference_php($val->end_date) ."ago";
			$campdata[$i]['is_live']="0";
		    }
		    
		    if($curstamp<$startstamp){
			$campdata[$i]['status']=$this->get_time_difference_php($val->start_date);
			$campdata[$i]['is_live']="2";
		    }
		}
		
		
		
		$i++;
	    }
	    //arsort($campdata);
	}
	
	
	if($jsondata->filter_type=="bestperforming"){
	    usort($campdata, function($a, $b) {
		return  $b['perc']-$a['perc'];
	    });
	    }
	    
	    
	    
	   // echo "<pre>"; print_R($campdata); die;
	    if(count($campdata)>0){
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		$data['campaignd']=$campdata;
	    }else{
		$data['resultcode']=404;
		$data['resultmsg']="No Campaigns Yet";
		
	    }
	    
	
	
	return $data;
	
    }
    
  
	public function get_time_difference_php($created_time)
	{
	
	$str = strtotime($created_time);
	$today = strtotime(date('Y-m-d H:i:s')); // It returns the time difference in Seconds...
	$time_differnce = abs($today - $str); // To Calculate the time difference in Years...
	$years = 60 * 60 * 24 * 365; // To Calculate the time difference in Months...
	$months = 60 * 60 * 24 * 30; // To Calculate the time difference in Days...
	$days = 60 * 60 * 24; // To Calculate the time difference in Hours...
	$hours = 60 * 60; // To Calculate the time difference in Minutes...
	$minutes = 60;
	if (intval($time_differnce / $years) > 1)
		{
		return intval($time_differnce / $years) . " years ";
		}
	  else
	if (intval($time_differnce / $years) > 0)
		{
		return intval($time_differnce / $years) . " year ";
		}
	  else
	if (intval($time_differnce / $months) > 1)
		{
		return intval($time_differnce / $months) . " months ";
		}
	  else
	if (intval(($time_differnce / $months)) > 0)
		{
		return intval(($time_differnce / $months)) . " month ";
		}
	  else
	if (intval(($time_differnce / $days)) > 1)
		{
		return intval(($time_differnce / $days)) . " days ";
		}
	  else
	if (intval(($time_differnce / $days)) > 0)
		{
		return intval(($time_differnce / $days)) . " day ";
		}
	  else
	if (intval(($time_differnce / $hours)) > 1)
		{
		return intval(($time_differnce / $hours)) . " hours ";
		}
	  else
	if (intval(($time_differnce / $hours)) > 0)
		{
		return intval(($time_differnce / $hours)) . " hour ";
		}
	  else
	if (intval(($time_differnce / $minutes)) > 1)
		{
		return intval(($time_differnce / $minutes)) . " minutes ";
		}
	  else
	if (intval(($time_differnce / $minutes)) > 0)
		{
		return intval(($time_differnce / $minutes)) . " minute ";
		}
	  else
	if (intval(($time_differnce)) > 1)
		{
		return intval(($time_differnce)) . " seconds ";
		}
	  else
		{
		   
		    return "few seconds ";
		}
	}
	
	
	public function get_audience_list($jsondata){
	  $isp_uid = 0;
	  if(isset($jsondata->isp_uid)){
	       $isp_uid = $jsondata->isp_uid;
	  }
	  $location_uid = $jsondata->loc_uid;
	  $dayscond='';
	if($jsondata->days!=''){
	    $days=$jsondata->days;
	    $enddate=date("Y-m-d");
	    $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	  //  $dayscond.="and date(added_on) between '".$startdate."' and '".$enddate."'";
	}
	  
	  $data = array();
	  $loyalty_reward = array();
	 /* $this->db2->select('*')->from('foodle_location_loyalty_reward_audience');
	  $this->db2->where(array("is_deleted" => "0", 'loc_uid' => $location_uid));
	  $query = $this->db2->get();*/
	 
	 $preaudiencarr=array("HIGH SPENDERS","LOW SPENDERS","REGULARS","ONE TIMERS","ADDICTED");
	 $audiencename=$this->db2->query("select audience_name from foodle_location_loyalty_reward_audience where is_deleted='0' and loc_id='".$jsondata->loc_id."'");
	 $existaudarr=array();
	 if($audiencename->num_rows()>0){
	    foreach($audiencename->result() as $val){
		$existaudarr[]=trim($val->audience_name);
	    }
	 }
	 foreach($preaudiencarr as $val){
	    $audience_name=$val;
	    $audiance_duration="";
	    $visit_from_condition=0;
	    $visit_from_value=0;
	    $visit_to_conditon=0;
	    $visit_to_value=0;
	    $visit_spend_between_condition=1;
	    $spend_from_condition=0;
	    $spend_from_value=0;
	    $spend_to_condition=0;
	    $spend_to_value=0;
	    if($val=="HIGH SPENDERS"){
		$audiance_duration=0;
		$spend_from_condition=1;
		$spend_from_value=10000;
		
	    }else if($val=="LOW SPENDERS"){
		$audiance_duration=0;
		$spend_from_condition=2;
		$spend_from_value=2000;
	    }else if($val=="REGULARS"){
		$audiance_duration=0;
		$visit_from_condition=1;
		$visit_from_value=5;
	    }
	    else if($val=="ONE TIMERS"){
		$audiance_duration=0;
		$visit_from_condition=2;
		$visit_from_value=1;
		$visit_to_conditon=1;
		$visit_to_value=1;
	    }
	     else if($val=="ADDICTED"){
		$audiance_duration=0;
		$visit_from_condition=1;
		$visit_from_value=10;
	    }
	   
	    if(!in_array($val,$existaudarr)){
		$insert_data = array(
		    'audience_name' => $val,
		    'audiance_duration' => $audiance_duration,
		    'visit_from_condition' => $visit_from_condition,
		    'visit_from_value' => $visit_from_value,
		    'visit_to_conditon' => $visit_to_conditon,
		    'visit_to_value' => $visit_to_value,
		    'visit_spend_between_condition' => $visit_spend_between_condition,
		    'spend_from_condition' => $spend_from_condition,
		    'spend_from_value' => $spend_from_value,
		    'spend_to_condition' => $spend_to_condition,
		    'spend_to_value' => $spend_to_value,
		    'loc_id' => $jsondata->loc_id,
		    'loc_uid' => $jsondata->loc_uid,
		    'is_deleted' => '0',
		    'added_on' => date('Y-m-d H:i:s')
	       );
	       $this->db2->insert('foodle_location_loyalty_reward_audience', $insert_data);
	       
	    }
	    
	    
	 }
	 
	 
	 $query=$this->db2->query("select * from foodle_location_loyalty_reward_audience where is_deleted='0'
						 and loc_uid='".$location_uid."' {$dayscond}");
	//  echo $this->db2->last_query(); die;
	  if($query->num_rows() > 0){
	       $data['resultcode'] = '200';
	       $data['resultmsg'] = 'Success';
	       $i = 0;
	       foreach($query->result() as $row){
		    $loyalty_reward[$i]['id'] = $row->id;
		    $loyalty_reward[$i]['audience_name'] = $row->audience_name;
		    $loyalty_reward[$i]['audiance_duration'] = $row->audiance_duration;
		    $loyalty_reward[$i]['visit_from_condition'] = $row->visit_from_condition;
		    $loyalty_reward[$i]['visit_from_value'] = $row->visit_from_value;
		    $loyalty_reward[$i]['visit_to_conditon'] = $row->visit_to_conditon;
		    $loyalty_reward[$i]['visit_to_value'] = $row->visit_to_value;
		    $loyalty_reward[$i]['visit_spend_between_condition'] = $row->visit_spend_between_condition;
		    $loyalty_reward[$i]['spend_from_condition'] = $row->spend_from_condition;
		    $loyalty_reward[$i]['spend_from_value'] = $row->spend_from_value;
		    $loyalty_reward[$i]['spend_to_condition'] = $row->spend_to_condition;
		    $loyalty_reward[$i]['spend_to_value'] = $row->spend_to_value;
		    
		    //get number of user falls in  audience
		    $userdata=$this->get_useraudience_list($jsondata->loc_id,$row->id);
		    $loyalty_reward[$i]['lifetime_user'] = $userdata['total_user'];
		    $loyalty_reward[$i]['user_added'] = 0;
		    $rules = '';
		    $timerule='';
		    $visit_rule = '';
		    $spend_rule = '';
		    if($row->visit_from_condition > 0){
			// $visit_rule .= "Visits";
			 if($row->visit_from_condition == '1'){
			      $visit_rule .= " > ".$row->visit_from_value;
			 }
			 else if($row->visit_from_condition == '2'){
			      $visit_rule .= " < ".$row->visit_from_value;
			 }
			 
			 if($row->visit_to_conditon > 0){
			      if($row->visit_to_conditon == '1'){
				   $visit_rule .= " & > ".$row->visit_to_value;
			      }
			      else if($row->visit_to_conditon == '2'){
				   $visit_rule .= " & < ".$row->visit_to_value;
			      }
			 }
		    }
		    if($row->spend_from_condition > 0){
			// $spend_rule .= "Spends";
			 if($row->spend_from_condition == '1'){
			      $spend_rule .= " > ".$row->spend_from_value;
			 }
			 else if($row->spend_from_condition == '2'){
			      $spend_rule .= " < ".$row->spend_from_value;
			 }
			 
			 if($row->spend_to_condition > 0){
			      if($row->spend_to_condition == '1'){
				   $spend_rule .= " & > ".$row->spend_to_value;
			      }
			      else if($row->spend_to_condition == '2'){
				   $spend_rule .= " & < ".$row->spend_to_value;
			      }
			 }
		    }
		    $spend_visit_rule = '';
		    if($visit_rule != '' && $spend_rule != ''){
			 $between_conditon = '';
			 if($row->visit_spend_between_condition == 1){
			      $between_conditon = ' AND ';
			 }
			 else{
			      $between_conditon = ' OR ';
			 }
			 $spend_visit_rule = $visit_rule.$between_conditon.$spend_rule;
		    }
		    else if($visit_rule != ''){
			 $spend_visit_rule = $visit_rule;
		    }
		    else if($spend_rule != ''){
			 $spend_visit_rule = $spend_rule;
		    }
		    if($row->audiance_duration == 0){
			 $timerule .= 'LT ';
		    }
		    else if($row->audiance_duration == 1){
			 $timerule .= '1M';
		    }
		    else{
			$timerule .= $row->audiance_duration.'M'; 
		    }
		    
		    
		    
		    $loyalty_reward[$i]['rules'] = $rules;
		    $loyalty_reward[$i]['time_rule'] = $timerule;
		    $loyalty_reward[$i]['visit_rule'] = $visit_rule;
		     $loyalty_reward[$i]['spend_rule'] = $spend_rule;
		    $i++;
	       }
	       $data['loyalty_reward']= $loyalty_reward;
	  }
	  else
	  {
            $data['resultcode'] = '404';
	    $data['resultmsg'] = 'No Audience';
	  }
	  return $data;
     }
     
     
     public function get_useraudience_list($loc_id,$audienceid){
	
	$audq=$this->db2->query("select * from foodle_location_loyalty_reward_audience where id='".$audienceid."'");
	$audresult=$audq->row_array();
	$userq=$this->db2->query("SELECT SUM(spend_amt) AS spend_amt,SUM(visit) AS visits,`user` FROM `foodle_user_spend_visit_earn` WHERE loc_id='".$loc_id."' GROUP BY `user`");
	
	if($userq->num_rows()>0){
	    $userarr=array();
	    foreach($userq->result() as $user){
		$spuseravailable=0;
		$vstuseravailable=0;
		//visits Condition 
		$spend_rule='';
		$visit_rule='';
		//echo $user->visits;die;
		if($audresult['visit_from_condition'] > 0){
			// $visit_rule .= "Visits";
			 if($audresult['visit_from_condition'] == '1'){
			      $visit_rule .= " >= ".$audresult['visit_from_value'];
			      
			      if($user->visits>=$audresult['visit_from_value'])
			      {
				$vstuseravailable=1;
			      }else{
				$vstuseravailable=0;
			      }
			 }
			 else if($audresult['visit_from_condition'] == '2'){
			      $visit_rule .= " <= ".$audresult['visit_from_value'];
			      if($user->visits<=$audresult['visit_from_value'])
			      {
				$vstuseravailable=1;
			      }else{
				$vstuseravailable=0;
			      }
			 }
			

			
			 if($audresult['visit_to_conditon'] > 0){
			      if($audresult['visit_to_conditon'] == '1'){
				   $visit_rule .= " & >= ".$audresult['visit_to_value'];
				    if($audresult['visit_from_condition'] == '2'){
				    if($user->visits <=$audresult['visit_from_value'] && $user->visits>=$audresult['visit_to_value'])
				     {
				       $vstuseravailable=1;
				     }else{
				       $vstuseravailable=0;
				     }
				   }
				   
			      }
			      else if($audresult['visit_to_conditon'] == '2'){
				   $visit_rule .= " & <= ".$audresult['visit_to_value'];
				   if($audresult['visit_from_condition'] == '1'){
				    if($user->visits >=$audresult['visit_from_value'] && $user->visits<=$audresult['visit_to_value'])
				     {
				       $vstuseravailable=1;
				     }else{
				       $vstuseravailable=0;
				     }
				   }
				    
			      }
			 }
		    }
		    
		    
		    //spent users
		    
		    if($audresult['spend_from_condition'] > 0){
			// $visit_rule .= "Visits";
			 if($audresult['spend_from_condition'] == '1'){
			       $spend_rule .= " >= ".$audresult['spend_from_value'];
			      if($user->spend_amt>=$audresult['spend_from_value'])
			      {
				$spuseravailable=1;
			      }else{
				$spuseravailable=0;
			      }
			 }
			 else if($audresult['spend_from_condition'] == '2'){
			      $spend_rule .= " <= ".$audresult['spend_from_value'];
			      if($user->spend_amt<=$audresult['spend_from_value'])
			      {
				$spuseravailable=1;
			      }else{
				$spuseravailable=0;
			      }
			 }
			 
			 if($audresult['spend_to_condition'] > 0){
			      if($audresult['spend_to_condition'] == '1'){
				   $spend_rule .= " >= ".$audresult['spend_to_value'];
				   if($audresult['spend_from_condition'] == '2'){
				    if($user->spend_amt <=$audresult['spend_from_value'] && $user->spend_amt>=$audresult['spend_to_value'])
				     {
				       $vstuseravailable=1;
				     }else{
				       $vstuseravailable=0;
				     }
				   }
				   
			      }
			      else if($audresult['spend_to_condition'] == '2'){
				  $spend_rule .= " <= ".$audresult['spend_to_value'];
				    if($audresult['spend_from_condition'] == '1'){
				    if($user->spend_amt >=$audresult['spend_from_value'] && $user->spend_amt<=$audresult['spend_to_value'])
				     {
				       $vstuseravailable=1;
				     }else{
				       $vstuseravailable=0;
				     }
				   }
			      }
			 }
		    }
		    
		    
		    $useravailable=0;
		    
		    $spend_visit_rule = '';
		    if($visit_rule != '' && $spend_rule != ''){
			 $between_conditon = '';
			 if($audresult['visit_spend_between_condition'] == 1){
			      $between_conditon = ' AND ';
			      if($spuseravailable==1 && $vstuseravailable==1){
				$useravailable=1;
			      }else{
				$useravailable=0;
			      }
			 }
			 else{
				if($spuseravailable==1 || $vstuseravailable==1){
				$useravailable=1;
			      }else{
				$useravailable=0;
			      }
			 }
			 $spend_visit_rule = $visit_rule.$between_conditon.$spend_rule;
		    }
		    else if($visit_rule != ''){
			 $spend_visit_rule = $visit_rule;
			    if($vstuseravailable==1){
				$useravailable=1;
			      }else{
				$useravailable=0;
			      }
		    }
		    else if($spend_rule != ''){
			 $spend_visit_rule = $spend_rule;
			 if($spuseravailable==1){
				$useravailable=1;
			      }else{
				$useravailable=0;
			      }
		    }
		    
		    
		    if($useravailable==1){
			$userarr[]=$user->user;
		    }
		    
		    
		    
	    
	    }
	    $data['total_user']=count($userarr);
	    $data['userarr']=$userarr;
	    
	}else{
	    $data['total_user']=0;
	    $data['last7_days']=0;
	}
	//echo "<pre>"; print_R($data); die;
	return $data;
     }
     
     
     public function user_audience_condition($username,$audienceid)
     {
	
     }
     
     
     public function foodle_add_loyalty_audience($jsondata){
	  $data = array();
	  
	  $location_uid = $jsondata->loc_uid;
	  $locationid = $jsondata->loc_id;
	  $foodle_audience_name = $jsondata->foodle_audience_name;
	  $foodle_audience_time_duration = $jsondata->foodle_audience_time_duration;
	  
	  $foodle_audience_visit_from_conditon = $jsondata->foodle_audience_visit_from_conditon;
	  $foodle_audience_visit_from_value = $jsondata->foodle_audience_visit_from_value;
	  $foodle_audience_visit_to_conditon = $jsondata->foodle_audience_visit_to_conditon;
	  $foodle_audience_visit_to_value = $jsondata->foodle_audience_visit_to_value;
	  $foodle_audience_spend_from_conditon = $jsondata->foodle_audience_spend_from_conditon;
	  $foodle_audience_spend_from_value = $jsondata->foodle_audience_spend_from_value;
	  $foodle_audience_spend_to_conditon = $jsondata->foodle_audience_spend_to_conditon;
	  $foodle_audience_spend_to_value = $jsondata->foodle_audience_spend_to_value;
	  $foodle_audience_visit_spend_conditon = $jsondata->foodle_audience_visit_spend_conditon;
	  
	  $audience_id = $jsondata->audience_id;
	  if($audience_id > 0){
	       $data['resultcode'] = 200;
	       $data['resultmsg'] = 'Success';
	       $location_update_data = array(
		    'audience_name' => $foodle_audience_name,
		    'audiance_duration' => $foodle_audience_time_duration,
		    'visit_from_condition' => $foodle_audience_visit_from_conditon,
		    'visit_from_value' => $foodle_audience_visit_from_value,
		    'visit_to_conditon' => $foodle_audience_visit_to_conditon,
		    'visit_to_value' => $foodle_audience_visit_to_value,
		    'visit_spend_between_condition' => $foodle_audience_visit_spend_conditon,
		    'spend_from_condition' => $foodle_audience_spend_from_conditon,
		    'spend_from_value' => $foodle_audience_spend_from_value,
		    'spend_to_condition' => $foodle_audience_spend_to_conditon,
		    'spend_to_value' => $foodle_audience_spend_to_value,
		    'loc_id' => $locationid,
		    'loc_uid' => $location_uid,
		    'is_deleted' => '0',
		    'added_on' => date('Y-m-d H:i:s')
	       );
	       $this->db2->where('id', $audience_id);
	       $this->db2->update('foodle_location_loyalty_reward_audience', $location_update_data);
	  }
	  else{
	       $data['resultcode'] = 200;
	       $data['resultmsg'] = 'Success';
	       $insert_data = array(
		    'audience_name' => $foodle_audience_name,
		    'audiance_duration' => $foodle_audience_time_duration,
		    'visit_from_condition' => $foodle_audience_visit_from_conditon,
		    'visit_from_value' => $foodle_audience_visit_from_value,
		    'visit_to_conditon' => $foodle_audience_visit_to_conditon,
		    'visit_to_value' => $foodle_audience_visit_to_value,
		    'visit_spend_between_condition' => $foodle_audience_visit_spend_conditon,
		    'spend_from_condition' => $foodle_audience_spend_from_conditon,
		    'spend_from_value' => $foodle_audience_spend_from_value,
		    'spend_to_condition' => $foodle_audience_spend_to_conditon,
		    'spend_to_value' => $foodle_audience_spend_to_value,
		    'loc_id' => $locationid,
		    'loc_uid' => $location_uid,
		    'is_deleted' => '0',
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->db2->insert('foodle_location_loyalty_reward_audience', $insert_data);
	       $slab_id = $this->db2->insert_id();
	  }
	  
	 return $data;
     }
     
     
     public function foodle_campaign_delete($jsondata){
	  $foodle_campaign_id = $jsondata->foodle_campaign_id;
	  $data = array();
	  $location_update_data = array(
		    'is_deleted' => 1,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->db2->where('id', $foodle_campaign_id);
	       $this->db2->update('foodle_campaign', $location_update_data);
		$data['resultcode'] = '200';
		$data['resultmsg'] = "Success";
	     
	  return $data;
     }
     
     
     public function delete_audience($jsondata){
	  $audience_id = $jsondata->audience_id;
	  $data = array();
	  $location_update_data = array(
		    'is_deleted' => 1,
		    'updated_on' => date('Y-m-d H:i:s')
	       );
	       $this->db2->where('id', $audience_id);
	       $this->db2->update('foodle_location_loyalty_reward_audience', $location_update_data);
	 $data['resultcode'] = '200';
		$data['resultmsg'] = "Success";
	     
	  return $data;
     }
     
    public function foodle_registration($jsondata){
	
	$locdata=$this->location_autogenerate_id($jsondata);
	//echo "======>>".$location_uid; die;
	$location_uid=$locdata['location_id'];
	$tabledata=array(
		    'enable_nas_down_noti' => 0,
		    'enable_channel' => 0,
		    'location_uid' => $location_uid,
		    'location_type' => 21,
		    'location_name' => $jsondata->location_name,
		    'geo_address' => $jsondata->geo_address,
		    'placeid' => $jsondata->placeid,
		    'latitude' => $jsondata->latitude,
		    'longitude' => $jsondata->longitude,
		    'region_id' => 0,
		    'address_1' => '',
		    'address_2' => '',
		    'state' => $jsondata->state,
		    'state_id' => 0,
		    'city' => $jsondata->city,
		    'city_id' => 0,
		    'pin' => $jsondata->postal_code,
		    'contact_person_name' => $jsondata->first_name,
		    'mobile_number' => $jsondata->mobile,
		    'status' => '1',
		    'is_deleted' => '0',
		    'created_on' => date('Y-m-d H:i:s'),
		    'isp_uid' => $jsondata->isp_uid,
		    'user_email' => '',
		    'location_brand' => '',
		    'provider_id' => $jsondata->isp_uid,
		    'hashtags' => '',
		    'zone_name' => '',
		    'country_name' => '',
		    'foodmenu_options_list' => '',
		    'enable_payrolldebit' => '',
		    'return_policy_number' => '',
		    'location_display_name' => ''
	       );
	$password= $this->encrypt->encode($jsondata->password);
	$decodepwd = $this->encrypt->decode($password);
	$checkq=$this->db2->query("select id,password,loc_id from foodle_registration where mobile='".$jsondata->mobile."' and location_name='".$jsondata->location_name."' and is_deleted='0'");
	//echo $this->db2->last_query(); die;
	
	if($checkq->num_rows()>0){
	    $rowdata=$checkq->row_array();
	    $locid=$rowdata['loc_id'];
	    $decodepwd = $this->encrypt->decode($rowdata['password']);
	    if($decodepwd!=$jsondata->password)
	    {
		$checkno=(isset($jsondata->checque_no) && $jsondata->checque_no!='')?$jsondata->checque_no:"";
		$payby=(isset($jsondata->checque_no) && $jsondata->checque_no!='')?1:0;
		$status=(isset($jsondata->checque_no) && $jsondata->checque_no!='')?0:1;
		$this->db2->insert("wifi_location",$tabledata);
		$locid=$this->db2->insert_id();
		$tabledata1=array("uid_location"=>$location_uid,"isp_uid"=>$jsondata->isp_uid,"location_id"=>$locid,"cp_type"=>"cp_foodle","created_on"=>date("Y-m-d H:i:s"));
		$this->db2->insert("wifi_location_cptype",$tabledata1);
		
		$tabledata3=array("location_name"=>$jsondata->location_name,"first_name"=>$jsondata->first_name,
				 "last_name"=>$jsondata->last_name,"mobile"=>$jsondata->mobile,
				 "password"=>$password,"geo_address"=>$jsondata->geo_address,"placeid"=>$jsondata->placeid,
				 "latitude"=>$jsondata->latitude,"longitude"=>$jsondata->longitude,"location_type"=>$jsondata->location_type,
				 "loc_id"=>$locid,"loc_uid"=>$location_uid,"wifi_ssid"=>$jsondata->wifi_ssid,"created_on"=>date("Y-m-d H:i:s"),
				 "register_paytmno"=>$jsondata->register_paytmno,"status"=>$status,"cheque_no"=>$checkno,"paymode"=>$payby,
				 "state"=>$jsondata->state,"city"=>$jsondata->city,"postal_code"=>$jsondata->postal_code,"signup_ip"=>$jsondata->clientip,"email"=>$jsondata->email);
		$this->db2->insert("foodle_registration",$tabledata3);
		if($jsondata->email!=''){
		   $email=$jsondata->email;
		   $message="test";
		   $sub="test subject";
		   // $this->send_via_vcemail($email,$message,$sub); 
		}
		
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		$data['loc_id']=$locid;
		$data['loc_uid']=$location_uid;
	    }else{
		$data['resultcode']=200;
		$data['resultmsg']="Success";
		$data['loc_id']=$locid;
		$data['loc_uid']=$location_uid;
	    }
	}else{
	
	$this->db2->insert("wifi_location",$tabledata);
	$locid=$this->db2->insert_id();
	$checkno=(isset($jsondata->checque_no) && $jsondata->checque_no!='')?$jsondata->checque_no:"";
		$payby=(isset($jsondata->checque_no) && $jsondata->checque_no!='')?1:0;
		$status=(isset($jsondata->checque_no) && $jsondata->checque_no!='')?0:1;
	$tabledata1=array("uid_location"=>$location_uid,"isp_uid"=>$jsondata->isp_uid,"location_id"=>$locid,"cp_type"=>"cp_foodle","created_on"=>date("Y-m-d H:i:s"));
	$this->db2->insert("wifi_location_cptype",$tabledata1);
	$tabledata3=array("location_name"=>$jsondata->location_name,"first_name"=>$jsondata->first_name,
			 "last_name"=>$jsondata->last_name,"mobile"=>$jsondata->mobile,
			 "password"=>$password,"geo_address"=>$jsondata->geo_address,"placeid"=>$jsondata->placeid,
			 "latitude"=>$jsondata->latitude,"longitude"=>$jsondata->longitude,"location_type"=>$jsondata->location_type,
			 "loc_id"=>$locid,"loc_uid"=>$location_uid,"wifi_ssid"=>$jsondata->wifi_ssid,"created_on"=>date("Y-m-d H:i:s")
			 ,"register_paytmno"=>$jsondata->register_paytmno,"status"=>$status,"cheque_no"=>$checkno,"paymode"=>$payby,
			  "state"=>$jsondata->state,"city"=>$jsondata->city,"postal_code"=>$jsondata->postal_code,"signup_ip"=>$jsondata->clientip,"email"=>$jsondata->email);
	$this->db2->insert("foodle_registration",$tabledata3);
	if($jsondata->email!=''){
		   $email=$jsondata->email;
		   $message="test";
		   $sub="test subject";
		  //  $this->send_via_vcemail($email,$message,$sub); 
	    }
	$data['resultcode']=200;
	$data['resultmsg']="Success";
	$data['loc_id']=$locid;
	$data['loc_uid']=$location_uid;
	}
	return $data;
	//echo $this->db2->last_query(); die;
    }
     
     public function location_autogenerate_id($jsondata){
	  $data = array();
	  $isp_uid = $jsondata->isp_uid;
	  //get last locationid
	  $id = 0;
	  $this->db2->select('id')->from('wifi_location')->where('isp_uid', $isp_uid)->order_by('id', 'DESC')->limit('1');
	  $query = $this->db2->get();
	  if($query->num_rows() > 0){
	      $row = $query->row_array();
	      $id = $row['id'];
	  }
	  $id = $id+1;
	  // id will be next id
	  // check last next id assign from temp table
	  $next_id_assigned = 0;
	  $this->db2->select('location_id')->from('wifi_locationid_temp')->where('isp_uid', $isp_uid)->order_by('id', 'DESC')->limit('1');
	  $query1 = $this->db2->get();
	  if($query1->num_rows() > 0){
	      $row1 = $query1->row_array();
	      $next_id_assigned = $row1['location_id'];
	  }
	  // which will be next id
	  $next_id = '';
	  if($next_id_assigned >= $id ){
	      $next_id = $next_id_assigned+1;
	  }else{
	      $next_id = $id;
	  }
        // insert in temp table
	  $insert_data = array(
	       'location_id' => $next_id,
	       'isp_uid' => $isp_uid,
	       'created_on' => date('Y-m-d H:i:s')
	  );
	  $this->db2->insert('wifi_locationid_temp', $insert_data);
	  $data['location_id'] = $isp_uid.$next_id;
	 
	  return $data;
     }
     
     public function password_availability($jsondata){
	$query1=$this->db2->query("select id,password from foodle_registration where mobile='".$jsondata->mobile."' and is_deleted='0'");
	if($query1->num_rows()>0){
	    foreach($query1->result() as $val){
	    $rowdata=$query1->row_array();
	    $decodepwd = $this->encrypt->decode($val->password);
	    if($decodepwd!=$jsondata->password)
	    {
		$data['resultcode']=200;
		$data['resultmsg']="Password Available";
	    }else{
		$data['resultcode']=404;
		$data['resultmsg']="Password Not Available";
		return $data;
	    }
	    }
	}else{
	    $data['resultcode']=200;
	    $data['resultmsg']="Password Available";
	}
	return $data;
     }
     
     public function retailer_login($jsondata){
	
	$query1=$this->db2->query("select id,password,loc_id,loc_uid from foodle_registration where mobile='".$jsondata->mobile."'  and is_deleted='0' and status='1'");
	if($query1->num_rows()>0){
	    
	    foreach($query1->result() as $val){
	    
	    $decodepwd = $this->encrypt->decode($val->password);
	    
		if($decodepwd!=$jsondata->password)
		{
		    $data['resultcode']=404;
		    $data['resultmsg']="Password not correct";
		}else{
		    $data['resultcode']=200;
		    $data['resultmsg']="Success";
		    $data['loc_id']=$val->loc_id;
		    $data['loc_uid']=$val->loc_uid;
		    return $data;
		}
	    }
	}else{
	    $data['resultcode']=404;
	    $data['resultmsg']="Password not correct";
	}
	return $data;
     }
     
     public function get_wallet_balance(){
	
     }
     
     public function top_spenders($jsondata){
	$data=array();
	$dayscond='';
	if($jsondata->filter_type=="month"){
	    $days=30;
	    $days=$jsondata->days;
	    $enddate=date("Y-m-d");
	    $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	    $dayscond.="and date(fus.added_on) between '".$startdate."' and '".$enddate."'";
	}
	$spendq=$this->db2->query("SELECT SUM(fus.`spend_amt`) AS amount,fus.`user`,fu.`firstname`,fu.`lastname` FROM `foodle_user_spend_visit_earn` fus
INNER JOIN `foodle_user` fu ON (fu.user=fus.user) WHERE fus.loc_id='".$jsondata->loc_id."'
 GROUP BY USER ORDER BY amount DESC");
	if($spendq->num_rows()>0){
	    $data['resultcode']=200;
	    $data['resultmsg']="Success";
	    $spenderdata=array();
	    $i=0;
	    foreach($spendq->result() as $val){
		$spenderdata[$i]['username']=$val->firstname." ".$val->lastname;
		$spenderdata[$i]['amount']=number_format($val->amount);
		$i++;
	    }
	    $data['spenderdata']=$spenderdata;
	}else{
	    $data['resultcode']=404;
	    $data['resultmsg']="No Spender yet";
	}
	return $data;
     }
     
     public function top_redeemer($jsondata){
	$data=array();
	$dayscond='';
	if($jsondata->filter_type=="month"){
	    $days=30;
	   
	    $enddate=date("Y-m-d");
	    $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	    $dayscond.="and date(fuo.redeemed_on) between '".$startdate."' and '".$enddate."'";
	}else if($jsondata->filter_type=="today"){
	     $days=1;
	    
	    $enddate=date("Y-m-d");
	    $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	    $dayscond.="and date(fuo.redeemed_on) between '".$startdate."' and '".$enddate."'";
	}
	
	
	$redeemq=$this->db2->query("SELECT COUNT(fuo.`user`) AS redeemcount,fuo.`user`,fu.`firstname`,fu.`lastname`,fuw.amount AS spend_bal,
(SELECT fllr.reward_name FROM `foodle_user_spends` fus
 INNER JOIN foodle_location_loyalty_reward fllr ON (fus.reward_id=fllr.id AND fus.loc_id='".$jsondata->loc_id."') WHERE fus.user=fuo.user ORDER BY fus.id DESC LIMIT 1) AS reward_name,
 (SELECT fus1.`spent_amt` FROM foodle_user_spends fus1 WHERE fus1.user=fuo.user AND fus1.loc_id='".$jsondata->loc_id."' ORDER BY fus1.id DESC LIMIT 1) AS ltspend,
 (SELECT fus2.`added_on` FROM foodle_user_spends fus2 WHERE fus2.user=fuo.user AND fus2.loc_id='".$jsondata->loc_id."' ORDER BY fus2.id DESC LIMIT 1) AS lastredeem
 FROM `foodle_unlocked_offer` fuo INNER JOIN 
 `foodle_user` fu ON (fu.user=fuo.user) 
INNER JOIN `foodle_user_wallet` fuw ON (fuo.user=fuw.user AND fuw.loc_id='".$jsondata->loc_id."') WHERE fuo.is_redeemed='1' AND fuo.loc_id='".$jsondata->loc_id."' {$dayscond} GROUP BY USER ORDER BY redeemcount DESC
 ");
	//echo $this->db2->last_query(); die;
	if($redeemq->num_rows()>0){
	    $data['resultcode']=200;
	    $data['resultmsg']="Success";
	    $redeemdata=array();
	    $userarr=array();
	    $i=0;
	    foreach($redeemq->result() as $vald){
		$redeemdata[$i]['redeemcount']=$vald->redeemcount;
		$redeemdata[$i]['username']=$vald->firstname." ".$vald->lastname;
		$redeemdata[$i]['spend_bal']=number_format($vald->spend_bal);
		$redeemdata[$i]['reward_name']=$vald->reward_name;
		$redeemdata[$i]['ltspend']=number_format($vald->ltspend);
		$redeemdata[$i]['user']=$vald->user;
		$redeemdata[$i]['lastredeem']=$this->get_time_difference_php($vald->lastredeem);
		$userarr[]=$vald->user;
		$i++;
	    }
	    $data['redeemdata']=$redeemdata;
	    $data['userlist']=implode(",",$userarr);
	    $data['usercount']=count($userarr);
	}else{
	    $data['resultcode']=404;
	    $data['resultmsg']="No Redeems yet";
	}
	return $data;
	
     }
     
     public function spend_wifi_data($jsondata){
	    $days=$jsondata->days;
	    $data=array();
	    if($days==7){
		$data['sincetext']="Since last week";
	    }else if($days==15){
		$data['sincetext']="Since last 15 days";
	    }else if($days==30){
		$data['sincetext']="Since last 30 days";
	    }else if($days==90){
		$data['sincetext']="Since last 90 days";
	    }
	  $enddate=date("Y-m-d");
	  $startdate=date('Y-m-d', strtotime('-'.$days.' days'));
	  $prevenddate = date('Y-m-d', strtotime('-1 days', strtotime($startdate)));
	  $prevstartdate = date('Y-m-d', strtotime('-'.$days.' days', strtotime($prevenddate)));
	  
	  $monthstart=date('Y-m-d', strtotime('-30 days'));
	  
	  $currspenddata=$this->db2->query("SELECT SUM(`spend_amt`) AS spent_amt ,`user`,DATE(`added_on`) as added_on FROM `foodle_user_spend_visit_earn`
WHERE   date(added_on) between '".$startdate."' and '".$enddate."' and loc_id='".$jsondata->loc_id."' GROUP BY USER,DATE(`added_on`) order by DATE(`added_on`) asc ");
	  if($currspenddata->num_rows()>0){
	    $curspendarr=array();
	    $spend=0;
	    $user=0;
	    foreach($currspenddata->result() as $spval){
		
		$curspendarr['spend'][$spval->added_on]=(!isset($curspendarr['spend'][$spval->added_on]))?$spval->spent_amt:$curspendarr['spend'][$spval->added_on]+$spval->spent_amt;
		$spend=$spend+$spval->spent_amt;
		$user=$user+1;
		
		//$curspendarr[]
	    }
	   // echo "<pre>"; print_R($curspendarr); die;
	    $curspendarr['average_spend']=number_format(round($spend/$days));
	    $curspendarr['average_user']=number_format(round($user));
	    $curspendarr['total_spend']=number_format($spend);
	    
	    
	    
	  }else{
	    $curspendarr['spend']=array();
	    $curspendarr['average_spend']=0;
	    $curspendarr['average_user']=0;
	    $curspendarr['total_spend']=0;
	  }
	  
	  $prevspendq=$this->db2->query("select sum(`spend_amt`) as spent_amt FROM `foodle_user_spend_visit_earn` where loc_id='".$jsondata->loc_id."' and date(added_on) between '".$prevstartdate."' and '".$prevenddate."'");
	  if($prevspendq->num_rows()>0){
	    $prevspend=$prevspendq->row_array();
	    $prevspendamt=$prevspend['spent_amt'];
	  }else{
	    $prevspendamt=0;
	  }
	  
	  $percarr=$this->count_percent_increment($curspendarr['total_spend'],$prevspendamt);
	  $curspendarr['spendpercwith_prev']=$percarr['perc'];
	  $curspendarr['is_negative']=$percarr['is_negative'];
	  
	  //reward q
	  $redeemq=$this->db2->query("SELECT COUNT(id) AS redemptioncount,SUM(`reward_amt`) AS total_reward,
(SELECT COUNT(fuo.id) FROM `foodle_unlocked_offer` fuo where
fuo.`is_redeemed`='1' AND DATE(fuo.`redeemed_on`) between '".$monthstart."' and '".$startdate."') AS monthly_redeem FROM `foodle_unlocked_offer` 
WHERE `is_redeemed`='1' and loc_id='".$jsondata->loc_id."' AND DATE(`redeemed_on`) between '".$startdate."' and '".$enddate."'");
	  //echo $this->db2->last_query(); die;
	 $redeemq= $redeemq->row_array();
	 $redeemarr=array();
	 $redeemarr['redemptioncount']=number_format($redeemq['redemptioncount']);
	 $redeemarr['redemptionavg']=number_format(round($redeemq['redemptioncount']/$days));
	 $redeemarr['redemptionamt']=number_format($redeemq['total_reward']);
	 $redeemarr['redemptionmonthly_count']=number_format($redeemq['monthly_redeem']);
	  $topredeemarr=array();
	  $topredeemq=$this->db2->query("SELECT DISTINCT(fuo.reward_id) AS reward_id, COUNT(fuo.reward_id) AS COUNT ,fllr.reward_name
    FROM foodle_unlocked_offer fuo
    INNER JOIN `foodle_location_loyalty_reward` fllr ON (fuo.reward_id=fllr.id) where DATE(`redeemed_on`) between '".$startdate."' and '".$enddate."'
    GROUP BY fuo.`reward_id` ORDER BY COUNT DESC LIMIT 3");
	  if($topredeemq->num_rows()>0){
	    foreach($topredeemq->result() as $val){
		$topredeemarr[]=$val->reward_name;
	    }
	  }
	  $redeemarr['topredemption']=$topredeemarr;
	  
	 /*Wifi Query*/
	 
	 $wifiarr=array();
	  $wifidata=$this->db2->query("SELECT COUNT(`user`) AS sesscount,`user`,DATE(`session_on`)  AS session_on FROM `paytm_wifi_session` 
WHERE plan_type='1' and loc_id='".$jsondata->loc_id."' AND DATE(`session_on`)  between '".$startdate."' and '".$enddate."' 
GROUP BY USER,DATE(`session_on`)");
	 // echo "===>>>".
	  if($wifidata->num_rows()>0){
	    
	    $ssession=0;
	    $user=0;
	    $userarr=array();
	    foreach($wifidata->result() as $spval){
		
		$wifiarr['wifiusage'][$spval->session_on]=(!isset($wifiarr['wifiusage'][$spval->session_on]))?1:$wifiarr['wifiusage'][$spval->session_on]+1;
		$session=$ssession+$spval->sesscount;
		$userarr[$spval->user]=$spval->user;
		
		//$curspendarr[]
	    }
	    $wifiarr['average_session']=number_format(round($session/$days));
	    $wifiarr['average_user']=number_format(round($user/$days));
	    $wifiarr['total_user']=number_format(count($userarr));
	    
	    
	    
	  }else{
	    $wifiarr['wifiusage']=array();
	    $wifiarr['average_session']=0;
	    $wifiarr['average_user']=0;
	    $wifiarr['total_user']=0;
	  }
	  
	  $prevwifiq=$this->db2->query("select count(distinct(`user`)) as user FROM `paytm_wifi_session` where loc_id='".$jsondata->loc_id."' and date(session_on) between '".$prevstartdate."' and '".$prevenddate."'");
	 // echo $this->db2->last_query(); die;
	  if($prevwifiq->num_rows()>0){
	    $prevswifi=$prevwifiq->row_array();
	    $prevswifiuser=$prevswifi['user'];
	  }else{
	    $prevswifiuser=0;
	  }
	//  echo "======>>".$prevswifiuser;
	  
	  $percarr=$this->count_percent_increment($wifiarr['total_user'],$prevswifiuser);
	  $wifiarr['wifipercwith_prev']=$percarr['perc'];
	  $wifiarr['is_negative']=$percarr['is_negative'];
	  
	  //Paid Wifi query
	  $paidwifiq=$this->db2->query("SELECT COUNT(distinct(user)) AS paidwifi_user,SUM(trim(TRAILING '0' FROM `price`)) AS wifi_price,
(SELECT SUM(trim(TRAILING '0' FROM `price`)) FROM `paytm_wifi_session`  where
DATE(session_on) between '".$monthstart."' and '".$startdate."') AS monthly_paid FROM `paytm_wifi_session` 
WHERE  DATE(`session_on`) between '".$startdate."' and '".$enddate."' and loc_id='".$jsondata->loc_id."' ");
	  //echo $this->db2->last_query(); die;
	 $paidwifiqdata= $paidwifiq->row_array();
	 $paidwifi_arr=array();
	  $paidwifi_arr['paidwifi_user']=number_format($paidwifiqdata['paidwifi_user']);
	 $paidwifi_arr['paidwfi_dailyavg']=number_format(round($paidwifiqdata['paidwifi_user']/$days));
	 $paidwifi_arr['paidwifi_value']=number_format($paidwifiqdata['wifi_price']);
	 $paidwifi_arr['paidwifi_monthly_paid']=number_format($paidwifiqdata['monthly_paid']);
	 
	 
	 $data['resultcode']=200;
	  
	  $data['spend_redemption']=$redeemarr;
	  $data['curspendarr']=$curspendarr;
	  $data['paid_wifi']=$paidwifi_arr;
	  $data['wifiarr']=$wifiarr;
	  $wififinarr=array();
	  $i=0;
	  //echo "<pre>"; print_R($wifiarr);die;
	  foreach($wifiarr['wifiusage'] as $key => $val){
	    
	   // echo ""
	    $the_date = strtotime($key);
	    $day = date('j', $the_date);
	    $month = date('n', $the_date);
	    $year = date('Y', $the_date);
	    $ts = mktime(0, 0, 0, $month, $day, $year)*1000;
	    $wififinarr[$i][]=$ts;
	    $wififinarr[$i][]=(int)$val;
	    $i++;
	  }
	// echo "<pre>"; print_R($curspendarr); die;
	  
	  $curspendfinarr=array();
	  $i=0;
	  foreach($curspendarr['spend'] as $key => $val){
	     $the_date = strtotime($key);
	    $day = date('j', $the_date);
	    $month = date('n', $the_date);
	    $year = date('Y', $the_date);
	    $ts = mktime(0, 0, 0, $month, $day, $year)*1000;
	    $curspendfinarr[$i][]=$ts;
	    $curspendfinarr[$i][]=(int)$val;
	    $i++;
	  }
	  
	   $data['spendgraph']=$curspendfinarr;
	   $data['wifigraph']=$wififinarr;
	  
	// echo "<pre>"; print_R($data); die;
	 
	  
	return $data;
	
     }
     
     public function interesting_trends($jsondata){
	$data=array();
	$locid=$jondata->loc_id;
	$wifiloyalty=$this->WiFi_Loyalty($jsondata);
	
	$locationloyalty=$this->location_loyalty($jsondata);

	$spendloyalty=$this->spends_trend($jsondata);
	
	$foodle_user=$this->foodle_user_trend($jsondata);
	
	$visitnearyou=$this->visit_nearyouuser($jsondata);
	$referred_user=$this->referred_user($jsondata);
	$data['resultcode']=200;
	$data['wifiloyalty']=$wifiloyalty;
	$data['locationloyalty']=$locationloyalty;
	$data['spendloyalty']=$spendloyalty;
	$data['foodle_user']=$foodle_user;
	$data['visitnearyou']=$visitnearyou;
	$data['referred_user']=$referred_user;
	
	return $data;
	
	
     }
     
     public function referred_user($jsondata){
	$data=array();
	$refer1=$this->db2->query("SELECT COUNT(id),`user` FROM `foodle_campaign_sent` WHERE `is_referred`='1'
				  and loc_id='".$jsondata->loc_id."' GROUP BY `user`");
	$referc=$refer1->num_rows();
	$referuarr=array();
	if($refer1->num_rows()>0){
	    foreach($refer1->result() as $val){
		$referuarr[]=$val->user;
	    }
	}
	$referspending20_count=0;
	$referspending50_count=0;
	$referspending20_arr=array();
	$referspending50_arr=array();
	$data['referc']=$referc;
	$data['referuarr']=(count($referuarr)>0)?implode(",",$referuarr):"";
	$data['referspending20_count']=$referspending20_count;
	$data['referspending20_arr']=(count($referspending20_arr)>0)?implode(",",$referspending20_arr):"";
	$data['referspending50_count']=$referspending50_count;
	$data['referspending50_arr']=(count($referspending50_arr)>0)?implode(",",$referspending50_arr):"";
	return $data;
	
     }
     
     public function visit_nearyouuser($jsondata){
	$data=array();
	$enddate=date("Y-m-d");
	$startdate15=date('Y-m-d', strtotime('-15 days'));
	$startdate30=date('Y-m-d', strtotime('-30 days'));
	$visitq=$this->db2->query("select user from foodle_unlocked_offer where date(opened_on)
				  between '".$startdate15."' and '".$enddate."' and loc_id!='".$jsondata->loc_id."' group by user");
	$visit15count=$visitq->num_rows();
	$visit15arr=array();
	if($visitq->num_rows()>0){
	    foreach($visitq->result() as $val){
		$visit15arr[]=$val->user;
	    }
	}
	
	$visit30q=$this->db2->query("select user from foodle_unlocked_offer where loc_id!='".$jsondata->loc_id."' group by user");
	$visit30count=$visit30q->num_rows();
	$visit30arr=array();
	if($visit30q->num_rows()>0){
	    foreach($visit30q->result() as $val){
		$visit30arr[]=$val->user;
	    }
	}
	
	$highspenderc=0;
	$highspenderarray=array();
	$data['visit15count']=$visit15count;
	$data['visit15arr']=(count($visit15arr)>0)?implode(",",$visit15arr):"";
	$data['visit30count']=$visit30count;
	$data['visit30arr']=(count($visit30arr)>0)?implode(",",$visit30arr):"";
	$data['highspenderc']=$highspenderc;
	$data['highspenderarray']=(count($highspenderarray)>0)?implode(",",$highspenderarray):"";
	return $data;
	
	
     }
     
    public function foodle_user_trend($jsondata){
	//signup q
	$data=array();
	$enddate=date("Y-m-d");
	$startdate=date('Y-m-d', strtotime('-30 days'));
	$signupq=$this->db2->query("select id,user from foodle_user where date(added_on) between '".$startdate."' and '".$enddate."'");
	$signupuserc=$signupq->num_rows();
	$signuparr=array();
	if($signupq->num_rows()>0){
	    foreach($signupq->result() as $val){
		$signuparr[]=$val->user;
	    }
	}
	
	
	$redemp1=$this->db2->query("SELECT COUNT(id) AS redemptioncount,`user` FROM `foodle_unlocked_offer`
				   WHERE is_redeemed='1' and loc_id='".$jsondata->loc_id."' GROUP BY `user`");
	
	$redeem5more=$redemp1->num_rows();
	$redeem5morearr=array();
	if($redemp1->num_rows()>0){
	    foreach($redemp1->result() as $val){
		$redeem5morearr[]=$val->user;
	    }
	}
	
	$foodlehigh_spender=0;
	$foodlehigh_spenderarr=array();
	$data['signupuserc']=$signupuserc;
	$data['signuparr']=(count($signuparr)>0)?implode(",",$signuparr):"";
	$data['redeem5more']=$redeem5more;
	$data['redeem5morearr']=(count($redeem5morearr)>0)?implode(",",$redeem5morearr):"";
	$data['foodlehigh_spender']=$foodlehigh_spender;
	$data['foodlehigh_spenderarr']=(count($foodlehigh_spenderarr)>0)?implode(",",$foodlehigh_spenderarr):"";
	
	return $data;
    }
    
    
     
     
     
     
     public function spends_trend($jsondata){
	$data=array();
	$totalspendq=$this->db2->query("SELECT SUM(spend_amt) as spend_amt FROM foodle_user_spend_visit_earn WHERE loc_id='".$jsondata->loc_id."'");
	$totalspendarr=$totalspendq->row_array();
	$totalspend=$totalspendarr['spend_amt'];
	
	$userspendq=$this->db2->query("SELECT SUM(spend_amt) AS spend_amt,`user` 
	FROM `foodle_user_spend_visit_earn` WHERE loc_id='".$jsondata->loc_id."' GROUP BY `user`");
	$totaluser=$userspendq->num_rows();
	$higher25=0;
	$higher25user=array();
	$lower25=0;
	$lower25user=array();
	$lower50=0;
	$lower50user=array();
	$averagespend=$totalspend/$totaluser;
	if($userspendq->num_rows()>0){
	  
	    foreach($userspendq->result() as $val){
		
		$perc=$this->count_percent_increment($val->spend_amt,$averagespend);
		//echo "<pre>"; print_R($perc);// die;
		if($perc[is_negative]==0){
		    if($perc['perc']>25){
			$higher25=$higher25+1;
			$higher25user[]=$val->user;
		    }
		}else{
		    if($perc['perc']<-25 ){
			$lower25=$lower25+1;
			$lower25user[]=$val->user;
		    }else if($perc['perc']<-50){
			$lower50=$lower50+1;
			$lower50user[]=$val->user;
		    }
		}
	    }
	    
	}
	
	$data['higher25']=$higher25;
	$data['higher25user']=(count($higher25user)>0)?implode(",",$higher25user):"";
	$data['lower25']=$lower25;
	$data['lower25user']=(count($lower25user)>0)?implode(",",$lower25user):"";
	$data['lower50']=$lower50;
	$data['lower50user']=(count($lower50user)>0)?implode(",",$lower50user):"";
	return $data;
	
     }
     
     
     public function location_loyalty($jsondata){
	$data=array();
	$totalvisitq=$this->db2->query("SELECT SUM(visit) as totalvisit FROM foodle_user_spend_visit_earn WHERE loc_id='".$jsondata->loc_id."'");
	$totalvisitqarr=$totalvisitq->row_array();
	$totalvisit=$totalvisitqarr['totalvisit'];
	
	$uservisitq=$this->db2->query("SELECT SUM(visit) AS uservisit,`user` 
	FROM `foodle_user_spend_visit_earn` WHERE loc_id='".$jsondata->loc_id."' GROUP BY `user`");
	$totaluser=$uservisitq->num_rows();
	$higher25=0;
	$higher25user=array();
	$lower25=0;
	$lower25user=array();
	$lower50=0;
	$lower50user=array();
	$averagevists=$totalvisit/$totaluser;
	if($uservisitq->num_rows()>0){
	  
	    foreach($uservisitq->result() as $val){
		
		$perc=$this->count_percent_increment($val->uservisit,$averagevists);
		
		if($perc[is_negative]==0){
		    if($perc['perc']>25){
			$higher25=$higher25+1;
			$higher25user[]=$val->user;
		    }
		}else{
		    if($perc['perc']<-25 ){
			$lower25=$lower25+1;
			$lower25user[]=$val->user;
		    }else if($perc['perc']<-50){
			$lower50=$lower50+1;
			$lower50user[]=$val->user;
		    }
		}
	    }
	    
	}
	
	$data['higher25']=$higher25;
	$data['higher25user']=(count($higher25user)>0)?implode(",",$higher25user):"";
	$data['lower25']=$lower25;
	$data['lower25user']=(count($lower25user)>0)?implode(",",$lower25user):"";
	$data['lower50']=$lower50;
	$data['lower50user']=(count($lower50user)>0)?implode(",",$lower50user):"";
	return $data;
	
     }
     
    public function WiFi_Loyalty($jsondata){
	$wifiloyalty_arr=array();
	$wlq=$this->db2->query("SELECT DISTINCT(`user`) AS user, COUNT(USER) AS count 
	    FROM `paytm_wifi_session` where loc_id='".$jsondata->loc_id."'
	    GROUP BY `user` ORDER BY count DESC");
	if($wlq->num_rows()>0){
	   $morethen5=0;
	   $times2_4=0;
	   $timesonce=0;
	   $morethen5_user=array();
	   $times2_4user=array();
	   $oncemonthless_user=array();
	   foreach($wlq->result() as $val){
		if($val->count>5){
		    $morethen5=$morethen5+1;
		    $morethen5_user[]=$val->user;
		}else if($val->count>=2 && $val->count<=4){
		    $times2_4=$times2_4+1;
		    $times2_4user[]=$val->user;
		}else if($val->count<=1){
		    $timesonce=$timesonce+1;
		    $oncemonthless_user[]=$val->user;
		}
	   }
	    $wifiloyalty_arr['morethen5']=$morethen5;
	    $wifiloyalty_arr['morethen5_user']=(count($morethen5_user)>0)?implode(",",$morethen5_user):"";
	    $wifiloyalty_arr['l2_4times']=$times2_4;
	    $wifiloyalty_arr['l2_4times_user']=(count($times2_4user)>0)?implode(",",$times2_4user):"";
	    $wifiloyalty_arr['oncemonthless']=$timesonce;
	    $wifiloyalty_arr['oncemonthless_user']=(count($oncemonthless_user)>0)?implode(",",$oncemonthless_user):"";
	}else{
	    $wifiloyalty_arr['morethen5']=0;
	    $wifiloyalty_arr['morethen5_user']='';
	    $wifiloyalty_arr['l2_4times']=0;
	    $wifiloyalty_arr['l2_4times_user']='';
	    $wifiloyalty_arr['oncemonthless']=0;
	    $wifiloyalty_arr['oncemonthless_user']='';
	}
	
	return $wifiloyalty_arr;
    }
     
      public function count_percent_increment($x,$y){
        $percent = '';
        if($y > 0){
            $diff = $x - $y ;
            $percent = ($diff/$y)*100;
            if($percent < 0)
            {
            $percent = $percent;
            }
            else
            {
            if($percent > 100){
                $percent = 100;
            }
            }
           
           
        }elseif($x == '0' && $y =='0'){
            $percent = 0;
        }
        else{
            $percent = 100;
        }
       $data['perc']=round($percent,2);
       if($diff<0){
	$data['is_negative']=1;
       }else{
	$data['is_negative']=0;
       }
       
        return $data;
    }
    
    public function send_campaign_msg($audience_user,$campaignid){
	$notificationarr=array();
	$query=$this->db2->query("SELECT wl.location_name,fllr.reward_name,fc.end_date FROM `foodle_campaign` fc
		INNER JOIN `foodle_location_loyalty_reward` fllr ON (fc.reward_id=fllr.id AND fllr.is_deleted='0')
		INNER JOIN `foodle_loyalty_reward` flr ON (fllr.reward_id=flr.id)
		INNER JOIN wifi_location wl ON (wl.id=fc.loc_id) where fc.id='".$campaignid."'");
	if($query->num_rows()>0){
	   $cdata =$query->row_array();
	   $location=$cdata['location_name'];
	   $rewardname=$cdata['reward_name'];
	   $expiry=date("jS M",strtotime($cdata['end_date']));
	   $nfuserq=$this->db2->query("select user,device_id from foodle_notification_user");
	   $nfuser=array();
	    $notificationarr['title']="FREE FOODLE ALERT!";
	    $notificationarr['message']=$rewardname." received from ".$location.".  Expires ".$expiry;
	    $usermessage="FREE Foodle Alert! ".$rewardname." received from ".$location.", expires ".$expiry.". Redeem now www.foodle.me";
	   if($nfuserq->num_rows()>0){
		foreach($nfuserq->result() as $val){
		    $nfuser[$val->user]['user']=$val->user;
		    $nfuser[$val->user]['client_id']=$val->device_id;
		}
	    }
	    $notificationsentarr=array();
	    $notification_sentq=$this->db2->query("select user from foodle_notification_sent where campaign_id='".$campaignid."'");
	    if($notification_sentq->num_rows()>0){
		foreach($notification_sentq->result() as $val){
		    $notificationsentarr[$val->user]=$val->user;
		}
	    }
	    foreach($audience_user as $val){
		if(!isset($notificationsentarr[$val])){
		    if(isset($nfuser[$val])){
		      // $notificationarr
		      $notificationarr['client_id']=$nfuser[$val]['client_id'];
		      $this->push_notification($notificationarr);
		    //  echo "=====>>push notification  ".$val."<br/>";
		      $tabledata=array("campaign_id"=>$campaignid,"user"=>$val,"sent_on"=>date("Y-m-d H:i:s"));
		      $this->db2->insert("foodle_notification_sent",$tabledata);
		    }else{
			// send text msg
			$this->infini_txtsmsgateway($val,$usermessage);
			$tabledata=array("campaign_id"=>$campaignid,"user"=>$val,"sent_on"=>date("Y-m-d H:i:s"),"notification_type"=>1);
			$this->db2->insert("foodle_notification_sent",$tabledata);
			// echo "=====>>Text Message  ".$val."<br/>";
		    }
		}
	    }
	   
	   //$notificationarr['client_id']==
	 //   $textmsg=
	    
	    
	}
	
    }
  
    
    public function push_notification($dataarr){
	
	$client_id = $dataarr['client_id'];
	$title = $dataarr['title'];
	$message = $dataarr['message'];
	$firebaseurl = 'https://fcm.googleapis.com/fcm/send';
	$fields = array();
	
	if (is_array($client_id))
		{
		$fields['registration_ids'] = $client_id;
		}
	  else
		{
		$fields['to'] = $client_id;
		}
	
	$fields['data'] = array(
		"message" => $message,
		"title" => $title,
		'vibrate' => 1,
		'largeIcon' => 'large_icon',
		'smallIcon' => 'small_icon'
	);
	$fields['notification'] = array(
		"title" => $title,
		"body" => $message,
		"text" => "Click me to open an Activity!",
		"sound" => "default"
	);
	$fields = json_encode($fields);
	
	$headers = array(
		'Authorization: key=' . "AAAAL52-pHE:APA91bH3qsIF-M0U4nFNvSaH48nIsvRksDzV2IeDQGU4e9SBYh1CXvqCz5xcPyRlpcfrthwED4rLI2Kzx3orsxA-2UCZruePlCfaKdO44uxWbdeWqtznW15ndexL17To5khheVpgg7D9",
		'Content-Type: application/json'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $firebaseurl);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
    }
    
    public function infini_txtsmsgateway($user_mobileno, $user_message)
    {
	 $infini_apikey = 'A921b98066097485eddeb9388665a8946';
	$infini_senderid = 'FOODLE';
	$baseUrl = "https://alerts.solutionsinfini.com/api/v4/?api_key=".$infini_apikey;
	$user_message=urlencode($user_message); 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl."&method=sms&message=$user_message&to=$user_mobileno&sender=$infini_senderid");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
	$result = curl_exec($ch);
	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
        if($statusCode==200){
	    return 1;
	}else{
	    return 0;
	}
	curl_close($ch);
    }
    
    public function send_via_vcemail($email,$message,$sub)
	{
		$this->load->library('email');
	    $from = SMTP_USER;
	    $fromname = 'FOODLE';
	    $config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => SMTP_HOST,
	    'smtp_port' => SMTP_PORT,
	    'smtp_user' => 'Foodle@shouut.com',
	    'smtp_pass' => 'Foodle@Shouut2019#',
	  // 'smtp_user' => SMTP_USER,
	  // 'smtp_pass' => SMTP_PASS,
	    'mailtype'  => 'html',
	    'charset'   => 'iso-8859-1',
	    'wordwrap'  => FALSE,
	    'crlf'      => "\r\n",
	    'newline'   => "\r\n"
	    );
		   $this->email->initialize($config);
                $this->email->from($from, $fromname);
                $this->email->to($email);
				 
                $this->email->subject($sub);
                $this->email->message($message);	
                $this->email->send();
               return true;
	}
	
    public function send_campaign_notification(){
	
	$csendq=$this->db2->query("select campaign_id,user from foodle_campaign_sent where is_referred='0'");
	$cmpgnsendarr=array();
	if($csendq->num_rows()>0){
	    foreach($csendq->result() as $val){
		$cmpgnsendarr[$val->campaign_id][$val->user]=$val->user;
	    }
	}
	
	 $nfuserq=$this->db2->query("select user,device_id from foodle_notification_user");
	$nfuser=array();
	   if($nfuserq->num_rows()>0){
		foreach($nfuserq->result() as $val){
		    $nfuser[$val->user]['user']=$val->user;
		    $nfuser[$val->user]['client_id']=$val->device_id;
		}
	    }
	    $notificationsentarr=array();
	    $notification_sentq=$this->db2->query("select user,campaign_id from foodle_notification_sent");
	    if($notification_sentq->num_rows()>0){
		foreach($notification_sentq->result() as $val){
		    $notificationsentarr[$val->campaign_id][$val->user]=$val->user;
		}
	    }
	
	$query=$this->db2->query("SELECT fc.id,wl.location_name,fllr.reward_name,fc.end_date FROM `foodle_campaign` fc
		INNER JOIN `foodle_location_loyalty_reward` fllr ON (fc.reward_id=fllr.id AND fllr.is_deleted='0')
		INNER JOIN `foodle_loyalty_reward` flr ON (fllr.reward_id=flr.id)
		INNER JOIN wifi_location wl ON (wl.id=fc.loc_id) where  fc.is_deleted='0' and
		now() between fc.start_date and fc.end_date");
	//echo "===>>".$this->db2->last_query();
	//echo "<pre>"; print_R($query->result()); die;
	if($query->num_rows()>0){
	    foreach($query->result() as $valx){
		
	   $location=$valx->location_name;
	   $rewardname=$valx->reward_name;
	   $expiry=date("jS M",strtotime($valx->end_date));
	  
	    $notificationarr['title']="FREE FOODLE ALERT!";
	    $notificationarr['message']=$rewardname." received from ".$location.".  Expires ".$expiry;
	    $usermessage="FREE Foodle Alert! ".$rewardname." received from ".$location.", expires ".$expiry.". Redeem now www.foodle.me";
	   
	    foreach($cmpgnsendarr[$valx->id] as $val){
		if(!isset($notificationsentarr[$valx->id][$val])){
		   // echo "===>>".$val."::::".$valx->id."<br/>";
		    if(isset($nfuser[$val])){
		      $notificationarr['client_id']=$nfuser[$val]['client_id'];
		      $this->push_notification($notificationarr);
		      $tabledata=array("campaign_id"=>$valx->id,"user"=>$val,"sent_on"=>date("Y-m-d H:i:s"));
		      $this->db2->insert("foodle_notification_sent",$tabledata);
		    }else{
			// send text msg
			$this->infini_txtsmsgateway($val,$usermessage);
			$tabledata=array("campaign_id"=>$valx->id,"user"=>$val,"sent_on"=>date("Y-m-d H:i:s"),"notification_type"=>1);
			$this->db2->insert("foodle_notification_sent",$tabledata);
			// echo "=====>>Text Message  ".$val."<br/>";
		    }
		}
	    }
		
	    }
	    echo "Notification Sent";
	}else{
	   echo "No Campaign Live"; 
	}
	
	
    }
	
	public function push_notification_test($dataarr){
	//echo "<pre>"l print_R
	$client_id = $dataarr['client_id'];
	$title = $dataarr['title'];
	$message = $dataarr['message'];
	$firebaseurl = 'https://fcm.googleapis.com/fcm/send';
	$fields = array();
	
	if (is_array($client_id))
		{
		$fields['registration_ids'] = $client_id;
		}
	  else
		{
		$fields['to'] = $client_id;
		}
	
	$fields['data'] = array(
		"message" => $message,
		"title" => $title,
		'vibrate' => 1,
		'largeIcon' => 'large_icon',
		'smallIcon' => 'small_icon'
	);
	$fields['notification'] = array(
		"title" => $title,
		"body" => $message,
		"text" => "Click me to open an Activity!",
		"sound" => "default"
	);
	$fields = json_encode($fields);
	//echo $fields; die;
	$headers = array(
		'Authorization: key=' . "AAAAL52-pHE:APA91bH3qsIF-M0U4nFNvSaH48nIsvRksDzV2IeDQGU4e9SBYh1CXvqCz5xcPyRlpcfrthwED4rLI2Kzx3orsxA-2UCZruePlCfaKdO44uxWbdeWqtznW15ndexL17To5khheVpgg7D9",
		'Content-Type: application/json'
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $firebaseurl);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	$result = curl_exec($ch);
	echo "<pre>"; print_R($result); die;
	curl_close($ch);
	
    }
     
     
     
     

    
    
    
    
   
    
    
    
} 