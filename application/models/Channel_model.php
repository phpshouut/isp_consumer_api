<?php
class Channel_model extends CI_Model{
    private $DB2;
    public function __construct(){
        $this->DB2 = $this->load->database('db2_publicwifi', TRUE);
        
    }
    public function locationLogin($request){
        $data = array();
        if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $data['is_location_login'] = '0';
	}else{
	    //$data['is_location_login'] = '1';
	    $data['is_location_login'] = '0';
	}
	$isp_uid = '';
	if(isset($request->isp_uid)){
	    $isp_uid = $request->isp_uid;
	}
	
	$currancy = '&#36;';
	$get_country = $this->db->query("select country_id from sht_isp_admin where isp_uid = '$isp_uid'");
	if($get_country->num_rows() > 0){
	    $row = $get_country->row_array();
	    $country_id = $row['country_id'];
	    $get_currency = $this->db->query("select scr.currency_symbol from sht_countries as sc inner join sht_currency as scr on (sc.currency_id = scr.currency_id) where sc.id = '$country_id'");
	    if($get_currency->num_rows() > 0){
		$row1 = $get_currency->row_array();
		$currancy = $row1['currency_symbol'];
	    }
	}
        $data['cuntry_currency'] = $currancy;
        return $data;
    }

     public function logout($provider_id){
        $delete = $this->DB2->query("delete from isp_login_session where isp_uid = '$provider_id'");
    }
    public function check_session($request){
        $data = array();
        if(isset($request->isp_uid)){
            $isp_uid = $request->isp_uid;
            $get = $this->DB2->query("select * from isp_login_session where isp_uid = '$isp_uid'");
            if($get->num_rows() > 0){
                $row = $get->row_array();
                $email = $row['email'];
                $password = $row['password'];
                $request = array("email" => $email, 'password' => $password, "isp_uid" => $isp_uid);
		$request = json_encode($request);
                $this->login(json_decode($request));
		$delete = $this->DB2->query("delete from isp_login_session where isp_uid = '$isp_uid'");
            }
        }
       return $data;

    }
    public function login($request){
	$data = array();
	if(!isset($request->email) || !isset($request->password)){
            $data['resultCode'] = '0';
            $data['resultMessage'] =  'Please Enter Username and Password';
        }
	else{
	    $email = $request->email;
            $password = $request->password;
	    // check brand from brand user table 
	    $query1 = $this->DB2->query("select * from brand_user WHERE email = '$email'");
	    if($query1->num_rows() > 0){
		$row = $query1->row_array();
                $user_id = $row['id'];
                $salt = $row['salt'];
		$current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row['expiry_date'])){
                    $data['resultCode'] = '0';
                    $data['resultMessage'] =  'Your voucher validity is expired.';
                }
		else{
		    $encrypt_password = md5($password . $salt);
		    if ($encrypt_password == $row['password']) {
			// get provider id of login brand
                        $brand_id = $row['brand_id'];
			$get_provider_id = $this->db->query("select provider_id from brand WHERE brand_id = '$brand_id'");
			$provider_id = '0';
                        if($get_provider_id->num_rows() > 0){
                            $provider_row = $get_provider_id->row_array();
                            $provider_id = $provider_row['provider_id'];
                        }
			$newdata = array(
                            'uid'  => uniqid('ang_'),
                            'logintype'  => 'brand',
                            'userid'     => $user_id,
                            'admin_userid' => '',
                            'brandid' => $row['brand_id'],
                            'provider_id' => $provider_id,
			    'location_uid' => '',
                        );
			$this->session->set_userdata($newdata);
			
			if($row['tc_read'] == '0'){
                            $data['resultCode'] = '2';
                            $data['resultMessage'] =  'Please check term conditionn';
                        }else{
                            $this->session->set_userdata('uid', uniqid('ang_'));
                            $data['resultCode'] = '1';
                            $data['resultMessage'] =  'Success';
                        }
		    }else{
                        $data['resultCode'] = '0';
                        $data['resultMessage'] =  'Invalid Password';
                    }
		}
	    }
	    else{
		if(isset($request->isp_uid)){
		    // isp login 
		    $email = $request->email;
		    $password = $request->password;
		    $isp_uid = $request->isp_uid;
		    $pass_check = md5($password);
		    $query1 = $this->db->query("select * from sht_isp_admin WHERE email = '$email' AND password = '$pass_check' and isp_uid = '$isp_uid'");
		    if($query1->num_rows() > 0){
			// set session table value
			$get_session = $this->DB2->query("select id from isp_login_session where isp_uid = '$isp_uid'");
			if($get_session->num_rows() > 0){
			    $update_session = $this->DB2->query("update isp_login_session set email = '$email', password = '$password' where isp_uid = '$isp_uid'");
			}else{
			    $insert_session = $this->DB2->query("insert into isp_login_session(isp_uid,email,password,created_on) values('$isp_uid', '$email', '$password', now())");
			}
			$isp_detail_row = $query1->row_array();
			$isp_id = $isp_detail_row['id'];
			$brand_name = $isp_detail_row['isp_name'];
			// get isp detail to update in brand table
			$get_isp_detail = $this->db->query("select address1, address2, original_image, small_image, logo_image from sht_isp_detail where isp_uid = '$isp_uid'");
			$adderss1= '';
                        $address2= '';
                        $original_image= '';
                        $small_image= '';
                        $logo_image= '';
			if($get_isp_detail->num_rows() > 0){
			    $get_isp_detail_row = $get_isp_detail->row_array();
			    $adderss1= $get_isp_detail_row['address1'];
			    $address2= $get_isp_detail_row['address2'];
			    $original_image= $get_isp_detail_row['original_image'];
			    $small_image= $get_isp_detail_row['small_image'];
			    $logo_image= $get_isp_detail_row['logo_image'];
			}
			//check brand created for isp or not
			$check = $this->DB2->query("select * from brand where provider_id = '$isp_uid'");
			if($check->num_rows() > 0){
			    $check_row = $check->row_array();
			    $brand_id = $check_row['brand_id'];
			    $this->DB2->query("update brand set brand_email = '$email', brand_name = '$brand_name', brand_address = '$address1', original_logo = '$original_image', original_logo_logoimage = '$logo_image', original_logo_smallimage = '$small_image' where provider_id = '$isp_uid'");
                        }else{
			    // register isp as brand
			    $this->DB2->query("insert into brand (brand_name, provider_id, brand_address, original_logo, original_logo_logoimage, original_logo_smallimage,brand_email) values('$brand_name',  '$isp_uid', '$address1', '$original_image', '$logo_image', '$small_image', '$email')");
			    $brand_id = $this->DB2->insert_id();
			}
			$newdata = array(
                            'uid'  => uniqid('ang_'),
                            'logintype'  => 'brand',
                            'isp_login' => "yes",
                            'userid'     => '',
                            'admin_userid' => '',
                            'brandid' => $brand_id,
                            'provider_id' => $isp_uid,
			    'location_uid' => '',
                        );
			$this->session->set_userdata($newdata);
                        $data['resultCode'] = '1';
                        $data['resultMessage'] =  'success';
		    }// location login
		    else{
			$email = $request->email;
			$password = $request->password;
			$isp_uid = $request->isp_uid;
			if(isset($request->location_uid)){
			    $location_uid = $request->location_uid;
			}else{
			    $location_uid = '';
			}
			
			// if brand not created then first create
			$isp_uid = $request->isp_uid;
			$brand_id = '0';
			$check_brand = $this->DB2->query("select brand_id from brand where provider_id = '$isp_uid'");
			if($check_brand->num_rows() > 0){
			    $brand_row = $check_brand->row_array();
			    $brand_id = $brand_row['brand_id'];
			}else{
			    $brand_name = '';
			    $query1 = $this->db->query("select * from sht_isp_admin WHERE isp_uid = '$isp_uid'");
			    if($query1->num_rows() > 0){
				$isp_detail_row = $query1->row_array();
				$brand_name = $isp_detail_row['isp_name'];
			    }
			    // get isp detail to update in brand table
			    $get_isp_detail = $this->db->query("select address1, address2, original_image, small_image, logo_image from sht_isp_detail where isp_uid = '$isp_uid'");
			    $adderss1= '';
			    $address2= '';
			    $original_image= '';
			    $small_image= '';
			    $logo_image= '';
			    if($get_isp_detail->num_rows() > 0){
				$get_isp_detail_row = $get_isp_detail->row_array();
				$adderss1= $get_isp_detail_row['address1'];
				$address2= $get_isp_detail_row['address2'];
				$original_image= $get_isp_detail_row['original_image'];
				$small_image= $get_isp_detail_row['small_image'];
				$logo_image= $get_isp_detail_row['logo_image'];
			    }
			    // register isp as brand
			    $this->DB2->query("insert into brand (brand_name, provider_id, brand_address, original_logo, original_logo_logoimage, original_logo_smallimage,brand_email) values('$brand_name',  '$isp_uid', '$address1', '$original_image', '$logo_image', '$small_image', '$email')");
			    $brand_id = $this->DB2->insert_id();
			}
			$check_location_login = $this->DB2->query("select id,location_uid from wifi_location where password = '$password' and user_email = '$email' and isp_uid = '$isp_uid'");
			if($check_location_login->num_rows() > 0){
			    $row_brand = $check_location_login->row_array();
			    $location_uid = $row_brand['location_uid'];
			    // set session table value
			    $get_session = $this->DB2->query("select id from isp_login_session where isp_uid = '$isp_uid' and location_uid = '$location_uid'");
			    if($get_session->num_rows() > 0){
				$update_session = $this->DB2->query("update isp_login_session set email = '$email', password = '$password' where isp_uid = '$isp_uid' and location_uid = '$location_uid'");
			    }else{
				$insert_session = $this->DB2->query("insert into isp_login_session(isp_uid,email,password,created_on, location_uid) values('$isp_uid', '$email', '$password', now(), '$location_uid')");
			    }
			    
			    $newdata = array(
				'uid'  => uniqid('ang_'),
				'logintype'  => 'location',
				'userid'     => '',
				'admin_userid' => '',
				'brandid' => $brand_id,
				'provider_id' => $isp_uid,
				'location_uid' => $location_uid
			    );
			    $this->session->set_userdata($newdata);
			    $data['resultCode'] = '1';
			    $data['resultMessage'] =  'Success';
			}else{
			    $data['resultCode'] = '0';
			    $data['resultMessage'] =  'Invalid Password';
			}
		    }
		}
		
	    }
            
	}
	return $data;
    }
     //new isp changes done
        public function login_before_location_login($request){
        if(!isset($request->email) || !isset($request->password)){
            $data['resultCode'] = '0';
            $data['resultMessage'] =  'Please Enter Username and Password';
        }
        else{
            $email = $request->email;
            $password = $request->password;
            $data = array();
            $query1 = $this->DB2->query("select * from brand_user WHERE email = '$email'");
            if($query1->num_rows() > 0){
                
                $row = $query1->row_array();
                $user_id = $row['id'];
                $salt = $row['salt'];
                $current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row['expiry_date'])){
                    $data['resultCode'] = '0';
                    $data['resultMessage'] =  'Your voucher validity is expired.';
                }
                else{
                    $encrypt_password = md5($password . $salt);
                    if ($encrypt_password == $row['password']) {
                        // get provider id of login brand
                        $brand_id = $row['brand_id'];
                        $get_provider_id = $this->db->query("select provider_id from brand WHERE brand_id = '$brand_id'");
                        $provider_id = '0';
                        if($get_provider_id->num_rows() > 0){
                            $provider_row = $get_provider_id->row_array();
                            $provider_id = $provider_row['provider_id'];
                        }
                        $newdata = array(
                            'uid'  => uniqid('ang_'),
                            'logintype'  => 'brand',
                            'userid'     => $user_id,
                            'admin_userid' => '',
                            'brandid' => $row['brand_id'],
                            'provider_id' => $provider_id
                        );
                        $this->session->set_userdata($newdata);
                        if($row['tc_read'] == '0'){
                            $data['resultCode'] = '2';
                            $data['resultMessage'] =  'Please check term conditionn';
                        }else{
                            $this->session->set_userdata('uid', uniqid('ang_'));
                            $data['resultCode'] = '1';
                            $data['resultMessage'] =  'Success';
                        }

                    }else{
                        $data['resultCode'] = '0';
                        $data['resultMessage'] =  'Invalid Password';
                    }
                }

            }
            
            else{
                //check is isp login 
                $email = $request->email;
                $password = $request->password;
                $isp_uid = $request->isp_uid;
                $data = array();
                $pass_check = md5($password);
                $query1 = $this->db->query("select * from sht_isp_admin WHERE email = '$email' AND password = '$pass_check' and isp_uid = '$isp_uid'");
                if($query1->num_rows() > 0){
                   // set session table value
                    $get_session = $this->DB2->query("select id from isp_login_session where isp_uid = '$isp_uid'");
                    if($get_session->num_rows() > 0){
                        $update_session = $this->DB2->query("update isp_login_session set email = '$email', password = '$password' where isp_uid = '$isp_uid'");
                    }else{
                        $insert_session = $this->DB2->query("insert into isp_login_session(isp_uid,email,password,created_on) values('$isp_uid', '$email', '$password', now())");
                    }
                    $isp_detail_row = $query1->row_array();
                    $isp_id = $isp_detail_row['id'];
                    $brand_name = $isp_detail_row['isp_name'];
                    // get isp detail to update in brand table
                    $get_isp_detail = $this->db->query("select address1, address2, original_image, small_image, logo_image from sht_isp_detail where isp_uid = '$isp_uid'");
                        $adderss1= '';
                        $address2= '';
                        $original_image= '';
                        $small_image= '';
                        $logo_image= '';
                    if($get_isp_detail->num_rows() > 0){
                        $get_isp_detail_row = $get_isp_detail->row_array();
                        $adderss1= $get_isp_detail_row['address1'];
                        $address2= $get_isp_detail_row['address2'];
                        $original_image= $get_isp_detail_row['original_image'];
                        $small_image= $get_isp_detail_row['small_image'];
                        $logo_image= $get_isp_detail_row['logo_image'];
                    }
                    //check brand created for isp or not
                    $check = $this->DB2->query("select * from brand where provider_id = '$isp_uid'");
                    if($check->num_rows() > 0){
                        $check_row = $check->row_array();
                        $brand_id = $check_row['brand_id'];
                      $this->DB2->query("update brand set brand_email = '$email', brand_name = '$brand_name', brand_address = '$address1', original_logo = '$original_image', original_logo_logoimage = '$logo_image', original_logo_smallimage = '$small_image' where provider_id = '$isp_uid'");
                        
                    }else{
                        // register isp as brand
                        $this->DB2->query("insert into brand (brand_name, provider_id, brand_address, original_logo, original_logo_logoimage, original_logo_smallimage,brand_email) values('$brand_name',  '$isp_uid', '$address1', '$original_image', '$logo_image', '$small_image', '$email')");
                        $brand_id = $this->DB2->insert_id();
                    }
                    $newdata = array(
                            'uid'  => uniqid('ang_'),
                            'logintype'  => 'brand',
                            'isp_login' => "yes",
                            'userid'     => '',
                            'admin_userid' => '',
                            'brandid' => $brand_id,
                            'provider_id' => $isp_uid
                        );
                            $this->session->set_userdata($newdata);
                            $data['resultCode'] = '1';
                            $data['resultMessage'] =  'success';
                    
                }else{
                        $data['resultCode'] = '0';
                        $data['resultMessage'] =  'Invalid Password';
                    }
                
                
            }
        }
        return $data;
    }

    //new isp changes done
        public function brand_header_info(){
        $data = array();
        if($this->session->userdata('userid') && $this->session->userdata('userid') != ''){
             $userid = $this->session->userdata('userid');
            $query = $this->DB2->query(" select bu.email, bu.expiry_date, b.original_logo_logoimage from brand_user as bu INNER JOIN brand as b ON bu.id=b
 .user_id
 WHERE bu
 .id='$userid'");
            if($query->num_rows()>0){
                foreach($query->result() as $row)
                    $data['user_email'] = $row->email;
                $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo.png';
                
                $now = strtotime(date('d-m-Y')); // or your date as well
                $datediff = strtotime($row->expiry_date) - $now;
                $days_left = floor($datediff/(60*60*24));
                $data['day_left']=$days_left;
		
                $data['resultCode'] = '1';
            }else{
                $data['resultCode'] = '0';
            }

        }else{
            if($this->session->userdata('brandid') && $this->session->userdata('brandid') != '' && $this->session->userdata('location_uid') == ''){
                $brandid = $this->session->userdata('brandid');
                $userid = $this->session->userdata('admin_userid');
                $query = $this->DB2->query(" select  b.original_logo_logoimage, brand_email from brand as b WHERE brand_id = $brandid");
                if($query->num_rows()>0){
                    foreach($query->result() as $row)
                    if($this->session->userdata('isp_login')&& $this->session->userdata('isp_login') == 'yes'){
                        $data['user_email'] = $row->brand_email;
			if($row->original_logo_logoimage != ''){
			    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo/logo/'.$row->original_logo_logoimage;
			}else{
			    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo.png';
			}
                         
                         //$data['brand_logo'] = 'http://cdn101.shouut.com/shouutmedia/airtel_logo/airtel-new-logo1.png';
                    }else{
                        $data['user_email'] = '';
			if($row->original_logo_logoimage != ''){
			    $data['brand_logo'] = IMAGEPATHURL.'brand/logo/logo/'.$row->original_logo_logoimage;
			}else{
			    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo.png';
			}
                        
                        //$data['brand_logo'] = 'http://cdn101.shouut.com/shouutmedia/airtel_logo/airtel-new-logo1.png';
                    }
                    
                        
                    $data['day_left']='Unlimited';
                    $data['resultCode'] = '1';
                }else{
                    $data['resultCode'] = '0';
                }

            }elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
		$location_uid = $this->session->userdata('location_uid');
		$user_email = '';
		$brand_logo = '';
		$get_loc_info = $this->DB2->query("select wl.user_email, wl.isp_uid, wlct.logo_image from wifi_location as wl left join wifi_location_cptype wlct on (wl.location_uid = wlct.uid_location) where wl.location_uid = '$location_uid'");
		if($get_loc_info->num_rows() > 0){
		    $row = $get_loc_info->row_array();
		    $user_email = $row['user_email'];
		    if(isset($row['logo_image']) && $row['logo_image'] != ''){
			$brand_logo = CLOUD_IMAGEPATH."isp_location_logo/logo/".$row['logo_image'];
		    }
		    if($brand_logo  == ''){
			$isp_uid = $this->session->userdata('provider_id');
			$query = $this->db->query("select small_image,logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
			if($query->num_rows() > 0){
			    $rowdata = $query->row();
			    if($rowdata->logo_image != ''){
				$brand_logo =  CLOUD_IMAGEPATH."isp_logo/logo/".$rowdata->logo_image;
			    }else{
				$brand_logo = CLOUD_IMAGEPATH.'isp_logo.png';
			    }
			    
			}else{
			    $brand_logo = CLOUD_IMAGEPATH.'isp_logo.png';
			}
		    }
		}
		$data['user_email'] = $user_email;
		$data['brand_logo'] = $brand_logo;
		$data['day_left']='Unlimited';
	    }else{
                $data['resultCode'] = '0';
            }

        }

        return $data;
    }
    

    //new isp changes done
    public function brand_subheader_info(){
        $data = array();
        if($this->session->userdata('userid') && $this->session->userdata('userid') != ''){
            $userid = $this->session->userdata('userid');
            $query = $this->DB2->query("select  (select count(*) from cp_offers where brand_id = b.brand_id and is_deleted = '0') as totalcamp,   (SELECT COUNT(*) FROM flash_sale WHERE
brand_id = b.brand_id AND is_temp = '0' AND is_deleted = '0') as totalflashsale, (SELECT COUNT(*) FROM brand_follower
 WHERE
brand_id = b.brand_id) as totalfollower, (SELECT COUNT(*) from store WHERE brand_id = b.brand_id AND is_deleted =
'0') as
totalstore,
(SELECT COUNT(*) from get_event WHERE brand_id=b.brand_id) as totaloffer, b
.brand_name, b
.original_logo_logoimage, b
.original_big_logo
from brand as b
             WHERE user_id = '$userid'");
            if($query->num_rows()>0){
                foreach($query->result() as $row)
                    $brand_id = '';
                if($this->session->userdata('brandid')){
                    $brand_id = $this->session->userdata('brandid');
                }
                $data['brand_id'] = $brand_id;
                $data['user_type'] = 'brand';
                $data['brand_name'] = strtoupper($row->brand_name);
		if($row->original_logo_logoimage != ''){
		    $data['brand_logo'] = IMAGEPATHURL.'brand/logo/logo/'.$row->original_logo_logoimage;
		}else{
		    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo.png';
		}
                
                $data['brand_banner'] = IMAGEPATHURL.'brand/banner/original/'
                    .$row->original_big_logo;
                $data['totalfollower'] = $row->totalfollower;
                $data['totalstore'] = $row->totalstore;
                $data['totaloffer'] = $row->totaloffer;
                $data['totalflashsale'] = $row->totalflashsale;
                $data['totalcampaign'] = $row->totalcamp;

                $data['resultCode'] = '1';
            }else{
                $data['resultCode'] = '0';
            }

        }else{
            if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
                $brandid = $this->session->userdata('brandid');
                $query = $this->DB2->query("select (select count(*) from cp_offers where brand_id = b.brand_id and is_deleted = '0' and created_by_location = '0') as totalcamp,  (SELECT COUNT(*) FROM brand_follower
 WHERE brand_id = b.brand_id) as totalfollower, (SELECT COUNT(*) from store WHERE brand_id = b.brand_id AND is_deleted =
'0') as totalstore,b.brand_name, b
.original_logo_logoimage, b
.original_big_logo
from brand as b  WHERE brand_id = '$brandid'");
                if($query->num_rows()>0){
                    foreach($query->result() as $row)
                        $brand_id = '';
                    if($this->session->userdata('brandid')){
                        $brand_id = $this->session->userdata('brandid');
                    }
                    $data['brand_id'] = $brand_id;
                    
                    $data['brand_name'] = strtoupper($row->brand_name);
                    
                    if($this->session->userdata('isp_login')&& $this->session->userdata('isp_login') == 'yes'){
                        $data['brand_banner'] = CLOUD_IMAGEPATH.'isp_logo/logo/'.$row->original_big_logo;
			if($row->original_logo_logoimage != ''){
			    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo/logo/'.$row->original_logo_logoimage;
			}else{
			    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo.png';
			}
                        
                        //$data['brand_logo'] = 'http://cdn101.shouut.com/shouutmedia/airtel_logo/airtel-new-logo1.png';
                        $data['user_type'] = 'isp';
                    }else{
                        $data['brand_banner'] = IMAGEPATHURL.'brand/banner/original/'.$row->original_big_logo;
			if($row->original_logo_logoimage != ''){
			    $data['brand_logo'] = IMAGEPATHURL.'brand/logo/logo/'.$row->original_logo_logoimage;
			}else{
			    $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo.png';
			}
                        
                        //$data['brand_logo'] = 'http://cdn101.shouut.com/shouutmedia/airtel_logo/airtel-new-logo1.png';
                        $data['user_type'] = 'admin';
                    }
                    
                    
                    $data['totalfollower'] = $row->totalfollower;
                    $data['totalstore'] = $row->totalstore;
                    $data['totaloffer'] = '0';
                    $data['totalflashsale'] = '0';
                    $data['totalcampaign'] = $row->totalcamp;
                    $data['resultCode'] = '1';
                }else{
                    $data['resultCode'] = '0';
                }
            }
	    elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
		$location_uid = $this->session->userdata('location_uid');
		$brand_logo = '';
		$brand_name = '';
		$location_id = '0';
		$get_loc_info = $this->DB2->query("select wl.id as location_id, wl.location_name,wl.user_email, wl.isp_uid, wlct.logo_image from wifi_location as wl left join wifi_location_cptype wlct on (wl.location_uid = wlct.uid_location) where wl.location_uid = '$location_uid'");
		if($get_loc_info->num_rows() > 0){
		    $row = $get_loc_info->row_array();
		    $brand_name = $row['location_name'];
		    $location_id = $row['location_id']; 
		    if(isset($row['logo_image']) && $row['logo_image'] != ''){
			$brand_logo = CLOUD_IMAGEPATH."isp_location_logo/logo/".$row['logo_image'];
		    }
		    if($brand_logo  == ''){
			$isp_uid = $this->session->userdata('provider_id');
			$query = $this->db->query("select small_image,logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
			if($query->num_rows() > 0){
			    $rowdata = $query->row();
			    if($rowdata->logo_image != ''){
				$brand_logo =  CLOUD_IMAGEPATH."isp_logo/logo/".$rowdata->logo_image;
			    }else{
				$brand_logo =  CLOUD_IMAGEPATH.'isp_logo.png';
			    }
			    
			}else{
			   $brand_logo = CLOUD_IMAGEPATH.'isp_logo.png';
			}
		    }
		}
		$offer_query = $this->DB2->query("select co.cp_offer_id from cp_offers as co inner join cp_offers_location as col on (co.cp_offer_id = col.cp_offer_id) where col.location_id = '$location_id' and co.is_deleted = '0' and co.created_by_location = '$location_uid'");
		//echo $this->DB2->last_query();
		$data['brand_banner'] = $brand_logo;
                $data['brand_logo'] = $brand_logo;
                $data['user_type'] = 'isp';
		$data['brand_id'] = $location_uid;
                $data['brand_name'] = strtoupper($brand_name);
		$data['totalfollower'] = '0';
                $data['totalstore'] = '0';
                $data['totaloffer'] = '0';
                $data['totalflashsale'] = '0';
		$data['totalcampaign'] = $offer_query->num_rows();
                $data['resultCode'] = '1';
	    }
	    else{
                $data['resultCode'] = '0';
            }
        }

        return $data;
    }
    // new isp changes done
    public function dashboard_active_campaign_list_oldddd(){
        $data = array();
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_brand_id = '$brandid' and created_by_location = '0' and status = '1' and is_deleted = '0' and campaign_end_date > now() ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                if($row->offer_type == "offer_add"){
                    $data[$i]['color_class'] = 'yellow';
                }
                elseif($row->offer_type == 'video_add'){
                    $data[$i]['color_class'] = 'grey';
                }
                else{
                    $data[$i]['color_class'] = 'red';
                }

                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;

                $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
                Left";
                $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
                $i++;
            }
        }
	elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $location_uid = $this->session->userdata('location_uid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_location = '$location_uid' and status = '1' and is_deleted = '0' and campaign_end_date > now() ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                if($row->offer_type == "offer_add"){
                    $data[$i]['color_class'] = 'yellow';
                }
                elseif($row->offer_type == 'video_add'){
                    $data[$i]['color_class'] = 'grey';
                }
                else{
                    $data[$i]['color_class'] = 'red';
                }

                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;

                $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
                Left";
                $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
                $i++;
            }
	}
        return $data;
    }
    // new isp changes done
    public function dashboard_expire_campaign_list_oldddd(){
        $data = array();
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_brand_id = '$brandid' and created_by_location = '0'  AND (status = '0' OR is_deleted = '1'  OR campaign_end_date < now()) ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                $j = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;
                $current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
                {
                    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
                }
                else{
                    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
                }
                $i++;
            }
        }
	elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
            $location_uid = $this->session->userdata('location_uid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_location = '$location_uid'  AND (status = '0' OR is_deleted = '1'  OR campaign_end_date < now()) ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                $j = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;
                $current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
                {
                    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
                }
                else{
                    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
                }
                $i++;
            }
        }
        return $data;
    }
    public function dashboard_active_campaign_list(){
        $data = array();
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_brand_id = '$brandid' and created_by_location = '0' and status = '1' and is_deleted = '0' and campaign_end_date > now() AND offer_type != 'audit_program' ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                if($row->offer_type == "offer_add"){
                    $data[$i]['color_class'] = 'yellow';
                }
                elseif($row->offer_type == 'video_add'){
                    $data[$i]['color_class'] = 'grey';
                }
                else{
                    $data[$i]['color_class'] = 'red';
                }

                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;

                $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
                Left";
                $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
                $i++;
            }
	    // for audit program
	    $query = $this->DB2->query("select cpo.is_perpetual ,cpo.campaign_start_date,cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_brand_id = '$brandid' and created_by_location = '0' and status = '1' and is_deleted = '0'  AND offer_type = 'audit_program' ORDER BY cp_offer_id DESC ");
	    foreach($query->result() as $row){
		$start_date =  strtotime($row->campaign_start_date);
		$campaign_end_date =  strtotime($row->campaign_end_date);
		if($campaign_end_date == '')
		{
		    $campaign_end_date = 0;
		}
		if($start_date > 0 && $start_date != '')
		{
		    if($campaign_end_date > strtotime(date("Y-m-d")) || $row->is_perpetual == '1')
		    {
			$data[$i]['offer_id'] = $row->cp_offer_id;
			if($row->offer_type == "offer_add"){
			    $data[$i]['color_class'] = 'yellow';
			}
			elseif($row->offer_type == 'video_add'){
			    $data[$i]['color_class'] = 'grey';
			}
			else{
			    $data[$i]['color_class'] = 'red';
			}
	
			$data[$i]['campaign_name'] = $row->campaign_name;
			$data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
			$get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id AND cpol.is_deleted = '0') where cpol.cp_offer_id = '$row->cp_offer_id'
	AND wl.status = 1 AND wl.is_deleted = 0");
			$toal_location = 0;
			$city = array();
			foreach($get_location_detail->result() as $row1){
			    $toal_location = $toal_location + 1;
			    $city[] = ucwords(strtolower($row1->city));
			}
			$city = array_values(array_unique($city));
			$city_name = array_slice($city, 0, 3);
			$city_name_more = array_slice($city, 3, count($city));
			$more_city = '';
			if(count($city_name_more) > 0){
			    $more_city = "+".count($city_name_more)."more";
			}
			$data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
			$data[$i]['total_location'] = $toal_location;
	
			if($row->is_perpetual == 0)
			{
			    $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
			Left";
			    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
			}
			else
			{
			    $data[$i]['days_left'] = 'Perpetual';
			    $data[$i]['days'] = 'Perpetual';
			}
			
			$i++;
		    }
		    
		}
                
            }
        }
	elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $location_uid = $this->session->userdata('location_uid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_location = '$location_uid' and status = '1' and is_deleted = '0' and campaign_end_date > now() AND offer_type != 'audit_program' ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                if($row->offer_type == "offer_add"){
                    $data[$i]['color_class'] = 'yellow';
                }
                elseif($row->offer_type == 'video_add'){
                    $data[$i]['color_class'] = 'grey';
                }
                else{
                    $data[$i]['color_class'] = 'red';
                }

                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;

                $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
                Left";
                $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
                $i++;
            }
	    // for audit program
	    $query = $this->DB2->query("select cpo.is_perpetual ,cpo.campaign_start_date,cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_location = '$location_uid'  and status = '1' and is_deleted = '0'  AND offer_type = 'audit_program' ORDER BY cp_offer_id DESC ");
	    foreach($query->result() as $row){
		$start_date =  strtotime($row->campaign_start_date);
		$campaign_end_date =  strtotime($row->campaign_end_date);
		if($campaign_end_date == '')
		{
		    $campaign_end_date = 0;
		}
		if($start_date > 0 && $start_date != '')
		{
		    if($campaign_end_date > strtotime(date("Y-m-d")) || $row->is_perpetual == '1')
		    {
			$data[$i]['offer_id'] = $row->cp_offer_id;
			if($row->offer_type == "offer_add"){
			    $data[$i]['color_class'] = 'yellow';
			}
			elseif($row->offer_type == 'video_add'){
			    $data[$i]['color_class'] = 'grey';
			}
			else{
			    $data[$i]['color_class'] = 'red';
			}
	
			$data[$i]['campaign_name'] = $row->campaign_name;
			$data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
			$get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id AND cpol.is_deleted = '0') where cpol.cp_offer_id = '$row->cp_offer_id'
	AND wl.status = 1 AND wl.is_deleted = 0");
			$toal_location = 0;
			$city = array();
			foreach($get_location_detail->result() as $row1){
			    $toal_location = $toal_location + 1;
			    $city[] = ucwords(strtolower($row1->city));
			}
			$city = array_values(array_unique($city));
			$city_name = array_slice($city, 0, 3);
			$city_name_more = array_slice($city, 3, count($city));
			$more_city = '';
			if(count($city_name_more) > 0){
			    $more_city = "+".count($city_name_more)."more";
			}
			$data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
			$data[$i]['total_location'] = $toal_location;
	
			if($row->is_perpetual == 0)
			{
			    $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
			Left";
			    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
			}
			else
			{
			    $data[$i]['days_left'] = 'Perpetual';
			    $data[$i]['days'] = 'Perpetual';
			}
			
			$i++;
		    }
		    
		}
                
            }
	}
	//echo "<pre>";print_r($data);die;
        return $data;
    }
    // new isp changes done
    public function dashboard_expire_campaign_list(){
        $data = array();
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_brand_id = '$brandid' and created_by_location = '0'  AND is_deleted = '0' AND (status = '0' OR campaign_end_date < now()) AND offer_type != 'audit_program' ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                $j = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;
                $current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
                {
                    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
                }
                else{
                    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
                }
                $i++;
            }
	    // for audit
	    $query = $this->DB2->query("select cpo.status,cpo.is_perpetual,cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_brand_id = '$brandid' and created_by_location = '0'  AND is_deleted = '0'   AND offer_type = 'audit_program' ORDER BY cp_offer_id DESC ");
	    //echo "<pre>";print_r($query->result());die;
            foreach($query->result() as $row){
		$is_inside = 0;
		if($row->is_perpetual == '1' && $row->status == '0')
		{
		    $is_inside = 1;
		}
		else
		{
		    if($row->is_perpetual == '0' && (strtotime($row->campaign_end_date) < strtotime(date('Y-m-d')) || strtotime($row->campaign_end_date) == '' || $row->status = '0'))
		    {
			$is_inside = 1;
		    }
		}
		if($is_inside == '1')
		{
		    $data[$i]['offer_id'] = $row->cp_offer_id;
		    $data[$i]['campaign_name'] = $row->campaign_name;
		    $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
		    $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id AND cpol.is_deleted = '0') where cpol.cp_offer_id = '$row->cp_offer_id'
    AND wl.status = 1 AND wl.is_deleted = 0");
		    $toal_location = 0;
		    $city = array();
		    $j = 0;
		    $city = array();
		    foreach($get_location_detail->result() as $row1){
			$toal_location = $toal_location + 1;
			$city[] = ucwords(strtolower($row1->city));
		    }
		    $city = array_values(array_unique($city));
		    $city_name = array_slice($city, 0, 3);
		    $city_name_more = array_slice($city, 3, count($city));
		    $more_city = '';
		    if(count($city_name_more) > 0){
			$more_city = "+".count($city_name_more)."more";
		    }
		    $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
		    $data[$i]['total_location'] = $toal_location;
		    $current_time =  date("Y-m-d");
		    if(strtotime($row->campaign_end_date) == '')
		    {
			if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
			{
			    $data[$i]['days'] = "";
			}
			else{
			    $data[$i]['days'] = '';
			}
		    }
		    else
		    {
			if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
			{
			    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
			}
			else{
			    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
			}
		    }
		    
		    $i++;
		}
                
            }
	    
        }
	elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
            $location_uid = $this->session->userdata('location_uid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_location = '$location_uid'  AND (status = '0' OR is_deleted = '1'  OR campaign_end_date < now()) AND offer_type != 'audit_program' ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id) where cpol.cp_offer_id = '$row->cp_offer_id'
AND wl.status = 1 AND wl.is_deleted = 0");
                $toal_location = 0;
                $city = array();
                $j = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;
                $current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
                {
                    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
                }
                else{
                    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
                }
                $i++;
            }
	  
	  // for audit
	    $query = $this->DB2->query("select cpo.is_perpetual,cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from cp_offers as cpo where created_by_location = '$location_uid'  AND (status = '0' OR is_deleted = '1'  OR campaign_end_date < now()) AND offer_type = 'audit_program' ORDER BY cp_offer_id DESC ");
            foreach($query->result() as $row){
		$is_inside = 0;
		if($row->is_perpetual == '1' && $row->status == '0')
		{
		    $is_inside = 1;
		}
		else
		{
		    if($row->is_perpetual == '0' && (strtotime($row->campaign_end_date) < strtotime(date('Y-m-d')) || strtotime($row->campaign_end_date) == '' || $row->status = '0'))
		    {
			$is_inside = 1;
		    }
		}
		if($is_inside = '1')
		{
		    $data[$i]['offer_id'] = $row->cp_offer_id;
		    $data[$i]['campaign_name'] = $row->campaign_name;
		    $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
		    $get_location_detail = $this->DB2->query("select cpol.location_id, wl.city from cp_offers_location as cpol inner join wifi_location as wl on(wl.id = cpol.location_id AND cpol.is_deleted = '0') where cpol.cp_offer_id = '$row->cp_offer_id'
    AND wl.status = 1 AND wl.is_deleted = 0");
		    $toal_location = 0;
		    $city = array();
		    $j = 0;
		    $city = array();
		    foreach($get_location_detail->result() as $row1){
			$toal_location = $toal_location + 1;
			$city[] = ucwords(strtolower($row1->city));
		    }
		    $city = array_values(array_unique($city));
		    $city_name = array_slice($city, 0, 3);
		    $city_name_more = array_slice($city, 3, count($city));
		    $more_city = '';
		    if(count($city_name_more) > 0){
			$more_city = "+".count($city_name_more)."more";
		    }
		    $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
		    $data[$i]['total_location'] = $toal_location;
		    $current_time =  date("Y-m-d");
		    if(strtotime($row->campaign_end_date) == '')
		    {
			if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
			{
			    $data[$i]['days'] = "";
			}
			else{
			    $data[$i]['days'] = '';
			}
		    }
		    else
		    {
			if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
			{
			    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
			}
			else{
			    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
			}
		    }
		    
		    $i++;
		}
                
            }  
        }
	//echo "<pre>";print_r($data);die;
        return $data;
    }
    // new isp changes done
    public function wifilocation(){
        $data = array();
        $brandid = '';
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
            $brandid = $this->session->userdata('brandid');
        }
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $location_uid = $this->session->userdata('location_uid');
	    $query= $this->DB2->query("select id, location_name, city, region_id, location_type from wifi_location WHERE
	    is_deleted='0' AND
	    status = '1' AND  location_uid = '$location_uid'");
	    foreach($query->result() as $row){
		$data[] = $row;
	    }
	}else{
	    $provider_where = '';
	    if($this->session->userdata('provider_id')){
		$provider_id = $this->session->userdata('provider_id');
		if($provider_id != '0'){
				    $provider_where = " AND provider_id = '$provider_id'";	
			    }
		
	    }
	    $query= $this->DB2->query("select id, location_name, city, region_id, location_type from wifi_location WHERE
	    is_deleted='0' AND
	    status = '1' AND location_brand = 'sh' $provider_where");
	    foreach($query->result() as $row){
		$data[] = $row;
	    }
	}
        
        return $data;
    }
    // new isp changes done
     public function daily_people_reached($request){

        $locations =  $request->location_selected;
        $locations='"'.implode('", "', $locations).'"';
        //$locations =  explode(',', $_POST['location_selected']);
        $data = array();
        $query = $this->DB2->query("select count(distinct(user_id)) as total_user from cp_dashboard_analytics where location_id in (".$locations.") group by DATE(viewed_on)");
        $total_user = 0;
        $i = 1;
        if($query->num_rows() > 0){
            foreach($query->result()  as $row){
                $total_user = $total_user+$row->total_user;
                $i++;
            }
        }
        $total_user = ceil($total_user/$i);

        $query1 = $this->DB2->query("select count(distinct(user_id)) as total_user from cp_dashboard_analytics where location_id in (".$locations.") and gender = 'M' group by DATE(viewed_on)");
        $total_male_user = 0;
        if($query1->num_rows() > 0){
            foreach($query1->result()  as $row){
                $total_male_user = $total_male_user+$row->total_user;
            }
        }
        $total_male_user = ceil($total_male_user/$i);

        $query2 = $this->DB2->query("select count(distinct(user_id)) as total_user from cp_dashboard_analytics where location_id in (".$locations.") and gender = 'F' group by DATE(viewed_on)");
        $total_female_user = 0;
        if($query2->num_rows() > 0){
            foreach($query2->result()  as $row){
                $total_female_user = $total_female_user+$row->total_user;
            }
        }
        $total_female_user = ceil($total_female_user/$i);

        $data['people_daily'] = $total_user;
        $data['people_daily_male'] = $total_male_user;
        $data['people_daily_female'] = $total_female_user;
        $data['resultCode'] = 1;

        //echo "<pre>";print_r($data);

        return $data;
    }
    // new isp changes done
    public function brand_budget_detail($request){
        $data = array();
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
        }
        $offer_type = $request->offer_type;
        $query = $this->DB2->query("select current_balance, (SELECT sum(total_budget) FROM `cp_offers` WHERE created_by_brand_id = '$brand_id' and now() between campaign_start_date and campaign_end_date) as locked_budget from cp_brand_budget where brand_id = '$brand_id'");
        $brand_balance = 0;
        $locked_budget = 0;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $brand_balance = (int)$row['current_balance'];
            $locked_budget = $row['locked_budget'];
        }
        $query1 = $this->DB2->query("select * from cp_location_budget where offer_type = '$offer_type'");
        $avg_cost = 0;
        if($query1->num_rows() > 0){
            $row1 = $query1->row_array();
            $avg_cost = ($row1['star']+$row1['premium']+ $row1['mass']) / 3;
        }
        $data['resultCode'] = 1;
        $data['current_balance'] = number_format((float)$brand_balance, 2, '.', '');
        $data['locked_budget'] = number_format((float)$locked_budget, 2, '.', '');
        $data['avg_cost'] = number_format((float)$avg_cost, 2, '.', '');

        //echo "<pre>";print_r($data);die;
        return $data;
    }
    // new isp changes done
    public function offersstore_list(){
        $data = array();
        $brandid = '';
        if($this->session->userdata('brandid')){
            $brandid = $this->session->userdata('brandid');
        }
        $query= $this->DB2->query("select store_id, store_name, city, region_id from store WHERE is_deleted='0' AND
        brand_id = '$brandid'");
        foreach($query->result() as $row){
            $data[] = $row;
        }
        return $data;
    }
    // new isp changes done
     public function brand_name_logo($request){
        $data = array();
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == ''){
            if(isset($request->brand_id) && $request->brand_id != ''){
                $brandid = $request->brand_id;
            }
            else{
                $brandid = $this->session->userdata('brandid');
            }

            $query = $this->DB2->query("select b.brand_name, b.original_logo_logoimage, b.original_big_logo
from brand as b WHERE brand_id = '$brandid'");
            if($query->num_rows()>0){
                foreach($query->result() as $row)
                $data['brand_name'] = strtoupper($row->brand_name);
                if($this->session->userdata('isp_login')&& $this->session->userdata('isp_login') == 'yes'){
                     $data['brand_logo'] = CLOUD_IMAGEPATH.'isp_logo/logo/'.$row->original_logo_logoimage;
                    $data['brand_banner'] = '';
                }else{
                    $data['brand_logo'] = IMAGEPATHURL.'brand/logo/logo/'.$row->original_logo_logoimage;
                    $data['brand_banner'] = IMAGEPATHURL.'brand/logo/logo/'.$row->original_big_logo;
                }
               
                $data['resultCode'] = '1';
            }else{
                $data['resultCode'] = '0';
            }
        }elseif($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $location_uid = $this->session->userdata('location_uid');
	    $brand_logo = '';
	    $get_loc_info = $this->DB2->query("select wl.user_email, wl.isp_uid, wlct.logo_image from wifi_location as wl left join wifi_location_cptype wlct on (wl.location_uid = wlct.uid_location) where wl.location_uid = '$location_uid'");
	    if($get_loc_info->num_rows() > 0){
		    $row = $get_loc_info->row_array();
		    $user_email = $row['user_email'];
		    if(isset($row['logo_image']) && $row['logo_image'] != ''){
			$brand_logo = CLOUD_IMAGEPATH."isp_location_logo/logo/".$row['logo_image'];
		    }
		    if($brand_logo  == ''){
			$isp_uid = $this->session->userdata('provider_id');
			$query = $this->db->query("select small_image,logo_image from sht_isp_detail where status='1' AND isp_uid='".$isp_uid."'");
			if($query->num_rows() > 0){
			    $rowdata = $query->row();
			    $brand_logo =  CLOUD_IMAGEPATH."isp_logo/logo/".$rowdata->logo_image;
			}
		    }
	    }
	    $data['resultCode'] = '1';
	    $data['brand_logo'] = $brand_logo;
            $data['brand_banner'] = $brand_logo;
	}else{
            $data['resultCode'] = '0';
        }


        return $data;
    }
    // new isp changes done
     public function add_campaign_offer($request){
        $data = array();
        $voucher_code_array = array();
        if (isset($_FILES) && !empty($_FILES['excelfile']['name'])) {
            $filename = $_FILES['excelfile']['name'];
            $allowed =  array('xlsx', 'xls');
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Please select valid excel file';
                return $data;
            }
            $base_url = 'assets/files/';
            move_uploaded_file($_FILES['excelfile']['tmp_name'], $base_url . $filename);
            $file = 'assets/files/'. $filename;
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet

            for ($i = 2; $i <= $arrayCount; $i++) {
                $voucher_code_excel = trim($allDataInSheet[$i]["A"]);
                if($voucher_code_excel != ''){
                    $voucher_code_array[$i] = $voucher_code_excel;
                }
            }
            if (empty($voucher_code_array)) {
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Excel can not be empty';
                return $data;
            }
            if(count($voucher_code_array) != count(array_unique($voucher_code_array))){
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Coupon Code should be unique';
                return $data;
            }
        }

        //echo "<pre>";print_r($_POST);die;
        $is_shouut_offer_check = $_POST['is_shouut_offer_check'];
        $offer_brand_id = '';
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
            $offer_brand_id = $this->session->userdata('brandid');
        }
        $video_duration = ceil($_POST['video_duration']);
        $brand_title = $_POST['brand_title'];
        $offer_type = $_POST['offer_type'];
        $campaign_name = $_POST['campaign_name'];
        $add_offer_exclusive_to_shouut = $_POST['add_offer_exclusive_to_shouut'];
        $age_group = $_POST['age_group'];
        $gender = $_POST['gender'];
        $active_between = $_POST['active_between'];
        $campaign_start_date = $_POST['campaign_start_date'];
        //$campaign_start_time = $_POST['campaign_start_time'];
        $campaign_start_time = "00:00";
        $start_date = date('Y-m-d H:i:s', strtotime("$campaign_start_date $campaign_start_time"));
        $campaign_end_date = $_POST['campaign_end_date'];
        //$campaign_end_time = $_POST['campaign_end_time'];
        $campaign_end_time = "23:55";
        $end_date = date('Y-m-d H:i:s', strtotime("$campaign_end_date $campaign_end_time"));
        $campaign_day_to_run = ($_POST['campaign_day_to_run']=="")?"all":$_POST['campaign_day_to_run'];
        $campaign_device = ($_POST['campaign_device']=="")?"all":$_POST['campaign_device'];
	$created_by_location = 0;
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $total_budget = '100000';
	    $created_by_location = $this->session->userdata('location_uid');
	}else{
	    $total_budget = $_POST['total_budget'];
	}
        
        $mass_budget = $_POST['mass_budget'];
        $premium_budget = $_POST['premium_budget'];
        $star_budget = $_POST['star_budget'];
        $offer_title = $_POST['offer_title'];
        $offer_desc = $_POST['offer_desc'];
        $voucher_type = $_POST['voucher_type'];
        $voucher_static_type = $_POST['voucher_static_type'];
        $voucher_code = $_POST['voucher_code'];
        $voucher_expiry = '';
        if($_POST['voucher_expiry'] != ''){
            $voucher_expiry = date('Y-m-d H:i:s', strtotime($_POST['voucher_expiry']));
        }
        $flash_time = '';
        if($_POST['flash_start_date'] != '' && $_POST['flash_start_time'] != ''){
            $flash_start_date = $_POST['flash_start_date'];
            $flash_start_time = $_POST['flash_start_time'];
            $flash_time = date('Y-m-d H:i:s', strtotime("$flash_start_date $flash_start_time"));
        }


        $captcha_accuracy_level = $_POST['captcha_accuracy_level'];
        $captcha_text = $_POST['captcha_text'];
        $term_and_condition = $_POST['term_and_condition'];
        $incentive_desc = $_POST['incentive_desc'];
        $brand_logo_as_banner = $_POST['brand_logo_as_banner'];
        $total_voucher = $_POST['total_voucher'];
        $redirect_url = $_POST['redirect_url'];
        $locations =  explode(',', $_POST['location_selected']);
        $stores = array_filter(explode(',', $_POST['store_selected']));
        //$appurl = array_filter(explode(',', $_POST['appurl']));
        $android_app_url = $_POST['android_app_url'];
        $ios_app_url = $_POST['ios_app_url'];
        $poll_question = array_filter(explode(',', $_POST['poll_question']));
	$scratch_offer = array_filter(explode(',', $_POST['scratch_offer']));
        $original = '';
        $logo = '';
        $small = '';
        $medium = '';
        $large = '';
        $video_path = '';
        if (isset($_FILES) && !empty($_FILES['UploadedFile']['name'])) {
            if($offer_type == 'video_add'){
                $filename = $_FILES['UploadedFile']['name'];
                $tmp = $_FILES['UploadedFile']['tmp_name'];
                $path = "assets/campaign/videos/";
                $amazonpath=AMAZONPATH.'campaign/videos/';
                $flash_logo_name = $this->resize_video($path,$tmp,$filename,$amazonpath);
                $video_path = $flash_logo_name;

            }
            else{
                $filename = $_FILES['UploadedFile']['name'];
                $tmp = $_FILES['UploadedFile']['tmp_name'];
                $path = 'assets/campaign/images/';
                $amazonpath=AMAZONPATH.'campaign/images/';
                $flash_logo_name = $this->resize_image($path,$tmp,$filename,$amazonpath);
                $original = $flash_logo_name[0];
                $logo = $flash_logo_name[1];
                $small = $flash_logo_name[2];
                $medium = $flash_logo_name[3];
                $large = $flash_logo_name[4];
            }

        }
        $original_poll = '';
        $logo_poll = '';
        $small_poll = '';
        $medium_poll = '';
        $large_poll = '';
        if (isset($_FILES) && !empty($_FILES['poll_logo']['name'])) {
            $filename = $_FILES['poll_logo']['name'];
            $tmp = $_FILES['poll_logo']['tmp_name'];
            $path = 'assets/campaign/poll_logo/';
            $amazonpath=AMAZONPATH.'campaign/poll_logo/';
            $flash_logo_name = $this->resize_image($path,$tmp,$filename,$amazonpath);
            $original_poll = $flash_logo_name[0];
            $logo_poll = $flash_logo_name[1];
            $small_poll = $flash_logo_name[2];
            $medium_poll = $flash_logo_name[3];
            $large_poll = $flash_logo_name[4];
        }


        $original_video_thumb = '';
        $logo_video_thumb = '';
        $small_video_thumb = '';
        $medium_video_thumb = '';
        $large_video_thumb = '';
        if (isset($_FILES) && !empty($_FILES['video_thumb']['name'])) {
            $filename = $_FILES['video_thumb']['name'];
            $tmp = $_FILES['video_thumb']['tmp_name'];
            $path = 'assets/campaign/video_thumb/';
            $amazonpath=AMAZONPATH.'campaign/video_thumb/';
            $video_thumb_name = $this->resize_image($path,$tmp,$filename,$amazonpath);
            $original_video_thumb = $video_thumb_name[0];
            $logo_video_thumb = $video_thumb_name[1];
            $small_video_thumb = $video_thumb_name[2];
            $medium_video_thumb = $video_thumb_name[3];
            $large_video_thumb = $video_thumb_name[4];
        }

        if($voucher_static_type == 'multiple'){
            $insert_offer = $this->DB2->query("insert into cp_offers(created_by_location,brand_id, created_by_brand_id, offer_type,campaign_name, offer_exclusive_to_shouut, age_group, gender, active_between, campaign_start_date,
                                         campaign_end_date, campaign_day_to_run, total_budget, mass_budget, premium_budget, star_budget, offer_title, offer_desc, term_and_condition, voucher_type, voucher_static_type, voucher_expiry, total_voucher,
                                         flash_start_date,brand_logo_as_banner,
                                         captcha_text, captcha_accuracy_level, redirect_url, incentive_desc, image_original,
                                         image_logo, image_small, image_medium, image_large, video_path, status,
                                         is_deleted, created_on, android_appurl, ios_appurl, platform_filter,
                                          poll_brand_title,poll_original, poll_large, poll_medium, poll_small, poll_logo, video_duration, video_thumb_original, video_thumb_large,
                                          video_thumb_medium, video_thumb_small,video_thumb_logo,is_web_offer) values(
                                         '$created_by_location','$offer_brand_id','$brand_id','$offer_type','$campaign_name', '$add_offer_exclusive_to_shouut', '$age_group', '$gender', '$active_between', '$start_date',
                                         '$end_date', '$campaign_day_to_run', '$total_budget', '$mass_budget', '$premium_budget', '$star_budget', '$offer_title', '$offer_desc', '$term_and_condition', '$voucher_type', '$voucher_static_type', '$voucher_expiry', '$total_voucher',
                                         '$flash_time','$brand_logo_as_banner',
                                         '$captcha_text', '$captcha_accuracy_level', '$redirect_url', '$incentive_desc', '$original',
                                         '$logo', '$small', '$medium', '$large', '$video_path','1',
                                         '0', now(), '$android_app_url', '$ios_app_url', '$campaign_device',
                                          '$brand_title' , '$original_poll', '$large_poll', '$medium_poll', '$small_poll', '$logo_poll', '$video_duration',
                                         '$original_video_thumb','$large_video_thumb','$medium_video_thumb',
                                          '$small_video_thumb','$logo_video_thumb', '$is_shouut_offer_check')");

        }
        else{
            $insert_offer = $this->DB2->query("insert into cp_offers(created_by_location,brand_id, created_by_brand_id, offer_type,campaign_name, offer_exclusive_to_shouut, age_group, gender, active_between, campaign_start_date,
                                         campaign_end_date, campaign_day_to_run, total_budget, mass_budget, premium_budget, star_budget, offer_title, offer_desc, term_and_condition, voucher_type, voucher_static_type, voucher_code, voucher_expiry, total_voucher,
                                         flash_start_date,brand_logo_as_banner,
                                         captcha_text, captcha_accuracy_level, redirect_url, incentive_desc, image_original,
                                         image_logo, image_small, image_medium, image_large, video_path, status,
                                         is_deleted, created_on, android_appurl, ios_appurl, platform_filter,
                                          poll_brand_title,poll_original, poll_large, poll_medium, poll_small, poll_logo, video_duration, video_thumb_original, video_thumb_large,
                                          video_thumb_medium, video_thumb_small,video_thumb_logo,is_web_offer) values(
                                         '$created_by_location','$offer_brand_id','$brand_id','$offer_type','$campaign_name', '$add_offer_exclusive_to_shouut', '$age_group', '$gender', '$active_between', '$start_date',
                                         '$end_date', '$campaign_day_to_run', '$total_budget', '$mass_budget', '$premium_budget', '$star_budget', '$offer_title', '$offer_desc', '$term_and_condition', '$voucher_type', '$voucher_static_type', '$voucher_code', '$voucher_expiry', '$total_voucher',
                                         '$flash_time','$brand_logo_as_banner',
                                         '$captcha_text', '$captcha_accuracy_level', '$redirect_url', '$incentive_desc', '$original',
                                         '$logo', '$small', '$medium', '$large', '$video_path','1',
                                         '0', now(), '$android_app_url', '$ios_app_url', '$campaign_device',
                                          '$brand_title' , '$original_poll', '$large_poll', '$medium_poll', '$small_poll', '$logo_poll', '$video_duration',
                                         '$original_video_thumb','$large_video_thumb','$medium_video_thumb',
                                          '$small_video_thumb','$logo_video_thumb', '$is_shouut_offer_check')");

        }
        $cp_offer_id = $this->DB2->insert_id();
        // add multiple coupon
        if(count($voucher_code_array) > 0){
            $voucher_code =  $voucher_code_array;
        }
        else{
            $voucher_code =  explode(',', $_POST['voucher_code']);
        }

        if($voucher_static_type == 'multiple'){
            foreach($voucher_code as $voucher_code1){
                $insert_voucher = "insert into cp_offer_coupon(cp_offer_id, coupon_code, is_used, created_on,
                     used_on) VALUES
                ('$cp_offer_id', '$voucher_code1','0', now(), '')";
                $query3 = $this->DB2->query($insert_voucher);
            }
        }
        //subtract budget from brand account
	// dont cut budget if location create offer
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $total_budget = '0';
	}else{
            $total_budget = '0';
        }
        $subtract_budget = $this->DB2->query("update cp_brand_budget set current_balance = (current_balance -
        $total_budget)  WHERE brand_id = '$brand_id'");

        //insert in cp_offer_location table
        foreach($locations as $location_id1){
            $insert_location = "insert into cp_offers_location(cp_offer_id, location_id) VALUES
                ('$cp_offer_id','$location_id1')";
            $query3 = $this->DB2->query($insert_location);
        }
        //insert in cp_offer_store table
        if(!empty($stores)) {
            foreach($stores as $store_id1){
                $insert_store = "insert into cp_offers_store(cp_offer_id, store_id) VALUES
                   ('$cp_offer_id','$store_id1')";
                $query3 = $this->DB2->query($insert_store);
            }
        }

        //insert in cp_offer_app_url table
        /*if(!empty($appurl)) {
           foreach($appurl as $appurl_1){
           $insert_store = "insert into cp_offers_app_url(cp_offer_id, app_url) VALUES
               ('$cp_offer_id','$appurl_1')";
           $query3 = $this->db->query($insert_store);
       }
     }*/
	if(!empty($scratch_offer)) {
	    
            foreach($scratch_offer as $scratch_offer1){
                $offer = ''; $visibility = ''; 
                $offer_array = array_filter(explode('::+::', $scratch_offer1));
                if(!empty($offer_array)) {
                    $offer = $offer_array[0];
                    $visibility = $offer_array[1];
                    $insert_question = "insert into cp_scratch_offer(offer_id, scratch_offer_title, visibility)
                        VALUES ('$cp_offer_id','$offer', '$visibility')";
                    $query3 = $this->DB2->query($insert_question);
                }
            }
        }
        //insert in cp_offer_question table
        if(!empty($poll_question)) {
            foreach($poll_question as $poll_question1){
                $question = ''; $option1 = ''; $option2 = ''; $option3 = ''; $option4 = '';
                $questions = array_filter(explode('::+::', $poll_question1));
                if(!empty($questions)) {
                    $qeuestion = $questions[0];
                    $option1 = $questions[1];
                    $option2 = $questions[2];
                    $option3 = $questions[3];
                    $option4 = $questions[4];
                    $insert_question = "insert into cp_offers_question(cp_offer_id, question, option1, option2, option3, option4)
                        VALUES ('$cp_offer_id','$qeuestion', '$option1', '$option2', '$option3', '$option4')";
                    $query3 = $this->DB2->query($insert_question);
                }
            }
        }
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
        return $data;
    }
    // new isp changes done
    public function getExtension($str){
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }
    // new isp changes done
    public function resize_image($imagesDirectory, $tmp, $image_name,$amazonpath){
        $allowed_extenstion = array('jpg', 'jpeg', 'png');
        $filename = stripslashes($image_name);

        $extension = $this->getExtension($filename);
        $extension = strtolower($extension);
        $filename = date('Ymd') . time() . rand() . '.' . $extension;
        if (!in_array($extension, $allowed_extenstion)) {
            return 'invalid extension file.';
        } else {
            move_uploaded_file($tmp, $imagesDirectory . $filename);

            $image_details = getimagesize($imagesDirectory . $filename);
            list($width, $height) = $image_details;

            if (!file_exists(realpath($imagesDirectory . 'logo'))) {
                mkdir($imagesDirectory . 'logo', 0777, TRUE);
            }
            if (!file_exists(realpath($imagesDirectory . 'small'))) {
                mkdir($imagesDirectory . 'small', 0777, TRUE);
            }
            if (!file_exists(realpath($imagesDirectory . 'medium'))) {
                mkdir($imagesDirectory . 'medium', 0777, TRUE);
            }
            if (!file_exists(realpath($imagesDirectory . 'large'))) {
                mkdir($imagesDirectory . 'large', 0777, TRUE);
            }
            if (!file_exists(realpath($imagesDirectory . 'original'))) {
                mkdir($imagesDirectory . 'original', 0777, TRUE);
            }

            $fname = $imagesDirectory . 'original/' . $width . '_' . $filename;
            $fname1 = $imagesDirectory . 'logo/100_' . $filename;
            $fname2 = $imagesDirectory . 'small/320_' . $filename;
            $fname3 = $imagesDirectory . 'medium/640_' . $filename;
            $fname4 = $imagesDirectory . 'large/960_' . $filename;


            $famazonname = $amazonpath.'original/'.$width.'_'. $filename;
            $famazonname1 = $amazonpath.'logo/100_'. $filename;
            $famazonname2 = $amazonpath.'small/320_'. $filename;
            $famazonname3 = $amazonpath.'medium/640_'. $filename;
            $famazonname4 = $amazonpath.'large/960_'. $filename;

            $channel_exists = array_key_exists('channels', $image_details);//$image_details['channels'];
            $logoimage = '';
            $smallimage = '';
            $mediumimage = '';
            $largeimage = '';

            /*if (!$channel_exists) {
                rename($imagesDirectory . $filename, $fname);
            } else {*/
            if ($extension == "jpg" || $extension == "jpeg") {
                $src = imagecreatefromjpeg($imagesDirectory . $filename);
            } else if ($extension == "png") {
                $src = imagecreatefrompng($imagesDirectory . $filename);
            } else {
                $src = imagecreatefromgif($imagesDirectory . $filename);
            }
            rename($imagesDirectory . $filename, $fname);
            //echo $src;
            $newwidth_100 = 100;
            if ($newwidth_100 <= $width) {
                $newheight_100 = ($height / $width) * $newwidth_100;
                $tmp_100 = imagecreatetruecolor($newwidth_100, $newheight_100);
                imagecopyresampled($tmp_100, $src, 0, 0, 0, 0, $newwidth_100, $newheight_100, $width, $height);
                imagejpeg($tmp_100, $fname1, 100);
                $logoimage = '100_' . $filename;
            }
            //die;
            $newwidth_320 = 320;
            if ($newwidth_320 <= $width) {
                $newheight_320 = ($height / $width) * $newwidth_320;
                $tmp_320 = imagecreatetruecolor($newwidth_320, $newheight_320);
                imagecopyresampled($tmp_320, $src, 0, 0, 0, 0, $newwidth_320, $newheight_320, $width, $height);
                imagejpeg($tmp_320, $fname2, 100);
                $smallimage = '320_' . $filename;
            }

            $newwidth_640 = 640;
            if ($newwidth_640 <= $width) {
                $newheight_640 = ($height / $width) * $newwidth_640;
                $tmp_640 = imagecreatetruecolor($newwidth_640, $newheight_640);
                imagecopyresampled($tmp_640, $src, 0, 0, 0, 0, $newwidth_640, $newheight_640, $width, $height);
                imagejpeg($tmp_640, $fname3, 100);
                $mediumimage = '640_' . $filename;
            }

            $newwidth_960 = 960;
            if ($newwidth_960 <= $width) {
                //echo $newheight_960.'=>'.$width.' yes'; die;
                $newheight_960 = ($height / $width) * $newwidth_960;
                $tmp_960 = imagecreatetruecolor($newwidth_960, $newheight_960);
                imagecopyresampled($tmp_960, $src, 0, 0, 0, 0, $newwidth_960, $newheight_960, $width, $height);
                imagejpeg($tmp_960, $fname4, 100);
                $largeimage = '960_' . $filename;
            }


            $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
            $this->s3->putObjectFile($fname1, bucket , $famazonname1, S3::ACL_PUBLIC_READ) ;
            $this->s3->putObjectFile($fname2, bucket , $famazonname2, S3::ACL_PUBLIC_READ) ;
            $this->s3->putObjectFile($fname3, bucket , $famazonname3, S3::ACL_PUBLIC_READ) ;
            $this->s3->putObjectFile($fname4, bucket , $famazonname4, S3::ACL_PUBLIC_READ) ;
            unlink($fname);
            unlink($fname1);
            unlink($fname2);

            unlink($fname3);
            unlink($fname4);
            //}
            $image_name = array($width."_".$filename, $logoimage, $smallimage, $mediumimage ,$largeimage);
            return $image_name;

        }

    }
    // new isp changes done
    public function resize_video($imagesDirectory, $tmp, $image_name, $amazonpath){
        $allowed_extenstion = array('mp4', 'flv', 'mpeg', 'avi');
        $filename = stripslashes($image_name);

        $extension = $this->getExtension($filename);
        $extension = strtolower($extension);
        $filename = date('Ymd') . time() . rand() . '.' . $extension;
        if (!in_array($extension, $allowed_extenstion)) {
            return 'invalid extension file.';
        } else {
            move_uploaded_file($tmp, $imagesDirectory . $filename);

            $fname = $imagesDirectory.$filename;
            $famazonname = $amazonpath.$filename;
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
            unlink($fname);
            return $filename;

        }
    }
    
    // nes isp changes done
    public function store_list(){
        $data = array();
        if($this->session->userdata('brandid')){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select  store.*,store_region.region_name, store_type.store_type from store
INNER JOIN  store_region ON (store.region_id = store_region.region_id) INNER JOIN store_type ON (store.store_type_id = store_type
.store_type_id)
WHERE
store.brand_id = '$brandid' AND store.is_deleted = '0' ORDER BY store.store_id DESC ");
            foreach($query->result() as $row){
                $data[] = $row;
            }
        }
        return $data;
    }
    // new isp changes done
    public function store_region(){
        $data = array();
        $query= $this->DB2->query("select * from store_region");
        foreach($query->result() as $row){
            $data[] = $row;
        }
        return $data;
    }
    // new isp changes done
    public function store_type(){
        $data = array();
        $query= $this->DB2->query("select * from store_type");
        foreach($query->result() as $row){
            $data[] = $row;
        }
        return $data;
    }
    // new isp changes done
    public function state_list(){
        $data = array();
        $query= $this->db->query("select id,state from sht_states");
        $i = 0;
        foreach($query->result() as $row){
            //$data[] = $row;
            $data[$i]['state_id'] = $row->id;
            $data[$i]['state_name'] = $row->state;
            $i++;
        }
        return $data;
    }
    // isp new changes done
    public function checkstorename($request){
        $data = array();
        $store_name = $request->store_name;
        $query = $this->DB2->get_where('store', array('is_deleted' => '0', 'store_name' => $store_name));
        //print_r($query);
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
        }
        else{
            $data['resultCode'] = '0';
        }
        return $data;
    }
    // new isp changes done
     public function add_store($request){
        /*{"region_id":"1","store_type_id":"2","store_name":"de","geo_lcoation":"de","store_address":"delhi","city"
:"delhi","state_name":"Karnataka","pin_code":"234","mobile_number":"434","alt_number":"3333"}*/
        $data = array();
        if(!isset($request->region_id) || !isset($request->store_type_id) || !isset($request->store_name) || !isset
            ($request->geo_lcoation) ||!isset($request->lat)|| !isset($request->long)|| !isset($request->placeid) ||
            !isset
            ($request->store_address)
            || !isset
            ($request->city)
            || !isset
            ($request->state_name) || !isset($request->pin_code) || !isset($request->mobile_number)){

            $data['resultCode'] = '0';
            $data['resultMessage'] =  'Please fill all the fields';

        }else{
            $alt_number = '';
            if(isset($request->alt_number)){
                $alt_number = $request->alt_number;
            }
            $brandid = '';
            if($this->session->userdata('brandid')){
                $brandid = $this->session->userdata('brandid');
            }
            $lat = (float)$request->lat ;
            $long = (float)$request->long;
            $store_name = mysql_real_escape_string($request->store_name);
            $geo_address = mysql_real_escape_string($request->geo_lcoation);
            $store_address = mysql_real_escape_string($request->store_address);
            $city = mysql_real_escape_string($request->city);
            $query = $this->DB2->query("insert into store (brand_id, region_id, store_name, geo_address, latitude, longitude, loc, placeid, address_1, city,
pin,
mobile_number,
 alternative_number, state, store_type_id, status, is_deleted, created_on) VALUES ('$brandid',
 '$request->region_id', '$store_name', '$geo_address', '$request->lat',
 '$request->long', POINT($lat, $long), '$request->placeid',
 '$store_address',
 '$city', '$request->pin_code', '$request->mobile_number', '$alt_number',
 '$request->state_name', '$request->store_type_id', '1', '0', now())");
            $data['resultCode'] = '1';
            $data['resultMessage'] =  'Success';
        }
        return $data;
    }
    // new isp changes done
    public function store_status($request){
        $data = array();
        $storeid = $request->store_id;
        $check = $this->DB2->query("select status from store WHERE store_id = '$storeid'");
        if($check->num_rows() > 0){
            $row = $check->row_array();
            $stat = '';
            $status = $row['status'];if($status == 0){
                $stat = '1';
            }
            else{
                $stat = '0';
            }
            $query= $this->DB2->query("update store set status = '$stat' WHERE store_id = '$storeid'");
        }
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
        return $data;
    }
    // new isp changes done
    public function checkstorename_edit($request){
        $data = array();
        $store_name = $request->store_name;
        $store_id = $request->store_id;
        $this->DB2->where('store_id !=', $store_id);
        $query = $this->DB2->get_where('store', array('is_deleted' => '0', 'store_name' => $store_name ));
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
        }
        else{
            $data['resultCode'] = '0';
        }
        return $data;
    }
    // new isp changes done
    public function update_store($request){
        /*{"region_id":"1","store_type_id":"2","store_name":"de","geo_lcoation":"de","store_address":"delhi","city"
:"delhi","state_name":"Karnataka","pin_code":"234","mobile_number":"434","alt_number":"3333"}*/
        $data = array();
        if(!isset($request->region_id) || !isset($request->store_type_id) || !isset($request->store_name) || !isset
            ($request->geo_lcoation) || !isset($request->store_address) || !isset($request->city) || !isset
            ($request->state_name) || !isset($request->pin_code) || !isset($request->mobile_number)){
            $data['resultCode'] = '0';
            $data['resultMessage'] =  'Please fill all the fields';

        }else{
            $alt_number = '';
            if(isset($request->alt_number)){
                $alt_number = $request->alt_number;
            }
            $lat = (float)$request->lat ;
            $long = (float)$request->long;
            $store_name = mysql_real_escape_string($request->store_name);
            $geo_address = mysql_real_escape_string($request->geo_lcoation);
            $store_address = mysql_real_escape_string($request->store_address);
            $city = mysql_real_escape_string($request->city);
            $query = $this->DB2->query("update store set region_id = '$request->region_id',
            store_name='$store_name', geo_address='$geo_address',
            address_1='$store_address', city='$city', pin= '$request->pin_code',
            mobile_number='$request->mobile_number', alternative_number='$alt_number', state='$request->state_name',
            store_type_id = '$request->store_type_id',latitude = '$request->lat', longitude =
            '$request->long', placeid = '$request->placeid',loc = POINT($lat, $long),
            modified_on =
             now()
            WHERE
            store_id =
            '$request->store_id'");
            $data['resultCode'] = '1';
            $data['resultMessage'] =  'Success';
        }
        return $data;
    }
    // new isp changes done
     public function delete_store($request){
        $data = array();
        $store_id = $request->store_id;
        /*$check_event = $this->DB2->query("select event_id from get_event_store_rel WHERE store_id = '$store_id'");
        //query to check event relete to selected store
        if($check_event->num_rows() > 0){
            foreach($check_event->result() as $row){
                echo $event_id = $row->event_id;
                $check_store = $this->DB2->query("select COUNT(*) as total_store from get_event_store_rel WHERE
                event_id = '$event_id'"); //query to check number of store of this event
                $num_store = $check_store->row_array();
                if($num_store['total_store'] > 1){//if event run on more then one store

                }else{//delete event
                    $delete_event = $this->DB2->query("update get_event set is_deleted = '1', modified_on = now() WHERE event_id='$event_id'");
                }
            }
        }*/
        $delete_store = $this->DB2->query("update store set is_deleted='1', modified_on = now() WHERE store_id = '$store_id'");
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
    }
     // new isp changes done
    public function setcampaignId($request){
        $data = array();
        $offer_id = $request->offer_id;
        $this->session->set_userdata('campaignid', $offer_id);
        $query = $this->DB2->query("select status, is_deleted, campaign_end_date from cp_offers WHERE cp_offer_id =
         '$offer_id'");
        $add_location = 0;
        $stop = 0;
        $pause = 0;
        $play = 0;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $is_live = 0;
            if(strtotime(date('Y-m-d H:i:s')) < strtotime($row['campaign_end_date'])){
                $is_live = 1;
            }
            if($row['status'] ==1 && $row['is_deleted'] == 0 && $is_live == 1){
                $add_location = 1;
                $stop = 1;
                $pause = 1;
            }
            if($row['status'] ==0 && $row['is_deleted'] == 0 && $is_live == 1){
                $play = 1;
            }
        }

        $data['resultCode'] = '1';
        $data['add_location'] = $add_location;
        $data['stop'] = $stop;
        $data['pause'] = $pause;
        $data['play'] = $play;
        return $data;

    }
    // new isp changes done
    public function get_campaign_visitor_detail($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }


        $data = array();
        $query = $this->DB2->query("select user_id, viewed_on, is_redirected from cp_dashboard_analytics WHERE  DATE(viewed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter  ORDER BY id desc");
        $i = 0;
        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
               
                $user_id = $row->user_id;
                $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                    $row_user = $get_user->row_array();
                     $data['users'][$i]['s_no'] = $i+1;
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->viewed_on) );
                    $gender = $row_user['gender'];
                    $gender = trim($gender);
                    if($gender != ''){
                        if($gender[0] == 'm' || $gender[0] == 'M'){
                            $gender = 'Male';
                        }
                        elseif($gender[0] == 'f' || $gender[0] == 'F'){
                            $gender = 'Female';
                        }
                        else{
                            $gender = '';
                        }
                    }
                    else{
                        $gender = '';
                    }
                    $data['users'][$i]['gender'] = $gender;
                    $redirected = 'No';
                    if($row->is_redirected == '1'){
                        $redirected = 'Yes';
                    }
                    $data['users'][$i]['redirected'] = $redirected;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;
    }
   



    // new isp changes done    
    public function campaign_graph_data($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
//age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
// time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;
        $total_redirect = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            /*$data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;
            $data[$j]['total_redirec'] = $row->total_redirect;
            $total_redirect = $total_redirect+$row->total_redirect;*/
            $data[$j]['actual_date'] = $row->viewed_on;
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $data[$j]['total_view'] = 0;
            $data[$j]['total_redirec'] = 0;

           /* $total_view_query = $this->db->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics cdal2
           WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $gender_where
            $age_group_filter $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->db->query("SELECT COUNT(*) AS total_redirect FROM cp_dashboard_analytics cdal2 WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on)  AND cdal2.cp_offer_id = '$campaign_id'
            AND cdal2.is_redirected = '1'   $gender_where $age_group_filter $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_redirec'] = $row1_total_view['total_redirect'];
            $total_redirect = $total_redirect+$row1_total_view['total_redirect'];*/
            $j++;
        }
        $query_get_data = $this->DB2->query("SELECT id,is_redirected,DATE(viewed_on) AS viewed_on
      FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter");
        foreach($query_get_data->result() as $query_get_data_row){
            $total_view = $total_view+1;
            $is_download = 0;
            if($query_get_data_row->is_redirected == '1'){
                $total_redirect = $total_redirect+1;
                $is_download = 1;
            }
            foreach($data as $key=>$value) {
                if($query_get_data_row->viewed_on == $value['actual_date']){
                    $data[$key]['total_view'] = $data[$key]['total_view']+1;
                    $data[$key]['total_redirec'] = $data[$key]['total_redirec']+$is_download;
                    break;
                }
            }
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_redirect'] = $total_redirect;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
   // new isp chagnes done
    public function pauseCampaign($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }

        $data = array();
        $query = $this->DB2->query("update cp_offers set status = '0' WHERE cp_offer_id = '$campaign_id'");
        $data['resultCode'] = 1;
        return $data;
    }
    // new isp changes done
    public function resumeCampaign($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }

        $data = array();
        $query = $this->DB2->query("update cp_offers set status = '1' WHERE cp_offer_id = '$campaign_id'");
        $data['resultCode'] = 1;
        return $data;
    }
    // new isp changes done
    public function stopCampaign($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }

        $data = array();
        $query = $this->DB2->query("update cp_offers set campaign_end_date = now() WHERE cp_offer_id = '$campaign_id'");
        $data['resultCode'] = 1;
        return $data;
    }
    // new isp changes done
    public function cp_offer_not_attached_location(){
        $data = array();
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    
	}else{
	    $campaign_id = '';
	    if($this->session->userdata('campaignid')){
		$campaign_id = $this->session->userdata('campaignid');
	    }
	    $location_ids_attached = array();
	    $get_attached_id = $this->DB2->query("select location_id from cp_offers_location WHERE cp_offer_id = '$campaign_id'");
	    foreach($get_attached_id->result() as $row){
		$location_ids_attached[] = $row->location_id;
	    }
	    $location_ids_attached='"'.implode('", "', $location_ids_attached).'"';
	    $provider_where = '';
	    if($this->session->userdata('provider_id')){
		$provider_id = $this->session->userdata('provider_id');
			    if($provider_id != '0'){
					    $provider_where = " AND provider_id = '$provider_id'";
			    }
		
	    }
	    $query= $this->DB2->query("select id, location_name, city, region_id, location_type from wifi_location WHERE is_deleted='0' AND
	    status = '1' AND location_brand = 'sh' and id NOT IN ($location_ids_attached) $provider_where");
	    foreach($query->result() as $row){
		$data[] = $row;
	    }
	}
        
        return $data;
    }
    // new isp changes done
    public function cp_offer_new_location($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
            //insert in cp_offer_location table
            foreach($request->location_selected as $location_id1){
                $insert_location = "insert into cp_offers_location(cp_offer_id, location_id) VALUES
                ('$campaign_id','$location_id1')";
                $query3 = $this->DB2->query($insert_location);
            }
        }
        $data = array();
        $data['resultCode'] = 1;
        return $data;
    }
    // new isp changes done
    public function campaign_graph_data_offerlisting($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

       /* $query = $this->db->query("SELECT DATE(viewed_on) AS viewed_on,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics_offer_listing cdal2 WHERE DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $time_filter $device_filter) AS total_view,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics_offer_detail cdal2 WHERE DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id ='$campaign_id'
               $time_filter $device_filter) AS total_detail_view
            FROM cp_dashboard_analytics_offer_listing cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");*/
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics_offer_listing cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        //echo "<pre>";print_r($query);die;
        $j = 0;
        $total_view = 0;
        $total_detail_view = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            /*$data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;
            $data[$j]['total_detail_view'] = $row->total_detail_view;
            $total_detail_view = $total_detail_view+$row->total_detail_view;*/
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics_offer_listing cdal2 WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_detail_view FROM cp_dashboard_analytics_offer_detail cdal2 WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id ='$campaign_id'
               $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_detail_view'] = $row1_total_view['total_detail_view'];
            $total_detail_view = $total_detail_view+$row1_total_view['total_detail_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_detail_view'] = $total_detail_view;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    // new isp changes done
        public function get_campaign_offerlisting_visitor_detail($request){

        $date_from = date("Y-m-d", strtotime( $request->date_from) );
        $date_to = date("Y-m-d", strtotime( $request->date_to) );
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        $data = array();
        $query = $this->DB2->query("select user_id, viewed_on from cp_dashboard_analytics_offer_detail WHERE  DATE(viewed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id'");
        $i = 0;

        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                
                $user_id = $row->user_id;
                $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                    $row_user = $get_user->row_array();
                    $data['users'][$i]['s_no'] = $i+1;
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->viewed_on) );
                    $gender = $row_user['gender'];
                    $gender = trim($gender);
                    if($gender[0] == 'm' || $gender[0] == 'M'){
                        $gender = 'Male';
                    }
                    elseif($gender[0] == 'f' || $gender[0] == 'F'){
                        $gender = 'Female';
                    }
                    else{
                        $gender = '';
                    }
                    $data['users'][$i]['gender'] = $gender;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;

    }

    // new isp changes done
     public function campaign_graph_data_bannerflash($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        /*$query = $this->db->query("SELECT DATE(viewed_on) AS viewed_on,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics_bannerflash_listing cdal2 WHERE
            DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $time_filter
            $device_filter) AS total_view,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics_bannerflash_detail cdal2 WHERE
              DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $time_filter
              $device_filter) AS total_detail_view,
             (SELECT COUNT(*) AS COUNT FROM cp_offer_reddem cdal2 WHERE
              DATE(cdal1.viewed_on)=DATE(cdal2.redeemed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $time_filter $device_filter) AS total_redeem
            FROM cp_dashboard_analytics_bannerflash_listing cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");*/
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics_bannerflash_listing cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");


        $j = 0;
        $total_view = 0;
        $total_detail_view = 0;
        $total_redeem = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            /*$data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;
            $data[$j]['total_detail_view'] = $row->total_detail_view;
            $total_detail_view = $total_detail_view+$row->total_detail_view;
            $data[$j]['total_redeem'] = $row->total_redeem;
            $total_redeem = $total_redeem+$row->total_redeem;*/
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics_bannerflash_listing cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_detail_view_query = $this->DB2->query("SELECT COUNT(*) AS total_detail_view FROM
            cp_dashboard_analytics_bannerflash_detail cdal2 WHERE
              DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $time_filter
              $device_filter");
            $row1_total_view = $total_detail_view_query->row_array();
            $data[$j]['total_detail_view'] = $row1_total_view['total_detail_view'];
            $total_detail_view = $total_detail_view+$row1_total_view['total_detail_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_redeem FROM cp_offer_reddem cdal2 WHERE
              DATE('$row->viewed_on')=DATE(cdal2.redeemed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $time_filter $device_filter");
            $row2_total_view = $total_redeem_query->row_array();
            $data[$j]['total_redeem'] = $row2_total_view['total_redeem'];
            $total_redeem = $total_redeem+$row2_total_view['total_redeem'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_detail_view'] = $total_detail_view;
        $return_array['total_redeem'] = $total_redeem;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    // new isp changes done
        public function get_campaign_bannerflash_visitor_detail($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }

        $data = array();
        /*$query = $this->db->query("select user_id, viewed_on from cp_dashboard_analytics_bannerflash_detail WHERE  DATE(viewed_on) BETWEEN
         '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id'");*/
        $query = $this->DB2->query("select user_id, redeemed_on, coupon_code from cp_offer_reddem WHERE  DATE(redeemed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter");
        $i = 0;

        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $user_id = $row->user_id;
                $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                    $row_user = $get_user->row_array();
                    $data['users'][$i]['s_no'] = $i+1;
                    
                    $data['users'][$i]['coupon_code'] = $row->coupon_code;
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->redeemed_on) );
                    $gender = $row_user['gender'];
                    $gender = trim($gender);
                    if($gender[0] == 'm' || $gender[0] == 'M'){
                        $gender = 'Male';
                    }
                    elseif($gender[0] == 'f' || $gender[0] == 'F'){
                        $gender = 'Female';
                    }
                    else{
                        $gender = '';
                    }
                    $data['users'][$i]['gender'] = $gender;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;

    }

    // new isp changes done
    public function campaign_graph_data_poll($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        /*$query = $this->db->query("SELECT DATE(viewed_on) AS viewed_on,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics cdal2 WHERE
            DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter) AS total_view
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");*/
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;

        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;

           /* $data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;*/
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;

        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }


    // new isp changes done
    public function campaign_graph_data_poll_question($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $data = array();

        $query = $this->DB2->query("select id,question, option1, option2, option3, option4 from cp_offers_question where cp_offer_id = '$campaign_id'");
        $i = 0;
        $question_record = array();
        foreach($query->result() as $row){
            $ansid = $row->id;
            $option1_count = 0;
            $option2_count = 0;
            $option3_count = 0;
            $option4_count = 0;
            $query1 = $this->DB2->query("select id, option_id from cp_poll_answer where question_id = '$ansid'");
            foreach($query1->result() as $row1){
                if($row1->option_id == 1){
                    $option1_count = $option1_count+1;
                }
                elseif($row1->option_id == 2){
                    $option2_count = $option2_count+1;
                }
                elseif($row1->option_id == 3){
                    $option3_count = $option3_count+1;
                }
                elseif($row1->option_id == 4){
                    $option4_count = $option4_count+1;
                }
            }
            $question_record[$i]['question'] = $row->question;
            $question_record[$i]['option1'] = $row->option1;
            $question_record[$i]['option1_count'] = $option1_count;
            $question_record[$i]['option2'] = $row->option2;
            $question_record[$i]['option2_count'] = $option2_count;
            $question_record[$i]['option3'] = $row->option3;
            $question_record[$i]['option3_count'] = $option3_count;
            $question_record[$i]['option4'] = $row->option4;
            $question_record[$i]['option4_count'] = $option4_count;
            $i++;
        }
        $data['resultCode'] = 1;
        $data['question_record'] = $question_record;
        return $data;
    }
    // new isp changes done
    public function campaign_graph_data_captcha($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();


        /*$query = $this->db->query("SELECT DATE(viewed_on) AS viewed_on,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics cdal2 WHERE
            DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $gender_where
            $age_group_filter $time_filter
            $device_filter) AS total_view,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics cdal2 WHERE DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
            AND cdal2.captcha_verified = '1'    $gender_where $age_group_filter $time_filter $device_filter) AS total_accurate
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");*/
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;
        $total_accurate = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            /* $data[$j]['total_view'] = $row->total_view;
           $total_view = $total_view+$row->total_view;
           $data[$j]['total_accurate'] = $row->total_accurate;
           $total_accurate = $total_accurate+$row->total_accurate;*/
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics cdal2 WHERE cdal2.cp_offer_id = '$campaign_id' AND
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) $gender_where $age_group_filter $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_accurate FROM cp_dashboard_analytics cdal2 WHERE cdal2.cp_offer_id = '$campaign_id'
            AND cdal2.captcha_verified = '1' AND DATE('$row->viewed_on')=DATE(cdal2.viewed_on) $gender_where $age_group_filter $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_accurate'] = $row1_total_view['total_accurate'];
            $total_accurate = $total_accurate+$row1_total_view['total_accurate'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_accurate'] = $total_accurate;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    // new isp changes done
    public function campaign_graph_data_appdownload($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");

        $j = 0;
        $total_view = 0;
        $total_redirect = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['actual_date'] = $row->viewed_on;
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $data[$j]['total_view'] = 0;
            $data[$j]['total_redirec'] = 0;

            /*$total_view_query = $this->db->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics cdal2
            WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $gender_where $age_group_filter $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->db->query("SELECT COUNT(*) AS total_redirect FROM cp_dashboard_analytics cdal2 WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
            AND cdal2.is_downloaded = '1'   $gender_where $age_group_filter $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_redirec'] = $row1_total_view['total_redirect'];
            $total_redirect = $total_redirect+$row1_total_view['total_redirect'];*/
            $j++;
        }
        $query_get_data = $this->DB2->query("SELECT id,is_downloaded,DATE(viewed_on) AS viewed_on
      FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter");
        foreach($query_get_data->result() as $query_get_data_row){
            $total_view = $total_view+1;
            $is_download = 0;
            if($query_get_data_row->is_downloaded == '1'){
                $total_redirect = $total_redirect+1;
                $is_download = 1;
            }
            foreach($data as $key=>$value) {
                if($query_get_data_row->viewed_on == $value['actual_date']){
                    $data[$key]['total_view'] = $data[$key]['total_view']+1;
                    $data[$key]['total_redirec'] = $data[$key]['total_redirec']+$is_download;
                    break;
                }
            }
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_redirect'] = $total_redirect;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );

        return $return_array;
    }

    // new isp changes done
        public function get_campaign_visitor_detail_appdownload($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        //echo $device_filter;

        $data = array();
        $query = $this->DB2->query("select user_id, viewed_on, is_redirected from cp_dashboard_analytics WHERE  DATE(viewed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' AND is_downloaded = '1'  $gender_where $age_group_filter
        $time_filter $device_filter  ORDER BY id desc");

        $i = 0;
        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $user_id = $row->user_id;
                $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                    $row_user = $get_user->row_array();
                    $data['users'][$i]['s_no'] = $i+1;
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->viewed_on) );
                    $gender = $row_user['gender'];
                    $gender = trim($gender);
                    if($gender != ''){
                        if($gender[0] == 'm' || $gender[0] == 'M'){
                            $gender = 'Male';
                        }
                        elseif($gender[0] == 'f' || $gender[0] == 'F'){
                            $gender = 'Female';
                        }
                        else{
                            $gender = '';
                        }
                    }
                    else{
                        $gender = '';
                    }
    
                    $data['users'][$i]['gender'] = $gender;
                    $redirected = 'No';
                    if($row->is_redirected == '1'){
                        $redirected = 'Yes';
                    }
                    $data['users'][$i]['redirected'] = $redirected;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;
    }

    // new isp changes done
    // get redeemable add graph data detail
    public function campaign_graph_data_redeemable($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y/m/d")) < strtotime($date_to)){
                    $date_to = date("Y/m/d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        /*$query = $this->db->query("SELECT DATE(viewed_on) AS viewed_on,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics cdal2 WHERE DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $gender_where $age_group_filter $time_filter $device_filter) AS total_view,
            (SELECT COUNT(*) AS COUNT FROM cp_offer_reddem cdal2 WHERE DATE(cdal1.viewed_on)=DATE(cdal2.redeemed_on) AND
             cdal2.cp_offer_id = '$campaign_id'
               $gender_where $age_group_filter $time_filter $device_filter) AS total_redeem
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");*/

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
             FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;
        $total_redeem = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            /*$data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;
            $data[$j]['total_redeem'] = $row->total_redeem;
            $total_redeem = $total_redeem+$row->total_redeem;*/

            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics cdal2 WHERE
            DATE
            ('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $gender_where
            $age_group_filter $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_redeem FROM cp_offer_reddem cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.redeemed_on) AND cdal2.cp_offer_id = '$campaign_id'
            $gender_where $age_group_filter $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_redeem'] = $row1_total_view['total_redeem'];
            $total_redeem = $total_redeem+$row1_total_view['total_redeem'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_redeem'] = $total_redeem;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }

    // new isp changes done
        public function get_campaign_redeemable_visitor_detail($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }

        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
// time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();
        $query = $this->DB2->query("select user_id, redeemed_on, coupon_code from cp_offer_reddem WHERE  DATE(redeemed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter");
        $i = 0;


        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $user_id = $row->user_id;
                $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                    $row_user = $get_user->row_array();
                    $data['users'][$i]['s_no'] = $i+1;
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->redeemed_on) );
                    $gender = $row_user['gender'];
                    $gender = trim($gender);
                    if($gender != ''){
                        if($gender[0] == 'm' || $gender[0] == 'M'){
                            $gender = 'Male';
                        }
                        elseif($gender[0] == 'f' || $gender[0] == 'F'){
                            $gender = 'Female';
                        }
                        else{
                            $gender = '';
                        }
                    }
                    else{
                        $gender = '';
                    }
                    $data['users'][$i]['gender'] = $gender;
    
                    $data['users'][$i]['coupon_code'] = $row->coupon_code;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;

    }

    // new isp chanes done
    public function additional_terms(){
        $data = array();
        if($this->session->userdata('userid') && $this->session->userdata('userid') != '') {
            $user_id = $this->session->userdata('userid');
            $query = $this->DB2->query("update brand_user set tc_read = '1' WHERE id = '$user_id'");
            $this->session->set_userdata('uid', uniqid('ang_'));
            $data['resultCode'] = '1';
            $data['resultMessage'] =  'Success';
        }  else{
            $data['resultCode'] = '0';
            $data['resultMessage'] =  'fail';
        }
    }
    public function signup($request){
        $data = array();
        if(!isset($request->email) || !isset($request->password) || !isset($request->cnf_password) || !isset
            ($request->mobile) || !isset($request->coupon)){
            $data['resultCode'] = '0';
            $data['resultMessage'] =  'Please fill all the fields';
        }else{
            $check_email = $this->DB2->query("select id from brand_user WHERE email = '$request->email'");
            if($check_email->num_rows() > 0){
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Email id already exist';
            }else{
                $email = $request->email;
                $new_pass = $request->password;
                $conf_pass = $request->cnf_password;
                $mobile = $request->mobile;
                $coupon_code = $request->coupon;
                if($new_pass == $conf_pass){
                    $check_email = $this->DB2->query("select brand_id from brand_user WHERE email = '$email'");
                    if($check_email->num_rows() > 0){
                        $data['resultCode'] = '0';
                        $data['resultMessage'] =  'Email Already exist';
                    }else{
                        $check_coupon = $this->DB2->query("select brand_id, validity from brand_coupon WHERE coupon_code = '$coupon_code' AND used = '0'");
                        if($check_coupon->num_rows()> 0){
                            $random_salt = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,2 ) .substr( md5( time() ), 1, 5);
                            $password = md5($new_pass.$random_salt);
                            $row = $check_coupon->row_array();
                            $brand_id = $row['brand_id'];
                            //check brand_already register
                            $brand_check = $this->DB2->query("select id from brand_user WHERE brand_id = '$brand_id'");
                            if($brand_check->num_rows() > 0){
                                $data['resultCode'] = '0';
                                $data['resultMessage'] =  'This Brand Already Register';
                            }else{
                                $validity = $row['validity'];
                                $expirydata = $this->date_validity($validity);
                                $insert = $this->DB2->query("insert into brand_user( email, salt, password, number, brand_id, coupon_code, created_on, expiry_date,
modified_on) VALUES ('$email', '$random_salt', '$password', '$mobile', '$brand_id', '$coupon_code',
 now(), '$expirydata', '')");

                                $user_id = $this->DB2->insert_id();
                                $updatecode = $this->DB2->query("update brand_coupon set used = '1', used_on = now() WHERE coupon_code = '$coupon_code'");

                                $updater_brand = $this->DB2->query("update brand set user_id ='$user_id'  WHERE brand_id = '$brand_id'");
                                $newdata = array(
                                    'uid'  => uniqid('ang_'),
                                    'userid'     => $user_id,
                                    'brandid' => $row['brand_id']
                                );
                                $this->session->set_userdata($newdata);
                                $data['resultCode'] = '1';
                                $data['resultMessage'] =  'Success';
                            }

                        }else{
                            $data['resultCode'] = '0';
                            $data['resultMessage'] =  'Coupon Code Not Valid';
                        }
                    }
                }else{
                    $data['resultCode'] = '0';
                    $data['resultMessage'] =  'Confirm password not correct';
                }
            }

        }

        return $data;
    }
    // new isp changes done
    public function brand_name(){
        $data = array();
        $query = $this->DB2->query("select brand_name, brand_id from brand WHERE is_deleted= '0'");
        if($query->num_rows()>0){
            foreach($query->result() as $row){
                $data[] = $row;
            }
        }
        return $data;
    }

    public function request_new_coupon($request){
        $data = array();
        if(!isset($request->email) || !isset($request->mobile) || !isset($request->business_name)){
            $data['resultCode'] = '0';
            $data['resultMessage'] =  'Please fill all the fields';
        }else{
            $insert = $this->DB2->query("insert into brand_coupon_request(email, mobile, brand_name, brand_id, requested_on) VALUES ('$request->email',
'$request->mobile', '$request->business_name', '$request->brand_id', now())");
            $email_coupon = $this->mail_coupon($request->email, $request->mobile, $request->business_name);
            $data['resultCode'] = '1';
            $data['resultMessage'] =  'Success';
        }

        return $data;
    }
    public function mail_coupon($email_id, $mobile, $business_name){
        $email = "talktous@shouut.com";
        $subject = "Coupon code request";
        $headers = 'From: SHOUUT <talktous@shouut.com>' . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $message = '';

        $message .= '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg3.jpg);
                        background-repeat:no-repeat; background-position:top center;">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px"><strong>Hi!</strong></h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">The details you have entered are:</p>
                        <br />
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Name:
                        <strong>'.$business_name.'</strong></p>
                         <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Phone Number:
                        <strong>'.$mobile.'</strong></p>
                         <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Email:
                        <strong>'.$email_id.'</strong>
                        </p>
 <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">This is a request to generate coupon code for a SHOUUT channel. </p>

<p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;"> For any query  : Please write to
us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>
                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT FLASH SALE</h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';

        mail($email,$subject,$message,$headers);
    }
    public function date_validity($validity){
        $day = '';
        $month = '';
        $year = '';
        $current_day = date("d");
        $current_month = date("m");
        $current_year = date("Y");

        if($current_day == '1'){
            $day = 30;
        }else{
            $day = $current_day - '1';
        }
        $expiry_month = $current_month + $validity;
        if($expiry_month > 12){
            $year = $current_year + '1';
            $month = $expiry_month - '12';
        }else{
            $month = $expiry_month;
            $year = $current_year;
        }

        //$expirydate =  $day.'-'.$month.'-'.$year;
        $expirydate = $year.'-'.$month.'-'.$day;
        return $expirydate;
    }



    public function brand_info(){
        $data = array();
        if($this->session->userdata('userid') && $this->session->userdata('userid') != ''){
            $userid = $this->session->userdata('userid');
            $query = $this->DB2->query("select brand.*, gc.category_name from brand INNER JOIN get_category as gc ON brand.brand_category_id= gc
.category_id where
user_id = '$userid'");
            if($query->num_rows()>0){
                foreach($query->result() as $row)
                    //return $row;
                    $data['brand_name'] = $row->brand_name;
                $data['brand_email'] = $row->brand_email;
                $data['brand_category'] = $row->category_name;
                $data['brand_logo'] = IMAGEPATHURL.'brand/logo/logo/'
                    .$row->original_logo_logoimage;
                $data['brand_banner'] = IMAGEPATHURL.'brand/banner/original/'
                    .$row->original_big_logo;
                $data['brand_color']= $row->brand_color;
                $data['brand_address']=$row->brand_address;

                $data['resultCode'] = '1';
            }else{
                $data['resultCode'] = '0';
            }

        }else{
            if($this->session->userdata('brandid') && $this->session->userdata('brandid') != ''){
                $brandid = $this->session->userdata('brandid');
                $query = $this->DB2->query("select brand.*, gc.category_name from brand INNER JOIN get_category as gc ON brand.brand_category_id= gc
.category_id where
brand_id = '$brandid'");
                if($query->num_rows()>0){
                    foreach($query->result() as $row)
                        //return $row;
                        $data['brand_name'] = $row->brand_name;
                    $data['brand_email'] = $row->brand_email;
                    $data['brand_category'] = $row->category_name;
                    $data['brand_logo']='http://localhost/shouutmedia/reward/brand/logo/logo/'
                        .$row->original_logo_logoimage;
                    $data['brand_banner'] = 'http://localhost/shouutmedia/reward/brand/banner/original/'
                        .$row->original_big_logo;
                    $data['brand_color']= $row->brand_color;
                    $data['brand_address']=$row->brand_address;

                    $data['resultCode'] = '1';
                }else{
                    $data['resultCode'] = '0';
                }

            }
        }

        return $data;
    }

    public function update_brand_email($request){
        $data = array();
        if($this->session->userdata('userid') && $this->session->userdata('userid') != ''){

            $userid = $this->session->userdata('userid');
            //$brand_email=$request->email;
            $brand_email = $_POST['email'];
            $update = $this->DB2->query("update brand set brand_email = '$brand_email' WHERE user_id = '$userid'");
            if(isset($_FILES['UploadedFile']['name']) && $_FILES['UploadedFile']['name'] != ''){
                $filename = $_FILES['UploadedFile']['name'];
                $tmp = $_FILES['UploadedFile']['tmp_name'];
                $path = IMAGEPATH.'brand/logo/';
                $amazonpath=AMAZONPATH.'brand/logo/';
                $flash_logo_name = $this->resize_image($path,$tmp,$filename, $amazonpath);
                $original = $flash_logo_name[0];
                $logo = $flash_logo_name[1];
                $small = $flash_logo_name[2];
                $medium = $flash_logo_name[3];
                $large = $flash_logo_name[4];
                $update = $this->DB2->query("update brand set original_logo='$original', original_logo_logoimage =
                '$logo', original_logo_smallimage = '$small', original_logo_mediumimage = '$medium',
                original_logo_largeimage = '$large'
                WHERE user_id = '$userid'");
            }
            $data['resultCode'] = '1';
            $data['resultMessage'] =  'Updated';
        }
        else{
            if($this->session->userdata('brandid') && $this->session->userdata('brandid') != ''){

                $brandid = $this->session->userdata('brandid');
                //$brand_email=$request->email;
                $brand_email = $_POST['email'];
                $update = $this->DB2->query("update brand set brand_email = '$brand_email' WHERE brand_id = '$brandid'");
                if(isset($_FILES['UploadedFile']['name']) && $_FILES['UploadedFile']['name'] != ''){
                    $filename = $_FILES['UploadedFile']['name'];
                    $tmp = $_FILES['UploadedFile']['tmp_name'];
                    $path = IMAGEPATH.'brand/logo/';
                    $amazonpath=AMAZONPATH.'brand/logo/';
                    $flash_logo_name = $this->resize_image($path,$tmp,$filename, $amazonpath);
                    $original = $flash_logo_name[0];
                    $logo = $flash_logo_name[1];
                    $small = $flash_logo_name[2];
                    $medium = $flash_logo_name[3];
                    $large = $flash_logo_name[4];
                    $update = $this->DB2->query("update brand set original_logo='$original', original_logo_logoimage =
                '$logo', original_logo_smallimage = '$small', original_logo_mediumimage = '$medium',
                original_logo_largeimage = '$large'
                WHERE brand_id = '$brandid'");
                }
                $data['resultCode'] = '1';
                $data['resultMessage'] =  'Updated';
            }else{
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Login Fail';
            }

        }

        return $data;
    }

    public function finish_brand_profile($request){
        $data = array();
        if($this->session->userdata('userid')){
            $userid = $this->session->userdata('userid');
            $brand_color=$request->brand_color;
            $brand_address=$request->brand_address;
            $update = $this->DB2->query("update brand set brand_color = '$brand_color', brand_address= '$brand_address' WHERE user_id = '$userid'");
            $data['resultCode'] = '1';
            $data['resultMessage'] =  'Updated';
        }
        else{
            if($this->session->userdata('brandid')){
                $brandid = $this->session->userdata('brandid');
                $brand_color=$request->brand_color;
                $brand_address=$request->brand_address;
                $update = $this->DB2->query("update brand set brand_color = '$brand_color', brand_address=
            '$brand_address' WHERE brand_id = '$brandid'");
                $data['resultCode'] = '1';
                $data['resultMessage'] =  'Updated';
            }
            else{
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Login Fail';
            }
        }


        return $data;
    }

    


   

   
    
    public function get_brandname($brandid){
        $brandQ = $this->DB2->query("SELECT brand_name,original_logo,original_logo_logoimage,original_logo_smallimage FROM brand WHERE brand_id='".$brandid."'");
        $num = $brandQ->num_rows();
        $brandarr = array();
        if($num > 0){
            $row = $brandQ->row();
            $brandarr['brand_name'] = $row->brand_name;
            $brandarr['brand_original_image'] = $row->original_logo;
            $brandarr['brand_logo_image'] = $row->original_logo_logoimage;
            $brandarr['brand_small_image'] = $row->original_logo_smallimage;
        }
        return $brandarr;
    }
//new isp changes done
    public function brandlist(){
        $data = array();
        $query= $this->DB2->query("select brand_id, brand_name from brand WHERE is_deleted = '0' AND status = '1' ORDER BY brand_name");
        foreach($query->result() as $row){
            $data[] = $row;
        }
        return $data;
    }
// new isp changes done
    public function setbrand($request){
        $data = array();
        $brand_id = $request->brand_id;
        $this->session->set_userdata('brandid', $brand_id);
        $data['resultCode'] = '1';

    }
    

    
   

    

    public function daily_analysis_update(){
        //channel_daily_analysis_log table to channel_analysis table
        $data = array();
        $all_daily_analysis = $this->DB2->query("SELECT brand_id,DATE(visit_date) AS visit_date,
(SELECT COUNT(*) AS COUNT FROM channel_daily_analysis_log cdal2 WHERE visit_via='web'
AND cdal1.brand_id=cdal2.brand_id AND DATE(cdal1.visit_date)=DATE(cdal2.visit_date)) AS webhit,
(SELECT COUNT(*) AS COUNT FROM channel_daily_analysis_log cdal2 WHERE visit_via='android'
AND cdal1.brand_id=cdal2.brand_id AND DATE(cdal1.visit_date)=DATE(cdal2.visit_date)) AS androidhit,
(SELECT COUNT(*) AS COUNT FROM channel_daily_analysis_log cdal2 WHERE visit_via='iphone'
AND cdal1.brand_id=cdal2.brand_id AND DATE(cdal1.visit_date)=DATE(cdal2.visit_date)) AS iphonehit,
(SELECT COUNT(DISTINCT user_id)  FROM  channel_daily_analysis_log cdal2 WHERE  cdal1.brand_id=cdal2.brand_id
AND DATE(cdal1.visit_date)=DATE(cdal2.visit_date) AND cdal2.visit_via='web' ) AS unique_web_count,
(SELECT COUNT(DISTINCT user_id)  FROM  channel_daily_analysis_log cdal2 WHERE  cdal1.brand_id=cdal2.brand_id
AND DATE(cdal1.visit_date)=DATE(cdal2.visit_date) AND cdal2.visit_via='android' ) AS unique_android_count,
(SELECT COUNT(DISTINCT user_id)  FROM  channel_daily_analysis_log cdal2 WHERE  cdal1.brand_id=cdal2.brand_id
AND DATE(cdal1.visit_date)=DATE(cdal2.visit_date) AND cdal2.visit_via='iphone' ) AS unique_iphone_count
FROM channel_daily_analysis_log cdal1
GROUP BY brand_id,DATE(visit_date) ORDER BY visit_date");
        foreach($all_daily_analysis->result() as $daily_analysis){
            $check = $this->DB2->query("select * from channel_analysis WHERE visite_date =
            '$daily_analysis->visit_date' AND brand_id = '$daily_analysis->brand_id'");
            if($check->num_rows() > 0){
                $update_channel_analysis = $this->DB2->query("update channel_analysis set web_count =
             web_count+$daily_analysis->webhit,android_count = android_count+$daily_analysis->androidhit,ios_count = ios_count+ $daily_analysis->iphonehit WHERE visite_date ='$daily_analysis->visit_date' AND brand_id = '$daily_analysis->brand_id'");
            }else{
                $insert_channel_analysis = $this->DB2->query("insert into channel_analysis(brand_id, web_count,
                            unique_web_count, android_count, unique_android_count, ios_count, unique_ios_count, visite_date)
                            values('$daily_analysis->brand_id', '$daily_analysis->webhit', '$daily_analysis->unique_web_count',
                            '$daily_analysis->androidhit', '$daily_analysis->unique_android_count', '$daily_analysis->iphonehit',
                            '$daily_analysis->unique_iphone_count', '$daily_analysis->visit_date')");
            }

        }
        //truncate table
        $truncate_daily_analysis = $this->DB2->query("TRUNCATE table channel_daily_analysis_log");
    }

    

    

    

   

   

    

   
   
    public function send_via_sms($mobile, $msg){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://173.45.76.226:81/send.aspx?username=shouut&pass=shouut&route=trans1&senderid=SHOUUT&numbers=".$mobile."&message=".$msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
        if($statusCode==200){
            return 1;
        }
        curl_close($ch);
    }
        public function getuser_mobile($user_id){
        $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
        if($get_user->num_rows() > 0){
            $row_user = $get_user->row_array();
            $screen_name = $row_user['firstname'];
            $email = $get_user['email'];
            $mobile = $get_user['mobile'];
            return $screen_name.'~~~'.$email.'~~~'.$mobile;
        }else{
            return '';
        }
    }


    public function send_success_mail($screen_name,$email,$vouchercode,$flash_deal_title,$brandname,$store_location){
        $subject = "Shouut Flash Sale: Voucher redemption confirmation";
        //$headers = 'From: SHOUUT <flashsale@shouut.com>' . "\r\n";
        // $headers .= 'Cc: aashish@shouut.com'. "\r\n";
        //$headers .= 'MIME-Version: 1.0' . "\r\n";
        //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg3.jpg);
                        background-repeat:no-repeat; background-position:top center;">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px"><strong>Hi '.$screen_name.',</strong></h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">Congrats
                        on claiming the Shouut Flash Sale voucher <strong>'.$vouchercode.'</strong> for the
                         <strong>'.$flash_deal_title
            .'</strong>offer of <strong>'.$brandname.'</strong> at <strong>
                         '.$store_location.'</strong></p>

<p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;"> For any query  : Please write to
us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>

                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT FLASH SALE</h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';
        //mail($email,$subject,$message,$headers);

        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 25;
        $mail->SMTPAuth = true;
        $mail->Username = "flashsale@shouut.com";
        $mail->Password = "shouut@#123";
        $mail->setFrom('flashsale@shouut.com', 'Flash Sale');
        $mail->addAddress($email, $screen_name);
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->msgHTML($message);
        $mail->AltBody = '';
        $mail->send();
        return 1;

    }

    public function send_success_mail_tobrand($screen_name,$email,$vouchercode,$flash_deal_title,$brandname,$store_location){
        $subject = "Shouut Flash Sale: Voucher redemption confirmation";
        //$headers = 'From: SHOUUT <flashsale@shouut.com>' . "\r\n";
        //$headers .= 'Cc: curators@shouut.com'. "\r\n";
        //$headers .= 'MIME-Version: 1.0' . "\r\n";
        //$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $message = '<html><head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>SHOUUT</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet" type="text/css">
                        </head>
                        <body style="padding:0px; margin:0px; background-color:#f2f1f1; font-family: Roboto, sans-serif; color:#333; font-size:100%">
                        <table width="600" height="auto" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr style=" background-image:url(https://d6l064q49upa5.cloudfront.net/emailer/top-bgg2.jpg);
                        background-repeat:no-repeat; background-position:top center;">
                        <td>
                        <table width="100%" height="320" cellpadding="15" cellspacing="0">
                        <tr align="right">
                        <td valign="top"><a href="https://www.shouut.com/" target="_blank"><img src="https://d6l064q49upa5.cloudfront.net/emailer/logo.png" alt=""/></a></td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr bgcolor="#fff">
                        <td valign="top" style="padding:15px;">
                        <h5 style="margin-bottom:5px; margin-top:0px">Hi</h5>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">
                       <strong>'.$screen_name.'</strong> has claimed a Shouut Flash Sale voucher <strong>'.$flash_deal_title.'</strong> at your
                       store/venue in <strong>'.$store_location.'</strong>
                       </p>
                        <p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;">
                      Voucher code: <strong>'.$vouchercode.'</strong>
                       </p>

<p style="margin-bottom:0px; margin-top:10px; font-weight:normal; font-size:14px;"> For any query  : Please write to
us <strong>talktous@shouut.com</strong> or call at +911165636400
</p>
                        <h5 style="font-size:14px; font-weight:normal;margin-bottom:5px; margin-top:10px;"><strong>Connect with us on:</strong></h5>
                        <p><span>
                        <a href="https://twitter.com/shouutin" target="_blank" style="cursor:pointer">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/t-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;<span>
                        <a href="https://www.facebook.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/f-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.youtube.com/channel/UChja2mu1SQt3cp-giUIPEzA" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/y-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href=" https://www.linkedin.com/company/10231082?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A10231082%2Cidx%3A3-1-12%2CtarId%3A1464343603217%2Ctas%3Ashouu" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/l-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://www.instagram.com/shouutin/" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/in-cons.png" alt=""/></a>
                        </span></p>
                         <h5 style="margin-bottom:5px; margin-top:10px;">Cheers</h5>
                        <h5 style="margin-bottom:5px; margin-top:10px;">Team Shouut</h5>
                        <h6 style="margin-bottom:5px; margin-top:10px;"><a href="https://www.shouut.com/" target="_blank">www.shouut.com</a></h6>
                        </td>
                        </tr>
                        <tr bgcolor="#febc12">
                        <td style="padding:10px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                        <td rowspan="2" width="40"><img src="https://d6l064q49upa5.cloudfront.net/emailer/footer-icons.png" alt=""/></td>
                        <td><h4 style="margin-bottom:0px; margin-top:0px; color:#fff; font-size:12px; font-weight:800">SHOUUT FLASH SALE</h4></td>
                        <td align="right"><h6 style="margin-bottom:0px; margin-top:0px;"><strong>GET SHOUUT ON MOBILE</strong></h6></td>
                        </tr>
                        <tr>
                        <td><h6 style="margin-bottom:0px; margin-top:0px;"><strong>WALK IN TO FIND ULTIMATE OFFERS NEAR YOU.</strong></h6></td>
                        <td align="right"><span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/ios-icons.png" alt=""/></a>
                        </span>&nbsp;&nbsp;&nbsp;<span>
                        <a href="https://play.google.com/store/apps/details?id=com.shouut.discover" target="_blank">
                        <img src="https://d6l064q49upa5.cloudfront.net/emailer/iphone-icons.png" alt=""/></a></span></td>
                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        </table>
                        </body>
                        </html>';
        //mail($email,$subject,$message,$headers);
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 25;
        $mail->SMTPAuth = true;
        $mail->Username = "flashsale@shouut.com";
        $mail->Password = "shouut@#123";
        $mail->setFrom('flashsale@shouut.com', 'Flash Sale');
        $mail->addAddress($email, $screen_name);
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->msgHTML($message);
        $mail->AltBody = '';
        $mail->send();
        return 1;

    }

    public function send_sms_touser($screen_name,$mobile,$vouchercode,$flash_deal_title,$brandname,$store_location){
        //$random_num = mt_rand(1000,9999); //return $random_num; die;
        $msgdata = urlencode('Hi '.$screen_name.',').'%0a'.'%0a'.
            urlencode('Congrats on claiming a Shouut Flash Sale Voucher ' .$vouchercode.' for the offer ' .$flash_deal_title)
            .'%0a'.urlencode('at '.$brandname.' in '.$store_location)
            .'%0a'.'%0a'.urlencode('Visit www.shouut.com to get awesome flash sale offers daily.')
            .'%0a'.'%0a'.urlencode('For any query : Please write to us talktous@shouut.com or call at 01165636400').'%0a'.urlencode("Cheers").'%0a'.'%0a'.urlencode
            ("Team Shouut");
        //echo $msgdata; die;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://173.45.76.226:81/send.aspx?username=shouut&pass=shouut&route=trans1&senderid=SHOUUT&numbers=".$mobile."&message=".$msgdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
        if($statusCode==200){
            return $vouchercode;
        }
        curl_close($ch);
    }
    public function avail_coupon_user_list($request){
        $data = array();
        $deal_id = $request->deal_id;
        $query = $this->db->query("select s.store_name,mf.user_id, mf.voucher_code from my_flash as mf INNER JOIN store as s ON (mf.redeem_on_store = s.store_id) WHERE mf.flash_deal_id = '$deal_id'");
        $i = 0;
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";
            foreach($query->result() as $row){
                $user_id = $row->user_id;
                 $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                    $row_user = $get_user->row_array();
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['store_name'] = $row->store_name;
                    $data['users'][$i]['voucher_code'] = $row->voucher_code;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;
    }


    public function redeem_coupon_user_list($request){
        $data = array();
        $deal_id = $request->deal_id;
        $query = $this->db->query("select s.store_name,mf.user_id, mf.voucher_code from my_flash as mf INNER JOIN
        store as s ON (mf.store_id = s.store_id) WHERE mf.flash_deal_id = '$deal_id'");
        $i = 0;
        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";
            foreach($query->result() as $row){
                $user_id = $row->user_id;
                $get_user = $this->DB2->query("select uid, firstname, lastname, mobile, email, gender, age_group from sht_users where uid = '$user_id'");
                if($get_user->num_rows() > 0){
                     $row_user = $get_user->row_array();
                    $data['users'][$i]['user_name'] = $row_user['firstname'];
                    $data['users'][$i]['user_mobile'] = substr($row_user['mobile'], 0, 6)."****";
                    $data['users'][$i]['store_name'] = $row->store_name;
                    $data['users'][$i]['voucher_code'] = $row->voucher_code;
                    $i++;
                }
               
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;
    }
   

   

   
    
	
	


    

    

    
    

    


    public function forget_password_emailcheck($request){
        $data = array();
        $email_id = $request->email;
        $query = $this->DB2->query("select id from brand_user WHERE email = '$email_id'");
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $data['id'] = $row['id'];
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No Email found";
        }
        return $data;
    }
    public function forget_password($request){
        $data = array();
        $password_new = $request->password;
        $id = $request->id;
        $random_salt = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,5 ) ,2 ) .substr( md5( time() ), 1, 5);
        $password = md5($password_new.$random_salt);
        $update = $this->DB2->query("update brand_user set salt = '$random_salt', password = '$password' WHERE id =
        '$id'");
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";

        return $data;
    }
    
   

   

  public function current_brand_balance(){
        $data = array();
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
        }
        $query = $this->DB2->query("select current_balance from cp_brand_budget where brand_id = '$brand_id'");
        $brand_balance = 0;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $brand_balance = (int)$row['current_balance'];
        }
        $data['balance'] = $brand_balance;
        return $data;
    }

    
    public function transection_detail($id){
        $data = array();
        $query = $this->DB2->query("select trns_resp from cp_brand_budget_added where id = '$id'");
        $email = '';
        $authIdCode = 0;
        $txnDateTime = 0;
        $transactionId = 0;
        $issuerRefNo = 0;
        $TxRefNo = 0;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $detail = $row['trns_resp'];
            $def = json_decode($detail);
            $email = $def->email;
            $authIdCode = $def->authIdCode;
            $txnDateTime = $def->txnDateTime;
            $transactionId = $def->transactionId;
            $issuerRefNo = $def->issuerRefNo;
            $TxRefNo = $def->TxRefNo;
        }
        $data['email'] = $email;
        $data['authIdCode'] = $authIdCode;
        $data['txnDateTime'] = $txnDateTime;
        $data['transactionId'] = $transactionId;
        $data['issuerRefNo'] = $issuerRefNo;
        $data['TxRefNo'] = $TxRefNo;
        return $data;
    }

    public function add_order($addorder = FALSE){
        $postdata=$this->input->post();
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
        }
        //echo "<pre>";print_r($postdata);die;
        if($addorder)
        {
            $tabledata=array("brand_id"=>$brand_id,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"1","trns_resp"=>json_encode($postdata),"created_on"=>$postdata['txnDateTime']);

            $query = $this->DB2->query("select brand_id from cp_brand_budget where brand_id = '$brand_id'");
            $budget_enter = $postdata['amount'];
            if($query->num_rows() > 0){
                $update = $this->DB2->query("update cp_brand_budget set current_balance = (current_balance + $budget_enter), updated_on = now() where brand_id = '$brand_id'");
            }
            else{
                $insert = $this->DB2->query("insert into cp_brand_budget (brand_id, current_balance, created_on) values('$brand_id', '$budget_enter', now())");
            }
            $this->db->insert('cp_brand_budget_added',$tabledata);
            $id = $this->DB2->insert_id();
            redirect(base_url()."channel/addFund_success_view/".$id);
        }
        else {
            $tabledata=array("brand_id"=>$brand_id,"total_amt"=>$postdata['amount'],"name"=>$postdata['firstName'],"email"=>$postdata['email'],
                "mobile"=>$postdata['mobileNo'],"payment_success"=>"0","trns_resp"=>json_encode($postdata),"created_on"=>(isset($postdata['txnDateTime']))?$postdata['txnDateTime']:date("Y-m-d H:i:s"));

            $this->DB2->insert('cp_brand_budget_added',$tabledata);
            $id = $this->DB2->insert_id();
            redirect(base_url()."channel/addFund_fail_view/".$id);
        }


    }


   
    public function daily_unique_user_update(){
        //working fine
        //channel_daily_analysis_log table to channel_analysis table

        $data = array();
        $all_daily_analysis = $this->DB2->query("SELECT location_id, DATE(visit_date) AS visit_date,
(SELECT COUNT(*) AS COUNT FROM channel_unique_daily_user cdal2 WHERE
 DATE(cdal1.visit_date)=DATE(cdal2.visit_date) AND cdal1.location_id=cdal2.location_id) AS
unique_macid
FROM channel_unique_daily_user cdal1
GROUP BY DATE(visit_date),location_id ORDER BY visit_date");
        //echo "<pre>";print_r($all_daily_analysis->result());die;
        foreach($all_daily_analysis->result() as $row){
            $unique_user = $row->unique_macid;
            $check = $this->DB2->query("select * from channel_unique_user WHERE visit_date =
            '$row->visit_date'  AND location_id = '$row->location_id'");
            if($check->num_rows() > 0){
                $update_offer_analysis = $this->DB2->query("update channel_unique_user set
                total_unique = total_unique+$unique_user
 WHERE visit_date = '$row->visit_date'  AND location_id = '$row->location_id'");
            }else{
                $insert_channel_analysis = $this->DB2->query("insert into channel_unique_user
                (location_id, total_unique,visit_date) values
           ('$row->location_id', '$row->unique_macid','$row->visit_date')");
            }

        }
        $query = $this->DB2->query("select location_id,macid, visit_date, osinfo from
        channel_unique_daily_user
        GROUP  by date(visit_date),macid,location_id");
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $insert = $this->DB2->query("insert into channel_captiveportal_macid(location_id,macid, visit_date, os_info)
                VALUES
                ('$row->location_id','$row->macid', '$row->visit_date', '$row->osinfo')");
            }
        }

        $truncate_daily_analysis = $this->DB2->query("TRUNCATE table
        channel_unique_daily_user");
        //echo "<pre>"; print_r($data);die;
    }

   
    

   

    public function add_campaign_survey_offer($request){
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
            $offer_brand_id = $this->session->userdata('brandid');
        }
        $is_shouut_offer_check = $request->is_shouut_offer_check;
        $offer_title = $request->offer_title;
        $offer_desc = $request->offer_desc;
        $offer_type = $request->offer_type;
        $age_group = $request->age_group;
        $mass_budget = $request->mass_budget;
        $premium_budget = $request->premium_budget;
        $star_budget = $request->star_budget;
        $campaign_day_to_run = '';
        if(is_array($request->campaign_day_to_run)){
            $campaign_day_to_run = implode(',', $request->campaign_day_to_run);
        }
        else{
            if($request->campaign_day_to_run==""){
                $campaign_day_to_run = 'all';
            }else{
                $campaign_day_to_run = $request->campaign_day_to_run;
            }
        }
        $campaign_device = '';
        if(is_array($request->campaign_device)){
            $campaign_device = implode(',', $request->campaign_device);
        }
        else{
            $campaign_device = 'all';
        }
        $active_between = '';
        if(is_array($request->active_between)){
            $active_between = implode(',', $request->active_between);
        }
        else{
            $active_between = 'all';
        }
        $campaign_name = $request->campaign_name;
        $location_selected = $request->location_selected;
        $gender = $request->gender;
	$created_by_location = '0';
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $created_by_location = $this->session->userdata('location_uid');
	    $total_budget = '100000';
	}else{
	    $total_budget = $request->total_budget;
	}
        
        $campaign_start_date = $request->campaign_start_date;
        $campaign_start_time = "00:00";
        $start_date = date('Y-m-d H:i:s', strtotime("$campaign_start_date $campaign_start_time"));
        $campaign_end_date = $request->campaign_end_date;
        $campaign_end_time = "23:55";
        $end_date = date('Y-m-d H:i:s', strtotime("$campaign_end_date $campaign_end_time"));
        $brand_title = $_POST['brand_title'];
        $insert_offer = $this->DB2->query("insert into cp_offers(created_by_location,offer_title, offer_desc,brand_id, created_by_brand_id, offer_type,
       campaign_name, offer_exclusive_to_shouut, age_group, gender, active_between, campaign_start_date,
                                         campaign_end_date, campaign_day_to_run, total_budget, mass_budget, premium_budget, star_budget,status,
                                         is_deleted, created_on, platform_filter,poll_brand_title ,is_web_offer ) values('$created_by_location','$offer_title', '$offer_desc',
                                         '$offer_brand_id','$brand_id','$offer_type','$campaign_name', '0',
                                         '$age_group', '$gender', '$active_between', '$start_date',
                                         '$end_date', '$campaign_day_to_run', '$total_budget', '$mass_budget', '$premium_budget', '$star_budget','1', '0', now(),
                                         '$campaign_device' ,'$brand_title', '$is_shouut_offer_check')");

        $cp_offer_id = $this->DB2->insert_id();

        //subtract budget from brand account
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $total_budget = '0';
	}else{
            $total_budget = '0';
        }
        $subtract_budget = $this->DB2->query("update cp_brand_budget set current_balance = (current_balance -
        $total_budget)  WHERE brand_id = '$brand_id'");
        //insert in cp_offer_location table
        foreach($location_selected as $location_id1){
            $insert_location = "insert into cp_offers_location(cp_offer_id, location_id) VALUES
                ('$cp_offer_id','$location_id1')";
            $query3 = $this->DB2->query($insert_location);
        }
        $pending_array = array();
        $pen_inc = 0;

        $question  = $request->questions;
        foreach($question as $question1){
            $question_value =  $question1->question;
            $question_option_choice =  $question1->question_option_choice;
            $insert_question = $this->DB2->query("insert into cp_survey_question(cp_offer_id, question, created_on, option_choice)
            values('$cp_offer_id', '$question_value', now(), '$question_option_choice')");
            $question_id = $this->DB2->insert_id();
            $option_array = $question1->options;
            foreach($option_array as $option_array1){
                $option_value =  $option_array1->value;
                $option_next_value =  $option_array1->next_question;
                $next_ques = '';
                $next_val = '';
                if($option_next_value != ''){
                    $next_val = substr($option_next_value, -1);
                    if($next_val == '0'){
                        $next_ques = 1;
                    }
                }

                $insert_option = $this->DB2->query("insert into cp_survey_option(ques_id, option_val, created_on, next_ques)
                values('$question_id', '$option_value', now(), '$next_ques')");
                $option_id = $this->DB2->insert_id();
                if($next_val != '' && $next_val != '0'){
                    $pending_array[$pen_inc]["question_id"] = $question_id;
                    $pending_array[$pen_inc]["option_id"] = $option_id;
                    $pending_array[$pen_inc]["next_ques"] = $next_val;
                    $pen_inc++;
                }
            }
        }
        $get_question_id_of_offer = $this->DB2->query("select ques_id from cp_survey_question where cp_offer_id =
        '$cp_offer_id'");
        $question_id_list = array();
        $i = 1;
        foreach($get_question_id_of_offer->result() as $valq)
        {
            $question_id_list[$i]= $valq->ques_id;
            $i++;
        }
        foreach($pending_array as $pending_array1){
            $next_question = $pending_array1['next_ques'];
            $option_updated = $pending_array1['option_id'];
            $next_question_id_update = $question_id_list[$next_question];
            $update = $this->DB2->query("update cp_survey_option set next_ques = '$next_question_id_update' WHERE
            option_id = '$option_updated'");
        }
        $data = array();
        $data['resultCode'] = '1';
        $data['offer_id'] = $cp_offer_id;
        $data['resultMessage'] = "Success";
        return $data;
    }

    public function add_campaign_survey_offer_image($request){
        $data = array();
        $offer_id = $_POST['offer_id'];
        $original = '';
        $logo = '';
        $small = '';
        $medium = '';
        $large = '';
        if (isset($_FILES) && !empty($_FILES['image_file']['name'])) {
            $filename = $_FILES['image_file']['name'];
            $tmp = $_FILES['image_file']['tmp_name'];
            $path = 'assets/campaign/images/';
            $amazonpath=AMAZONPATH.'campaign/images/';
            $flash_logo_name = $this->resize_image($path,$tmp,$filename, $amazonpath);
            $original = $flash_logo_name[0];
            $logo = $flash_logo_name[1];
            $small = $flash_logo_name[2];
            $medium = $flash_logo_name[3];
            $large = $flash_logo_name[4];

        }
        $original_poll = '';
        $logo_poll = '';
        $small_poll = '';
        $medium_poll = '';
        $large_poll = '';
        if (isset($_FILES) && !empty($_FILES['poll_logo']['name'])) {
            $filename = $_FILES['poll_logo']['name'];
            $tmp = $_FILES['poll_logo']['tmp_name'];
            $path = 'assets/campaign/poll_logo/';
            $amazonpath=AMAZONPATH.'campaign/poll_logo/';
            $flash_logo_name = $this->resize_image($path,$tmp,$filename, $amazonpath);
            $original_poll = $flash_logo_name[0];
            $logo_poll = $flash_logo_name[1];
            $small_poll = $flash_logo_name[2];
            $medium_poll = $flash_logo_name[3];
            $large_poll = $flash_logo_name[4];
        }
        $update = $this->DB2->query("update cp_offers set image_original = '$original', image_logo = '$logo',
        image_small = '$small', image_medium = '$medium', image_large = '$large', poll_original =
        '$original_poll', poll_large = '$large_poll', poll_medium = '$medium_poll', poll_small =
        '$small_poll', poll_logo = '$logo_poll' WHERE
        cp_offer_id =
        '$offer_id'");
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
        return $data;
    }

    public function campaign_graph_data_survey($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_survey_answer cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND
            cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;

        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_survey_answer cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select  offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }



    public function campaign_graph_data_survey_question($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                $device_filter = " AND osinfo in ($device) ";
            }
        }

        $data = array();
        $query = $this->DB2->query("select ques_id, question from cp_survey_question where cp_offer_id = '$campaign_id'");
        $i = 0;
        $question_record = array();
        foreach($query->result() as $row){
            $question_id = $row->ques_id;
            $question_record[$i]['question'] = $row->question;
            $option_query = $this->DB2->query("select * from cp_survey_option where ques_id = '$question_id'");
            $j = 1;
            $analytics_array = array();
            $k = 0;
            foreach($option_query->result() as $row1){
                // get option click
                $option_id = $row1->option_id;
                $option_count = $this->DB2->query("select count(*) as total_ans from cp_survey_answer where cp_offer_id = '$campaign_id'
				and question_id = '$question_id' and option_id = '$option_id' and DATE(viewed_on) BETWEEN '$date_from' AND '$date_to'
				$gender_where $age_group_filter $time_filter $device_filter");
                $total_click = 0;
                if($option_count->num_rows() > 0){
                    $row2 = $option_count->row_array();
                    $total_click = $row2['total_ans'];
                }

                $analytics_array[$k]['option'.$j] = $row1->option_val;
                $analytics_array[$k]['option'.$j.'_count'] = (int)$total_click;
                $k++;
                //$question_record[$i]['option'.$j] = $row1->option_val;
                //$question_record[$i]['option'.$j.'_count'] = $total_click;

                $j++;
            }

            $question_record[$i]['options'] = $analytics_array;
            $i++;
        }

        $data['resultCode'] = 1;
        $data['question_record'] = $question_record;
        //echo "<pre>";print_r($data);
        return $data;
    }
    
    // for Home channel api
    
    // new isp changes done
    public function isp_user_list(){
        $data = array();
        if($this->session->userdata('provider_id')){
            $provider_id = $this->session->userdata('provider_id');    
        }
        
        $query= $this->db->query("select id, isp_uid, uid, firstname, lastname from sht_users WHERE isp_uid = '$provider_id'");
        foreach($query->result() as $row){
            $data[] = $row;
        }
        return $data;
    }
         public function add_campaign_homwifi_offer($request){
        $data = array();
        $voucher_code_array = array();
        if (isset($_FILES) && !empty($_FILES['excelfile']['name'])) {
            $filename = $_FILES['excelfile']['name'];
            $allowed =  array('xlsx', 'xls');
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Please select valid excel file';
                return $data;
            }
            $base_url = 'assets/files/';
            move_uploaded_file($_FILES['excelfile']['tmp_name'], $base_url . $filename);
            $file = 'assets/files/'. $filename;
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
            $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet

            for ($i = 2; $i <= $arrayCount; $i++) {
                $voucher_code_excel = trim($allDataInSheet[$i]["A"]);
                if($voucher_code_excel != ''){
                    $voucher_code_array[$i] = $voucher_code_excel;
                }
            }
            if (empty($voucher_code_array)) {
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Excel can not be empty';
                return $data;
            }
            if(count($voucher_code_array) != count(array_unique($voucher_code_array))){
                $data['resultCode'] = '0';
                $data['resultMessage'] =  'Coupon Code should be unique';
                return $data;
            }
        }

        //echo "<pre>";print_r($_POST);die;
        $is_shouut_offer_check = $_POST['is_shouut_offer_check'];
        $offer_brand_id = '';
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
            $offer_brand_id = $this->session->userdata('brandid');
        }
        $video_duration = ceil($_POST['video_duration']);
        $brand_title = $_POST['brand_title'];
        $offer_type = $_POST['offer_type'];
        $campaign_name = $_POST['campaign_name'];
        $add_offer_exclusive_to_shouut = $_POST['add_offer_exclusive_to_shouut'];
        $age_group = $_POST['age_group'];
        $gender = $_POST['gender'];
        $active_between = $_POST['active_between'];
        $campaign_start_date = $_POST['campaign_start_date'];
        //$campaign_start_time = $_POST['campaign_start_time'];
        $campaign_start_time = "00:00";
        $start_date = date('Y-m-d H:i:s', strtotime("$campaign_start_date $campaign_start_time"));
        $campaign_end_date = $_POST['campaign_end_date'];
        //$campaign_end_time = $_POST['campaign_end_time'];
        $campaign_end_time = "23:55";
        $end_date = date('Y-m-d H:i:s', strtotime("$campaign_end_date $campaign_end_time"));
        $campaign_day_to_run = ($_POST['campaign_day_to_run']=="")?"all":$_POST['campaign_day_to_run'];
        $campaign_device = ($_POST['campaign_device']=="")?"all":$_POST['campaign_device'];
        $total_budget = $_POST['total_budget'];
        $mass_budget = $_POST['mass_budget'];
        $premium_budget = $_POST['premium_budget'];
        $star_budget = $_POST['star_budget'];
        $offer_title = $_POST['offer_title'];
        $offer_desc = $_POST['offer_desc'];
        $voucher_type = $_POST['voucher_type'];
        $voucher_static_type = $_POST['voucher_static_type'];
        $voucher_code = $_POST['voucher_code'];
        $voucher_expiry = '';
        if($_POST['voucher_expiry'] != ''){
            $voucher_expiry = date('Y-m-d H:i:s', strtotime($_POST['voucher_expiry']));
        }
        $flash_time = '';
        if($_POST['flash_start_date'] != '' && $_POST['flash_start_time'] != ''){
            $flash_start_date = $_POST['flash_start_date'];
            $flash_start_time = $_POST['flash_start_time'];
            $flash_time = date('Y-m-d H:i:s', strtotime("$flash_start_date $flash_start_time"));
        }


        $captcha_accuracy_level = $_POST['captcha_accuracy_level'];
        $captcha_text = $_POST['captcha_text'];
        $term_and_condition = $_POST['term_and_condition'];
        $incentive_desc = $_POST['incentive_desc'];
        $brand_logo_as_banner = $_POST['brand_logo_as_banner'];
        $total_voucher = $_POST['total_voucher'];
        $redirect_url = $_POST['redirect_url'];
        $locations =  explode(',', $_POST['location_selected']);
        $stores = array_filter(explode(',', $_POST['store_selected']));
        //$appurl = array_filter(explode(',', $_POST['appurl']));
        $android_app_url = $_POST['android_app_url'];
        $ios_app_url = $_POST['ios_app_url'];
        $poll_question = array_filter(explode(',', $_POST['poll_question']));
        $original = '';
        $logo = '';
        $small = '';
        $medium = '';
        $large = '';
        $video_path = '';
        if (isset($_FILES) && !empty($_FILES['UploadedFile']['name'])) {
            if($offer_type == 'video_add'){
                $filename = $_FILES['UploadedFile']['name'];
                $tmp = $_FILES['UploadedFile']['tmp_name'];
                $path = "assets/campaign/videos/";
                //$amazonpath=AMAZONPATH.'campaign/videos/';
                $flash_logo_name = $this->resize_video($path,$tmp,$filename);
                $video_path = $flash_logo_name;

            }
            else{
                $filename = $_FILES['UploadedFile']['name'];
                $tmp = $_FILES['UploadedFile']['tmp_name'];
                $path = 'assets/campaign/images/';
                //$amazonpath=AMAZONPATH.'campaign/images/';
                $flash_logo_name = $this->resize_image($path,$tmp,$filename);
                $original = $flash_logo_name[0];
                $logo = $flash_logo_name[1];
                $small = $flash_logo_name[2];
                $medium = $flash_logo_name[3];
                $large = $flash_logo_name[4];
            }

        }
        $original_poll = '';
        $logo_poll = '';
        $small_poll = '';
        $medium_poll = '';
        $large_poll = '';
        if (isset($_FILES) && !empty($_FILES['poll_logo']['name'])) {
            $filename = $_FILES['poll_logo']['name'];
            $tmp = $_FILES['poll_logo']['tmp_name'];
            $path = 'assets/campaign/poll_logo/';
            //$amazonpath=AMAZONPATH.'campaign/poll_logo/';
            $flash_logo_name = $this->resize_image($path,$tmp,$filename);
            $original_poll = $flash_logo_name[0];
            $logo_poll = $flash_logo_name[1];
            $small_poll = $flash_logo_name[2];
            $medium_poll = $flash_logo_name[3];
            $large_poll = $flash_logo_name[4];
        }


        $original_video_thumb = '';
        $logo_video_thumb = '';
        $small_video_thumb = '';
        $medium_video_thumb = '';
        $large_video_thumb = '';
        if (isset($_FILES) && !empty($_FILES['video_thumb']['name'])) {
            $filename = $_FILES['video_thumb']['name'];
            $tmp = $_FILES['video_thumb']['tmp_name'];
            $path = 'assets/campaign/video_thumb/';
            //$amazonpath=AMAZONPATH.'campaign/video_thumb/';
            $video_thumb_name = $this->resize_image($path,$tmp,$filename);
            $original_video_thumb = $video_thumb_name[0];
            $logo_video_thumb = $video_thumb_name[1];
            $small_video_thumb = $video_thumb_name[2];
            $medium_video_thumb = $video_thumb_name[3];
            $large_video_thumb = $video_thumb_name[4];
        }

        if($voucher_static_type == 'multiple'){
            $insert_offer = $this->DB2->query("insert into home_cp_offers(brand_id, created_by_brand_id, offer_type,campaign_name, offer_exclusive_to_shouut, age_group, gender, active_between, campaign_start_date,
                                         campaign_end_date, campaign_day_to_run, total_budget, mass_budget, premium_budget, star_budget, offer_title, offer_desc, term_and_condition, voucher_type, voucher_static_type, voucher_expiry, total_voucher,
                                         flash_start_date,brand_logo_as_banner,
                                         captcha_text, captcha_accuracy_level, redirect_url, incentive_desc, image_original,
                                         image_logo, image_small, image_medium, image_large, video_path, status,
                                         is_deleted, created_on, android_appurl, ios_appurl, platform_filter,
                                          poll_brand_title,poll_original, poll_large, poll_medium, poll_small, poll_logo, video_duration, video_thumb_original, video_thumb_large,
                                          video_thumb_medium, video_thumb_small,video_thumb_logo,is_web_offer) values(
                                         '$offer_brand_id','$brand_id','$offer_type','$campaign_name', '$add_offer_exclusive_to_shouut', '$age_group', '$gender', '$active_between', '$start_date',
                                         '$end_date', '$campaign_day_to_run', '$total_budget', '$mass_budget', '$premium_budget', '$star_budget', '$offer_title', '$offer_desc', '$term_and_condition', '$voucher_type', '$voucher_static_type', '$voucher_expiry', '$total_voucher',
                                         '$flash_time','$brand_logo_as_banner',
                                         '$captcha_text', '$captcha_accuracy_level', '$redirect_url', '$incentive_desc', '$original',
                                         '$logo', '$small', '$medium', '$large', '$video_path','1',
                                         '0', now(), '$android_app_url', '$ios_app_url', '$campaign_device',
                                          '$brand_title' , '$original_poll', '$large_poll', '$medium_poll', '$small_poll', '$logo_poll', '$video_duration',
                                         '$original_video_thumb','$large_video_thumb','$medium_video_thumb',
                                          '$small_video_thumb','$logo_video_thumb', '$is_shouut_offer_check')");

        }
        else{
            $insert_offer = $this->DB2->query("insert into home_cp_offers(brand_id, created_by_brand_id, offer_type,campaign_name, offer_exclusive_to_shouut, age_group, gender, active_between, campaign_start_date,
                                         campaign_end_date, campaign_day_to_run, total_budget, mass_budget, premium_budget, star_budget, offer_title, offer_desc, term_and_condition, voucher_type, voucher_static_type, voucher_code, voucher_expiry, total_voucher,
                                         flash_start_date,brand_logo_as_banner,
                                         captcha_text, captcha_accuracy_level, redirect_url, incentive_desc, image_original,
                                         image_logo, image_small, image_medium, image_large, video_path, status,
                                         is_deleted, created_on, android_appurl, ios_appurl, platform_filter,
                                          poll_brand_title,poll_original, poll_large, poll_medium, poll_small, poll_logo, video_duration, video_thumb_original, video_thumb_large,
                                          video_thumb_medium, video_thumb_small,video_thumb_logo,is_web_offer) values(
                                         '$offer_brand_id','$brand_id','$offer_type','$campaign_name', '$add_offer_exclusive_to_shouut', '$age_group', '$gender', '$active_between', '$start_date',
                                         '$end_date', '$campaign_day_to_run', '$total_budget', '$mass_budget', '$premium_budget', '$star_budget', '$offer_title', '$offer_desc', '$term_and_condition', '$voucher_type', '$voucher_static_type', '$voucher_code', '$voucher_expiry', '$total_voucher',
                                         '$flash_time','$brand_logo_as_banner',
                                         '$captcha_text', '$captcha_accuracy_level', '$redirect_url', '$incentive_desc', '$original',
                                         '$logo', '$small', '$medium', '$large', '$video_path','1',
                                         '0', now(), '$android_app_url', '$ios_app_url', '$campaign_device',
                                          '$brand_title' , '$original_poll', '$large_poll', '$medium_poll', '$small_poll', '$logo_poll', '$video_duration',
                                         '$original_video_thumb','$large_video_thumb','$medium_video_thumb',
                                          '$small_video_thumb','$logo_video_thumb', '$is_shouut_offer_check')");

        }
        $cp_offer_id = $this->DB2->insert_id();
        // add multiple coupon
        if(count($voucher_code_array) > 0){
            $voucher_code =  $voucher_code_array;
        }
        else{
            $voucher_code =  explode(',', $_POST['voucher_code']);
        }

        if($voucher_static_type == 'multiple'){
            foreach($voucher_code as $voucher_code1){
                $insert_voucher = "insert into home_cp_offer_coupon(cp_offer_id, coupon_code, is_used, created_on,
                     used_on) VALUES
                ('$cp_offer_id', '$voucher_code1','0', now(), '')";
                $query3 = $this->DB2->query($insert_voucher);
            }
        }
        //subtract budget from brand account
        $subtract_budget = $this->DB2->query("update cp_brand_budget set current_balance = (current_balance -
        $total_budget)  WHERE brand_id = '$brand_id'");

        //insert in cp_offer_location table
        foreach($locations as $location_id1){
            $insert_location = "insert into home_cp_offers_users(cp_offer_id, user_uid) VALUES
                ('$cp_offer_id','$location_id1')";
            $query3 = $this->DB2->query($insert_location);
        }
        //insert in cp_offer_store table
        if(!empty($stores)) {
            foreach($stores as $store_id1){
                $insert_store = "insert into home_cp_offers_store(cp_offer_id, store_id) VALUES
                   ('$cp_offer_id','$store_id1')";
                $query3 = $this->DB2->query($insert_store);
            }
        }

        //insert in cp_offer_app_url table
        /*if(!empty($appurl)) {
           foreach($appurl as $appurl_1){
           $insert_store = "insert into cp_offers_app_url(cp_offer_id, app_url) VALUES
               ('$cp_offer_id','$appurl_1')";
           $query3 = $this->db->query($insert_store);
       }
     }*/
        //insert in cp_offer_question table
        if(!empty($poll_question)) {
            foreach($poll_question as $poll_question1){
                $question = ''; $option1 = ''; $option2 = ''; $option3 = ''; $option4 = '';
                $questions = array_filter(explode('::+::', $poll_question1));
                if(!empty($questions)) {
                    $qeuestion = $questions[0];
                    $option1 = $questions[1];
                    $option2 = $questions[2];
                    $option3 = $questions[3];
                    $option4 = $questions[4];
                    $insert_question = "insert into home_cp_offers_question(cp_offer_id, question, option1, option2, option3, option4)
                        VALUES ('$cp_offer_id','$qeuestion', '$option1', '$option2', '$option3', '$option4')";
                    $query3 = $this->DB2->query($insert_question);
                }
            }
        }
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
        return $data;
    }
        public function add_campaign_survey_homewifi_offer($request){
        $brand_id = '';
        if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
            $offer_brand_id = $this->session->userdata('brandid');
        }
        $is_shouut_offer_check = $request->is_shouut_offer_check;
        $offer_title = $request->offer_title;
        $offer_desc = $request->offer_desc;
        $offer_type = $request->offer_type;
        $age_group = $request->age_group;
        $mass_budget = $request->mass_budget;
        $premium_budget = $request->premium_budget;
        $star_budget = $request->star_budget;
        $campaign_day_to_run = '';
        if(is_array($request->campaign_day_to_run)){
            $campaign_day_to_run = implode(',', $request->campaign_day_to_run);
        }
        else{
            if($request->campaign_day_to_run==""){
                $campaign_day_to_run = 'all';
            }else{
                $campaign_day_to_run = $request->campaign_day_to_run;
            }
        }
        $campaign_device = '';
        if(is_array($request->campaign_device)){
            $campaign_device = implode(',', $request->campaign_device);
        }
        else{
            $campaign_device = 'all';
        }
        $active_between = '';
        if(is_array($request->active_between)){
            $active_between = implode(',', $request->active_between);
        }
        else{
            $active_between = 'all';
        }
        $campaign_name = $request->campaign_name;
        $location_selected = $request->location_selected;
        $gender = $request->gender;
        $total_budget = $request->total_budget;
        $campaign_start_date = $request->campaign_start_date;
        $campaign_start_time = "00:00";
        $start_date = date('Y-m-d H:i:s', strtotime("$campaign_start_date $campaign_start_time"));
        $campaign_end_date = $request->campaign_end_date;
        $campaign_end_time = "23:55";
        $end_date = date('Y-m-d H:i:s', strtotime("$campaign_end_date $campaign_end_time"));
        $brand_title = $_POST['brand_title'];
        $insert_offer = $this->DB2->query("insert into home_cp_offers(offer_title, offer_desc,brand_id, created_by_brand_id, offer_type,
       campaign_name, offer_exclusive_to_shouut, age_group, gender, active_between, campaign_start_date,
                                         campaign_end_date, campaign_day_to_run, total_budget, mass_budget, premium_budget, star_budget,status,
                                         is_deleted, created_on, platform_filter,poll_brand_title ,is_web_offer ) values('$offer_title', '$offer_desc',
                                         '$offer_brand_id','$brand_id','$offer_type','$campaign_name', '0',
                                         '$age_group', '$gender', '$active_between', '$start_date',
                                         '$end_date', '$campaign_day_to_run', '$total_budget', '$mass_budget', '$premium_budget', '$star_budget','1', '0', now(),
                                         '$campaign_device' ,'$brand_title', '$is_shouut_offer_check')");

        $cp_offer_id = $this->DB2->insert_id();

        //subtract budget from brand account
        $subtract_budget = $this->DB2->query("update cp_brand_budget set current_balance = (current_balance -
        $total_budget)  WHERE brand_id = '$brand_id'");
        //insert in cp_offer_location table
        foreach($location_selected as $location_id1){
            $insert_location = "insert into home_cp_offers_users(cp_offer_id, user_uid) VALUES
                ('$cp_offer_id','$location_id1')";
            $query3 = $this->DB2->query($insert_location);
        }
        $pending_array = array();
        $pen_inc = 0;

        $question  = $request->questions;
        foreach($question as $question1){
            $question_value =  $question1->question;
            $question_option_choice =  $question1->question_option_choice;
            $insert_question = $this->DB2->query("insert into home_cp_survey_question(cp_offer_id, question, created_on, option_choice)
            values('$cp_offer_id', '$question_value', now(), '$question_option_choice')");
            $question_id = $this->DB2->insert_id();
            $option_array = $question1->options;
            foreach($option_array as $option_array1){
                $option_value =  $option_array1->value;
                $option_next_value =  $option_array1->next_question;
                $next_ques = '';
                $next_val = '';
                if($option_next_value != ''){
                    $next_val = substr($option_next_value, -1);
                    if($next_val == '0'){
                        $next_ques = 1;
                    }
                }

                $insert_option = $this->DB2->query("insert into home_cp_survey_option(ques_id, option_val, created_on, next_ques)
                values('$question_id', '$option_value', now(), '$next_ques')");
                $option_id = $this->DB2->insert_id();
                if($next_val != '' && $next_val != '0'){
                    $pending_array[$pen_inc]["question_id"] = $question_id;
                    $pending_array[$pen_inc]["option_id"] = $option_id;
                    $pending_array[$pen_inc]["next_ques"] = $next_val;
                    $pen_inc++;
                }
            }
        }
        $get_question_id_of_offer = $this->DB2->query("select ques_id from home_cp_survey_question where cp_offer_id =
        '$cp_offer_id'");
        $question_id_list = array();
        $i = 1;
        foreach($get_question_id_of_offer->result() as $valq)
        {
            $question_id_list[$i]= $valq->ques_id;
            $i++;
        }
        foreach($pending_array as $pending_array1){
            $next_question = $pending_array1['next_ques'];
            $option_updated = $pending_array1['option_id'];
            $next_question_id_update = $question_id_list[$next_question];
            $update = $this->DB2->query("update home_cp_survey_option set next_ques = '$next_question_id_update' WHERE
            option_id = '$option_updated'");
        }
        $data = array();
        $data['resultCode'] = '1';
        $data['offer_id'] = $cp_offer_id;
        $data['resultMessage'] = "Success";
        return $data;
    }

    public function add_campaign_survey_homewifi_offer_image($request){
        $data = array();
        $offer_id = $_POST['offer_id'];
        $original = '';
        $logo = '';
        $small = '';
        $medium = '';
        $large = '';
        if (isset($_FILES) && !empty($_FILES['image_file']['name'])) {
            $filename = $_FILES['image_file']['name'];
            $tmp = $_FILES['image_file']['tmp_name'];
            $path = 'assets/campaign/images/';
            //$amazonpath=AMAZONPATH.'campaign/images/';
            $flash_logo_name = $this->resize_image($path,$tmp,$filename);
            $original = $flash_logo_name[0];
            $logo = $flash_logo_name[1];
            $small = $flash_logo_name[2];
            $medium = $flash_logo_name[3];
            $large = $flash_logo_name[4];

        }
        $original_poll = '';
        $logo_poll = '';
        $small_poll = '';
        $medium_poll = '';
        $large_poll = '';
        if (isset($_FILES) && !empty($_FILES['poll_logo']['name'])) {
            $filename = $_FILES['poll_logo']['name'];
            $tmp = $_FILES['poll_logo']['tmp_name'];
            $path = 'assets/campaign/poll_logo/';
            //$amazonpath=AMAZONPATH.'campaign/poll_logo/';
            $flash_logo_name = $this->resize_image($path,$tmp,$filename);
            $original_poll = $flash_logo_name[0];
            $logo_poll = $flash_logo_name[1];
            $small_poll = $flash_logo_name[2];
            $medium_poll = $flash_logo_name[3];
            $large_poll = $flash_logo_name[4];
        }
        $update = $this->DB2->query("update home_cp_offers set image_original = '$original', image_logo = '$logo',
        image_small = '$small', image_medium = '$medium', image_large = '$large', poll_original =
        '$original_poll', poll_large = '$large_poll', poll_medium = '$medium_poll', poll_small =
        '$small_poll', poll_logo = '$logo_poll' WHERE
        cp_offer_id =
        '$offer_id'");
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
        return $data;
    }
    public function dashboard_active_campaign_list_homewifi(){
        $data = array();
        if($this->session->userdata('brandid')){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from home_cp_offers as cpo where created_by_brand_id = '$brandid' and status = '1' and is_deleted = '0' and campaign_end_date > now() ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                if($row->offer_type == "offer_add"){
                    $data[$i]['color_class'] = 'yellow';
                }
                elseif($row->offer_type == 'video_add'){
                    $data[$i]['color_class'] = 'grey';
                }
                else{
                    $data[$i]['color_class'] = 'red';
                }

                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.user_uid from home_cp_offers_users as cpol  where cpol.cp_offer_id = '$row->cp_offer_id'");
                $toal_location = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = '';
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;

                $data[$i]['days_left'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days
                Left";
                $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24));
                $i++;
            }
        }
        return $data;
    }
    
    public function dashboard_expire_campaign_list_homewifi(){
        $data = array();
        if($this->session->userdata('brandid')){
            $brandid = $this->session->userdata('brandid');
            $query = $this->DB2->query("select cpo.campaign_end_date,cpo.cp_offer_id,cpo.campaign_name, cpo.offer_type from home_cp_offers as cpo where created_by_brand_id = '$brandid'  AND (status = '0' OR is_deleted = '1'  OR campaign_end_date < now()) ORDER BY cp_offer_id DESC ");
            $i = 0;
            foreach($query->result() as $row){
                $data[$i]['offer_id'] = $row->cp_offer_id;
                $data[$i]['campaign_name'] = $row->campaign_name;
                $data[$i]['offer_type'] = strtoupper(substr(str_replace('_', ' ',$row->offer_type), 0, -1));
                $get_location_detail = $this->DB2->query("select cpol.user_uid from home_cp_offers_users as cpol  where cpol.cp_offer_id = '$row->cp_offer_id'");
                $toal_location = 0;
                $city = array();
                $j = 0;
                $city = array();
                foreach($get_location_detail->result() as $row1){
                    $toal_location = $toal_location + 1;
                    $city[] = ucwords(strtolower($row1->city));
                }
                $city = array_values(array_unique($city));
                $city_name = array_slice($city, 0, 3);
                $city_name_more = array_slice($city, 3, count($city));
                $more_city = '';
                if(count($city_name_more) > 0){
                    $more_city = "+".count($city_name_more)."more";
                }
                $data[$i]['city_name'] = implode(", ",$city_name)." ".$more_city;
                $data[$i]['total_location'] = $toal_location;
                $current_time =  date("Y-m-d H:i:s");
                if(strtotime($current_time) > strtotime($row->campaign_end_date))//expire
                {
                    $data[$i]['days'] = "Expired ".floor((time() - strtotime($row->campaign_end_date)) / (60 * 60 * 24)). " days ago";
                }
                else{
                    $data[$i]['days'] = floor(( strtotime($row->campaign_end_date) -time()) / (60 * 60 * 24)). " Days Left";
                }
                $i++;
            }
        }
        return $data;
    }
    
    public function campaign_graph_data_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
//age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
// time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;
        $total_redirect = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $data[$j]['actual_date'] = $row->viewed_on;
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $data[$j]['total_view'] = 0;
            $data[$j]['total_redirec'] = 0;

            $j++;
        }
        $query_get_data = $this->DB2->query("SELECT id,is_redirected,DATE(viewed_on) AS viewed_on
      FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter");
        foreach($query_get_data->result() as $query_get_data_row){
            $total_view = $total_view+1;
            $is_download = 0;
            if($query_get_data_row->is_redirected == '1'){
                $total_redirect = $total_redirect+1;
                $is_download = 1;
            }
            foreach($data as $key=>$value) {
                if($query_get_data_row->viewed_on == $value['actual_date']){
                    $data[$key]['total_view'] = $data[$key]['total_view']+1;
                    $data[$key]['total_redirec'] = $data[$key]['total_redirec']+$is_download;
                    break;
                }
            }
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
         
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_redirect'] = $total_redirect;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    
    public function get_campaign_visitor_detail_homewifi($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }


        $data = array();
        $query = $this->DB2->query("select user_id, viewed_on, is_redirected from home_cp_dashboard_analytics WHERE  DATE(viewed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter  ORDER BY id desc");
        $i = 0;
        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $data['users'][$i]['s_no'] = $i+1;
                $user_id = $row->user_id;
                $user_detail = $this->db->query("select firstname, lastname, mobile from sht_users where uid = '$user_id'");
                if($user_detail->num_rows() > 0){
                    $row_check = $user_detail->row_array();
                    $data['users'][$i]['user_name'] = $row_check['firstname'].' '.$row_check['lastname'] ;
                    $data['users'][$i]['user_mobile'] = substr($row_check['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->viewed_on) );
                    //$gender = $check[0]['gender'];
                    $gender = '';
                    $gender = trim($gender);
                    if($gender != ''){
                        if($gender[0] == 'm' || $gender[0] == 'M'){
                            $gender = 'Male';
                        }
                        elseif($gender[0] == 'f' || $gender[0] == 'F'){
                            $gender = 'Female';
                        }
                        else{
                            $gender = '';
                        }
                    }
                    else{
                        $gender = '';
                    }
    
                    $data['users'][$i]['gender'] = $gender;
                    $redirected = 'No';
                    if($row->is_redirected == '1'){
                        $redirected = 'Yes';
                    }
                    $data['users'][$i]['redirected'] = $redirected;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;
    }
    
    public function setcampaignId_homewifi($request){
        $data = array();
        $offer_id = $request->offer_id;
        $this->session->set_userdata('campaignid', $offer_id);
        $query = $this->DB2->query("select status, is_deleted, campaign_end_date from home_cp_offers WHERE cp_offer_id =
         '$offer_id'");
        $add_location = 0;
        $stop = 0;
        $pause = 0;
        $play = 0;
        if($query->num_rows() > 0){
            $row = $query->row_array();
            $is_live = 0;
            if(strtotime(date('Y-m-d H:i:s')) < strtotime($row['campaign_end_date'])){
                $is_live = 1;
            }
            if($row['status'] ==1 && $row['is_deleted'] == 0 && $is_live == 1){
                $add_location = 1;
                $stop = 1;
                $pause = 1;
            }
            if($row['status'] ==0 && $row['is_deleted'] == 0 && $is_live == 1){
                $play = 1;
            }
        }

        $data['resultCode'] = '1';
        $data['add_location'] = $add_location;
        $data['stop'] = $stop;
        $data['pause'] = $pause;
        $data['play'] = $play;
        return $data;

    }
    
    public function pauseCampaign_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }

        $data = array();
        $query = $this->DB2->query("update home_cp_offers set status = '0' WHERE cp_offer_id = '$campaign_id'");
        $data['resultCode'] = 1;
        return $data;
    }
    public function resumeCampaign_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }

        $data = array();
        $query = $this->DB2->query("update home_cp_offers set status = '1' WHERE cp_offer_id = '$campaign_id'");
        $data['resultCode'] = 1;
        return $data;
    }
    public function stopCampaign_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }

        $data = array();
        $query = $this->DB2->query("update home_cp_offers set campaign_end_date = now() WHERE cp_offer_id = '$campaign_id'");
        $data['resultCode'] = 1;
        return $data;
    }
    public function cp_offer_not_attached_user(){
        $data = array();

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        $location_ids_attached = array();
        $get_attached_id = $this->DB2->query("select user_uid from home_cp_offers_users WHERE cp_offer_id = '$campaign_id'");
        foreach($get_attached_id->result() as $row){
            $location_ids_attached[] = $row->user_uid;
        }
        $location_ids_attached='"'.implode('", "', $location_ids_attached).'"';
        $provider_where = '';
        if($this->session->userdata('provider_id')){
            $provider_id = $this->session->userdata('provider_id');
			if($provider_id != '0'){
					$provider_where = " AND isp_uid = '$provider_id'";
			}
            
        }
        $query= $this->db->query("select id, isp_uid, uid, firstname, lastname from sht_users WHERE  uid NOT IN ($location_ids_attached) $provider_where");
        foreach($query->result() as $row){
            $data[] = $row;
        }
        return $data;
    }
    // new isp changes done
    public function cp_offer_new_user($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
            //insert in cp_offer_location table
            foreach($request->location_selected as $location_id1){
                $insert_location = "insert into home_cp_offers_users(cp_offer_id, user_uid) VALUES
                ('$campaign_id','$location_id1')";
                $query3 = $this->DB2->query($insert_location);
            }
        }
        $data = array();
        $data['resultCode'] = 1;
        return $data;
    }
    
    public function campaign_graph_data_offerlisting_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_dashboard_analytics_offer_listing cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        //echo "<pre>";print_r($query);die;
        $j = 0;
        $total_view = 0;
        $total_detail_view = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM home_cp_dashboard_analytics_offer_listing cdal2 WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_detail_view FROM home_cp_dashboard_analytics_offer_detail cdal2 WHERE DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id ='$campaign_id'
               $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_detail_view'] = $row1_total_view['total_detail_view'];
            $total_detail_view = $total_detail_view+$row1_total_view['total_detail_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
           
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_detail_view'] = $total_detail_view;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    public function get_campaign_bannerflash_visitor_detail_homewifi($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }

        $data = array();
        $query = $this->DB2->query("select user_id, redeemed_on, coupon_code from home_cp_offer_reddem WHERE  DATE(redeemed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter");
        $i = 0;

        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $data['users'][$i]['s_no'] = $i+1;
                $user_id = $row->user_id;
                $user_detail = $this->db->query("select firstname, lastname, mobile from sht_users where uid = '$user_id'");
                if($user_detail->num_rows() > 0){
                    $row_check = $user_detail->row_array();
                    $data['users'][$i]['coupon_code'] = $row->coupon_code;
                    $data['users'][$i]['user_name'] = $row_check['firstname'].' '.$row_check['lastname'] ;
                    $data['users'][$i]['user_mobile'] = substr($row_check['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->redeemed_on) );
                    //$gender = $check[0]['gender'];
                    $gender = '';
                    $gender = trim($gender);
                    if($gender[0] == 'm' || $gender[0] == 'M'){
                        $gender = 'Male';
                    }
                    elseif($gender[0] == 'f' || $gender[0] == 'F'){
                        $gender = 'Female';
                    }
                    else{
                        $gender = '';
                    }
                    $data['users'][$i]['gender'] = $gender;
    
    
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;

    }
    
    public function campaign_graph_data_bannerflash_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_dashboard_analytics_bannerflash_listing cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");


        $j = 0;
        $total_view = 0;
        $total_detail_view = 0;
        $total_redeem = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM home_cp_dashboard_analytics_bannerflash_listing cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_detail_view_query = $this->DB2->query("SELECT COUNT(*) AS total_detail_view FROM
            home_cp_dashboard_analytics_bannerflash_detail cdal2 WHERE
              DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $time_filter
              $device_filter");
            $row1_total_view = $total_detail_view_query->row_array();
            $data[$j]['total_detail_view'] = $row1_total_view['total_detail_view'];
            $total_detail_view = $total_detail_view+$row1_total_view['total_detail_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_redeem FROM home_cp_offer_reddem cdal2 WHERE
              DATE('$row->viewed_on')=DATE(cdal2.redeemed_on) AND cdal2.cp_offer_id = '$campaign_id'
             $time_filter $device_filter");
            $row2_total_view = $total_redeem_query->row_array();
            $data[$j]['total_redeem'] = $row2_total_view['total_redeem'];
            $total_redeem = $total_redeem+$row2_total_view['total_redeem'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_detail_view'] = $total_detail_view;
        $return_array['total_redeem'] = $total_redeem;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    public function campaign_graph_data_poll_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;

        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;

           /* $data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;*/
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM home_cp_dashboard_analytics cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;

        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    
    public function campaign_graph_data_poll_question_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $data = array();

        $query = $this->DB2->query("select id,question, option1, option2, option3, option4 from home_cp_offers_question where cp_offer_id = '$campaign_id'");
        $i = 0;
        $question_record = array();
        foreach($query->result() as $row){
            $ansid = $row->id;
            $option1_count = 0;
            $option2_count = 0;
            $option3_count = 0;
            $option4_count = 0;
            $query1 = $this->DB2->query("select id, option_id from home_cp_poll_answer where question_id = '$ansid'");
            foreach($query1->result() as $row1){
                if($row1->option_id == 1){
                    $option1_count = $option1_count+1;
                }
                elseif($row1->option_id == 2){
                    $option2_count = $option2_count+1;
                }
                elseif($row1->option_id == 3){
                    $option3_count = $option3_count+1;
                }
                elseif($row1->option_id == 4){
                    $option4_count = $option4_count+1;
                }
            }
            $question_record[$i]['question'] = $row->question;
            $question_record[$i]['option1'] = $row->option1;
            $question_record[$i]['option1_count'] = $option1_count;
            $question_record[$i]['option2'] = $row->option2;
            $question_record[$i]['option2_count'] = $option2_count;
            $question_record[$i]['option3'] = $row->option3;
            $question_record[$i]['option3_count'] = $option3_count;
            $question_record[$i]['option4'] = $row->option4;
            $question_record[$i]['option4_count'] = $option4_count;
            $i++;
        }
        $data['resultCode'] = 1;
        $data['question_record'] = $question_record;
        return $data;
    }
    
    public function campaign_graph_data_captcha_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;
        $total_accurate = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM home_cp_dashboard_analytics cdal2 WHERE cdal2.cp_offer_id = '$campaign_id' AND
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) $gender_where $age_group_filter $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_accurate FROM home_cp_dashboard_analytics cdal2 WHERE cdal2.cp_offer_id = '$campaign_id'
            AND cdal2.captcha_verified = '1' AND DATE('$row->viewed_on')=DATE(cdal2.viewed_on) $gender_where $age_group_filter $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_accurate'] = $row1_total_view['total_accurate'];
            $total_accurate = $total_accurate+$row1_total_view['total_accurate'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_accurate'] = $total_accurate;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    
    public function campaign_graph_data_appdownload_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");

        $j = 0;
        $total_view = 0;
        $total_redirect = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['actual_date'] = $row->viewed_on;
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $data[$j]['total_view'] = 0;
            $data[$j]['total_redirec'] = 0;

            $j++;
        }
        $query_get_data = $this->DB2->query("SELECT id,is_downloaded,DATE(viewed_on) AS viewed_on
      FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter");
        foreach($query_get_data->result() as $query_get_data_row){
            $total_view = $total_view+1;
            $is_download = 0;
            if($query_get_data_row->is_downloaded == '1'){
                $total_redirect = $total_redirect+1;
                $is_download = 1;
            }
            foreach($data as $key=>$value) {
                if($query_get_data_row->viewed_on == $value['actual_date']){
                    $data[$key]['total_view'] = $data[$key]['total_view']+1;
                    $data[$key]['total_redirec'] = $data[$key]['total_redirec']+$is_download;
                    break;
                }
            }
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_redirect'] = $total_redirect;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );

        return $return_array;
    }
    
    public function get_campaign_visitor_detail_appdownload_homewifi($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        //echo $device_filter;

        $data = array();
        $query = $this->DB2->query("select user_id, viewed_on, is_redirected from home_cp_dashboard_analytics WHERE  DATE(viewed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' AND is_downloaded = '1'  $gender_where $age_group_filter
        $time_filter $device_filter  ORDER BY id desc");

        $i = 0;
        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $data['users'][$i]['s_no'] = $i+1;
                $user_id = $row->user_id;
                $user_detail = $this->db->query("select firstname, lastname, mobile from sht_users where uid = '$user_id'");
                if($user_detail->num_rows() > 0){
                    $row_check = $user_detail->row_array();
                    $data['users'][$i]['user_name'] = $row_check['firstname'].' '.$row_check['lastname'] ;
                    $data['users'][$i]['user_mobile'] = substr($row_check['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->viewed_on) );
                    //$gender = $check[0]['gender'];
                    $gender = '';
                    $gender = trim($gender);
                    if($gender != ''){
                        if($gender[0] == 'm' || $gender[0] == 'M'){
                            $gender = 'Male';
                        }
                        elseif($gender[0] == 'f' || $gender[0] == 'F'){
                            $gender = 'Female';
                        }
                        else{
                            $gender = '';
                        }
                    }
                    else{
                        $gender = '';
                    }
    
                    $data['users'][$i]['gender'] = $gender;
                    $redirected = 'No';
                    if($row->is_redirected == '1'){
                        $redirected = 'Yes';
                    }
                    $data['users'][$i]['redirected'] = $redirected;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;
    }
    
    public function campaign_graph_data_redeemable_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y/m/d")) < strtotime($date_to)){
                    $date_to = date("Y/m/d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();


        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
             FROM home_cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;
        $total_redeem = 0;
        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM home_cp_dashboard_analytics cdal2 WHERE
            DATE
            ('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id' $gender_where
            $age_group_filter $time_filter $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];

            $total_redeem_query = $this->DB2->query("SELECT COUNT(*) AS total_redeem FROM home_cp_offer_reddem cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.redeemed_on) AND cdal2.cp_offer_id = '$campaign_id'
            $gender_where $age_group_filter $time_filter $device_filter");
            $row1_total_view = $total_redeem_query->row_array();
            $data[$j]['total_redeem'] = $row1_total_view['total_redeem'];
            $total_redeem = $total_redeem+$row1_total_view['total_redeem'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
           
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['total_redeem'] = $total_redeem;
        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    
    public function get_campaign_redeemable_visitor_detail_homewifi($request){

        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }
        //gender filter
        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }

        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
// time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(redeemed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();
        $query = $this->DB2->query("select user_id, redeemed_on, coupon_code from home_cp_offer_reddem WHERE  DATE(redeemed_on) BETWEEN
        '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter");
        $i = 0;


        // echo $query->num_rows();
        if($query->num_rows() > 0){
            $data['resultCode'] = '1';
            $data['resultMessage'] = "Success";

            foreach($query->result() as $row){
                $data['users'][$i]['s_no'] = $i+1;
                $user_id = $row->user_id;
                $user_detail = $this->db->query("select firstname, lastname, mobile from sht_users where uid = '$user_id'");
                if($user_detail->num_rows() > 0){
                    $row_check = $user_detail->row_array();
                    $data['users'][$i]['user_name'] = $row_check['firstname'].' '.$row_check['lastname'] ;
                    $data['users'][$i]['user_mobile'] = substr($row_check['mobile'], 0, 6)."****";
                    $data['users'][$i]['viewed_on'] = date("d-m-Y H:i", strtotime( $row->redeemed_on) );
                    //$gender = $check[0]['gender'];
                    $gender = '';
                    $gender = trim($gender);
                    if($gender != ''){
                        if($gender[0] == 'm' || $gender[0] == 'M'){
                            $gender = 'Male';
                        }
                        elseif($gender[0] == 'f' || $gender[0] == 'F'){
                            $gender = 'Female';
                        }
                        else{
                            $gender = '';
                        }
                    }
                    else{
                        $gender = '';
                    }
                    $data['users'][$i]['gender'] = $gender;
    
                    $data['users'][$i]['coupon_code'] = $row->coupon_code;
                    $i++;
                }
                
            }
        }else{
            $data['resultCode'] = '0';
            $data['resultMessage'] = "No user";
        }

        return $data;

    }
    
    public function campaign_graph_data_survey_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM home_cp_survey_answer cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND
            cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;

        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM home_cp_survey_answer cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select  offer_type, campaign_name from home_cp_offers where cp_offer_id = '$campaign_id'");
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from home_cp_budget_used where cp_offer_id = '$campaign_id'");

        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }
    
    public function campaign_graph_data_survey_question_homewifi($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from home_cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                $device_filter = " AND osinfo in ($device) ";
            }
        }

        $data = array();
        $query = $this->DB2->query("select ques_id, question from home_cp_survey_question where cp_offer_id = '$campaign_id'");
        $i = 0;
        $question_record = array();
        foreach($query->result() as $row){
            $question_id = $row->ques_id;
            $question_record[$i]['question'] = $row->question;
            $option_query = $this->DB2->query("select * from home_cp_survey_option where ques_id = '$question_id'");
            $j = 1;
            $analytics_array = array();
            $k = 0;
            foreach($option_query->result() as $row1){
                // get option click
                $option_id = $row1->option_id;
                $option_count = $this->DB2->query("select count(*) as total_ans from home_cp_survey_answer where cp_offer_id = '$campaign_id'
				and question_id = '$question_id' and option_id = '$option_id' and DATE(viewed_on) BETWEEN '$date_from' AND '$date_to'
				$gender_where $age_group_filter $time_filter $device_filter");
                $total_click = 0;
                if($option_count->num_rows() > 0){
                    $row2 = $option_count->row_array();
                    $total_click = $row2['total_ans'];
                }

                $analytics_array[$k]['option'.$j] = $row1->option_val;
                $analytics_array[$k]['option'.$j.'_count'] = (int)$total_click;
                $k++;
                //$question_record[$i]['option'.$j] = $row1->option_val;
                //$question_record[$i]['option'.$j.'_count'] = $total_click;

                $j++;
            }

            $question_record[$i]['options'] = $analytics_array;
            $i++;
        }

        $data['resultCode'] = 1;
        $data['question_record'] = $question_record;
        //echo "<pre>";print_r($data);
        return $data;
    }
    
    public function cp_offer_attached_location(){
        $data = array();
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    
	}else{
	    $campaign_id = '';
	    if($this->session->userdata('campaignid')){
		$campaign_id = $this->session->userdata('campaignid');
	    }
	    $location_ids_attached = array();
	    $get_attached_id = $this->DB2->query("select location_id from cp_offers_location WHERE cp_offer_id = '$campaign_id'");
	    foreach($get_attached_id->result() as $row){
		$location_ids_attached[] = $row->location_id;
	    }
	    $location_ids_attached='"'.implode('", "', $location_ids_attached).'"';
	    $provider_where = '';
	    if($this->session->userdata('provider_id')){
		$provider_id = $this->session->userdata('provider_id');
			    if($provider_id != '0'){
					    $provider_where = " AND provider_id = '$provider_id'";
			    }
		
	    }
	    $query= $this->DB2->query("select id, location_name, city, region_id, location_type from wifi_location WHERE is_deleted='0' AND
	    status = '1' AND location_brand = 'sh' and id  IN ($location_ids_attached) $provider_where");
	    foreach($query->result() as $row){
		$data[] = $row;
	    }
	}
        
        return $data;
    }
    
    // new isp changes done
    public function cp_offer_remove_location($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
            //insert in cp_offer_location table
            foreach($request->location_selected as $location_id1){
                //$insert_location = "insert into cp_offers_location(cp_offer_id, location_id) VALUES
                //('$campaign_id','$location_id1')";
                //$query3 = $this->DB2->query($insert_location);
		$this->DB2->query("delete from cp_offers_location where cp_offer_id = '$campaign_id' and location_id = '$location_id1'");
            }
        }
        $data = array();
        $data['resultCode'] = 1;
        return $data;
    }
public function add_audit_program($request){
        $data = array();
        $offer_type = $_POST['offer_type'];
        $campaign_name = $_POST['campaign_name'];
	$audit_program_id = $_POST['audit_program_id'];
	$created_by_location = 0;
	$offer_brand_id = '';
        $brand_id = '';
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != ''){
	    $created_by_location = $this->session->userdata('location_uid');
	}
	if($this->session->userdata('brandid')){
            $brand_id = $this->session->userdata('brandid');
            $offer_brand_id = $this->session->userdata('brandid');
        }
	if($audit_program_id == '0' || $audit_program_id == '')
	{
	    // insert
	    $insert_data = array(
	       'created_by_location' => $created_by_location,
	       'brand_id' => $offer_brand_id,
	       'created_by_brand_id' => $brand_id,
	       'offer_type' => $offer_type,
	       'offer_type' => $offer_type,
	       'campaign_name' => $campaign_name,
	       'status' => '0',
	       'created_on' => date('Y-m-d H:i:s')
	    );
	    $insert = $this->DB2->insert('cp_offers', $insert_data);
	    $audit_program_id = $this->DB2->insert_id();
	    // for log
	    $this->retail_audit_log_record('PROGRAM_CREATE','0',$audit_program_id);
	}
	else
	{
	    // update
	    $update_data = array(
	       'campaign_name' => $campaign_name,
	       'updated_on' => date('Y-m-d H:i:s'),
	    );
	    $this->DB2->where('cp_offer_id', $audit_program_id);
	    $update = $this->DB2->update('cp_offers', $update_data);
	}
        
        $data['resultCode'] = '1';
        $data['resultMessage'] = "Success";
	$data['program_id'] = $audit_program_id;
        return $data;
    }
    public function get_audit_program($request){
        $data = array();
        $audit_program_id = $request->audit_program_id;
	$campaign_name = '';
	$campaign_start_date = '';
	$campaign_end_date = '';
	$is_perpetual = 0;
	$program_code = '';
	$is_checklist_all = '0';
	$this->DB2->select('campaign_name, campaign_start_date, campaign_end_date, is_perpetual, program_code,is_checklist_all')->from('cp_offers');
	$this->DB2->where('cp_offer_id', $audit_program_id);
	$get = $this->DB2->get();
	if($get->num_rows() > 0)
	{
	    $row = $get->row_array();
	    $campaign_name = $row['campaign_name'];
	    $is_perpetual = $row['is_perpetual'];
	    $program_code = $row['program_code'];
	    $start_date =  strtotime($row['campaign_start_date']);
	    if($start_date != '')
	    {
		$campaign_start_date = date('d.m.Y',strtotime($row['campaign_start_date']));
	    }
	    $end_date =  strtotime($row['campaign_end_date']);
	    if($end_date != '')
	    {
		$campaign_end_date = date('d.m.Y',strtotime($row['campaign_end_date']));
	    }
	    $is_checklist_all = $row['is_checklist_all'];
	}
        $data['campaign_name'] = $campaign_name;
	$data['campaign_start_date'] = $campaign_start_date;
	$data['campaign_end_date'] = $campaign_end_date;
	$data['is_perpetual'] = $is_perpetual;
	$data['program_code'] = $program_code;
	$data['is_checklist_all'] = $is_checklist_all;
        return $data;
    }
    
    public function retail_audit_location($request){
        $data = array();
	$audit_program_id = $request->audit_program_id;
	$previous_location_id = array();
	$this->DB2->select("id, location_id")->from('cp_offers_location');
	$this->DB2->where(array('cp_offer_id' => $audit_program_id, 'is_deleted' => '0'));
	$get = $this->DB2->get();
	foreach($get->result() as $row_old_ques)
	{
	    $previous_location_id[] = $row_old_ques->location_id;
	}
	
        $brandid = '';
        if($this->session->userdata('brandid') && $this->session->userdata('location_uid') == '')
	{
            $brandid = $this->session->userdata('brandid');
        }
	if($this->session->userdata('location_uid') && $this->session->userdata('location_uid') != '')
	{
	    $location_uid = $this->session->userdata('location_uid');
	    $query= $this->DB2->query("select id, location_name, location_uid, city, region_id, location_type, zone_name from wifi_location WHERE is_deleted='0' AND status = '1' AND  location_uid = '$location_uid' AND location_type = '13' order by location_name asc");
	    $i = 0;
	    foreach($query->result() as $row){
		$selected = '';
		if(in_array($row->id, $previous_location_id))
		{
		    $selected = 'selected';
		}
		$data[$i]['id'] = $row->id;
		$data[$i]['location_name'] = $row->location_name."(".$row->location_uid.")";
		
		$data[$i]['city'] = $row->city;
		$data[$i]['region_id'] = $row->region_id;
		$data[$i]['zone_name'] = $row->zone_name;
		$data[$i]['location_type'] = $row->location_type;
		$data[$i]['selected'] = $selected;
		$i++;
	    }
	}
	else
	{
	    $provider_where = '0';
	    if($this->session->userdata('provider_id')){
		$provider_id = $this->session->userdata('provider_id');
		$provider_where = " AND provider_id = '$provider_id'";	
	    }
	    else{
		$provider_where = " AND provider_id = '$provider_id'";	
	    }
	    $query= $this->DB2->query("select id, location_name,location_uid, city, region_id,zone_name, location_type from wifi_location WHERE is_deleted='0' AND status = '1' AND location_brand = 'sh' AND location_type = '13' $provider_where order by location_name asc");
	    $i = 0;
	    foreach($query->result() as $row){
		$selected = '';
		if(in_array($row->id, $previous_location_id))
		{
		    $selected = 'selected';
		}
		$data[$i]['id'] = $row->id;
		$data[$i]['location_name'] = $row->location_name."(".$row->location_uid.")";
		$data[$i]['city'] = $row->city;
		$data[$i]['region_id'] = $row->region_id;
		$data[$i]['zone_name'] = $row->zone_name;
		$data[$i]['location_type'] = $row->location_type;
		$data[$i]['selected'] = $selected;
		$i++;
	    }
	}
        //echo "<pre>";print_r($data);die;
        return $data;
    }
    
    public function add_audit_program_location($request){
        $data = array();
	$audit_program_id = $_POST['audit_program_id'];
	$locations =  explode(',', $_POST['location_selected']);
	
	
	$previous_location_id = array();
	$this->DB2->select("id, location_id")->from('cp_offers_location');
	$this->DB2->where(array('cp_offer_id' => $audit_program_id, 'is_deleted' => '0'));
	$get = $this->DB2->get();
	foreach($get->result() as $row_old_ques)
	{
	    $previous_location_id[] = $row_old_ques->location_id;
	}
	foreach($locations as $location_id1)
	{
	    if($location_id1 != '')
	    {
		if(in_array($location_id1, $previous_location_id))
		{// update old
		    $pos = array_search($location_id1, $previous_location_id);
		    unset($previous_location_id[$pos]);
		    $previous_location_id = array_values($previous_location_id);
		}
		else
		{
		    // insert new
		    $insert_data = array(
			'cp_offer_id' => $audit_program_id,
			'location_id' => $location_id1,
			'created_on' => date('Y-m-d H:i:s')
		    );
		    $insert = $this->DB2->insert('cp_offers_location', $insert_data);
		}
	    }
	    
	}
	// remove deleted store
	foreach($previous_location_id as $previous_location_id1)
	{
	    // update
	    $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s'),
	    );
	    $this->DB2->where(array('cp_offer_id'=>$audit_program_id, 'location_id' => $previous_location_id1));
	    $update = $this->DB2->update('cp_offers_location', $update_data);
	}
	$data['resultCode'] = '1';
        return $data;
    }


    public function add_audit_program_date($request){
        $data = array();
	$audit_program_id = $_POST['audit_program_id'];
	$is_perpetual = $_POST['is_perpetual'];
	if($is_perpetual == '1')
	{
	    $start_date = date('Y-m-d H:i:s');
	    $end_date = '0000-00-00 00:00:00';
	    $this->DB2->select('campaign_start_date')->from('cp_offers');
	    $this->DB2->where(array('cp_offer_id'=>$audit_program_id));
	    $get = $this->DB2->get();
	    if($get->num_rows() > 0)
	    {
		$row = $get->row_array();
		if(strtotime($row['campaign_start_date']) > '0')
		{
		    $start_date = $row['campaign_start_date'];
		}
	    }
	}
	else
	{
	    $campaign_start_time = "00:00";
	    $campaign_start_date = $_POST['campaign_start_date'];
	    $start_date = date('Y-m-d H:i:s', strtotime("$campaign_start_date $campaign_start_time"));
	    $campaign_end_date = $_POST['campaign_end_date'];
	    $campaign_end_time = "23:55";
	    $end_date = date('Y-m-d H:i:s', strtotime("$campaign_end_date $campaign_end_time"));
	}
	
	$update_data = array(
	   'campaign_start_date' => $start_date,
	   'campaign_end_date' => $end_date,
	   'is_perpetual' => $is_perpetual,
	   'updated_on' => date('Y-m-d H:i:s'),
	);
	$this->DB2->where(array('cp_offer_id'=>$audit_program_id));
	$update = $this->DB2->update('cp_offers', $update_data);
	
	$data['resultCode'] = '1';
        return $data;
    }
    
    public function add_audit_program_code($request){
        $data = array();
	$resultCode = 1;
	$audit_program_id = $_POST['audit_program_id'];
	$program_code = $_POST['program_code'];
	// check in program table
	$this->DB2->select("cp_offer_id")->from("cp_offers");
	$this->DB2->where(array("program_code" => $program_code, "cp_offer_id !=" => $audit_program_id));
	$check = $this->DB2->get();
	if($check->num_rows() > 0)
	{
	    $resultCode = 0;
	}
	// check in location code and promoter code
	$this->DB2->select('id')->from("retailaudit_login_pin");
	$this->DB2->where(array('pin' => $program_code));
	$this->DB2->or_where(array('promoter_pin' => $program_code));
	$check1 = $this->DB2->get();
	if($check1->num_rows() > 0)
	{
	    $resultCode = 0;
	}
	if($resultCode == '1')
	{
	    $update_data = array(
		'program_code' => $program_code,
		'status' => '1',
		'updated_on' => date('Y-m-d H:i:s'),
	    );
	    $this->DB2->where(array('cp_offer_id'=>$audit_program_id));
	    $update = $this->DB2->update('cp_offers', $update_data);
	}
	$data['resultCode'] = $resultCode;
        return $data;
    }
    public function add_audit_program_sku($request){
        $data = array();
	$resultCode = 1;
	$sku_id = $_POST['sku_id'];
	$sku_name = $_POST['sku_name'];
	$sku_instruction = $_POST['sku_instruction'];
	$logn_shot = $_POST['logn_shot'];
	$close_up = $_POST['close_up'];
	$retail_sig = $_POST['retail_sig'];
	$other_proff = $_POST['other_proff'];
	$audit_program_id = $_POST['audit_program_id'];
	if($sku_id == 0 || $sku_id == '')
	{
	    // insert
	    $insert_data = array(
	       'sku_name' => $sku_name,
	       'sku_image' => '',
	       'sku_install_instruction' => $sku_instruction,
	       'long_shot' => $logn_shot,
	       'close_up' => $close_up,
	       'retailer_signature' => $retail_sig,
	       'other' => $other_proff,
	       'offer_id' => $audit_program_id,
	       'created_on' => date('Y-m-d H:i:s')
	    );
	    $insert = $this->DB2->insert('retailaudit_program_skudetail', $insert_data);
	    $sku_id = $this->DB2->insert_id();
	}
	else
	{
	    // update
	    $update_data = array(
	       'sku_name' => $sku_name,
	       'sku_install_instruction' => $sku_instruction,
	       'long_shot' => $logn_shot,
	       'close_up' => $close_up,
	       'retailer_signature' => $retail_sig,
	       'other' => $other_proff,
	       'offer_id' => $audit_program_id,
	       'updated_on' => date('Y-m-d H:i:s')
	    );
	    $this->DB2->where(array('id'=>$sku_id));
	    $update = $this->DB2->update('retailaudit_program_skudetail', $update_data);
	}
	
	if (isset($_FILES) && !empty($_FILES['sku_image']['name'])) {
	    $filename = $_FILES['sku_image']['name'];
            $tmp = $_FILES['sku_image']['tmp_name'];
            $path = 'assets/sku_images/';
	    $amazonpath=AMAZONPATH_360.'retail_audit/program_sku/';
	    
	    $extension = $this->getExtension($filename);
	    $extension = strtolower($extension);
	    $filename = date('Ymd') . time() . rand() . '.' . $extension;
	    move_uploaded_file($tmp, $path . $filename);
	    
	    $fname = $path . $filename;
	    $famazonname = $amazonpath. $filename;
	    $this->s3->putBucket(bucket, S3::ACL_PUBLIC_READ);
            $this->s3->putObjectFile($fname, bucket , $famazonname, S3::ACL_PUBLIC_READ) ;
	    unlink($fname);
	    $update_data = array(
	       'sku_image' => $filename,
	    );
	    $this->DB2->where(array('id'=>$sku_id));
	    $update = $this->DB2->update('retailaudit_program_skudetail', $update_data);
	}
	
	$data['resultCode'] = $resultCode;
        return $data;
    }
    public function get_audit_program_sku($request){
        $data = array();
	$resultCode = 1;
	$audit_program_id = $request->audit_program_id;
	$skus = array();
	$this->DB2->select('*')->from('retailaudit_program_skudetail');
	$this->DB2->where(array("offer_id" => $audit_program_id, "is_deleted" => '0'));
	$this->DB2->order_by("id DESC");
	$get = $this->DB2->get();
	$i = 0;
	foreach($get->result() as $row)
	{
	    $sku_image = '';
	    if($row->sku_image != '')
	    {
		$sku_image = 'https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/retail_audit/program_sku/'.$row->sku_image;
	    }
	    $skus[$i]['sku_id'] = $row->id;
	    $skus[$i]['sku_name'] = $row->sku_name;
	    $skus[$i]['sku_image'] = $sku_image;
	    $skus[$i]['sku_install_instruction'] = $row->sku_install_instruction;
	    
	    $skus[$i]['long_shot'] = $row->long_shot;
	    $skus[$i]['close_up'] = $row->close_up;
	    $skus[$i]['retailer_signature'] = $row->retailer_signature;
	    $skus[$i]['other'] = $row->other;
	    $i++;
	}
	$data['skus'] = $skus;
        return $data;
    }
    
    
    public function add_audit_program_sku_delete($request){
        $data = array();
	$resultCode = 1;
	$sku_id = $_POST['sku_id'];
	
	// insert
	    $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s')
	    );
	    $this->DB2->where(array('id'=>$sku_id));
	    $update = $this->DB2->update('retailaudit_program_skudetail', $update_data);
	$data['resultCode'] = $resultCode;
        return $data;
    }
    
    public function create_excel_program_location(){
		$data = array();
		$total_record = 0;
		$gen = '';
		//$audit_program_id = $request->audit_program_id;
		$audit_program_id = $_POST['audit_program_id'];
		$query = $this->DB2->query("select col.id, col.name, col.user_email, col.user_mobile, wl.location_name from cp_offers_location as col inner join wifi_location as wl on(col.location_id = wl.id) where col.is_deleted = '0' and cp_offer_id = '$audit_program_id'");
		$record_found = 0;
		if($query->num_rows() > 0){
			$record_found = 1;
			
			
			$this->load->library('excel');
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->createSheet();
				$objPHPExcel->setActiveSheetIndex(0);
				$styleArray = array(
					'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'name'  => 'Tahoma',
					'size' => 14
				));
				//Set sheet style
				$styleArray1 = array(
					'font'  => array(
					'bold'  => false,
					'color' => array('rgb' => '000000'),
					'name'  => 'Tahoma',
					'size' => 12
				));
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
				
				$objPHPExcel->getActiveSheet()->setTitle("Locations");
				
				//Set sheet columns Heading
				$objPHPExcel->getActiveSheet()->setCellValue('A1','ID');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Location Name');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('C1','User Name');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('D1','User Email');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray);
				$objPHPExcel->getActiveSheet()->setCellValue('E1','User Mobile');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray);
				
				$i = 1;
				$j = 2;// excel row number
			
			
			foreach($query->result() as $row){
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$row->id);
                $objPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$row->location_name);
                $objPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$row->name);
                $objPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$row->user_email);
                $objPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($styleArray1);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$row->user_mobile);
                $objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($styleArray1);
				
				$i++;
				$j++;
			}
			
			
			 $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save('assets/location_list.xlsx');
			
		}
		
		$data['audit_program_id'] = $audit_program_id;
		return $data;
    }
    
    public function get_total_location_total_user_assign(){
	$data = array();
	$audit_program_id = $_POST['audit_program_id'];
	//$audit_program_id = '212';
	$previous_location_id = array();
	$this->DB2->select("id, location_id, name")->from('cp_offers_location');
	$this->DB2->where(array('cp_offer_id' => $audit_program_id, 'is_deleted' => '0'));
	$get = $this->DB2->get();
	$total_assign_team = 0;
	foreach($get->result() as $row_old_ques)
	{
	    if($row_old_ques->name != '')
	    {
		$total_assign_team++;
	    }
	}
	$data['total_assign_team'] = $total_assign_team;
	return $data;
    }
    
    public function update_location_program_team($request){
        $data = array();
	$resultCode = 0;
	$resultMsg = 'Please Select Excel';
        $team_array = array();
	$audit_program_id = $_POST['audit_program_id'];
        if (isset($_FILES) && !empty($_FILES['excel_file']['name'])) {
            $filename = $_FILES['excel_file']['name'];
            $allowed =  array('xlsx', 'xls');
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $resultCode = 0;
                $resultMsg =  'Please select valid excel file';
            }
	    else
	    {
		$resultCode = 1;
		$resultMsg = 'Success';
		$base_url = 'assets/files/';
		move_uploaded_file($_FILES['excel_file']['tmp_name'], $base_url . $filename);
		$file = 'assets/files/'. $filename;
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		$arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
		$count = 0;
		for ($i = 2; $i <= $arrayCount; $i++) {
		    $id = trim($allDataInSheet[$i]["A"]);
		    $user = trim($allDataInSheet[$i]["C"]);
		    $user_email = trim($allDataInSheet[$i]["D"]);
		    $user_mobile = trim($allDataInSheet[$i]["E"]);
		    if($user != ''){
			$team_array[$count]['id'] = $id;
			$team_array[$count]['user'] = $user;
			$team_array[$count]['user_email'] = $user_email;
			$team_array[$count]['user_mobile'] = $user_mobile;
			$count++;
		    }
		}
		
		
	    }
            
        }
	if(count($team_array) > 0)
	{
	    foreach($team_array as $row)
	    {
		$id = $row['id'];
		$user = $row['user'];
		$user_email = $row['user_email'];
		$user_mobile = $row['user_mobile'];
		$update_data = array(
		    'name' => $user,
		    'user_email' => $user_email,
		    'user_mobile' => $user_mobile,
		    'updated_on' => date('Y-m-d H:i:s'),
		);
		$this->DB2->where(array('cp_offer_id'=> $audit_program_id, 'id' => $id));
		$update = $this->DB2->update('cp_offers_location', $update_data);
	    }
	}
        
        $data['resultCode'] = $resultCode;
        $data['resultMessage'] = $resultMsg;
        return $data;
    }
    public function add_audit_program_is_cheklist_all($request){
        $data = array();
	$resultCode = 1;
	$audit_program_id = $_POST['audit_program_id'];
	$is_checklist_all = $_POST['is_checklist_all'];
	
	    $update_data = array(
		'is_checklist_all' => $is_checklist_all,
		'updated_on' => date('Y-m-d H:i:s'),
	    );
	    $this->DB2->where(array('cp_offer_id'=>$audit_program_id));
	    $update = $this->DB2->update('cp_offers', $update_data);
	
	$data['resultCode'] = $resultCode;
        return $data;
    }
    
    public function add_audit_program_checklist_sku($request){
        $data = array();
	$resultCode = 1;
	$sku_id = $_POST['sku_id'];
	$sku_name = $_POST['sku_name'];
	$audit_program_id = $_POST['audit_program_id'];
	if($sku_id == 0 || $sku_id == '')
	{
	    // insert
	    $insert_data = array(
	       'sku_name' => $sku_name,
	       'program_id' => $audit_program_id,
	       'created_on' => date('Y-m-d H:i:s')
	    );
	    $insert = $this->DB2->insert('retailaudit_checklist_sku', $insert_data);
	    $sku_id = $this->DB2->insert_id();
	}
	else
	{
	    // update
	    $update_data = array(
	       'sku_name' => $sku_name,
	       'updated_on' => date('Y-m-d H:i:s')
	    );
	    $this->DB2->where(array('id'=>$sku_id));
	    $update = $this->DB2->update('retailaudit_checklist_sku', $update_data);
	}
	
	$data['resultCode'] = $resultCode;
        return $data;
    }
    public function get_audit_program_checklist_sku($request){
        $data = array();
	$resultCode = 1;
	$audit_program_id = $request->audit_program_id;
	$skus = array();
	$this->DB2->select('*')->from('retailaudit_checklist_sku');
	$this->DB2->where(array("program_id" => $audit_program_id, "is_deleted" => '0'));
	$this->DB2->order_by("id DESC");
	$get = $this->DB2->get();
	$i = 0;
	foreach($get->result() as $row)
	{
	    
	    $skus[$i]['sku_id'] = $row->id;
	    $skus[$i]['sku_name'] = $row->sku_name;
	    $i++;
	}
	$data['skus'] = $skus;
        return $data;
    }
    
    public function add_audit_program_checklist_sku_delete($request){
        $data = array();
	$resultCode = 1;
	$sku_id = $_POST['sku_id'];
	
	// insert
	    $update_data = array(
	       'is_deleted' => '1',
	       'updated_on' => date('Y-m-d H:i:s')
	    );
	    $this->DB2->where(array('id'=>$sku_id));
	    $update = $this->DB2->update('retailaudit_checklist_sku', $update_data);
	$data['resultCode'] = $resultCode;
        return $data;
    }
    public function retail_audit_log_record($slug, $loc_id, $program_id)
    {
	 $insert_data = array(
	      'slug' => $slug,
	      'loc_id' => $loc_id,
	      'key_id' => $program_id,
	      'added_on' => date('Y-m-d H:i:s'),
	 );
	 $insert = $this->DB2->insert('retailaudit_log_info', $insert_data);
    }
    public function campaign_graph_data_scratch($request){
        $campaign_id = '';
        if($this->session->userdata('campaignid')){
            $campaign_id = $this->session->userdata('campaignid');
        }
        if(isset($request->date_from) && $request->date_from != ''){
            $date_from = date("Y-m-d", strtotime( $request->date_from) );
        }
        else{
            $get_start_date = $this->DB2->query("select date(campaign_start_date) as date_from, date
            (campaign_end_date) as date_to from cp_offers where
            cp_offer_id = '$campaign_id'");
            if($get_start_date->num_rows() > 0){
                $row_date = $get_start_date->row_array();
                $date_from = date("Y-m-d", strtotime( $row_date['date_from']) );
                $date_to = date("Y-m-d", strtotime( $row_date['date_to']) );
                if(strtotime(date("Y-m-d")) < strtotime($date_to)){
                    $date_to = date("Y-m-d");
                }
            }
        }
        if(isset($request->date_to) && $request->date_to != ''){
            $date_to = date("Y-m-d", strtotime( $request->date_to));
        }

        $gender_where = '';
        if(isset($request->gender_filter)){
            if($request->gender_filter == 'm' || $request->gender_filter == 'f'){
                if($request->gender_filter == 'm'){
                    $gender_where = ' AND gender = "M" ';
                }
                elseif($request->gender_filter == 'f'){
                    $gender_where = ' AND gender = "F" ';
                }
            }
        }
        //age group filter
        $age_group_filter = '';
        if(isset($request->age_group)){
            if(count($request->age_group) > 0){
                $age_group='"'.implode('", "', $request->age_group).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $age_group_filter = " AND age_group in ($age_group) ";
            }
        }
        // time filter
        $time_filter = '';
        if(isset($request->time_filter)){
            if(count($request->time_filter) > 0){
                $time_or_query = '';
                $t = 0;
                foreach($request->time_filter as $filt_time){
                    $time_sended = explode('-', $filt_time);
                    $time_from =  $time_sended[0];
                    $time_to =  $time_sended[1];
                    if($t == 0){
                        $time_or_query = " TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    else{
                        $time_or_query = $time_or_query." OR TIME(viewed_on) BETWEEN '$time_from' AND '$time_to' ";
                    }
                    $t++;
                }
                $time_filter = " AND ($time_or_query) ";
            }
        }
        //device filter
        $device_filter = '';
        if(isset($request->device_filter)){
            if(count($request->device_filter) > 0){
                $device='"'.implode('", "', $request->device_filter).'"';
                //$age_group_filter = ' AND age_group in (".$age.") ';
                $device_filter = " AND osinfo in ($device) ";
            }
        }
        $data = array();

        /*$query = $this->db->query("SELECT DATE(viewed_on) AS viewed_on,
            (SELECT COUNT(*) AS COUNT FROM cp_dashboard_analytics cdal2 WHERE
            DATE(cdal1.viewed_on)=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter) AS total_view
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");*/
        $query = $this->DB2->query("SELECT DATE(viewed_on) AS viewed_on
            FROM cp_dashboard_analytics cdal1 WHERE DATE(viewed_on) BETWEEN '$date_from' AND '$date_to' AND cp_offer_id = '$campaign_id' $gender_where $age_group_filter $time_filter $device_filter
            GROUP BY DATE(viewed_on) ORDER BY viewed_on");
        $j = 0;
        $total_view = 0;

        foreach($query->result() as $row){
            $analysis_date = date_parse($row->viewed_on);
            $monthNum  = $analysis_date['month'];
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $data[$j]['date'] = $analysis_date['day'].' '.$monthName;

           /* $data[$j]['total_view'] = $row->total_view;
            $total_view = $total_view+$row->total_view;*/
            $total_view_query = $this->DB2->query("SELECT COUNT(*) AS total_view FROM cp_dashboard_analytics cdal2 WHERE
            DATE('$row->viewed_on')=DATE(cdal2.viewed_on) AND cdal2.cp_offer_id = '$campaign_id'  $gender_where
            $age_group_filter $time_filter
            $device_filter");
            $row_total_view = $total_view_query->row_array();
            $data[$j]['total_view'] =  $row_total_view['total_view'];
            $total_view = $total_view + $row_total_view['total_view'];
            $j++;
        }
        $get_budget = $this->DB2->query("select total_budget, offer_type, campaign_name from cp_offers where cp_offer_id = '$campaign_id'");
        $total_budget = 0;
        $campaign_type = '';
        $campaign_name = '';
        if($get_budget->num_rows() > 0){
            $get_budget_row = $get_budget->row_array();
            $total_budget = $get_budget_row['total_budget'];
            $campaign_type = strtoupper(substr(str_replace('_', ' ',$get_budget_row['offer_type']), 0, -1));
            $campaign_name = strtoupper($get_budget_row['campaign_name']);
        }
        $get_budget_used = $this->DB2->query("select mass_budget,premium_budget,star_budget from cp_budget_used where cp_offer_id = '$campaign_id'");

        $total_budget_used = 0;
        if($get_budget_used->num_rows() > 0){
            /*$get_budget_used_row = $get_budget_used->row_array();
            $total_budget_used = $total_budget_used+$get_budget_used_row['mass_budget']+$get_budget_used_row['premium_budget']+$get_budget_used_row['star_budget'];
        */
            foreach($get_budget_used->result() as $get_budget_used_row){
                $total_budget_used = $total_budget_used+$get_budget_used_row->mass_budget+$get_budget_used_row->premium_budget+$get_budget_used_row->star_budget;
            }
        }
        $return_array = array();
        $return_array['data'] = $data;
        $return_array['total_view'] = $total_view;

        $return_array['total_budget'] = $total_budget;
        $return_array['total_budget_used'] = $total_budget_used;
        $return_array['campaign_type'] = $campaign_type;
        $return_array['campaign_name'] = $campaign_name;
        $return_array['date_from'] = date("d-m-Y", strtotime( $date_from) );
        $return_array['date_to'] = date("d-m-Y", strtotime( $date_to) );
        return $return_array;
    }

}

?>