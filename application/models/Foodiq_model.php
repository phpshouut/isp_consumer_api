<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
class Foodiq_model extends CI_Model {
    private $db2; private $currsymbol = '';
    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('db2_publicwifi', TRUE);
    }
    
    public function country_phonecode($jsondata){
        $data = array();
        $phonecodeQ = $this->db->query("SELECT DISTINCT(phonecode) FROM `sht_countries` order by phonecode");
        if($phonecodeQ->num_rows() > 0){
            $i = 0;
            foreach($phonecodeQ->result() as $phnobj){
                $data['phonecode'][$i] = $phnobj->phonecode;
                $i++;
            }
        }
        
        $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$jsondata->isp_uid."'");
        if($ispcountryQ->num_rows() > 0){
            $crowdata = $ispcountryQ->row();
            $countryid = $crowdata->country_id;
            
            $countryQ = $this->db->query("SELECT phonecode FROM sht_countries WHERE id='".$countryid."'");
            if($countryQ->num_rows() > 0){
                $rowdata = $countryQ->row();
                //$data['isp_phonecode'] = $rowdata->phonecode;
                $data['isp_phonecode'] = '1';
            }
        }
        
        return $data;
    }
    public function authenticate_user($jsondata){
        $data = array();
	$password = isset($jsondata->password) ? md5($jsondata->password) : '';
	$isp_uid = isset($jsondata->isp_uid) ? $jsondata->isp_uid : '';
	$is_refiller = isset($jsondata->is_refiller) ? $jsondata->is_refiller : '0';
	$loginas = isset($jsondata->loginaskey) ? $jsondata->loginaskey : '1';
	
	if($loginas == '1'){
	    $this->db2->select('uid, enableuser, password')->from('foodiq_users');
	    $this->db2->where(array('uid' => $jsondata->mobile, 'password' => $password));
	    if($isp_uid != ''){
		$this->db2->where('isp_uid', $isp_uid);
	    }
	    $checkuserQ = $this->db2->get();
	    
	    if($checkuserQ->num_rows() > 0){
		$data['resultCode'] = '200';
		$data['resultMsg'] = 'User authenticate successfully.';
	    }
	    else{
		$data['resultCode'] = '401';
		$data['resultMsg'] = 'Please enter the correct credentials.';
	    }
	}
	elseif($loginas == '2'){
	    $refiller_password = $jsondata->password;
	    $this->db2->select('id, refiller_name, storage_id')->from('foodiq_location_refiller');
	    $this->db2->where(array('refiller_mobile' => $jsondata->mobile, 'refiller_password' => $refiller_password, 'status' => '1', 'is_deleted' => '0'));
	    $checkrefillerQ = $this->db2->get();
	    
	    if($checkrefillerQ->num_rows() > 0){
		$rawdata = $checkrefillerQ->row();
		$data['resultCode'] = '200';
		$data['resultMsg'] = 'User authenticate successfully.';
		$data['refiller_id'] = $rawdata->id;
		$data['refiller_name'] = $rawdata->refiller_name;
	    }else{
		$data['resultCode'] = '401';
		$data['resultMsg'] = 'Please enter the correct credentials.';
	    }
        }
	else{
	    $data['resultCode'] = '401';
	    $data['resultMsg'] = 'Please enter the correct credentials.';
	}
        
        return $data;
    }
    public function check_user_existence($jsondata){
        $data = array();
	$isp_uid = isset($jsondata->isp_uid) ? $jsondata->isp_uid : '';
	
        $this->db2->select('uid, enableuser, password')->from('foodiq_users');
        $this->db2->where('uid', $jsondata->mobile);
	if($isp_uid != ''){
	    $this->db2->where('isp_uid', $isp_uid);
	}
        $checkuserQ = $this->db2->get();
        
        if($checkuserQ->num_rows() > 0){          
            $user_rawdata = $checkuserQ->row();
	    $is_password_set = ($user_rawdata->password != '') ? '1' : '0';
            $is_verified = $user_rawdata->enableuser;
            $data['resultCode'] = '422';
            $data['resultMsg'] = 'User already exists.';
            $data['is_verify'] = "$is_verified";
            $data['mobile'] = $user_rawdata->uid;
	    $data['is_password_set'] = $is_password_set;
	    $data['is_foodiquser'] = '1';
	    
	    $this->db2->select('id')->from('foodiq_location_refiller');
	    $this->db2->where(array('refiller_mobile' => $jsondata->mobile, 'status' => '1', 'is_deleted' => '0'));
	    $checkrefillerQ = $this->db2->get();
	    if($checkrefillerQ->num_rows() > 0){
		$refdata = $checkrefillerQ->row();
		$data['refiller_id'] = $refdata->id;
		$data['is_password_set'] = '1';
		$data['is_refiller'] = '1';
	    }else{
		$data['is_refiller'] = '0';
	    }
        }
        else{
	    
	    $this->db2->select('id')->from('foodiq_location_refiller');
	    $this->db2->where(array('refiller_mobile' => $jsondata->mobile, 'status' => '1', 'is_deleted' => '0'));
	    $checkrefillerQ = $this->db2->get();
	    if($checkrefillerQ->num_rows() > 0){
		$refdata = $checkrefillerQ->row();
		
		$data['resultCode'] = '422';
		$data['resultMsg'] = 'User already exists.';
		$data['is_verify'] = "1";
		$data['refiller_id'] = $refdata->id;
		$data['is_password_set'] = '1';
		$data['is_refiller'] = '1';
		$data['is_foodiquser'] = '0';
	    }else{
		$data['resultCode'] = '404';
		$data['resultMsg'] = 'User doesnot exists.';
	    }
	    
	    /*$this->db2->select('id')->from('foodiq_location_refiller');
	    $this->db2->where(array('refiller_uid' => $jsondata->mobile, 'status' => '1', 'is_deleted' => '0'));
	    $checkrefillerQ = $this->db2->get();
	    if($checkrefillerQ->num_rows() > 0){
		$data['resultCode'] = '422';
		$data['resultMsg'] = 'User already exists.';
		$data['is_verify'] = "1";
		$data['refiller_uid'] = $jsondata->mobile;
		$data['is_password_set'] = '1';
		$data['is_refiller'] = '1';
	    }else{
		$data['resultCode'] = '404';
		$data['resultMsg'] = 'User doesnot exists.';
	    }*/
        }
        
        return $data;
    }
    
    public function add_edit_foodiq_user($jsondata){
        $data = array();
	$isp_uid = $jsondata->isp_uid;
        $this->db2->select('uid, enableuser, firstname, lastname, email, company, company_id, dob, address, food_type, food_allergy, mobile, payroll_debit, country_code')->from('foodiq_users');
        $this->db2->where('uid', $jsondata->mobile);
	$this->db2->where('isp_uid', $isp_uid);
        $checkuserQ = $this->db2->get();
        $password = isset($jsondata->password) ? md5($jsondata->password) : '';
	$platform = isset($jsondata->platform) ? $jsondata->platform : '';
	$dob = $jsondata->dob;
	if($dob != ''){
	    $dob = date('d-m-Y', strtotime($jsondata->dob));
	}
	
        if($checkuserQ->num_rows() > 0){
            $is_update = $jsondata->is_update;
            if($is_update == '1'){
                $userdataArr = array(
                    'isp_uid' => $jsondata->isp_uid,
                    'enableuser' => '1',
                    'uid' => $jsondata->mobile,
                    'username' => $jsondata->mobile,
                    'firstname' => $jsondata->firstname,
                    'lastname' => $jsondata->lastname,
                    'email' => $jsondata->email,
                    'company' => $jsondata->company,
                    'company_id' => $jsondata->company_id,
                    'dob' => $dob,
                    'address' => $jsondata->company_address,
                    'food_type' => $jsondata->food_type,
                    'food_allergy' => $jsondata->food_allergy,
                    'country_code' => $jsondata->country_code,
                    'mobile' => $jsondata->mobile,
		    'platform' => $platform
                );
                
                $this->db2->update('foodiq_users', $userdataArr, array('uid' => $jsondata->mobile, 'isp_uid' => $isp_uid));
                $data['resultCode'] = '200';
                $data['resultMsg'] = 'User Profile Updated Successfully.';
                $data['is_verify'] = '1';
            }else{
                $user_rawdata = $checkuserQ->row(); 
                $is_verified = $user_rawdata->enableuser;
		$dbdob = $user_rawdata->dob;
		if($dbdob != ''){
		    $dbdob = date('d-m-Y', strtotime($user_rawdata->dob));
		}
		
                $data['resultCode'] = '422';
                $data['resultMsg'] = 'User already exists.';
                $data['is_verify'] = "$is_verified";
                $data['country_code'] = $user_rawdata->country_code;
                $data['mobile'] = $user_rawdata->uid;
                $data['firstname'] = $user_rawdata->firstname;
                $data['lastname'] = $user_rawdata->lastname;
                $data['email'] = $user_rawdata->email;
                $data['company'] = $user_rawdata->company;
                $data['company_id'] = $user_rawdata->company_id;
                $data['dob'] = $dbdob;
                $data['address'] = $user_rawdata->address;
                $data['food_type'] = $user_rawdata->food_type;
                $data['food_allergy'] = $user_rawdata->food_allergy;
                $data['payroll_debit'] = $user_rawdata->payroll_debit;
            }
        }else{
            $userdataArr = array(
                'isp_uid' => $jsondata->isp_uid,
                'enableuser' => '0',
                'uid' => $jsondata->mobile,
		'password' => $password,
                'username' => $jsondata->mobile,
                'firstname' => $jsondata->firstname,
                'lastname' => $jsondata->lastname,
                'email' => $jsondata->email,
                'company' => $jsondata->company,
                'company_id' => $jsondata->company_id,
                'dob' => $dob,
                'address' => $jsondata->company_address,
                'food_type' => $jsondata->food_type,
                'food_allergy' => $jsondata->food_allergy,
                'country_code' => $jsondata->country_code,
                'mobile' => $jsondata->mobile,
		'platform' => $platform,
		'createdon' => date('Y-m-d')
            );
            
            $this->db2->insert('foodiq_users', $userdataArr);
            
            $isp_uid = $jsondata->isp_uid;
            $mobile = $jsondata->mobile;
	    $this->add_registration_amount($isp_uid, $mobile);
            
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'User registered successfully.';
            $data['is_verify'] = '0';
            
        }
        
        return $data;
    }
    public function add_registration_amount($isp_uid, $mobile){
	/*$bankdetQ = $this->db2->query("SELECT registration_amount FROM foodiq_bank_details WHERE isp_uid='".$isp_uid."'");
	if($bankdetQ->num_rows() > 0){
	    $bankrawdata = $bankdetQ->row();
	    $registration_amount = $bankrawdata->registration_amount;
	    $walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE mobile='".$mobile."'");
	    if($walletQ->num_rows() > 0){
		$this->db2->query("UPDATE foodiq_wallet SET wallet_amount = (wallet_amount + $registration_amount) WHERE mobile='".$jsondata->mobile."'");
	    }else{
		$this->db2->insert("foodiq_wallet", array("wallet_amount" => $registration_amount, "mobile" => $mobile));
	    }
	}*/
	
	$registration_amount = '10';
	$walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE isp_uid='".$isp_uid."' AND mobile='".$mobile."'");
	if($walletQ->num_rows() == 0){
	    $this->db2->insert("foodiq_wallet", array("wallet_amount" => $registration_amount, 'isp_uid' => $isp_uid, "mobile" => $mobile));
	}
	
	return 1;
    }
    public function infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message){
	$baseUrl = "http://alerts.solutionsinfini.com/api/v4/?api_key=".$infini_apikey;
	$user_message=urlencode($user_message); 

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl."&method=sms&message=$user_message&to=$user_mobileno&sender=$infini_senderid");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);

	$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
	if($statusCode==200){
	    return 1;
	}
	else{
	    return 0;
	}
	curl_close($ch);
    }
    
    public function twilio_sendsms($mobileno, $country_code){
        $baseUrl = "http://api.authy.com/protected/json/phones/verification/";
        
        //{"carrier":"Vodafone Essar South Ltd","is_cellphone":true,"message":"पाठ संदेश +91 750-394-5244 करने के लिए भेजा है।","seconds_to_expire":599,"uuid":"b11b0ff0-b27a-0136-92a7-0a030cdfa5be","success":true}
        // TO SEND OTP
        $headers = array(
	    "Content-Type: application/json",
	    "Accept: application/json"
	);
	$jsonVal = (object) array();
	$curl = curl_init($baseUrl.'start?api_key=FLx3j4GVfaUUYBBOHUKnWayu314JkQ3C&via=sms&phone_number='.$mobileno.'&country_code='.$country_code);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($jsonVal));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
	//curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $curl_response = curl_exec($curl);

	//var_dump($curl_response); die;
        return 1;
	
    }
    public function twilio_verifysms($verifycode, $mobileno, $country_code){
        $baseUrl = "http://api.authy.com/protected/json/phones/verification/";
        
        //string(135) "{"error_code":"60022","message":"Verification code is incorrect","errors":{"message":"Verification code is incorrect"},"success":false}"
        //string(58) "{"message":"Verification code is correct.","success":true}"
        //string(179) "{"message":"No pending verifications for +91 750-394-5244 found.","success":false,"errors":{"message":"No pending verifications for +91 750-394-5244 found."},"error_code":"60023"}" 
        // TO VERIFY OTP
        $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $baseUrl."check?api_key=FLx3j4GVfaUUYBBOHUKnWayu314JkQ3C&verification_code=".$verifycode."&phone_number=".$mobileno."&country_code=".$country_code);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);
        
        //var_dump($result);
        return $result;
	
    }
    public function send_otp($jsondata){
        $data = array();
        $user_mobileno = $jsondata->mobile;
        
        $getcountryQ = $this->db2->query("SELECT country_code FROM foodiq_users WHERE uid='".$user_mobileno."' AND isp_uid='".$jsondata->isp_uid."'");
        $country_code = $getcountryQ->row()->country_code;
        
        if($country_code == 91){      
            $otpno = mt_rand(1000,9999);
            $user_message="Your Wifi Otp is ".$otpno;
            $txtsmsQ = $this->db->query("SELECT * FROM sht_sms_gateway WHERE isp_uid='".$jsondata->isp_uid."' ORDER BY id DESC LIMIT 1");
            if($txtsmsQ->num_rows() > 0){
                foreach($txtsmsQ->result() as $txtsmsobj){
    
                    $infini_apikey = $txtsmsobj->infini_apikey;
                    $infini_senderid = $txtsmsobj->infini_senderid;
                    $sender = $txtsmsobj->scope;
                    
                    if(($infini_apikey != '') && ($infini_senderid != '')){
                        $this->infini_txtsmsgateway($infini_apikey, $infini_senderid, $user_mobileno, $user_message);
                        $data['resultCode'] = '200';
                        $data['resultMsg'] = 'OTP Send Successfully.';
                        $data['otp_number'] = $otpno;
                    }else{
                        $data['resultCode'] = '404';
                        $data['resultMsg'] = 'No SMS Gateway setup for this ISP.';
                        $data['otp_number'] = '0';
                    }
                }
            }
        }else{
            $this->twilio_sendsms($user_mobileno, $country_code);
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'OTP Send Successfully.';
            $data['otp_number'] = '0000';
        }
        return $data;
    }
    public function verify_otp($jsondata){
        $mobileno = $jsondata->mobile;
        $isp_uid = $jsondata->isp_uid;
        $data = array();
        
        $getcountryQ = $this->db2->query("SELECT country_code FROM foodiq_users WHERE uid='".$mobileno."'");
        $country_code = $getcountryQ->row()->country_code;
        
        if($country_code == 91){
            $this->db2->select('uid')->from('foodiq_users');
            $this->db2->where('uid', $jsondata->mobile);
            $checkuserQ = $this->db2->get();
            if($checkuserQ->num_rows() > 0){
                $this->db2->update('foodiq_users', array('enableuser' => '1'), array('uid' => $jsondata->mobile));
                $data['resultCode'] = '200';
                $data['resultMsg'] = 'Account verified successfully.';
            }else{
                $data['resultCode'] = '404';
                $data['resultMsg'] = 'User doesnot exists.';
            }
        }else{
            $verifycode = isset($jsondata->verifycode) ? $jsondata->verifycode : '0000';
            $twilio_response = json_decode($this->twilio_verifysms($verifycode, $mobileno, $country_code));
            if(isset($twilio_response->error_code)){
                $data['resultCode'] = '422';
                $data['resultMsg'] = 'OOPS! OTP not matching. Please enter the valid OTP again.';
            }else{
                $this->db2->update('foodiq_users', array('enableuser' => '1'), array('uid' => $jsondata->mobile));
                $data['resultCode'] = '200';
                $data['resultMsg'] = 'Account verified successfully.';
            }
        }
        
        return $data;
    }
    public function get_location_list($jsondata){
        $data = array();
        $distance = 100;
        $earths_radius = 6371;
        $isp_uid = $jsondata->isp_uid;
        $user_latitude = $jsondata->user_latitude;
        $user_longitude = $jsondata->user_longitude;
        //AND (acos(sin($user_latitude) * sin(latitude) + cos($user_latitude) * cos(latitude) * cos(longitude - ($user_longitude))) * $earths_radius) <= ".$distance."
	
	$bannerimageQ = $this->db2->query("SELECT id, media_type, original_image FROM `foodiq_media` WHERE media_type='banner_logo' AND isp_uid='$isp_uid' AND status='1'");
	if($bannerimageQ->num_rows() > 0){
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Media Found.';
	    $bn=0;
	    foreach($bannerimageQ->result() as $bannobj){
		$media_type = $bannobj->media_type;
		if($media_type == 'banner_logo'){
		    $data['banner_logo'][$bn] = CDNPATH.'isp/foodiq/original/'.$bannobj->original_image;
		    $bn++;
		}
	    }
	}
	
        $getlocationQuery = $this->db2->query("SELECT id, location_uid, location_name, geo_address, placeid, latitude, longitude, (acos(sin(RADIANS($user_latitude)) * sin(RADIANS(latitude)) + cos(RADIANS($user_latitude)) * cos(RADIANS(latitude)) * cos(RADIANS(longitude) - RADIANS(($user_longitude)))) * $earths_radius) as distance FROM wifi_location WHERE isp_uid='".$isp_uid."' AND status='1' AND is_deleted='0' AND location_type='15' ORDER BY distance ASC");
        if($getlocationQuery->num_rows() > 0){
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'Location Lists.';
            $i = 0;
            foreach($getlocationQuery->result() as $locobj){
                $data['locationlists'][$i]['location_id'] = $locobj->id;
                $data['locationlists'][$i]['location_uid'] = $locobj->location_uid;
                $data['locationlists'][$i]['location_name'] = $locobj->location_name;
                $data['locationlists'][$i]['geo_address'] = $locobj->geo_address;
                $data['locationlists'][$i]['placeid'] = $locobj->placeid;
                $data['locationlists'][$i]['latitude'] = $locobj->latitude;
                $data['locationlists'][$i]['longitude'] = $locobj->longitude;
                $data['locationlists'][$i]['distance'] = round($locobj->distance, 2).' KM';
                $i++;
            }
        }else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'No nearby location found.';
        }
        
        return $data;
    }
    
    public function getmenu_display_option($location_uid){
	$query = $this->db2->query("SELECT foodmenu_options_list FROM wifi_location WHERE location_uid='".$location_uid."'");
	return $query->row()->foodmenu_options_list;
    }
    public function get_location_items($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        
	$ordersessid = isset($jsondata->ordersessid) ? $jsondata->ordersessid : '';
	if($ordersessid != ''){
	    $mobile = $ordersessid;
	}
	
        $chkforprevlocQ = $this->db2->query("SELECT id FROM foodiq_items_cart WHERE mobile='".$mobile."' AND location_uid != '".$location_uid."' AND DATE(added_on) = '".date('Y-m-d')."'");
        if($chkforprevlocQ->num_rows() > 0){
            $location_name = $this->getlocation_name($location_uid);
            $data['is_cartitems'] = '1';
            $data['cart_alert'] = 'You already have items in your cart for location: '.$location_name. ' If proceed, cart will get empty.';
        }else{
            $data['is_cartitems'] = '0';
            $data['cart_alert'] = '';
        }
        
        $data['display_option'] = $this->getmenu_display_option($location_uid);
        
        $menucartQ = $this->db2->query("SELECT COUNT(id) as cartitems FROM foodiq_items_cart WHERE mobile='".$mobile."' AND DATE(added_on)='".date('Y-m-d')."'");
        if($menucartQ->num_rows() > 0){
            $cart_rawdata = $menucartQ->row();
            $data['cart_total_items'] = $cart_rawdata->cartitems;
        }
	
	$campaign = array();
	$today_date = date('Y-m-d');
	$this->db2->select('fs.id,fs.campaign_name, fs.end_date, fs.campaign_type, fs.purchase_value, fs.smartcash_value');
	$this->db2->where(array('fs.status' => 1, 'fs.is_deleted' => 0, 'fs.location_uid' => $location_uid));
	$this->db2->where(array('DATE(start_date) <=' => $today_date, 'DATE(end_date) >=' => $today_date));
	$this->db2->from('foodiq_smartcash as fs');
	$get = $this->db2->get();
	if($get->num_rows() > 0){
	    $i = 0;
	    
	    foreach($get->result() as $row){
		$campaign[$i]['campaign_id'] = $row->id;
		$campaign[$i]['campaign_name'] = $row->campaign_name;
		$campaign[$i]['campaign_type'] = $row->campaign_type;
		$campaign[$i]['purchase_value'] = $row->purchase_value;
		$campaign[$i]['smartcash_value'] = $row->smartcash_value;
		$expires_in = date('Y-m-d',strtotime($row->end_date));
		$date1_ts = strtotime($today_date);
		$date2_ts = strtotime($expires_in);
		$diff = ($date2_ts - $date1_ts);
		$days = round($diff / 86400);
		if($days == 0){
		    $days = "Today";
		}else{
		    $days = $days." Days";
		}
		$campaign[$i]['expire_days'] = $days;
		$product = array();
		
		$this->db2->select('fmm.product_titlename,fm.food_item_price, fsi.menu_id')->from('foodiq_smartcash_item as fsi');
		$this->db2->where(array('fsi.smartcash_id' => $row->id, 'fsi.is_deleted' => 0));
		$this->db2->join('foodiq_menulist as fm', 'fsi.menu_id = fm.id', 'inner');
		$this->db2->join('foodiq_master_menulist as fmm', 'fm.master_menu_id = fmm.id', 'inner');
		$get_product = $this->db2->get();
		$j = 0;
		foreach($get_product->result() as $pro_row){
		    $product[$j]['product_id'] = $pro_row->menu_id;
		    $product[$j]['product_name'] = $pro_row->product_titlename;
		    $product[$j]['product_price'] = $pro_row->food_item_price;
		    $j++;
		}
		$campaign[$i]['product'] = $product;
		$i++;
	    }
	    
	    //$data['campaign'] = $campaign;
	}
	
	$data['campaign'] = $campaign;
	
        $menulistQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity, tb2.menu_categoryname, tb2.product_titlename, tb2.product_itemshortdesc, tb1.food_item_price, tb2.menu_display_option, tb2.food_item_barcode FROM  foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.location_uid='".$location_uid."' AND tb1.status='1' ORDER BY tb2.menu_categoryid ASC");
        if($menulistQ->num_rows() > 0){
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'Items lists';
            $i = 0;
            
            foreach($menulistQ->result() as $itemobj){
                $id = $itemobj->id;
                $category = trim($itemobj->menu_categoryname);
		
                if (array_key_exists("$category", $data)) {
                    $data["$category"][$i]["category"] = $category;
                    $i++;
                }else{
                    $i = 0;
                }
                
                $data["menutabs"][$id] = $category;
                $data["$category"][$i]['item_id'] = $itemobj->id;
                $data["$category"][$i]['item_name'] = $itemobj->product_titlename;
                $data["$category"][$i]['item_shortdesc'] = $itemobj->product_itemshortdesc;
                $data["$category"][$i]['item_price'] = $itemobj->food_item_price;
                $data["$category"][$i]['item_quantity'] = $itemobj->food_item_quantity;
                $data["$category"][$i]['menu_display_option'] = $itemobj->menu_display_option;
                
            }
            //print_r($data); die;
            $menutabs = array_unique($data["menutabs"]);
            //ksort($menutabs);
            $data["menutabs"] = array_values($menutabs);
        }else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'Currently, Items are not available for this location.';
        }
        
        return $data;
    }
    
    
    public function currency_symbol($jsondata){
        $data = array();
        $ispcountryQ = $this->db->query("SELECT country_id FROM sht_isp_admin WHERE isp_uid='".$jsondata->isp_uid."'");
        if($ispcountryQ->num_rows() > 0){
            $crowdata = $ispcountryQ->row();
            $countryid = $crowdata->country_id;
            
            $countryQ = $this->db->query("SELECT * FROM sht_countries WHERE id='".$countryid."'");
            if($countryQ->num_rows() > 0){
                $rowdata = $countryQ->row();
                $currid = $rowdata->currency_id;
                
                $currencyQ = $this->db->get_where('sht_currency', array('currency_id' => $currid));
                if($currencyQ->num_rows() > 0){
                    $currobj = $currencyQ->row();
                    $currsymbol = $currobj->currency_symbol;
                    
                    if($jsondata->isp_uid == '101'){
                        $data['currency'] = '&#36;';
                    }else{
                        $data['currency'] = $currsymbol;
                    }
                }
            }
        }else{
            $data['currency'] = '&#8377;';
        }
        return $data;
    }
    public function list_cart_items($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        
	$ordersessid = isset($jsondata->ordersessid) ? $jsondata->ordersessid : '';
	if($ordersessid != ''){
	    $mobile = $ordersessid;
	}
	
        $walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE isp_uid='".$isp_uid."' AND mobile='".$jsondata->mobile."'");
        if($walletQ->num_rows() > 0){
            $cart_rawdata = $walletQ->row();
            $data['wallet_balance'] = $cart_rawdata->wallet_amount;
        }else{
            $data['wallet_balance'] = '0';
        }
        
	$campaign_id = isset($jsondata->campaign_id) ? $jsondata->campaign_id : '0';
	$coupon_discount = 0;
	$today_date = date('Y-m-d');
	$coupon_name = '';
	if($campaign_id != '0'){
	    $this->db2->select('fs.id, fs.campaign_type, fs.purchase_value, fs.smartcash_value');
	    $this->db2->where(array('fs.status' => 1, 'fs.is_deleted' => 0, 'fs.id' => $campaign_id));
	    $this->db2->from('foodiq_smartcash as fs');
	    $get = $this->db2->get();
	    if($get->num_rows() > 0){
		foreach($get->result() as $row){
		    if($row->campaign_type == 4){
			$coupon_discount = $row->purchase_value - $row->smartcash_value;
		    }
		    else{
			$this->db2->select('fm.food_item_price')->from('foodiq_smartcash_item as fsi');
			$this->db2->where(array('fsi.smartcash_id' => $row->id, 'fsi.is_deleted' => 0));
			$this->db2->join('foodiq_menulist as fm', 'fsi.menu_id = fm.id', 'inner');
			//$this->db2->join('foodiq_master_menulist as fmm', 'fm.master_menu_id = fmm.id', 'inner');
			$get_product = $this->db2->get();
			$product_price = 0;
			foreach($get_product->result() as $pro_row){
			    $product_price = $product_price+$pro_row->food_item_price;
			}
			$coupon_discount = $product_price - $row->smartcash_value;
		    }
		}
	    }
	}
	
	
	$item_array = array();
        $checkusercartQ = $this->db2->query("SELECT tb1.id, tb1.menu_id, tb1.qty, tb3.product_titlename, tb2.food_item_price FROM foodiq_items_cart as tb1 INNER JOIN foodiq_menulist as tb2 ON(tb1.menu_id=tb2.id) INNER JOIN foodiq_master_menulist as tb3 ON(tb2.master_menu_id=tb3.id) WHERE tb1.mobile='".$mobile."' AND tb1.location_uid='".$location_uid."' AND DATE(tb1.added_on)='".date('Y-m-d')."'");
        if($checkusercartQ->num_rows() > 0){
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'Added Items List';
            $i = 0; $total_price = 0;
            foreach($checkusercartQ->result() as $cartobj){
                $data['cartitems'][$i]['cart_id'] = $cartobj->id;
                $data['cartitems'][$i]['item_id'] = $cartobj->menu_id;
                $data['cartitems'][$i]['item_name'] = $cartobj->product_titlename;
                $data['cartitems'][$i]['item_qty'] = $cartobj->qty;
                $data['cartitems'][$i]['item_price'] = sprintf('%0.2f', round( ($cartobj->qty * $cartobj->food_item_price) , 2));
		$item_array[$cartobj->menu_id]['product_id'] = $cartobj->menu_id;
		$item_array[$cartobj->menu_id]['product_qty'] = $cartobj->qty;
		$item_array[$cartobj->menu_id]['product_price'] = $cartobj->food_item_price;
		
                $total_price += round( ($cartobj->qty * $cartobj->food_item_price) , 2);
                $i++;
            }
	    
	    $today_date = date('Y-m-d');
	    $offer_array = array();
	    $is_coupon_valid = 0;
	    $campaign = array();
	    $this->db2->select('fs.id,fs.campaign_name, fs.end_date, fs.campaign_type, fs.purchase_value, fs.smartcash_value');
	    $this->db2->where(array('fs.status' => 1, 'fs.is_deleted' => 0));
	    $this->db2->where(array('DATE(start_date) <=' => $today_date, 'DATE(end_date) >=' => $today_date));
	    $this->db2->from('foodiq_smartcash as fs');
	    $get = $this->db2->get();
	    if($get->num_rows() > 0){
		$i = 0;
		foreach($get->result() as $row){
		    $campaign[$i]['campaign_id'] = $row->id;
		    $campaign[$i]['campaign_type'] = $row->campaign_type;
		    $campaign[$i]['purchase_value'] = $row->purchase_value;
		    $campaign[$i]['smartcash_value'] = $row->smartcash_value;
		    $product = array();
		    
		    $this->db2->select('fmm.product_titlename,fm.food_item_price, fsi.menu_id')->from('foodiq_smartcash_item as fsi');
		    $this->db2->where(array('fsi.smartcash_id' => $row->id, 'fsi.is_deleted' => 0));
		    $this->db2->join('foodiq_menulist as fm', 'fsi.menu_id = fm.id', 'inner');
		    $this->db2->join('foodiq_master_menulist as fmm', 'fm.master_menu_id = fmm.id', 'inner');
		    $get_product = $this->db2->get();
		    $j = 0;
		    $pqarr=array();
		    foreach($get_product->result() as $pro_row){
			$product[$j]['product_id'] = $pro_row->menu_id;
			$product[$j]['product_name'] = $pro_row->product_titlename;
			$product[$j]['product_price'] = $pro_row->food_item_price;
			$pqarr[$pro_row->menu_id][]=$j;
			$j++;
		    }
		    $campaign[$i]['product'] = $product;
		    $campaign[$i]['product_qty'] = $pqarr;
		    $i++;
		}
		$camp_i = 0;
		foreach($campaign as $row_camp){
		    if($row_camp['campaign_type'] == 4){
			if($row_camp['purchase_value'] == $total_price){
			    if($campaign_id == $row_camp['campaign_id']){
				$is_coupon_valid = 1;
			    }
			    $offer_array[$camp_i]['campaign_id'] = $row_camp['campaign_id'];
			    $offer_array[$camp_i]['campaign_type'] = $row_camp['campaign_type'];
			    $offer_array[$camp_i]['purchase_value'] = $row_camp['purchase_value'];
			    $offer_array[$camp_i]['smartcash_value'] = $row_camp['smartcash_value'];
			    $camp_i++;
			}
		    }
		    else{
			$is_valid = 1;
			foreach($row_camp['product'] as $prd_row){
			    if(!array_key_exists($prd_row['product_id'], $item_array)){
				
				    $is_valid=0;
				
			    }else{
				
				if(count($row_camp['product_qty'][$prd_row['product_id']])>$item_array[$prd_row['product_id']]['product_qty'])
				{
				    $is_valid=0;
				}
			    }
			}
			if($is_valid==1){
			    if($campaign_id == $row_camp['campaign_id']){
				$is_coupon_valid = 1;
			    }
			    $offer_array[$camp_i]['campaign_id'] = $row_camp['campaign_id'];
			    $offer_array[$camp_i]['campaign_type'] = $row_camp['campaign_type'];
			    $offer_array[$camp_i]['purchase_value'] = $row_camp['purchase_value'];
			    $product_comb = array();
			    $p_i = 0;
			    foreach($row_camp['product'] as $prd_row){
				$product_comb[$p_i]['product_id'] = $prd_row['product_id'];
				$product_comb[$p_i]['product_name'] = $prd_row['product_name'];
				$product_comb[$p_i]['product_price'] = $prd_row['product_price'];
				$p_i++;
			    }
			    $offer_array[$camp_i]['smartcash_value'] = $row_camp['smartcash_value'];
			    $offer_array[$camp_i]['combo_product'] = $product_comb;
			    $camp_i++;
			}
		    }
		}
	    }
		
	    $data['offer_array'] = $offer_array;
	    
	    if($is_coupon_valid == 1){
		$data['campaign_id'] = $campaign_id;
		$data['coupon_discount'] = $coupon_discount;
	    }
	    else{
		$coupon_discount = '0';
		$data['campaign_id'] = '0';
		$data['coupon_discount'] = '0';
	    }
	    $country_code = isset($jsondata->country_code) ? $jsondata->country_code : '1';
	    if($country_code == 91){
		$data['tax_type'] = 'GST';
		$data['tax_percent'] = '18';
	    }elseif($country_code == 1){
		$data['tax_type'] = 'HST';
		$data['tax_percent'] = '13';
	    }
	    
	    $data['subtotal_bill_amount'] = sprintf('%0.2f', $total_price);
	    if($coupon_discount != '0'){
		$total_price = ($total_price - $coupon_discount);
		$data['discount_amount'] = sprintf('%0.2f', $total_price);
	    }else{
		$data['discount_amount'] = '0';
	    }
	    
	    $tax = $data['tax_percent'];
	    $tax_amount = round( (($total_price * $tax) / 100) , 2);
	    $data['tax_amount'] = sprintf('%0.2f', $tax_amount);
	    $total_price = ($total_price + $tax_amount);
            
            $data['total_bill_amount'] = sprintf('%0.2f', $total_price);
            
            $wallet_balance = $data['wallet_balance'];
            if($wallet_balance < $total_price){
                $data['insufficient_balance'] = sprintf('%0.2f', round(($total_price - $wallet_balance), 2));
            }else{
                $data['insufficient_balance'] = 0;
            }
	    
        }
        else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'No Items Added To Cart.';
        }

        return $data;
    }
    public function scanbarcode_itemdetails($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        $item_barcode = $jsondata->item_barcode;
        
        $checkbarcodeQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity, tb2.product_titlename, tb2.product_itemshortdesc, tb2.has_itemsize, tb2.measure_itemsize, tb2.value_itemsize, tb1.food_item_price FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE tb2.food_item_barcode='".$item_barcode."' AND tb1.location_uid='".$location_uid."' AND tb1.status='1'");
        if($checkbarcodeQ->num_rows() > 0){
            $chkrawdata = $checkbarcodeQ->row();
            $item_id = $chkrawdata->id;
            $totalQuantity = $chkrawdata->food_item_quantity;
            
            $cartqty = 0;
            $menucartQ = $this->db2->query("SELECT SUM(qty) as cartqty FROM foodiq_items_cart WHERE menu_id='".$item_id."' AND location_uid='".$location_uid."' AND DATE(added_on)='".date('Y-m-d')."'");
            if($menucartQ->num_rows() > 0){
                $crawdata = $menucartQ->row();
                $cartqty = $crawdata->cartqty;
            }
            
            if(($totalQuantity < ($cartqty + 1)) && ($totalQuantity > 0)){
                $data['resultCode'] = '404';
                $data['resultMsg'] = 'Item currently not available. Please try again later.';
            }
            else{
                $data['resultCode'] = '200';
                $data['resultMsg'] = 'Item Available.';
                $data['product_titlename'] = $chkrawdata->product_titlename;
                $data['product_itemshortdesc'] = $chkrawdata->product_itemshortdesc;
                if($chkrawdata->has_itemsize == '1'){
                    $data['measure_itemsize'] = $chkrawdata->measure_itemsize;
                    $data['value_itemsize'] = $chkrawdata->value_itemsize;
                }else{
                    $data['measure_itemsize'] = 'NA';
                    $data['value_itemsize'] = 'NA';
                }
                $data['food_item_price'] = $chkrawdata->food_item_price;
		$data['totalQuantity'] = ($totalQuantity-$cartqty);
                
            }
        }
        else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'This item currently not get added. Please try again later.';
        }
        return $data;
    }
    public function addtocart($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        $item_barcode = $jsondata->item_barcode;
        $item_id = isset($jsondata->item_id) ? $jsondata->item_id : '';
        $enterqty = isset($jsondata->enterqty) ? $jsondata->enterqty : '1';
	
	$ordersessid = isset($jsondata->ordersessid) ? $jsondata->ordersessid : '';
	if($ordersessid != ''){
	    $mobile = $ordersessid;
	}
	
	
        $this->db2->query("DELETE FROM foodiq_items_cart WHERE DATE(added_on) != '".date('Y-m-d')."'");
        $this->db2->query("DELETE FROM foodiq_items_cart WHERE mobile='".$mobile."' AND location_uid != '".$location_uid."'");
        
        if($item_barcode != ''){
	    $checkbarcodeQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE tb2.food_item_barcode='".$item_barcode."' AND tb1.location_uid='".$location_uid."' AND tb1.status='1'");
        }
        elseif($item_id != ''){
	    $checkbarcodeQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity FROM foodiq_menulist as tb1 WHERE tb1.id='".$item_id."' AND tb1.location_uid='".$location_uid."' AND tb1.status='1'");
        }
	else{
	    $checkbarcodeQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity FROM foodiq_menulist as tb1 WHERE 1 AND tb1.location_uid='".$location_uid."' AND tb1.status='1'");
	}

        if($checkbarcodeQ->num_rows() > 0){
            $chkrawdata = $checkbarcodeQ->row();
            $item_id = $chkrawdata->id;
            $totalQuantity = $chkrawdata->food_item_quantity;
            
            $cartqty = 0;
            $menucartQ = $this->db2->query("SELECT SUM(qty) as cartqty FROM foodiq_items_cart WHERE menu_id='".$item_id."' AND DATE(added_on)='".date('Y-m-d')."'");
            if($menucartQ->num_rows() > 0){
                $crawdata = $menucartQ->row();
                $cartqty = $crawdata->cartqty;
            }
            
            if(($totalQuantity < ($cartqty + $enterqty)) && ($totalQuantity > 0)){
                $data['resultCode'] = '404';
                $data['resultMsg'] = 'Item currently not available. Please try again later.';
            }
            else{
                $checkusercartQ = $this->db2->query("SELECT id FROM foodiq_items_cart WHERE menu_id='".$item_id."' AND mobile='".$mobile."' AND DATE(added_on)='".date('Y-m-d')."'");
                if($checkusercartQ->num_rows() > 0){
                    //$this->db2->query("UPDATE foodiq_items_cart SET qty = qty+1 WHERE menu_id='".$item_id."' AND mobile='".$mobile."' AND DATE(added_on)='".date('Y-m-d')."'");
		    $finalqty = '`qty` + '.$enterqty;
		    $this->db2->set('qty', $finalqty , FALSE);
		    $this->db2->where(array('menu_id' => $item_id, 'mobile' => $mobile, 'DATE(added_on)' => date('Y-m-d')));
		    $this->db2->update('foodiq_items_cart');
		    
                }else{
                    $this->db2->insert('foodiq_items_cart', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'menu_id' => $item_id, 'qty' => $enterqty, 'added_on' => date('Y-m-d H:i:s')));
                }
                
                $data['resultCode'] = '200';
                $data['resultMsg'] = 'Item Added To Cart.';
            }
        }
        else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'This item currently not get added. Please try again later.';
        }
        return $data;
    }
    public function add_order_for_payment($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        $total_amount = $jsondata->total_amount;
	
	$ordersessid = isset($jsondata->ordersessid) ? $jsondata->ordersessid : '';
	$campaign_id = isset($jsondata->campaign_id) ? $jsondata->campaign_id : '0';
	$platform = isset($jsondata->platform) ? $jsondata->platform : '';
	if($ordersessid != ''){
	    $mobile = $ordersessid;
	}
        
	$addorderQ = $this->db2->insert('foodiq_order', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'order_type' => 'orderpay', 'mobile' => $mobile, 'amount' => $total_amount, 'payment_received' => '1', 'platform' => $platform, 'campaign_id' => $campaign_id, 'added_on' => date('Y-m-d H:i:s')));
	$order_id = $this->db2->insert_id();
	
	$checkusercartQ = $this->db2->query("SELECT tb1.menu_id, tb1.qty, tb3.product_titlename, tb2.food_item_price, tb3.is_perishable FROM foodiq_items_cart as tb1 LEFT JOIN foodiq_menulist as tb2 ON(tb1.menu_id=tb2.id) INNER JOIN foodiq_master_menulist as tb3 ON(tb2.master_menu_id=tb3.id) WHERE tb1.mobile='".$mobile."' AND tb1.location_uid='".$location_uid."' AND DATE(tb1.added_on)='".date('Y-m-d')."'");
	if($checkusercartQ->num_rows() > 0){
	    foreach($checkusercartQ->result() as $cartobj){
                $qty = $cartobj->qty;
                $price = ($qty * $cartobj->food_item_price);
		$is_perishable = $cartobj->is_perishable;
		$menuid = $cartobj->menu_id;
		$this->db2->insert('foodiq_order_details', array('order_id' => $order_id, 'menu_id' => $cartobj->menu_id, 'menu_name' => $cartobj->product_titlename, 'qty' => $cartobj->qty, 'price' => $price));
                
                
                $finalqty = '`food_item_quantity` - '.$qty;
                $this->db2->set('food_item_quantity', $finalqty , FALSE);
                $this->db2->where('id', $cartobj->menu_id);
                $this->db2->update('foodiq_menulist');
		
		//SoldInventory Report
		$balance_shelf_left = '0';
		$balance_stock_left = '0';
		
		$this->db2->select('food_item_quantity')->from('foodiq_menulist');
		$this->db2->where('id' , $menuid);
		$balanceshelfQ = $this->db2->get();
		if($balanceshelfQ->num_rows() > 0){
		    $balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
		}
		
		$this->db2->select('quantity')->from('foodiq_warehouse');
		$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
		$balancestockQ = $this->db2->get();
		if($balancestockQ->num_rows() > 0){
		    $balance_stock_left = $balancestockQ->row()->quantity;
		}
		
		$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => '0', 'menu_id' => $menuid, 'quantity' => $qty, 'actionfrom_id' => $location_uid, 'actionto_id' => '', 'actionfrom' => 'soldfromshelve', 'actionto' => '', 'action' => 'soldproduct', 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
	    }
	}
	
	$this->db2->query("UPDATE foodiq_wallet SET wallet_amount = (wallet_amount - $total_amount) WHERE isp_uid='".$isp_uid."' AND mobile='".$jsondata->mobile."'");
	$this->db2->delete("foodiq_items_cart", array('mobile' => $mobile));
	
	$data['resultCode'] = '200';
	$data['resultMsg'] = 'Payment Received.';
        
        return $data;
        
    }
    public function delete_cart_item($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $cart_id = $jsondata->cart_id;
        
        $checkusercartQ = $this->db2->query("SELECT id FROM foodiq_items_cart WHERE id='".$cart_id."'");
        if($checkusercartQ->num_rows() > 0){
            $this->db2->delete("foodiq_items_cart", array('id' => $cart_id));
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'Added Items List';
        }else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'Item Not Found.';
        }
        return $data;
    }
    public function check_wallet_amount($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
	$mobile = $jsondata->mobile;

        $walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE isp_uid='".$isp_uid."' AND mobile='".$mobile."'");
        if($walletQ->num_rows() > 0){
            $cart_rawdata = $walletQ->row();
            $data['wallet_balance'] = $cart_rawdata->wallet_amount;
        }else{
            $data['wallet_balance'] = '0.00';
        }
        
	$data['wallet_limit'] = '100.00';
	$this->db2->select('wallet_limit')->from('foodiq_user_wallet_limit');
	$this->db2->where(array('isp_uid' => $isp_uid, 'uid' => $mobile));
	$walletlimitQ = $this->db2->get();
	if($walletlimitQ->num_rows() > 0){
	    $data['wallet_limit'] = $walletlimitQ->row()->wallet_limit;
	}
	
	
        $payrollenableQuery = $this->db2->query(" SELECT id FROM `wifi_location` WHERE location_uid='".$location_uid."' AND enable_payrolldebit='1'");
	if($payrollenableQuery->num_rows() > 0){
	    $data['enable_payrolldebit'] = '1';
	}else{
	    $data['enable_payrolldebit'] = '0';
	}
	    
	if($location_uid != ''){
	    $payrollQuery = $this->db2->query(" SELECT payroll_request, payroll_debit FROM `foodiq_payroll_users` WHERE uid='".$mobile."' AND location_uid='".$location_uid."' AND payroll_debit='1'");
	    if($payrollQuery->num_rows() > 0){
		$payrolldata = $payrollQuery->row();
		$data['payroll_request'] = $payrolldata->payroll_request;
		$data['payroll_debit'] = $payrolldata->payroll_debit;
	    }else{
                $data['payroll_request'] = '0';
		$data['payroll_debit'] = '0';
	    }
	}else{
	    $data['payroll_debit'] = '0';
	}
        
        
        return $data;
    }
    public function change_menuqty($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $cart_id = $jsondata->cart_id;
        $action = $jsondata->action;
        $cartqty = 0;
        
        $checkusercartQ = $this->db2->query("SELECT menu_id, COALESCE(SUM(qty), 0) as cartqty FROM foodiq_items_cart WHERE id='".$cart_id."'");
        if($checkusercartQ->num_rows() > 0){
            $rawdata = $checkusercartQ->row();
            $item_id = $rawdata->menu_id;
            if(!is_null($item_id)){
                $cartqty = $rawdata->cartqty;
                
                $totalquantityQ = $this->db2->query("SELECT food_item_quantity FROM foodiq_menulist WHERE id='".$item_id."'");
                $trawdata = $totalquantityQ->row();
                $totalQuantity = $trawdata->food_item_quantity;
                
                if($action == 'add'){
                    $chkqty = $cartqty + 1;
                }else{
                    $chkqty = $cartqty - 1;
                }
                
                if(($totalQuantity < ($chkqty)) && ($totalQuantity > 0)){
                    $data['resultCode'] = '404';
                    $data['resultMsg'] = 'No More Item currently available. Please try again later.';
                }
                else{
                    if($action == 'add'){
                        $this->db2->set('qty', '`qty`+1', FALSE);
                    }else{
                        $this->db2->set('qty', '`qty`-1', FALSE);
                    }
                    $this->db2->where('id', $cart_id);
                    $this->db2->update('foodiq_items_cart');
                    
                    $data['resultCode'] = '200';
                    $data['resultMsg'] = 'Item Qty Updated Successfully.';
                }
            }else{
                $data['resultCode'] = '404';
                $data['resultMsg'] = 'Item Not Found.';
            }
        }else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'Item Not Found.';
        }
        return $data;
    }
    public function search_item($jsondata){
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $search_term = $jsondata->search_term;
        
        $menulistQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity, tb2.menu_categoryname, tb2.product_titlename, tb2.product_itemshortdesc, tb1.food_item_price FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE tb1.isp_uid='".$isp_uid."' AND tb1.location_uid='".$location_uid."' AND tb1.status='1' AND MATCH(tb2.product_titlename,tb2.product_itemshortdesc) AGAINST('$search_term*' IN BOOLEAN MODE )");
        if($menulistQ->num_rows() > 0){
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'Items lists';
            $i = 0;
            
            foreach($menulistQ->result() as $itemobj){
                /*$id = $itemobj->id;
                $category = $itemobj->menu_categoryname;
                if (array_key_exists("$category", $data)) {
                    $data["$category"][$i]["category"] = $category;
                    $i++;
                }else{
                    $i = 0;
                }*/
                
                //$data["menutabs"][$id] = $category;
                $data["items"][$i]['item_id'] = $itemobj->id;
                $data["items"][$i]['item_name'] = $itemobj->product_titlename;
                $data["items"][$i]['item_shortdesc'] = $itemobj->product_itemshortdesc;
                $data["items"][$i]['item_price'] = $itemobj->food_item_price;
                $data["items"][$i]['item_quantity'] = $itemobj->food_item_quantity;
                $i++;
                
            }
            //$menutabs = array_unique($data["menutabs"]);
            //ksort($menutabs);
            //$data["menutabs"] = array_values($menutabs);
        }else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'No items found.';
        }
        
        return $data;
    }
    //https://stripe.com/blog/multiple-cards
    //https://stripe.com/docs/recipes/updating-customer-cards
    public function get_stripe_details($isp_uid, $location_uid){
	$stripe = array();
	if($location_uid != '0'){
	    $bankdetQ = $this->db2->query("SELECT * FROM foodiq_bank_details WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
	}else{
	    $bankdetQ = $this->db2->query("SELECT * FROM foodiq_bank_details WHERE isp_uid='".$isp_uid."' ORDER BY id DESC LIMIT 1");
	}
	if($bankdetQ->num_rows() > 0){
	    $bankrawdata = $bankdetQ->row();
	    $stripe['stripe_username'] = $bankrawdata->stripe_username;
	    $stripe['stripe_password'] = $bankrawdata->stripe_password;
	    $stripe['stripe_secret_key'] = $bankrawdata->stripe_secret_key;
	    $stripe['stripe_publication_key'] = $bankrawdata->stripe_publication_key;
	}else{
	    $stripe['stripe_username'] = 'rajiv@shouut.com';
	    $stripe['stripe_password'] = 'Shouut@081117';
	    $stripe['stripe_secret_key'] = 'sk_test_9MWVlAvbF9rdJCzkN6FsLLXk';
	    $stripe['stripe_publication_key'] = 'pk_test_BPzoRxhbOg68kZcgyEkNLh4Z';
	}
	
	return $stripe;
    }
    public function addtowallet($jsondata){
	$isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
	$stripe_details = $this->get_stripe_details($isp_uid, $location_uid);
        
	$this->load->library('Stripegateway', $stripe_details);
        
        $token_id = $jsondata->token_id;
        $mobile = $jsondata->mobile;
        $amount = $jsondata->amount;
        $currency = $jsondata->currency;
        $payment_type = $jsondata->payment_type; //addtowallet / payorder
	$card_id = $jsondata->card_id; //decode card_id first
	$save_card = $jsondata->save_card;
        
        $this->db2->select('email, id')->from('foodiq_users');
        $this->db2->where(array('uid' => $jsondata->mobile, 'isp_uid' => $isp_uid));
        $checkuserQ = $this->db2->get();
        $user_rawdata = $checkuserQ->row(); 
	
	if($payment_type == 'addtowallet'){
	    
	    if(($save_card == '0') && ($token_id != '')){
		$stripecharge = array(
		    'amount' => $amount,
		    'currency' => 'cad',
		    'token' => $token_id,		    
		    'description' => 'FoodIQ Charge for '.$user_rawdata->email
		);
		$stripe_response = $this->stripegateway->create_charges($stripecharge);
		$transaction_details = json_encode($stripe_response);
		$paystatus = $stripe_response->status;
	    }
	    else{
		
		$user_id = $user_rawdata->id;
		$stripecustQ = $this->db2->query("SELECT stripe_customer_id, card_id FROM foodiq_stripe_customer WHERE user_id='".$user_id."'");
		if($stripecustQ->num_rows() > 0){
		    $custrawdata = $stripecustQ->row();
		    $customer_id = $custrawdata->stripe_customer_id;
		    $customer_id = $this->encrypt->decode($customer_id);
		    $dbcard_id = $custrawdata->card_id;
		}
		
		if(($token_id != '') && ($card_id != '')){ //Add new card for save
		    if($stripecustQ->num_rows() > 0){
			$stripe_customer = array(
			    'token' => $token_id,
			    'customer_id' => $customer_id
			);
			$card_id = $this->stripegateway->create_card($stripe_customer);
			
			$stripecharge = array(
			    'amount' => $amount,
			    'currency' => 'cad',
			    'customer' => $customer_id,
			    'source' => $card_id,
			    'description' => 'FoodIQ Charge for '.$user_rawdata->email
			);
		    }
		    else{    
			$stripe_customer = array(
			    'source' => $token_id,
			    'email' => $user_rawdata->email
			);
			$customer_id = $this->stripegateway->create_customer($stripe_customer);
			
                        $stripe_customer = array(
			    'token' => $token_id,
			    'customer_id' => $customer_id
			);
			$card_id = $this->stripegateway->create_card($stripe_customer);
                        
			$stripecharge = array(
			    'amount' => $amount,
			    'currency' => 'cad',
			    'customer' => $customer_id,
			    'source' => $card_id,
			    'description' => 'FoodIQ Charge for '.$user_rawdata->email
			);
		    }
		    
		    
		    $card_id = $this->encrypt->encode($card_id);
		    $customer_id = $this->encrypt->encode($customer_id);
		    $this->db2->insert("foodiq_stripe_customer", array('token_id' => $token_id, 'stripe_customer_id' => $customer_id, 'user_id' => $user_id, 'card_id' => $card_id, 'mobile' => $mobile, 'added_on' => date('Y-m-d H:i:s') ));
		    
		}
		elseif(($token_id == '') && ($card_id != '')){ //Comes from Saved Card Lists
		    
		    $card_id = $this->encrypt->decode($card_id);
		    $stripecharge = array(
			'amount' => $amount,
			'currency' => 'cad',
			'customer' => $customer_id,
			'source' => $card_id,
			'description' => 'FoodIQ Charge for '.$user_rawdata->email
		    );
		}
		
		$stripe_response = $this->stripegateway->create_customer_charges($stripecharge);
		$transaction_details = json_encode($stripe_response);
		//var_dump($transaction_details); die;
		$paystatus = $stripe_response->status;
	    }
	    
	    if($paystatus == 'succeeded'){
		$total_amount = ($amount/100);
		
		$previous_wallet_balance = '0';
		$walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE isp_uid='".$isp_uid."' AND mobile='".$jsondata->mobile."'");
		if($walletQ->num_rows() > 0){
		    $wrawdata = $walletQ->row();
		    $previous_wallet_balance = $wrawdata->wallet_amount;
		    $this->db2->query("UPDATE foodiq_wallet SET wallet_amount = (wallet_amount + $total_amount) WHERE isp_uid='".$isp_uid."' AND mobile='".$jsondata->mobile."'");
		}else{
		    $this->db2->insert("foodiq_wallet", array("wallet_amount" => $total_amount, "isp_uid" => $isp_uid, "mobile" => $mobile));
		}
		
		$addorderQ = $this->db2->insert('foodiq_order', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'order_type' => 'addtowallet', 'mobile' => $mobile, 'amount' => $total_amount, 'previous_wallet_balance' => $previous_wallet_balance, 'payment_received' => '1', 'added_on' => date('Y-m-d H:i:s')));
		$order_id = $this->db2->insert_id();
		
		$this->db2->insert('foodiq_transaction_details', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'order_id' => $order_id, 'transaction_response' => $transaction_details, 'added_on' => date('Y-m-d H:i:s')));
		
		$data['resultCode'] = '200';
		$data['resultMsg'] = 'Payment Received.';
		$data['paystatus'] = $paystatus;
	    }
	    else{
		$data['resultCode'] = '402';
		$data['resultMsg'] = 'Payment Failed.';
		$data['paystatus'] = $paystatus;
		$this->db2->insert('foodiq_transaction_details', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'transaction_response' => $transaction_details, 'added_on' => date('Y-m-d H:i:s')));
	    }
	}else{
	    $data['resultCode'] = '400';
            $data['resultMsg'] = 'Invalid Payment Type';
	}
        return $data;
    }
    
    public function payrolldebit($jsondata){
	$data = array();	
	$isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        $amount = $jsondata->amount;
        $currency = $jsondata->currency;
	
        $this->db2->select('payroll_debit')->from('foodiq_users');
        $this->db2->where(array('uid' => $jsondata->mobile , 'payroll_debit' => '1'));
        $checkuserQ = $this->db2->get();
        
        if($checkuserQ->num_rows() > 0){            
	    
	    $walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE isp_uid='".$isp_uid."' AND mobile='".$jsondata->mobile."'");
	    if($walletQ->num_rows() > 0){
		$this->db2->query("UPDATE foodiq_wallet SET wallet_amount = (wallet_amount + $total_amount) WHERE isp_uid='".$isp_uid."' AND mobile='".$jsondata->mobile."'");
	    }else{
		$this->db2->insert("foodiq_wallet", array("wallet_amount" => $total_amount, "isp_uid" => $isp_uid, "mobile" => $mobile));
	    }
	    
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Payment Received.';
	    $data['paystatus'] = 'succeeded';
            
        }
        else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'Payroll Debit Option Not Available.';
	    $data['paystatus'] = 'failed';
        }
        
        return $data;
	
    }
    
    public function getlocation_name($location_uid){
        $locationQuery = $this->db2->query("SELECT location_name FROM wifi_location WHERE location_uid='".$location_uid."'");
        return $locationQuery->row()->location_name;
    }
    public function order_details($orderid){
        $items = array();
        $orderQuery = $this->db2->query("SELECT menu_name, qty, price FROM `foodiq_order_details` WHERE order_id='".$orderid."'");
        if($orderQuery->num_rows() > 0){
            $it = 0;
            foreach($orderQuery->result() as $ordrobj){
                $items[$it]['menu_name'] = $ordrobj->menu_name;
                $items[$it]['qty'] = $ordrobj->qty;
                $items[$it]['price'] = $ordrobj->price;
                $it++;
            }
        }
        return $items;
    }
    public function past_orders_list($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        
        $orderQuery = $this->db2->query("SELECT id, order_type,amount,location_uid FROM `foodiq_order` WHERE mobile='".$mobile."' ORDER BY added_on DESC");
        if($orderQuery->num_rows() > 0){
            $data['resultCode'] = '200';
            $i = 0;
            foreach($orderQuery->result() as $ordrobj){
                if($ordrobj->order_type == 'addtowallet'){
                    $data['transaction'][$i]['order_id'] = $ordrobj->id;
                    $data['transaction'][$i]['order_type'] = $ordrobj->order_type;
                    $data['transaction'][$i]['action'] = 'Amount Credit To Wallet';
                    $data['transaction'][$i]['amount'] = $ordrobj->amount;
                }
                elseif($ordrobj->order_type == 'orderpay'){
                    $data['transaction'][$i]['order_id'] = $ordrobj->id;
                    $data['transaction'][$i]['order_type'] = $ordrobj->order_type;
                    $data['transaction'][$i]['action'] = 'Amount Debit From Wallet';
                    $data['transaction'][$i]['amount'] = $ordrobj->amount;
                    $data['transaction'][$i]['location'] = $this->getlocation_name($ordrobj->location_uid);
                    //$data['transaction'][$i]['item_details'] = $this->order_details($ordrobj->id);
                }
                $i++;
            }
        }else{
	    $data['resultCode'] = '400';
            $data['resultMsg'] = 'No order placed yet.';
	}
        
        return $data;
    }
    public function transaction_details($orderid){
        $transdet = array();
        $transacQ = $this->db2->query("SELECT transaction_response FROM foodiq_transaction_details WHERE order_id='".$orderid."'");
        if($transacQ->num_rows() > 0){
            $rawdata = $transacQ->row();
            $transac_resp = json_decode($rawdata->transaction_response);
            $transdet['transaction_id'] = $transac_resp->balance_transaction;
            $transdet['card_id'] = $transac_resp->source->id;
            $transdet['customer_id'] = $transac_resp->source->customer;
            $transdet['brand'] = $transac_resp->source->brand;
            $transdet['exp_month'] = $transac_resp->source->exp_month;
            $transdet['exp_year'] = $transac_resp->source->exp_year;
            $transdet['last4'] = $transac_resp->source->last4;
            $transdet['name'] = $transac_resp->source->name;
            $transdet['funding'] = $transac_resp->source->funding; 
        }
        return $transdet;
    }
    public function past_orders_details($jsondata){
        $data = array();
        $order_id = $jsondata->order_id;
        //sg2plcpnl0068.prod.sin2.secureserver.net
        $orderQuery = $this->db2->query("SELECT id, order_type,amount,location_uid, added_on FROM `foodiq_order` WHERE id='".$order_id."'");
        if($orderQuery->num_rows() > 0){
            $data['resultCode'] = '200';
            $ordrobj = $orderQuery->row();
            if($ordrobj->order_type == 'addtowallet'){
                $data['order_id'] = $ordrobj->id;
                $data['order_type'] = $ordrobj->order_type;
                $data['action'] = 'Amount Credit To Wallet';
                $data['amount'] = $ordrobj->amount;
                $data['added_on'] = $ordrobj->added_on;
                $data['transaction_details'] = $this->transaction_details($ordrobj->id);
                
            }
            elseif($ordrobj->order_type == 'orderpay'){
                $data['order_id'] = $ordrobj->id;
                $data['order_type'] = $ordrobj->order_type;
                $data['action'] = 'Amount Debit From Wallet';
                $data['amount'] = $ordrobj->amount;
                $data['location'] = $this->getlocation_name($ordrobj->location_uid);
                $data['item_details'] = $this->order_details($ordrobj->id);
                $data['added_on'] = $ordrobj->added_on;
            }
        }else{
	    $data['resultCode'] = '400';
            $data['resultMsg'] = 'No order details found.';
	}
        
        return $data;
    }
    public function empty_cart_items($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        $mobile = $jsondata->mobile;
        $ordersessid = isset($jsondata->ordersessid) ? $jsondata->ordersessid : '';
	if($ordersessid != ''){
	    $mobile = $ordersessid;
	}
	
        $this->db2->query("DELETE FROM foodiq_items_cart WHERE mobile='".$mobile."' ");
        $data['resultCode'] = '200';
        $data['resultMsg'] = 'Cart is empty now!';
        
        return $data;
    }
    public function dietary_preferences($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        
        $data['resultCode'] = '200';
        $data['dietary_list'][0]['diet_id'] = '0';
        $data['dietary_list'][0]['dietary_name'] = 'All Types Of Food';
        
        $dietaryQ = $this->db2->query("select id, dietary_name from foodiq_dietary_preferences where isp_uid = '$isp_uid' ");
        if($dietaryQ->num_rows() > 0){
            $i = 1;
            foreach($dietaryQ->result() as $fobj){
                $data['dietary_list'][$i]['diet_id'] = $fobj->id;
                $data['dietary_list'][$i]['dietary_name'] = $fobj->dietary_name;
                $i++;
            }
            
            
        }
        
        return $data;
    }
    public function allergy_preferences($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        
        $data['resultCode'] = '200';
        $data['allergy_list'][0]['allergy_id'] = '0';
        $data['allergy_list'][0]['allergy_name'] = 'No Allergy';
        
        $dietaryQ = $this->db2->query("select id, allergy_name from foodiq_allergies where isp_uid = '$isp_uid' ");
        if($dietaryQ->num_rows() > 0){
            $i = 1;
            foreach($dietaryQ->result() as $fobj){
                $data['allergy_list'][$i]['allergy_id'] = $fobj->id;
                $data['allergy_list'][$i]['allergy_name'] = $fobj->allergy_name;
                $i++;
            }
            
            
        }
        
        return $data;
    }
    public function list_saved_cards($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
	$stripe_details = $this->get_stripe_details($isp_uid, $location_uid);
	$this->load->library('Stripegateway', $stripe_details);
	
	$mobileno = $jsondata->mobile;
	
	$autocardid = '';
	$autorechargeQ = $this->db2->query("SELECT min_amount_drops, amount_to_load, card_id FROM foodiq_auto_recharge WHERE isp_uid= '".$isp_uid."' AND mobile='".$mobileno."' AND status='1'");
	if($autorechargeQ->num_rows() > 0){
	    $autorawdata = $autorechargeQ->row();
	    $data['min_amount_drops'] = $autorawdata->min_amount_drops;
	    $data['amount_to_load'] = $autorawdata->amount_to_load;
	    
	    $autocardid = $this->encrypt->decode($autorawdata->card_id);
	}
	else{
	    $data['min_amount_drops'] = '';
	    $data['amount_to_load'] = '';
	}
	
        $this->db2->select('email, id')->from('foodiq_users');
        $this->db2->where(array('isp_uid' => $isp_uid, 'uid' => $jsondata->mobile));
        $checkuserQ = $this->db2->get();
        $user_rawdata = $checkuserQ->row();
	
	$user_id = $user_rawdata->id;
	$stripecustQ = $this->db2->query("SELECT stripe_customer_id FROM foodiq_stripe_customer WHERE user_id='".$user_id."'");
	if($stripecustQ->num_rows() > 0){
	    $customer_id = $stripecustQ->row()->stripe_customer_id;
	    $customer_id = $this->encrypt->decode($customer_id);
	    $card_details = array(
		'customer_id' => $customer_id
	    );
	    $stripe_response = $this->stripegateway->list_all_cards($card_details);
	    $data['resultCode'] = '200';
            $data['resultMsg'] = 'Saved Cards Found.';
	    
	    //print_r($stripe_response); die;
	    if(count($stripe_response) > 0){
		$i = 0;
		foreach($stripe_response as $stripeobj){
		    $data['saved_cards'][$i]['card_id'] = $this->encrypt->encode($stripeobj->id);
		    $data['saved_cards'][$i]['customer_id'] = $this->encrypt->encode($stripeobj->customer);
		    $data['saved_cards'][$i]['brand'] = $stripeobj->brand;
		    $data['saved_cards'][$i]['exp_month'] = $stripeobj->exp_month;
		    $data['saved_cards'][$i]['exp_year'] = $stripeobj->exp_year;
		    $data['saved_cards'][$i]['last4'] = $stripeobj->last4;
		    $data['saved_cards'][$i]['name'] = $stripeobj->name;
		    $data['saved_cards'][$i]['funding'] = $stripeobj->funding;
		    if($autocardid == $stripeobj->id){
			$data['saved_cards'][$i]['default_auto_recharge_card'] = '1';
		    }else{
			$data['saved_cards'][$i]['default_auto_recharge_card'] = '0';
		    }
		    $i++;
		}
	    }
	}
	else{
	    $data['resultCode'] = '404';
            $data['resultMsg'] = 'No Saved Cards Found.';
	}
        return $data;
    }
    
    public function foodiq_wallet_limit($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        
        $walletlimitQ = $this->db2->query("SELECT wallet_limit FROM foodiq_wallet_limit WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."'");
	if($walletlimitQ->num_rows() > 0){
            $rawdata = $walletlimitQ->row();
            $data['resultCode'] = '200';
            $data['wallet_limit'] = $rawdata->wallet_limit;
        }else{
            $data['resultCode'] = '200';
            $data['wallet_limit'] = '0';
        }
        return $data;
    }
    
    public function foodiq_wallet_slabs($jsondata){
        $data = array();
        $isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
        
        $walletlimitQ = $this->db2->query("SELECT slab_amount FROM foodiq_wallet_slabs WHERE isp_uid='".$isp_uid."' AND location_uid='".$location_uid."' ORDER BY slab_amount ASC");
	if($walletlimitQ->num_rows() > 0){
            $rawdata = $walletlimitQ->row();
            $data['resultCode'] = '200';
            $data['resultMsg'] = 'Slabs Found';
            $i = 0;
            foreach($walletlimitQ->result() as $wallobj){
                $data['slabs'][$i] = $wallobj->slab_amount;
                $i++;
            }
        }else{
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'No Slabs Found';
        }
        return $data;
    }
    public function delete_saved_card($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
	$stripe_details = $this->get_stripe_details($isp_uid, $location_uid);
	$this->load->library('Stripegateway', $stripe_details);
	
	$mobile  = $jsondata->mobile;
	$delcard_id = $jsondata->card_id;
	
	$is_matched = '0';
	$this->db2->select("id, stripe_customer_id, card_id")->from('foodiq_stripe_customer');
	$this->db2->where('mobile' , $mobile);
	$stripecustQ = $this->db2->get();
	if($stripecustQ->num_rows() > 0){
	    foreach($stripecustQ->result() as $custrawdata){
		$customer_id = $custrawdata->stripe_customer_id;
		$dbcard_id = $custrawdata->card_id;
		$id = $custrawdata->id;
		
		$dbcard_id = $this->encrypt->decode($dbcard_id);
		$customer_id = $this->encrypt->decode($customer_id);
		$chkcard_id = $this->encrypt->decode($delcard_id);
		
		if($dbcard_id == $chkcard_id){
		    $card_details = array(
			'customer_id' => $customer_id,
			'card_id' => $dbcard_id
		    );
		    $stripe_response = $this->stripegateway->delete_card($card_details);
		    
		    $this->db2->delete('foodiq_stripe_customer', array('id' => $id));
		    $is_matched = '1';
		    break;
		}
		else{
		    continue;
		}
	    }
	    
	    if($is_matched == '1'){
		
		$this->db2->select("id, card_id")->from('foodiq_auto_recharge');
		$this->db2->where(array('isp_uid' => $isp_uid, 'mobile' => $mobile, 'status' => '1'));
		$autorchrgQ = $this->db2->get();
		if($autorchrgQ->num_rows() > 0){
		    foreach($autorchrgQ->result() as $custrawdata){
			$dbcard_id = $custrawdata->card_id;
			$aid = $custrawdata->id;
			
			$dbcard_id = $this->encrypt->decode($dbcard_id);
			$chkcard_id = $this->encrypt->decode($delcard_id);
			
			if($dbcard_id == $chkcard_id){
			    $this->db2->update('foodiq_auto_recharge', array('status' => '0', 'updated_on' => date('Y-m-d H:i:s')), array('id' => $aid));
			    break;
			}
			else{
			    continue;
			}
		    }
		}
		
		$data['resultCode'] = '200';
		$data['resultMsg'] = 'Card deleted successfully.';
	    }
	    else{
		$data['resultCode'] = '404';
		$data['resultMsg'] = 'Card Not Found.';
	    }
	}
	else{
	    $data['resultCode'] = '404';
	    $data['resultMsg'] = 'Card Not Found.';
	}
	return $data;
    }
    public function payroll_request($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$location_uid = $jsondata->location_uid;
	$mobile = $jsondata->mobile;
	
	$payrollQuery = $this->db->query(" SELECT id FROM `foodiq_payroll_users` WHERE uid='".$mobile."' AND location_uid='".$location_uid."' AND payroll_request='1'");
	if($payrollQuery->num_rows() > 0){
	    $data['resultCode'] = '422';
	    $data['resultMsg'] = 'Your payroll debit has been applied for already. Awaiting approval from admin.';
	}
	else{
	    $this->db2->select('firstname, lastname, email, company, company_id, dob')->from('foodiq_users');
	    $this->db2->where('uid' , $mobile);
	    $checkuserQ = $this->db2->get();
	    
	    if($checkuserQ->num_rows() > 0){
		$postdata = $checkuserQ->row_array();
		$payrolldata = array(
		    'isp_uid' => $isp_uid,
		    'location_uid' =>$location_uid,
		    'firstname' => $postdata['firstname'],
		    'lastname' => $postdata['lastname'],
		    'uid' => $mobile,
		    'employee_id' => $postdata['company_id'],
		    'company_name' => $postdata['company'],
		    'dob' => $postdata['dob'],
		    'email' => $postdata['email'],
		    'payroll_request' => '1',
		    'requested_on' => date('Y-m-d H:i:s')
		);
		
		
		$payrollQuery = $this->db->query(" SELECT id FROM `foodiq_payroll_users` WHERE uid='".$mobile."' AND location_uid='".$location_uid."' ");
		if($payrollQuery->num_rows() > 0){
		    $this->db->update('foodiq_payroll_users', $payrolldata, array('id' => $emp_id));
		}else{
		    $this->db->insert('foodiq_payroll_users', $payrolldata);
		}
	    }
	    
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Your payroll debit request has been submitted successfully.';
	}
	
	return $data;
    }
    public function foodiq_policies($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$policytype = $jsondata->policytype;
	
	$policyQ = $this->db->query("SELECT term_condition, privacy_policy FROM sht_isp_detail WHERE isp_uid='".$isp_uid."'");
	if($policyQ->num_rows() > 0){
	    $rawdata = $policyQ->row();
            if($policytype == 'term_condition'){
                $data['term_condition'] = $rawdata->term_condition;
            }else{
                $data['privacy_policy'] = $rawdata->privacy_policy;
            }
	}
	return $data;
    }
    
    public function foodiq_credentials($jsondata){
	$isp_uid = $jsondata->isp_uid;
	$location_uid = $jsondata->location_uid;
	$credentials = $this->get_stripe_details($isp_uid, $location_uid);
	return $credentials;
    }

    public function importusers(){
	$shtusersQ = $this->db2->query("SELECT isp_uid, uid, enableuser, firstname, lastname, email, company, company_id, dob, address, food_type, food_allergy, mobile, payroll_debit, country_code, createdon FROM sht_users WHERE baseplanid IS NULL AND (createdon BETWEEN '2018-12-03' AND '2018-12-06') ");
	foreach($shtusersQ->result() as $jsondata){
	    $userdataArr = array(
                'isp_uid' => $jsondata->isp_uid,
                'enableuser' => $jsondata->enableuser,
                'uid' => $jsondata->uid,
                'username' => $jsondata->mobile,
                'firstname' => $jsondata->firstname,
                'lastname' => $jsondata->lastname,
                'email' => $jsondata->email,
                'company' => $jsondata->company,
                'company_id' => $jsondata->company_id,
                'dob' => $jsondata->dob,
                'address' => $jsondata->address,
                'food_type' => $jsondata->food_type,
                'food_allergy' => $jsondata->food_allergy,
                'country_code' => $jsondata->country_code,
                'mobile' => $jsondata->mobile,
		'payroll_debit' => $jsondata->payroll_debit,
		'createdon' => $jsondata->createdon
            );
            //print_r($userdataArr); die;
            $this->db2->insert('foodiq_users', $userdataArr);
	}
    }
    public function update_forgot_password($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$this->db2->select('id')->from('foodiq_users');
	if($jsondata->pagetype == 'forgotpage'){
	    $this->db2->where(array('isp_uid' => $isp_uid, 'uid' => $jsondata->mobile, 'enableuser' => '1'));
	}else{
	    $this->db2->where(array('isp_uid' => $isp_uid, 'uid' => $jsondata->mobile));
	}
        $checkuserQ = $this->db2->get();
        
	$newpassword = md5($jsondata->newpassword);
	
        if($checkuserQ->num_rows() > 0){
	    $this->db2->update('foodiq_users', array('password' => $newpassword), array('isp_uid' => $isp_uid, 'uid' => $jsondata->mobile) );
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Password Updated.';
	}else{
	    $data['resultCode'] = '404';
	    $data['resultMsg'] = 'User doesnot exists or unverified.';
	}
	
	return $data;
    }
    /***********************************************************************************
     *		TABLET APIS BEGINS
     **********************************************************************************/
    
    public function check_location_existence($jsondata){
	$data = array();
	$location_uid = $jsondata->location_uid;
	$getlocationQuery = $this->db2->query("SELECT isp_uid, location_uid, location_name, geo_address, placeid, latitude, longitude FROM wifi_location WHERE location_uid='".$location_uid."' AND status='1' AND is_deleted='0' AND location_type='15'");
	if($getlocationQuery->num_rows() > 0){
	    $data = $getlocationQuery->row_array();
	    $data['resultCode'] = '200';
            $data['resultMsg'] = 'Location details';
	}else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'Invalid LocationID get entered.';
        }
        
        return $data;
    }

    public function pay_as_guest($jsondata){
	$isp_uid = $jsondata->isp_uid;
        $location_uid = $jsondata->location_uid;
	$stripe_details = $this->get_stripe_details($isp_uid, $location_uid);
	
	/*$stripe_details = array();
	$stripe_details['stripe_username'] = 'rajiv@shouut.com';
	$stripe_details['stripe_password'] = 'Shouut@081117';
	$stripe_details['stripe_secret_key'] = 'sk_test_9MWVlAvbF9rdJCzkN6FsLLXk';
	$stripe_details['stripe_publication_key'] = 'pk_test_BPzoRxhbOg68kZcgyEkNLh4Z';*/
        
	$this->load->library('Stripegateway', $stripe_details);
        
        $token_id = $jsondata->token_id;
        $ordersessid = $jsondata->ordersessid;
        $amount = $jsondata->total_amount;
	
	$stripecharge = array(
	    'amount' => $amount,
	    'currency' => 'cad',
	    'token' => $token_id,		    
	    'description' => 'FoodIQ Charge for guest user '.$ordersessid
	);
	$stripe_response = $this->stripegateway->create_charges($stripecharge);
	$transaction_details = json_encode($stripe_response);
	$paystatus = $stripe_response->status;
	
	if($paystatus == 'succeeded'){
	    $total_amount = ($amount/100);
	    
	    $addorderQ = $this->db2->insert('foodiq_guest_order', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'order_type' => 'orderpay', 'guest_user' => $ordersessid, 'amount' => $total_amount, 'payment_received' => '1', 'added_on' => date('Y-m-d H:i:s')));
	    $order_id = $this->db2->insert_id();
	    
	    $checkusercartQ = $this->db2->query("SELECT tb1.menu_id, tb1.qty, tb3.product_titlename, tb2.food_item_price FROM foodiq_items_cart as tb1 LEFT JOIN foodiq_menulist as tb2 ON(tb1.menu_id=tb2.id) INNER JOIN foodiq_master_menulist as tb3 ON(tb2.master_menu_id=tb3.id) WHERE tb1.mobile='".$ordersessid."' AND tb1.location_uid='".$location_uid."' AND DATE(tb1.added_on)='".date('Y-m-d')."'");
	    if($checkusercartQ->num_rows() > 0){
		foreach($checkusercartQ->result() as $cartobj){
		    $qty = $cartobj->qty;
		    $price = ($qty * $cartobj->food_item_price);
		    $this->db2->insert('foodiq_guest_order_details', array('order_id' => $order_id, 'menu_id' => $cartobj->menu_id, 'menu_name' => $cartobj->product_titlename, 'qty' => $cartobj->qty, 'price' => $price));
		    
		    
		    $finalqty = '`food_item_quantity` - '.$qty;
		    $this->db2->set('food_item_quantity', $finalqty , FALSE);
		    $this->db2->where('id', $cartobj->menu_id);
		    $this->db2->update('foodiq_menulist');
		}
	    }
	    
	    $this->db2->delete("foodiq_items_cart", array('mobile' => $ordersessid));    
	    $this->db2->insert('foodiq_transaction_details', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $ordersessid, 'order_id' => $order_id, 'transaction_response' => $transaction_details, 'added_on' => date('Y-m-d H:i:s')));
	    
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Payment Received.';
	    $data['paystatus'] = $paystatus;
	}
	else{
	    $data['resultCode'] = '402';
	    $data['resultMsg'] = 'Payment Failed.';
	    $data['paystatus'] = $paystatus;
	    $this->db2->insert('foodiq_transaction_details', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $ordersessid, 'transaction_response' => $transaction_details, 'added_on' => date('Y-m-d H:i:s')));
	}
        
	return $data;
    }
    public function manage_autorecharge($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$location_uid = $jsondata->location_uid;
	$mobile = $jsondata->mobile;
	$amount_to_load = $jsondata->amount_to_load;
	$min_amount_drops = $jsondata->min_amount_drops;
	$card_id = $jsondata->card_id;
	$token_id = $jsondata->token_id;
	$customer_id = isset($jsondata->customer_id) ? $jsondata->customer_id : '';
	$remove_autorecharge = isset($jsondata->remove_autorecharge) ? $jsondata->remove_autorecharge : '0';
	
	$autorechargeQ = $this->db2->query("SELECT id, status FROM foodiq_auto_recharge WHERE isp_uid='".$isp_uid."' AND mobile='".$mobile."' AND card_id='".$card_id."'"); 
	
	if($remove_autorecharge == '1'){
	    $this->db2->update('foodiq_auto_recharge', array('status' => '0', 'updated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $isp_uid, 'mobile' => $mobile));
	    
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Card Removed Successfully.';
	}
	else if(($card_id != 'newcard') && ($token_id == '')){
	    if($autorechargeQ->num_rows() == 0){
		$this->db2->insert('foodiq_auto_recharge', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'amount_to_load'=> $amount_to_load, 'min_amount_drops' => $min_amount_drops, 'customer_id' => $customer_id, 'card_id' => $card_id, 'status' => '1', 'added_on' => date('Y-m-d H:i:s') ));
		$this->db2->update('foodiq_auto_recharge', array('status' => '0', 'updated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $isp_uid, 'mobile' => $mobile, 'card_id !=' => $card_id));
	    }
	    else{
		$this->db2->update('foodiq_auto_recharge', array('amount_to_load' => $amount_to_load, 'min_amount_drops' => $min_amount_drops, 'status' => '1', 'updated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $isp_uid, 'mobile' => $mobile, 'card_id' => $card_id));
		$this->db2->update('foodiq_auto_recharge', array('status' => '0', 'updated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $isp_uid, 'mobile' => $mobile, 'card_id !=' => $card_id));
	    }
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Card AutoRecharge Saved.';
	}
	else if(($card_id == 'newcard') && ($token_id != '')){
	    
	    $stripe_details = $this->get_stripe_details($isp_uid, $location_uid);
	    $this->load->library('Stripegateway', $stripe_details);
	    
	    $this->db2->select('email, id')->from('foodiq_users');
	    $this->db2->where(array('isp_uid' => $isp_uid, 'uid' => $jsondata->mobile));
	    $checkuserQ = $this->db2->get();
	    $user_rawdata = $checkuserQ->row(); 
	    $user_id = $user_rawdata->id;
	    
	    $stripecustQ = $this->db2->query("SELECT stripe_customer_id, card_id FROM foodiq_stripe_customer WHERE user_id='".$user_id."'");
	    if($stripecustQ->num_rows() > 0){
		$custrawdata = $stripecustQ->row();
		$customer_id = $this->encrypt->decode($custrawdata->stripe_customer_id);
		$dbcard_id = $custrawdata->card_id;
	    }
	    else{
		$stripe_customer = array(
		    'source' => $token_id,
		    'email' => $user_rawdata->email
		);
		$customer_id = $this->stripegateway->create_customer($stripe_customer);
	    }
	    
	    $stripe_card = array(
		'token' => $token_id,
		'customer_id' => $customer_id
	    );
	    $card_id = $this->stripegateway->create_card($stripe_card);
	    
	    
	    $card_id = $this->encrypt->encode($card_id);
	    $customer_id = $this->encrypt->encode($customer_id);
	    if($autorechargeQ->num_rows() == 0){
		$this->db2->insert('foodiq_auto_recharge', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'amount_to_load'=> $amount_to_load, 'min_amount_drops' => $min_amount_drops, 'customer_id' => $customer_id, 'card_id' => $card_id, 'status' => '1', 'added_on' => date('Y-m-d H:i:s') ));
		$this->db2->update('foodiq_auto_recharge', array('status' => '0', 'updated_on' => date('Y-m-d H:i:s')), array('isp_uid' => $isp_uid, 'mobile' => $mobile, 'card_id !=' => $card_id));
	    }
	    
	    $this->db2->insert("foodiq_stripe_customer", array('token_id' => $token_id, 'stripe_customer_id' => $customer_id, 'user_id' => $user_id, 'card_id' => $card_id, 'mobile' => $mobile, 'added_on' => date('Y-m-d H:i:s') ));
	    
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Card AutoRecharge Saved.';
	}
	else {
	    $data['resultCode'] = '401';
	    $data['resultMsg'] = 'Something went wrong. Please try again.';
	}
	
	return $data;
    }
    public function wallet_autorecharge_cron($jsondata){
	$isp_uid = $jsondata->isp_uid;
	$mobile = $jsondata->mobile;
	$location_uid = '0';
	//$card_id = isset($jsondata->card_id) ? $jsondata->card_id : '';
	
	$walletQ = $this->db2->query("SELECT wallet_amount FROM foodiq_wallet WHERE isp_uid='".$isp_uid."' AND mobile='".$mobile."'");
	if($walletQ->num_rows() > 0){
	    $cart_rawdata = $walletQ->row();
	    $wallet_amount = $cart_rawdata->wallet_amount;
	}else{
	    $wallet_amount = '0';
	}
	
	$autorechargeQ = $this->db2->query("SELECT min_amount_drops, amount_to_load, card_id, customer_id FROM foodiq_auto_recharge WHERE isp_uid='".$isp_uid."' AND mobile='".$mobile."' AND status='1' AND min_amount_drops >= $wallet_amount");
	//echo $this->db2->last_query(); die;
	if($autorechargeQ->num_rows() > 0){
	    
	    $rechrawdata = $autorechargeQ->row();
	    $min_amount_drops = $rechrawdata->min_amount_drops;
	    $amount_to_load = $rechrawdata->amount_to_load;
	    $card_id = $rechrawdata->card_id;
	    
	    $stripe_details = $this->get_stripe_details($isp_uid, '0');
	    $this->load->library('Stripegateway', $stripe_details);
	    
	    $this->db2->select('id, email, uid')->from('foodiq_users');
	    $this->db2->where(array('isp_uid' => $isp_uid, 'uid' => $mobile));
	    $allusersQ = $this->db2->get();
	    
	    if($allusersQ->num_rows() > 0){
		foreach($allusersQ->result() as $userobj){
		    $mobile = $userobj->uid;
		    $user_id = $userobj->id;
		    $email = $userobj->email;
		    
		    if($wallet_amount <= $min_amount_drops){
			$stripecustQ = $this->db2->query("SELECT stripe_customer_id, card_id FROM foodiq_stripe_customer WHERE user_id='".$user_id."' AND card_id='".$card_id."'");
			//echo $this->db2->last_query(); die;
			if($stripecustQ->num_rows() > 0){
			    $custrawdata = $stripecustQ->row();
			    $customer_id = $custrawdata->stripe_customer_id;
			    $card_id = $custrawdata->card_id;
			    
			    $stripecharge = array(
				'amount' => ($amount_to_load * 100),
				'currency' => 'cad',
				'customer' => $customer_id,
				'source' => $card_id,
				'description' => 'FoodIQ Charge for '.$email
			    );
			    $stripe_response = $this->stripegateway->create_customer_charges($stripecharge);
			    $transaction_details = json_encode($stripe_response);
			    $paystatus = $stripe_response->status;
			    
			    if($paystatus == 'succeeded'){
				$total_amount = ($amount_to_load);
				
				if($walletQ->num_rows() > 0){
				    $this->db2->query("UPDATE foodiq_wallet SET wallet_amount = (wallet_amount + $total_amount) WHERE isp_uid='".$isp_uid."' AND mobile='".$mobile."'");
				}else{
				    $this->db2->insert("foodiq_wallet", array("wallet_amount" => $total_amount, "isp_uid" => $isp_uid, "mobile" => $mobile));
				}
				
				$addorderQ = $this->db2->insert('foodiq_order', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'order_type' => 'addtowallet', 'mobile' => $mobile, 'amount' => $total_amount, 'previous_wallet_balance' => $wallet_amount, 'payment_received' => '1', 'added_on' => date('Y-m-d H:i:s')));
				$order_id = $this->db2->insert_id();
				
				$this->db2->insert('foodiq_transaction_details', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'order_id' => $order_id, 'transaction_response' => $transaction_details, 'added_on' => date('Y-m-d H:i:s')));
			    }
			    else{
				$this->db2->insert('foodiq_transaction_details', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'mobile' => $mobile, 'transaction_response' => $transaction_details, 'added_on' => date('Y-m-d H:i:s')));
			    }
			}
		    }
		}
	    }
	}
	return 1;
    }
    public function check_apk_version($jsondata){
	$data = array();
	$versionQ = $this->db2->query("SELECT version,is_force_update FROM `foodiq_apk_version` WHERE isp_uid='".$jsondata->isp_uid."' ORDER BY added_on DESC LIMIT 1 ");
	if($versionQ->num_rows() > 0){
	    $rawdata = $versionQ->row();
	    $data['version'] = $rawdata->version;
	    $data['is_force_update'] = $rawdata->is_force_update;
	}
	return $data;
    }
    public function encrypt_bankdetails(){
	$this->db2->select('id, stripe_customer_id, card_id')->from('foodiq_stripe_customer');
	$stripecustQ = $this->db2->get();
	if($stripecustQ->num_rows() > 0){
	    foreach($stripecustQ->result() as $stobj){
		$card_id = $this->encrypt->encode($stobj->card_id);
		$customer_id = $this->encrypt->encode($stobj->stripe_customer_id);
		
		$this->db2->update('foodiq_stripe_customer', array('stripe_customer_id' => $customer_id, 'card_id' => $card_id), array('id' => $stobj->id));
	    }
	}
	
	
	$this->db2->select('id, customer_id, card_id')->from('foodiq_auto_recharge');
	$stripecustQ = $this->db2->get();
	if($stripecustQ->num_rows() > 0){
	    foreach($stripecustQ->result() as $stobj){
		$card_id = $this->encrypt->encode($stobj->card_id);
		$customer_id = $this->encrypt->encode($stobj->customer_id);
		
		$this->db2->update('foodiq_auto_recharge', array('customer_id' => $customer_id, 'card_id' => $card_id), array('id' => $stobj->id));
	    }
	}
    }
    /***********************************************************************************
    *		INVENTORY APIS BEGINS
    **********************************************************************************/
    public function check_trash_items($location_uid=''){
	$today_date = date('Y-m-d');
	$where = ' 1 AND ';
	if($location_uid != ''){
	    $where = " tb1.location_uid = '$location_uid' AND ";
	}
	$foodmenuQ = $this->db2->query("SELECT tb1.id, tb1.isp_uid, tb1.location_uid, tb2.is_perishable, tb2.perishable_days FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE $where tb1.status = '1'");
	if($foodmenuQ->num_rows() > 0){
	    foreach($foodmenuQ->result() as $fobj){
		$menu_id = $fobj->id;
		$is_perishable = $fobj->is_perishable;
		$perishable_days = $fobj->perishable_days;
		$isp_uid = $fobj->isp_uid;
		$location_uid = $fobj->location_uid;
		
		if($is_perishable == '1'){
		    $invntryQ = $this->db2->query("SELECT COALESCE(SUM(quantity),0) as storeqty  FROM foodiq_inventory_actions WHERE menu_id='".$menu_id."' AND action='addtoshelf' AND (CURDATE() >= DATE_ADD( DATE(added_on), INTERVAL ".$perishable_days." DAY ))");
		}else{
		    $invntryQ = $this->db2->query("SELECT COALESCE(SUM(quantity),0) as storeqty  FROM foodiq_inventory_actions WHERE menu_id='".$menu_id."' AND action='addinventory' AND (CURDATE() >= DATE_ADD( DATE(added_on), INTERVAL ".$perishable_days." DAY ))");
		}
		
		if($invntryQ->num_rows() > 0){
		    $laststoreqty = $invntryQ->row()->storeqty;
		    
		    $order_qty = 0;
		    $orderQuery = $this->db2->query("SELECT COALESCE(SUM(tb2.qty),0) as itemorderqty FROM `foodiq_order` as tb1 INNER JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE (CURDATE() >= DATE_ADD( DATE(tb1.added_on), INTERVAL ".$perishable_days." DAY )) AND tb2.menu_id='".$menu_id."'");
		    if($orderQuery->num_rows() > 0){
			$order_qty = $orderQuery->row()->itemorderqty;
		    }
		    
		    if($order_qty < $laststoreqty){
			$trashqty = ($laststoreqty - $order_qty);
			
			if($trashqty != '0'){
			    $chktodayoptionQ = $this->db2->query("SELECT id FROM foodiq_inventory_automated WHERE location_uid = '$location_uid' AND menu_id = '$menu_id' AND action = 'trashitems'");
			    if($chktodayoptionQ->num_rows() == 0){
				$this->db2->insert('foodiq_inventory_automated', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'menu_id' => $menu_id, 'quantity' => $trashqty, 'action' => 'trashitems', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
			    }
			    else{
				$trshid = $chktodayoptionQ->row()->id;
				$updated_on = date('Y-m-d H:i:s');
				
				$this->db2->set(array('quantity' => $trashqty, 'updated_on' => $updated_on));
				$this->db2->where(array('id' => $trshid));
				$this->db2->update('foodiq_inventory_automated');
			    }
			}
			/*$items_rol = '0';
			$getitem_rol = $this->db2->query("SELECT location_refiller_uid, items_rol FROM foodiq_inventory_settings WHERE isp_uid='$isp_uid' AND location_uid='$location_uid' AND status='1' AND is_deleted='0'");
			if($getitem_rol->num_rows() > 0){
			    $itemrawdata = $getitem_rol->row_array();
			    $items_rol = $itemrawdata['items_rol'];
			    $refiller_ids = $itemrawdata['location_refiller_uid'];
			}
			
			if(strpos($refiller_ids, ',') !== FALSE){
			    $refillerArr = explode(',' , $refiller_ids);
			    foreach($refillerArr as $refiller_id){
				$chktodayoptionQ = $this->db2->query("SELECT id FROM foodiq_inventory_automated WHERE location_uid = '$location_uid' AND refiller_id = '$refiller_id' AND menu_id = '$menu_id' AND action = 'trashitems' AND DATE(added_on) = '".date('Y-m-d')."'");
				if($chktodayoptionQ->num_rows() == 0){
				    $this->db2->insert('foodiq_inventory_automated', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menu_id, 'quantity' => $trashqty, 'action' => 'trashitems', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
				}
			    }
			}
			else{
			    $chktodayoptionQ = $this->db2->query("SELECT id FROM foodiq_inventory_automated WHERE location_uid = '$location_uid' AND refiller_id = '$refiller_ids' AND menu_id = '$menu_id' AND action = 'trashitems' AND DATE(added_on) = '".date('Y-m-d')."'");
			    if($chktodayoptionQ->num_rows() == 0){
			        $this->db2->insert('foodiq_inventory_automated', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_ids, 'menu_id' => $menu_id, 'quantity' => $trashqty, 'action' => 'trashitems', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
			    }
			}
			*/
		    }
		}
		
	    }
	}
	return 1;
    }
    
    public function check_moveable_items($location_uid=''){
	$today_date = date('Y-m-d');
	$where = ' 1 AND ';
	if($location_uid != ''){
	    $where = " tb1.location_uid = '$location_uid' AND ";
	}
	$foodmenuQ = $this->db2->query("SELECT tb1.id, tb1.master_menu_id, tb1.isp_uid, tb1.location_uid, tb1.food_item_quantity FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id=tb2.id) WHERE $where tb1.status = '1' AND tb2.is_perishable='0'");
	//echo $this->db2->last_query(); die;
	if($foodmenuQ->num_rows() > 0){
	    foreach($foodmenuQ->result() as $fobj){
		$menu_id = $fobj->id;
		$isp_uid = $fobj->isp_uid;
		$location_uid = $fobj->location_uid;
		$food_item_quantity = $fobj->food_item_quantity;
		
		$items_rol = '0';
		$getitem_rol = $this->db2->query("SELECT location_refiller_uid, items_rol FROM foodiq_inventory_settings WHERE isp_uid='$isp_uid' AND location_uid='$location_uid' AND status='1' AND is_deleted='0'");
		if($getitem_rol->num_rows() > 0){
		    $itemrawdata = $getitem_rol->row_array();
		    $items_rol = $itemrawdata['items_rol'];
		    $refiller_ids = $itemrawdata['location_refiller_uid'];
		}
		
		$storeqty = 0;
		$chkwarehouse = $this->db2->query("SELECT quantity FROM foodiq_warehouse WHERE menu_id='$menu_id' AND location_uid='$location_uid'");
		if($chkwarehouse->num_rows() > 0){
		    $storeqty = $chkwarehouse->row()->quantity;
		}
		
		//SELECT COALESCE(SUM(tb2.qty),0) as orderqty FROM `foodiq_order` as tb1 RIGHT JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE DATE(tb1.added_on) >= ( CURDATE() - INTERVAL $items_rol DAY ) AND tb2.menu_id='$menu_id' 
		$getordersfor_rolQ = $this->db2->query("SELECT COALESCE(SUM(tb2.qty),0) as orderqty FROM `foodiq_order` as tb1 RIGHT JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE DATE(tb1.added_on) >= ( CURDATE() - INTERVAL $items_rol DAY ) AND tb2.menu_id='$menu_id' ");
		$last_orderqty = $getordersfor_rolQ->row()->orderqty;
		
		$recommend_move_qty = 0;
		if($food_item_quantity < $last_orderqty){
		    $recommend_move_qty = ($last_orderqty - $food_item_quantity);
		    if($storeqty == '0'){
			$recommend_move_qty = '0';
		    }
		    elseif($storeqty < $recommend_move_qty){
			$recommend_move_qty = $storeqty;
		    }
		}
		
		if($recommend_move_qty != '0'){
		    $chktodayoptionQ = $this->db2->query("SELECT id FROM foodiq_inventory_automated WHERE location_uid = '$location_uid' AND menu_id = '$menu_id' AND action = 'moveitems'");
		    if($chktodayoptionQ->num_rows() == 0){
			$this->db2->insert('foodiq_inventory_automated', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'menu_id' => $menu_id, 'quantity' => $recommend_move_qty, 'action' => 'moveitems', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
		    }
		    else{
			$rcmdid = $chktodayoptionQ->row()->id;
			$updated_on = date('Y-m-d H:i:s');
			
			$this->db2->set(array('quantity' => $recommend_move_qty, 'updated_on' => $updated_on));
			$this->db2->where(array('id' => $rcmdid));
			$this->db2->update('foodiq_inventory_automated');
		    }
		    
		    /*
		    if(strpos($refiller_ids, ',') !== FALSE){
			$refillerArr = explode(',' , $refiller_ids);
			foreach($refillerArr as $refiller_id){
			    
			    $chktodayoptionQ = $this->db2->query("SELECT id FROM foodiq_inventory_automated WHERE location_uid = '$location_uid' AND refiller_id = '$refiller_id' AND menu_id = '$menu_id' AND action = 'moveitems' AND DATE(added_on) = '".date('Y-m-d')."'");
			    if($chktodayoptionQ->num_rows() == 0){
				$this->db2->insert('foodiq_inventory_automated', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menu_id, 'quantity' => $recommend_move_qty, 'action' => 'moveitems', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
			    }
			}
		    }
		    else{
			$chktodayoptionQ = $this->db2->query("SELECT id FROM foodiq_inventory_automated WHERE location_uid = '$location_uid' AND refiller_id = '$refiller_ids' AND menu_id = '$menu_id' AND action = 'moveitems' AND DATE(added_on) = '".date('Y-m-d')."'");
			if($chktodayoptionQ->num_rows() == 0){
			    $this->db2->insert('foodiq_inventory_automated', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_ids, 'menu_id' => $menu_id, 'quantity' => $recommend_move_qty, 'action' => 'moveitems', 'status' => '1', 'added_on' => date('Y-m-d H:i:s')));
			}
		    }
		    */
		}
	    }
	}
	
	return 1;
    }
    
    public function scan_inventory_item($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$item_barcode = $jsondata->item_barcode;
	$location_uid = $jsondata->location_uid;
	
	$checkbarcodeQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity, tb2.product_titlename, tb2.food_carret_barcode, tb2.food_item_barcode, tb2.food_carret_quantity, tb2.is_perishable, tb2.perishable_days, tb2.supplier_name FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id = tb2.id) WHERE tb1.location_uid = '".$location_uid."' AND tb1.status='1' AND (tb2.food_item_barcode='".$item_barcode."' OR tb2.food_carret_barcode='".$item_barcode."') ");
        if($checkbarcodeQ->num_rows() > 0){
	    
	    $data['resultCode'] = '200';
            $data['resultMsg'] = 'Item Found.';
	    
	    $rawdata = $checkbarcodeQ->row();
	    $food_carton_barcode = $rawdata->food_carret_barcode;
	    $food_item_barcode = $rawdata->food_item_barcode;
	    $food_item_quantity = $rawdata->food_item_quantity;
	    $cartonqty = $rawdata->food_carret_quantity;
	    $menu_id = $rawdata->id;
	    $perishable_days = $rawdata->perishable_days;
	    
	    $data['menu_id'] = $menu_id;
	    $data['product_titlename'] = $rawdata->product_titlename;
	    $data['item_barcode'] = $item_barcode;
	    $data['supplier_name'] = $rawdata->supplier_name;
	    if($food_carton_barcode == $item_barcode){
		$data['scan_itemtype'] = 'Carton';
	    }else{
		$data['scan_itemtype'] = 'Item';
	    }
	    $data['shelf_qty'] = $food_item_quantity;
	    
	    $storeqty = 0;
	    $chkwarehouse = $this->db2->query("SELECT quantity FROM foodiq_warehouse WHERE menu_id='$menu_id' AND location_uid='$location_uid'");
	    if($chkwarehouse->num_rows() > 0){
		$storeqty = $chkwarehouse->row()->quantity;
	    }
	    $data['stock_qty'] = $storeqty;
	    
	    if($data['scan_itemtype'] == 'Carton'){
		$data['recommend_add_qty'] = $cartonqty;
	    }else{
		$data['recommend_add_qty'] = '';
	    }
	    
	    $data['is_perishable'] = $rawdata->is_perishable;
	}
	else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'This item currently not get added. Please try again later.';
        }
        return $data;
    }
    
    public function refiller_allocated_locations($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	
	/*$this->db2->select('id')->from('foodiq_location_refiller');
	$this->db2->where(array('refiller_mobile' => $refiller_uid, 'status' => '1', 'is_deleted' => '0'));
	$checkrefillerQ = $this->db2->get();
	if($checkrefillerQ->num_rows() > 0){
	    $refdata = $checkrefillerQ->row();
	    $refiller_id = $refdata->id;
	}*/
	
	$getlocationQuery = $this->db2->query("SELECT tb1.location_uid, tb1.location_name FROM wifi_location as tb1 INNER JOIN foodiq_inventory_settings as tb2 ON(tb1.location_uid = tb2.location_uid) WHERE FIND_IN_SET( '$refiller_id', tb2.location_refiller_uid ) AND tb2.status='1' AND tb2.is_deleted='0'");
        if($getlocationQuery->num_rows() > 0){
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Refiller associated locations found.';
	    
	    $i = 0;
	    foreach($getlocationQuery->result() as $locobj){
		$data['allocated_location'][$i]['location_uid'] = $locobj->location_uid;
		$data['allocated_location'][$i]['location_name'] = $locobj->location_name;
		$i++;
	    }
	}else{
	    $data['resultCode'] = '404';
	    $data['resultMsg'] = 'No location found.';
	}
	
	return $data;
    }
    public function inventory_action_options($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	$location_uid = $jsondata->location_uid;
	$action_options = $jsondata->action_options;
	$menu_id = $jsondata->menu_id;
	$storeqty = $jsondata->storeqty;
	$menu_barcode = $jsondata->barcode;
	$perishable_days = '0'; $is_perishable = '0';
	
	
	$movefrom_options = "<option value = ''>Select From</option>";
	$moveto_options = "<option value = ''>Select To</option>";
	$returnfrom_options = "<option value = ''>Select From</option>";
	$returnto_options = "<option value = ''>Select To</option>";
	$trashfrom_options = "<option value = ''>Select From</option>";
	$adjustfrom_options = "<option value = ''>Select From</option>";
	
	
	$this->db2->select("tb1.id, tb1.food_item_quantity, tb2.supplier_name, tb2.is_perishable, tb2.perishable_days")->from("foodiq_menulist as tb1");
	$this->db2->join('foodiq_master_menulist as tb2', 'tb1.master_menu_id = tb2.id', 'inner');
	$this->db2->where(array('tb1.id' => $menu_id));
	$menudetQ = $this->db2->get();
	$menudet_rawdata = $menudetQ->row_array();
	$supplier_name = $menudet_rawdata['supplier_name'];
	$food_item_quantity = $menudet_rawdata['food_item_quantity'];
	$perishable_days = $menudet_rawdata['perishable_days'];
	$is_perishable = $menudet_rawdata['is_perishable'];
	
	$getlocationQuery = $this->db2->query("SELECT tb1.location_uid, tb1.location_name, tb2.location_storage_id, (SELECT tb3.storage_location_name FROM foodiq_location_storage as tb3 WHERE tb2.location_storage_id = tb3.id) as store_name FROM wifi_location as tb1 INNER JOIN foodiq_inventory_settings as tb2 ON(tb1.location_uid = tb2.location_uid) WHERE FIND_IN_SET( '$refiller_id', tb2.location_refiller_uid ) AND tb2.location_uid='$location_uid' AND tb2.status='1' AND tb2.is_deleted='0'");
        if($getlocationQuery->num_rows() > 0){
	    $data['resultCode'] = '200';
	    foreach($getlocationQuery->result() as $locobj){
		$dblocation_uid = $locobj->location_uid;
		$storage_id = $locobj->location_storage_id;
		$store_name = $locobj->store_name;
		$shelf_name = $locobj->location_name;
		
		if($action_options == 'move_options'){
		    /*if($dblocation_uid == $location_uid){
			$movefrom_options .= "<option value = '$storage_id' data-action='movefromstore'>".$store_name." (Store)</option>";
			$moveto_options .= "<option value = '$location_uid' data-action='movetoshelve'>".$shelf_name." (Shelf)</option>";
		    }
		    else{
			$moveto_options .= "<option value = '$location_uid' data-action='movetoshelve'>".$shelf_name." (Shelf)</option>";
		    }*/
		    
		    if($is_perishable == '0'){
			$movefrom_options .= "<option value = '$storage_id' data-action='movefromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='movefromshelve'>".$shelf_name." (Shelf)</option>";
			$moveto_options .= "<option value = '$storage_id' data-action='movetostore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='movetoshelve'>".$shelf_name." (Shelf)</option>";
		    }
		    elseif($is_perishable == '1'){
			$movefrom_options .= "<option value = '$location_uid' data-action='movefromshelve'>".$shelf_name." (Shelf)</option>";
		    }
		    
		    $where = "FIND_IN_SET('".$refiller_id."', `tb1`.`location_refiller_uid`)";
		    $this->db2->select('tb2.location_uid, tb2.location_name')->from('foodiq_inventory_settings as tb1');
		    $this->db2->join('wifi_location as tb2', 'tb2.location_uid = tb1.location_uid', 'inner');
		    $this->db2->where($where);
		    $this->db2->where(array('tb1.location_uid !=' => $location_uid, 'tb1.location_storage_id' => $storage_id));
		    $chkmultilocQ = $this->db2->get();
		    if($chkmultilocQ->num_rows() > 0){
			foreach($chkmultilocQ->result() as $multiobj){
			    $moveto_options .= "<option value = '".$multiobj->location_uid."' data-action='movetoshelve'>".$multiobj->location_name." (Shelf)</option>";
			}
		    }
		    
		    $items_rol = '0';
		    $getitem_rol = $this->db2->query("SELECT items_rol FROM foodiq_inventory_settings WHERE isp_uid='$isp_uid' AND location_uid='$location_uid' AND status='1' AND is_deleted='0'");
		    if($getitem_rol->num_rows() > 0){
			$items_rol = $getitem_rol->row()->items_rol;
		    }
		    
		    
		    $getordersfor_rolQ = $this->db2->query("SELECT COALESCE(SUM(tb2.qty),0) as orderqty FROM `foodiq_order` as tb1 RIGHT JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE DATE(tb1.added_on) >= ( CURDATE() - INTERVAL $items_rol DAY ) AND tb2.menu_id='$menu_id' ");
		    $last_orderqty = $getordersfor_rolQ->row()->orderqty;
		    
		    $recommend_move_qty = 0;
		    if($food_item_quantity < $last_orderqty){
			$recommend_move_qty = ($last_orderqty - $food_item_quantity);
			if($storeqty <= $recommend_move_qty){
			    $recommend_move_qty = $storeqty;
			}
		    }
		    
		    $data['recommend_move_qty'] = $recommend_move_qty;
		}
		elseif($action_options == 'return_options'){
		    if($dblocation_uid == $location_uid){
			if($is_perishable == '1'){
			    $returnfrom_options .= "<option value = '$location_uid' data-action='returnfromshelve'>".$shelf_name." (Shelf)</option>";
			}else{
			    $returnfrom_options .= "<option value = '$storage_id' data-action='returnfromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='returnfromshelve'>".$shelf_name." (Shelf)</option>";
			}
		    }
		    
		    if(strpos($supplier_name, ',') !== FALSE){
			$suppArr = explode(',' , $supplier_name);
			foreach($suppArr as $suppname){
			    $returnto_options .= "<option value = '$suppname' data-action='returntosupplier'>".$suppname." (Supplier)</option>";
			}
		    }else{
			$returnto_options .= "<option value = '$supplier_name' data-action='returntosupplier'>".$supplier_name." (Supplier)</option>";
		    }
		    $data['recommend_return_qty'] = '';
		    
		}
		elseif($action_options == 'trash_options'){
		    
		    if($is_perishable == '1'){
			if($dblocation_uid == $location_uid){
			    $trashfrom_options .= "<option value = '$location_uid' data-action='trashfromshelve'>".$shelf_name." (Shelf)</option>";
			}			
		    }else{
			if($dblocation_uid == $location_uid){
			    $trashfrom_options .= "<option value = '$storage_id' data-action='trashfromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='trashfromshelve'>".$shelf_name." (Shelf)</option>";
			}
		    }
		    
		    $order_qty = 0;
		    $orderQuery = $this->db2->query("SELECT COALESCE(SUM(tb2.qty),0) as itemorderqty FROM `foodiq_order` as tb1 INNER JOIN `foodiq_order_details` as tb2 ON(tb1.id=tb2.order_id) WHERE (CURDATE() >= DATE_ADD( DATE(tb1.added_on), INTERVAL ".$perishable_days." DAY )) AND tb2.menu_id='".$menu_id."'");
		    if($orderQuery->num_rows() > 0){
			$order_qty = $orderQuery->row()->itemorderqty;
		    }
		    
		    if($is_perishable == '1'){
			$invntryQ = $this->db2->query("SELECT COALESCE(SUM(quantity),0) as storeqty FROM foodiq_inventory_actions WHERE menu_id='".$menu_id."' AND action='addtoshelf' AND (CURDATE() >= DATE_ADD( DATE(added_on), INTERVAL ".$perishable_days." DAY ))");
		    }else{
			$invntryQ = $this->db2->query("SELECT COALESCE(SUM(quantity),0) as storeqty FROM foodiq_inventory_actions WHERE menu_id='".$menu_id."' AND action='addinventory' AND (CURDATE() >= DATE_ADD( DATE(added_on), INTERVAL ".$perishable_days." DAY ))");
		    }

		    if($invntryQ->num_rows() > 0){
			$laststoreqty = $invntryQ->row()->storeqty;
			
			//$data['checking_qty'] = ($perishable_days .'@@@'. $laststoreqty .'@@@'. $order_qty);
			
			if($order_qty > $laststoreqty){
			    $data['recommend_trash_qty'] = '0';
			}
			else{
			    $data['recommend_trash_qty'] = ($laststoreqty - $order_qty);
			}
		    }
		    else{
			$data['recommend_trash_qty'] = '0';
		    }
		}
		elseif($action_options == 'adjust_options'){
		    if($is_perishable == '0'){
			$adjustfrom_options .= "<option value = '$storage_id' data-action='adjustfromstore'>".$store_name." (Store)</option><option value = '$location_uid' data-action='adjustfromshelve'>".$shelf_name." (Shelf)</option>";
		    }else{
			$adjustfrom_options .= "<option value = '$location_uid' data-action='adjustfromshelve'>".$shelf_name." (Shelf)</option>";
		    }
		}
	    }
	    
	    $data['movefrom_options'] = $movefrom_options;
	    $data['moveto_options'] = $moveto_options;
	    $data['returnfrom_options'] = $returnfrom_options;
	    $data['returnto_options'] = $returnto_options;
	    $data['trashfrom_options'] = $trashfrom_options;
	    $data['adjustfrom_options'] = $adjustfrom_options;
	}
	else{
            $data['resultCode'] = '404';
            $data['resultMsg'] = 'No associated locations found.';
        }
	return $data;
    }
    public function add_inventory_item($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$menuid = $jsondata->menu_id;
	$is_perishable = $jsondata->is_perishable;
	$menuqty = $jsondata->enterqty;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	$location_uid = $jsondata->location_uid;
	
	$balance_shelf_left = '0';
	$balance_stock_left = '0';
	
	if($is_perishable == '1'){
	    $invntrytype = 'addtoshelf';
	    $finalqty = '`food_item_quantity` + '.$menuqty;
	    $this->db2->set('food_item_quantity', $finalqty , FALSE);
	    $this->db2->where('id', $menuid);
	    $this->db2->update('foodiq_menulist');
	    
	    $this->db2->select('food_item_quantity')->from('foodiq_menulist');
	    $this->db2->where('id' , $menuid);
	    $balanceshelfQ = $this->db2->get();
	    if($balanceshelfQ->num_rows() > 0){
		$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
	    }
	    
	    $this->db2->select('quantity')->from('foodiq_warehouse');
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $balancestockQ = $this->db2->get();
	    if($balancestockQ->num_rows() > 0){
		$balance_stock_left = $balancestockQ->row()->quantity;
	    }
	    
	    $this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => '', 'actionto_id' => '', 'actionfrom' => '', 'actionto' => '', 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
	    
	    $data['resultCode'] = '200';
            $data['resultMsg'] = 'Item Added to Shelf Successfully.';
	}
	elseif($is_perishable == '0'){
	    $invntrytype = 'addinventory';
	    $chkwarehouse = $this->db2->query("SELECT id FROM foodiq_warehouse WHERE menu_id='$menuid' AND location_uid='$location_uid'");
	    if($chkwarehouse->num_rows() > 0){
		$wfinalqty = '`quantity` + '.$menuqty;
		$this->db2->set('quantity', $wfinalqty , FALSE);
		$this->db2->where('menu_id', $menuid);
		$this->db2->update('foodiq_warehouse');
	    }else{
		$this->db2->insert('foodiq_warehouse', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'added_on' => date('Y-m-d H:i:s')));
	    }
	    
	    $this->db2->select('food_item_quantity')->from('foodiq_menulist');
	    $this->db2->where('id' , $menuid);
	    $balanceshelfQ = $this->db2->get();
	    if($balanceshelfQ->num_rows() > 0){
		$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
	    }
	    
	    $this->db2->select('quantity')->from('foodiq_warehouse');
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $balancestockQ = $this->db2->get();
	    if($balancestockQ->num_rows() > 0){
		$balance_stock_left = $balancestockQ->row()->quantity;
	    }
	    
	    $this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => '', 'actionto_id' => '', 'actionfrom' => '', 'actionto' => '', 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
	    
	    $data['resultCode'] = '200';
            $data['resultMsg'] = 'Item Added to Store Successfully.';
	}
	
	return $data;
    }
    public function move_inventory_item($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	$menuid = $jsondata->menu_id;
	$menuqty = $jsondata->enterqty;
	$movefrom = $jsondata->movefrom;
	$moveto = $jsondata->moveto;
	$location_uid = $jsondata->location_uid;
	$storeqty = $jsondata->storeqty;
	$invntrytype = 'moveproduct';
	$balance_stock_left = '0';
	$balance_shelf_left = '0';
	$movefrom_id = $jsondata->movefrom_id;
	$moveto_id = $jsondata->moveto_id;
	
	$shelfqty = 0;
	$checkbarcodeQ = $this->db2->query("SELECT id, food_item_quantity FROM foodiq_menulist WHERE id='$menuid'");
        if($checkbarcodeQ->num_rows() > 0){
	    $shelfqty = $checkbarcodeQ->row()->food_item_quantity;
	}
	
	if($movefrom == 'movefromstore' && $moveto == 'movetoshelve'){
	    
	    if($movefrom == 'movefromstore'){
		if($menuqty > $storeqty){
		    $data['resultCode'] = '403';
		    $data['resultMsg'] = 'Store inventory is less than the quantity entered.';
		    return $data; die;
		}
	    }
	    
	    $finalqty = '`food_item_quantity` + '.$menuqty;
	    $this->db2->set('food_item_quantity', $finalqty , FALSE);
	    $this->db2->where('id', $menuid);
	    $this->db2->update('foodiq_menulist');
	    
	    $wfinalqty = '`quantity` - '.$menuqty;
	    $this->db2->set('quantity', $wfinalqty , FALSE);
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $this->db2->update('foodiq_warehouse');
	    
	    //$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $jsondata->movefrom_id, 'actionto_id' => $jsondata->moveto_id, 'actionfrom' => $movefrom, 'actionto' => $moveto, 'action' => $invntrytype, 'added_on' => date('Y-m-d H:i:s')));
	}
	elseif($movefrom == 'movefromshelve' && $moveto == 'movetostore'){
	    
	    if($movefrom == 'movefromshelve'){
		if($menuqty > $shelfqty){
		    $data['resultCode'] = '403';
		    $data['resultMsg'] = 'Shelf inventory is less than the quantity entered.';
		    return $data; die;
		}
	    }
	    
	    $finalqty = '`food_item_quantity` - '.$menuqty;
	    $this->db2->set('food_item_quantity', $finalqty , FALSE);
	    $this->db2->where('id', $menuid);
	    $this->db2->update('foodiq_menulist');
	    
	    $wfinalqty = '`quantity` + '.$menuqty;
	    $this->db2->set('quantity', $wfinalqty , FALSE);
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $this->db2->update('foodiq_warehouse');
	    
	    //$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $jsondata->movefrom_id, 'actionto_id' => $jsondata->moveto_id, 'actionfrom' => $movefrom, 'actionto' => $moveto, 'action' => $invntrytype, 'added_on' => date('Y-m-d H:i:s')));
	}
	elseif($movefrom == 'movefromshelve' && $moveto == 'movetoshelve'){
	    
	    if($movefrom == 'movefromshelve'){
		if($menuqty > $shelfqty){
		    $data['resultCode'] = '403';
		    $data['resultMsg'] = 'Shelf inventory is less than the quantity entered.';
		    return $data; die;
		}
	    }
	    
	    $finalqty = '`food_item_quantity` - '.$menuqty;
	    $this->db2->set('food_item_quantity', $finalqty , FALSE);
	    $this->db2->where(array('id' => $menuid, 'location_uid' => $location_uid));
	    $this->db2->update('foodiq_menulist');
	    
	    $this->db2->select('t1.id, t1.location_uid')->from('foodiq_menulist as t1');
	    $this->db2->join('foodiq_master_menulist as t2', 't1.master_menu_id=t2.id', 'inner');
	    $this->db2->where('t1.location_uid', $jsondata->moveto_id);
	    $this->db2->group_start();
	    $this->db2->where('t2.food_item_barcode', $jsondata->barcode);
	    $this->db2->or_where('t2.food_carret_barcode', $jsondata->barcode);
	    $this->db2->group_end();
	    $secondshelfQ = $this->db2->get();
	    if($secondshelfQ->num_rows() > 0){
		$srawdata = $secondshelfQ->row();
		$smenuid = $srawdata->id;
		$slocation_uid = $srawdata->location_uid;
		
		$finalqty = '`food_item_quantity` + '.$menuqty;
		$this->db2->set('food_item_quantity', $finalqty , FALSE);
		$this->db2->where(array('id' => $smenuid));
		$this->db2->update('foodiq_menulist');
	    
		$sbalance_stock_left = '0';
		$sbalance_shelf_left = '0';
	    
		$this->db2->select('food_item_quantity')->from('foodiq_menulist');
		$this->db2->where('id' , $smenuid);
		$sbalanceshelfQ = $this->db2->get();
		if($sbalanceshelfQ->num_rows() > 0){
		    $sbalance_shelf_left = $sbalanceshelfQ->row()->food_item_quantity;
		}
		
		$this->db2->select('quantity')->from('foodiq_warehouse');
		$this->db2->where(array('menu_id' => $smenuid, 'location_uid' => $slocation_uid));
		$sbalancestockQ = $this->db2->get();
		if($sbalancestockQ->num_rows() > 0){
		    $sbalance_stock_left = $sbalancestockQ->row()->quantity;
		}
		
		$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $slocation_uid, 'refiller_id' => $refiller_id, 'menu_id' => $smenuid, 'quantity' => $menuqty, 'actionfrom_id' => $jsondata->movefrom_id, 'actionto_id' => $jsondata->moveto_id, 'actionfrom' => $movefrom, 'actionto' => $moveto, 'action' => $invntrytype , 'balance_stock' => $sbalance_stock_left, 'balance_shelf' => $sbalance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
	    }
	}
	
	
	//Calculating Balance Qty
	$this->db2->select('food_item_quantity')->from('foodiq_menulist');
	$this->db2->where('id' , $menuid);
	$balanceshelfQ = $this->db2->get();
	if($balanceshelfQ->num_rows() > 0){
	    $balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
	}
	
	$this->db2->select('quantity')->from('foodiq_warehouse');
	$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	$balancestockQ = $this->db2->get();
	if($balancestockQ->num_rows() > 0){
	    $balance_stock_left = $balancestockQ->row()->quantity;
	}
	
	$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $movefrom_id, 'actionto_id' => $moveto_id, 'actionfrom' => $movefrom, 'actionto' => $moveto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
	
	$data['resultCode'] = '200';
	$data['resultMsg'] = 'Item get moved successfully.';
	return $data;
	
    }
    public function return_inventory_item($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	$menuid = $jsondata->menu_id;
	$menuqty = $jsondata->enterqty;
	$returnfrom = $jsondata->returnfrom;
	$returnto = $jsondata->returnto;
	$location_uid = $jsondata->location_uid;
	$invntrytype = 'returnproduct';
	$balance_stock_left = '0';
	$balance_shelf_left = '0';
	
	if($returnfrom == 'returnfromstore' && $returnto == 'returntosupplier'){
		$finalqty = '`quantity` - '.$menuqty;
		$this->db2->set('quantity', $finalqty , FALSE);
		$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
		$this->db2->update('foodiq_warehouse');
		
		$this->db2->select('food_item_quantity')->from('foodiq_menulist');
		$this->db2->where('id' , $menuid);
		$balanceshelfQ = $this->db2->get();
		if($balanceshelfQ->num_rows() > 0){
		    $balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
		}
		
		$this->db2->select('quantity')->from('foodiq_warehouse');
		$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
		$balancestockQ = $this->db2->get();
		if($balancestockQ->num_rows() > 0){
		    $balance_stock_left = $balancestockQ->row()->quantity;
		}
		
		$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $jsondata->returnfrom_id, 'actionto_id' => $jsondata->returnto_id, 'actionfrom' => $returnfrom, 'actionto' => $returnto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
	}
	elseif($returnfrom == 'returnfromshelve' && $returnto == 'returntosupplier'){
		$finalqty = '`food_item_quantity` - '.$menuqty;
		$this->db2->set('food_item_quantity', $finalqty , FALSE);
		$this->db2->where('id', $menuid);
		$this->db2->update('foodiq_menulist');
		
		$this->db2->select('food_item_quantity')->from('foodiq_menulist');
		$this->db2->where('id' , $menuid);
		$balanceshelfQ = $this->db2->get();
		if($balanceshelfQ->num_rows() > 0){
		    $balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
		}
		
		$this->db2->select('quantity')->from('foodiq_warehouse');
		$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
		$balancestockQ = $this->db2->get();
		if($balancestockQ->num_rows() > 0){
		    $balance_stock_left = $balancestockQ->row()->quantity;
		}
		
		$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $jsondata->returnfrom_id, 'actionto_id' => $jsondata->returnto_id, 'actionfrom' => $returnfrom, 'actionto' => $returnto, 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
	}
	
	$data['resultCode'] = '200';
	$data['resultMsg'] = 'Item get return successfully.';
	return $data;
    }
    public function trash_inventory_item($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	$menuid = $jsondata->menu_id;
	$menuqty = $jsondata->enterqty;
	$location_uid = $jsondata->location_uid;
	$invntrytype = 'trashproduct';
	$trashfrom = $jsondata->trashfrom;
	$trashfrom_id = $jsondata->trashfrom_id;
	$balance_stock_left = '0';
	$balance_shelf_left = '0';
	
	if($trashfrom == 'trashfromshelve'){
	    $finalqty = '`food_item_quantity` - '.$menuqty;
	    $this->db2->set('food_item_quantity', $finalqty , FALSE);
	    $this->db2->where('id', $menuid);
	    $this->db2->update('foodiq_menulist');
	    
	    $this->db2->select('food_item_quantity')->from('foodiq_menulist');
	    $this->db2->where('id' , $menuid);
	    $balanceshelfQ = $this->db2->get();
	    if($balanceshelfQ->num_rows() > 0){
		$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
	    }
	    
	    $this->db2->select('quantity')->from('foodiq_warehouse');
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $balancestockQ = $this->db2->get();
	    if($balancestockQ->num_rows() > 0){
		$balance_stock_left = $balancestockQ->row()->quantity;
	    }
	}
	elseif($trashfrom == 'trashfromstore'){
	    $wfinalqty = '`quantity` - '.$menuqty;
	    $this->db2->set('quantity', $wfinalqty , FALSE);
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $this->db2->update('foodiq_warehouse');
	    
	    $this->db2->select('food_item_quantity')->from('foodiq_menulist');
	    $this->db2->where('id' , $menuid);
	    $balanceshelfQ = $this->db2->get();
	    if($balanceshelfQ->num_rows() > 0){
		$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
	    }
	    
	    $this->db2->select('quantity')->from('foodiq_warehouse');
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $balancestockQ = $this->db2->get();
	    if($balancestockQ->num_rows() > 0){
		$balance_stock_left = $balancestockQ->row()->quantity;
	    }
	}
	
	$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $jsondata->trashfrom_id, 'actionto_id' => '', 'actionfrom' => $trashfrom, 'actionto' => '', 'action' => $invntrytype, 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
	
	$data['resultCode'] = '200';
	$data['resultMsg'] = 'Item get trashed successfully.';
	return $data;
    }
    public function suggested_items($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_uid = $jsondata->refiller_uid;
	$refiller_id = $jsondata->refiller_id;
	$location_uid = $jsondata->location_uid;
	
	$trashitems = $this->check_trash_items($location_uid);
	$moveitems = $this->check_moveable_items($location_uid);
	
	$suggestQ = $this->db2->query("SELECT menu_id, quantity, action FROM foodiq_inventory_automated WHERE location_uid='".$location_uid."'");
	if($suggestQ->num_rows() > 0){
	    $i = 0; $j = 0;
	    foreach($suggestQ->result() as $sobj){
		$menu_id = $sobj->menu_id;
		$menu_name = ''; $item_barcode = ''; $is_perishable = '0';
		
		$checkproductQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity, tb2.supplier_name, tb2.is_perishable, tb2.perishable_days, tb2.product_titlename, tb2.food_item_barcode FROM foodiq_menulist as tb1 INNER JOIN foodiq_master_menulist as tb2 ON(tb1.master_menu_id = tb2.id) WHERE tb1.id = '".$menu_id."' ");
		if($checkproductQ->num_rows() > 0){
		    $itemrawdata = $checkproductQ->row();
		    $menu_name = $itemrawdata->product_titlename;
		    $item_barcode = $itemrawdata->food_item_barcode;
		    $is_perishable = $itemrawdata->is_perishable;
		}
		if($sobj->action == 'moveitems'){
		    $data['moveitems_list'][$i]['menu_id'] = $sobj->menu_id;
		    $data['moveitems_list'][$i]['menu_name'] = $menu_name;
		    $data['moveitems_list'][$i]['item_barcode'] = $item_barcode;
		    $data['moveitems_list'][$i]['is_perishable'] = $is_perishable;
		    $data['moveitems_list'][$i]['menu_qty'] = $sobj->quantity;
		    $i++;
		}
		else{
		    $data['trashitems_list'][$j]['menu_id'] = $sobj->menu_id;
		    $data['trashitems_list'][$j]['menu_name'] = $menu_name;
		    $data['trashitems_list'][$j]['item_barcode'] = $item_barcode;
		    $data['trashitems_list'][$j]['is_perishable'] = $is_perishable;
		    $data['trashitems_list'][$j]['menu_qty'] = $sobj->quantity;
		    $j++;
		}
	    }
	    
	    $data['moveitems'] = $i;
	    $data['trashitems'] = $j;
	}
	else{
	    $data['moveitems'] = '0';
	    $data['trashitems'] = '0';
	    $data['moveitems_list'] = 'No items found to move.';
	    $data['trashitems_list'] = 'No items found to trash.';
	}
	
	return $data;
	
    }
    
    public function send_items_report($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$refiller_id = $jsondata->refiller_id;
	$location_uid = $jsondata->location_uid;
	$item_barcode = $jsondata->item_barcode;
	$report_message = $jsondata->report_message;
	
	$this->db2->insert('foodiq_feedback_logs', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'item_barcode' => $item_barcode, 'message' => $report_message, 'added_on' => date('Y-m-d H:i:s')));
	
	$data['resultCode'] = '200';
	$data['resultMsg'] = 'Feedback submitted successfully.';
	
	return $data;
	
    }
    public function stock_adjustment($jsondata){
	$data = array();
	$isp_uid = $jsondata->isp_uid;
	$menuqty = $jsondata->enterqty;
	$refiller_id = isset($jsondata->refiller_id) ? $jsondata->refiller_id : '';
	$location_uid = $jsondata->location_uid;
	$menuid = $jsondata->menu_id;
	$adjustfrom = isset($jsondata->adjustfrom) ? $jsondata->adjustfrom : 'adjustfromshelve';
	$adjustfrom_id = isset($jsondata->adjustfrom_id) ? $jsondata->adjustfrom_id : $location_uid;
	$balance_stock_left = '0';
	$balance_shelf_left = '0';
	
	if($adjustfrom == 'adjustfromstore'){
	    $this->db2->select('id')->from('foodiq_warehouse');
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $checkstoreitemQ = $this->db2->get();
	    if($checkstoreitemQ->num_rows() > 0){ 
		$wfinalqty = '`quantity` + '.$menuqty;
		$this->db2->set('quantity', $wfinalqty , FALSE);
		$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
		$this->db2->update('foodiq_warehouse');
	    }
	    else{
		$this->db2->insert('foodiq_warehouse', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'menu_id' => $menuid, 'quantity' => $menuqty, 'added_on' => date('Y-m-d H:i:s')));
	    }
	    
	    $this->db2->select('food_item_quantity')->from('foodiq_menulist');
	    $this->db2->where('id' , $menuid);
	    $balanceshelfQ = $this->db2->get();
	    if($balanceshelfQ->num_rows() > 0){
		$balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
	    }
	    
	    $this->db2->select('quantity')->from('foodiq_warehouse');
	    $this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
	    $balancestockQ = $this->db2->get();
	    if($balancestockQ->num_rows() > 0){
		$balance_stock_left = $balancestockQ->row()->quantity;
	    }
	    
	    $this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $adjustfrom_id, 'actionto_id' => '', 'actionfrom' => $adjustfrom, 'actionto' => '', 'action' => 'adjustinventory', 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left , 'added_on' => date('Y-m-d H:i:s')));
	    
	    $data['resultCode'] = '200';
	    $data['resultMsg'] = 'Store Inventory Adjusted Successfully.';
	}
	elseif($adjustfrom == 'adjustfromshelve'){
	    $this->db2->select('food_item_quantity')->from('foodiq_menulist');
	    $this->db2->where('id', $menuid);
	    $location_menuQ = $this->db2->get();
	    if($location_menuQ->num_rows() > 0){
		$finalqty = '`food_item_quantity` + '.$menuqty;
		$this->db2->set('food_item_quantity', $finalqty , FALSE);
		$this->db2->where('id', $menuid);
		$this->db2->update('foodiq_menulist');
		
		$this->db2->select('food_item_quantity')->from('foodiq_menulist');
		$this->db2->where('id' , $menuid);
		$balanceshelfQ = $this->db2->get();
		if($balanceshelfQ->num_rows() > 0){
		    $balance_shelf_left = $balanceshelfQ->row()->food_item_quantity;
		}
		
		$this->db2->select('quantity')->from('foodiq_warehouse');
		$this->db2->where(array('menu_id' => $menuid, 'location_uid' => $location_uid));
		$balancestockQ = $this->db2->get();
		if($balancestockQ->num_rows() > 0){
		    $balance_stock_left = $balancestockQ->row()->quantity;
		}
		
		$this->db2->insert('foodiq_inventory_actions', array('isp_uid' => $isp_uid, 'location_uid' => $location_uid, 'refiller_id' => $refiller_id, 'menu_id' => $menuid, 'quantity' => $menuqty, 'actionfrom_id' => $adjustfrom_id, 'actionto_id' => '', 'actionfrom' => $adjustfrom, 'actionto' => '', 'action' => 'adjustinventory', 'balance_stock' => $balance_stock_left, 'balance_shelf' => $balance_shelf_left, 'added_on' => date('Y-m-d H:i:s')));
		
		$data['resultCode'] = '200';
		$data['resultMsg'] = 'Shelf Inventory Adjusted Successfully.';
	    }
	}
	else{
	    $data['resultCode'] = '404';
	    $data['resultMsg'] = 'Invalid Adjustment.';
	}
	return $data;
	
    }
    /***********************************************************************************
    *		CRON APIS BEGINS
    **********************************************************************************/
    public function openingstock_cronjob(){
	$isp_uid = '223';
	
	$getlocationQuery = $this->db2->query("SELECT id, location_uid FROM wifi_location WHERE isp_uid='".$isp_uid."' AND status='1' AND is_deleted='0' AND location_type='15'");
        if($getlocationQuery->num_rows() > 0){
	    foreach($getlocationQuery->result() as $locobj){
		$location_uid = $locobj->location_uid;
		$menulistQ = $this->db2->query("SELECT tb1.id, tb1.food_item_quantity FROM foodiq_menulist as tb1 WHERE tb1.isp_uid='".$isp_uid."' AND tb1.location_uid='".$location_uid."' AND tb1.status='1'");
		if($menulistQ->num_rows() > 0){
		    foreach($menulistQ->result() as $mobj){
			$menu_id = $mobj->id;
			
			$checkfortodayslotQ = $this->db2->query("SELECT id FROM foodiq_inventory_openingstock WHERE isp_uid='$isp_uid' AND location_uid='$location_uid' AND menu_id='$menu_id' AND DATE(added_on) = '".date('Y-m-d')."'");
			if($checkfortodayslotQ->num_rows() == 0){
			    $openingshelfstock = $mobj->food_item_quantity;
			    $opstockArr = array(
				'isp_uid' => $isp_uid,
				'location_uid' => $location_uid,
				'menu_id' => $menu_id,
				'openingstock' => $openingshelfstock,
				'stocktype' => 'shelf',
				'added_on' => date('Y-m-d H:i:s')
			    );
			    
			    $this->db2->insert('foodiq_inventory_openingstock', $opstockArr);
			    
			    sleep(1);
			    $this->db2->select('quantity')->from('foodiq_warehouse');
			    $this->db2->where(array('menu_id' => $menu_id, 'location_uid' => $location_uid));
			    $balancestockQ = $this->db2->get();
			    if($balancestockQ->num_rows() > 0){
				$openingstock = $balancestockQ->row()->quantity;		
				$opstockArr = array(
				    'isp_uid' => $isp_uid,
				    'location_uid' => $location_uid,
				    'menu_id' => $menu_id,
				    'openingstock' => $openingstock,
				    'stocktype' => 'storage',
				    'added_on' => date('Y-m-d H:i:s')
				);
				
				$this->db2->insert('foodiq_inventory_openingstock', $opstockArr);
			    }
			}
		    }
		}
	    }
	}
    }

    public function send_single_user_notification($jsondata){
	$data = array();
	
	$usernotify = array(
	    'isp_uid' => $jsondata->isp_uid,
	    'username' => $jsondata->mobile,
	    'client_id' => $jsondata->token_id,
	    'added_on' => date('Y-m-d H:i:s')
	);
	
	$this->db2->select('id')->from('foodiq_firebase_notification');
	$this->db2->where('username', $jsondata->mobile);
	$checkuserQ = $this->db2->get();
	if($checkuserQ->num_rows() == 0){
	    $this->db2->insert('foodiq_firebase_notification', $usernotify);
	}
	else{
	    $this->db2->update('foodiq_firebase_notification', $usernotify, array('username' => $jsondata->mobile));
	}
	
	$client_id = $jsondata->token_id;
	$title = 'NEW OFFERS!';
	$message = 'Redeem exciting SMARTCASH Vouchers and order food FREE!';
	$firebaseurl = 'https://fcm.googleapis.com/fcm/send';
	$fields = array();
	if(is_array($client_id)) {
	    $fields['registration_ids'] = $client_id;
	} else {
	    $fields['to'] = $client_id;
	}
	$fields['data'] = array ( "message" => $message, "title" => $title, 'vibrate'   => 1, 'largeIcon' => 'large_icon', 'smallIcon' => 'small_icon');
	$fields['notification'] = array( "title" => $title, "body"  => $message, "text"  => "Click me to open an Activity!", "sound" => "default" );
	
	$fields = json_encode ( $fields );
	//print_r($fields); die;
    
	$headers = array (
	    'Authorization: key=' . "AAAAf5Y_mEg:APA91bHoHBk8d2VkH0BrEa3fn--BvuWuTxKdF038b30zo0ZIreCj-irIIq7BeQFxvoUp6CR-UgNst3zrszqvUKtPcaTox8hc2uaNw_OGWV-ZdK_YQHxQZf-gl0rzuhCxrz3I3BjP6ORh",
	    'Content-Type: application/json'
	);
    
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, $firebaseurl );
	curl_setopt ( $ch, CURLOPT_POST, true );
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	$result = curl_exec ( $ch );
	curl_close ( $ch );
	
	return $result;
    }
    
    public function send_all_users_notification(){
	$data = array();
	
	$usernotify = array(
	    'isp_uid' => $jsondata->isp_uid,
	    'username' => $jsondata->mobile,
	    'client_id' => $jsondata->token_id,
	    'added_on' => date('Y-m-d H:i:s')
	);
	
	$this->db2->select('id, client_id')->from('foodiq_firebase_notification');
	$checkuserQ = $this->db2->get();
	if($checkuserQ->num_rows() > 0){
	    foreach($checkuserQ->result() as $chkuobj){
		
		$client_id = $chkuobj->client_id;
		$title = 'NEW OFFERS!';
		$message = 'Redeem exciting SMARTCASH Vouchers and order food FREE!';
		$firebaseurl = 'https://fcm.googleapis.com/fcm/send';
		$fields = array();
		if(is_array($client_id)) {
		    $fields['registration_ids'] = $client_id;
		} else {
		    $fields['to'] = $client_id;
		}
		$fields['data'] = array ( "message" => $message, "title" => $title, 'vibrate'   => 1, 'largeIcon' => 'large_icon', 'smallIcon' => 'small_icon');
		$fields['notification'] = array( "title" => $title, "body"  => $message, "text"  => "Click me to open an Activity!", "sound" => "default" );
		
		$fields = json_encode ( $fields );
		//print_r($fields); die;
	    
		$headers = array (
		    'Authorization: key=' . "AAAAf5Y_mEg:APA91bHoHBk8d2VkH0BrEa3fn--BvuWuTxKdF038b30zo0ZIreCj-irIIq7BeQFxvoUp6CR-UgNst3zrszqvUKtPcaTox8hc2uaNw_OGWV-ZdK_YQHxQZf-gl0rzuhCxrz3I3BjP6ORh",
		    'Content-Type: application/json'
		);
	    
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $firebaseurl );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		
		return $result;
	    }
	}
    }
}

?>