<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('asia/kolkata');
error_reporting(0);
class CokeportalApi extends CI_Controller {
    public function __construct(){
        parent::__construct();
	$this->load->library('My_PHPMailer');
        $this->load->library('mongo_db');
        $this->load->model('cokeportal_model');
		//$this->load->model('cokeportal_modeltest');
    }
    public function index(){
	//echo $_SERVER['DOCUMENT_ROOT'];
        //$this->load->view('welcome_message');
echo "WELCOME";
    }
    private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	public function log($function,$request, $responce){
		$data = $this->cokeportal_model->log($function, $request, $responce);
	}
        
 		
	public function cp2_offers_totalmb(){
		/*$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352",
		"apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","via":"web","macid":"",
		"platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->cokeportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->cokeportal_model->cp2_offers_totalmb($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid(){
/*$_POST['requestData'] = ' {"userId":"57bc131bab121c770f981352", "apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf", "locationId":"9",
"via":"web","macid":"7C:D3:0A:06:22:95","platform_filter":"Android Phone","offer_type":"coke_video_add"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->cokeportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->cokeportal_model->captive_ad_data_grid($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data_grid',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function cp_offer_sceen_user()
	{
		  // $_POST['requestData'] = ' {"userid":"58208dffca94f1fd680d7b4f","cpofferid":"6"}';
	//$_POST['requestData'] = ' {"userid":"57bc0292ab121c3d0b98134d","cpofferid":"110","locationid":"25","via":"web","platform_filter":"Android Phone","macid":"64:BC:0C:7C:F7:75","offertype":"coke_video_add","mbcost":"500"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{

			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->cp_offer_sceen_user($decodeData);

		}
		$this->log('cp_offer_sceen_user',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}

	public function captive_portal_impression()
	{
		//$_POST['requestData'] = '{"locationId" : "9", "macid" : "3c:4d", "platform_filter" : "Android Phone"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->captive_portal_impression($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('captive_portal_impression',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function captive_ad_data_grid_detail(){
		//$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352","offer_id":"93", "offer_type":"coke_contest_add","locationId" : "9"}';

//echo "<pre>"; print_R($_POST); die;
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->captive_ad_data_grid_detail($decodeData);

		}
		$this->log('captive_ad_data_grid_detail',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function captive_ad_survey_data()
	{
		//$_POST['requestData'] = '{"userId":"9560588376","locationId":"140","via":"web","macid":"","platform_filter":"Android Phone","offer_type":"coke_survey_add"}';

//echo "<pre>"; print_R($_POST); die;
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->captive_ad_survey_data($decodeData);

		}
		$this->log('captive_ad_survey_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function get_mb_data()
	{
		//$_POST['requestData'] = '{"amount":"15"}';
		$decodeData = json_decode($_POST['requestData']);
		$data = $this->cokeportal_model->get_mb_data($decodeData);
		$this->_sendResponse(json_encode($data));
	}
	

	public function activate_voucher()
		{
			
         //     $_POST['requestData'] = '{"userId":"57bc131bab121c770f981352","voucher":"483094","locationId":"9"}';
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->activate_voucher($decodeData);
			
		}
		$this->log('activate_voucher',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
			
		}
		
			public function voucher_availed()
		{
			/* $_POST['requestData'] = ' {"userId":"57bc131bab121c770f981352", "apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf", "locationId":"1",
			"via":"web","macid":"7C:D3:0A:06:22:95","platform_filter":"Android Phone","voucher":"12345678"}';	*/
			 $data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->voucher_availed($decodeData);
				
			}
			$this->log('voucher_availed',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
		}
		
		
		
	public function insert_user_option()
	{
/*$_POST['requestData'] = '{
"userId":"57bc131bab121c770f981352","locationId":"12","offer_id":"87","via":"web","platform_filter":"Android Phone",
 "items": [{"question_id"
:"58","option_id":"292"},{"question_id":"58","option_id":"293"},{"question_id":"59","option_id":"294"}]}';*/
//$_POST['requestData']=' {"userId":"57bc131bab121c770f981352","apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","via":"web","macid":"","platform_filter":"Android Phone","offer_id":"107","items":[{\"question_id\":\"75\",\"option_id\":\"363\"},{\"question_id\":\"75\",\"option_id\":\"369\"},{\"question_id\":\"75\",\"option_id\":\"373\"},{\"question_id\":\"75\",\"option_id\":\"375\"}]}';


$data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->insert_user_option($decodeData);
				
			}
			$this->log('insert_user_option',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
 
	}	
	
	public function coke_products_mbdata()
	{
		//$_POST['requestData'] = '{"locationId":"25"}';
		$data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->coke_products_mbdata($decodeData);
				
			}
			$this->log('coke_products_mbdata',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
 
	}	
	
	//Apis for web
	
		public function cp2_offers_totalmb_web(){
		/*$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352",
		"apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","via":"web","macid":"",
		"platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->cokeportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->cokeportal_model->cp2_offers_totalmb_web($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb_web',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid_web(){
/*$_POST['requestData'] = ' {"userId":"57bc131bab121c770f981352", "apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf", "locationId":"9",
"via":"web","macid":"7C:D3:0A:06:22:95","platform_filter":"Android Phone","offer_type":"coke_video_add"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->cokeportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->cokeportal_model->captive_ad_data_grid_web($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data_grid_web',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function cp_offer_sceen_user_web()
	{
		  // $_POST['requestData'] = ' {"userid":"58208dffca94f1fd680d7b4f","cpofferid":"6"}';
	//$_POST['requestData'] = '{"userid":"57cae91bab121ccd18fdf050","cpofferid":"56","locationid":"9999","via":"web","platform_filter":"Windows Desktop","macid":"64:BC:0C:7C:F7:75","offertype":"coke_survey_add","mbcost":"500"}';
	//echo "<pre>"; print_R($_POST); die;
	
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{

			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->cp_offer_sceen_user_web($decodeData);

		}
		$this->log('cp_offer_sceen_user_web',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}

	
	
	public function captive_ad_data_grid_detail_web(){
		//$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352","offer_id":"93", "offer_type":"coke_contest_add","locationId" : "9"}';

//echo "<pre>"; print_R($_POST); die;
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->captive_ad_data_grid_detail_web($decodeData);

		}
		$this->log('captive_ad_data_grid_detail_web',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function captive_ad_survey_data_web()
	{
		//$_POST['requestData'] = '  {"userId":"57bc131bab121c770f981352","apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9999","via":"web","macid":"","platform_filter":"Windows Desktop","offer_type":"coke_survey_add"}';

//echo "<pre>"; print_R($_POST); die;
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->captive_ad_survey_data_web($decodeData);

		}
		$this->log('captive_ad_survey_data_web',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function get_mb_data_web()
	{
		//$_POST['requestData'] = '{"amount":"15"}';
		$decodeData = json_decode($_POST['requestData']);
		$data = $this->cokeportal_model->get_mb_data($decodeData);
		$this->_sendResponse(json_encode($data));
	}
	

	public function activate_voucher_web()
		{
			
         //     $_POST['requestData'] = '{"userId":"57bc131bab121c770f981352","voucher":"483094","locationId":"9"}';
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->cokeportal_model->activate_voucher_web($decodeData);
			
		}
		$this->log('activate_voucher_web',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
			
		}
		
			public function voucher_availed_web()
		{
			/* $_POST['requestData'] = ' {"userId":"57bc131bab121c770f981352", "apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf", "locationId":"1",
			"via":"web","macid":"7C:D3:0A:06:22:95","platform_filter":"Android Phone","voucher":"12345678"}';	*/
			 $data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->voucher_availed_web($decodeData);
				
			}
			$this->log('voucher_availed_web',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
		}
		
		
		
	public function insert_user_option_web()
	{
/*$_POST['requestData'] = '{
"userId":"57bc131bab121c770f981352","locationId":"12","offer_id":"87","via":"web","platform_filter":"Android Phone",
 "items": [{"question_id"
:"58","option_id":"292"},{"question_id":"58","option_id":"293"},{"question_id":"59","option_id":"294"}]}';*/
//$_POST['requestData']='  {"userId":"57c01afeb047402026801312","apikey":"14722076146df10c35054d2ea5f00924d536385817","locationId":"9999","via":"web","macid":"C0:EE:FB:70:0E:B3","platform_filter":"Android Phone","offer_id":"57","items":[{"option_id":"36"},{"option_id":"42"},{"option_id":"45"},{"option_id":"49"},{"option_id":"54"},{"option_id":"57"}]}';


$data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->insert_user_option_web($decodeData);
				
			}
			$this->log('insert_user_option_web',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
 
	}	
	
	public function coke_products_mbdata_web()
	{
		//$_POST['requestData'] = '{"locationId":"9999"}';
		$data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->coke_products_mbdata_web($decodeData);
				
			}
			$this->log('coke_products_mbdata_web',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
 
	}

public function coke_user_web()
		{
			/*$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352",
  "apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","via":"web","macid":"",
  "platform_filter":"Android Phone"}';*/
			 $data = array();
			if(count($_POST) == 0){
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'please send valid request';
			}else{
				$decodeData = json_decode($_POST['requestData']);
				$data = $this->cokeportal_model->coke_user_web($decodeData);
				
			}
			$this->log('coke_user_web',$_POST['requestData'], $data);//call log function
			$this->_sendResponse(json_encode($data));
		}	
	
	
	
	
	
	
}

?>
