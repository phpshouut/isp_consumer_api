<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
//date_default_timezone_set('asia/kolkata');
error_reporting(0);
class BeaconApi extends CI_Controller {
    public function __construct(){
	header("Access-Control-Allow-Origin: *");
        parent::__construct();
	//$this->load->library('My_PHPMailer');
        $this->load->library('mongo_db');
        $this->load->model('beaconApi_model');
    }
    public function index(){
	//echo $_SERVER['DOCUMENT_ROOT'];
        //$this->load->view('welcome_message');

    }
    
    public function onehop_traffic_analytics(){
	$json = file_get_contents("php://input");
	//$this->log('onehop_traffic_analytics','', $json);//call log function
	
	$data = array();
	if(count($json) == 0){
		$data['resultCode'] = '0';
		$data['resultMsg'] = 'please send valid request';
	}else{
		
		//$decodeData = json_decode($_POST['requestData']);
		$decodeData = json_decode($json, true);
		$data = $this->beaconApi_model->onehop_traffic_analytics($decodeData);
	}
	//$this->log('onehop_traffic_analytics',$json, $data);//call log function
	//$this->_sendResponse(json_encode($data));
    }

    public function extract_datalogs(){
	//echo $this->insights_model->decrypt_data('1a1f793ad6771f98cdf40fa0d0f8ad2c55b6ca59:a24ed186d5f836bf:jVQvw/hCWwnVAwXc9xZT28htGb82BTDaO/ugPHmYebc=');
	//echo '<br/>';
	//echo $this->insights_model->decrypt_data('1a1f793ad6771f98cdf40fa0d0f8ad2c55b6ca59:a52367c2f04252ad:nVzHqi6G10FJoT7NFyYe6foDvlX39sZn23Z8iuTv3Ak=');

	$this->beaconApi_model->extract_datalogs();
    }
    
    public function get_and_save_mac_to_device_info(){
	$this->beaconApi_model->get_and_save_mac_to_device_info();
    }
}

?>
