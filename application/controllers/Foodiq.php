<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Foodiq extends CI_Controller {
    private $authkey = 'c2hvdXV0OmZvb2RJUQ==';
    
    public function __construct(){
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        $this->load->model('Foodiq_model', 'foodiq_model');
	$this->load->library('encrypt');
    }
    public function twilio_sendsms(){
        $mobileno = '6477871167'; $country_code='1';
        $baseUrl = "https://api.authy.com/protected/json/phones/verification/";
        
        // TO SEND OTP
        $headers = array(
	    "Content-Type: application/json",
	    "Accept: application/json"
	);
	$jsonVal = (object) array();
	$curl = curl_init($baseUrl.'start?api_key=FLx3j4GVfaUUYBBOHUKnWayu314JkQ3C&via=sms&phone_number='.$mobileno.'&country_code='.$country_code);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($jsonVal));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
	//curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $curl_response = curl_exec($curl);
        
	echo $mobileno . '<br/>';
	var_dump($curl_response);
    }
    public function call_ajax(){
        $this->load->view('foodiq_view');
    }

    public function country_phonecode(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->country_phonecode($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function authenticate_user(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile", "password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->authenticate_user($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function check_user_existence(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->check_user_existence($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function add_edit_foodiq_user(){
        //$_POST['requestData'] = '{ "mobile" : "7503945244", "firstname" : "Vikas", "lastname" : "Gupta", "email" : "vikas@shouut.com", "company" : "Shouut", "company_id" : "EMP001", "company_address" : "chirag delhi", "food_type" : "all types", "food_allergy" : "No", "isp_uid" : "101"} ';
        
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile", "firstname", "lastname", "email", "company", "company_id", "company_address", "dob", "food_type", "food_allergy", "isp_uid", "is_update");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->add_edit_foodiq_user($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function send_otp(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile", "isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->send_otp($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function get_location_list(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "user_latitude", "user_longitude");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->get_location_list($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function get_location_items(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "mobile", "location_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->get_location_items($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    
    public function list_cart_items(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile", "country_code");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->list_cart_items($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function verify_otp(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile", "isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->verify_otp($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    

    public function scanbarcode_itemdetails(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "mobile", "location_uid", "item_barcode");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->scanbarcode_itemdetails($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function addtocart(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "mobile", "location_uid", "item_barcode");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->addtocart($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function currency_symbol(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->currency_symbol($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function add_order_for_payment(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile", "total_amount");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->add_order_for_payment($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function delete_cart_item(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "cart_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->delete_cart_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function check_wallet_amount(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->check_wallet_amount($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function change_menuqty(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "cart_id", "action");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->change_menuqty($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function search_item(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "search_term");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->search_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function addtowallet(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "token_id", "card_id", "save_card", "mobile", "amount", "currency", "payment_type");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->addtowallet($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function payrolldebit(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile", "amount", "currency");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->payrolldebit($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function past_orders_list(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->past_orders_list($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function past_orders_details(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("order_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->past_orders_details($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function empty_cart_items(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->empty_cart_items($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function dietary_preferences(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->dietary_preferences($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function allergy_preferences(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->allergy_preferences($decodeData);
                }
            }
        }
        echo json_encode($data);
    }

    public function list_saved_cards(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->list_saved_cards($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function foodiq_wallet_limit(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->foodiq_wallet_limit($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function foodiq_wallet_slabs(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->foodiq_wallet_slabs($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function delete_saved_card(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile", "card_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->delete_saved_card($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function payroll_request(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->payroll_request($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function foodiq_policies(){
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "policytype");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->foodiq_policies($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function foodiq_credentials(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->foodiq_credentials($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function importusers(){
	//$this->foodiq_model->importusers();
    }

    public function update_forgot_password(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "mobile", "newpassword", "pagetype");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->update_forgot_password($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function encrypt_bankdetails(){
	$this->foodiq_model->encrypt_bankdetails();
    }
    /***********************************************************************************
     *		TABLET APIS BEGINS
     **********************************************************************************/
    
    public function check_location_existence(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("location_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->check_location_existence($decodeData);
                }
            }
        }
        echo json_encode($data);
    }


    public function pay_as_guest(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "ordersessid", "total_amount", "token_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->pay_as_guest($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function manage_autorecharge(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "mobile", "amount_to_load", "min_amount_drops", "card_id", "token_id", "remove_autorecharge");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->manage_autorecharge($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function wallet_autorecharge_cron(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->wallet_autorecharge_cron($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    /***********************************************************************************
    *		INVENTORY APIS BEGINS
    **********************************************************************************/
    public function check_trash_items(){
	$this->foodiq_model->check_trash_items();
    }
    public function check_moveable_items(){
	$this->foodiq_model->check_moveable_items();
    }
    public function refiller_allocated_locations(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "refiller_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->refiller_allocated_locations($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function inventory_action_options(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "refiller_uid", "menu_id", "action_options", "storeqty", "barcode");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->inventory_action_options($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function scan_inventory_item(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "refiller_uid", "location_uid", "item_barcode");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->scan_inventory_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function add_inventory_item(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "refiller_uid", "menu_id", "is_perishable", 'enterqty');
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->add_inventory_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function move_inventory_item(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "refiller_uid", "menu_id", "barcode", "movefrom", "moveto", "movefrom_id", "moveto_id", "enterqty", "storeqty");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->move_inventory_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function return_inventory_item(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "refiller_uid", "menu_id", "barcode", "returnfrom", "returnto", "returnfrom_id", "returnto_id", "enterqty", "storeqty");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->return_inventory_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function trash_inventory_item(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "refiller_uid", "menu_id", "barcode", "trashfrom", "trashfrom_id", "enterqty", "storeqty");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    $data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->trash_inventory_item($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    public function suggested_items(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "refiller_uid", "refiller_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->suggested_items($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    


    public function check_apk_version(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->check_apk_version($decodeData);
                }
            }
        }
        echo json_encode($data);
    }
    
    public function send_items_report(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "item_barcode", "refiller_id", "report_message");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->send_items_report($decodeData);
                }
            }
        }
	//$decodeData = '';
	//$data = $this->foodiq_model->smartcash_campaign($decodeData);
        echo json_encode($data);
    }
    public function stock_adjustment(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "location_uid", "menu_id", "refiller_id", "enterqty");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->stock_adjustment($decodeData);
                }
            }
        }
	//$decodeData = '';
	//$data = $this->foodiq_model->smartcash_campaign($decodeData);
        echo json_encode($data);
    }
    
    /***********************************************************************************
    *		CRON APIS BEGINS
    **********************************************************************************/
    public function openingstock_cronjob(){
	//$this->foodiq_model->openingstock_cronjob();
    }

    public function send_single_user_notification(){
	$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("isp_uid", "mobile", "token_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
                    $decodeData = json_decode($postdata);
                    $data = $this->foodiq_model->send_single_user_notification($decodeData);
                }
            }
        }
        echo json_encode($data);
    }

    public function send_all_users_notification(){
	$this->foodiq_model->send_all_users_notification();
    }
}

?>