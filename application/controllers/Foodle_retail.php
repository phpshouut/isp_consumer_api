<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(0);
class Foodle_retail extends CI_Controller {
    private $authkey = 'c2hvdXV0Rm9vZGxl';
    
    public function __construct(){
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
	$this->load->helper('string');
	$this->load->library('encrypt');
        $this->load->model('Foodle_retail_model', 'foodle_retail_model');
    }
	
	public function log($function,$request, $responce){
		$data = $this->foodle_retail_model->log($function, $request, $responce);
	}
    

    public function item_list(){
		
	/*$postdata['requestData']='{"loc_id":"1093"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_retail_model->item_list($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->item_list($decodeData);
                }
            }
        }
		$this->log('item_list',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function create_reward_offer(){
	/*$postdata['requestData']='{"loc_id":"1087","loc_uid":"1011087","reward_id":"1","reward_name":"test Coffe","slab_amt":"100"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_retail_model->create_reward_offer($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","loc_uid","reward_id","reward_name","slab_amt");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->create_reward_offer($decodeData);
                }
            }
        }
		$this->log('create_reward_offer',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function reward_rules_list(){
	/*$postdata['requestData']='{"loc_id":"1093","loc_uid":"1011087","type":"all","days":"7"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_retail_model->reward_rules_list($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","type","days");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->reward_rules_list($decodeData);
                }
            }
        }
		$this->log('reward_rules_list',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function edit_rules(){
	/*$postdata['requestData']='{"loc_id":"1087","loc_uid":"1011087","loc_reward_id":"10","slab_amt":"1200","reward_name":"Pomegranate Juice 1"}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->edit_rules($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","loc_uid","loc_reward_id","slab_amt");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->edit_rules($decodeData);
                }
            }
        }
		$this->log('edit_rules',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	
    }
    
    
     public function activate_deactivate_rule(){
	/*$postdata['requestData']='{"loc_id":"1087","loc_uid":"1011087","loc_reward_id":"10","is_deactivate":"0"}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->activate_deactivate_rule($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","loc_uid","loc_reward_id","slab_amt");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->activate_deactivate_rule($decodeData);
                }
            }
        }
		$this->log('activate_deactivate_rule',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	
    }
    
    public function campaign_create(){
	/*$postdata['requestData']='{"loc_id":"1093","loc_uid":"1011095","reward_id":"1","campaign_name":"test1","reward_name":"Alcohol Beer DarkBeer","audience_id":"5","start_date":"2019-04-13 13:36:00","end_date":"2019-04-30 13:37:00","slab_amt":"100","campaign_type":"1","redemption_expiryday":"10","refer_friend":"2","notaudience":"0","audience_name":"","userlist":""}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->campaign_create($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
	//echo json_encode($postdata); die;
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
		
		
                $requiredObjects = array("loc_id","loc_uid","reward_id","campaign_name","reward_name",
					 "audience_id","start_date","end_date","slab_amt",
					 "redemption_expiryday","campaign_type");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->campaign_create($decodeData);
                }
            }
        }
		$this->log('campaign_create',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function campaign_list(){
	/*$postdata['requestData']='{"loc_id":"1093","filter_type":"active","days":7}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->campaign_list($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","filter_type");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->campaign_list($decodeData);
                }
            }
        }
	    $this->log('campaign_list',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
     public function campaign_delete(){
	/*$postdata['requestData']='{"foodle_campaign_id":"1087"}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->campaign_list($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("foodle_campaign_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->campaign_delete($decodeData);
                }
            }
        }
	    $this->log('campaign_delete',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    

    
    
    public function get_audience_list(){
	/*$postdata['requestData']='{"loc_id":"1093","loc_uid":"1011095","days":"7"}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->get_audience_list($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","loc_uid","days");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->get_audience_list($decodeData);
                }
            }
        }
	    $this->log('get_audience_list',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
    
    public function foodle_add_loyalty_audience(){
	/*$postdata['requestData']='{"loc_id":"1087","loc_uid":"1011087","foodle_audience_name":"test 123","foodle_audience_time_duration":"0"
	,"foodle_audience_visit_from_conditon":"1","foodle_audience_visit_from_value":"10",
	"foodle_audience_visit_to_conditon":"2","foodle_audience_visit_to_value":"15","foodle_audience_spend_from_conditon":"1",
	"foodle_audience_spend_from_value":"1500","foodle_audience_spend_to_conditon":"2","foodle_audience_spend_to_value":"3500",
	"foodle_audience_visit_spend_conditon":"1","audience_id":"6"}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->foodle_add_loyalty_audience($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","loc_uid","foodle_audience_name","foodle_audience_time_duration",
					 "foodle_audience_time_duration","foodle_audience_visit_from_conditon","foodle_audience_visit_from_value",
	"foodle_audience_visit_to_conditon","foodle_audience_visit_to_value","foodle_audience_spend_from_conditon",
	"foodle_audience_spend_from_value","foodle_audience_spend_to_conditon","foodle_audience_spend_to_value",
	"foodle_audience_visit_spend_conditon");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->foodle_add_loyalty_audience($decodeData);
                }
            }
        }
	    $this->log('foodle_add_loyalty_audience',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
    
    public function delete_audience(){
	/*$postdata['requestData']='{"audience_id":"1087"}';
        $decodeData = json_decode($postdata['requestData']);
	
        $data= $this->foodle_retail_model->campaign_list($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("audience_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->delete_audience($decodeData);
                }
            }
        }
	    $this->log('delete_audience',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
    
    public function foodle_registration(){
	/*$postdata['requestData']='{"location_name":"sdf","first_name":"sdf","last_name":"sdf","mobile":"9560588376","password":"asd","geo_address":"Chirala, Andhra Pradesh, India","placeid":"ChIJoY22shlESjoRATMi9ji2G6w","latitude":"15.813576","longitude":"asd","location_type":"restaurant","wifi_ssid":"","isp_uid":"101","register_paytmno":"","checque_no":"sdf"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->foodle_registration($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("location_name","first_name","mobile","password","geo_address",
					 "placeid","latitude","longitude","location_type","wifi_ssid","isp_uid","register_paytmno");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->foodle_registration($decodeData);
                }
            }
        }
	    $this->log('foodle_registration',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
    public function password_availability(){
	/*$postdata['requestData']='{"mobile":"9560588376","password":"sandycool8860"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->password_availability($decodeData);
        echo json_encode($data); die;*/
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("mobile","password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->password_availability($decodeData);
                }
            }
        }
	    $this->log('password_availability',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
    
    public function retailer_login(){
	/*$postdata['requestData']='{"mobile":"9560588376","password":"sandycool8860"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->retailer_login($decodeData);
        echo json_encode($data); die;*/
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("mobile","password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->retailer_login($decodeData);
                }
            }
        }
	    $this->log('retailer_login',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
   /* public function get_wallet_balance(){
	$postdata['requestData']='{"loc_id":"1093"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->get_wallet_balance($decodeData);
        echo json_encode($data); die;
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("mobile","password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->get_wallet_balance($decodeData);
                }
            }
        }
	    $this->log('get_wallet_balance',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }*/
   
   public function top_spenders(){
    
   /* $postdata['requestData']='{"loc_id":"1087","filter_type":"month"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->top_spenders($decodeData);
        echo json_encode($data); die;*/
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","filter_type");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->top_spenders($decodeData);
                }
            }
        }
	    $this->log('top_spenders',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    
   }
   
   
   public function top_redeemer(){
    
  /*  $postdata['requestData']='{"loc_id":"1093","filter_type":"month"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->top_redeemer($decodeData);
        echo json_encode($data); die;*/
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","filter_type");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->top_redeemer($decodeData);
                }
            }
        }
	    $this->log('top_redeemer',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    
   }
   
   
   
   /*Chart data*/
   
    public function spend_wifi_data(){
	//$postdata['requestData']='{"loc_id":"1093","days":30}';
       /* $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->spend_wifi_data($decodeData);
        echo json_encode($data); die;*/
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id","days");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->spend_wifi_data($decodeData);
                }
            }
        }
	    $this->log('top_redeemer',$postdata, json_encode($data));//call log function
	    echo json_encode($data);	
	
    }
    
    
    public function interesting_trends(){
	/*$postdata['requestData']='{"loc_id":"1093"}';
        $decodeData = json_decode($postdata['requestData']);
	$data= $this->foodle_retail_model->interesting_trends($decodeData);
        echo json_encode($data); die;*/
	
	$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("loc_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_retail_model->interesting_trends($decodeData);
                }
            }
        }
	    $this->log('interesting_trends',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
    }
    
    public function send_campaign_msg(){
	$audience_user=array("9560588376","8178840723");
	$this->foodle_retail_model->send_campaign_msg($audience_user,46);
    }
	
	public function push_notifcation_test(){
		$postdata['requestData']='{"client_id":"cKbellfYdfo:APA91bGMDPXMBe8qKMZVBUmLFn4JRBE6nFCmdDSS6uEyOmoNEwL4_4JqxfOSa-rK4kQ0Qery9o1h9GpRPGqTG-iw0xEJtFwAHIILBz7g-Q1ee9bCg82Tll9jPLz3dkSsy4H_vts0tlLz"}';
		
        //$decodeData = json_decode($postdata['requestData']);
		$decodeData['client_id']="c5EsIJpIkH8:APA91bHHypm9DbA_H2fl97tnQjkI9UkZ_rg4H_AunfmztmtJ7d3oxuCvmjTkf0SiumdCJ2b_hBhHSfepteo5rPz5EuHkA_LGTs0bKo4acFbbW5rUgXGPNv6El-ons_V2_XXV7RufRxx6";
		$decodeData['title']="foodle";
		$decodeData['message']="foodle alert msg";
	$data= $this->foodle_retail_model->push_notification_test($decodeData);
        echo json_encode($data); die;
		$postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array();
                $requestObjects = json_decode($postdata, true);
				
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
			$dataarr['client_id']=$decodeData->client_id;
			$dataarr['title']="foodle";
			$dataarr['message']="Foodle alert";
			$data = $this->foodle_retail_model->push_notifcation_test($decodeData);
                }
            }
        }
	    $this->log('push_notifcation_test',$postdata, json_encode($data));//call log function
	    echo json_encode($data);
	}
    
    
    
    
    
    
    
    
    
    
    
	
	

}

?>