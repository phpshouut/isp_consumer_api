<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->model('api_model');
	}

	public function index(){
		echo "home page";
		
	}
	
	public function login(){
		//$_POST['requestData'] = '{"username": "admin@shouut.com", "password" : "shouut", "isp_uid" : "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->login($decodeData);
		}
		echo json_encode($data);
	}
	
	public function isp_detail(){
		//$_POST['requestData'] = '{"isp_uid": "002"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->isp_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function slider_data(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->slider_data($decodeData);
		}
		echo json_encode($data);
	}
	
	public function live_usage (){
		//$_POST['requestData'] = '{"isp_uid": "300", "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->live_usage($decodeData);
		}
		echo json_encode($data);
	}
	
	public function promotions (){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->promotions($decodeData);
		}
		echo json_encode($data);
	}

	public function account_detail(){
		//$_POST['requestData'] = '{"isp_uid": "001", "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->account_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function update_mobile_send_otp(){
		//$_POST['requestData'] = '{"old_number": "9650896116" , "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_mobile_send_otp($decodeData);
		}
		echo json_encode($data);
	}
	public function update_mobile_verify_otp(){
		//$_POST['requestData'] = '{"new_number": "9650896118", "otp" : "5553" , "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_mobile_verify_otp($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function usage_logs(){
		//$_POST['requestData'] = '{"user_uid": "30100023", "month" : "06 2017"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->usage_logs($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function my_plan(){
		//$_POST['requestData'] = '{"user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->my_plan($decodeData);
		}
		echo json_encode($data);
	}
	
	public function recommended_plans(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "isp_uid" : "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_plans($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function active_topups(){
		//$_POST['requestData'] = '{"user_uid": "110000000001"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->active_topups($decodeData);
		}
		echo json_encode($data);
	}
	
	public function recommended_speed_topup(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_speed_topup($decodeData);
		}
		echo json_encode($data);
	}
	
	public function recommended_unaccoundancy_topup(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_unaccoundancy_topup($decodeData);
		}
		echo json_encode($data);
	}
	public function recommended_data_topup(){
		//$_POST['requestData'] = '{"user_uid": "30100023", "isp_uid": "301"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_data_topup($decodeData);
		}
		echo json_encode($data);
	}
	public function pending_bills(){
		//$_POST['requestData'] = '{"user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->pending_bills($decodeData);
		}
		echo json_encode($data);
	}
	public function previous_bills(){
		//$_POST['requestData'] = '{"user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->previous_bills($decodeData);
		}
		echo json_encode($data);
	}
	public function current_service_request(){
		//$_POST['requestData'] = '{"user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->current_service_request($decodeData);
		}
		echo json_encode($data);
	}
	public function generate_service_request(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "ticket_type": "suspend_service", "ticket_priority" : "low", "ticket_description": "Suspend my service to 10 day"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->generate_service_request($decodeData);
		}
		echo json_encode($data);
	}
	
	public function update_password(){
		//$_POST['requestData'] = '{"user_uid": "30000002" ,"current_password": "shouut", "new_password": "shouut"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_password($decodeData);
		}
		echo json_encode($data);
	}
	public function contact_us(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->contact_us($decodeData);
		}
		echo json_encode($data);
	}
	
}
