<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DineoutController extends CI_Controller {
    public function __construct(){
        parent :: __construct();
        $this->load->model('DineoutModel', 'dineoutmodel');
    }
    
    public function index(){
        
    }
    
    public function get_dineout_menu_list(){
        $this->dineoutmodel->get_dineout_menu_list();
    }
    
}


?>