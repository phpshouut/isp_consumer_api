<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('asia/kolkata');
//error_reporting(0);
class FlashApi extends CI_Controller {
    public function __construct(){
        parent::__construct();
		$this->load->library('My_PHPMailer');
        $this->load->library('mongo_db');
        $this->load->model('flashapi_model');
    }
    public function index(){
	//echo $_SERVER['DOCUMENT_ROOT'];
        //$this->load->view('welcome_message');

    }
    private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	public function log($function,$request, $responce, $processTime=''){
		$data = $this->flashapi_model->log($function, $request, $responce, $processTime);
	}

	public function flash_listing(){
		/*$_POST['requestData'] = '{"is_login": "1","guest_userid": "","login_userid": "56504eff01fcff067f8b4567",
		"apikey": "14481037585f4dcc3b5aa765d61d8327deb882cf99","change_location":"",
		"location": "28.4592283::77.0824265", "start": "0","offset": "10"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);


				if($responsecode == 1){
					$data = $this->flashapi_model->flash_listing($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->flash_listing($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('flash_listing',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function flash_deal_detail_new(){
		/*$_POST['requestData'] = '{"is_login": "1","guest_userid": "","login_userid": "56504eff01fcff067f8b4567",
        "apikey": "14481037585f4dcc3b5aa765d61d8327deb882cf99","change_location":"",
        "location": "28.4440147::77.063768","dealid":"1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No more flash to show at this time.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//$data = $this->flashapi_model->flash_deal_detail($decodeData);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->flash_deal_detail_new($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->flash_deal_detail_new($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->_sendResponse(json_encode($data));
	}

	public function check_distance(){
		/*$_POST['requestData'] = '{ "location": "28.4439926::77.0637655", "change_location": "", "is_login": "1",
		"login_userid": "56504eff01fcff067f8b4567", "guest_userid": "", "apikey":
		"14481037585f4dcc3b5aa765d61d8327deb882cf99","dealid": "8"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No more flash to show at this time.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//$data = $this->flashapi_model->flash_deal_detail($decodeData);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->check_distance($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->check_distance($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->_sendResponse(json_encode($data));
	}

	public function share_deal_redemption(){
		/*$_POST['requestData'] = '{"is_login": "1","guest_userid": "","login_userid": "5698d47c01fcff0a6e8b4567",
		"apikey": "14528564445f4dcc3b5aa765d61d8327deb882cf99", "dealid": "1", "sharevia": "facebook"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No more flash to show at this time.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->share_deal_redemption($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->share_deal_redemption($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		//$this->log('share_deal_redemption',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function generate_voucher_code(){
		/*$_POST['requestData'] = '{"is_login": "1","guest_userid": "","login_userid": "56504eff01fcff067f8b4567",
		"apikey": "14481037585f4dcc3b5aa765d61d8327deb882cf99","change_location":"","location": "28.5407267000::77.2263989000",
		"dealid":"13", "via": "android"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong Request.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//$data = $this->flashapi_model->flash_deal_detail($decodeData);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->generate_voucher_code($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->generate_voucher_code($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->_sendResponse(json_encode($data));
	}

	public function coupon_code_send(){
		/*$_POST['requestData'] = '{"is_login": "1", "login_userid": "56504eff01fcff067f8b4567", "apikey":
		"14481037585f4dcc3b5aa765d61d8327deb882cf99","dealid" : "8", "type" : "sms"}';*/
		$data = array();
		$decodeData = json_decode($_POST['requestData']);
		if($decodeData->is_login == '1' ){
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->coupon_code_send($decodeData);
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}else{
			$data['resultCode'] = '0';
		}

		$this->_sendResponse(json_encode($data));

	}

	public function my_deals(){
		/*$_POST['requestData'] = '{"login_userid":"55c1e0da01fcffdc498b460f","change_location":"",
		"guest_userid":"57287fe1ab121c761b4c4480","apikey":"14491218925f4dcc3b5aa765d61d8327deb882cf99",
		"location":"28.5386690::77.2270210","is_login":"1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No more flash to show at this time.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->my_deals($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->my_deals($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('my_deals',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}

	public function store_list(){
		/*$_POST['requestData'] = '{ "location": "28.4439926::77.0637655", "change_location": "", "is_login": "1",
		"login_userid": "56504eff01fcff067f8b4567", "guest_userid": "", "apikey":
		"14481037585f4dcc3b5aa765d61d8327deb882cf99","dealid": "2"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'No more flash to show at this time.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//$data = $this->flashapi_model->flash_deal_detail($decodeData);
			if($decodeData->is_login == '1'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->store_list($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else if($decodeData->guest_userid != ''){
				$responsecode = $this->flashapi_model->authenticateApiGuestuser($decodeData->guest_userid);
				if($responsecode == 1){
					$data = $this->flashapi_model->store_list($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->_sendResponse(json_encode($data));
	}

	public function share_deal(){
		/*$_POST['requestData'] = '{"dealid": "1", "sharevia": "whatsapp"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Invalid Request.';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->share_deal($decodeData);
		}
		$this->_sendResponse(json_encode($data));
	}
	public function sms_flash_redeem(){
		/*$_POST['requestData'] = '{"mobile": "9650896116", "deal_id": "23"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->sms_flash_redeem($decodeData);
		}
		$this->_sendResponse(json_encode($data));
	}

	public function sms_brand_voucher_code_redeem(){
		/*$_POST['requestData'] = '{"mobile": "9650896116", "voucher_code": "23"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->sms_brand_voucher_code_redeem($decodeData);
		}
		$this->_sendResponse(json_encode($data));
	}

	public function shouut_sms_redeemption(){
		$data_responce = array();
		/*$phone = '9650896116';
		$sms = "SHOUUT 7471161";
		$sms = rtrim($sms);*/
		$phone = $_GET['msisdn'];
		$phone = substr($phone, -10);
		$sms = $_GET['sms'];
		 $data_responce = array();
		if(isset($sms) && $sms != ''){
			$sms = explode(" ", $sms);
			$sms = array_values(array_filter($sms));
			if(count($sms) == 3){//call redemption
				//$sms = "SHOUUT REDEEM OF12";
				$data_responce['resultCode'] = '1';
				$data_responce['resultMessage'] = 'Success';
				$deal_id = substr($sms[2],2);
				$requestData = array(
					'deal_id' => $deal_id,
					'mobile'=> $phone
				);
				$service_url = 'https://www.shouut.com/mobileApp/android/flashApi/sms_flash_redeem';
				$curl = curl_init($service_url);
				$requestData = $requestData;
				$data = json_encode($requestData);
				$curl_post_data = array("requestData" => $data);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
				$curl_response = curl_exec($curl);
				curl_close($curl);
			}
			elseif(count($sms) == 2){// call avail

				$data_responce['resultCode'] = '1';
				$data_responce['resultMessage'] = 'Success';
				$msg = $sms[1];
				$find_hash = strpos($sms[1], "#");
				if($find_hash === false){
					//$sms = "SHOUUT 4583948"; redeem brand voucher for wifi internet
					$voucher_code = $msg;
					$requestData = array(
						'voucher_code' => $voucher_code,
						'mobile'=> $phone
					);
					$service_url = 'https://www.shouut.com/mobileApp/android/flashApi/sms_brand_voucher_code_redeem';
					$curl = curl_init($service_url);
					$requestData = $requestData;
					$data = json_encode($requestData);
					$curl_post_data = array("requestData" => $data);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$curl_response = curl_exec($curl);
					curl_close($curl);

				}
				else{
					//$sms = "SHOUUT 52#4583948"; avail voucher code at store
					$msg = explode("#", $msg);
					$store_id = $msg[0];
					$coupon_code = $msg[1];
					$requestData = array(
						'store_id' => $store_id,
						'coupon_code' => $coupon_code,
						'mobile'=> $phone
					);
					$service_url = 'https://www.shouut.com/channel/api/channel/sms_flash_avail';
					$curl = curl_init($service_url);
					$requestData = $requestData;
					$data = json_encode($requestData);
					$curl_post_data = array("requestData" => $data);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					$curl_response = curl_exec($curl);
					curl_close($curl);
				}


			}
			else{// wrong msg pattern
				$data_responce['resultCode'] = '0';
				$data_responce['resultMessage'] = 'Wrong Message pattern';
			}
		}
		else{//blank msg
			$data_responce['resultCode'] = '0';
			$data_responce['resultMessage'] = 'Message can not be blank';
		}
		if($data_responce['resultCode'] == '0'){
			$msg = urlencode($data_responce['resultMessage']);
			$mobile = $phone;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://173.45.76.226:81/send.aspx?username=shouut&pass=shouut&route=trans1&senderid=SHOUUT&numbers=".$mobile."&message=".$msg);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_exec($ch);
			$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Returns 200 if everything went well
			curl_close($ch);
		}
		//$this->_sendResponse(json_encode($data));
	}


	public function wifi_locations(){
		/*$_POST['requestData'] = '{"location": "28.5417267000::77.2263989000"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_locations($decodeData);
		}
		$this->log('wifi_locations',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_locations1(){
		$data = array();
		$data = $this->flashapi_model->wifi_locations1();
		$this->log('wifi_locations1','', $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_locations2(){
		/*$_POST['requestData'] = '{"location": "28.5417267000::77.2263989000"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_locations2($decodeData);
		}
		$this->log('wifi_locations2',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_offers2(){
		/*$_POST['requestData'] = '{"userId": "56979e3c01fcff5c4d8b4567",
		"apikey":"1452777020faf598d3d341e10b8643f4d7f87ba08b", "location": "28.5417267000::77.2263989000",
		"locationId": "2", "via":"android"}';*/
		$time_start = microtime(true);
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_offers2($decodeData);
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$time_end = microtime(true);
		$processTime = ($time_end-$time_start);
		$this->log('wifi_offers2',$_POST['requestData'], $data, $processTime);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_new_bssid(){
		/*$_POST['requestData'] = '{"is_login": "1","login_userid": "56979e3c01fcff5c4d8b4567","guest_userid":
		"","apikey":"1452777020faf598d3d341e10b8643f4d7f87ba08b"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if($decodeData->login_userid == '10'){
				$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->login_userid,
					$decodeData->apikey);
				if($responsecode == 1){
					$data = $this->flashapi_model->wifi_new_bssid($decodeData);
				}else{
					$data['resultCode'] = '0';
					$data['resultMsg'] = 'Unauthorised access. Please Login again.';
				}
			}
			else{
				$data = $this->flashapi_model->wifi_new_bssid($decodeData);
			}

		}
		$this->_sendResponse(json_encode($data));
		$this->log('wifi_new_bssid',$_POST['requestData'], $data);//call log function
	}

	public function wifi_location_provider_id(){
		//$_POST['requestData'] = '{"bssid": "C4:01:7C:01:74:48"}';
		$time_start = microtime(true);
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_location_provider_id($decodeData);
		}
		$this->_sendResponse(json_encode($data));
		$time_end = microtime(true);
		$processTime = ($time_end-$time_start);
		$this->log('wifi_location_provider_id',$_POST['requestData'], $data, $processTime);//call log function
	}

	public function wifi_default_offer(){

		$data = array();
		$data = $this->flashapi_model->wifi_default_offer();
		$this->_sendResponse(json_encode($data));
	}
	public function wifi_registration(){
		/*$_POST['requestData'] = '{"Userid": "56504eff01fcff067f8b4567", "locationid": "1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_registration($decodeData);
		}
		$this->log('wifi_registration',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}

	public function wifi_offer_redemption(){
		/*$_POST['requestData'] = '{"userId": "56504eff01fcff067f8b4567",
		"apikey":"14481037585f4dcc3b5aa765d61d8327deb882cf99", "location": "28.5417267000::77.2263989000", "offerId"
		: "2"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_offer_redemption($decodeData);
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_offer_redemption',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));


	}

	public function wifi_plans(){
		/*$_POST['requestData'] = '{"userId": "56504eff01fcff067f8b4567",
		"apikey":"14481037585f4dcc3b5aa765d61d8327deb882cf99", "location": "28.5417267000::77.2263989000",
		"providername" : "tikona", "providerId": "1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_plans($decodeData);
			}else{
				$data['resultCode'] = '0';
				$data['resultMsg'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_plans',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}

	public function wifi_user_ipaddress_registration(){
		/*$_POST['requestData'] = '{"Userid": "56504eff01fcff067f8b4567", "APMAC": "fasa", "UEMAC": "acbd", "IpAddress":
		"gggg"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_user_ipaddress_registration($decodeData);
		}
		$this->log('wifi_user_ipaddress_registration',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_user_session_add(){
		/*$_POST['requestData'] = '{"userId": "5698d47c01fcff0a6e8b4567", "apikey":
		"14528564445f4dcc3b5aa765d61d8327deb882cf99", "locationid": "1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_user_session_add($decodeData);
			}else{
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_user_session_add',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_user_free_session_add(){
		/*$_POST['requestData'] = '{"userId": "5698d47c01fcff0a6e8b4567", "apikey":
		"14528564445f4dcc3b5aa765d61d8327deb882cf99"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_user_free_session_add($decodeData);
			}else{
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_user_free_session_add',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_user_session_list(){
		/*$_POST['requestData'] = '{"userId": "5698d47c01fcff0a6e8b4567", "apikey":
		"14528564445f4dcc3b5aa765d61d8327deb882cf99", "start": "0", "offset": "10"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_user_session_list($decodeData);
			}else{
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_user_session_list',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_user_available_session(){
		/*$_POST['requestData'] = '{"userId": "5698d47c01fcff0a6e8b4567", "apikey":
		"14528564445f4dcc3b5aa765d61d8327deb882cf99", "providerId": "1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->flashapi_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
				$data = $this->flashapi_model->wifi_user_available_session($decodeData);
			}else{
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_user_available_session',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}

	public function wifi_location_macid(){
		/*$_POST['requestData'] = '{"locationId": "1"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_location_macid($decodeData);
		}
		$this->log('wifi_location_macid',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function captive_portal_location_wifi_offer(){
		/*$_POST['requestData'] = '{"locationId": ""}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->captive_portal_location_wifi_offer($decodeData);

		}
		$this->log('captive_portal_location_wifi_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function user_free_wifi_voucher_codes(){
		//$_POST['requestData'] = '{"phone": "7503945244"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->user_free_wifi_voucher_codes($decodeData);

		}
		$this->log('user_free_wifi_voucher_codes',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function user_free_wifi_voucher_offer(){
		/*$_POST['requestData'] = '{"voucherid": "2", "locationId": "10", via: "web"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->user_free_wifi_voucher_offer($decodeData);

		}
		$this->log('user_free_wifi_voucher_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function user_free_wifi_voucher_code_use(){
		/*$_POST['requestData'] = '{"voucherid": "2"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->user_free_wifi_voucher_code_use($decodeData);

		}
		$this->log('user_free_wifi_voucher_code_use',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_offer_detail(){
		/*$_POST['requestData'] = '{"offerId": "2", "locationId": ""}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_offer_detail($decodeData);

		}
		$this->log('wifi_offer_detail',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function wifi_user_otp(){
		/*$_POST['requestData'] = '{"macid": "dk9D4", "mobile": "9485858454", "otp": "3432"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_user_otp($decodeData);

		}
		$this->log('wifi_user_otp',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function wifi_user_otp_verify(){
		/*$_POST['requestData'] = '{"mobile": "9485858454", "otp": "3432"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_user_otp_verify($decodeData);

		}
		$this->log('wifi_user_otp_verify',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function wifi_user_new_registration(){
		/*$_POST['requestData'] = '{"mobile": "9485858454"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->wifi_user_new_registration($decodeData);

		}
		$this->log('wifi_user_new_registration',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}


	public function check_user_last_seen_wifioffer(){
		/*$_POST['requestData'] = '{"userid": "56979e3c01fcff5c4d8b4567"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->flashapi_model->check_user_last_seen_wifioffer($decodeData);

		}
		$this->log('check_user_last_sceen_wifioffer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
}

?>
