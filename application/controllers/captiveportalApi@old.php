<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('asia/kolkata');
error_reporting(0);
class CaptiveportalApi extends CI_Controller {
    public function __construct(){
        parent::__construct();
	header("Access-Control-Allow-Origin: *");
	$this->load->library('My_PHPMailer');
        $this->load->library('mongo_db');
        $this->load->model('captiveportal_model');
    }
    public function index(){
	//echo $_SERVER['DOCUMENT_ROOT'];
        //$this->load->view('welcome_message');

    }
    private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	public function log($function,$request, $responce){
		$data = $this->captiveportal_model->log($function, $request, $responce);
	}
        
        public function captive_ad_data(){
           
		//$_POST['requestData'] = '{"userId":"57bc0292ab121c3d0b98134d","apikey":"14719392186a02e1baa78be9fcde64b38d9cc5ebae","locationId":"12","via":"android"}';
		
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                           
				$data = $this->captiveportal_model->captive_ad_data($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	 public function offerlisting_ad_data(){
           
	/*	$_POST['requestData'] = '{"userId":"577ccc487343408918a877d1","apikey":"14677965523058cfec51bc9064ef835d32f6ed3304",
                 "locationId":"9","via":"android"}';*/
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiGuestuser($decodeData->userId);
			if($responsecode == 1){
                           
				$data = $this->captiveportal_model->get_offerlisting_data($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('offerlisting_ad_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	 public function sponsoredlisting_ad_data(){
		 
           
		/*$_POST['requestData'] = '{"userId":"580b4951734340b27c20c024","apikey":"14677965523058cfec51bc9064ef835d32f6ed3304",
                 "locationId":"9","via":"android"}';*/
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiGuestuser($decodeData->userId);
			if($responsecode == 1){
                           
				$data = $this->captiveportal_model->get_sponsered_listing_data($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('sponsoredlisting_ad_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	

        public function redirect_url_api()
        {
           /* $_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589",
                 "locationId":"12","offer_id":"1",via":"android"}';*/
           /* $_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","offer_id":"1",
                 "locationId":"12","via":"android"}';*/
           $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                          
				$data = $this->captiveportal_model->redirect_url_api($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('redirect_url_api',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		  public function app_download_data()
        {
          /* $_POST['requestData'] = '{"userId":"580efc1aca94f11973ef3a71","apikey":"1477377050573d2d9146266da22e1cb943b71bcccf",
                 "locationId":"9","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';*/
          
           $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                          
				$data = $this->captiveportal_model->app_download_data($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('app_download_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		 public function captcha_accurate_api()
        {
           /* $_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589",
                 "locationId":"12","offer_id":"1",via":"android"}';*/
           /* $_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","offer_id":"1",
                 "locationId":"12","via":"android"}';*/
           $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                          
				$data = $this->captiveportal_model->captcha_accurate_api($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captcha_accurate_api',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		
		 public function offer_ad_detail(){
		 
           
		/*$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589",
                 "locationId":"9","via":"android","offer_id":"2"}';*/
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiGuestuser($decodeData->userId);
			if($responsecode == 1){
                           
				$data = $this->captiveportal_model->offer_ad_detail($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('offer_ad_detail',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
        
        public function poll_answer()
        {
            /*$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","offer_id":"10","ques_id":"1","option_id":"1",
                 "locationId":"12","via":"android"}';*/
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                          
				$data = $this->captiveportal_model->poll_answer($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
			
		$this->log('poll_answer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		
		 public function poll_data_test()
        {
           
                          
				$data = $this->captiveportal_model->poll_ad_datatest($decodeData);
		
		$this->_sendResponse(json_encode($data));
        }
		
		 public function redeem_offer()
        {
           /*   $_POST['requestData'] = '{"userId":"57e13ed1ca94f1a1b5b7068b","apikey":"14743794738824e883cab3aaada59ee84d173bc702","offer_id":"7",
                 "locationId":"12","via":"android"}';*/
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                          
				$data = $this->captiveportal_model->redeem_offer($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('redeem_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
            
        }
		
			 public function shouut_sms_offer()
        {
			
			
			
           //   $_POST['requestData'] = '{"userId":"57e13ed1ca94f1a1b5b7068b","apikey":"14743794738824e883cab3aaada59ee84d173bc702","mobile":"9560588376"}';
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){
                          
				$data = $this->captiveportal_model->shouut_sms_link($decodeData);
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('Shouut_sms_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
            
        }
		
		 public function app_install_click()
        {
		  //   $_POST['requestData'] = ' {"userid":"58208dffca94f1fd680d7b4f","cpofferid":"6"}';
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->app_install_click($decodeData);
			
		}
		$this->log('app_install_click',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
            
        }

   // dineout api
	public function dineout_offer_click_analytics()
	{


		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->dineout_offer_click_analytics($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_offer_click_analytics',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function dineout_sponsor_click_analytics()
	{


		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->dineout_sponsor_click_analytics($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_sponsor_click_analytics',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function dineout_offer_click_booking()
	{
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->dineout_offer_click_booking($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_offer_click_booking',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function dineout_sponsor_click_booking()
	{
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->dineout_sponsor_click_booking($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_sponsor_click_booking',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	//cp 2 api
	
		public function vadodracp2_offers_totalmb(){
		//$_POST['requestData'] = ' {"userId":"57cae91bab121ccd18fdf050","apikey":"1472915739488584dd74ef4c332cd1dbcf0d77b0e2","locationId":"9","via":"web","macid":"","platform_filter":"Android Phone"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->captiveportal_model->vadodracp2_offers_totalmb($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function cp2_offers_totalmb(){
		/*$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352",
		"apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->captiveportal_model->cp2_offers_totalmb($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid(){

	//	$_POST['requestData'] = '{"userId":"57e13ed1ca94f1a1b5b7068b","apikey":"14743794738824e883cab3aaada59ee84d173bc702","locationId":"9","via":"web","macid":"","platform_filter":"Android Phone"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->captiveportal_model->captive_ad_data_grid($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data_grid',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function cp_offer_sceen_user()
	{
		// $_POST['requestData'] = ' {"userid":"58208dffca94f1fd680d7b4f","cpofferid":"6"}';
		//	$_POST['requestData'] = '{"userid":"57bc131bab121c770f981352","cpofferid":"44","locationid":"9","via":"web","platform_filter":"","macid":"","offertype":"redeem"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{

			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->cp_offer_sceen_user($decodeData);

		}
		$this->log('cp_offer_sceen_user',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}
	public function captive_portal_impression()
	{
		//$_POST['requestData'] = '{"locationId" : "9", "macid" : "3c:4d", "platform_filter" : "Android Phone"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->captive_portal_impression($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('captive_portal_impression',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid_detail(){
		//$_POST['requestData'] = '{"offer_id":"9", "offer_type" : "video_add"}';

//echo "<pre>"; print_R($_POST); die;
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->captive_ad_data_grid_detail($decodeData);

		}
		$this->log('captive_ad_data_grid_detail',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function get_mb_data()
	{
		//$_POST['requestData'] = '{"amount":"15"}';
		$decodeData = json_decode($_POST['requestData']);
		$data = $this->captiveportal_model->get_mb_data($decodeData);
		$this->_sendResponse(json_encode($data));
	}
	public function insert_paytm_success()
	{
		//$_POST['requestData']='{""}'
		$decodeData = json_decode($_POST['requestData']);
		$data = $this->captiveportal_model->insert_paytm_success($decodeData);
	}
	public function activate_voucher()
	{

		//  $_POST['requestData'] = ' {"mobile":"9560588376","voucher":"3877874"}';
		//$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->activate_voucher($decodeData);

		}
		$this->log('activate_voucher',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}
	public function voucher_availed()
	{
		//	 $_POST['requestData'] = ' {"mobile":"9560588376","voucher":"3877874"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->voucher_availed($decodeData);

		}
		$this->log('voucher_availed',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function cp2_offers_totalmb_website(){
		/*$_POST['requestData'] = '{"userId":"586cfd80ca94f1f78ac2c9ff",
  "apikey":"14835377922c8914b5dfb5a95ae26025d1d07ab07a","location":"28.5494490000::77.2001370000",
  "platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->captiveportal_model->cp2_offers_totalmb_website($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb_website',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid_website(){
		/*$_POST['requestData'] = '{"userId":"586cfd80ca94f1f78ac2c9ff",
  "apikey":"14835377922c8914b5dfb5a95ae26025d1d07ab07a","location":"28.5494490000::77.2001370000",
  "platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,
				$decodeData->apikey);
			if($responsecode == 1){

				$data = $this->captiveportal_model->captive_ad_data_grid_website($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data_grid_website',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function skhotspot_voucher_validate(){
		//$_POST['requestData'] = '{"mobile":"9650896116", "voucher":"462943"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_model->skhotspot_voucher_validate($decodeData);
		}
		$this->log('skhotspot_voucher_validate',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function get_survey_question()
	{
			//$_POST['requestData'] = '{"userId":"5878b1b3ca94f1decb73d608","apikey":"148430481974d1998d373741e9164432ca923fc09f","offer_id":"15" ,"ques_id":"","option_id":""}';
			//$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,$decodeData->apikey);
			
			if($responsecode == 1){

				$data = $this->captiveportal_model->get_survey_question($decodeData);
			}else{
				
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('get_survey_question',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_subsequent_question()
	{
		
			//$_POST['requestData'] = '{"userId":"5878b1b3ca94f1decb73d608","apikey":"148430481974d1998d373741e9164432ca923fc09f","locationId":"10","offer_id":"15" ,"ques_id":"1","option_id":"4"}';
		//	$_POST['requestData'] = ' {"userId":"57bc131bab121c770f981352","apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","via":"web","macid":"","platform_filter":"Android Phone","offer_id":"78","ques_id":"1","option_id":"2"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			
			//echo "========>>>".$decodeData->userId;
			$responsecode = $this->captiveportal_model->authenticateApiuser($decodeData->userId,$decodeData->apikey);
			
			if($responsecode == 1){

				$data = $this->captiveportal_model->get_subsequent_question($decodeData);
			}else{
				
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('get_subsequent_question',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
}

?>
