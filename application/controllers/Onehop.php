<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onehop extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->model('onehop_model');
	}

	public function index(){
		echo "Onehop page";
		
	}
	public function network_create_modify(){
		$this->onehop_model->network_create_modify();
	}
	
}
