<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Offlineapitest extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->model('offlineapitest_model');
		  $this->load->model('publicwifi_model');
		ini_set('memory_limit', '-1');
		
	}

	public function index(){
		echo "home page";
		
	}
	public function log($request, $responce){
		$data = $this->offlineapitest_model->log($request, $responce);
	}
	
	 private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	
	public function sync_datacontent()
	{
		//echo "sync_datacontent <br>";
	 
	$_POST['requestData'] = '{"macId":["b8:27:eb:0e:92:89","50:3e:aa:4c:e0:b2\n"],"apikey":"","buildid":"","syncincomplete":0}';
	//	$_POST['requestData'] = '{"macId":["b8:27:eb:ef:d7:4a"],"apikey":"","buildid":"","syncincomplete":0}';
		//$postdata = file_get_contents("php://input");
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//$decodeData = json_decode($postdata);
			$data = $this->offlineapitest_model->sync_datacontent($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->log($_POST['requestData'], json_encode($data));//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function sync_db()
	{
		//$_POST['requestData'] = '{"macId":["b8:27:eb:6b:0e:8a","b8:27:eb:3e:5b:df\n"],"apikey":"56d3e872441ccff58e6919230c1ef345","buildid":"4.3"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->sync_db($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function new_ssid_get()
	{
	$_POST['requestData'] = '{"macId":["b8:27:eb:79:ee:eb"]}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->new_ssid_get($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));	
	}

	public function sync_upload_db()
	{
		$_POST['requestData'] = '{"sqlfile":"406_upload_radius.sql","apikey":"b8:27:eb:79:ee:eb"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->sync_upload_db($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}	
	
	public function get_resouces_data()
	{
	//	$_POST['requestData'] = '{"macId":"b8:27:eb:6d:aa:79"]}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->get_resouces_data($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));	
		
	}
	
	
	
	public function update_vendor_info()
	{
		echo phpinfo(); die;
		
	}
	
	public function ping_insert_data()
	{
		
			if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->ping_insert_data($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function build_version_start()
	{
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->build_version_start($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function build_version_update()
	{
		//$_POST['requestData'] = '{"payload_receive":"405283000","lastid":"514","sync_comp":"1","upload_status":"1"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->build_version_update($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_server_info()
	{
		//$_POST['requestData'] = '{"macId":"b8:27:eb:79:ee:eb"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->get_server_info($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function copy_s3_state()
	{
		//$_POST['requestData'] = '{"macId":"b8:27:eb:79:ee:eb","syncmanully":1}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapitest_model->copy_s3_state($decodeData);
		}
		$this->log($_POST['requestData'], json_encode($data));//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function send_visitor_email()
	{
	//	$_POST['requestData']=  '{"visitingemail":"sandeep@shouut.com","person_tomett":"Deepak","visiting_company":"IBM","meeting_purpose":"official","visitor_name":"vikas","visitor_email":"vikas@shouut.com","visitor_org":"SHOUUT"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapitest_model->send_visitor_email($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function cancel_sync()
	{
		//$_POST['requestData']=  '{"loc_id":"364"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapitest_model->cancel_sync($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function sync_data_completed()
	{
		
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapitest_model->sync_data_completed($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function sync_update_statuses()
	{
		//$_POST['requestData']='{"macid":"b8:27:eb:79:ee:eb","buildid":"9","progress_status":"zipdownloaded","dataarr":{"zip_size":8204438,"last_file":"offlinedynamic.zip"}}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapitest_model->sync_update_statuses($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function visitor_email_action(){
		//$_POST['requestData']='{"registerid":"4","type":"accept"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapitest_model->visitor_email_action($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	
	
	
	
	
	
	
}
