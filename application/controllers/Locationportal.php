<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Locationportal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
                $this->load->model('locationportal_model');
		//$this->load->library('mongo_db');
		
		$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
		$this->load->library('s3',$paramsthree);
	}
	public function index(){
		//print_r($_SESSION);die;
		//echo $_SERVER['DOCUMENT_ROOT'];
		//$this->load->view('welcome_message');
		//echo $_SERVER['DOCUMENT_ROOT'];
		//$this->publicwifi_model->createuserpanelfolder('cp2', '101', '11', '6');

		
	}
	public function log($function,$request, $responce){
		$data = $this->locationportal_model->log($function, $request, $responce);
	}
	
	public function login(){
		//$_POST['requestData'] = '{"user_email" : "styagibtech@gmail.com", "password":"213456"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->login($decodeData);
		}
		echo json_encode($data);
	}
	public function insights(){
		//$_POST['requestData'] = '{"location_id" : "23", "start_date":"2017-11-21", "end_date":"2017-11-27", "visitor_type" : "", "day_filter" : "", "traffic_type" : "traffic,session_time,data_use"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->insights($decodeData);
		}
		echo json_encode($data);
	}
	
	public function current_pland_detail_retail_cafe(){
		//$_POST['requestData'] = '{"location_uid" : "140146"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->current_pland_detail_retail_cafe($decodeData);
		}
		echo json_encode($data);
	}
	
	public function update_plan_bandwidth_retail_cafe(){
		//$_POST['requestData'] = '{"location_uid" : "101233", "plan_id" : "6","download_rate" : "12","upload_rate" : "11","time_limit" : "10", "no_of_session" : "3"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->update_plan_bandwidth_retail_cafe($decodeData);
		}
		echo json_encode($data);
	}
	public function get_plan_list(){
		
		//$_POST['requestData'] = '{"location_uid" : "10194"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->get_plan_list($decodeData);
		}
		echo json_encode($data);
	}
	public function get_plan_detail(){
		
		//$_POST['requestData'] = '{"plan_id" : "6"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->get_plan_detail($decodeData);
		}
		echo json_encode($data);
	}
	public function current_image_path(){
		//$_POST['requestData'] = '{"location_uid" : "140146"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->current_image_path($decodeData);
		}
		echo json_encode($data);
	}
	
	public function data_report(){
		//$_POST['requestData'] = '{"location_uid" : "12196","location_id" : "23","startdate" : "2018-01-02","enddate" : "2018-01-02"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->reseller_traffic_report($decodeData);
		}
		echo json_encode($data);
	}
	public function user_report(){
		//$_POST['requestData'] = '{"location_uid" : "101233","location_id" : "86","startdate" : "2017-12-18","enddate" : "2017-12-18"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->user_report($decodeData);
		}
		echo json_encode($data);
	}
	public function traffic_report(){
		//$_POST['requestData'] = '{"location_id" : "91","location_uid":"101247", "startdate": "2017-12-11", "enddate": "2017-12-11"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->traffic_report($decodeData);
		}
		echo json_encode($data);
	}
	public function terms_of_use_get(){
		//$_POST['requestData'] = '{"location_uid" : "101247"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->terms_of_use_get($decodeData);
		}
		echo json_encode($data);
	}
	public function update_terms_of_use(){
		//$_POST['requestData'] = '{"location_uid" : "101247", "terms": "testing"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->update_terms_of_use($decodeData);
		}
		echo json_encode($data);
	}
	public function update_password(){
		//$_POST['requestData'] = '{"location_uid" : "101247", "current_password": "testing", "new_password": "testing"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->update_password($decodeData);
		}
		echo json_encode($data);
	}
	
		public function pin_signupdata(){
	//	$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->pin_signupdata($decodeData);
		}
		echo json_encode($data);
	}
	
	public function update_signupdata(){
	//	$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","signuppoint":"400"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->update_signupdata($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function update_pervisitdata(){
		//$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","pervisitpoint":"400"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->update_pervisitdata($decodeData);
		}
		echo json_encode($data);
	}
	
	public function pin_reset(){
	//	$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->pin_reset($decodeData);
		}
		echo json_encode($data);
	}
	
	public function loyalty_membercafe()
	{
			//$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","search":""}';
			if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->loyalty_membercafe($decodeData);
		}
		echo json_encode($data);
	}
	
	public function loyalty_membercafe_detail()
	{
		//$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","uid":"9560588376"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->loyalty_membercafe_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function list_reward_cafe()
	{
		//$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->list_reward_cafe($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_update_cafereward()
	{
		//$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","itemid":"44","item_name":"test143","loyalty_points":"150","start_date":"27-12-2017","end_date":"30-12-2017"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->add_update_cafereward($decodeData);
		}
		echo json_encode($data);
	}
	
	public function edit_cafereward()
	{
		//$_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","id":"44"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->edit_cafereward($decodeData);
		}
		echo json_encode($data);
	}
	
	 public function delete_cafereward()
   {
	  // $_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","delid":"44"}';
	   if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->delete_cafereward($decodeData);
		}
		echo json_encode($data);
   }
   
   public function loyalty_memberretail()
	{
		//	$_POST['requestData'] = '{"location_id" : "104","location_uid":"101265","search":""}';
			if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->loyalty_memberretail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function list_reward_retail()
	{
		//$_POST['requestData'] = '{"location_id" : "104","location_uid":"101265"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->list_reward_retail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_update_retailreward()
	{
		//$_POST['requestData'] = '{"location_id" : "104","location_uid":"101265","itemid":"44","offer_name":"test143","offer_desc":"test","offer_value":"15","start_date":"27-12-2017","end_date":"10-01-2018"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->add_update_retailreward($decodeData);
		}
		echo json_encode($data);
	}
	
		public function edit_retailreward()
	{
	//	$_POST['requestData'] = '{"location_id" : "104","location_uid":"101265","id":"44"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->edit_retailreward($decodeData);
		}
		echo json_encode($data);
	}
	
	 public function delete_retailreward()
   {
	  // $_POST['requestData'] = '{"location_id" : "76","location_uid":"101219","delid":"44"}';
	   if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->delete_retailreward($decodeData);
		}
		echo json_encode($data);
   }
	
	/**********HOTEL API STARTS******************/
	
	public function hotel_active_guestcount()
	{
		//$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142"}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_active_guestcount($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotel_active_guestlist()
	{
		//$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142","search":""}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_active_guestlist($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotel_editguest()
	{
		//$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142","id":"41"}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_editguest($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotelplan_list()
	{
		//$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotelplan_list($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hoteladdupdate_guest_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142","mobileno":"9060588376","planid":"6","email":"sandee121@gmail.com","lastname":"sasdf"
		,"age":"18-30","gender":"M","roomno":"a12","devices":"2","checkin":"02-01-2018 16:16","checkout":"17-01-2018 12:16","firstname":"asd","userid":"43"}';*/
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hoteladdupdate_guest_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotelupdate_exit_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142","mobileno":"9060588376","email":"sandee121@gmail.com","lastname":"sasdf"
		,"age":"18-30","gender":"M","roomno":"a1234","firstname":"asd","userid":"43"}';*/
	   if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotelupdate_exit_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotel_guest_setting_add()
	{
		//$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142","plan_id":"6","plan_hour":"4","is_enable":"1"
		//}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_guest_setting_add($decodeData);
		}
		echo json_encode($data);
	}
	
	 public function hotel_guest_setting()
	{
	//$_POST['requestData'] = '{"location_id" : "51","location_uid":"101142"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_guest_setting($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotel_expired_user()
	{
		//$_POST['requestData'] = '{"location_id": "51","location_uid":"101142"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_expired_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hotel_delete_user()
	{
		//$_POST['requestData'] = '{"location_id": "51","location_uid":"101142","mobile":"9060588376"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hotel_delete_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function mail_chimp_settingdata()
	{
	//	$_POST['requestData'] = '{"location_id": "51","location_uid":"101142"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->mail_chimp_settingdata($decodeData);
		}
		echo json_encode($data);
	}
	
	public function mailchimp_list()
	{
		//$_POST['requestData'] = '{"location_id": "51","location_uid":"101142","apikey":"cab8bc62d58ae20d87234eaaa5995511-us17"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->mailchimp_list($decodeData);
		}
		echo json_encode($data);
	}
	
		public function mail_chimp_settingadd()
	{
		//$_POST['requestData'] = '{"location_id": "51","location_uid":"101142","apikey":"cab8bc62d58ae20d87234eaaa5995511-us17","listid":"9cddd1826c","is_enable":"1"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->mail_chimp_settingadd($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function hospital_active_guestcount()
	{
		//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118"}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_active_guestcount($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function hospital_active_guestlist()
	{
		//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","search":""}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_active_guestlist($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospital_editguest()
	{
	//	$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","id":"8"}';
		
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_editguest($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospitalplan_list()
	{
		//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospitalplan_list($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospitaladdupdate_guest_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","mobileno":"9060588376","planid":"6","lastname":"sasdf"
		,"roomno":"a123","devices":"2","checkin":"02-01-2018 16:16","checkout":"30-01-2018 12:16","firstname":"asd","userid":"9"}';*/
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospitaladdupdate_guest_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospitalupdate_exit_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","mobileno":"9060588376","lastname":"sasdf"
		,"roomno":"a1234","firstname":"asd","userid":"9"}';*/
	   if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospitalupdate_exit_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospital_guest_setting_add()
	{
		//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","plan_id":"6","plan_hour":"4","is_enable":"1"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_guest_setting_add($decodeData);
		}
		echo json_encode($data);
	}
	
	 public function hospital_guest_setting()
	{
	//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_guest_setting($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospital_expired_user()
	{
		//$_POST['requestData'] = '{"location_id": "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_expired_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function hospital_delete_user()
	{
		//$_POST['requestData'] = '{"location_id": "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospital_delete_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_active_guestcount()
	{
		//$_POST['requestData'] = '{"location_id" : "40","location_uid":"101119"}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_active_guestcount($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_active_guestlist()
	{
		//$_POST['requestData'] = '{"location_id" : "40","location_uid":"101119","search":""}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_active_guestlist($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_editguest()
	{
	//	$_POST['requestData'] = '{"location_id" : "40","location_uid":"101119","id":"12"}';
		
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_editguest($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterpriseplan_list()
	{
		//$_POST['requestData'] = '{"location_id" : "40","location_uid":"101119"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospitalplan_list($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function enterpriseaddupdate_guest_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "40","location_uid":"101119","mobileno":"9060588380","planid":"6","lastname":"sasdf"
		,"employeeid":"xcvva123","usertype":"11","devices":"2","firstname":"asd","userid":""}';*/
		
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterpriseaddupdate_guest_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterpriseupdate_exit_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","mobileno":"9060588376","lastname":"sasdf"
		,"roomno":"a1234","firstname":"asd","userid":"9"}';*/
	   if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterpriseupdate_exit_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	 public function enterprise_guest_setting()
	{
	//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_guest_setting($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_guest_setting_add()
	{
		//$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","plan_id":"6","plan_hour":"4","is_enable":"1"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_guest_setting_add($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_expired_user()
	{
		//$_POST['requestData'] = '{"location_id": "40","location_uid":"101119"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_expired_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_usertype()
	{
		//$_POST['requestData'] = '{"location_id": "40","location_uid":"101119"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_usertype($decodeData);
		}
		echo json_encode($data);
	}
	
	public function enterprise_delete_user()
	{
		//$_POST['requestData'] = '{"location_id": "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_delete_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function institute_active_guestcount()
	{
		//$_POST['requestData'] = '{"location_id" : "42","location_uid":"101121"}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->institute_active_guestcount($decodeData);
		}
		echo json_encode($data);
	}
	
	public function institute_active_guestlist()
	{
		//$_POST['requestData'] = '{"location_id" : "42","location_uid":"101121","search":""}';
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->institute_active_guestlist($decodeData);
		}
		echo json_encode($data);
	}
	
	public function institute_editguest()
	{
		//$_POST['requestData'] = '{"location_id" : "42","location_uid":"101121","id":"10"}';
		
		 if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->institute_editguest($decodeData);
		}
		echo json_encode($data);
	}
	
	public function instituteplan_list()
	{
		//$_POST['requestData'] = '{"location_id" : "42","location_uid":"101121"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->hospitalplan_list($decodeData);
		}
		echo json_encode($data);
	}
	
	public function instituteaddupdate_guest_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "42","location_uid":"101121","mobileno":"9060588380","planid":"6","lastname":"sasdf"
		,"idcardid":"xcvva123","usertype":"11","devices":"2","firstname":"sandyc","userid":""}';*/
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->instituteaddupdate_guest_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function instituteupdate_exit_detail()
	{
		/*$_POST['requestData'] = '{"location_id" : "39","location_uid":"101118","mobileno":"9060588376","lastname":"sasdf"
		,"idcardid":"a1234","firstname":"asd","userid":"9"}';*/
	   if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->instituteupdate_exit_detail($decodeData);
		}
		echo json_encode($data);
	}
	
	public function institute_expired_user()
	{
		//$_POST['requestData'] = '{"location_id": "40","location_uid":"101119"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->institute_expired_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function institute_usertype()
	{
		//$_POST['requestData'] = '{"location_id": "40","location_uid":"101119"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->enterprise_usertype($decodeData);
		}
		echo json_encode($data);
	}
	
	public function institute_delete_user()
	{
		//$_POST['requestData'] = '{"location_id": "39","location_uid":"101118"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->institute_delete_user($decodeData);
		}
		echo json_encode($data);
	}
	
	public function fild_boy_login(){
		//$_POST['requestData'] = '{"isp_uid" : "101", "password":"9555532855", "username": "mayank@shouut.com"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->fild_boy_login($decodeData);
		}
		$this->log('fild_boy_login',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function user_current_kyc(){
		//$_POST['requestData'] = '{"isp_uid" : "101", "user_uid":"10100532"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->user_current_kyc($decodeData);
		}
		$this->log('user_current_kyc',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function add_kyc(){
		/*$_POST['requestData'] = '{"address_id_doc":"aadhar_card","corporate_id_doc":"electricity_bill","photo_id_doc":"aadhar_card","corporate_id_num":"","image":"KYC_PHOTO_CAPTURE.zip","user_uid":"10100532","isp_uid":"101","address_id_num":"","photo_id_num":"usbs","field_boy_uid":"54"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->add_kyc($decodeData);
		}
		$this->log('add_kyc',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function add_user_payment()
	{
		//$_POST['requestData'] = '{"isp_uid":"101","user_uid":"10100532","field_boy_uid":"54","date_time":"2018-05-15 15:48:30","type":"dd","amount":"1","bank_name":"bn","bank_address":"ba","bank_IFSE":"ifse","account_num":"uan","cheque_num":"","dd_num":"ddn","issued_date":"2018-5-15","image":"BILLING_PHOTO_ZIP.zip"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->add_user_payment($decodeData);
		}
		$this->log('add_user_payment',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
		
		
	}
	
	public function getactive_userslist(){
		//$_POST['requestData'] = '{"isp_uid":"101","field_boy_uid":"54"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->getactive_userslist($decodeData);
		}
		$this->log('getactive_userslist',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function get_fieldboy_task(){
		//$_POST['requestData'] = '{"isp_uid":"101","field_boy_uid":"54"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->get_fieldboy_task($decodeData);
		}
		//$this->log('get_fieldboy_task',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function get_fieldboy_ticket(){
		//$_POST['requestData'] = '{"isp_uid":"101","field_boy_uid":"54"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->get_fieldboy_ticket($decodeData);
		}
		//$this->log('get_fieldboy_task',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function fieldboy_ticket_status_update(){
		//$_POST['requestData'] = '{"isp_uid":"101","field_boy_uid":"54"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->locationportal_model->fieldboy_ticket_status_update($decodeData);
		}
		//$this->log('get_fieldboy_task',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
}

?>
