<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->model('api_model');
		//$this->load->library('mongo_db');
		include APPPATH . 'libraries/encdec_paytm.php';
	}

	public function index(){
		echo phpinfo();
		
	}
	public function log($function,$request, $responce){
		$data = $this->api_model->log($function, $request, $responce);
	}
	public function login(){
		//$_POST['requestData'] = '{"username": "admin@shouut.com", "password" : "shouut", "isp_uid" : "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->login($decodeData);
		}
		$this->log('login',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function isp_detail(){
		//$_POST['requestData'] = '{"isp_uid": "200"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->isp_detail($decodeData);
		}
		$this->log('isp_detail',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function slider_data(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->slider_data($decodeData);
		}
		$this->log('slider_data',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function live_usage (){
		
		//$_POST['requestData'] = '{"isp_uid": "100", "user_uid": "10000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->live_usage($decodeData);
		}
		$this->log('live_usage',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function promotions (){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->promotions($decodeData);
		}
		$this->log('promotions',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}

	public function account_detail(){
		//$_POST['requestData'] = '{"isp_uid": "001", "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->account_detail($decodeData);
		}
		$this->log('account_detail',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function update_mobile_send_otp(){
		//$_POST['requestData'] = '{"old_number": "9650896116" , "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_mobile_send_otp($decodeData);
		}
		$this->log('update_mobile_send_otp',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function update_mobile_verify_otp(){
		//$_POST['requestData'] = '{"new_number": "9650896118", "otp" : "5553" , "user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_mobile_verify_otp($decodeData);
		}
		$this->log('update_mobile_verify_otp',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	
	public function usage_logs(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "month" : "05 2017"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->usage_logs($decodeData);
		}
		$this->log('usage_logs',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	
	public function my_plan(){
		//$_POST['requestData'] = '{"user_uid": "10000002", "isp_uid" : "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->my_plan($decodeData);
		}
		$this->log('my_plan',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function recommended_plans(){
		//$_POST['requestData'] = '{"user_uid": "10000002", "isp_uid" : "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_plans($decodeData);
		}
		$this->log('recommended_plans',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function recommended_plans_app(){
		//$_POST['requestData'] = '{"user_uid": "10000002", "isp_uid" : "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_plans_app($decodeData);
		}
		$this->log('recommended_plans_app',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	
	public function active_topups(){
		//$_POST['requestData'] = '{"user_uid": "10000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->active_topups($decodeData);
		}
		$this->log('active_topups',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function recommended_speed_topup(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_speed_topup($decodeData);
		}
		$this->log('recommended_speed_topup',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function recommended_unaccoundancy_topup(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_unaccoundancy_topup($decodeData);
		}
		$this->log('recommended_unaccoundancy_topup',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function recommended_data_topup(){
		//$_POST['requestData'] = '{"user_uid": "10000002", "isp_uid": "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->recommended_data_topup($decodeData);
		}
		$this->log('recommended_data_topup',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function pending_bills(){
		//$_POST['requestData'] = '{"user_uid": "10100003"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->pending_bills($decodeData);
		}
		$this->log('pending_bills',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function previous_bills(){
		//$_POST['requestData'] = '{"user_uid": "10100003"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->previous_bills($decodeData);
		}
		$this->log('previous_bills',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function current_service_request(){
		//$_POST['requestData'] = '{"user_uid": "30000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->current_service_request($decodeData);
		}
		$this->log('current_service_request',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function service_type(){
		//$_POST['requestData'] = '{"isp_uid": "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->service_type($decodeData);
		}
		$this->log('service_type',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function generate_service_request(){
		//$_POST['requestData'] = '{"user_uid": "30000002", "ticket_type": "suspend_service", "ticket_priority" : "low", "ticket_description": "Suspend my service to 10 day"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->generate_service_request($decodeData);
		}
		$this->log('generate_service_request',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	
	public function update_password(){
		//$_POST['requestData'] = '{"user_uid": "30000002" ,"current_password": "shouut", "new_password": "shouut"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_password($decodeData);
		}
		$this->log('update_password',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function contact_us(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->contact_us($decodeData);
		}
		$this->log('contact_us',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function user_permissions(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->user_permissions($decodeData);
		}
		$this->log('user_permissions',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function about_us(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->about_us($decodeData);
		}
		$this->log('about_us',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function our_services(){
		//$_POST['requestData'] = '{"isp_uid": "300"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->our_services($decodeData);
		}
		$this->log('our_services',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function request_plan_topup(){
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->request_plan_topup($decodeData);
		}
		$this->log('request_plan_topup',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function plan_type_plan_all(){
		//$_POST['requestData'] = '{"isp_uid": "100", "plan_type" : "Data"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->plan_type_plan_all($decodeData);
		}
		$this->log('plan_type_plan_all',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function isp_all_plan_data(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->isp_all_plan_data($decodeData);
		}
		$this->log('isp_all_plan_data',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function update_email(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_email($decodeData);
		}
		$this->log('update_email',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function schedule_next_plan(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->schedule_next_plan($decodeData);
		}
		$this->log('schedule_next_plan',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function check_already_speed_topup_apply(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->check_already_speed_topup_apply($decodeData);
		}
		$this->log('check_already_speed_topup_apply',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function check_already_data_unacc_topup_apply(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->check_already_data_unacc_topup_apply($decodeData);
		}
		$this->log('check_already_data_unacc_topup_apply',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function get_topup_price(){
		//$_POST['requestData'] = '{"user_uid": "10000002"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->get_topup_price($decodeData);
		}
		$this->log('get_topup_price',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function calculate_speed_topup_price(){
		//$_POST['requestData'] = '{"topupid": "14", "week": "1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->calculate_speed_topup_price($decodeData);
		}
		$this->log('calculate_speed_topup_price',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function calculate_data_unacc_topup_price(){
		//$_POST['requestData'] = '{"topupid": "14", "week": "1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->calculate_data_unacc_topup_price($decodeData);
		}
		$this->log('calculate_data_unacc_topup_price',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function citrus_transection_detail(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->citrus_transection_detail($decodeData);
		}
		$this->log('citrus_transection_detail',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function citrus_credential(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->citrus_credential($decodeData);
		}
		$this->log('citrus_credential',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function add_topup_order_citrus(){
		//$_POST['requestData'] ='{"user_uid":"10000002","topup_id":"13","topuptype":"datatopup","addorder":"1","postdata":{"TxStatus":"SUCCESS","TxId":"594fe87aae8fe","TxRefNo":"CTX170625164745441798","pgTxnNo":"2932621202271761","pgRespCode":"0","TxMsg":"Transaction Successful","amount":"10.00","authIdCode":"999999","issuerRefNo":"717629861131","signature":"f9d8acd34392012b8737d86f94e4fda8a7b0b316","transactionId":"1950431","paymentMode":"CREDIT_CARD","TxGateway":"HDFC PG (Citrus Plus)","currency":"INR","maskedCardNumber":"555555XXXXXX4444","cardType":"MCRD","encryptedCardNumber":"","expiryMonth":"","expiryYear":"","cardHolderName":"gtt","txn3DSecure":"","eci":"","cardCode":"","txnType":"SALE","requestedCurrency":"","requestedAmount":"","dccCurrency":"","dccAmount":"","exchangeRate":"","dccOfferId":"","mcpCurrency":"","mcpAmount":"","offerExchangeRate":"","firstName":"hhh","lastName":"hhh","email":"sandeep@shouut.com","addressStreet1":"hhh","addressStreet2":"","addressCity":"hgh","addressState":"gr","addressCountry":"INDIA","addressZip":"777777","mobileNo":"9898888888","isCOD":"false","txnDateTime":"2017-06-25 22:15:01","impsMmid":"","impsMobileNumber":""},"total_days":"0","userid":"2"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->add_topup_order_citrus($decodeData);
		}
		$this->log('add_topup_order_citrus',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function update_bill_success_citrus(){
		//$_POST['requestData'] = '{"postdata":{"TxStatus":"SUCCESS","TxId":"5950ddaa6f225","TxRefNo":"CTX1706261009276981493","pgTxnNo":"7929108411571771","pgRespCode":"0","TxMsg":"Transaction Successful","amount":"3200.00","authIdCode":"999999","issuerRefNo":"717746340090","signature":"b7bc360b5dc166558f666e246aa06fb5f0b0a512","transactionId":"1950670","paymentMode":"CREDIT_CARD","TxGateway":"HDFC PG (Citrus Plus)","currency":"INR","maskedCardNumber":"555555XXXXXX4444","cardType":"MCRD","encryptedCardNumber":"","expiryMonth":"","expiryYear":"","cardHolderName":"sd","txn3DSecure":"","eci":"","cardCode":"","txnType":"SALE","requestedCurrency":"","requestedAmount":"","dccCurrency":"","dccAmount":"","exchangeRate":"","dccOfferId":"","mcpCurrency":"","mcpAmount":"","offerExchangeRate":"","firstName":"fas","lastName":"afsd","email":"a.mi.t.aboftest1@gmail.com","addressStreet1":"fasd","addressStreet2":"","addressCity":"fasd","addressState":"afds","addressCountry":"INDIA","addressZip":"544444","mobileNo":"9844444444","isCOD":"false","txnDateTime":"2017-06-26 15:39:26","impsMmid":"","impsMobileNumber":""},"user_uid":"10000002","addorder":"1","amount_payed":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->update_bill_success_citrus($decodeData);
		}
		$this->log('update_bill_success_citrus',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function isp_payment_gateway(){
		//$_POST['requestData'] = '{"isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->isp_payment_gateway($decodeData);
		}
		$this->log('isp_payment_gateway',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function paytm_checksum(){
		//$_POST['requestData'] = '{"isp_uid":"101", "orderid" : "123", "industry_type_id" : "Retail", "channel_id" : "WEB", "txn_amount" : "100", "mobile_no" : "", "call_back_url" : "abc.com", "email" : "deepak@shouut.com"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->paytm_checksum($decodeData);
		}
		$this->log('paytm_checksum',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function topup_payment(){
		//$_POST['requestData'] ='{"user_uid":"10000002","topup_id":"13","topuptype":"datatopup","addorder":"1","postdata":{"TxStatus":"SUCCESS","TxId":"594fe87aae8fe","TxRefNo":"CTX170625164745441798","pgTxnNo":"2932621202271761","pgRespCode":"0","TxMsg":"Transaction Successful","amount":"10.00","authIdCode":"999999","issuerRefNo":"717629861131","signature":"f9d8acd34392012b8737d86f94e4fda8a7b0b316","transactionId":"1950431","paymentMode":"CREDIT_CARD","TxGateway":"HDFC PG (Citrus Plus)","currency":"INR","maskedCardNumber":"555555XXXXXX4444","cardType":"MCRD","encryptedCardNumber":"","expiryMonth":"","expiryYear":"","cardHolderName":"gtt","txn3DSecure":"","eci":"","cardCode":"","txnType":"SALE","requestedCurrency":"","requestedAmount":"","dccCurrency":"","dccAmount":"","exchangeRate":"","dccOfferId":"","mcpCurrency":"","mcpAmount":"","offerExchangeRate":"","firstName":"hhh","lastName":"hhh","email":"sandeep@shouut.com","addressStreet1":"hhh","addressStreet2":"","addressCity":"hgh","addressState":"gr","addressCountry":"INDIA","addressZip":"777777","mobileNo":"9898888888","isCOD":"false","txnDateTime":"2017-06-25 22:15:01","impsMmid":"","impsMobileNumber":""},"total_days":"0","userid":"2"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->topup_payment($decodeData);
		}
		$this->log('topup_payment',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function bill_payment(){
		//$_POST['requestData'] = '{"postdata":{"TxStatus":"SUCCESS","TxId":"5950ddaa6f225","TxRefNo":"CTX1706261009276981493","pgTxnNo":"7929108411571771","pgRespCode":"0","TxMsg":"Transaction Successful","amount":"3200.00","authIdCode":"999999","issuerRefNo":"717746340090","signature":"b7bc360b5dc166558f666e246aa06fb5f0b0a512","transactionId":"1950670","paymentMode":"CREDIT_CARD","TxGateway":"HDFC PG (Citrus Plus)","currency":"INR","maskedCardNumber":"555555XXXXXX4444","cardType":"MCRD","encryptedCardNumber":"","expiryMonth":"","expiryYear":"","cardHolderName":"sd","txn3DSecure":"","eci":"","cardCode":"","txnType":"SALE","requestedCurrency":"","requestedAmount":"","dccCurrency":"","dccAmount":"","exchangeRate":"","dccOfferId":"","mcpCurrency":"","mcpAmount":"","offerExchangeRate":"","firstName":"fas","lastName":"afsd","email":"a.mi.t.aboftest1@gmail.com","addressStreet1":"fasd","addressStreet2":"","addressCity":"fasd","addressState":"afds","addressCountry":"INDIA","addressZip":"544444","mobileNo":"9844444444","isCOD":"false","txnDateTime":"2017-06-26 15:39:26","impsMmid":"","impsMobileNumber":""},"user_uid":"10000002","addorder":"1","amount_payed":"0"}';
		/*$_POST['requestData'] = '{
		"gateway_name": "EBS",
    "user_uid": "10100527",
    "addorder": "1",
    "amount_payed": "94",
    "postdata": {
        "PaymentId": "83716566",
        "TransactionId": "238847078",
        "ResponseCode": 0,
        "ResponseMessage": "Transaction Successful",
        "AccountId": "22206",
        "MerchantRefNo": "223",
        "Amount": "94.00",
        "DateCreated": "2017-09-22",
        "Description": "Test Transaction",
        "Mode": "TEST",
        "IsFlagged": "No",
        "BillingName": "jvs",
        "BillingAddress": "North Mada Street",
        "BillingCity": "Chennai",
        "BillingState": "Tamilnadu",
        "BillingPostalCode": "600019",
        "BillingCountry": "IND",
        "BillingPhone": "9015137090",
        "BillingEmail": "vkvjcj@gmail.com",
        "DeliveryName": "Test_Name",
        "DeliveryAddress": "North Mada Street",
        "DeliveryCity": "Chennai",
        "DeliveryState": "Tamilnadu",
        "DeliveryPostalCode": "600019",
        "DeliveryCountry": "IND",
        "DeliveryPhone": "01234567890",
        "PaymentStatus": "Authorized",
        "PaymentMode": "Credit Card",
        "SecureHash": "0386FDAB0B1C950EE01FBEFA5D985473E4AC9EC0"
    }
}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$this->log('bill_payment',$_POST['requestData'], '');//call log function
			$this->log('bill_payment_before_success',$_POST['requestData'], "it is only request");//call log function
			$data = $this->api_model->bill_payment($decodeData);
		}
		$this->log('bill_payment',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function usage_graph_date(){
		//$_POST['requestData'] = '{"user_uid":"10100003"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->usage_graph_date($decodeData);
		}
		$this->log('usage_graph_date',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function usage_graph(){
		//$_POST['requestData'] = '{"user_uid":"10100003", "from": "2018-01-01", "to" : "2018-03-01"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->usage_graph($decodeData);
		}
		$this->log('usage_graph',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	public function speed_graph(){
		//$_POST['requestData'] = '{"user_uid":"crmservicesindiapvtltd", "date_range": "today"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->api_model->speed_graph($decodeData);
		}
		$this->log('speed_graph',$_POST['requestData'], $data);//call log function
		echo json_encode($data);
	}
	
	public function get_logs()
	{
		$table="wifi_cp3_users";
		$where=array("mobileno"=>"9560588376");
		 $this->api_model->get_logs($table,$where);
	}
	
	public function sync_guest_session()
	{
			//$table="entr_guest_setting";
		$where=array();
		$this->api_model->sync_guest_session($table,$where);
	}
}
