<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Setup extends CI_Controller {
    
    public function __construct(){
		parent::__construct();
                $this->load->model('setup_model');
		
	}
        
        public function  add_plan()
        {
          /*  $_POST['requestData'] = '{"authKey":"122334","plan":{"plan_name"
:"Dp1","plan_desc":"Dp1","plan_type":"DP","dwnload_rate":"1024","upload_rate":"1024","timelimit":"5"
,"data_limit":"2"},"burst_mode":1,"burst_data":{"burst_download_speed":"500","burst_upload_speed":"100",
"burst_download_threshold":"500","burst_upload_threshold":"100","burst_time":"500","burst_priority":"100"}}';*/

$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(isset($decodeData->authKey))
                        {
							
			$response = $this->setup_model->authentication($decodeData->authKey);
			
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->add_plan($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		} 

            echo json_encode($data);
            
        }
        
        public function add_nas()
        {
        // $_POST['requestData'] = '{"authKey":"122334","nasname":"test","nasip":"192.168.0.1","nassecret":"testing123"}';
         $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(isset($decodeData->authKey))
                        {
			$response = $this->setup_model->authentication($decodeData->authKey);
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->add_nas($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		}

            echo json_encode($data);
         
        }
        
        public function register_user()
        {
        /* $_POST['requestData'] = '{"authKey":"122334","mobileno":"9560588376","macid":"00:ff:fff","firstname":"test","lastname":"tyagi","email":"testing@mail.com"}';*/
        $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                        if(isset($decodeData->authKey))
                        {
			//echo "========>>>".$decodeData->userId;
			$response = $this->setup_model->authentication($decodeData->authKey);
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->register_user($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		}

            echo json_encode($data);
         
       
        }
        
         public function data_balance()
        {
        // $_POST['requestData'] = '{"authKey":"122334","mobileno":"9560588376"}';
        $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                        if(isset($decodeData->authKey))
                        {
			//echo "========>>>".$decodeData->userId;
			$response = $this->setup_model->authentication($decodeData->authKey);
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->data_balance($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		}

            echo json_encode($data);
         
       
        }
        
         public function data_expiry()
        {
        // $_POST['requestData'] = '{"authKey":"122334","mobileno":"00:ff:fff"}';
        $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                        if(isset($decodeData->authKey))
                        {
			//echo "========>>>".$decodeData->userId;
			$response = $this->setup_model->authentication($decodeData->authKey);
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->data_expiry($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		}

            echo json_encode($data);
         
       
        }
        
        public function recharge_account()
        {
           
         //   $_POST['requestData'] = '{"authKey":"122334","mobileno":"9560588376","planid":"38"}';
            
             $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                        if(isset($decodeData->authKey))
                        {
			//echo "========>>>".$decodeData->authKey;
			$response = $this->setup_model->authentication($decodeData->authKey);
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->recharge_account($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		}

            echo json_encode($data);
            
        }
	
	public function login()
	{
	    //$_POST['requestData'] = '{"authKey":"122334","macid":"00:ff:fff" ,"mobileno":"9560588376"}';
            
             $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                        if(isset($decodeData->authKey))
                        {
			//echo "========>>>".$decodeData->authKey;
			$response = $this->setup_model->authentication($decodeData->authKey);
                        $responsecode=1;
			if($response['resultCode'] == 1){
                           
				$data = $this->setup_model->login($decodeData,$response['authkey_id']);
			}else{
                            
				$data=$response;
				
			}
                        }
                        else{
                            $data['resultCode'] = '0';
			$data['resultMsg'] = 'No authKey provided';
                        }
		}

            echo json_encode($data);
	}
        
		public function add_location(){
		//$_POST['requestData'] = '{"authKey": "abcd", "location_type":"1","location_name":" a Client  Location","geo_address":"Delhi, India","placeid":"ChIJL_P_CXMEDTkRw0ZdG-0GVvw","lat":"28.661898","long":"77.227396","address1":"fasd","address2":"fasd","pin":"333344","state":"9","city":"325","zone":"17","location_id":" 100123","contact_person_name":"fasd","email_id":"prashant@shouut.com","mobile_no":"8777777777"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$authKey = '';
			if(isset($decodeData->authKey)){
				$authKey = $decodeData->authKey;
			}
			$authentication = $this->setup_model->authentication($authKey);
			if($authentication['resultCode'] == '1'){
				$authkey_id = $authentication['authkey_id'];
				$data = $this->setup_model->add_location($decodeData, $authkey_id);
			}
			else{
				$data = $authentication;
			}
			
		}
		echo json_encode($data);
	}
	public function get_locations(){
		//$_POST['requestData'] = '{"authKey": "abcd"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$authKey = '';
			if(isset($decodeData->authKey)){
				$authKey = $decodeData->authKey;
			}
			$authentication = $this->setup_model->authentication($authKey);
			if($authentication['resultCode'] == '1'){
				$authkey_id = $authentication['authkey_id'];
				$data = $this->setup_model->get_locations($authkey_id);
			}
			else{
				$data = $authentication;
			}
		}
		echo json_encode($data);

	}
	public function nas_plas_assoc(){
		//$_POST['requestData'] = '{"authKey": "abcd", "location_uid": "121161", "planId": "27", "nasId": "29"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$authKey = '';
			if(isset($decodeData->authKey)){
				$authKey = $decodeData->authKey;
			}
			$authentication = $this->setup_model->authentication($authKey);
			if($authentication['resultCode'] == '1'){
				$authkey_id = $authentication['authkey_id'];
				$data = $this->setup_model->nas_plas_assoc($decodeData,$authkey_id);
			}
			else{
				$data = $authentication;
			}
		}
		echo json_encode($data);

	}
	
	public function configure_router(){
		//$_POST['requestData'] = '{"authKey": "abcd", "router_ip": "14.102.15.253", "router_user" : "ispadmin", "router_password": "ispadmin", "router_port" : "", "location_uid": "121161", "router_type": "wapac", "ssid": "FREEWIFI"}';
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$authKey = '';
			if(isset($decodeData->authKey)){
				$authKey = $decodeData->authKey;
			}
			$authentication = $this->setup_model->authentication($authKey);
			if($authentication['resultCode'] == '1'){
				$authkey_id = $authentication['authkey_id'];
				$data = $this->setup_model->configure_router($decodeData,$authkey_id);
			}
			else{
				$data = $authentication;
			}
		}
		echo json_encode($data);

	}
		
	

        
}