<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nodeapi extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		    $this->load->library('mongo_db');
		$this->load->model('nodeapi_model');
		
	}
	private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	
		public function log($function,$request, $responce){
		$data = $this->nodeapi_model->log($function, $request, $responce);
	}

	public function index(){
		echo "home page";
		
		
	}
	
	public function get_location_frommac()
	{

		$postdata = file_get_contents("php://input");
		
		$data = array();
		if(count($postdata) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($postdata);
			//echo "<pre>"; print_R($postdata); die;
			$data = $this->nodeapi_model->get_location_frommac($decodeData);

		}
		$this->log('get_location_frommac',$postdata, $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function check_emailexist()
	{
		$postdata = file_get_contents("php://input");
		
		$data = array();
		if(count($postdata) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($postdata);
			//echo "<pre>"; print_R($postdata); die;
			$data = $this->nodeapi_model->check_emailexist($decodeData);

		}
		$this->log('check_emailexist',$postdata, $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	
	
}
