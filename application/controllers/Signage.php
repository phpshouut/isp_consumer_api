<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Signage extends CI_Controller {
    private $authkey = 'bcaf75d0a47bad12faa1272e3d2e68a5==';
    
    public function __construct(){
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
        $this->load->model('Signage_model', 'signage_model');
    }
	
	public function log($function,$request, $responce){
		$data = $this->signage_model->log($function, $request, $responce);
	}
    

    public function signage_login(){
		
		
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("signage_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->signage_model->signage_login($decodeData);
                }
            }
        }
		$this->log('signage_login',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
	
	
	public function signage_offer(){
		
		
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("signage_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->signage_model->signage_offer($decodeData);
                }
            }
        }
		$this->log('signage_offer',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
	
	public function signage_sync(){
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("signage_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->signage_model->signage_sync($decodeData);
                }
            }
        }
		$this->log('signage_sync',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
    

}

?>