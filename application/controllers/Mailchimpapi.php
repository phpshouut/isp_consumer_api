<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailchimpapi extends CI_Controller {

	public function __construct(){
		
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		
		$this->load->model('mailchimpapi_model');
		
	}

	public function index(){
		echo "home page";
		
	}
	
	public function cafe_mailchimpsync()
	{
		$this->mailchimpapi_model->cafe_mailchimpsync();
	}
	
	public function retail_mailchimpsyc()
	{
		$this->mailchimpapi_model->retail_mailchimpsyc();
	}
	
	public function hotel_mailchimpsync()
	{
		$this->mailchimpapi_model->hotel_mailchimpsync();
	}
	
	public function cafe_group_mailchimpsyc(){
		$this->mailchimpapi_model->cafe_group_mailchimpsyc();
	}
	public function cafe_push_msg(){
		$this->mailchimpapi_model->cafe_push_msg();
	}
	
	public function promenade_previousdata_mailchimpsync(){
		$this->mailchimpapi_model->promenade_previousdata_mailchimpsync();
	}
	
	
}
