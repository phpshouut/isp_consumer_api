<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Naswifi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
                $this->load->model('naswifi_model');
                $this->load->model('planpublicwifi_model');
                 
		
	}
	public function index(){
		//print_r($_SESSION);die;
		//echo $_SERVER['DOCUMENT_ROOT'];
		//$this->load->view('welcome_message');
		echo $_SERVER['DOCUMENT_ROOT'];

	}
	
	public function listing_nas(){
		//$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->listing_nas($decodeData);
		}
                
              echo json_encode($data);
                
	}
	
	public function nas_type(){
		//$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->nas_type($decodeData);
		}
              echo json_encode($data);
                
	}
        
        public function getstatename(){
		//$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->planpublicwifi_model->getstatename($decodeData->stateid);
		}
                
              echo json_encode($data);
                
	}
        
         public function getcityname(){
	//	$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->planpublicwifi_model->getcityname($decodeData->stateid,$decodeData->cityid);
		}
                
              echo json_encode($data);
                
	}
        
         public function getzonename(){
		//$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->planpublicwifi_model->getzonename($decodeData->zoneid);
		}
                
              echo json_encode($data);
                
	}
        
         public function viewall_search_results(){
		/*$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"search_user":"delh","state":"all","city"
:"all","zone":"all"}}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->viewall_search_results($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
         public function checknas_exist(){
		//$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"nasname":"shouuthq"}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->checknas_exist($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
          public function checknasip_exist(){
		/*$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"search_user":"delh","state":"all","city"
:"all","zone":"all"}}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->checknasip_exist($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
          public function ping(){
		/*$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"search_user":"delh","state":"all","city"
:"all","zone":"all"}}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->ping($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
              public function add_nas_data(){
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->add_nas_data($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
          public function edit_nas(){
	//	$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"nasid":"6"}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->edit_nas($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
         public function add_zonedata(){
	//	$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"nasid":"6"}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->add_zonedata($decodeData);
		}
                
              echo json_encode($data);
                
	}
        
         public function add_citydata(){
	//	$_POST['requestData'] = '{"isp_uid":"200","dept_id":0,"super_admin":"1","postdata":{"city_add":"dp","add_city_state":"13"}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->add_citydata($decodeData);
		}
                
              echo json_encode($data);
                
	}
	
	public function add_nas_data_dynamic(){
		//$_POST['requestData'] = '{"isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->add_nas_data_dynamic($decodeData);
		}
                
              echo json_encode($data);
                
	}
	
	public function add_apmac_country()
	{
		//$_POST['requestData'] = '{"macId":"b8:27:eb:79:ee:eb","country_id":"101","added_on":"2018-03-20 12:27:13"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
                      $data = $this->naswifi_model->add_apmac_country($decodeData);
		}
                
              echo json_encode($data);
	}
      
        
        
        
     
}

?>
