<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(0);
class Location extends CI_Controller {
var $api_url;
var $isp_uid;

	public function __construct(){
		parent :: __construct();
		$path = FCPATH;
		if($path != ''){
			$path = $path.'assets';
				// change permission 
			$fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
			if( $fldrperm != 0777) {
				$output = exec("sudo chmod -R 0777 \"$path\"");
			}
		}
		$this->load->model('location_model');
		$this->load->model('permission_model');
		$permission = $this->permission_model->isp_portal_permission();
		if($permission['isp_publicwifi_permission'] == '0'){
			$this->session->unset_userdata('isp_session');
			$url = base_url();
			$url = str_replace("/publicwifi/","",$url);
			redirect($url);
		}
		$this->load->model('plan_model');
		if(!isset($this->session->userdata['isp_session']['isp_uid'])){
			redirect(base_url());
		}
		$this->isp_uid = $this->session->userdata['isp_session']['isp_uid'];
		$this->api_url = APIPATH."publicwifi/";
		$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
		$this->load->library('s3',$paramsthree);
		//$this->api_url = "http://103.206.180.82/isp_consumer_api/publicwifi/";
		//api function changes
		//publicwifi_model: location_autogenerate_id, add_hotel_captive_portal,nas_setup_list,plan_list
			//add new function: plan_detail,check_plan_already_attached, save_plan
	}

	public function index(){
		
		//$data['locations'] = $this->location_model->location_dashboard();
		$data['locations'] = json_decode($this->location_model->location_dashboard_new());
		//echo "<pre>";print_r($data);die;
		$this->load->view('location/location_dashboard',$data);
	}
	
	public function location_dashboard_search(){
		$search_val = $this->input->post("search_val");
		$data = $this->location_model->location_dashboard_search($search_val);
		echo $data;
	}
	
	/*public function location_list($type = NULL){
		if(isset($_POST['location_type_filter']) && $_POST['location_type_filter'] != ''){
			$type = $_POST['location_type_filter'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
		$search_type_filter = '';
		if(isset($_POST['search_type_filter']) && $_POST['search_type_filter'] != ''){
			$search_type_filter = $_POST['search_type_filter'];
		}
		
			$data['locations'] = $this->location_model->location_dashboard();
			$data['location_list'] = $this->location_model->location_list($type, $state_id, $city_id,$search_type_filter);
			$data['filtertype'] = $type;
			$this->load->view('location/location_view',$data);
		
	}*/
	public function location_list($type = NULL){
		if(isset($_POST['location_type_filter']) && $_POST['location_type_filter'] != ''){
			$type = $_POST['location_type_filter'];
		}
		if($type == ''){
			redirect(base_url()."location");
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
		$search_type_filter = '';
		if(isset($_POST['search_type_filter']) && $_POST['search_type_filter'] != ''){
			$search_type_filter = $_POST['search_type_filter'];
		}
		$online_filter = '';
		if(isset($_POST['online_filter']) && $_POST['online_filter'] != ''){
			$online_filter = $_POST['online_filter'];
		}
		$limit = 20;
		$offset = 0;
		//$data['locations'] = $this->location_model->location_dashboard();
		$data['locations'] = json_decode($this->location_model->location_dashboard_new_listview($type, $state_id, $city_id,$search_type_filter));
		$data['location_list'] = $this->location_model->location_list_new($type, $state_id, $city_id,$search_type_filter,$limit,$offset,$online_filter);
		$data['filtertype'] = $type;
		$data['offline_offline_filter'] = $online_filter;
		$this->load->view('location/location_view',$data);
		
	}
	public function location_list_more(){
		$type = '';
		if(isset($_POST['location_type_filter']) && $_POST['location_type_filter'] != ''){
			$type = $_POST['location_type_filter'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
		$search_type_filter = '';
		if(isset($_POST['search_type_filter']) && $_POST['search_type_filter'] != ''){
			$search_type_filter = $_POST['search_type_filter'];
		}
		$limit = '20';
		if(isset($_POST['limit']) && $_POST['limit'] != ''){
			$limit = $_POST['limit'];
		}
		$offset = '0';
		if(isset($_POST['offset']) && $_POST['offset'] != ''){
			$offset = $_POST['offset'];
		}
		$online_filter = '';
		if(isset($_POST['online_filter']) && $_POST['online_filter'] != ''){
			$online_filter = $_POST['online_filter'];
		}
		$data = $this->location_model->location_list_new($type, $state_id, $city_id,$search_type_filter,$limit,$offset,$online_filter);
		echo json_encode($data);
	}
	
	public function add_location_view($type = NULL){
		$data['locationid'] = $this->location_model->add_location_view();
		$data['location_type'] = $type;
		//print_r($data);die;
		$this->load->view('location/add_location',$data);
	}
	
	public function city_list(){
		$state_id = $this->input->post("state_id");
		$city_list = $this->location_model->city_list($state_id);
		echo $city_list;
	}
	public function zone_list(){
		$city_id = $this->input->post("city_id");
		$zone_list = $this->location_model->zone_list($city_id);
		echo $zone_list;
	}
	
	public function add_location(){
		$data = $this->location_model->add_location();
		echo $data->created_location_id;
	}
	public function add_captive_portal(){
	
		$data = $this->location_model->add_captive_portal();
		//echo $data->created_cp_id;
		echo $data->error;
	}
	public function change_location_status(){
		$data = $this->location_model->change_location_status();
		echo $data;
	}
	public function edit_location(){
		
		$location_id = $id = $this->input->get('id');
		$data['locationid'] = $location_id;
		$data['edit_location_data'] = $this->location_model->edit_location_data($location_id);
		//echo "<pre>";print_r($data);die;
		$this->load->view('location/edit_location',$data);
	}
	public function location_voucher_list(){
		$location_uid = $this->input->post("location_uid");
		$data = $this->location_model->location_voucher_list($location_uid);
		echo $data;
	}
	public function add_location_vouchers(){
		$data = $this->location_model->add_location_vouchers();
		echo $data;
	}
	public function delete_voucher(){
		$voucher_id = $this->input->post("voucher_id");
		$data = $this->location_model->delete_voucher($voucher_id);
		echo $data;
	}
	public function update_location_voucher_status(){
		$data = $this->location_model->update_location_voucher_status();
		echo $data;
	}
	public function nas_list(){
		$router_type = $this->input->post("router_type");
		$data = $this->location_model->nas_list($router_type);
		echo $data;
	}
	public function update_other_router_with_nas_location(){
		$data = $this->location_model->update_other_router_with_nas_location();
		echo $data;
	}
	public function nas_setup_list(){
		$data = $this->location_model->nas_setup_list();
		echo $data;
	}
	public function add_location_access_point(){
		$data = $this->location_model->add_location_access_point();
		echo $data;
	}
	public function location_access_point_list(){
		$location_uid = $this->input->post("location_uid");
		$data = $this->location_model->location_access_point_list($location_uid);
		echo $data;
	}
	public function delete_access_point(){
		$access_point_id = $this->input->post("access_point_id");
		$data = $this->location_model->delete_access_point($access_point_id);
		echo $data;
	}
	public function add_zone(){
		$data = $this->location_model->add_zone();
		echo $data;
	}
	public function add_city(){
		$data = $this->location_model->add_city();
		echo $data;
	}
	
	public function check_cp_selected_for_location(){
		$location_uid = $this->input->post('location_uid');
		$data = $this->location_model->check_cp_selected_for_location($location_uid);
		echo $data;
	}
	
	public function start_configuration(){
		$this->load->library('routerlib');
		$router_id = $this->input->post('router_ip');
		$router_user = $this->input->post('router_user');
		$router_password = $this->input->post('router_password');
		$router_port = $this->input->post('router_port');
		$nasid = $this->input->post('nasid');
		$location_id = $this->input->post('location_id');
		
		if($router_port != ''){
			$this->router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port") or die("couldn't connectto router");
		}else{
			$this->router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password") or die("couldn't connectto router");	
		}
		
		// get nas short name from nas table
		$nas_detail = $this->location_model->get_nas_detail($nasid);
		$username = $nas_detail->username;
		$nasaddress = $nas_detail->nasaddress;
		$is_dynamic = $nas_detail->is_dynamic;
		$secret = '';
		if(isset($nas_detail->secret)){
			$secret = $nas_detail->secret;
		}
		$this->session->set_userdata('client_ap_configuration',array(
			'nasid' => $nasid,
			'is_dynamic' => $is_dynamic,
			'nasname' => $nasaddress,
			'username' => $username,
			'secret' => $secret,
			'location_id' => $location_id,
			'router_id' => $router_id,
			'router_user' => $router_user,
			'router_password' => $router_password,
			'router_port' => $router_port,
		));
		
	}
	
	public function start_configuration1(){
		$router_type_wap = $this->input->post('router_type_wap');
		
		$data = $this->session->userdata('client_ap_configuration');
		$wan_connection_type = '';
		$data['wan_connection_type']  = $wan_connection_type;
		
		$data['router_type_wap']  = $router_type_wap;
		// get cp path from sht_isp_admin table
		$get_path = $this->location_model->get_isp_url('','for_mikrotik');
		$data['cp_path']  = $get_path->url;
		$data['subdomain_ip']  = $get_path->subdomain_ip;
		$data['main_ssid']  = $get_path->main_ssid;
		$data['guest_ssid']  = $get_path->guest_ssid;
		$this->session->set_userdata('client_ap_configuration', $data);
		
		$router_id = $this->session->userdata['client_ap_configuration']['router_id'];
		$router_user = $this->session->userdata['client_ap_configuration']['router_user'];
		$router_password = $this->session->userdata['client_ap_configuration']['router_password'];
		$router_port = $this->session->userdata['client_ap_configuration']['router_port'];
		$this->load->library('routerlib');
		if($router_port != ''){
			$conn = $this->router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password", "$router_port") or die("couldn't connectto router");
		}else{
			$conn = $this->router_conn = RouterOS::connect("$router_id", "$router_user", "$router_password") or die("couldn't connectto router");	
		}
		
		// check router type select and connected are match
		$match = $this->check_router_selected_matched($router_type_wap);
		
		echo $match;
		
	}
	public function check_router_selected_matched($router_type){
		$data = $this->location_model->check_router_selected_matched($router_type);
		return $data;
	}
	public function start_configuration2(){
		
		$data = $this->location_model->done_configuration();
		echo $data;
		
	}
	

	
	
	public function reset_router(){
		$router_type = $this->input->post('router_type');
		$data = $this->location_model->reset_router($router_type);
		echo $data;
	}
	public function check_wap_router_reset_porperly(){
		$router_type = $this->input->post('router_type');
		$data = $this->location_model->check_wap_router_reset_porperly($router_type);
		echo $data;
	}
	public function plan_list(){
		$plan_type = $this->input->post('plan_type');
		$plan_id = '';
		if($this->input->post('plan_id')){
			$plan_id = $this->input->post('plan_id');
		}
		$data = $this->location_model->plan_list($plan_type,$plan_id);
		echo $data;
	}
	public function add_hotel_captive_portal(){
	
		$data = $this->location_model->add_hotel_captive_portal();
		echo $data->created_cp_id;
		
	}
	public function ap_location_list(){
		$ap_location_type = $this->input->post("ap_location_type");
		$ap_location_list = $this->location_model->ap_location_list($ap_location_type);
		echo $ap_location_list;
	}
	public function add_ap_location(){
		$data = $this->location_model->add_ap_location();
		echo $data;
	}
	public function is_location_proper_setup(){
		$location_uid = $this->input->post('location_uid');
		$data = $this->location_model->is_location_proper_setup($location_uid);
		echo $data;
	}
	public function onehop_get_network_detail(){
		$location_uid = $this->input->post('location_uid');
		$data = $this->location_model->onehop_get_network_detail($location_uid);
		echo $data;
	}
	public function onehop_get_network_create(){
		$data = $this->location_model->onehop_get_network_create();
		echo $data;
	}
	public function onehop_add_apmac(){
		$data = $this->location_model->onehop_add_apmac();
		echo $data;
	}
	public function onehop_add_ssid(){
		$data = $this->location_model->onehop_add_ssid();
		echo $data;
	}
	public function onehop_get_ssid_detail(){
		$data = $this->location_model->onehop_get_ssid_detail();
		echo $data;
	}
	public function get_isp_url(){
		$location_id = $this->input->post("created_location_id");
		$data = $this->location_model->get_isp_url($location_id);
		echo json_encode($data);
	}
	public function onehop_delete_ssid(){
		$data = $this->location_model->onehop_delete_ssid();
		echo $data;
	}
	public function plan_detail(){
		$data = $this->location_model->plan_detail();
		echo $data;
	}
	public function check_plan_already_attached(){
		$plan_id = $this->input->post('plan_id');
		$data = $this->location_model->check_plan_already_attached($plan_id);
		echo $data;
	}
	public function save_plan(){
		$data = $this->location_model->save_plan();
		echo $data;
	}
	public function terms_of_use(){
		$location_uid = $this->input->post("location_uid");
		$data = $this->location_model->terms_of_use($location_uid);
		echo $data;
	}
	public function update_terms_of_use(){
		$location_uid = $this->input->post("location_uid");
		$text_value = $this->input->post("text_value");
		$data = $this->location_model->update_terms_of_use($location_uid,$text_value);
		echo $data;
	}
	public function current_image_path(){
		$location_uid = $this->input->post('location_uid');
		$data = $this->location_model->current_image_path($location_uid);
		echo $data;
	}
	public function current_cp_path(){
		$location_uid = $this->input->post('location_uid');
		$data = $this->location_model->current_cp_path($location_uid);
		echo $data;
	}
	public function get_location_ap_move_where(){
		$data = $this->location_model->get_location_ap_move_where();
		echo $data;
	}
	public function onehop_move_ap(){
		$onehop_move_macid = $this->input->post('onehop_move_macid');
		$onehop_move_location_uid = $this->input->post('onehop_move_location_uid');
		$data = $this->location_model->onehop_move_ap($onehop_move_macid,$onehop_move_location_uid);
		echo $data;
	}
	public function ping_mikrotik_ip_address(){
		$ip_address = $this->input->post('ip_address');
		$data = $this->location_model->ping_mikrotik_ip_address($ip_address);
		echo $data;
	}
	public function ping_onehop_ap(){
		$location_uid = $this->input->post('location_uid');
		$data = $this->location_model->ping_onehop_ap($location_uid);
		echo $data;
	}
	public function ping_onehop_ap_wise(){
		$location_uid = $this->input->post('location_uid');
		$macid = $this->input->post('macid');
		$data = $this->location_model->ping_onehop_ap_wise($location_uid,$macid);
		echo $data;
	}
	
	public function remove_slider_image(){
		$location_uid = $this->input->post('location_uid');
		$slider_type = $this->input->post('slider_type');
		$data = $this->location_model->remove_slider_image($location_uid,$slider_type);
		echo $data;
	}
	public function delete_wifi_location(){
		$data = $this->location_model->delete_wifi_location();
		echo $data;
	}
	
	
	
	public function content_builder_list(){
		$data = $this->location_model->content_builder_list();
		echo $data;
	}
	public function add_content_builder(){
	
		$data = $this->location_model->add_content_builder();
		echo $data->code;
		
	}
	public function add_main_category(){
		$data = $this->location_model->add_main_category();
		echo $data->created_category_id;
	}
	public function add_sub_category(){
		$data = $this->location_model->add_sub_category();
		echo $data->created_category_id;
	}
	public function add_content(){
		$data = $this->location_model->add_content();
		echo $data->created_category_id;
	}
	
	public function delete_content(){
		$content_id = $this->input->post("content_id");
		$data = $this->location_model->delete_content($content_id);
		echo $data;
	}
	public function delete_sub_category(){
		$sub_category_id = $this->input->post("sub_category_id");
		$data = $this->location_model->delete_sub_category($sub_category_id);
		echo $data;
	}
	public function delete_main_category(){
		$main_category_id = $this->input->post("main_category_id");
		$data = $this->location_model->delete_main_category($main_category_id);
		echo $data;
	}
	public function download_microtic_dynamic_ip_script(){
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
		header('Content-Disposition: attachment;filename="assets/mickrotik_dynamic_ip_script.pdf"');
		header('Cache-Control: max-age=0');
		ob_clean();
		flush();
		readfile('assets/mickrotik_dynamic_ip_script.pdf'); exit;

	}
	public function get_content_poll_survey(){
		$data = $this->location_model->get_content_poll_survey();
		echo $data;
	}
	public function add_content_poll(){
		$data = $this->location_model->add_content_poll();
		echo $data->created_category_id;
	}
	
	public function update_content_poll(){
		$data = $this->location_model->update_content_poll();
		echo $data->created_category_id;
	}
	
	public function add_content_survey(){
		$data = $this->location_model->add_content_survey();
		echo $data->created_category_id;
	}
	public function update_content_survey(){
		$data = $this->location_model->update_content_survey();
		echo $data->created_category_id;
	}
	
	public function add_content_pdf(){
		$data = $this->location_model->add_content_pdf();
		echo $data->created_category_id;
	}
	public function add_content_upload(){
		$data = $this->location_model->add_content_upload();
		echo $data->created_category_id;
	}
	public function get_onehop_existing_network(){
		$data = $this->location_model->get_onehop_existing_network();
		echo $data;
	}
	public function use_existing_onehop_network(){
		$data = $this->location_model->use_existing_onehop_network();
		echo $data;
	}
	public function add_content_wiki(){
		$data = $this->location_model->add_content_wiki();
		echo $data->is_created;
	}
	
	public function raspberry_ping_sync_info(){
		$data = $this->location_model->raspberry_ping_sync_info();
		echo json_encode($data);
	}
	
	public function location_list_export_to_excel(){
		$type = '';
		if(isset($_POST['location_type_filter']) && $_POST['location_type_filter'] != ''){
			$type = $_POST['location_type_filter'];
		}
		$state_id = '';
		if(isset($_POST['state_id_filter']) && $_POST['state_id_filter'] != ''){
			$state_id = $_POST['state_id_filter'];
		}
		$city_id = '';
		if(isset($_POST['city_id_filter']) && $_POST['city_id_filter'] != ''){
			$city_id = $_POST['city_id_filter'];
		}
		$search_type_filter = '';
		if(isset($_POST['search_type_filter']) && $_POST['search_type_filter'] != ''){
			$search_type_filter = $_POST['search_type_filter'];
		}
		$online_filter = '';
		if(isset($_POST['online_filter']) && $_POST['online_filter'] != ''){
			$online_filter = $_POST['online_filter'];
		}
		$result = $this->location_model->location_list_export_to_excel($type, $state_id, $city_id,$search_type_filter,$online_filter);
		echo $result;
	}
	
	public function download_location_list_excel(){
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
		header('Content-Disposition: attachment;filename="assets/location_list.xlsx"');
		header('Cache-Control: max-age=0');
		ob_clean();
		flush();
		readfile('assets/location_list.xlsx'); exit;
	
	}
	public function get_location_group(){
		$location_type = $_POST['location_type'];
		$data = $this->location_model->get_location_group($location_type);
		echo json_encode($data);
	}
	public function create_location_group(){
		$data = $this->location_model->create_location_group();
		echo $data;
	}
	public function add_location_to_group(){
		$data = $this->location_model->add_location_to_group();
		echo $data;
	}
	public function delete_loc_group(){
		$data = $this->location_model->delete_loc_group();
		echo $data;
	}
	public function update_location_group(){
		$data = $this->location_model->update_location_group();
		echo $data;
	}
	public function get_group_location_list(){
		$data = $this->location_model->get_group_location_list();
		echo json_encode($data);
	}
	
	public function add_otp_sms(){
		$data = $this->location_model->add_otp_sms();
		echo $data;
	}
	
	public function sms_list(){
		$location_uid = $this->input->post("location_uid");
		$data = $this->location_model->sms_list($location_uid);
		echo $data;
	}
	
	public function offline_organisation_list(){
		$data = $this->location_model->offline_organisation_list();
		echo $data;
	}
	
	public function add_offline_vm_organisation(){
		$data = $this->location_model->add_offline_vm_organisation();
		echo $data->resultCode;
	}
	
	public function vm_offline_department_list(){
		$data = $this->location_model->vm_offline_department_list();
		echo $data;
	}
	public function add_offline_vm_department(){
		$data = $this->location_model->add_offline_vm_department();
		echo $data->resultCode;
	}
	
	public function vm_offline_employee_list(){
		$data = $this->location_model->vm_offline_employee_list();
		echo $data;
	}
	
	public function add_vm_offline_employee(){
		$data = $this->location_model->add_vm_offline_employee();
		echo $data->resultCode;
	}
	public function delete_offline_vm_organisation(){
		$organisation_id = $this->input->post("organisation_id");
		$data = $this->location_model->delete_offline_vm_organisation($organisation_id);
		echo $data;
	}
	public function delete_offline_vm_department(){
		$department_id = $this->input->post("department_id");
		$data = $this->location_model->delete_offline_vm_department($department_id);
		echo $data;
	}
	public function delete_offline_vm_employee(){
		$employee_id = $this->input->post("employee_id");
		$data = $this->location_model->delete_offline_vm_employee($employee_id);
		echo $data;
	}
	
	public function contestifi_content_builder_list(){
		$data = $this->location_model->contestifi_content_builder_list();
		echo $data;
	}
	
	public function add_contistifi_quiz_question(){
		$data = $this->location_model->add_contistifi_quiz_question();
		echo $data->created_question_id;
	}
	public function delte_contestifi_quiz_question(){
		$question_id = $this->input->post("question_id");
		$location_id = $this->input->post("locationid");
		$data = $this->location_model->delte_contestifi_quiz_question($question_id,$location_id);
		echo $data;
	}
	public function get_contestifi_quiz_question(){
		$data = $this->location_model->get_contestifi_quiz_question();
		echo $data;
	}
	public function update_contistifi_quiz_question(){
		$data = $this->location_model->update_contistifi_quiz_question();
		echo $data->update_question_id;
	}
	public function add_contestifi_contest(){
		$data = $this->location_model->add_contestifi_contest();
		echo $data->created_contest_id;
	}
	public function delte_contestifi_contest(){
		$contest_id = $this->input->post("contest_id");
		$location_id = $this->input->post("locationid");
		$data = $this->location_model->delte_contestifi_contest($contest_id,$location_id);
		echo $data;
	}
	public function get_contestifi_contest_for_edit(){
		$data = $this->location_model->get_contestifi_contest_for_edit();
		echo $data;
	}
	public function update_contestifi_contest(){
		$data = $this->location_model->update_contestifi_contest();
		echo $data->created_contest_id;
	}
	public function add_contistifi_contest_prize(){
		$data = $this->location_model->add_contistifi_contest_prize();
		echo $data->created_prize_id;
	}
	
	public function delte_contestifi_contest_prize(){
		$prize_id = $this->input->post("prize_id");
		$location_id = $this->input->post("locationid");
		$data = $this->location_model->delte_contestifi_contest_prize($prize_id,$location_id);
		echo $data;
	}
	public function contestifi_add_content_builder(){
	
		$data = $this->location_model->contestifi_add_content_builder();
		echo $data->code;
		
	}
	public function get_contestifi_signup_term_condition(){
		$location_id = $this->input->post('location_id');
		$data = $this->location_model->get_contestifi_signup_term_condition($location_id);
		echo $data;
	}
	public function add_contestifi_terms_conditon(){
		$data = $this->location_model->add_contestifi_terms_conditon();
		echo $data;
	}
	public function add_contistifi_survey_question(){
		$data = $this->location_model->add_contistifi_survey_question();
		echo $data->created_question_id;
	}
	public function update_contistifi_survey_question(){
		$data = $this->location_model->update_contistifi_survey_question();
		echo $data->update_question_id;
	}
	
}
