<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Publicwifi extends CI_Controller {
	public function __construct(){
		parent::__construct();
		
		header("Access-Control-Allow-Origin: *");
                $this->load->model('publicwifi_model');
		$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
		$this->load->library('s3',$paramsthree);
		
		//$this->load->library('mongo_db');
	}
	public function index(){
		//print_r($_SESSION);die;
		//$this->load->view('welcome_message');
		//echo $_SERVER['DOCUMENT_ROOT'];
		$this->publicwifi_model->isp_license_data('101');

		
	}
	
	// CI query changes
	public function location_count(){
		//$_POST['requestData'] = '{"isp_uid" : "300"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_count($decodeData);
		}
		echo json_encode($data);
	}
	
	// CI query changes
	public function location_list(){
		//$_POST['requestData'] = '{"isp_uid":"100","location_type":"1","state":"","city":"","search_pattern":"14"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function location_list_data_usages(){
		//$_POST['requestData'] = '{"isp_uid":"100","location_type":"1","state":"","city":"","search_pattern":"14"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_list_data_usages($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function state_list(){
		//$_POST['requestData'] = '{"isp_uid" : "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->state_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function city_list(){
		//$_POST['requestData'] = '{"state_id" : "12"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->city_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function zone_list(){
		//$_POST['requestData'] = '{"city_id" : "12"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->zone_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function plan_list(){
		//$_POST['requestData'] = '{"plan_type" : "2","isp_uid" : "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->plan_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function location_autogenerate_id(){
		//$_POST['requestData'] = '{"isp_uid" : "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_autogenerate_id($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function add_location(){
		//$_POST['requestData'] = '{"isp_uid":"101","created_location_id":"1054","location_type":"7","location_name":"Retail Secure test","geo_address":"Chirag Dilli, New Delhi, Delhi, India","placeid":"ChIJu-usCSbiDDkRV1pfVrcr-HU","lat":"28.5376320000","long":"77.2282860000","address1":"Chirag Delhi","address2":"chirag delhi","pin":"112211","state":"9","city":"325","zone":"0","location_id":"1011065","contact_person_name":"Deepak","email_id":"deepak@shouut.com","mobile_no":"9856323254","enable_channel":"1","enable_nas_down_notification":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_location($decodeData);
		}
		echo json_encode($data);
	}
	
	// CI query changes
	public function add_captive_portal(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"1011072","locationid":"1055","cp_type":"cp2","cp1_plan":"0","cp1_no_of_daily_session":"0","cp2_plan":"7","cp2_signip_data":"100","cp2_daily_data":"100","cp3_hybrid_plan":"","cp3_data_plan":"0","cp3_signip_data":"0","cp3_daily_data":"0","image_original":"","image_small":"","image_logo":"","retail_image_original":"","retail_image_small":"","retail_image_logo":"","is_otpdisabled":"0","main_ssid":"testteste","icon_color":"#000000","original_slider1":"","original_slider2":"","login_with_mobile_email":"0","is_socialloginenable":"0","slider_background_color":"","is_pinenable":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_captive_portal($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function change_location_status(){
		//$_POST['requestData'] = '{"location_uid" : "1011058", "isp_uid" : "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->change_location_status($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function edit_location_data(){
		//$_POST['requestData'] = '{"locationid" : "10052"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->edit_location_data($decodeData);
		}
		echo json_encode($data);
	}
	
	// CI query changes
	public function nas_list(){
		//$_POST['requestData'] = '{"isp_uid" : "101", "router_type": "6"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->nas_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function get_isp_url(){
		//$_POST['requestData'] = '{"isp_uid" : "101", "location_id": "797"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_isp_url($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function get_nas_detail(){
		//$_POST['requestData'] ='{"nasid":"3"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_nas_detail($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function total_data_used(){
		//$_POST['requestData'] ='{"isp_uid":"140","from":"27.06.2018","to":"03.07.2018","gender":"","age_group":[],"time_slot":[],"locations":[],"state_id":"","city_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->total_data_used($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function location_wise_total_data_used(){
		//$_POST['requestData'] ='{"isp_uid":"101","from":"26.06.2018","to":"02.07.2018","gender":"","age_group":[],"time_slot":[],"locations":[],"limit":10,"offset":0,"state_id":"","city_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_wise_total_data_used($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function traffic_summary_total_user(){
		//$_POST['requestData'] ='{"isp_uid":"100","from":"07.06.2017 ","to":" 13.06.2017","gender":"","age_group":["18","18-30"],"time_slot":[],"locations":[]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->traffic_summary_total_user($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function traffic_summary_location_wise(){
		//$_POST['requestData'] ='{"isp_uid":"100","from":"06.06.2017","to":"12.06.2017","gender":"","age_group":[],"time_slot":[],"locations":[]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->traffic_summary_location_wise($decodeData);
		}
		echo json_encode($data);
	}
	public function legal_invention_get_user(){
		//$_POST['requestData'] ='{"isp_uid":"101","start":0,"limit":100,"mobile":"","selecteddate":"10.09.2017 13:02:01","selecteddateto":"28.09.2017 13:02:01","location_nasip":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->legal_invention_get_user($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function legal_invention_total_user(){
		//$_POST['requestData'] ='{"isp_uid":"100","mobile":"9650896116","selecteddate":"10.06.2017 13:02:01","selecteddateto":"12.06.2017 13:02:01","location_nasip":"172.23.43.234"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->legal_invention_total_user($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function location_voucher_list(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_voucher_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function add_location_vouchers(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_location_vouchers($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function delete_voucher(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_voucher($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function update_location_voucher_status(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_location_voucher_status($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function update_microtic_router_with_nas_location(){
		//$_POST['requestData'] ='{"location_id":"97","nasaddress":"38.113.162.252","nasid":"56","isp_uid":"165"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_microtic_router_with_nas_location($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function check_cp_selected_for_location(){
		//$_POST['requestData'] ='{"location_uid":"15"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$location_uid = $decodeData->location_uid;
			$data = $this->publicwifi_model->check_cp_selected_for_location($location_uid);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function update_other_router_with_nas_location(){
		//$_POST['requestData'] ='{"location_uid":"57","nasid":"28","isp_uid":"111","router_type":"6"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_other_router_with_nas_location($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function nas_setup_list(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->nas_setup_list($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function add_location_access_point(){
		/*$_POST['requestData'] ='{"location_uid":"10054","access_point_id":"","access_point_ssid":"aaa","access_point_hotspot_type":"1"
,"access_point_macid":"bbbb"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_location_access_point($decodeData);
		}
		echo json_encode($data);
	}
	public function raspberry_ping_sync_info(){
		//$_POST['requestData'] = '{"location_uid":"140212","macid":"b8:27:eb:79:ee:eb"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->raspberry_ping_sync_info($decodeData);
		}
		echo json_encode($data);
	}
	public function location_access_point_list(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_access_point_list($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_access_point(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_access_point($decodeData);
		}
		echo json_encode($data);
	}
	public function add_zone(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_zone($decodeData);
		}
		echo json_encode($data);
	}
	public function add_city(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_city($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function location_dashboard_search(){
		//$_POST['requestData'] ='{"isp_uid":"101","search_val":"ccd"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_dashboard_search($decodeData);
		}
		echo json_encode($data);
	}
	
	public function wifi_session_report(){
		//$_POST['requestData'] ='{"isp_uid":"100","start_date":"2017-06-14","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_session_report($decodeData);
		}
		echo json_encode($data);
	}
	public function wifi_first_free_session_user(){
		//$_POST['requestData'] ='{"locationid":"9","start_date":"2017-06-14","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_first_free_session_user($decodeData);
		}
		echo json_encode($data);
	}
	public function wifi_session_user(){
		//$_POST['requestData'] ='{"locationid":"9","start_date":"2017-06-14","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_session_user($decodeData);
		}
		echo json_encode($data);
	}
	public function wifi_session_report_locationvise(){
		//$_POST['requestData'] ='{"isp_uid":"100","locationid":"9","start_date":"2017-06-14","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_session_report_locationvise($decodeData);
		}
		echo json_encode($data);
	}
	public function wifi_first_free_session_user_locationvise(){
		//$_POST['requestData'] ='{"locationid":"9","start_date":"2017-06-14","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_first_free_session_user_locationvise($decodeData);
		}
		echo json_encode($data);
	}
	public function wifi_session_user_locationvise(){
		//$_POST['requestData'] ='{"locationid":"9","start_date":"2017-06-14","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_session_user_locationvise($decodeData);
		}
		echo json_encode($data);
	}
	
	public function wifi_session_failure_report(){
		//$_POST['requestData'] ='{"isp_uid":"100","start_date":"2017-06-21","end_date":"2017-06-21"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_session_failure_report($decodeData);
		}
		echo json_encode($data);
	}
	public function configure_microtic_router(){
		//$_POST['requestData'] = '{"secret":"123456","router_type":"921GS-5HPacD","router_id":"103.39.237.228","router_user":"admin","router_password":"","router_port":"","location_name":"Kharsia_mANT","location_id":"73","cp_path":"https://www.shouut.com/ispadmins/aashi.shouut.com/onehop/hotspot_v2","subdomain_ip":"","isp_uid":"143","main_ssid":"","guest_ssid":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->configure_microtic_router($decodeData);
		}
		echo json_encode($data);
	}
	public function test_deepak(){
		//$_POST['requestData'] = '{"secret":"stplnetnoc","router_type":"750r2-gr3","router_id":"103.70.124.87","router_user":"admin","router_password":"","router_port":"","location_name":"thripollia","location_id":"217","cp_path":"https:\/\/stpl.shouut.com\/mikrotik_kt\/hotspot_institutev2","subdomain_ip":"103.20.214.230","isp_uid":"129","main_ssid":"thripollia","guest_ssid":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->configure_microtic_router($decodeData);
		}
		echo json_encode($data);
	}
	public function reset_router(){
		/*$_POST['requestData'] = '{"router_type":"WAP","router_id":"14.102.15.252","router_user":"admin"
,"router_password":""}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->reset_router($decodeData);
		}
		echo json_encode($data);
	}
	public function check_wap_router_reset_porperly(){
		/*$_POST['requestData'] = '{"router_type":"Rb941-2nD","router_id":"113.11.225.102","router_user":"admin"
,"router_password":"admin4567"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->check_wap_router_reset_porperly($decodeData);
		}
		echo json_encode($data);
	}
	public function check_router_selected_matched(){
		/*$_POST['requestData'] = '{"router_type":"Rb941-2nD","router_id":"113.11.225.102","router_user":"admin"
,"router_password":"admin4567"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->check_router_selected_matched($decodeData);
		}
		echo json_encode($data);
	}
	public function add_hotel_captive_portal(){
		/*$_POST['requestData'] = '{"isp_uid":"100","location_uid":" 100150","locationid":"36","cp_type":"cp_hotel","cp_hotel_plan_type":"1","cp_home_selected_plan_id":"6","image_original":"20170825124438v9332.jpg","image_small":"600_20170825124438v9332.jpg","image_logo":"300_20170825124438v9332.jpg"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_hotel_captive_portal($decodeData);
		}
		echo json_encode($data);
	}
	
	public function ap_location_list(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->ap_location_list($decodeData);
		}
		echo json_encode($data);
	}
	public function add_ap_location(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_ap_location($decodeData);
		}
		echo json_encode($data);
	}
	public function is_location_proper_setup(){
		//$_POST['requestData'] ='{"nasid":"3"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->is_location_proper_setup($decodeData);
		}
		echo json_encode($data);
	}
	public function location_logo_name(){
		//$_POST['requestData'] ='{"nasid":"3"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_logo_name($decodeData);
		}
		echo json_encode($data);
	}
	public function traffic_summary_total_session(){
		//$_POST['requestData'] ='{"isp_uid":"121","from":"13.09.2017","to":"19.09.2017","gender":"","age_group":[],"time_slot":[],"locations":[]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->traffic_summary_total_session($decodeData);
		}
		echo json_encode($data);
	}
	public function traffic_session_summary_location_wise(){
		//$_POST['requestData'] ='{"isp_uid":"121","from":"13.09.2017","to":"19.09.2017","gender":"","age_group":[],"time_slot":[],"locations":[]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->traffic_session_summary_location_wise($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_get_network_detail(){
		//$_POST['requestData'] ='{"location_uid":"121163"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_get_network_detail($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_get_network_create(){
		//$_POST['requestData'] ='{"location_uid":"190229","network_name":"Caf\u00e9 Coffee Day Hyderabad","description":"Cafe Coffee Day, Hyderabad, Telangana, India","maxaps":"16","coa_status":"1","coa_ip":"103.20.214.108","coa_secret":"testing123","address":"Cafe Coffee Day, Hyderabad, Telangana, India","latitude":"17.4358760000","longitude":"78.3665930000","service_name":"NetworkCreate","beacon_status":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_get_network_create($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_add_apmac(){
		//$_POST['requestData'] ='{"location_uid":"121163","onehop_ap_mac":"8A:DC:96:36:CD:E8","onehop_ap_name":"test"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_add_apmac($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_add_ssid(){
		/*$_POST['requestData'] ='{"location_uid":"101144","onehop_ssid_index":"0","onehop_ssid_name":"ENTERPRISE_TEST","onehop_association":"0","onehop_wap_mode":"3","onehop_forwarding":"1","onehop_cp_mode":"2","onehop_cp_url":"https:\/\/decibel.shouut.com\/onehop\/hotspot_enterprisev2\/login.php","onehop_auth_server_ip":"103.20.214.108","onehop_auth_server_port":"1813","onehop_auth_server_secret":"test123","onehop_accounting":"1","onehop_acc_server_ip":"103.20.214.108","onehop_acc_server_port":"1812","onehop_acc_server_secret":"testing123","onehop_acc_interval":"60","onehop_psk":"","onehop_wallgarden_ip_list":"103.20.213.149,103.20.214.108,103.20.215.104", "onehop_wallgarden_domain_list" :"cdn101.shouut.com,s3-ap-southeast-1.amazonaws.com,secure4.arcot.com,3dsecure.payseal.com,static2.paytm.in,static3.paytm.in,static1.paytm.in,static4.paytm.in,secure.paytm.in,accounts-uat.paytm.com,pguat.paytm.com,facebook.com,fbcdn.net,facebook.net,maps.googleapis.com,akamaihd.net,decibel.shouut.com,connect.facebook.net,fbstatic-a.akamaihd.net,m.facebood.com,d392o9g87c202y.cloudfront.net,cloudfront.net,decibel.shouut.com"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_add_ssid($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_get_ssid_detail(){
		/*$_POST['requestData'] ='{"location_uid":"121163","onehop_ssid_index":"0","onehop_ssid_name":"tests","onehop_association":"1","onehop_wap_mode":"3","onehop_forwarding":null,"onehop_cp_mode":"0","onehop_cp_url":"#onehop_auth_server_ip=","onehop_auth_server_ip":null,"onehop_auth_server_port":"","onehop_auth_server_secret":"","onehop_accounting":"0","onehop_acc_server_ip":"","onehop_acc_server_port":"","onehop_acc_server_secret":"","onehop_acc_interval":"","onehop_psk":"test"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_get_ssid_detail($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_delete_ssid(){
		//$_POST['requestData'] ='';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_delete_ssid($decodeData);
		}
		echo json_encode($data);
	}
	
	public function plan_detail(){
		//$_POST['requestData'] ='{"plan_id":"27"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->plan_detail($decodeData);
		}
		echo json_encode($data);
	}
	public function check_plan_already_attached(){
		//$_POST['requestData'] ='{"location_uid":"121163"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->check_plan_already_attached($decodeData);
		}
		echo json_encode($data);
	}
	public function save_plan(){
		//$_POST['requestData'] ='{"plan_id":null,"data_limit":null,"time_limit":null,"plan_name":null,"plan_desc":null,"downrate":null,"uprate":null,"plan_type":null,"isp_uid":"121"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->save_plan($decodeData);
		}
		echo json_encode($data);
	}
	
	public function new_fun(){
	
			$data = $this->publicwifi_model->new_fun();
		
		echo json_encode($data);
	}
	public function terms_of_use(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->terms_of_use($decodeData);
		}
		echo json_encode($data);
	}
	public function update_terms_of_use(){
		//$_POST['requestData'] ='{"location_uid":"121478","text_value":"<p>afasdfasfasfasfasfasd<\/p>\n"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_terms_of_use($decodeData);
		}
		echo json_encode($data);
	}
	public function current_image_path(){
		//$_POST['requestData'] ='{"location_uid":"121163"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->current_image_path($decodeData);
		}
		echo json_encode($data);
	}
	public function current_cp_path(){
		//$_POST['requestData'] ='{"location_uid":"121228"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->current_cp_path($decodeData);
		}
		echo json_encode($data);
	}
	
	public function get_location_ap_move_where(){
		//$_POST['requestData'] = '{"isp_uid" : "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_location_ap_move_where($decodeData);
		}
		echo json_encode($data);
	}
	public function onehop_move_ap(){
		/*$_POST['requestData'] ='{"onehop_move_macid":"b0:4e:26:df:a0:b2","onehop_move_location_uid":"101370", "router_type": "6"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->onehop_move_ap($decodeData);
		}
		echo json_encode($data);
	}
	public function ping_onehop_ap(){
		//$_POST['requestData'] ='{"location_uid":"101121"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->ping_onehop_ap_location_wise($decodeData);
		}
		echo json_encode($data);
	}
	
	public function ping_onehop_ap_wise(){
		//$_POST['requestData'] ='{"location_uid":"101121"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->ping_onehop_ap_wise($decodeData);
		}
		echo json_encode($data);
	}
	public function remove_slider_image(){
		//$_POST['requestData'] ='{"location_uid":"140194","slider_type":"slider2"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->remove_slider_image($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_wifi_location(){
		//$_POST['requestData'] = '{"location_uid" : "10054", "isp_uid" : "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_wifi_location($decodeData);
		}
		echo json_encode($data);
	}
	
	public function wifi_offer_category(){
		//$_POST['requestData'] = '{"isp_uid" : "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_offer_category($decodeData);
		}
		echo json_encode($data);
	}
	public function add_offline_category(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101232","category_name":"afsda","offline_icon":"","offline_category_type":"2"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_offline_category($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_offline_category(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101232","category_name":"afsda","offline_icon":"","offline_category_type":"2"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_offline_category($decodeData);
		}
		echo json_encode($data);
	}
	
	public function content_builder_list(){
		//$_POST['requestData'] ='{"location_uid":"101319"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->content_builder_list($decodeData);
		}
		echo json_encode($data);
	}
	public function add_content_builder(){
		/*$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101317","locationid":"182","cp_type":"cp_offline","cp_hotel_plan_type":"","cp_home_selected_plan_id":"undefined","image_original":"","image_small":"","image_logo":"","cp_enterprise_user_type":"0","institutional_user_list":"","main_ssid":"Test","guest_ssid":"","cp_cafe_plan_id":"","is_otpdisabled":"1","retail_image_original":"","retail_image_small":"","retail_image_logo":"","num_of_session_per_day":"0","icon_color":"#000000","is_socialloginenable":"0","is_pinenable":"0","is_wifidisabled":"1","login_with_mobile_email":"0","offer_redemption_mode":"0","custom_captive_url":"","custom_location_plan":"0","custon_cp_signip_data":"0","custon_cp_daily_data":"0","custon_cp_no_of_daily_session":"0","original_slider1":"","original_slider2":"","synk_frequency":"2","offline_cp_type":"1","offline_cp_source_folder_path":"","offline_cp_landing_page_path":""}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content_builder($decodeData);
		}
		echo json_encode($data);
	}
	public function add_main_category(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","main_category_name":"SSC","main_category_desc":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_main_category($decodeData);
		}
		echo json_encode($data);
	}
	public function add_sub_category(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","main_category_name":"SSC","main_category_desc":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_sub_category($decodeData);
		}
		echo json_encode($data);
	}
	public function add_content(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","main_category_name":"SSC","main_category_desc":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_content(){
		//$_POST['requestData'] ='{"content_id":"12"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_content($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_sub_category(){
		//$_POST['requestData'] ='{"content_id":"12"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_sub_category($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_main_category(){
		//$_POST['requestData'] ='{"main_category_id":"10"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_main_category($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_content_poll(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","category_id":"3","is_main_category":"1","content_title":"fasdf","content_file":"","content_id":"","file_type":"poll","file_previous_name":"","poll_querstion":"afsdf","option_one":"fasdf","option_two":"fasdf","option_three":"fasd","option_four":"","option_five":"","option_six":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content_poll($decodeData);
		}
		echo json_encode($data);
	}
	public function get_content_poll_survey(){
		//$_POST['requestData'] = '{"content_id":"29"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_content_poll_survey($decodeData);
		}
		echo json_encode($data);
	}
	public function update_content_poll(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","content_id":"24","content_title":"Poll title","content_file":"","file_type":"poll","poll_querstion":"Poll question New","option_one":"aafsdf","option_two":"b","option_three":"c","option_four":"","option_five":"","option_six":"","file_previous_name":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_content_poll($decodeData);
		}
		echo json_encode($data);
	}
	public function add_content_survey(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","category_id":"3","is_main_category":"1","content_title":"fasd","content_file":"","file_type":"survey","questions":{"0":{"question_type":"1","question":"ENtery Type Question","options":{}},"1":{"question_type":"2","question":"Single CHoice question","options":{"0":"option a","1":"option b","2":"option c"}},"2":{"question_type":"3","question":"MultiChoice question","options":{"0":"opa","1":"opb"}}}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content_survey($decodeData);
		}
		echo json_encode($data);
	}
	
	public function update_content_survey(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","content_id":"39","content_title":"Survey 1","content_file":"","file_type":"survey","file_previous_name":"","questions":{"0":{"question_type":"1","question":"ENtery Type Question","options":{}},"1":{"question_type":"2","question":"Single CHoice question","options":{"0":"option a","1":"option b","2":"option c"}},"2":{"question_type":"3","question":"MultiChoice question","options":{"0":"opa","1":"opb"}}}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_content_survey($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_content_upload(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","main_category_name":"SSC","main_category_desc":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content_upload($decodeData);
		}
		echo json_encode($data);
	}
	public function get_onehop_existing_network(){
		//$_POST['requestData'] = '{"isp_uid" : "1012"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_onehop_existing_network($decodeData);
		}
		echo json_encode($data);
	}
	public function use_existing_onehop_network(){
		//$_POST['requestData'] ='{"isp_uid":"197","previous_network_id":"46", "location_uid": "197213"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->use_existing_onehop_network($decodeData);
		}
		echo json_encode($data);
	}
	public function nas_ping_status(){
		$this->publicwifi_model->nas_ping_status();
	}
	
	public function add_content_wiki(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101317","locationid":"182","category_id":"0","is_main_category":"0","content_title":"Dehradun Histrory","wikipedia_title_url":"Dehradun","wikipedia_title":"Dehradun","wikipedia_pageid":"1156018", "wikipedia_content": "", "content_id":"77","file_type": "wiki"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content_wiki($decodeData);
		}
		echo json_encode($data);
	}
	public function update_device_online_offline_status(){
		$this->publicwifi_model->update_device_online_offline_status();
	}
	
	public function state_list_for_filter(){
		//$_POST['requestData'] = '{"isp_uid" : "197"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->state_list_for_filter($decodeData);
		}
		echo json_encode($data);
	}
	// CI query changes
	public function city_list_for_filter(){
		//$_POST['requestData'] = '{"isp_uid" : "101","state_id" : "4137"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->city_list_for_filter($decodeData);
		}
		echo json_encode($data);
	}
	
	public function wifi_total_session(){
		//$_POST['requestData'] = '{"isp_uid":"101","start_date":"2018-03-22","end_date":"2018-03-29","state_id":"","city_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->wifi_total_session($decodeData);
		}
		echo json_encode($data);
	}
	
	public function get_location_group(){
		//$_POST['requestData'] = '{"isp_uid":"101","start_date":"2018-03-22","end_date":"2018-03-29","state_id":"","city_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_location_group($decodeData);
		}
		echo json_encode($data);
	}
	
	public function create_location_group(){
		/*$_POST['requestData'] ='{"location_uid":"121163","onehop_ssid_index":"0","onehop_ssid_name":"tests","onehop_association":"1","onehop_wap_mode":"3","onehop_forwarding":null,"onehop_cp_mode":"0","onehop_cp_url":"#onehop_auth_server_ip=","onehop_auth_server_ip":null,"onehop_auth_server_port":"","onehop_auth_server_secret":"","onehop_accounting":"0","onehop_acc_server_ip":"","onehop_acc_server_port":"","onehop_acc_server_secret":"","onehop_acc_interval":"","onehop_psk":"test"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->create_location_group($decodeData);
		}
		echo json_encode($data);
	}
	public function add_location_to_group(){
		//$_POST['requestData'] ='{"isp_uid":"101","group_id":"1","locations":["197905","1979843","197592"]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_location_to_group($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_loc_group(){
		//$_POST['requestData'] ='{"isp_uid":"101","group_id":"7"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_loc_group($decodeData);
		}
		echo json_encode($data);
	}
	
	public function update_location_group(){
		//$_POST['requestData'] ='{"isp_uid":"101","group_name":"Deepak NEw","username":"Deepak NEw","password":"NEWNEWt","group_id":"2","locations":["7"]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_location_group($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function get_group_location_list(){
		//$_POST['requestData'] = '{"isp_uid":"101","start_date":"2018-03-22","end_date":"2018-03-29","state_id":"","city_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_group_location_list($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_otp_sms(){
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_otp_sms($decodeData);
		}
		echo json_encode($data);
	}
	
	public function sms_list(){
		//$_POST['requestData'] ='{"location_uid":"1011025", "location_id": "1025"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->sms_list($decodeData);
		}
		echo json_encode($data);
	}
	public function offline_organisation_list(){
		//$_POST['requestData'] ='{"location_uid":"1011037"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->offline_organisation_list($decodeData);
		}
		echo json_encode($data);
	}
	
	
	public function add_offline_vm_organisation(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","main_category_name":"SSC","main_category_desc":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_offline_vm_organisation($decodeData);
		}
		echo json_encode($data);
	}
	
	public function vm_offline_department_list(){
		//$_POST['requestData'] ='{"location_uid":"1011037"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->vm_offline_department_list($decodeData);
		}
		echo json_encode($data);
	}
	public function add_offline_vm_department(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","main_category_name":"SSC","main_category_desc":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_offline_vm_department($decodeData);
		}
		echo json_encode($data);
	}
	
	public function vm_offline_employee_list(){
		//$_POST['requestData'] ='{"location_uid":"1011037"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->vm_offline_employee_list($decodeData);
		}
		echo json_encode($data);
	}
	public function add_vm_offline_employee(){
		/*$_POST['requestData'] = '{"isp_uid":"101","location_uid":"1011037","locationid":"1030","employee_name":"Deepak","employee_email"
:"Deepak@gmail.com","employee_mobile":"9650896116","org_id":"3","dept_id":"2"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_vm_offline_employee($decodeData);
		}
		echo json_encode($data);
	}
	
	public function delete_offline_vm_organisation(){
		//$_POST['requestData'] ='{"main_category_id":"10"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_offline_vm_organisation($decodeData);
		}
		echo json_encode($data);
	}
	
	public function delete_offline_vm_department(){
		//$_POST['requestData'] ='{"main_category_id":"10"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_offline_vm_department($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_offline_vm_employee(){
		//$_POST['requestData'] ='{"main_category_id":"10"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_offline_vm_employee($decodeData);
		}
		echo json_encode($data);
	}
	
	public function create_zip_for_offline_content()
	{
		//$_POST['requestData'] ='{"location_uid": "101407"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->create_zip_for_offline_content($decodeData);
		}
		echo json_encode($data);
	}
	
	public function create_zip_for_all_location_manually()
	{
		$data = $this->publicwifi_model->create_zip_for_all_location_manually();
	}
	
	public function contestifi_content_builder_list(){
		//$_POST['requestData'] ='{"location_uid":"1971028"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->contestifi_content_builder_list($decodeData);
		}
		echo json_encode($data);
	}
	public function add_contistifi_quiz_question(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","category_id":"3","is_main_category":"1","content_title":"fasdf","content_file":"","content_id":"","file_type":"poll","file_previous_name":"","poll_querstion":"afsdf","option_one":"fasdf","option_two":"fasdf","option_three":"fasd","option_four":"","option_five":"","option_six":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_contistifi_quiz_question($decodeData);
		}
		echo json_encode($data);
	}
	public function delte_contestifi_quiz_question(){
		//$_POST['requestData'] ='{"question_id":"4","location_id":"1024"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delte_contestifi_quiz_question($decodeData);
		}
		echo json_encode($data);
	}
	
	public function get_contestifi_quiz_question(){
		//$_POST['requestData'] = '{"question_id":"29"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_contestifi_quiz_question($decodeData);
		}
		echo json_encode($data);
	}
	public function update_contistifi_quiz_question(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"1971028","locationid":"1024","quiz_question_id":"2","poll_querstion":"WHat is Shouut New","option_one":"aaa","option_two":"b","option_three":"c","option_four":"dfasdfas","contest_id":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_contistifi_quiz_question($decodeData);
		}
		echo json_encode($data);
	}
	public function add_contestifi_contest(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"1971028","locationid":"1024","contest_name":"tesfs","contest_type":"1","bg_color":"#000000","contest_frequency":"4","contestifi_custom_frequecny":"12"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_contestifi_contest($decodeData);
		}
		echo json_encode($data);
	}
	public function delte_contestifi_contest(){
		//$_POST['requestData'] ='{"question_id":"4","location_id":"1024"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delte_contestifi_contest($decodeData);
		}
		echo json_encode($data);
	}
	public function get_contestifi_contest_for_edit(){
		
		//$_POST['requestData'] = '{"contest_id":"4"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_contestifi_contest_for_edit($decodeData);
		}
		echo json_encode($data);
	}
	public function update_contestifi_contest(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"1971028","locationid":"1024","contest_name":"tesfs","contest_type":"1","bg_color":"#000000","contest_frequency":"4","contestifi_custom_frequecny":"12"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_contestifi_contest($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_contistifi_contest_prize(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","category_id":"3","is_main_category":"1","content_title":"fasdf","content_file":"","content_id":"","file_type":"poll","file_previous_name":"","poll_querstion":"afsdf","option_one":"fasdf","option_two":"fasdf","option_three":"fasd","option_four":"","option_five":"","option_six":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_contistifi_contest_prize($decodeData);
		}
		echo json_encode($data);
	}
	
	public function delte_contestifi_contest_prize(){
		//$_POST['requestData'] ='{"question_id":"4","location_id":"1024"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delte_contestifi_contest_prize($decodeData);
		}
		echo json_encode($data);
	}
	
	public function contestifi_add_content_builder(){
		/*$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101317","locationid":"182","cp_type":"cp_offline","cp_hotel_plan_type":"","cp_home_selected_plan_id":"undefined","image_original":"","image_small":"","image_logo":"","cp_enterprise_user_type":"0","institutional_user_list":"","main_ssid":"Test","guest_ssid":"","cp_cafe_plan_id":"","is_otpdisabled":"1","retail_image_original":"","retail_image_small":"","retail_image_logo":"","num_of_session_per_day":"0","icon_color":"#000000","is_socialloginenable":"0","is_pinenable":"0","is_wifidisabled":"1","login_with_mobile_email":"0","offer_redemption_mode":"0","custom_captive_url":"","custom_location_plan":"0","custon_cp_signip_data":"0","custon_cp_daily_data":"0","custon_cp_no_of_daily_session":"0","original_slider1":"","original_slider2":"","synk_frequency":"2","offline_cp_type":"1","offline_cp_source_folder_path":"","offline_cp_landing_page_path":""}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->contestifi_add_content_builder($decodeData);
		}
		echo json_encode($data);
	}
	public function get_contestifi_signup_term_condition(){
		//$_POST['requestData'] ='{"location_uid":"121163"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_contestifi_signup_term_condition($decodeData);
		}
		echo json_encode($data);
	}
	public function add_contestifi_terms_conditon(){
		//$_POST['requestData'] ='{"location_id":"1033","terms":"fsadf"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_contestifi_terms_conditon($decodeData);
		}
		echo json_encode($data);
	}
	public function contestifi_otp_send(){
		//$_POST['requestData'] ='{"location_id":"1033","terms":"fsadf"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->contestifi_otp_send($decodeData);
		}
		echo json_encode($data);
	}
	public function add_contistifi_survey_question(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","category_id":"3","is_main_category":"1","content_title":"fasdf","content_file":"","content_id":"","file_type":"poll","file_previous_name":"","poll_querstion":"afsdf","option_one":"fasdf","option_two":"fasdf","option_three":"fasd","option_four":"","option_five":"","option_six":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_contistifi_survey_question($decodeData);
		}
		echo json_encode($data);
	}
	public function update_contistifi_survey_question(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"1971028","locationid":"1024","quiz_question_id":"2","poll_querstion":"WHat is Shouut New","option_one":"aaa","option_two":"b","option_three":"c","option_four":"dfasdfas","contest_id":"1"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_contistifi_survey_question($decodeData);
		}
		echo json_encode($data);
	}
	
	public function retail_audit_sku_pin_list(){
		//$_POST['requestData'] ='{"location_uid":"1011037"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->retail_audit_sku_pin_list($decodeData);
		}
		echo json_encode($data);
	}
	
	public function add_retail_audit_sku(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"197329","locationid":"325","sku_name":"HDFC","sku_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_retail_audit_sku($decodeData);
		}
		echo json_encode($data);
	}
	public function add_retail_audit_pin(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"197329","locationid":"325","sku_name":"HDFC","sku_id":""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_retail_audit_pin($decodeData);
		}
		echo json_encode($data);
	}
	public function add_content_survey_retail_audit(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","category_id":"3","is_main_category":"1","content_title":"fasd","content_file":"","file_type":"survey","questions":{"0":{"question_type":"1","question":"ENtery Type Question","options":{}},"1":{"question_type":"2","question":"Single CHoice question","options":{"0":"option a","1":"option b","2":"option c"}},"2":{"question_type":"3","question":"MultiChoice question","options":{"0":"opa","1":"opb"}}}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_content_survey_retail_audit($decodeData);
		}
		echo json_encode($data);
	}
	public function get_content_poll_survey_retail_audit(){
		//$_POST['requestData'] = '{"content_id":"29"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->get_content_poll_survey_retail_audit($decodeData);
		}
		echo json_encode($data);
	}
	public function update_content_survey_retail_audit(){
		//$_POST['requestData'] = '{"isp_uid":"101","location_uid":"101319","locationid":"184","content_id":"39","content_title":"Survey 1","content_file":"","file_type":"survey","file_previous_name":"","questions":{"0":{"question_type":"1","question":"ENtery Type Question","options":{}},"1":{"question_type":"2","question":"Single CHoice question","options":{"0":"option a","1":"option b","2":"option c"}},"2":{"question_type":"3","question":"MultiChoice question","options":{"0":"opa","1":"opb"}}}}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->update_content_survey_retail_audit($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_retail_audit_sku_name(){
		//$_POST['requestData'] ='{"main_category_id":"10"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_retail_audit_sku_name($decodeData);
		}
		echo json_encode($data);
	}
	public function add_event_module_signup_extra_field(){
		//$_POST['requestData'] = '{"isp_uid":"140","location_uid":"140446","locationid":"451","field_name":"Address"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->add_event_module_signup_extra_field($decodeData);
		}
		echo json_encode($data);
	}
	public function delete_event_module_signup_field(){
		//$_POST['requestData'] ='{"main_category_id":"10"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->delete_event_module_signup_field($decodeData);
		}
		echo json_encode($data);
	}
	public function import_location(){
		echo "BLock";die;
		$data = $this->publicwifi_model->import_location();
		
	}
	public function update_store_unique_id(){
	
		$data = $this->publicwifi_model->update_store_unique_id();
		
	}
	public function update_sku_through_excel(){
	
		$data = $this->publicwifi_model->update_sku_through_excel();
		
	}
	public function lms_vm_offline_department_list(){
		//$_POST['requestData'] ='{"location_uid":"1011037"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->lms_vm_offline_department_list($decodeData);
		}
		echo json_encode($data);
	}
	public function lms_vm_offline_employee_list(){
		//$_POST['requestData'] ='{"location_uid":"1011037"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->lms_vm_offline_employee_list($decodeData);
		}
		echo json_encode($data);
	}
	public function lms_content_builder_list(){
		//$_POST['requestData'] ='{"isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->lms_content_builder_list($decodeData);
		}
		echo json_encode($data);
	}
	public function lms_contestifi_content_builder_list(){
		//$_POST['requestData'] ='{"location_uid":"1971028"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->lms_contestifi_content_builder_list($decodeData);
		}
		echo json_encode($data);
	}
	public function lms_get_content_location(){
		//$_POST['requestData'] = '{"isp_uid":"101","content_id":"3379","is_contest":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->lms_get_content_location($decodeData);
		}
		echo json_encode($data);
	}
	public function lms_add_content_location(){
		//$_POST['requestData'] ='{"isp_uid":"101","content_id":"3379","is_contest":"0","locations":["987","988","989"]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->lms_add_content_location($decodeData);
		}
		echo json_encode($data);
	}
	public function treya_user_simultanes(){
		$data = $this->publicwifi_model->treya_user_simultanes();
		
	}
	
	public function foodle_loyalty_reward_list(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_loyalty_reward_list($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_location_loyalty_reward_list(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_location_loyalty_reward_list($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_add_loyalty_rewards(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122","locationid":"1055","loyalty_reward":"2","loyalty_reward_name":"Capuccino"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_add_loyalty_rewards($decodeData);
		}
		echo json_encode($data);
	}
	public function location_loyalty_reward_delete(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_loyalty_reward_delete($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_add_loyalty_slab(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122","locationid":"1055","slab_loyalty_reward_amount":"100","slab_location_loyalty_reward":"2","foodle_slab_id":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_add_loyalty_slab($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_location_loyalty_slab_list(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_location_loyalty_slab_list($decodeData);
		}
		echo json_encode($data);
	}
	public function location_loyalty_slab_delete(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_loyalty_slab_delete($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_location_loyalty_reward_audience_list(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_location_loyalty_reward_audience_list($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_add_loyalty_audience(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122","locationid":"1055","slab_loyalty_reward_amount":"100","slab_location_loyalty_reward":"2","foodle_slab_id":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_add_loyalty_audience($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_campaign_list(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_campaign_list($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_add_campaign(){
		//$_POST['requestData'] ='{"isp_uid":"101","location_uid":"1011122","locationid":"1055","foodle_campaign_name":"Test Camp","foodle_campaign_location_reward":"2","foodle_campaign_location_audience":"1","foodle_campaign_start_date":"15-03-2019","foodle_campaign_end_date":"28-03-2019","foodle_campaign_expire_day":"45","foodle_campaign_id":"0"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_add_campaign($decodeData);
		}
		echo json_encode($data);
	}
	public function foodle_campaign_delete(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->foodle_campaign_delete($decodeData);
		}
		echo json_encode($data);
	}
	public function location_loyalty_audience_delete(){
		//$_POST['requestData'] ='{"location_uid":"10054"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->publicwifi_model->location_loyalty_audience_delete($decodeData);
		}
		echo json_encode($data);
	}
	
	
}

?>
