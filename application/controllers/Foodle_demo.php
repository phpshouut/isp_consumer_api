<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Foodle_demo extends CI_Controller {
    private $authkey = 'c2hvdXV0Rm9vZGxl';
    
    public function __construct(){
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Allow-Headers: Content-Type, Authorization");
	$this->load->helper('string');
	$this->load->library('encrypt');
		$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
		$this->load->library('s3',$paramsthree);
        $this->load->model('Foodle_demo_model', 'foodle_demo_model');
    }
	
	public function log($function,$request, $responce){
		$data = $this->foodle_demo_model->log($function, $request, $responce);
	}
    

    public function mobile_login(){
		
	/*$postdata['requestData']='{"mobile":"9560588376"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->mobile_login($decodeData);*/
       // echo json_encode($data); die;
        $postdata = file_get_contents('php://input');
		//echo "<pre>"; print_R($postdata); die;
		$headers_request = apache_request_headers();
		//echo "<pre>"; print_R($headers_request); die;
		//echo "sssss"; die;
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';
			//echo "====>>".$contenttype."::::".$authorization; 	
            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
				//echo "sssssss"; die;
            }
            else{
                $requiredObjects = array("mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
					$decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->mobile_login($decodeData);
                }
            }
        }
		$this->log('mobile_login',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function verify_otp(){
		
	/*$postdata['requestData']='{"mobile":"9560588376","otp":"4913"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->verify_otp($decodeData);*/
       // echo json_encode($data); die;
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","otp");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->verify_otp($decodeData);
                }
            }
        }
		$this->log('verify_otp',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function send_otp(){
		
	/*$postdata['requestData']='{"mobile":"9560588376"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->send_otp($decodeData);*/
       // echo json_encode($data); die;
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->send_otp($decodeData);
                }
            }
        }
		$this->log('send_otp',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function foodle_offer(){
		/*$postdata['requestData']='{"mobile":"9560588376","lat":"","long":""}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->foodle_offer($decodeData);
        echo json_encode($data); die;*/
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->foodle_offer($decodeData);
                }
            }
        }
		$this->log('foodle_offer',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function unlock_offer(){
	/*$postdata['requestData']='{"mobile":"9560588376","reward_id":"19","loc_id":"1093","is_campaign":0}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->unlock_offer($decodeData);
		echo json_encode($data);die;
		die;*/
       
        $postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->unlock_offer($decodeData);
                }
            }
        }
		$this->log('unlock_offer',$postdata, json_encode($data));//call log function
        echo json_encode($data);
    }
    
    public function user_registration(){
		
		/*$postdata['requestData']='{"mobile":"9560588376","firstname":"san","lastname":"tya","email":"styagibtech@gmail.com","password":"sandycool8860","device_id":""}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->user_registration($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","firstname","lastname","email","password","gender","age_group");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->user_registration($decodeData);
                }
            }
        }
		$this->log('user_registration',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function user_login(){
		
		/*$postdata['requestData']='{"mobile":"9560588376","password":"sandycool8860","device_id":"sdsdffdf"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->user_login($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->user_login($decodeData);
                }
            }
        }
		$this->log('user_login',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function forget_password(){
		/*$postdata['requestData']='{"mobile":"9560588376","password":"sandycool8860"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->forget_password($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->forget_password($decodeData);
                }
            }
        }
		$this->log('forget_password',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function logout(){
		/*$postdata['requestData']='{"mobile":"9560588376","device_id":"sdsdffdf"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->logout($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","device_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->logout($decodeData);
                }
            }
        }
		$this->log('logout',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	
	
	
	
	
	public function redeem_check(){
		/*$postdata['requestData']='{"mobile":"9560588376","reward_id":"1"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->redeem_check($decodeData);
		echo json_encode($data);
		die;
		*/
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","reward_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->redeem_check($decodeData);
                }
            }
        }
		$this->log('redeem_check',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function redeem_offer(){
		//{"mobile":"9889991090","reward_id":"54","redeemed_by":"2"}
		/*$postdata['requestData']='{"mobile":"8178840723","reward_id":"7","redeemed_by":"2"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->redeem_offer($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","reward_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->redeem_offer($decodeData);
                }
            }
        }
		$this->log('redeem_offer',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function refer_campaign(){
		/*$postdata['requestData']='{"mobile":["8178840723","7503945244"],"campaign_id":"1","loc_id":"1093"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->refer_campaign($decodeData);
		echo json_encode($data);
		die;*/
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("campaign_id","loc_id","referred_by");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->refer_campaign($decodeData);
                }
            }
        }
		$this->log('refer_campaign',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function team_login(){
	//	$postdata['requestData']='{"mobile":"9560588376","password":"sdf"}';
       /* $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->team_login($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("mobile","password");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->team_login($decodeData);
                }
            }
        }
		$this->log('team_login',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function scan_barcode(){
		/*$postdata['requestData']='{"loc_id":1093,"qr_code":"SIFPLC27"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->scan_barcode($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("loc_id","qr_code");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->scan_barcode($decodeData);
                }
            }
        }
		$this->log('scan_barcode',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function version_check(){
		/*$postdata['requestData']='{"platform":"android","app_type":"user"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->version_check($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("platform");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->version_check($decodeData);
                }
            }
        }
		$this->log('version_check',$postdata, json_encode($data));//call log function
        echo json_encode($data);
		
	}
	
	public function invoice_list(){
		/*$postdata['requestData']='{"loc_id":"1093","team_id":"2"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->invoice_list($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("loc_id","team_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->invoice_list($decodeData);
                }
            }
        }
		$this->log('invoice_list',$postdata, json_encode($data));//call log function
        echo json_encode($data);
		
	}
	
	public function invoice_upload(){
		/*$postdata['requestData']='{"invoice_id":"","loc_id":"1093","user":"8178840723","invoice_amt":"2500","invoice_no":"asd12","team_id":"2"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->invoice_upload($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("user","loc_id","invoice_amt","invoice_no","team_id");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->invoice_upload($decodeData);
                }
            }
        }
		$this->log('invoice_upload',$postdata, json_encode($data));//call log function
        echo json_encode($data);
		
	}
	
	public function foodle_terms_privacy(){
		/*$postdata['requestData']='{"type":"privacy"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->foodle_terms_privacy($decodeData);
		echo json_encode($data);
		die;*/
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array();
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->foodle_terms_privacy($decodeData);
                }
            }
        }
		$this->log('foodle_terms_privacy',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function user_spends(){
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("user");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->user_spends($decodeData);
                }
            }
        }
		$this->log('user_spends',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function user_profile(){
		/*$postdata['requestData']='{"user":"9560588376"}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->user_profile($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("user");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->user_spends($decodeData);
                }
            }
        }
		$this->log('user_spends',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	public function edit_profile(){
	/*	$postdata['requestData']='{
	"full_name":"sandeep Tyagi1",
	"phone_number":"9560588376",
	"email":"mahesh@shouut.com",
	"gender":"Male",
	"dob":"10/03/1994",
	"profile_image":""
	}';
        $decodeData = json_decode($postdata['requestData']);
        $data= $this->foodle_demo_model->edit_profile($decodeData);
		echo json_encode($data);
		die;*/
		
		$postdata = file_get_contents('php://input');
        $data = array();
        if(empty($postdata)){
            $data['resultCode'] = '400';
            $data['resultMsg'] = 'Bad Request';
        }
        else{
            $headers_request = apache_request_headers();
            $contenttype = isset( $headers_request['Content-Type'] ) ? $headers_request['Content-Type'] : '';
            $authorization = isset( $headers_request['Authorization'] ) ? $headers_request['Authorization'] : '';

            if(($contenttype != 'application/json') || ($authorization != $this->authkey)){
                $data['resultCode'] = '400';
                $data['resultMsg'] = 'Bad Request (Header not sent properly.)';
            }
            else{
                $requiredObjects = array("phone_number","full_name","gender");
                $requestObjects = json_decode($postdata, true);
                $keynotpresent = array_diff_key( array_flip( $requiredObjects ) , $requestObjects );
                if(count($keynotpresent) > 0){
                    $data['resultCode'] = '400';
                    $data['resultMsg'] = 'Bad Request';
                    //$data['keynotpresent'] = $keynotpresent;
                }else{
					
		    $decodeData = json_decode($postdata);
                    $data = $this->foodle_demo_model->user_spends($decodeData);
                }
            }
        }
		$this->log('user_spends',$postdata, json_encode($data));//call log function
        echo json_encode($data);
	}
	
	
	
	
    
    
    
    
    
	
	

}

?>