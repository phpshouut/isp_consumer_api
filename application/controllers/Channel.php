<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(0);
class Channel extends CI_Controller {
	public function __construct(){
		
		parent::__construct();
		$path = FCPATH;
		if($path != ''){
		    $path = $path.'assets';
			// change permission 
		    $fldrperm = substr(sprintf('%o', fileperms("$path")), -4);
		    if( $fldrperm != 0777) {
			$output = exec("sudo chmod -R 0777 \"$path\"");
		    }
		}
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Allow-Origin: *");
		
		$this->load->library('session');
		//$this->load->library('mongo_db');
		$this->load->library('excel');
		$this->load->library('My_PHPMailer');
		$this->load->model('channel_model');
		$paramsthree=array("accessKey"=>awsAccessKey,"secretKey"=>awsSecretKey);
		$this->load->library('s3',$paramsthree);

	}
	public function index(){
		//echo $this->session->userdata('logintype');
		print_r($this->session->userdata);die;
		//echo $_SERVER['DOCUMENT_ROOT'];
		$this->load->view('welcome_message');
		echo $_SERVER['DOCUMENT_ROOT'];

	}
	// new isp changes done
	public function locationLogin(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->locationLogin($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function login(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->login($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function brand_header_info(){
		$data = $this->channel_model->brand_header_info();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function brand_subheader_info(){
		$data = $this->channel_model->brand_subheader_info();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function dashboard_active_campaign_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_active_campaign_list($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function dashboard_expire_campaign_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_expire_campaign_list($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function wifilocation(){
		$data = $this->channel_model->wifilocation();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function daily_people_reached(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->daily_people_reached($request);
		$this->_sendResponse(json_encode($data));

	}
	// isp changes done
	public function brand_budget_detail(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->brand_budget_detail($request);
		$this->_sendResponse(json_encode($data));
	}
	// isp changes done
	public function offersstore_list(){
		$data = $this->channel_model->offersstore_list();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function brand_name_logo(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->brand_name_logo($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp chanegs done
	public function add_campaign_offer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_campaign_offer($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function store_list(){
		$data = $this->channel_model->store_list();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function store_region(){
		$data = $this->channel_model->store_region();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function store_type(){
		$data = $this->channel_model->store_type();
		$this->_sendResponse(json_encode($data));
	}
	//new isp changes done
	public function state_list(){
		$data = $this->channel_model->state_list();
		$this->_sendResponse(json_encode($data));
	}
	//new isp changes done
	public function checkstorename(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->checkstorename($request);
		$this->_sendResponse(json_encode($data));
	}
	// isp new changes done
	public function add_store(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_store($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function store_status(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->store_status($request);
		$this->_sendResponse(json_encode($data));
	}
	//new isp chagnes done
	public function checkstorename_edit(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->checkstorename_edit($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function update_store(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->update_store($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function delete_store(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->delete_store($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function setcampaignId(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->setcampaignId($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function get_campaign_visitor_detail(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_visitor_detail($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function campaign_graph_data(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp chanes done
	public function pauseCampaign(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->pauseCampaign($request);
		$this->_sendResponse(json_encode($data));

	}
	// new isp chanes done
	public function resumeCampaign(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->resumeCampaign($request);
		$this->_sendResponse(json_encode($data));

	}
	// new isp change done
	public function stopCampaign(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->stopCampaign($request);
		$this->_sendResponse(json_encode($data));

	}
	// new isp chagnes done
	public function cp_offer_not_attached_location(){
		$data = $this->channel_model->cp_offer_not_attached_location();
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function cp_offer_new_location(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->cp_offer_new_location($request);
		$this->_sendResponse(json_encode($data));

	}
	// new isp changes done
	public function campaign_graph_data_offerlisting(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_offerlisting($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function get_campaign_offerlisting_visitor_detail(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_offerlisting_visitor_detail($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function campaign_graph_data_bannerflash(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_bannerflash($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function get_campaign_bannerflash_visitor_detail(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_bannerflash_visitor_detail($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function campaign_graph_data_poll(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_poll($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function campaign_graph_data_poll_question(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_poll_question($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function campaign_graph_data_captcha(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_captcha($request);
		$this->_sendResponse(json_encode($data));
	}
	
	// new isp changes done
	public function campaign_graph_data_appdownload(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_appdownload($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function get_campaign_visitor_detail_appdownload(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_visitor_detail_appdownload($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function campaign_graph_data_redeemable(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_redeemable($request);
		$this->_sendResponse(json_encode($data));
	}
	// new isp changes done
	public function get_campaign_redeemable_visitor_detail(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_redeemable_visitor_detail($request);
		$this->_sendResponse(json_encode($data));
	}
	public function test(){
		$image = $_FILES['deepak']['name'];
		$tmp = $_FILES['deepak']['tmp_name'];
		$path = $_SERVER['DOCUMENT_ROOT'].'/android/assets/';
		move_uploaded_file($tmp,$path.$image);
		$imagearr = getimagesize($path.$image);
		echo '<pre>'; print_r($imagearr);
	}
	private function _sendResponse($data) {
		//header('application/json; charset=utf-8');
		echo $data;
	}

	public function additional_terms(){
		$data = $this->channel_model->additional_terms();
		$this->_sendResponse(json_encode($data));
	}
	
	public function check_session(){
		$this->session->userdata('uid');
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->check_session($request);
		if($this->session->userdata('uid') && $this->session->userdata('uid') != '' ) {
			print 'authentified';
		}
	}
	public function logout (){
		if($this->session->userdata('provider_id') && $this->session->userdata('provider_id') != '' ) {
			$provider_id =  $this->session->userdata('provider_id');
			$data = $this->channel_model->logout($provider_id);
		}
		$this->session->unset_userdata('uid');

	}
	public function signup(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->signup($request);
		$this->_sendResponse(json_encode($data));
	}
	public function brand_name(){
		$data = $this->channel_model->brand_name();
		$this->_sendResponse(json_encode($data));
	}

	public function request_new_coupon(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->request_new_coupon($request);
		$this->_sendResponse(json_encode($data));
	}

	
	

	

	public function brand_info(){
		$data = $this->channel_model->brand_info();
		$this->_sendResponse(json_encode($data));
	}

	public function update_brand_email(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->update_brand_email($request);
		$this->_sendResponse(json_encode($data));
	}
	public function finish_brand_profile(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->finish_brand_profile($request);
		$this->_sendResponse(json_encode($data));
	}

	public function brandlist(){
		$data = $this->channel_model->brandlist();
		$this->_sendResponse(json_encode($data));
	}
	public function setbrand(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->setbrand($request);
		$this->_sendResponse(json_encode($data));
	}

	
	
	



// for dashboard
	public function daily_analysis_update(){
		$this->channel_model->daily_analysis_update();
	}

	public function daily_offer_analysis_update(){
		$this->channel_model->daily_offer_analysis_update();
	}


	public function daily_flash_analysis_update(){
		$this->channel_model->daily_flash_analysis_update();
	}
	public function daily_wifioffer_analysis_update(){
		$this->channel_model->daily_wifioffer_analysis_update();
	}
	public function daily_captiveportal_analysis_update(){
		$this->channel_model->daily_captiveportal_analysis_update();
	}
	public function get_toal_visiter(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_toal_visiter($request);
		$this->_sendResponse(json_encode($data));
	}
	public function dashboard_offer_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_offer_list($request);
		$this->_sendResponse(json_encode($data));
	}

	public function dashboard_flash_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_flash_list($request);
		$this->_sendResponse(json_encode($data));
	}
	public function dashboard_wifioffer_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_wifioffer_list($request);
		$this->_sendResponse(json_encode($data));
	}

	public function dashboard_voucher_list(){
		$postdata = file_get_contents("php://input");

		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_voucher_list($request);
		//$data = $this->channel_model->dashboard_voucher_list();
		$this->_sendResponse(json_encode($data));
	}
	public function dashboard_graph_data(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_graph_data($request);
		$this->_sendResponse(json_encode($data));
	}
	public function sms_flash_avail(){
		/*$_POST['requestData'] = '{"store_id": "1725", "coupon_code": "23", "mobile" : "9650896116"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->channel_model->sms_flash_avail($decodeData);
		}
		$this->_sendResponse(json_encode($data));
	}
	public function avail_coupon_code(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->avail_coupon_code($request);
		$this->_sendResponse(json_encode($data));
	}

	public function avail_coupon_user_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->avail_coupon_user_list($request);
		$this->_sendResponse(json_encode($data));
	}
	public function redeem_coupon_user_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->redeem_coupon_user_list($request);
		$this->_sendResponse(json_encode($data));
	}
	public function wifi_offer(){
		$data = $this->channel_model->wifi_offer();
		$this->_sendResponse(json_encode($data));
	}

	public function undeletewifioffer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->undeletewifioffer($request);
		$this->_sendResponse(json_encode($data));
	}
	public function publishwifioffer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->publishwifioffer($request);
		$this->_sendResponse(json_encode($data));
	}

	
	public function addwifioffer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->addwifioffer($request);
		$this->_sendResponse(json_encode($data));
	}
	public function wifi_card_show(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->wifi_card_show($request);
		$this->_sendResponse(json_encode($data));
	}
	
	public function delete_wifioffer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->delete_wifioffer($request);
		$this->_sendResponse(json_encode($data));
	}

	public function wifiofferexcel(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->wifiofferexcel($request);
		$this->_sendResponse(json_encode($data));
	}
	public function edit_wifioffer_selected_store(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->edit_wifioffer_selected_store($request);
		$this->_sendResponse(json_encode($data));
	}

	public function editwifioffer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->editwifioffer($request);
		$this->_sendResponse(json_encode($data));
	}
	

	

	public function brand_default_tags(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->brand_default_tags($request);
		$this->_sendResponse(json_encode($data));
	}

	public function forget_password_emailcheck(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->forget_password_emailcheck($request);
		$this->_sendResponse(json_encode($data));
	}
	public function forget_password(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->forget_password($request);
		$this->_sendResponse(json_encode($data));
	}

	public function check_wifi_offer_editable(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->check_wifi_offer_editable($request);
		$this->_sendResponse(json_encode($data));
	}

	public function wifi_offer_locations(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->wifi_offer_locations($request);
		$this->_sendResponse(json_encode($data));
	}

	public function avail_wifi_coupon_user_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->avail_wifi_coupon_user_list($request);
		$this->_sendResponse(json_encode($data));
	}

	public function brand_voucher_activated_user_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->brand_voucher_activated_user_list($request);
		$this->_sendResponse(json_encode($data));
	}
	public function brand_voucher_redeem_user_list(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->brand_voucher_redeem_user_list($request);
		$this->_sendResponse(json_encode($data));
	}
	
	
	
	
	
	
	
	
	

	

	public function addFund_view(){
		$data["current_brand_balance"] =$this->channel_model->current_brand_balance(); ;
		$this->load->view('fund_entry', $data);
	}
	public function addFund_success_view($id){
		$data["current_brand_balance"] =$this->channel_model->current_brand_balance();
		$data["transection_detail"] = $this->channel_model->transection_detail($id);
		$this->load->view('fund_success', $data);
	}
	public function addFund_fail_view($id){
		$data["transection_detail"] = $this->channel_model->transection_detail($id);
		$this->load->view('fund_fail', $data);
	}
	public function addFund(){
		$amount =  $this->input->post('FundAmount');
		$formPostUrl = CITRUSPOSTURL;
		$secret_key = CITRUSSECRETKEY;
		$vanityUrl = CITRUSVANITYURL;
		$merchantTxnId = uniqid();
		$orderAmount = $amount;
		$currency = CITRUSCURRENCY;
		$dataid = $vanityUrl . $orderAmount . $merchantTxnId . $currency;
		$securitySignature = hash_hmac('sha1', $dataid, $secret_key);
		$notifyUrl = base_url() . 'channel/paymentnotify';
		$returnUrl = base_url() . 'channel/paymentresponse';
		$data['citrusdetail'] = array("merchantTxnId" => $merchantTxnId,"orderAmount" => $orderAmount,
			"currency" => $currency,"returnUrl" => $returnUrl,   "notifyUrl" => $notifyUrl,
			"secSignature" => $securitySignature,'posturl'=>$formPostUrl);
		$this->load->view('fund',$data);
	}
	public function paymentnotify() {

	}


	public function paymentresponse() {
		$postdata=$this->input->post();


		$datarr=array();

		if (isset($_POST['TxId'])) {
			// echo "sssssssssss"; die;
			set_include_path('../lib' . PATH_SEPARATOR . get_include_path());
			$secret_key = CITRUSSECRETKEY;
			$data = "";
			$flag = "true";


			$paymentPostResponse = $_POST;
			if (isset($_POST['TxId'])) {
				$txnid = $_POST['TxId'];
				$data .= $txnid;
			}
			if (isset($_POST['TxStatus'])) {
				$txnstatus = $_POST['TxStatus'];
				$data .= $txnstatus;
			}
			if (isset($_POST['amount'])) {
				$amount = $_POST['amount'];
				$data .= $amount;
			}
			if (isset($_POST['pgTxnNo'])) {
				$pgtxnno = $_POST['pgTxnNo'];
				$data .= $pgtxnno;
			}
			if (isset($_POST['issuerRefNo'])) {
				$issuerrefno = $_POST['issuerRefNo'];
				$data .= $issuerrefno;
			}
			if (isset($_POST['authIdCode'])) {
				$authidcode = $_POST['authIdCode'];
				$data .= $authidcode;
			}
			if (isset($_POST['firstName'])) {
				$firstName = $_POST['firstName'];
				$data .= $firstName;
			}
			if (isset($_POST['lastName'])) {
				$lastName = $_POST['lastName'];
				$data .= $lastName;
			}
			if (isset($_POST['pgRespCode'])) {
				$pgrespcode = $_POST['pgRespCode'];
				$data .= $pgrespcode;
			}
			if (isset($_POST['addressZip'])) {
				$pincode = $_POST['addressZip'];
				$data .= $pincode;
			}
			if (isset($_POST['signature'])) {
				$signature = $_POST['signature'];
			}

			$respSignature = hash_hmac('sha1', $data, $secret_key);
			if ($signature != "" && strcmp($signature, $respSignature) != 0) {
				$flag = "false";
			}

			if ($flag == "true") {

				if ($txnstatus == "SUCCESS") {
					$this->channel_model->add_order(true);

				} else {
					$this->channel_model->add_order();

				}
			} else {

				$this->channel_model->add_order();
				// $this->render('payment-failed', array('eventdetail' => $eventDetail, "eventStockList" => $eventStockList, 'paymentPostResponse' => $paymentPostResponse, 'amount' => $amount, 'event_id' => $event_id, 'message' => 'Citrus Response Signature and Our (Merchant) Signature Mis-Mactch'));
			}
		} else {

			$this->redirect(base_url()."channel");
		}
	}

	

	public function daily_unique_user_update(){
		$this->channel_model->daily_unique_user_update();
	}

	
	

	

	public function add_campaign_survey_offer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_campaign_survey_offer($request);
		$this->_sendResponse(json_encode($data));
	}

	public function add_campaign_survey_offer_image(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_campaign_survey_offer_image($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_survey(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_survey($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_survey_question(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_survey_question($request);
		$this->_sendResponse(json_encode($data));
	}
	// for Home channel api
	
	
	public function isp_user_list(){
		$data = $this->channel_model->isp_user_list();
		$this->_sendResponse(json_encode($data));
	}
	public function add_campaign_homwifi_offer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_campaign_homwifi_offer($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_campaign_survey_homewifi_offer(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_campaign_survey_homewifi_offer($request);
		$this->_sendResponse(json_encode($data));
	}

	public function add_campaign_survey_homewifi_offer_image(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_campaign_survey_homewifi_offer_image($request);
		$this->_sendResponse(json_encode($data));
	}
	public function dashboard_active_campaign_list_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_active_campaign_list_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	
	public function dashboard_expire_campaign_list_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->dashboard_expire_campaign_list_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_campaign_visitor_detail_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_visitor_detail_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function setcampaignId_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->setcampaignId_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function pauseCampaign_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->pauseCampaign_homewifi($request);
		$this->_sendResponse(json_encode($data));

	}
	public function resumeCampaign_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->resumeCampaign_homewifi($request);
		$this->_sendResponse(json_encode($data));

	}
	public function stopCampaign_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->stopCampaign_homewifi($request);
		$this->_sendResponse(json_encode($data));

	}
	public function cp_offer_not_attached_user(){
		$data = $this->channel_model->cp_offer_not_attached_user();
		$this->_sendResponse(json_encode($data));
	}
	public function cp_offer_new_user(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->cp_offer_new_user($request);
		$this->_sendResponse(json_encode($data));

	}
	public function campaign_graph_data_offerlisting_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_offerlisting_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_campaign_bannerflash_visitor_detail_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_bannerflash_visitor_detail_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_bannerflash_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_bannerflash_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_poll_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_poll_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_poll_question_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_poll_question_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_captcha_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_captcha_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_appdownload_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_appdownload_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_campaign_visitor_detail_appdownload_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_visitor_detail_appdownload_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_redeemable_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_redeemable_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_campaign_redeemable_visitor_detail_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_campaign_redeemable_visitor_detail_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_survey_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_survey_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_survey_question_homewifi(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_survey_question_homewifi($request);
		$this->_sendResponse(json_encode($data));
	}
	
	// new isp chagnes done
	public function cp_offer_attached_location(){
		$data = $this->channel_model->cp_offer_attached_location();
		$this->_sendResponse(json_encode($data));
	}
	
	// new isp changes done
	public function cp_offer_remove_location(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->cp_offer_remove_location($request);
		$this->_sendResponse(json_encode($data));

	}
	
	
	public function add_audit_program(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_audit_program(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_audit_program($request);
		$this->_sendResponse(json_encode($data));
	}
	public function retail_audit_location(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->retail_audit_location($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_location(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_location($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_date(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_date($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_code(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_code($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_sku(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_sku($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_audit_program_sku(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_audit_program_sku($request);
		$this->_sendResponse(json_encode($data));
	}
	
	public function add_audit_program_sku_delete(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_sku_delete($request);
		$this->_sendResponse(json_encode($data));
	}
	
	public function create_excel_program_location(){
		
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->create_excel_program_location($request);
		$this->_sendResponse(json_encode($data));
	}
	public function download_excel_program_location (){
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
		header('Content-Disposition: attachment;filename="assets/location_list.xlsx"');
		header('Cache-Control: max-age=0');
		ob_clean();
		flush();
		readfile('assets/location_list.xlsx'); exit;
	
	}
	public function get_total_location_total_user_assign(){
		
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_total_location_total_user_assign($request);
		$this->_sendResponse(json_encode($data));
	}
	public function update_location_program_team(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->update_location_program_team($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_is_cheklist_all(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_is_cheklist_all($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_checklist_sku(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_checklist_sku($request);
		$this->_sendResponse(json_encode($data));
	}
	public function get_audit_program_checklist_sku(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->get_audit_program_checklist_sku($request);
		$this->_sendResponse(json_encode($data));
	}
	public function add_audit_program_checklist_sku_delete(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->add_audit_program_checklist_sku_delete($request);
		$this->_sendResponse(json_encode($data));
	}
	public function campaign_graph_data_scratch(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = $this->channel_model->campaign_graph_data_scratch($request);
		$this->_sendResponse(json_encode($data));
	}
}

?>
