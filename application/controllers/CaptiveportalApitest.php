<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
//date_default_timezone_set('asia/kolkata');
//error_reporting(0);
class CaptiveportalApitest extends CI_Controller {
    public function __construct(){
header("Access-Control-Allow-Origin: *");
        parent::__construct();
	$this->load->library('My_PHPMailer');
       // $this->load->library('mongo_db');
        $this->load->model('captiveportal_modeltest');
    }
    public function index(){
	//echo $_SERVER['DOCUMENT_ROOT'];
        //$this->load->view('welcome_message');

    }
    private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	public function log($function,$request, $responce){
		//$data = $this->captiveportal_modeltest->log($function, $request, $responce);
	}
	
	
        
        public function captive_ad_data(){
           
		$_POST['requestData'] =  '{"isp_uid":"189","userId":"9560588376","locationId":"1070","via":"web","macid":"","platform_filter":"Android Phone"}';
	//	$_POST['requestData'] =  '{"isp_uid":"101","userId":"8178840727","locationId":"50","via":"web","macid":"","platform_filter":""}';
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			
			if(!isset($decodeData->usertype)){
				$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){
                $validatearr=array("userId","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->captive_ad_data($decodeData);
				}
				
			}else{
                            
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	 public function offerlisting_ad_data(){
           
		/*$_POST['requestData'] = '{"userId":"9560588376","apikey":"149785476711ba152fe3c5e65beb743d4151996b14",
                 "locationId":"3","via":"android","platform_filter":"all","macid":"sdd12:sdsf123","isp_uid":"101"}';*/
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
				$responsecode = $this->captiveportal_modeltest->authenticateApiGuestuser($decodeData->userId);
				
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
				$validatearr=array("userId","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->get_offerlisting_data($decodeData);
				}
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('offerlisting_ad_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	 public function sponsoredlisting_ad_data(){
		 
           
		/*$_POST['requestData'] = '{"userId":"580b4951734340b27c20c024","apikey":"14677965523058cfec51bc9064ef835d32f6ed3304",
                 "locationId":"3","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';*/
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
				$responsecode = $this->captiveportal_modeltest->authenticateApiGuestuser($decodeData->userId);
				
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){
                $validatearr=array("userId","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->get_sponsered_listing_data($decodeData);
				}
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('sponsoredlisting_ad_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	

        public function redirect_url_api()
        {
			//$_POST['requestData'] = '{"userId":"5947732fca94f1be817930ba","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","offer_id":"1","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';
          // $_POST['requestData'] = '{"userId":"5947732fca94f1be817930ba","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","offer_id":"1",via":"android"}';
           /* $_POST['requestData'] = '{"userId":"9560588376","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","offer_id":"2",
                 "locationId":"12","via":"android","view_counter":"1"}';*/
           $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(!isset($decodeData->usertype)){
				$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
	
			if($responsecode == 1){
                $validatearr=array("userId","locationId","offer_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->redirect_url_api($decodeData);
				}          
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('redirect_url_api',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		  public function app_download_data()
        {
         //$_POST['requestData'] = '{"userId":"7503945244","locationId":"34","via":"android","isp_uid":"134","platform_filter":"all","macid":"00:0C:F1:56:98:AD"}';
          
           $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "<pre>"; print_R($decodeData);die;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
		
			if($responsecode == 1){
                  $validatearr=array("userId","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->app_download_data($decodeData);
				}         
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('app_download_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
	
		
		 public function captcha_accurate_api()
        {
			//$_POST['requestData'] = '{"userId":"9560588376","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","offer_id":"4","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';
            
           $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
                $validatearr=array("userId","locationId","offer_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->captcha_accurate_api($decodeData);
				}              
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captcha_accurate_api',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		
		 public function offer_ad_detail(){
		 
        //$_POST['requestData'] = '{"userId":"580b4951734340b27c20c024","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","offer_id":"6","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';   
		//$_POST['requestData'] = '{"userId":"9560588376","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","via":"android","offer_id":"6","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';
                
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiGuestuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
                 $validatearr=array("userId","locationId","offer_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->offer_ad_detail($decodeData);
				}            
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('offer_ad_detail',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
        
        public function poll_answer()
        {
          /*  $_POST['requestData'] = '{"userId":"5947732fca94f1be817930ba","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","offer_id":"2","ques_id":"2","option_id":"1",
                 "locationId":"3","via":"android"}';*/
             //$_POST['requestData'] = '{"userId":"9560588376","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1ss","option_id":"1","via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
                 $validatearr=array("userId","locationId","offer_id","ques_id","option_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->poll_answer($decodeData);
				}            
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
			
		$this->log('poll_answer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
        }
		
		
		 public function poll_data_test()
        {
           
                          
				$data = $this->captiveportal_modeltest->poll_ad_datatest($decodeData);
		
		$this->_sendResponse(json_encode($data));
        }
		
		 public function redeem_offer()
        {
           /*   $_POST['requestData'] = '{"userId":"5947732fca94f1be817930ba","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","offer_id":"5",
                 "locationId":"3","via":"android"}';*/
           //  $_POST['requestData'] = '{"userId":"9560588376","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1","via":"android","platform_filter":"all","macid":"sdd12:sdsf123"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(!isset($decodeData->usertype)){
				$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
                $validatearr=array("userId","locationId","offer_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->redeem_offer($decodeData);
				}          
				
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('redeem_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
            
        }
		
			 public function shouut_sms_offer()
        {
			
			
			
           //   $_POST['requestData'] = '{"userId":"57e13ed1ca94f1a1b5b7068b","apikey":"14743794738824e883cab3aaada59ee84d173bc702","mobile":"9560588376"}';
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){
                          
				$data = $this->captiveportal_modeltest->shouut_sms_link($decodeData);
			}else{
                            
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('Shouut_sms_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
            
        }
		
		 public function app_install_click()
        {
		  //   $_POST['requestData'] = ' {"userid":"58208dffca94f1fd680d7b4f","cpofferid":"6"}';
             //$_POST['requestData'] = '{"userId":"57a9c0beb0474013641b0fd7","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
              $data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->app_install_click($decodeData);
			
		}
		$this->log('app_install_click',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
            
        }

   // dineout api
	public function dineout_offer_click_analytics()
	{


		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->dineout_offer_click_analytics($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_offer_click_analytics',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function dineout_sponsor_click_analytics()
	{


		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->dineout_sponsor_click_analytics($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_sponsor_click_analytics',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function dineout_offer_click_booking()
	{
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->dineout_offer_click_booking($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_offer_click_booking',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function dineout_sponsor_click_booking()
	{
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->dineout_sponsor_click_booking($decodeData);
			$data['resultCode'] = '1';
			$data['resultMsg'] = 'Success';
		}

		$this->log('dineout_sponsor_click_booking',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	//cp 2 api
	
		public function vadodracp2_offers_totalmb(){
		//$_POST['requestData'] = ' {"userId":"5947732fca94f1be817930ba","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","via":"web","macid":"","platform_filter":"Android Phone"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){

				$data = $this->captiveportal_modeltest->vadodracp2_offers_totalmb($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	
	
	public function cp2_offers_totalmb(){
		/*$_POST['requestData'] = '{"userId":"57bc131bab121c770f981352",
		"apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","platform_filter":"Android Phone"}';*/
//$_POST['requestData']=' {"userId":"9560588376","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","via":"web","macid":"","platform_filter":"Android Phone"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){
				$validatearr=array("userId","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->cp2_offers_totalmb($decodeData);
				}
				
			}else{

				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid(){

		//$_POST['requestData'] = '{"userId":"9560588376","apikey":"149785476711ba152fe3c5e65beb743d4151996b14","locationId":"3","via":"web","macid":"","platform_filter":"Android Phone","isp_uid":"200"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){
				$validatearr=array("userId","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->captive_ad_data_grid($decodeData);
				}
				
			}else{

				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data_grid',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function cp_offer_sceen_user()
	{
		// $_POST['requestData'] = ' {"userid":"58208dffca94f1fd680d7b4f","cpofferid":"6"}';
			$_POST['requestData'] = '{"isp_uid":"101","userid":"9560588376","cpofferid":"244","locationid":"404","via":"web","platform_filter":"","macid":"","offertype":"scratch","scratchid":"5","scratch_coupon":"667272"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{

			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("userid","locationid","cpofferid");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->cp_offer_sceen_user($decodeData);
				}
		}
		$this->log('cp_offer_sceen_user',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}
	public function captive_portal_impression()
	{
		//$_POST['requestData'] = '{"locationId" : "9", "macid" : "3c:4d", "platform_filter" : "Android Phone"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->captive_portal_impression($decodeData);
					$data['resultCode'] = '1';
					$data['resultMsg'] = 'Success';
				}
			
		}

		$this->log('captive_portal_impression',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function captive_portal_uniqueimpression()
	{
		//$_POST['requestData'] = '{"locationId" : "50", "macid" : "xx:xx:xx", "platform_filter" : ""}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->captive_portal_uniqueimpression($decodeData);
					$data['resultCode'] = '1';
					$data['resultMsg'] = 'Success';
				}
			
		}

		$this->log('captive_portal_uniqueimpression',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	
	public function captive_ad_data_grid_detail(){
		//$_POST['requestData'] = '{"offer_id":"2", "offer_type" : "poll_add","locationId":"51"}';

//echo "<pre>"; print_R($_POST); die;
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("offer_id","locationId");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->captive_ad_data_grid_detail($decodeData);
				}
			

		}
		$this->log('captive_ad_data_grid_detail',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function get_mb_data()
	{
		
		//$_POST['requestData'] = '{"isp_uid":"101","locationId":"11","amount":55}';
		$decodeData = json_decode($_POST['requestData']);
		$validatearr=array("locationId");
		$valid=$this->validate_request($decodeData,$validatearr);
		if(!$valid){
			$data['resultCode'] = '0';
			$data['resultmessage'] = 'Data Submitted Is Wrong';
		}else{
			$data = $this->captiveportal_modeltest->get_mb_data($decodeData);
		}
		
		$this->log('get_mb_data',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
		public function get_mb_list()
	{
		//$_POST['requestData'] = '{"locationid":"254"}';
		$decodeData = json_decode($_POST['requestData']);
		$validatearr=array("locationid");
		$valid=$this->validate_request($decodeData,$validatearr);
		if(!$valid){
			$data['resultCode'] = '0';
			$data['resultmessage'] = 'Data Submitted Is Wrong';
		}else{
			$data = $this->captiveportal_modeltest->get_mb_list($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	public function insert_paytm_success()
	{
		//$_POST['requestData']='{""}'
		$decodeData = json_decode($_POST['requestData']);
		$data = $this->captiveportal_modeltest->insert_paytm_success($decodeData);
	}
	public function activate_voucher()
	{

		//  $_POST['requestData'] = ' {"mobile":"9560588376","voucher":"3877874"}';
		//$_POST['requestData'] = '{"userId":"9560588376","apikey":"1470742718201444b4d0173c5c7ce9271dd88a4589","locationId":"12","offer_id":"10" ,"ques_id":"1","option_id":"1",via":"android"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("mobile");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->activate_voucher($decodeData);
			}
			

		}
		$this->log('activate_voucher',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));

	}
	public function voucher_availed()
	{
		//	 $_POST['requestData'] = ' {"mobile":"9560588376","voucher":"3877874"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("mobile");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->voucher_availed($decodeData);
			}
			

		}
		$this->log('voucher_availed',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function cp2_offers_totalmb_website(){
		/*$_POST['requestData'] = '{"userId":"586cfd80ca94f1f78ac2c9ff",
  "apikey":"14835377922c8914b5dfb5a95ae26025d1d07ab07a","location":"28.5494490000::77.2001370000",
  "platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){

				$data = $this->captiveportal_modeltest->cp2_offers_totalmb_website($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('cp2_offers_totalmb_website',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function captive_ad_data_grid_website(){
		/*$_POST['requestData'] = '{"userId":"586cfd80ca94f1f78ac2c9ff",
  "apikey":"14835377922c8914b5dfb5a95ae26025d1d07ab07a","location":"28.5494490000::77.2001370000",
  "platform_filter":"Android Phone"}';*/

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){

				$data = $this->captiveportal_modeltest->captive_ad_data_grid_website($decodeData);
			}else{

				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('captive_ad_data_grid_website',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}

	public function skhotspot_voucher_validate(){
		//$_POST['requestData'] = '{"mobile":"9650896116", "voucher":"462943"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->skhotspot_voucher_validate($decodeData);
		}
		$this->log('skhotspot_voucher_validate',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	
	public function get_survey_question()
	{
			//$_POST['requestData'] = '{"userId":"5878b1b3ca94f1decb73d608","apikey":"148430481974d1998d373741e9164432ca923fc09f","offer_id":"15" ,"ques_id":"","option_id":""}';
		//$_POST['requestData'] = '{"userId":"9560588376","apikey":"","locationId":"12","offer_id":"138" ,"ques_id":"1","option_id":"1","via":"android","isp_uid":"101",""}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
				$validatearr=array("userId","locationId","offer_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->get_survey_question($decodeData);
				}
				
			}else{
				
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('get_survey_question',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_subsequent_question()
	{
		
			//$_POST['requestData'] = '{"userId":"5878b1b3ca94f1decb73d608","apikey":"148430481974d1998d373741e9164432ca923fc09f","locationId":"10","offer_id":"15" ,"ques_id":"1","option_id":"4"}';
			//$_POST['requestData'] = ' {"userId":"9560588376","apikey":"1471943451c997edb6bd818c371a2a1b98dc5dafaf","locationId":"9","via":"web","macid":"","platform_filter":"Android Phone","offer_id":"78","ques_id":"1","option_id":"2"}';

		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			
			//echo "========>>>".$decodeData->userId;
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			
			if($responsecode == 1){
				$validatearr=array("userId","locationId","offer_id");
				$valid=$this->validate_request($decodeData,$validatearr);
				if(!$valid){
					$data['resultCode'] = '0';
					$data['resultmessage'] = 'Data Submitted Is Wrong';
				}else{
					$data = $this->captiveportal_modeltest->get_subsequent_question($decodeData);
				}
				
			}else{
				
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('get_subsequent_question',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	//flashapi code merge
	
	public function wifi_location_macid(){
		//$_POST['requestData'] = '{"locationId": "51","isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->wifi_location_macid($decodeData);
		}
		$this->log('wifi_location_macid',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function cp_top_risht_isp_logo(){
		//$_POST['requestData'] = '{"locationId": "51","isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'Wrong request';

		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->cp_top_risht_isp_logo($decodeData);
		}
		$this->log('cp_top_risht_isp_logo',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function captive_portal_location_wifi_offer(){
		/*$_POST['requestData'] = '{"locationId": ""}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->captive_portal_location_wifi_offer($decodeData);

		}
		$this->log('captive_portal_location_wifi_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function wifi_user_otp(){
		//$_POST['requestData'] = '{"macid": "dk9D4", "mobile": "9485858454", "otp": "3432"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->wifi_user_otp($decodeData);

		}
		$this->log('wifi_user_otp',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function wifi_user_otp_verify(){
		//$_POST['requestData'] = '{"mobile": "9485858454", "otp": "3432"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->wifi_user_otp_verify($decodeData);

		}
		$this->log('wifi_user_otp_verify',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
		
	public function wifi_user_new_registration(){
		//$_POST['requestData'] = '{"mobile": "9485858454"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->wifi_user_new_registration($decodeData);

		}
		$this->log('wifi_user_new_registration',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function wifi_user_free_session_add(){
		/*$_POST['requestData'] = '{"userId": "5698d47c01fcff0a6e8b4567", "apikey":
		"14528564445f4dcc3b5aa765d61d8327deb882cf99"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			if(!isset($decodeData->usertype)){
					$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			}else{
				$responsecode=1;
			}
			if($responsecode == 1){
				$data = $this->captiveportal_modeltest->wifi_user_free_session_add($decodeData);
			}else{
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_user_free_session_add',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function user_free_wifi_voucher_offer(){
	//	$_POST['requestData'] = '{"voucherid": "4", "locationId": "3", "via": "web"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->user_free_wifi_voucher_offer($decodeData);

		}
		$this->log('user_free_wifi_voucher_offer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function user_free_wifi_voucher_code_use(){
		/*$_POST['requestData'] = '{"voucherid": "2"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->user_free_wifi_voucher_code_use($decodeData);

		}
		$this->log('user_free_wifi_voucher_code_use',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
		public function user_free_wifi_voucher_codes(){
		$_POST['requestData'] = '{"phone": "7503945244"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->user_free_wifi_voucher_codes($decodeData);

		}
		$this->log('user_free_wifi_voucher_codes',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function check_user_last_seen_wifioffer(){
		/*$_POST['requestData'] = '{"userid": "56979e3c01fcff5c4d8b4567"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->check_user_last_seen_wifioffer($decodeData);

		}
		$this->log('check_user_last_sceen_wifioffer',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_locationid_mac()
	{
			//$_POST['requestData'] = '{"macid":"8A:DC:96:36:CD:E7"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("macid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_locationid_mac($decodeData);
			}	
			

		}
		$this->log('get_locationid_mac',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function isp_payment_gateways()
	{
		//$_POST['requestData'] = '{"isp_uid": "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("isp_uid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->isp_payment_gateways($decodeData);
			}
			

		}
		$this->log('isp_payment_gateways',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function isp_sms_gateways()
	{
		//$_POST['requestData'] = '{"isp_uid": "101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("isp_uid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->isp_sms_gateways($decodeData);
			}
			

		}
		$this->log('isp_sms_gateways',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function isp_email_gateways()
	{
		//$_POST['requestData'] = '{"isp_uid": "100"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("isp_uid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->isp_email_gateways($decodeData);
			}
			

		}
		$this->log('isp_email_gateways',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	
	public function get_htlocation_logo()
	{
		//$_POST['requestData'] = '{"locationId": "51","isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("isp_uid","locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_htlocation_logo($decodeData);
			}
			

		}
		$this->log('get_htlocation_logo',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	
	public function get_location_frommac()
	{
		$_POST['requestData'] = '{"macid": "12345"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array();
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_location_frommac($decodeData);
			}
			

		}
		$this->log('get_location_frommac',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_location_type()
	{
		//	$_POST['requestData'] = '{"locationId": "3"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_location_type($decodeData);
			}
			

		}
		$this->log('get_location_type',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	
	public function guest_setting()
	{
		//$_POST['requestData'] = '{"locationId":"39","type":"hospital"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->guest_setting($decodeData);
			}
			

		}
		$this->log('guest_setting',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	public function wifi_maruti_suzuki_campaign_analytics(){
		/*$_POST['requestData'] = '{"userId": "9650896116", "apikey":"14528564445f4dcc3b5aa765d61d8327deb882cf99", "macid": "macid here", "platform_filter": "Andorid Phone"}';*/
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			$responsecode = $this->captiveportal_modeltest->authenticateApiuser($decodeData->userId);
			if($responsecode == 1){
				$data = $this->captiveportal_modeltest->wifi_maruti_suzuki_campaign_analytics($decodeData);
			}else{
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Unauthorised access. Please Login again.';
			}
		}
		$this->log('wifi_maruti_suzuki_campaign_analytics',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function cafe_signup_pervisit()
	{
	//$_POST['requestData'] = '{"locationId":"22","userId":"9560588376"}';
		
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId","userId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->cafe_signup_pervisit($decodeData);
			}
		}
		$this->log('cafe_signup_pervisit',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function user_reward_list()
	{
		//$_POST['requestData'] = '{"locationId":"22","userId":"9560588376"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId","userId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->user_reward_list($decodeData);
			}
		}
		$this->log('user_reward_list',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function voucher_redeem_generate()
	{
		//$_POST['requestData'] = '{"locationId":"22","userId":"9560588376","rewards":["1::2::200","2::1::150"]}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId","userId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->voucher_redeem_generate($decodeData);
			}
			
		}
		$this->log('user_reward_list',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function cafe_pin()
	{
	//	$_POST['requestData'] = '{"locationId":"79","isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->cafe_pin($decodeData);
			}
			
		}
		$this->log('cafe_pin',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function is_otp_disabled()
	{
			//$_POST['requestData'] = '{"locationId":"51","isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->is_otp_disabled($decodeData);
			}
			
		}
		$this->log('is_otp_disabled',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_country_code()
	{
		//$_POST['requestData'] = '{"isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("isp_uid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_country_code($decodeData);
			}
			
		}
		$this->log('get_country_code',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function is_voucher_available()
	{
		//$_POST['requestData'] = '{"locationId":"91","isp_uid":"101"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->is_voucher_available($decodeData);
			}
			
		}
		$this->log('get_country_code',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function otp_gateway_type()
	{
		//$_POST['requestData'] = '{"isp_uid":"111"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("isp_uid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->otp_gateway_type($decodeData);
			}
			
			
		}
		$this->log('otp_gateway_type',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function onehop_traffic_analytics()
	{
	    $json = file_get_contents("php://input");
		//$this->log('onehop_traffic_analytics','', $json);//call log function
		
		$data = array();
		if(count($json) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			//$decodeData = json_decode($_POST['requestData']);
			$decodeData = json_decode($json, true);
			//$data = $this->captiveportal_modeltest->onehop_traffic_analytics($decodeData);
		}
		//$this->log('onehop_traffic_analytics',$json, $data);//call log function
		//$this->_sendResponse(json_encode($data));

	}
	
	public function retail_offer_list()
	{
		//$_POST['requestData'] = '{"locationId":"104","isp_uid":"101","macid":"ss:sss:ss"}';
		$data = array();
		if(count($_POST) == 0){
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultcode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->retail_offer_list($decodeData);
			}
			
		}
		$this->log('retail_offer_list',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function retail_offer_redeem()
	{
		//$_POST['requestData'] = '{"offerid":"4","uid":"9560588376","locationId":"104","macid":"ss:sss:ss"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId","offerid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->retail_offer_redeem($decodeData);
			}
			
		}
		$this->log('retail_offer_redeem',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function retail_background_wifisetting()
	{
		//$_POST['requestData'] = '{"locationId":"104","isp_uid":"101"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->retail_background_wifisetting($decodeData);
			}
			
		}
		$this->log('retail_background_wifisetting',$_POST['requestData'], $data);//call log function
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function get_captivedata_param()
	{
		//$_POST['requestData'] = '{"locationId":"80","isp_uid":"101"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_captivedata_param($decodeData);
			}
			
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
		public function get_userid_fromac()
	{
		//$_POST['requestData'] = '{"macid":"xx:aa:ss:09"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("macid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_userid_fromac($decodeData);
			}
			
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function cafe_rewardlist()
	{
		//$_POST['requestData'] = '{"locationId":"80"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->cafe_rewardlist($decodeData);
			}
			
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function send_vc_email_msg()
	{
			//$_POST['requestData'] = '{"locationId":"80","vc_code":"FB1VMZ43","userId":"573975690","email":"styagibtech@gmail.com","is_sendvcemail":"1","isp_uid":"101"}';
			if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId","userId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->send_vc_email_msg($decodeData);
			}
			
		}
			$this->log('send_vc_email_msg',$_POST['requestData'], $data);//call log function
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_customslideshow_logo()
	{
		//$_POST['requestData'] = '{"locationId":"51","isp_uid":"101"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_customslideshow_logo($decodeData);
			}
			
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_defaultlocation_logo()
	{
		//$_POST['requestData'] = '{"locationId":"71","isp_uid":"121"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locationId");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->get_defaultlocation_logo($decodeData);
			}
			
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function send_otp()
	{
		$_POST['requestData']=  '{"isp_uid":"101","mobile":"9015137090"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			
			$validatearr=array("isp_uid","mobile");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->send_otp($decodeData);
			}
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
		public function validate_blacklisted()
	{
		//$_POST['requestData']=  '{"locid":"254","param":"9560588376"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$validatearr=array("locid");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->validate_blacklisted($decodeData);
			}
			
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function validate_request($request,$validatearr)
	{
		
		$valid=true;
		foreach($validatearr as $val){
			//echo "======>>".$request->$val."<br/>";
			if(isset($request->$val) && $request->$val!='')
			{
				if($val=="isp_uid"){
					if(!is_numeric($request->$val)){
						$valid=false;
					}
				}else if($val=="userId"){
					$length=strlen($request->$val);
					if($length>=4 && $length<=16){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
					}else{
						$valid=false;
					}
				}else if($val=="shmobileno"){
					$length=strlen($request->$val);
					if($length>=4 && $length<=16){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
					}else{
						$valid=false;
					}
				}else if($val=="locationId"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="macid"){
						if(!preg_match('/([a-fA-F0-9]{2}[:|\-]?){6}/', $request->$val)){
							$valid=false;
						}
				}else if($val=="offer_id"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="view_counter"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="ques_id"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="option_id"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="cpofferid"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="userid"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="locationid"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}else if($val=="mobile"){
					$length=strlen($request->$val);
					if($length>=6 && $length<=16){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
					}else{
						$valid=false;
					}
				}else if($val=="offerid"){
						if(!is_numeric($request->$val)){
							$valid=false;
						}
				}
			}else{
				$valid=false;
			}
			
		}
				return $valid;
	}
	
	
	public function send_otp_dominos()
	{
		//$_POST['requestData']=  '{"mobile":"9810678773"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			
			$validatearr=array("mobile");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->send_otp_dominos($decodeData);
			}
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
		
		
		
	}
	
	public function user_registration()
	{
		$_POST['requestData']=  '{"isp_uid":"101","shmobileno":"9814575210","passord":"aasdasd","gender":"18-30","age_group":"M","expiration":"2018-09-30","uemail":"sdsda@asd.com"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			
			$validatearr=array("isp_uid","shmobileno");
			$valid=$this->validate_request($decodeData,$validatearr);
			if(!$valid){
				$data['resultCode'] = '0';
				$data['resultmessage'] = 'Data Submitted Is Wrong';
			}else{
				$data = $this->captiveportal_modeltest->user_registration($decodeData);
			}
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function lg_webos_offer()
	{
		$_POST['requestData']=  '{"last_offer":""}';
		//$_POST = file_get_contents('php://input');
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
		//	$decodeData = json_decode($_POST);
			
			
			$data = $this->captiveportal_modeltest->lg_webos_offer($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
		
	}
	
	 public function retail_score_api()
	{
		$_POST['requestData']=  '{"locid":"668","date":"2018-11-03 13:40:10"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->captiveportal_modeltest->score_api($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	
	
	
	
	
	

	
}

?>
