<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Offlineapi extends CI_Controller {

	public function __construct(){
		parent :: __construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->model('offlineapi_model');
		  $this->load->model('publicwifi_model');
		ini_set('memory_limit', '-1');
		
	}
	
	public function log($request, $responce){
		$data = $this->offlineapi_model->log($request, $responce);
	}

	public function index(){
		echo "home page";
		
	}
	
	 private function _sendResponse($data) {
	//header('application/json; charset=utf-8');
	echo $data;
    }
	
	public function sync_datacontent()
	{
		//echo "sync_datacontent <br>";
	 //echo "<pre>"; print_R($_POST);
	//b8:27:eb:d3:e6:cb
		//$_POST['requestData'] = '{"macId":["b8:27:eb:79:ee:eb","b8:27:eb:2c:bb:be\n"],"apikey":"","buildid":"","syncincomplete":0}';
		
		//$_POST['requestData'] = '{"macId":[""],"apikey":"","buildid":"","syncincomplete":0}';
		//$postdata = file_get_contents("php://input");
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);
			//$decodeData = json_decode($postdata);
			$data = $this->offlineapi_model->sync_datacontent($decodeData);
		}
		$this->log($_POST['requestData'], json_encode($data));//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function sync_db()
	{
		//$_POST['requestData'] = '{"macId":["b8:27:eb:6b:0e:8a","b8:27:eb:3e:5b:df\n"],"apikey":"56d3e872441ccff58e6919230c1ef345","buildid":"4.3"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->sync_db($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function new_ssid_get()
	{
	//$_POST['requestData'] = '{"macId":["b8:27:eb:6d:aa:79","b8:27:eb:38:ff:2c\n"]}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->new_ssid_get($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));	
	}

	public function sync_upload_db()
	{
		//$_POST['requestData'] = '{"sqlfile":"232_upload_radius.sql","apikey":"b8:27:eb:6b:0e:8a"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->sync_upload_db($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}	
	
	public function get_resouces_data()
	{
		//$_POST['requestData'] = '{"macId":["b8:27:eb:6d:aa:79","b8:27:eb:38:ff:2c\n"]}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->get_resouces_data($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));	
		
	}
	
	
	
	public function update_vendor_info()
	{
		echo phpinfo(); die;
		
	}
	
	public function ping_insert_data()
	{
		//$_POST['requestData']='{"loc_id":"243","macid":["b8:27:eb:89:bd:9a","b8:27:eb:dc:e8:cf\n"],"status":1,"pinged_on":"2018-03-13 15:55:21"}';
			if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->ping_insert_data($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function build_version_start()
	{
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->build_version_start($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function build_version_update()
	{
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->build_version_update($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_server_info()
	{
		//$_POST['requestData'] = '{"macId":"b8:27:eb:79:ee:eb"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->get_server_info($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function get_server_info_secure()
	{
		//$_POST['requestData'] = '{"macId":"b8:27:eb:79:ee:eb"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->get_server_info_secure($decodeData);
		}
		
		$this->_sendResponse(json_encode($data));
	}
	
	public function copy_s3_state()
	{
		//$_POST['requestData'] = '{"macId":"b8:27:eb:79:ee:eb","syncmanully":1}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			$decodeData = json_decode($_POST['requestData']);

			$data = $this->offlineapi_model->copy_s3_state($decodeData);
		}
		$this->log($_POST['requestData'], json_encode($data));//call log function
		$this->_sendResponse(json_encode($data));
	}
	
	public function send_visitor_email()
	{
		//$_POST['requestData']=  '{"visitingemail":"sandeep@shouut.com","person_tomett":"Deepak","visiting_company":"IBM","meeting_purpose":"official","visitor_name":"vikas","visitor_email":"vikas@shouut.com","visitor_org":"SHOUUT"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapi_model->send_visitor_email($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
	
	public function cancel_sync()
	{
		//$_POST['requestData']=  '{"loc_id":"364"}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapi_model->cancel_sync($decodeData);
		}
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function sync_data_completed()
	{
		
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapi_model->sync_data_completed($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
		
	}
	
	public function sync_update_statuses()
	{
		//$_POST['requestData']='{"macid":"b8:27:eb:79:ee:eb","buildid":"9","progress_status":"zipdownloaded","dataarr":{"zip_size":8204438,"last_file":"offlinedynamic.zip"}}';
		if(count($_POST) == 0){
		$data = array();
			$data['resultCode'] = '0';
			$data['resultMsg'] = 'please send valid request';
		}else{
			
			$decodeData = json_decode($_POST['requestData']);
			$data = $this->offlineapi_model->sync_update_statuses($decodeData);
		}
		//$data = $this->captiveportal_modeltest->get_captivedata_param();
		$this->_sendResponse(json_encode($data));
	}
}
