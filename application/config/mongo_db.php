<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------------
| This file will contain the settings needed to access your Mongo database.
|
|
| ------------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| ------------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['write_concerns'] Default is 1: acknowledge write operations.  ref(http://php.net/manual/en/mongo.writeconcerns.php)
|	['journal'] Default is TRUE : journal flushed to disk. ref(http://php.net/manual/en/mongo.writeconcerns.php)
|	['read_preference'] Set the read preference for this connection. ref (http://php.net/manual/en/mongoclient.setreadpreference.php)
|	['read_preference_tags'] Set the read preference for this connection.  ref (http://php.net/manual/en/mongoclient.setreadpreference.php)
|
| The $config['mongo_db']['active'] variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| MongoDB has a simple web based administrative port at 28017 by default.

| There is no HTTP access at the default port of 27017 (which is what the error message is trying to suggest). The default port is
    used for native driver access, not HTTP traffic.
*/


/*$config['mongo_host'] = 'localhost';
$config['mongo_port'] = '27017';
$config['mongo_db'] = 'shouut';
$config['mongo_user'] = 'admin';
$config['mongo_pass'] = 'admin';
$config['mongo_persist'] = TRUE;
$config['mongo_persist_key'] = 'ci_mongo_persist';*/

//db.createUser({user: "admin", pwd: "admin", roles: [{role: "userAdmin", db: "shouut"}]})

$config['mongo_db']['active'] = 'default';

$config['mongo_db']['default']['no_auth'] = FALSE;
$config['mongo_db']['default']['hostname'] = '103.20.213.152';
$config['mongo_db']['default']['port'] = '27017';
//$config['mongo_db']['default']['port'] = '28017'; 
$config['mongo_db']['default']['username'] = 'admin';
$config['mongo_db']['default']['password'] = 'G1@nt@sh0uut#@)!%';
$config['mongo_db']['default']['database'] = 'decibel_analytics';
$config['mongo_db']['default']['db_debug'] = TRUE;
$config['mongo_db']['default']['return_as'] = 'array';
$config['mongo_db']['default']['write_concerns'] = (int)1;
$config['mongo_db']['default']['journal'] = TRUE;
$config['mongo_db']['default']['read_preference'] = NULL;
$config['mongo_db']['default']['read_preference_tags'] = NULL;

/*$config['mongo_db']['default']['no_auth'] = FALSE;
$config['mongo_db']['test']['hostname'] = 'localhost';
$config['mongo_db']['test']['port'] = '27017';
$config['mongo_db']['test']['username'] = 'username';
$config['mongo_db']['test']['password'] = 'password';
$config['mongo_db']['test']['database'] = 'database';
$config['mongo_db']['test']['db_debug'] = TRUE;
$config['mongo_db']['test']['return_as'] = 'array';
$config['mongo_db']['test']['write_concerns'] = (int)1;
$config['mongo_db']['test']['journal'] = TRUE;
$config['mongo_db']['test']['read_preference'] = NULL;
$config['mongo_db']['test']['read_preference_tags'] = NULL;*/

/* End of file database.php */
/* Location: ./application/config/database.php */
