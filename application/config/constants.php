<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


//define misc type constant
define('PLANTYPE','plan_type');
define('TIMECALCULATION','time_calculated_on');
define('DATACALCULATION','data_calculated_on');
define('TOPUPTYPE','topup_type');

//define table Constant

define('SHTMISC','sht_miscellaneous');
define('SHTMISCDETAIL','sht_miscellaneous_detail');
define('SHTPLAN','sht_plan');
define('SHTTOPUP','sht_topup');
define('SHTNAS','nas');



// for payment gateway(citrus) staging
//define('CITRUSSECRETKEY','328039deb6d6987d605a46ac2b6fa999205f52bf');
//define('CITRUSPOSTURL','https://sandbox.citruspay.com/shouut');
//define('CITRUSVANITYURL','sk0ceka6cn');
// for payment gateway(citrus) live
define('CITRUSSECRETKEY','3ecd0ed6bb71827e216912a7c85d2db5590e7322');
define('CITRUSPOSTURL','https://www.citruspay.com/shouut');
define('CITRUSVANITYURL','x586xy8bnd');

define('CITRUSCURRENCY','INR');
//define home page image path
define('IMAGEPATH','https://www.shouut.com/isp_portal/assets/media/');
define('IMAGEPATHISP','https://www.shouut.com/isp_portal/ispmedia/');

//define('REWARD_IMAGEPATH','https://d392o9g87c202y.cloudfront.net/reward/');
define('REWARD_IMAGEPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/reward/');
define('REWARD_IMAGEPATHURL','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/reward/');
//define('REWARD_IMAGEPATHURL','https://d392o9g87c202y.cloudfront.net/reward/');

define('CAMPAIGN_IMAGEPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/reward/campaign/images/');
//define('CAMPAIGN_IMAGEPATH','https://d392o9g87c202y.cloudfront.net/reward/campaign/images/');
//define('CAMPAIGN_VIDEOPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/reward/campaign/videos/');
define('CAMPAIGN_VIDEOPATH','https://d392o9g87c202y.cloudfront.net/reward/campaign/videos/');
//define('ISPPATH','https://d392o9g87c202y.cloudfront.net/isp/isp_logo/');
define('ISPPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_logo/');
define('WIFI_IMAGEPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/reward/');
//define('WIFI_IMAGEPATH','https://d392o9g87c202y.cloudfront.net/reward/');
//define('ISP_IMAGEPATH','https://d392o9g87c202y.cloudfront.net/isp/isp_logo/');
define('ISP_IMAGEPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/isp_logo/');

define('CDNPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/');

define('VOUCHERPATH','https://www.shouut.com/isp_hotspot/');
define('FOODLE_IMAGEPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/foodle_rewards_icon/');


define('SHTSERVICES','sht_services');
define('SHTPLANREGION','sht_plan_region');
define('SHTPLANPRICING','sht_plan_pricing');
define('SHTTOPUPPRICING','sht_topup_pricing');
define('SHTTOPUPREGION','sht_topup_region');
define('SHTDATACCTN','sht_dataunaccountancy');
define('SHTSPEEDTOPUP','sht_speedtopup');
define('PLANACTIVATIONTAB','sht_plan_activation_panel');
define('TOPUPACTIVATIONTAB','sht_topup_activation_panel');
define('SHTDEPTREGION','sht_dept_region');
define('SHTDEPT','sht_department');
define('SHTISPPERM','sht_isp_permission');
define('SHTTEAMUSER','sht_isp_users');


//mongo db tableconstant
/********* Tables Name ****************/
define('PREFIX','sh_');
define('TBL_USER',PREFIX.'user');
define('TBL_GUEST_USER',PREFIX.'guest_user');
define('TBL_EMOTION',PREFIX.'emotion');
define('TBL_MESSAGE',PREFIX.'message');
define('TBL_CLUSTER_MESSAGE',PREFIX.'cluster_message');
define('TBL_MESSAGE_TYPE',PREFIX.'message_type');
define('TBL_MESSAGE_LIKES',PREFIX.'message_likes');
define('TBL_MESSAGE_COMMENTS',PREFIX.'message_comments');
define('TBL_LOGIN_DETAILS',PREFIX.'login_details');
define('TBL_USER_FOLLOW',PREFIX.'user_follow');
define('TBL_PLACE_FOLLOW',PREFIX.'place_follow');
define('TBL_BOOKMARK',PREFIX.'bookmark');
define('TBL_CITIES',PREFIX.'cities');
define('TBL_STATES',PREFIX.'states');
define('TBL_COUNTRY',PREFIX.'country');
define('TBL_PEOPLE_PICKS',PREFIX.'people_picks');
define('TBL_PLACE_PICKS',PREFIX.'place_picks');
define('TBL_ANALYTICS_LOCATION','TBL_ANALYTICS_LOCATION');
define('TBL_REWARD_LIKES','re_reward_likes');
define('TBL_CAMPAIGN','re_campaign');
define('TBL_BRAND','re_brand');
define('TBL_REWARD_CARD','re_reward_card');
define('TBL_BRAND_FOLLOW','re_brand_follow');


//define('CLOUD_IMAGEPATH','http://cdn101.shouut.com/shouutmedia/isp/');
define('CLOUD_IMAGEPATH','https://s3-ap-southeast-1.amazonaws.com/giantlabsshouut/shouutmedia/isp/');
// Amazon S3 path
define('AMAZONPATHWIKI','shouutmedia/isp/');
define('AMAZONPATH','shouutmedia/reward/');
define('AMAZONPATHWEB','shouutmedia/website/');
define('AMAZONPATHLOCAL','shouutmedia/live/');
//define('awsAccessKey', 'AKIAIFYZMW5VRSTAF6MA');
//define('awsSecretKey', '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');
//define('awsAccessKey', 'AKIAJ5JL6EIP5BICL2NQ');
//define('awsSecretKey', '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');
define('bucket','giantlabsshouut');
define('AMAZONPATH_360','shouutmedia/');

define('ISPURL', 'https://www.shouut.com/');
define('ROUTERFILEPATH',$_SERVER['DOCUMENT_ROOT'].'/isp_consumer_api/');// for copy router file

define('ISPUID', '101');
define('NASCONF','http://103.206.180.82/clientsconf/script.php');

define('SMTP_HOST', 'ssl://smtp.gmail.com');
define('SMTP_PORT', 465);
define('SMTP_USER', 'talktous@shouut.com');
define('SMTP_PASS', 'G1@nt@sh0uut#');

// new constant
define('AUTH_SERVER_IP', '103.20.214.108');
define('WHITELIST_IP1', '103.20.213.149');
define('WHITELIST_IP2', '103.20.214.108');
define('ONEHOP_BEACON_URL', 'http://169.38.113.103/passiveprob/passiveController/onehop_traffic_analytics_india');
//define('ONEHOP_BEACON_URL', 'http://169.38.93.196/passiveprob/passiveController/onehop_traffic_analytics_india');
//define('ONEHOP_BEACON_URL', 'https://www.shouut.com/isp_consumer_api/beaconApi/onehop_traffic_analytics');
//define('ONEHOP_BEACON_URL', 'https://www.shouut.com/isp_consumer_api/captiveportalApi/onehop_traffic_analytics');
define('IS_INDIA_COUNTRY', '1');
define('ISP_URL', 'https://www.shouut.com/ispadmins/');
define('OFFLINE_CODE', 'https://www.shouut.com/');
define("OFFLINE_DIR","/var/www/html/isp_hotspot/offline/");


require_once( BASEPATH .'database/DB.php' );
//$CI =& get_instance(); $db = $CI->db;
$db =& DB();
$query = $db->get( 's3_authkey' );
foreach( $query->result() as $row ){
    if($row->awsAccessKey) {
        define( 'awsAccessKey', $row->awsAccessKey);
    }
    if($row->awsSecretKey) {
        define( 'awsSecretKey', $row->awsSecretKey );
    }
}


