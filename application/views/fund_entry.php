<!--!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
</div>
<form id="myForm" align="center" method="post" action="<?php echo base_url()?>channel/addFund" name="formorder">
Enter Amount: <input type="text" name="FundAmount">
    <input type="Submit" value="Pay Now"/>
</form>

</body>
</html-->



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>SHOUUT</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">

</head>

<body>
<!-- Start your project here-->
<div class="web-container">
    <div class="row">
        <div class="col-sm-12">
            <div class="modal-title"><center><img src="<?php echo base_url()?>assets/img/add-store.png" class="img-responsive"/></center></div>
            <div class="modal-title"><h4>ADD MONEY</h4></div>
            <div class="modal-title"><h5>CURRENT BALANCE : &#8377; <?php echo $current_brand_balance['balance'];
                    ?></h5></div>
            <!--/.web-body -->
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <form id="myForm" name="myForm" onsubmit="return validateForm()" align="center" method="post"
                  action="<?php echo base_url()
                  ?>channel/addFund" name="formorder">
                <div class="form-group">
                    <div class="col-sm-10 offset-md-1">
                        <label>ENTER AMOUNT TO ADD TO WALLET</label>
                        <div class="form-group-bd">
                            <input type="tel" class="form-control form-input" id="FundAmount" name="FundAmount">
                            <span class="form-group-text">&#8377;</span>
                            <span id="amount_error" style="color:red"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10 offset-md-1">
                        <label>QUICK ADD</label>
                        <div class="col-sm-12" style="padding:0px;">
                            <div class="col-sm-3" style="padding:0px 5px">
                                <button type="button" class="btn btn-primary-outline waves-effect
                                            btn-block" id="1000" onclick="add_amount(this.id)">&#8377; <span
                                        >1000</span></button>
                            </div>
                            <div class="col-sm-3" style="padding:0px 5px">
                                <button type="button" class="btn btn-primary-outline waves-effect
                                            btn-block" id="2000" onclick="add_amount(this.id)">&#8377; <span >2000</span></button>
                            </div>
                            <div class="col-sm-3" style="padding:0px 5px">
                                <button type="button" class="btn btn-primary-outline waves-effect
                                            btn-block" id="4000" onclick="add_amount(this.id)">&#8377; <span >4000</span></button>
                            </div>
                            <div class="col-sm-3" style="padding:0px 5px">
                                <button type="button" class="btn btn-primary-outline waves-effect
                                            btn-block" id="8000" onclick="add_amount(this.id)">&#8377; <span >8000</span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <button type="submit" class="btn btn-secondary btn-lg waves-effect waves-light btn-block" style="padding:15px 0px; margin:35px 0px">NEXT</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--web-container --->




<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/tether.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/mdb.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {
        //code here
       // $('#gridSystemModal').modal('show');
    });
    function add_amount(id){
        $("#FundAmount").val(id);
    }
    function validateForm() {
        var x = document.forms["myForm"]["FundAmount"].value;
        if (x == null || x == "") {
            $("#amount_error").html("Enter amount");
            return false;
        }
        else if(isNaN(x)){
            $("#amount_error").html("Enter valid amount");
            return false;
        }
        else{
            $("#amount_error").html("");
            return true;
        }
    }
</script>
</body>

</html>
