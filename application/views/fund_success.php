<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>SHOUUT</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="<?php echo base_url()?>assets/css/mdb.min.css" rel="stylesheet">

    <!-- Your custom styles (optional) -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">

</head>

<body>
<!-- Start your project here-->
<div class="web-container">
    <div class="row">
        <div class="col-sm-12">
            <div class="web-header">
                <img src="<?php echo base_url()?>assets/img/logo.png"  class="img-responsive"/>
            </div>
            <!--/.web-header -->
            <div class="web-body">
                <img src="<?php echo base_url()?>assets/img/circle-img.png"  class="img-responsive"/>
                <h4>Payment Successful!</h4>
                <h5>CURRENT BALANCE</h5>
                <h2><small>&#8377;</small> <?php echo $current_brand_balance['balance']?></h2>
            </div>
            <!--/.web-body -->
        </div>
    </div>
    <div class="row" style="margin-top:15px;">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <p>Authorisation ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $transection_detail['authIdCode']?></p>
            <p>Trn. Date & Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $transection_detail['txnDateTime']?> </p>
            <p>Transaction ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $transection_detail['transactionId']?></p>
            <p>Issuer Ref No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $transection_detail['issuerRefNo']?></p>
            <p>Trn. Ref No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $transection_detail['TxRefNo']?></p>
            <h5 style="color:#36465f; font-size:12px; font-weight:600">Reciept has also been emailed to <?php echo $transection_detail['email']?></h5>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <a href="#" class="pull-right" style="color:#e61e5a; font-size:12px; text-decoration:underline">Download as PDF</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <button type="button" class="btn btn-secondary btn-lg waves-effect waves-light btn-block"
                    style="padding:15px 0px; margin:15px 0px" onclick="close_window()">DONE</button>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp;</div>
    </div>
</div>

<!-- JQuery -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js"></script>
<script>
    function close_window() {
        window.close();
    }
</script>
</body>

</html>
