<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

?>
<!DOCTYPE html>
<html>
<head>
    <title>Payment Redirection...</title>
</head>
<body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<form id="stripedetailform" action="<?php echo base_url().'foodiq/spaysuccess' ?>" method="post">
	<div style="margin:auto;">
		<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
		data-key="pk_test_BPzoRxhbOg68kZcgyEkNLh4Z"
		data-amount="<?php echo $amount*100;?>" data-description="Payment For <?php echo "$".$amount;?>">
		</script>
	</div>
	<input type="hidden" name="amt" value="<?php echo $amount*100;?>">
	<input type="hidden" name="userid" value="<?php echo $mobile;?>">
        <input type="submit" class="stripe-button-el" value="Submit">
</form>
<script type="text/javascript">var base_url = "<?php echo base_url() ?>";</script>
<script type="text/javascript">
 $(function(){  // document.ready function...
   $('.stripe-button-el').css('display', 'none');
   setTimeout(function(){
      $('.stripe-button-el').trigger( "click" );
    },5000);
});

$(document).on("DOMNodeRemoved",".stripe_checkout_app", close);

</script>
</body>
</html>
